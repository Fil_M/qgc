--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.7
-- Dumped by pg_dump version 9.5.7

-- Started on 2017-08-02 11:39:32 AEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE qgc;
--
-- TOC entry 3549 (class 1262 OID 69572)
-- Name: qgc; Type: DATABASE; Schema: -; Owner: fil
--

CREATE DATABASE qgc WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE qgc OWNER TO fil;

\connect qgc

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12427)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--


--
-- TOC entry 3552 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--


SET search_path = public, pg_catalog;

--
-- TOC entry 487 (class 1255 OID 69620)
-- Name: admin_menu_children(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION admin_menu_children(root_id integer) RETURNS TABLE(admin_menu_id integer, level smallint, menu_text character varying, href character varying, parent_menu_id integer)
    LANGUAGE sql
    AS $_$
WITH RECURSIVE breadcrumb(admin_menu_id,level, menu_text,href, parent_menu_id) AS (
          SELECT admin_menu_id, level,menu_text, href,parent_menu_id FROM admin_menu  WHERE admin_menu_id = $1
          UNION
            SELECT admin_menu.admin_menu_id,admin_menu.level, admin_menu.menu_text,admin_menu.href, admin_menu.parent_menu_id FROM admin_menu , breadcrumb
            WHERE breadcrumb.admin_menu_id = admin_menu.parent_menu_id)
        SELECT * FROM breadcrumb where admin_menu_id != $1;
$_$;


ALTER FUNCTION public.admin_menu_children(root_id integer) OWNER TO postgres;

--
-- TOC entry 488 (class 1255 OID 69621)
-- Name: admin_menu_children(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION admin_menu_children(root_id integer, lvl integer) RETURNS TABLE(admin_menu_id integer, level smallint, menu_text character varying, href character varying, parent_menu_id integer)
    LANGUAGE sql
    AS $_$
WITH RECURSIVE breadcrumb(admin_menu_id,level, menu_text,href, parent_menu_id) AS (
          SELECT admin_menu_id, level,menu_text, href,parent_menu_id FROM admin_menu  WHERE admin_menu_id = $1
          UNION
            SELECT admin_menu.admin_menu_id,admin_menu.level, admin_menu.menu_text,admin_menu.href, admin_menu.parent_menu_id FROM admin_menu , breadcrumb
            WHERE breadcrumb.admin_menu_id = admin_menu.parent_menu_id)
        SELECT * FROM breadcrumb where admin_menu_id != $1 and level = $2;
$_$;


ALTER FUNCTION public.admin_menu_children(root_id integer, lvl integer) OWNER TO postgres;

--
-- TOC entry 489 (class 1255 OID 69622)
-- Name: atw_status(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION atw_status(id integer) RETURNS TABLE(cnt_doc integer, sum_stat integer)
    LANGUAGE plpgsql COST 1 ROWS 1
    AS $_$
DECLARE
 cntt integer;
 summ integer;

BEGIN
	SELECT coalesce(sum(status),0)  from atw_document ad where ad.atw_id = $1 into summ;
    
    select count(atw_document_id) from atw_document ad  where ad.atw_id = $1 into cntt;

    
    return query select cntt,summ;
    
    
END;
$_$;


ALTER FUNCTION public.atw_status(id integer) OWNER TO postgres;

--
-- TOC entry 490 (class 1255 OID 69623)
-- Name: calloffs(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION calloffs(area integer, well integer) RETURNS TABLE(calloff_order_id integer, con_name text)
    LANGUAGE sql COST 2 ROWS 10
    AS $_$
select calloff_order_id,name from calloff_order co LEFT JOIN  contractor c using(contractor_id) where area_id = $1 
and $2 in (select (unnest(string_to_array(co.well_ids::text,'|')::int[])));
$_$;


ALTER FUNCTION public.calloffs(area integer, well integer) OWNER TO postgres;

--
-- TOC entry 491 (class 1255 OID 69624)
-- Name: contactall_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION contactall_from_ids(con_ids text) RETURNS TABLE(seltype text, contact_id integer, first_name text, last_name text, email text, expires date)
    LANGUAGE sql COST 2 ROWS 10
    AS $_$
select 'EXT'::text as seltype,contact_id,contact_firstname,contact_lastname,contact_email,expires from contact c
WHERE contact_id in 
(select (unnest(string_to_array($1::text,'|')::int[])));
$_$;


ALTER FUNCTION public.contactall_from_ids(con_ids text) OWNER TO postgres;

--
-- TOC entry 492 (class 1255 OID 69625)
-- Name: contacts_email_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION contacts_email_from_ids(con_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select contact_email as email from contact c
WHERE contact_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),', ','*') as contactss;
$_$;


ALTER FUNCTION public.contacts_email_from_ids(con_ids text) OWNER TO postgres;

--
-- TOC entry 493 (class 1255 OID 69626)
-- Name: contacts_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION contacts_from_ids(con_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select contact_firstname|| ' ' || contact_lastname as name from contact c
WHERE contact_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),', ','*') as contactss;
$_$;


ALTER FUNCTION public.contacts_from_ids(con_ids text) OWNER TO postgres;

--
-- TOC entry 494 (class 1255 OID 69627)
-- Name: contractor_email_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION contractor_email_from_ids(con_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select a.email from address a
LEFT join address_to_relation ar using (address_id)
WHERE ar.contractor_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),', ','*') as emails;
$_$;


ALTER FUNCTION public.contractor_email_from_ids(con_ids text) OWNER TO postgres;

--
-- TOC entry 495 (class 1255 OID 69628)
-- Name: contractorall_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION contractorall_from_ids(con_ids text) RETURNS TABLE(seltype text, contact_id integer, first_name text, last_name text, email text, expires date)
    LANGUAGE sql COST 2 ROWS 5
    AS $_$
select 'CON'::text as seltype,contractor_id,con_name,'CONTRACTOR'::text,a.email,'31-12-3000'::date
 from contractor c
 LEFT JOIN address_to_relation  using(contractor_id)
 LEFT JOIN address a using (address_id) 
WHERE contractor_id in 
(select (unnest(string_to_array($1::text,'|')::int[])));
$_$;


ALTER FUNCTION public.contractorall_from_ids(con_ids text) OWNER TO postgres;

--
-- TOC entry 496 (class 1255 OID 69629)
-- Name: contractors_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION contractors_from_ids(con_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select con_name from contractor c
WHERE contractor_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),', ','*') as contractors;
$_$;


ALTER FUNCTION public.contractors_from_ids(con_ids text) OWNER TO postgres;

--
-- TOC entry 497 (class 1255 OID 69630)
-- Name: coos_from_well(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION coos_from_well(well text, cooline text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(
(select calloff_order_id from calloff_order 
where calloff_order_id in 
	(select (unnest(string_to_array($2::text,'|')::int[]))) 
	and well_ids like '%'||$1||'%' )),',','*') as coos
$_$;


ALTER FUNCTION public.coos_from_well(well text, cooline text) OWNER TO postgres;

--
-- TOC entry 498 (class 1255 OID 69631)
-- Name: email_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION email_from_ids(emp_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select a.email from address a
LEFT join address_to_relation ar using (address_id)
WHERE ar.employee_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),', ','*') as emails;
$_$;


ALTER FUNCTION public.email_from_ids(emp_ids text) OWNER TO postgres;

--
-- TOC entry 499 (class 1255 OID 69632)
-- Name: employeeall_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION employeeall_from_ids(emp_ids text) RETURNS TABLE(seltype text, contact_id integer, first_name text, last_name text, email text, expires date)
    LANGUAGE sql COST 3 ROWS 5
    AS $_$
select 'EMP'::text as seltype,employee_id,firstname,lastname,a.email,'31-12-3000'::date
 from employee e
 LEFT JOIN address_to_relation  using(employee_id)
 LEFT JOIN address a using (address_id) 
WHERE employee_id in 
(select (unnest(string_to_array($1::text,'|')::int[])));
$_$;


ALTER FUNCTION public.employeeall_from_ids(emp_ids text) OWNER TO postgres;

--
-- TOC entry 500 (class 1255 OID 69633)
-- Name: employees_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION employees_from_ids(emp_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select firstname|| ' ' ||lastname as name from employee e
WHERE employee_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),', ','*') as employees;
$_$;


ALTER FUNCTION public.employees_from_ids(emp_ids text) OWNER TO postgres;

--
-- TOC entry 501 (class 1255 OID 69634)
-- Name: insnumbers(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION insnumbers(wellid integer) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
select array_to_string(array(select instruction_no from site_instruction where well_id = $1  order by instruction_no),', ','*') as nums;
$_$;


ALTER FUNCTION public.insnumbers(wellid integer) OWNER TO postgres;

--
-- TOC entry 502 (class 1255 OID 69635)
-- Name: locs_from_land(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION locs_from_land(lowner_id integer) RETURNS text
    LANGUAGE sql COST 1
    AS $_$
 select  array_to_string(array(select location from gravel_water_loc where landowner_id = $1),', ','*')
$_$;


ALTER FUNCTION public.locs_from_land(lowner_id integer) OWNER TO postgres;

--
-- TOC entry 503 (class 1255 OID 69636)
-- Name: plant_rates(integer, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION plant_rates(plant_id integer, dat date) RETURNS TABLE(stand_rate double precision, plant_rate double precision)
    LANGUAGE sql
    AS $_$
with data as
(SELECT pt.plant_rate as rate,pt.stand_rate from plant_type pt LEFT JOIN plant p using(plant_type_id)  where p.plant_id = $1 
UNION
SELECT pt.new_rate as rate,pt.stand_new_rate as stand_rate from plant_type pt LEFT JOIN plant p using (plant_type_id)  where p.plant_id = $1 and pt.effective_from <= $2
)
select max(stand_rate),max(rate) from data;
$_$;


ALTER FUNCTION public.plant_rates(plant_id integer, dat date) OWNER TO postgres;

--
-- TOC entry 504 (class 1255 OID 69637)
-- Name: plant_rates(text, date); Type: FUNCTION; Schema: public; Owner: fil
--

CREATE FUNCTION plant_rates(query text, dat date) RETURNS TABLE(plant_id integer, plant_unit text, plant_name text, stand_rate double precision, plant_rate double precision)
    LANGUAGE sql
    AS $_$
with data as
(SELECT p.plant_id,p.plant_unit,p.plant_name,pt.plant_rate as rate,pt.stand_rate from plant p LEFT JOIN plant_type pt using (plant_type_id) 
 where upper(p.plant_unit) like $1||'%' and p.removed is false 
UNION
SELECT p.plant_id,p.plant_unit,p.plant_name,pt.new_rate as rate,pt.stand_new_rate as stand_rate from plant p LEFT JOIN plant_type pt using (plant_type_id)
 where upper(p.plant_unit) like $1||'%'
 and p.removed is false and pt.effective_from <= $2
order by plant_unit)
select plant_id,plant_unit,plant_name,max(stand_rate),max(rate) from data group by plant_id,plant_unit,plant_name;
$_$;


ALTER FUNCTION public.plant_rates(query text, dat date) OWNER TO fil;

--
-- TOC entry 505 (class 1255 OID 69638)
-- Name: process_calloff_audit(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_calloff_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        --
        -- Create a row in calloff_order_audit to reflect the operation performed on calloff_order
        -- make use of the special variable TG_OP to work out the operation.
        --
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO calloff_order_audit SELECT  OLD.*,now(),'DEL';
            RETURN OLD;
       
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO calloff_order_audit SELECT  OLD.*,now(),'UPD';
            RETURN NEW;
         END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$$;


ALTER FUNCTION public.process_calloff_audit() OWNER TO postgres;

--
-- TOC entry 506 (class 1255 OID 69639)
-- Name: process_plant_type_audit(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION process_plant_type_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        --
        -- Create a row in plant_type_audit to reflect the operation performed on plant_type,
        -- make use of the special variable TG_OP to work out the operation.
        --
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO plant_type_audit SELECT  OLD.*,now(),'DEL';
            RETURN OLD;
       
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO plant_type_audit SELECT  OLD.*,now(),'UPD';
            RETURN NEW;
         END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$$;


ALTER FUNCTION public.process_plant_type_audit() OWNER TO postgres;

--
-- TOC entry 507 (class 1255 OID 69640)
-- Name: sent_atw_emails(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION sent_atw_emails(atwid integer, last_email integer) RETURNS text
    LANGUAGE sql COST 3
    AS $_$
select array_to_string(array(select sent_company_emails from email_result er
LEFT JOIN email_log el using(email_log_id)
where el.receiver_type = 'ATW'
and el.dkt_id = $1
and el.email_log_id != $2),'|','*')

$_$;


ALTER FUNCTION public.sent_atw_emails(atwid integer, last_email integer) OWNER TO postgres;

--
-- TOC entry 508 (class 1255 OID 69641)
-- Name: subscopes(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION subscopes(crewid integer) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select sub_crew_name from crew c LEFT JOIN sub_crew sc using(crew_id) WHERE c.crew_id = $1 order by sub_crew_name ),', ','*'); 
$_$;


ALTER FUNCTION public.subscopes(crewid integer) OWNER TO postgres;

--
-- TOC entry 509 (class 1255 OID 69642)
-- Name: subscopes_from_ids(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION subscopes_from_ids(coo_id integer) RETURNS TABLE(sub_crew_id integer, sub_crew_name text)
    LANGUAGE sql COST 2 ROWS 1
    AS $_$
select sub_crew_id, sub_crew_name from sub_crew sc
WHERE sub_crew_id in 
(select (unnest(string_to_array(sub_crew_ids,'|')::int[])) from calloff_order where calloff_order_id = $1)
order by sub_crew_name;
$_$;


ALTER FUNCTION public.subscopes_from_ids(coo_id integer) OWNER TO postgres;

--
-- TOC entry 510 (class 1255 OID 69643)
-- Name: subscopes_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION subscopes_from_ids(sub_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select sub_crew_name from sub_crew
WHERE sub_crew_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),'; ','*') as names;
$_$;


ALTER FUNCTION public.subscopes_from_ids(sub_ids text) OWNER TO postgres;

--
-- TOC entry 511 (class 1255 OID 69644)
-- Name: subscopes_from_ids(text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION subscopes_from_ids(sub_ids text, ids_string boolean) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select sub_crew_name from sub_crew sc
WHERE sub_crew_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),'; ','*' ) as subs
$_$;


ALTER FUNCTION public.subscopes_from_ids(sub_ids text, ids_string boolean) OWNER TO postgres;

--
-- TOC entry 512 (class 1255 OID 69645)
-- Name: supervisors_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION supervisors_from_ids(sup_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
select array_to_string(array(select firstname|| ' '||lastname from employee
WHERE employee_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))),'; ','*') as names;
$_$;


ALTER FUNCTION public.supervisors_from_ids(sup_ids text) OWNER TO postgres;

--
-- TOC entry 513 (class 1255 OID 69646)
-- Name: unique_coo(integer, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION unique_coo(crew_id integer, area_id integer, wellids text) RETURNS boolean
    LANGUAGE sql COST 1
    AS $_$
select true from calloff_order co  where area_id = $2 and crew_id = $1  and string_to_array(co.well_ids::text,'|')::int[]  && (select string_to_array($3::text,'|')::int[])
$_$;


ALTER FUNCTION public.unique_coo(crew_id integer, area_id integer, wellids text) OWNER TO postgres;

--
-- TOC entry 514 (class 1255 OID 69647)
-- Name: unique_coo(integer, integer, integer, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION unique_coo(crew_id integer, sub_crewid integer, area_id integer, contractor_id integer, wellids text) RETURNS TABLE(any_at_all smallint, any_not_closed smallint)
    LANGUAGE plpgsql COST 1 ROWS 1
    AS $_$
DECLARE
  any_at smallint;
  not_closed smallint;
 BEGIN
 
select 1 from calloff_order co  where co.contractor_id = $4 and co.area_id = $3 and co.crew_id = $1 and sub_crew_id = $2 and string_to_array(co.well_ids::text,'|')::int[]  && (select string_to_array($5::text,'|')::int[]) into any_at;
select 1 from calloff_order co  where co.contractor_id = $4 and co.area_id = $3 and co.crew_id = $1 and sub_crew_id = $2 and string_to_array(co.well_ids::text,'|')::int[]  && (select string_to_array($5::text,'|')::int[]) 
and completion_cert_id is null into not_closed;

return query select coalesce(any_at,0::smallint),coalesce(not_closed,0::smallint);

 END;
$_$;


ALTER FUNCTION public.unique_coo(crew_id integer, sub_crewid integer, area_id integer, contractor_id integer, wellids text) OWNER TO postgres;

--
-- TOC entry 515 (class 1255 OID 69648)
-- Name: unique_coo(integer, integer, integer, integer, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION unique_coo(crew_id integer, sub_crew_id integer, area_id integer, contractor_id integer, wellids text, cooid integer) RETURNS boolean
    LANGUAGE sql COST 1
    AS $_$
select true from calloff_order co  where co.calloff_order_id != $6 and contractor_id = $4 and area_id = $3 and crew_id = $1 and sub_crew_id = $2 and string_to_array(co.well_ids::text,'|')::int[]  && (select string_to_array($5::text,'|')::int[])
$_$;


ALTER FUNCTION public.unique_coo(crew_id integer, sub_crew_id integer, area_id integer, contractor_id integer, wellids text, cooid integer) OWNER TO postgres;

--
-- TOC entry 516 (class 1255 OID 69649)
-- Name: wells(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION wells(areaid integer) RETURNS text
    LANGUAGE sql COST 1
    AS $_$
select array_to_string(array(select well_name from well where area_id = $1 and removed is false order by well_name),', ','*') as wells;
$_$;


ALTER FUNCTION public.wells(areaid integer) OWNER TO postgres;

--
-- TOC entry 517 (class 1255 OID 69650)
-- Name: wells_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION wells_from_ids(well_ids text) RETURNS text
    LANGUAGE sql COST 1
    AS $_$
select array_to_string(array(select well_name from well w
WHERE well_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))order by well_name),', ','*') as wells;
$_$;


ALTER FUNCTION public.wells_from_ids(well_ids text) OWNER TO postgres;

--
-- TOC entry 518 (class 1255 OID 69651)
-- Name: wells_from_ids(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION wells_from_ids(well_ids text, diff text) RETURNS text
    LANGUAGE sql COST 4
    AS $_$
   select array_to_string(array(select well_name from well where well_id = ANY ($1::integer[])),', ','*')
$_$;


ALTER FUNCTION public.wells_from_ids(well_ids text, diff text) OWNER TO postgres;

--
-- TOC entry 519 (class 1255 OID 69652)
-- Name: wells_name_order_from_ids(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION wells_name_order_from_ids(well_ids text) RETURNS text
    LANGUAGE sql COST 2
    AS $_$
with tn as (select well_name,well_id from well w
WHERE well_id in 
(select (unnest(string_to_array($1::text,'|')::int[])))order by well_name)
select array_to_string(array(select well_id from tn order by well_name),'|','*') as wellids ;
$_$;


ALTER FUNCTION public.wells_name_order_from_ids(well_ids text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 183 (class 1259 OID 69653)
-- Name: address; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE address (
    address_id integer NOT NULL,
    address_line_1 character varying(128),
    address_line_2 character varying(128),
    suburb character varying(32),
    city character varying(32),
    telephone character varying(16),
    mobile character varying(16),
    fax character varying(16),
    postcode character varying(4),
    email character varying(128),
    state_id integer
);


ALTER TABLE address OWNER TO fil;

--
-- TOC entry 184 (class 1259 OID 69659)
-- Name: address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE address_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_address_id_seq OWNER TO fil;

--
-- TOC entry 3554 (class 0 OID 0)
-- Dependencies: 184
-- Name: address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE address_address_id_seq OWNED BY address.address_id;


--
-- TOC entry 185 (class 1259 OID 69661)
-- Name: address_to_relation; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE address_to_relation (
    employee_id integer,
    company_id integer,
    client_id integer,
    address_type character(3),
    address_id integer,
    contractor_id integer,
    destination_id integer,
    address_to_relation_id integer NOT NULL
);


ALTER TABLE address_to_relation OWNER TO fil;

--
-- TOC entry 186 (class 1259 OID 69664)
-- Name: address_to_relation_address_to_relation_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE address_to_relation_address_to_relation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_to_relation_address_to_relation_id_seq OWNER TO fil;

--
-- TOC entry 3555 (class 0 OID 0)
-- Dependencies: 186
-- Name: address_to_relation_address_to_relation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE address_to_relation_address_to_relation_id_seq OWNED BY address_to_relation.address_to_relation_id;


--
-- TOC entry 187 (class 1259 OID 69666)
-- Name: admin_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin_menu (
    admin_menu_id integer NOT NULL,
    level smallint,
    menu_text character varying(32),
    href character varying(64),
    parent_menu_id integer,
    menu_order smallint,
    min_profile_id smallint,
    company_id integer
);


ALTER TABLE admin_menu OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 69669)
-- Name: admin_menu_admin_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_menu_admin_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_menu_admin_menu_id_seq OWNER TO postgres;

--
-- TOC entry 3556 (class 0 OID 0)
-- Dependencies: 188
-- Name: admin_menu_admin_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_menu_admin_menu_id_seq OWNED BY admin_menu.admin_menu_id;


--
-- TOC entry 189 (class 1259 OID 69671)
-- Name: adodb_logsql; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE adodb_logsql (
    created timestamp without time zone NOT NULL,
    sql0 character varying(250) NOT NULL,
    sql1 text NOT NULL,
    params text NOT NULL,
    tracer text NOT NULL,
    timer numeric(16,6) NOT NULL
);


ALTER TABLE adodb_logsql OWNER TO fil;

--
-- TOC entry 190 (class 1259 OID 69677)
-- Name: amcor_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=amcor'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE amcor_completion_cert OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 69682)
-- Name: amcor_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=amcor'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE amcor_docket_day OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 69686)
-- Name: amcor_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=amcor'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE amcor_employee OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 69690)
-- Name: amcor_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=amcor'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE amcor_field_estimate OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 69694)
-- Name: amcor_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=amcor'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE amcor_field_estimate_line OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 69698)
-- Name: amcor_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=amcor'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE amcor_gravel_water OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 69703)
-- Name: amcor_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=amcor'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE amcor_hour OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 69708)
-- Name: amcor_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=amcor'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE amcor_hour_audit OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 69713)
-- Name: amcor_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=amcor'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE amcor_plant OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 69717)
-- Name: amcor_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=amcor'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE amcor_site_instruction OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 69721)
-- Name: amcor_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=amcor'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE amcor_system OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 69725)
-- Name: amcor_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW amcor_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=amcor'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE amcor_upload OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 69729)
-- Name: area; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE area (
    area_id integer NOT NULL,
    area_name character varying(32),
    removed boolean DEFAULT false,
    contractor_id integer DEFAULT 0
);


ALTER TABLE area OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 69734)
-- Name: area_area_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE area_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE area_area_id_seq OWNER TO postgres;

--
-- TOC entry 3557 (class 0 OID 0)
-- Dependencies: 203
-- Name: area_area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE area_area_id_seq OWNED BY area.area_id;


--
-- TOC entry 204 (class 1259 OID 69736)
-- Name: atw; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE atw (
    atw_id integer NOT NULL,
    atw_number character varying(32),
    property_address character varying(256),
    site_contact character varying(64),
    landowner character varying(64),
    employee_id integer,
    create_date date DEFAULT ('now'::text)::date,
    contractor_ids character varying(64),
    employee_ids character varying(64),
    contact_ids character varying(64),
    expire_date date,
    removed boolean DEFAULT false,
    email_sent boolean DEFAULT false,
    area_id integer,
    well_ids character varying(256),
    meeting_time timestamp with time zone,
    site_contact_fone character varying(64),
    notes text,
    to_email_contacts character varying(256),
    to_email_employees character varying(256),
    to_email_contractors character varying(64)
);


ALTER TABLE atw OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 69745)
-- Name: atw_atw_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE atw_atw_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE atw_atw_id_seq OWNER TO postgres;

--
-- TOC entry 3558 (class 0 OID 0)
-- Dependencies: 205
-- Name: atw_atw_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE atw_atw_id_seq OWNED BY atw.atw_id;


--
-- TOC entry 206 (class 1259 OID 69747)
-- Name: atw_document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE atw_document (
    atw_document_id integer NOT NULL,
    atw_id integer,
    atw_type_id integer,
    document_type character varying(64),
    document_name character varying(128),
    status smallint,
    upload_emp_id integer,
    document_date date,
    expire_date date
);


ALTER TABLE atw_document OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 69750)
-- Name: atw_document_atw_document_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE atw_document_atw_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE atw_document_atw_document_id_seq OWNER TO postgres;

--
-- TOC entry 3559 (class 0 OID 0)
-- Dependencies: 207
-- Name: atw_document_atw_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE atw_document_atw_document_id_seq OWNED BY atw_document.atw_document_id;


--
-- TOC entry 208 (class 1259 OID 69752)
-- Name: atw_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE atw_type (
    atw_type_id integer NOT NULL,
    type_name character varying(32)
);


ALTER TABLE atw_type OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 69755)
-- Name: atw_type_atw_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE atw_type_atw_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE atw_type_atw_type_id_seq OWNER TO postgres;

--
-- TOC entry 3560 (class 0 OID 0)
-- Dependencies: 209
-- Name: atw_type_atw_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE atw_type_atw_type_id_seq OWNED BY atw_type.atw_type_id;


--
-- TOC entry 210 (class 1259 OID 69757)
-- Name: batch; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE batch (
    batch_id integer NOT NULL,
    contractor_id integer,
    calloff_order_id integer,
    table_id integer,
    filename character varying(128),
    zipdir character varying(128),
    batch_type character varying(16),
    area_name character varying(64),
    well_name character varying(64),
    create_date date DEFAULT ('now'::text)::date
);


ALTER TABLE batch OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 69761)
-- Name: batch_batch_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE batch_batch_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE batch_batch_id_seq OWNER TO postgres;

--
-- TOC entry 3561 (class 0 OID 0)
-- Dependencies: 211
-- Name: batch_batch_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE batch_batch_id_seq OWNED BY batch.batch_id;


--
-- TOC entry 212 (class 1259 OID 69763)
-- Name: bruhl_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=bruhl'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE bruhl_completion_cert OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 69768)
-- Name: bruhl_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=bruhl'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE bruhl_docket_day OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 69772)
-- Name: bruhl_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=bruhl'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE bruhl_employee OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 69776)
-- Name: bruhl_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=bruhl'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE bruhl_field_estimate OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 69780)
-- Name: bruhl_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=bruhl'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE bruhl_field_estimate_line OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 69784)
-- Name: bruhl_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=bruhl'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE bruhl_gravel_water OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 69789)
-- Name: bruhl_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=bruhl'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE bruhl_hour OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 69794)
-- Name: bruhl_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=bruhl'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE bruhl_hour_audit OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 69799)
-- Name: bruhl_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=bruhl'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE bruhl_plant OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 69803)
-- Name: bruhl_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=bruhl'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE bruhl_site_instruction OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 69807)
-- Name: bruhl_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=bruhl'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE bruhl_system OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 69811)
-- Name: bruhl_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW bruhl_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=bruhl'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE bruhl_upload OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 69815)
-- Name: budget; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE budget (
    budget_id integer NOT NULL,
    budget_name character varying(64),
    removed boolean DEFAULT false
);


ALTER TABLE budget OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 69819)
-- Name: budget_budget_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE budget_budget_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE budget_budget_id_seq OWNER TO postgres;

--
-- TOC entry 3562 (class 0 OID 0)
-- Dependencies: 225
-- Name: budget_budget_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE budget_budget_id_seq OWNED BY budget.budget_id;


--
-- TOC entry 226 (class 1259 OID 69821)
-- Name: calloff_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE calloff_order (
    calloff_order_id integer NOT NULL,
    request_estimate_id integer,
    field_estimate_id integer,
    contractor_id integer,
    area_id integer,
    crew_id integer,
    contract_num character varying(32),
    commencement_date date,
    cost_estimate double precision,
    services character varying(1024),
    remarks character varying(1024),
    qgc_signee character varying(64),
    creation_date date DEFAULT ('now'::text)::date,
    created_by integer,
    removed boolean DEFAULT false,
    well_ids character varying(256),
    qgc_sig text,
    qgc_approve_date date,
    landowner_id_1 integer,
    gravel_loc_id_1 integer,
    landowner_id_2 integer,
    water_loc_id_2 integer,
    gravel_return smallint,
    water_return smallint,
    email_emp_ids character varying(128),
    completion_cert_id integer,
    sub_crew_ids character varying(64),
    est_compl_date date,
    budget_id integer
);


ALTER TABLE calloff_order OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 69829)
-- Name: calloff_order_audit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE calloff_order_audit (
    calloff_order_id integer,
    request_estimate_id integer,
    field_estimate_id integer,
    contractor_id integer,
    area_id integer,
    crew_id integer,
    contract_num character varying(32),
    commencement_date date,
    cost_estimate double precision,
    services character varying(1024),
    remarks character varying(1024),
    qgc_signee character varying(64),
    creation_date date,
    created_by integer,
    removed boolean,
    well_ids character varying(256),
    qgc_sig text,
    qgc_approve_date date,
    landowner_id_1 integer,
    gravel_loc_id_1 integer,
    landowner_id_2 integer,
    water_loc_id_2 integer,
    gravel_return smallint,
    water_return smallint,
    email_emp_ids character varying(128),
    completion_cert_id integer,
    sub_crew_ids character varying(64),
    est_compl_date date,
    budget_id integer,
    audit_time timestamp with time zone,
    del_update character(4)
);


ALTER TABLE calloff_order_audit OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 69835)
-- Name: calloff_order_calloff_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE calloff_order_calloff_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE calloff_order_calloff_order_id_seq OWNER TO postgres;

--
-- TOC entry 3563 (class 0 OID 0)
-- Dependencies: 228
-- Name: calloff_order_calloff_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE calloff_order_calloff_order_id_seq OWNED BY calloff_order.calloff_order_id;


--
-- TOC entry 229 (class 1259 OID 69837)
-- Name: certificate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE certificate (
    certificate_id integer NOT NULL,
    certificate_type character varying(32),
    certificate_desc character varying(64),
    removed boolean DEFAULT false
);


ALTER TABLE certificate OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 69841)
-- Name: certificate_certificate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE certificate_certificate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE certificate_certificate_id_seq OWNER TO postgres;

--
-- TOC entry 3564 (class 0 OID 0)
-- Dependencies: 230
-- Name: certificate_certificate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE certificate_certificate_id_seq OWNED BY certificate.certificate_id;


--
-- TOC entry 231 (class 1259 OID 69843)
-- Name: clein_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=clein'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE clein_completion_cert OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 69848)
-- Name: clein_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=clein'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE clein_docket_day OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 69852)
-- Name: clein_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=clein'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE clein_employee OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 69856)
-- Name: clein_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=clein'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE clein_field_estimate OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 69860)
-- Name: clein_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=clein'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE clein_field_estimate_line OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 69864)
-- Name: clein_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=clein'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE clein_gravel_water OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 69869)
-- Name: clein_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=clein'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE clein_hour OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 69874)
-- Name: clein_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=clein'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE clein_hour_audit OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 69879)
-- Name: clein_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=clein'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE clein_plant OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 69883)
-- Name: clein_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=clein'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE clein_site_instruction OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 69887)
-- Name: clein_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=clein'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE clein_system OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 69891)
-- Name: clein_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW clein_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=clein'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE clein_upload OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 69895)
-- Name: client; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE client (
    client_id integer NOT NULL,
    client_name character varying(128),
    abn character varying(16),
    removed boolean DEFAULT false
);


ALTER TABLE client OWNER TO fil;

--
-- TOC entry 244 (class 1259 OID 69899)
-- Name: client_client_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE client_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_client_id_seq OWNER TO fil;

--
-- TOC entry 3565 (class 0 OID 0)
-- Dependencies: 244
-- Name: client_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE client_client_id_seq OWNED BY client.client_id;


--
-- TOC entry 245 (class 1259 OID 69901)
-- Name: company; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE company (
    company_id integer NOT NULL,
    company_name character varying(128),
    company_logo character varying(128),
    abn character varying(16),
    company_splash character varying(64),
    contact_firstname character varying(32),
    contact_lastname character varying(32),
    contact_email character varying(64),
    sub_domain character varying(64)
);


ALTER TABLE company OWNER TO fil;

--
-- TOC entry 246 (class 1259 OID 69907)
-- Name: company_company_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE company_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE company_company_id_seq OWNER TO fil;

--
-- TOC entry 3566 (class 0 OID 0)
-- Dependencies: 246
-- Name: company_company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE company_company_id_seq OWNED BY company.company_id;


--
-- TOC entry 247 (class 1259 OID 69909)
-- Name: contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE contact (
    contact_id integer NOT NULL,
    atw_id integer,
    employee_id integer,
    contact_firstname character varying(32),
    contact_lastname character varying(32),
    contact_email character varying(64),
    contact_fone character varying(16),
    removed boolean DEFAULT false,
    in_mail_list boolean DEFAULT true,
    expires date
);


ALTER TABLE contact OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 69914)
-- Name: contact_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contact_contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contact_contact_id_seq OWNER TO postgres;

--
-- TOC entry 3567 (class 0 OID 0)
-- Dependencies: 248
-- Name: contact_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contact_contact_id_seq OWNED BY contact.contact_id;


--
-- TOC entry 249 (class 1259 OID 69916)
-- Name: contractor; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE contractor (
    contractor_id integer NOT NULL,
    username character varying(64),
    password character varying(64),
    name character varying(32),
    last_login timestamp without time zone,
    removed boolean,
    profile_id smallint,
    db_link character varying(16),
    db character varying(16),
    domain character varying(64),
    con_name character varying(32),
    expense_percentage double precision,
    employee_id integer,
    contract_no character varying(64),
    partial_complete boolean DEFAULT false
);


ALTER TABLE contractor OWNER TO fil;

--
-- TOC entry 250 (class 1259 OID 69920)
-- Name: contractor_contractor_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE contractor_contractor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contractor_contractor_id_seq OWNER TO fil;

--
-- TOC entry 3568 (class 0 OID 0)
-- Dependencies: 250
-- Name: contractor_contractor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE contractor_contractor_id_seq OWNED BY contractor.contractor_id;


--
-- TOC entry 251 (class 1259 OID 69922)
-- Name: corbets_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=corbets'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE corbets_completion_cert OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 69927)
-- Name: corbets_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=corbets'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE corbets_docket_day OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 69931)
-- Name: corbets_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=corbets'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE corbets_employee OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 69935)
-- Name: corbets_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=corbets'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE corbets_field_estimate OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 69940)
-- Name: corbets_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=corbets'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE corbets_field_estimate_line OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 69944)
-- Name: corbets_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=corbets'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE corbets_gravel_water OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 69949)
-- Name: corbets_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=corbets'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE corbets_hour OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 69954)
-- Name: corbets_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=corbets'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE corbets_hour_audit OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 69959)
-- Name: corbets_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=corbets'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE corbets_plant OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 69963)
-- Name: corbets_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=corbets'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE corbets_site_instruction OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 69967)
-- Name: corbets_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=corbets'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE corbets_system OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 69971)
-- Name: corbets_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW corbets_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=corbets'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE corbets_upload OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 69975)
-- Name: crew; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE crew (
    crew_id integer NOT NULL,
    crew_name character varying(64),
    removed boolean DEFAULT false,
    contractor_id integer DEFAULT 0
);


ALTER TABLE crew OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 69980)
-- Name: crew_crew_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE crew_crew_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE crew_crew_id_seq OWNER TO postgres;

--
-- TOC entry 3569 (class 0 OID 0)
-- Dependencies: 264
-- Name: crew_crew_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE crew_crew_id_seq OWNED BY crew.crew_id;


--
-- TOC entry 265 (class 1259 OID 69982)
-- Name: email_attachment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email_attachment (
    email_attachment_id integer NOT NULL,
    email_log_id integer,
    attachment_name character varying(1024),
    recipient_type character varying(8),
    email character varying(1024),
    attach_date date DEFAULT ('now'::text)::date,
    email_sent boolean DEFAULT false
);


ALTER TABLE email_attachment OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 69990)
-- Name: email_attachment_email_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_attachment_email_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_attachment_email_attachment_id_seq OWNER TO postgres;

--
-- TOC entry 3570 (class 0 OID 0)
-- Dependencies: 266
-- Name: email_attachment_email_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_attachment_email_attachment_id_seq OWNED BY email_attachment.email_attachment_id;


--
-- TOC entry 267 (class 1259 OID 69992)
-- Name: email_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email_log (
    email_log_id integer NOT NULL,
    employee_id integer,
    company_id integer,
    receiver_id integer,
    receiver_type character varying(3),
    subject character varying(512),
    dkt_id integer,
    fault character varying(256),
    dkt_date date,
    coos character varying(64),
    supervisor_group_id integer
);


ALTER TABLE email_log OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 69998)
-- Name: email_log_email_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_log_email_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_log_email_log_id_seq OWNER TO postgres;

--
-- TOC entry 3571 (class 0 OID 0)
-- Dependencies: 268
-- Name: email_log_email_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_log_email_log_id_seq OWNED BY email_log.email_log_id;


--
-- TOC entry 269 (class 1259 OID 70000)
-- Name: email_recipient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email_recipient (
    email_recipient_id integer NOT NULL,
    recipient_type character varying(8),
    form_type character varying(4) NOT NULL,
    emails character varying(512)
);


ALTER TABLE email_recipient OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 70006)
-- Name: email_recipient_email_recipient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_recipient_email_recipient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_recipient_email_recipient_id_seq OWNER TO postgres;

--
-- TOC entry 3572 (class 0 OID 0)
-- Dependencies: 270
-- Name: email_recipient_email_recipient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_recipient_email_recipient_id_seq OWNED BY email_recipient.email_recipient_id;


--
-- TOC entry 271 (class 1259 OID 70008)
-- Name: email_result; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email_result (
    email_result_id integer NOT NULL,
    email_log_id integer,
    sent_receiver_emails character varying(512),
    sent_company_emails character varying(512),
    receiver_message_id character varying(64),
    company_message_id character varying(64),
    attachments character varying(128),
    receiver_fault character varying(512),
    company_fault character varying(512),
    send_time timestamp with time zone DEFAULT now()
);


ALTER TABLE email_result OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 70015)
-- Name: email_result_email_result_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_result_email_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_result_email_result_id_seq OWNER TO postgres;

--
-- TOC entry 3573 (class 0 OID 0)
-- Dependencies: 272
-- Name: email_result_email_result_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_result_email_result_id_seq OWNED BY email_result.email_result_id;


--
-- TOC entry 273 (class 1259 OID 70017)
-- Name: email_rule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email_rule (
    receiver_type character varying(3) NOT NULL,
    email_rule_include character varying(128),
    full_description character varying(64)
);


ALTER TABLE email_rule OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 70020)
-- Name: email_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email_type (
    receiver_type character varying(3) NOT NULL,
    description character varying(32)
);


ALTER TABLE email_type OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 70023)
-- Name: employee; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE employee (
    employee_id integer NOT NULL,
    username character varying(64),
    password character varying(64),
    firstname character varying(32),
    lastname character varying(32),
    last_login timestamp without time zone,
    removed boolean,
    profile_id smallint,
    next_of_kin_name character varying(64),
    next_of_kin_fone character varying(12),
    company_id integer,
    signature text,
    locked boolean DEFAULT false,
    emp_type character varying(4),
    session_id character varying(32)
);


ALTER TABLE employee OWNER TO fil;

--
-- TOC entry 276 (class 1259 OID 70030)
-- Name: employee_certificate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE employee_certificate (
    employee_id integer,
    contractor_id integer,
    certificate_id integer,
    certificate character varying(64),
    expiry_date date,
    emp_type character(3),
    removed boolean DEFAULT false
);


ALTER TABLE employee_certificate OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 70034)
-- Name: employee_employee_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE employee_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE employee_employee_id_seq OWNER TO fil;

--
-- TOC entry 3574 (class 0 OID 0)
-- Dependencies: 277
-- Name: employee_employee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE employee_employee_id_seq OWNED BY employee.employee_id;


--
-- TOC entry 278 (class 1259 OID 70036)
-- Name: expense_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE expense_type (
    expense_type_id integer NOT NULL,
    type_expense character varying(64),
    contractor_id integer DEFAULT 0,
    removed boolean DEFAULT false
);


ALTER TABLE expense_type OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 70041)
-- Name: expense_type_expense_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE expense_type_expense_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE expense_type_expense_type_id_seq OWNER TO postgres;

--
-- TOC entry 3575 (class 0 OID 0)
-- Dependencies: 279
-- Name: expense_type_expense_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE expense_type_expense_type_id_seq OWNED BY expense_type.expense_type_id;


--
-- TOC entry 280 (class 1259 OID 70043)
-- Name: ext_contractor; Type: TABLE; Schema: public; Owner: fil
--

CREATE TABLE ext_contractor (
    ext_contractor_id integer NOT NULL,
    con_name character varying(32),
    expense_percentage double precision,
    crew_id integer
);


ALTER TABLE ext_contractor OWNER TO fil;

--
-- TOC entry 281 (class 1259 OID 70046)
-- Name: ext_contractor_ext_contractor_id_seq; Type: SEQUENCE; Schema: public; Owner: fil
--

CREATE SEQUENCE ext_contractor_ext_contractor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ext_contractor_ext_contractor_id_seq OWNER TO fil;

--
-- TOC entry 3576 (class 0 OID 0)
-- Dependencies: 281
-- Name: ext_contractor_ext_contractor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fil
--

ALTER SEQUENCE ext_contractor_ext_contractor_id_seq OWNED BY ext_contractor.ext_contractor_id;


--
-- TOC entry 282 (class 1259 OID 70048)
-- Name: gravel_water; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gravel_water (
    gravel_water_id integer NOT NULL,
    crew_id integer,
    crew_name character varying(512),
    total double precision,
    volume double precision,
    end_date date,
    sub_crew_id integer,
    site_instruction_id integer,
    completion_cert_id integer
);


ALTER TABLE gravel_water OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 70054)
-- Name: gravel_water_gravel_water_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gravel_water_gravel_water_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gravel_water_gravel_water_id_seq OWNER TO postgres;

--
-- TOC entry 3577 (class 0 OID 0)
-- Dependencies: 283
-- Name: gravel_water_gravel_water_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gravel_water_gravel_water_id_seq OWNED BY gravel_water.gravel_water_id;


--
-- TOC entry 284 (class 1259 OID 70056)
-- Name: gravel_water_loc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gravel_water_loc (
    gravel_water_loc_id integer NOT NULL,
    landowner_id integer,
    loc_type smallint,
    location character varying(128),
    pushed_up_rate double precision DEFAULT '-1'::integer,
    non_pushed_up_rate double precision DEFAULT '-1'::integer,
    water_rate double precision DEFAULT '-1'::integer,
    removed boolean DEFAULT false
);


ALTER TABLE gravel_water_loc OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 70063)
-- Name: gravel_water_loc_gravel_water_loc_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gravel_water_loc_gravel_water_loc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gravel_water_loc_gravel_water_loc_id_seq OWNER TO postgres;

--
-- TOC entry 3578 (class 0 OID 0)
-- Dependencies: 285
-- Name: gravel_water_loc_gravel_water_loc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gravel_water_loc_gravel_water_loc_id_seq OWNED BY gravel_water_loc.gravel_water_loc_id;


--
-- TOC entry 286 (class 1259 OID 70065)
-- Name: hem_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=hem'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE hem_completion_cert OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 70070)
-- Name: hem_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=hem'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE hem_docket_day OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 70074)
-- Name: hem_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=hem'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE hem_employee OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 70078)
-- Name: hem_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=hem'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE hem_field_estimate OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 70082)
-- Name: hem_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=hem'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE hem_field_estimate_line OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 70086)
-- Name: hem_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=hem'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE hem_gravel_water OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 70091)
-- Name: hem_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=hem'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE hem_hour OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 70096)
-- Name: hem_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=hem'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE hem_hour_audit OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 70101)
-- Name: hem_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=hem'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE hem_plant OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 70105)
-- Name: hem_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_id
   FROM dblink('dbname=hem'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_id integer);


ALTER TABLE hem_site_instruction OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 70109)
-- Name: hem_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=hem'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE hem_system OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 70113)
-- Name: hem_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW hem_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=hem'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE hem_upload OWNER TO postgres;

--
-- TOC entry 298 (class 1259 OID 70117)
-- Name: landowner; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE landowner (
    landowner_id integer NOT NULL,
    owner_name character varying(128),
    address character varying(256),
    removed boolean DEFAULT false
);


ALTER TABLE landowner OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 70121)
-- Name: landowner_landowner_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE landowner_landowner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE landowner_landowner_id_seq OWNER TO postgres;

--
-- TOC entry 3579 (class 0 OID 0)
-- Dependencies: 299
-- Name: landowner_landowner_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE landowner_landowner_id_seq OWNED BY landowner.landowner_id;


--
-- TOC entry 300 (class 1259 OID 70123)
-- Name: menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE menu (
    menu_id integer NOT NULL,
    menu_name character varying(32),
    menu_href character varying(128),
    menu_level smallint,
    menu_order smallint,
    menu_text character varying(32),
    removed boolean DEFAULT false
);


ALTER TABLE menu OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 70127)
-- Name: menu_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE menu_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE menu_menu_id_seq OWNER TO postgres;

--
-- TOC entry 3580 (class 0 OID 0)
-- Dependencies: 301
-- Name: menu_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE menu_menu_id_seq OWNED BY menu.menu_id;


--
-- TOC entry 302 (class 1259 OID 70129)
-- Name: mikejones_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=mikejones'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE mikejones_completion_cert OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 70134)
-- Name: mikejones_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete
   FROM dblink('dbname=mikejones'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision);


ALTER TABLE mikejones_docket_day OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 70138)
-- Name: mikejones_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=mikejones'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE mikejones_employee OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 70142)
-- Name: mikejones_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=mikejones'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE mikejones_field_estimate OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 70147)
-- Name: mikejones_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=mikejones'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE mikejones_field_estimate_line OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 70151)
-- Name: mikejones_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=mikejones'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE mikejones_gravel_water OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 70156)
-- Name: mikejones_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=mikejones'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE mikejones_hour OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 70161)
-- Name: mikejones_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=mikejones'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE mikejones_hour_audit OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 70166)
-- Name: mikejones_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=mikejones'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE mikejones_plant OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 70170)
-- Name: mikejones_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=mikejones'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE mikejones_site_instruction OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 70174)
-- Name: mikejones_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=mikejones'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE mikejones_system OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 70178)
-- Name: mikejones_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW mikejones_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=mikejones'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE mikejones_upload OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 70182)
-- Name: njcon_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=njcontracting'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE njcon_completion_cert OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 70187)
-- Name: njcon_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=njcontracting'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE njcon_docket_day OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 70191)
-- Name: njcon_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=njcontracting'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE njcon_employee OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 70195)
-- Name: njcon_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=njcontracting'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE njcon_field_estimate OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 70200)
-- Name: njcon_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=njcontracting'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE njcon_field_estimate_line OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 70204)
-- Name: njcon_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=njcontracting'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE njcon_gravel_water OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 70209)
-- Name: njcon_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=njcontracting'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE njcon_hour OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 70214)
-- Name: njcon_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=njcontracting'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE njcon_hour_audit OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 70219)
-- Name: njcon_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=njcontracting'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE njcon_plant OWNER TO postgres;

--
-- TOC entry 323 (class 1259 OID 70223)
-- Name: njcon_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=njcontracting'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE njcon_site_instruction OWNER TO postgres;

--
-- TOC entry 324 (class 1259 OID 70227)
-- Name: njcon_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=njcontracting'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE njcon_system OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 70231)
-- Name: njcon_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW njcon_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=njcontracting'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE njcon_upload OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 70235)
-- Name: old_employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE old_employee (
    employee_id integer,
    username character varying(64),
    password character varying(64),
    firstname character varying(32),
    lastname character varying(32),
    last_login timestamp without time zone,
    removed boolean,
    profile_id smallint,
    next_of_kin_name character varying(64),
    next_of_kin_fone character varying(12),
    company_id integer,
    signature text,
    locked boolean,
    emp_type character varying(4)
);


ALTER TABLE old_employee OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 70241)
-- Name: ostwald_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=ostwaldbros'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE ostwald_completion_cert OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 70246)
-- Name: ostwald_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=ostwaldbros'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE ostwald_docket_day OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 70250)
-- Name: ostwald_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=ostwaldbros'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE ostwald_employee OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 70254)
-- Name: ostwald_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=ostwaldbros'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE ostwald_field_estimate OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 70258)
-- Name: ostwald_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=ostwaldbros'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE ostwald_field_estimate_line OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 70262)
-- Name: ostwald_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=ostwaldbros'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE ostwald_gravel_water OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 70267)
-- Name: ostwald_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=ostwaldbros'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE ostwald_hour OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 70272)
-- Name: ostwald_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=ostwaldbros'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE ostwald_hour_audit OWNER TO postgres;

--
-- TOC entry 335 (class 1259 OID 70277)
-- Name: ostwald_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=ostwaldbros'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE ostwald_plant OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 70281)
-- Name: ostwald_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=ostwaldbros'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE ostwald_site_instruction OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 70285)
-- Name: ostwald_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=ostwaldbros'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE ostwald_system OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 70289)
-- Name: ostwald_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW ostwald_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=ostwaldbros'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE ostwald_upload OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 70293)
-- Name: plant_corbets_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plant_corbets_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plant_corbets_plant_id_seq OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 70295)
-- Name: plant_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE plant_type (
    plant_type_id integer NOT NULL,
    p_type character varying(64),
    removed boolean DEFAULT false,
    plant_rate double precision,
    new_rate double precision,
    effective_from date,
    stand_rate double precision,
    stand_new_rate double precision,
    contractor_id integer DEFAULT 0,
    unit_of_measure character varying(8),
    price_type character varying(16)
);


ALTER TABLE plant_type OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 70300)
-- Name: plant_type_audit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE plant_type_audit (
    plant_type_id integer NOT NULL,
    p_type character varying(64),
    removed boolean DEFAULT false,
    plant_rate double precision,
    new_rate double precision,
    effective_from date,
    stand_rate double precision,
    stand_new_rate double precision,
    contractor_id integer DEFAULT 0,
    unit_of_measure character varying(8),
    price_type character varying(16),
    audit_time timestamp with time zone,
    del_update character varying(4)
);


ALTER TABLE plant_type_audit OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 70305)
-- Name: plant_type_plant_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plant_type_plant_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plant_type_plant_type_id_seq OWNER TO postgres;

--
-- TOC entry 3581 (class 0 OID 0)
-- Dependencies: 342
-- Name: plant_type_plant_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE plant_type_plant_type_id_seq OWNED BY plant_type.plant_type_id;


--
-- TOC entry 343 (class 1259 OID 70307)
-- Name: price_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE price_type (
    price_type_id integer NOT NULL,
    price_name character varying(32)
);


ALTER TABLE price_type OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 70310)
-- Name: price_type_price_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE price_type_price_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE price_type_price_type_id_seq OWNER TO postgres;

--
-- TOC entry 3582 (class 0 OID 0)
-- Dependencies: 344
-- Name: price_type_price_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE price_type_price_type_id_seq OWNED BY price_type.price_type_id;


--
-- TOC entry 345 (class 1259 OID 70312)
-- Name: profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE profile (
    profile_id integer NOT NULL,
    profile_text character varying(32)
);


ALTER TABLE profile OWNER TO postgres;

--
-- TOC entry 346 (class 1259 OID 70315)
-- Name: profile_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE profile_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE profile_profile_id_seq OWNER TO postgres;

--
-- TOC entry 3583 (class 0 OID 0)
-- Dependencies: 346
-- Name: profile_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE profile_profile_id_seq OWNED BY profile.profile_id;


--
-- TOC entry 347 (class 1259 OID 70317)
-- Name: qgc_backup_area; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_area AS
 SELECT t1.area_id,
    t1.area_name,
    t1.removed,
    t1.contractor_id
   FROM dblink('dbname=qgc_backup'::text, 'Select area_id,area_name,removed,contractor_id from area'::text) t1(area_id integer, area_name text, removed boolean, contractor_id integer);


ALTER TABLE qgc_backup_area OWNER TO postgres;

--
-- TOC entry 348 (class 1259 OID 70321)
-- Name: qgc_backup_calloff_order; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_calloff_order AS
 SELECT t5.calloff_order_id,
    t5.request_estimate_id,
    t5.field_estimate_id,
    t5.contractor_id,
    t5.area_id,
    t5.crew_id,
    t5.contract_num,
    t5.commencement_date,
    t5.cost_estimate,
    t5.services,
    t5.remarks,
    t5.qgc_signee,
    t5.creation_date,
    t5.created_by,
    t5.removed,
    t5.well_ids,
    t5.qgc_sig,
    t5.qgc_approve_date,
    t5.landowner_id_1,
    t5.gravel_loc_id_1,
    t5.landowner_id_2,
    t5.water_loc_id_2,
    t5.gravel_return,
    t5.water_return,
    t5.email_emp_ids,
    t5.completion_cert_id
   FROM dblink('dbname=qgc_backup'::text, 'Select calloff_order_id, request_estimate_id, field_estimate_id, contractor_id, area_id,crew_id, contract_num, commencement_date, cost_estimate,
    services, remarks,qgc_signee, creation_date,created_by,removed, well_ids,qgc_sig,qgc_approve_date,landowner_id_1,gravel_loc_id_1,landowner_id_2,
     water_loc_id_2, gravel_return, water_return, email_emp_ids,completion_cert_id
   from calloff_order'::text) t5(calloff_order_id integer, request_estimate_id integer, field_estimate_id integer, contractor_id integer, area_id integer, crew_id integer, contract_num text, commencement_date date, cost_estimate double precision, services text, remarks text, qgc_signee text, creation_date date, created_by integer, removed boolean, well_ids text, qgc_sig text, qgc_approve_date date, landowner_id_1 integer, gravel_loc_id_1 integer, landowner_id_2 integer, water_loc_id_2 integer, gravel_return smallint, water_return smallint, email_emp_ids text, completion_cert_id integer);


ALTER TABLE qgc_backup_calloff_order OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 70326)
-- Name: qgc_backup_contractor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_contractor AS
 SELECT t3.contractor_id,
    t3.username,
    t3.password,
    t3.name,
    t3.last_login,
    t3.removed,
    t3.profile_id,
    t3.db_link,
    t3.db,
    t3.domain,
    t3.con_name,
    t3.expense_percentage,
    t3.employee_id,
    t3.contract_no
   FROM dblink('dbname=qgc_backup'::text, 'Select * from contractor'::text) t3(contractor_id integer, username text, password text, name text, last_login date, removed boolean, profile_id integer, db_link text, db text, domain text, con_name text, expense_percentage double precision, employee_id integer, contract_no text);


ALTER TABLE qgc_backup_contractor OWNER TO postgres;

--
-- TOC entry 350 (class 1259 OID 70330)
-- Name: qgc_backup_crew; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_crew AS
 SELECT t2.crew_id,
    t2.crew_name,
    t2.removed,
    t2.contractor_id
   FROM dblink('dbname=qgc_backup'::text, 'Select crew_id,crew_name,removed,contractor_id from crew'::text) t2(crew_id integer, crew_name text, removed boolean, contractor_id integer);


ALTER TABLE qgc_backup_crew OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 70334)
-- Name: qgc_backup_email_attachment; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_email_attachment AS
 SELECT t2.email_attachment_id,
    t2.email_log_id,
    t2.attachment_name,
    t2.recipient_type,
    t2.email,
    t2.attach_date,
    t2.email_sent
   FROM dblink('dbname=qgc_backup'::text, 'select * from email_attachment'::text) t2(email_attachment_id integer, email_log_id integer, attachment_name text, recipient_type text, email text, attach_date date, email_sent boolean);


ALTER TABLE qgc_backup_email_attachment OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 70338)
-- Name: qgc_backup_email_log; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_email_log AS
 SELECT t2.email_log_id,
    t2.employee_id,
    t2.company_id,
    t2.receiver_id,
    t2.receiver_type,
    t2.subject,
    t2.dkt_id,
    t2.fault,
    t2.dkt_date,
    t2.coos,
    t2.supervisor_group_id
   FROM dblink('dbname=qgc_backup'::text, 'select * from email_log'::text) t2(email_log_id integer, employee_id integer, company_id integer, receiver_id integer, receiver_type text, subject text, dkt_id integer, fault text, dkt_date date, coos text, supervisor_group_id integer);


ALTER TABLE qgc_backup_email_log OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 70342)
-- Name: qgc_backup_email_result; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_email_result AS
 SELECT t2.email_result_id,
    t2.email_log_id,
    t2.sent_receiver_emails,
    t2.sent_company_emails,
    t2.receiver_message_id,
    t2.company_message_id,
    t2.attachments,
    t2.receiver_fault,
    t2.company_fault,
    t2.send_time
   FROM dblink('dbname=qgc_backup'::text, 'select * from email_result'::text) t2(email_result_id integer, email_log_id integer, sent_receiver_emails text, sent_company_emails text, receiver_message_id text, company_message_id text, attachments text, receiver_fault text, company_fault text, send_time timestamp without time zone);


ALTER TABLE qgc_backup_email_result OWNER TO postgres;

--
-- TOC entry 354 (class 1259 OID 70346)
-- Name: qgc_backup_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_employee AS
 SELECT t3.employee_id,
    t3.username,
    t3.password,
    t3.firstname,
    t3.lastname,
    t3.last_login,
    t3.removed,
    t3.next_of_kin_name,
    t3.next_of_kin_fone,
    t3.company_id,
    t3.signature,
    t3.locked,
    t3.emp_type
   FROM dblink('dbname=qgc_backup'::text, 'Select * from employee'::text) t3(employee_id integer, username text, password text, firstname text, lastname text, last_login date, removed boolean, profile_id integer, next_of_kin_name text, next_of_kin_fone text, company_id integer, signature text, locked boolean, emp_type text);


ALTER TABLE qgc_backup_employee OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 70350)
-- Name: qgc_backup_gravel_water_loc; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_gravel_water_loc AS
 SELECT t2.gravel_water_loc_id,
    t2.landowner_id,
    t2.loc_type,
    t2.location,
    t2.pushed_up_rate,
    t2.non_pushed_up_rate,
    t2.water_rate,
    t2.removed
   FROM dblink('dbname=qgc_backup'::text, 'Select * from gravel_water_loc'::text) t2(gravel_water_loc_id integer, landowner_id integer, loc_type smallint, location text, pushed_up_rate double precision, non_pushed_up_rate double precision, water_rate double precision, removed boolean);


ALTER TABLE qgc_backup_gravel_water_loc OWNER TO postgres;

--
-- TOC entry 356 (class 1259 OID 70354)
-- Name: qgc_backup_landowner; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_landowner AS
 SELECT t2.landowner_id,
    t2.owner_name,
    t2.address,
    t2.removed
   FROM dblink('dbname=qgc_backup'::text, 'Select * from landowner'::text) t2(landowner_id integer, owner_name text, address text, removed boolean);


ALTER TABLE qgc_backup_landowner OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 70358)
-- Name: qgc_backup_plant_type; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_plant_type AS
 SELECT t2.plant_type_id,
    t2.p_type,
    t2.removed,
    t2.plant_rate,
    t2.new_rate,
    t2.effective_from,
    t2.stand_rate,
    t2.stand_new_rate,
    t2.contractor_id,
    t2.unit_of_measure,
    t2.price_type
   FROM dblink('dbname=qgc_backup'::text, 'Select * from plant_type'::text) t2(plant_type_id integer, p_type text, removed boolean, plant_rate double precision, new_rate double precision, effective_from date, stand_rate double precision, stand_new_rate double precision, contractor_id integer, unit_of_measure text, price_type text);


ALTER TABLE qgc_backup_plant_type OWNER TO postgres;

--
-- TOC entry 358 (class 1259 OID 70362)
-- Name: qgc_backup_request_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_request_estimate AS
 SELECT t3.request_estimate_id,
    t3.contractor_id,
    t3.area_id,
    t3.well_id,
    t3.field_estimate_id,
    t3.type_id,
    t3.supervisor_group_id,
    t3.created_by,
    t3.work_check,
    t3.other_text,
    t3.scope_works,
    t3.site_conditions,
    t3.gravel_return,
    t3.water_return,
    t3.est_start_date,
    t3.est_compl_date,
    t3.create_date,
    t3.removed,
    t3.qgc_sig,
    t3.qgc_approve_date,
    t3.contractor_signee,
    t3.contractor_sig,
    t3.contractor_sig_date,
    t3.well_ids,
    t3.contractor_estimate,
    t3.qgc_signee,
    t3.landowner_id_1,
    t3.gravel_loc_id_1,
    t3.landowner_id_2,
    t3.water_loc_id_2,
    t3.email_emp_ids
   FROM dblink('dbname=qgc_backup'::text, 'SELECT * from request_estimate'::text) t3(request_estimate_id integer, contractor_id integer, area_id integer, well_id integer, field_estimate_id integer, type_id integer, supervisor_group_id integer, created_by integer, work_check text, other_text text, scope_works text, site_conditions text, gravel_return integer, water_return integer, est_start_date date, est_compl_date date, create_date date, removed boolean, qgc_sig text, qgc_approve_date date, contractor_signee text, contractor_sig text, contractor_sig_date date, well_ids text, contractor_estimate double precision, qgc_signee text, landowner_id_1 integer, gravel_loc_id_1 integer, landowner_id_2 integer, water_loc_id_2 integer, email_emp_ids text);


ALTER TABLE qgc_backup_request_estimate OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 70367)
-- Name: qgc_backup_well; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW qgc_backup_well AS
 SELECT t3.well_id,
    t3.area_id,
    t3.well_name,
    t3.removed,
    t3.contractor_id
   FROM dblink('dbname=qgc_backup'::text, 'Select well_id,area_id,well_name,removed,contractor_id from well'::text) t3(well_id integer, area_id integer, well_name text, removed boolean, contractor_id integer);


ALTER TABLE qgc_backup_well OWNER TO postgres;

--
-- TOC entry 360 (class 1259 OID 70371)
-- Name: qgc_work_scope; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE qgc_work_scope (
    qgc_work_scope_id integer NOT NULL,
    ext_contractor_id integer,
    crew_id integer,
    area_id integer,
    well_id integer,
    amount double precision,
    removed boolean DEFAULT false,
    end_date date
);


ALTER TABLE qgc_work_scope OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 70375)
-- Name: qgc_work_scope_qgc_work_scope_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE qgc_work_scope_qgc_work_scope_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE qgc_work_scope_qgc_work_scope_id_seq OWNER TO postgres;

--
-- TOC entry 3584 (class 0 OID 0)
-- Dependencies: 361
-- Name: qgc_work_scope_qgc_work_scope_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE qgc_work_scope_qgc_work_scope_id_seq OWNED BY qgc_work_scope.qgc_work_scope_id;


--
-- TOC entry 362 (class 1259 OID 70377)
-- Name: rby_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=rby'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE rby_completion_cert OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 70382)
-- Name: rby_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete,
    t2.sub_crew_id
   FROM dblink('dbname=rby'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete,sub_crew_id  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision, sub_crew_id integer);


ALTER TABLE rby_docket_day OWNER TO postgres;

--
-- TOC entry 364 (class 1259 OID 70386)
-- Name: rby_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=rby'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE rby_employee OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 70390)
-- Name: rby_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=rby'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE rby_field_estimate OWNER TO postgres;

--
-- TOC entry 366 (class 1259 OID 70394)
-- Name: rby_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=rby'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE rby_field_estimate_line OWNER TO postgres;

--
-- TOC entry 367 (class 1259 OID 70398)
-- Name: rby_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=rby'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE rby_gravel_water OWNER TO postgres;

--
-- TOC entry 368 (class 1259 OID 70403)
-- Name: rby_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=rby'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE rby_hour OWNER TO postgres;

--
-- TOC entry 369 (class 1259 OID 70408)
-- Name: rby_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=rby'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE rby_hour_audit OWNER TO postgres;

--
-- TOC entry 370 (class 1259 OID 70413)
-- Name: rby_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=rby'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE rby_plant OWNER TO postgres;

--
-- TOC entry 371 (class 1259 OID 70417)
-- Name: rby_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=rby'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE rby_site_instruction OWNER TO postgres;

--
-- TOC entry 372 (class 1259 OID 70421)
-- Name: rby_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=rby'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE rby_system OWNER TO postgres;

--
-- TOC entry 373 (class 1259 OID 70425)
-- Name: rby_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rby_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=rby'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE rby_upload OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 70429)
-- Name: request_estimate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE request_estimate (
    request_estimate_id integer NOT NULL,
    contractor_id integer,
    area_id integer,
    well_id integer,
    field_estimate_id integer DEFAULT 0,
    type_id integer,
    supervisor_group_id integer,
    created_by integer,
    work_check character varying(32),
    other_text character varying(1024),
    scope_works character varying(1024),
    site_conditions character varying(1024),
    gravel_return smallint,
    water_return smallint,
    est_start_date date,
    est_compl_date date,
    create_date date,
    removed boolean DEFAULT false,
    qgc_sig text,
    qgc_approve_date date DEFAULT ('now'::text)::date,
    contractor_signee character varying(64),
    contractor_sig text,
    contractor_sig_date date DEFAULT ('now'::text)::date,
    well_ids character varying(256),
    contractor_estimate double precision,
    qgc_signee character varying(64),
    landowner_id_1 integer,
    gravel_loc_id_1 integer,
    landowner_id_2 integer,
    water_loc_id_2 integer,
    email_emp_ids character varying(128),
    crew_id integer,
    sub_crew_ids character varying(64),
    field_estimate_ids character varying(64)
);


ALTER TABLE request_estimate OWNER TO postgres;

--
-- TOC entry 375 (class 1259 OID 70439)
-- Name: request_estimate_request_estimate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE request_estimate_request_estimate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE request_estimate_request_estimate_id_seq OWNER TO postgres;

--
-- TOC entry 3585 (class 0 OID 0)
-- Dependencies: 375
-- Name: request_estimate_request_estimate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE request_estimate_request_estimate_id_seq OWNED BY request_estimate.request_estimate_id;


--
-- TOC entry 376 (class 1259 OID 70441)
-- Name: sessions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sessions (
    id character varying(32) NOT NULL,
    access integer,
    data text
);


ALTER TABLE sessions OWNER TO postgres;

--
-- TOC entry 377 (class 1259 OID 70447)
-- Name: site_ins_required; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE site_ins_required (
    site_ins_required_id integer NOT NULL,
    calloff_order_id integer,
    contractor_id integer,
    instruction_no integer,
    employee_id integer,
    create_date date,
    notification_date date,
    supervisor_ids character varying(64),
    qgc_sig text,
    qgc_approve_date date,
    qgc_signee character varying(128),
    emailed_create boolean DEFAULT false,
    emailed_notification boolean DEFAULT false
);


ALTER TABLE site_ins_required OWNER TO postgres;

--
-- TOC entry 378 (class 1259 OID 70455)
-- Name: site_ins_required_site_ins_required_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE site_ins_required_site_ins_required_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE site_ins_required_site_ins_required_id_seq OWNER TO postgres;

--
-- TOC entry 3586 (class 0 OID 0)
-- Dependencies: 378
-- Name: site_ins_required_site_ins_required_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE site_ins_required_site_ins_required_id_seq OWNED BY site_ins_required.site_ins_required_id;


--
-- TOC entry 379 (class 1259 OID 70457)
-- Name: standdown_reason; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE standdown_reason (
    standdown_reason_id integer NOT NULL,
    reason character varying(32),
    removed boolean DEFAULT false
);


ALTER TABLE standdown_reason OWNER TO postgres;

--
-- TOC entry 380 (class 1259 OID 70461)
-- Name: standdown_reason_standdown_reason_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE standdown_reason_standdown_reason_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE standdown_reason_standdown_reason_id_seq OWNER TO postgres;

--
-- TOC entry 3587 (class 0 OID 0)
-- Dependencies: 380
-- Name: standdown_reason_standdown_reason_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE standdown_reason_standdown_reason_id_seq OWNED BY standdown_reason.standdown_reason_id;


--
-- TOC entry 381 (class 1259 OID 70463)
-- Name: state; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE state (
    state_id integer NOT NULL,
    state_name character varying(8)
);


ALTER TABLE state OWNER TO postgres;

--
-- TOC entry 382 (class 1259 OID 70466)
-- Name: state_state_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE state_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE state_state_id_seq OWNER TO postgres;

--
-- TOC entry 3588 (class 0 OID 0)
-- Dependencies: 382
-- Name: state_state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE state_state_id_seq OWNED BY state.state_id;


--
-- TOC entry 383 (class 1259 OID 70468)
-- Name: sub_crew; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sub_crew (
    sub_crew_id integer NOT NULL,
    crew_id integer,
    sub_crew_name character varying(64),
    removed boolean DEFAULT false,
    old_crew_id integer
);


ALTER TABLE sub_crew OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 70472)
-- Name: sub_crew_sub_crew_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sub_crew_sub_crew_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sub_crew_sub_crew_id_seq OWNER TO postgres;

--
-- TOC entry 3589 (class 0 OID 0)
-- Dependencies: 384
-- Name: sub_crew_sub_crew_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sub_crew_sub_crew_id_seq OWNED BY sub_crew.sub_crew_id;


--
-- TOC entry 385 (class 1259 OID 70474)
-- Name: supervisor_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE supervisor_group (
    supervisor_group_id integer NOT NULL,
    group_name character varying(64),
    removed boolean DEFAULT false,
    employee_ids character varying(64),
    group_emails character varying(512)
);


ALTER TABLE supervisor_group OWNER TO postgres;

--
-- TOC entry 386 (class 1259 OID 70481)
-- Name: supervisor_group_supervisor_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE supervisor_group_supervisor_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE supervisor_group_supervisor_group_id_seq OWNER TO postgres;

--
-- TOC entry 3590 (class 0 OID 0)
-- Dependencies: 386
-- Name: supervisor_group_supervisor_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE supervisor_group_supervisor_group_id_seq OWNED BY supervisor_group.supervisor_group_id;


--
-- TOC entry 387 (class 1259 OID 70483)
-- Name: system; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE system (
    system_id integer NOT NULL,
    sys_name character varying(64),
    sys_val character varying(128)
);


ALTER TABLE system OWNER TO postgres;

--
-- TOC entry 388 (class 1259 OID 70486)
-- Name: system_system_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE system_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system_system_id_seq OWNER TO postgres;

--
-- TOC entry 3591 (class 0 OID 0)
-- Dependencies: 388
-- Name: system_system_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE system_system_id_seq OWNED BY system.system_id;


--
-- TOC entry 389 (class 1259 OID 70488)
-- Name: tandw_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel_m3,
    t2.total_water_kl,
    t2.gravel_units,
    t2.water_units
   FROM dblink('dbname=tandw'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel_m3 double precision, total_water_kl double precision, gravel_units double precision, water_units double precision);


ALTER TABLE tandw_completion_cert OWNER TO postgres;

--
-- TOC entry 390 (class 1259 OID 70493)
-- Name: tandw_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete
   FROM dblink('dbname=tandw'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision);


ALTER TABLE tandw_docket_day OWNER TO postgres;

--
-- TOC entry 391 (class 1259 OID 70497)
-- Name: tandw_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=tandw'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE tandw_employee OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 70501)
-- Name: tandw_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee
   FROM dblink('dbname=tandw'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text);


ALTER TABLE tandw_field_estimate OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 70505)
-- Name: tandw_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=tandw'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE tandw_field_estimate_line OWNER TO postgres;

--
-- TOC entry 394 (class 1259 OID 70509)
-- Name: tandw_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed
   FROM dblink('dbname=tandw'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean);


ALTER TABLE tandw_gravel_water OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 70514)
-- Name: tandw_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids
   FROM dblink('dbname=tandw'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text);


ALTER TABLE tandw_hour OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 70519)
-- Name: tandw_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time
   FROM dblink('dbname=tandw'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone);


ALTER TABLE tandw_hour_audit OWNER TO postgres;

--
-- TOC entry 397 (class 1259 OID 70524)
-- Name: tandw_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_plant AS
 SELECT t2.plant_id,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed
   FROM dblink('dbname=tandw'::text, 'Select plant_id,plant_type_id,plant_unit,removed from plant'::text) t2(plant_id integer, plant_type_id integer, plant_unit text, removed boolean);


ALTER TABLE tandw_plant OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 70528)
-- Name: tandw_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date
   FROM dblink('dbname=tandw'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date);


ALTER TABLE tandw_site_instruction OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 70532)
-- Name: tandw_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=tandw'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE tandw_system OWNER TO postgres;

--
-- TOC entry 400 (class 1259 OID 70536)
-- Name: tandw_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW tandw_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=tandw'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE tandw_upload OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 70540)
-- Name: tmp_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tmp_message (
    tmp_message_id integer NOT NULL,
    expires timestamp with time zone,
    employee_id integer,
    message text,
    seen boolean DEFAULT false,
    email_id integer
);


ALTER TABLE tmp_message OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 70547)
-- Name: tmp_message_tmp_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tmp_message_tmp_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tmp_message_tmp_message_id_seq OWNER TO postgres;

--
-- TOC entry 3592 (class 0 OID 0)
-- Dependencies: 402
-- Name: tmp_message_tmp_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tmp_message_tmp_message_id_seq OWNED BY tmp_message.tmp_message_id;


--
-- TOC entry 403 (class 1259 OID 70549)
-- Name: tmp_wells; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tmp_wells (
    area_name character varying(64),
    well_id integer,
    coos character varying(128)
);


ALTER TABLE tmp_wells OWNER TO postgres;

--
-- TOC entry 404 (class 1259 OID 70552)
-- Name: upload_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE upload_type (
    upload_type_id integer NOT NULL,
    description character varying(32),
    sub_type character varying(16)
);


ALTER TABLE upload_type OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 70555)
-- Name: upload_type_upload_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE upload_type_upload_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE upload_type_upload_type_id_seq OWNER TO postgres;

--
-- TOC entry 3593 (class 0 OID 0)
-- Dependencies: 405
-- Name: upload_type_upload_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE upload_type_upload_type_id_seq OWNED BY upload_type.upload_type_id;


--
-- TOC entry 406 (class 1259 OID 70557)
-- Name: wad_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=wad'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE wad_completion_cert OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 70562)
-- Name: wad_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete
   FROM dblink('dbname=wad'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision);


ALTER TABLE wad_docket_day OWNER TO postgres;

--
-- TOC entry 408 (class 1259 OID 70566)
-- Name: wad_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=wad'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE wad_employee OWNER TO postgres;

--
-- TOC entry 409 (class 1259 OID 70570)
-- Name: wad_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=wad'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE wad_field_estimate OWNER TO postgres;

--
-- TOC entry 410 (class 1259 OID 70574)
-- Name: wad_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=wad'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE wad_field_estimate_line OWNER TO postgres;

--
-- TOC entry 411 (class 1259 OID 70578)
-- Name: wad_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=wad'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE wad_gravel_water OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 70583)
-- Name: wad_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=wad'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE wad_hour OWNER TO postgres;

--
-- TOC entry 413 (class 1259 OID 70588)
-- Name: wad_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=wad'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE wad_hour_audit OWNER TO postgres;

--
-- TOC entry 414 (class 1259 OID 70593)
-- Name: wad_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=wad'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE wad_plant OWNER TO postgres;

--
-- TOC entry 415 (class 1259 OID 70597)
-- Name: wad_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=wad'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE wad_site_instruction OWNER TO postgres;

--
-- TOC entry 416 (class 1259 OID 70601)
-- Name: wad_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=wad'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE wad_system OWNER TO postgres;

--
-- TOC entry 417 (class 1259 OID 70605)
-- Name: wad_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wad_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=wad'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE wad_upload OWNER TO postgres;

--
-- TOC entry 418 (class 1259 OID 70609)
-- Name: well; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE well (
    well_id integer NOT NULL,
    area_id integer,
    well_name character varying(32),
    removed boolean DEFAULT false,
    contractor_id integer DEFAULT 0
);


ALTER TABLE well OWNER TO postgres;

--
-- TOC entry 419 (class 1259 OID 70614)
-- Name: well_well_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE well_well_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE well_well_id_seq OWNER TO postgres;

--
-- TOC entry 3594 (class 0 OID 0)
-- Dependencies: 419
-- Name: well_well_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE well_well_id_seq OWNED BY well.well_id;


--
-- TOC entry 420 (class 1259 OID 70616)
-- Name: wr_completion_cert; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_completion_cert AS
 SELECT t2.completion_cert_id,
    t2.calloff_order_id,
    t2.employee_id,
    t2.crew_id,
    t2.area_id,
    t2.well_ids,
    t2.contract_num,
    t2.cost_estimate,
    t2.comments,
    t2.total_t1,
    t2.total_t2,
    t2.expense,
    t2.total,
    t2.completion_date,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.total_gravel,
    t2.total_water,
    t2.gravel_units,
    t2.water_units,
    t2.sub_crew_ids,
    t2.percentage_complete
   FROM dblink('dbname=wr'::text, 'Select * from completion_cert'::text) t2(completion_cert_id integer, calloff_order_id integer, employee_id integer, crew_id integer, area_id integer, well_ids text, contract_num text, cost_estimate double precision, comments text, total_t1 double precision, total_t2 double precision, expense double precision, total double precision, completion_date date, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, total_gravel double precision, total_water double precision, gravel_units double precision, water_units double precision, sub_crew_ids text, percentage_complete double precision);


ALTER TABLE wr_completion_cert OWNER TO postgres;

--
-- TOC entry 421 (class 1259 OID 70621)
-- Name: wr_docket_day; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_docket_day AS
 SELECT t2.docket_day_id,
    t2.docket_date,
    t2.status,
    t2.calloff_order_id,
    t2.percentage_complete
   FROM dblink('dbname=wr'::text, 'SELECT docket_day_id,docket_date,status,calloff_order_id,percentage_complete  from docket_day'::text) t2(docket_day_id integer, docket_date date, status smallint, calloff_order_id integer, percentage_complete double precision);


ALTER TABLE wr_docket_day OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 70625)
-- Name: wr_employee; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_employee AS
 SELECT t2.employee_id,
    t2.firstname,
    t2.lastname,
    t2.removed
   FROM dblink('dbname=wr'::text, 'Select employee_id, firstname,lastname,removed from employee'::text) t2(employee_id integer, firstname text, lastname text, removed boolean);


ALTER TABLE wr_employee OWNER TO postgres;

--
-- TOC entry 423 (class 1259 OID 70629)
-- Name: wr_field_estimate; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_field_estimate AS
 SELECT t1.field_estimate_id,
    t1.request_estimate_id,
    t1.site_instruction_id,
    t1.calloff_order_id,
    t1.area_id,
    t1.well_ids,
    t1.created_by,
    t1.create_date,
    t1.contractor_sig_date,
    t1.total_hours,
    t1.estimate_total,
    t1.removed,
    t1.supervisor_group_id,
    t1.comments,
    t1.qgc_approve_date,
    t1.contractor_sig,
    t1.qgc_sig,
    t1.contractor_signee,
    t1.qgc_signee,
    t1.crew_id,
    t1.sub_crew_id
   FROM dblink('dbname=wr'::text, 'Select * from field_estimate'::text) t1(field_estimate_id integer, request_estimate_id integer, site_instruction_id integer, calloff_order_id integer, area_id integer, well_ids text, created_by integer, create_date date, contractor_sig_date date, total_hours double precision, estimate_total double precision, removed boolean, supervisor_group_id integer, comments text, qgc_approve_date date, contractor_sig text, qgc_sig text, contractor_signee text, qgc_signee text, crew_id integer, sub_crew_id integer);


ALTER TABLE wr_field_estimate OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 70633)
-- Name: wr_field_estimate_line; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_field_estimate_line AS
 SELECT t1.field_estimate_line_id,
    t1.field_estimate_id,
    t1.plant_type,
    t1.machine,
    t1.rate,
    t1.hours,
    t1.sub_total,
    t1.plant_type_id,
    t1.washdown,
    t1.plant_id
   FROM dblink('dbname=wr'::text, 'Select * from field_estimate_line'::text) t1(field_estimate_line_id integer, field_estimate_id integer, plant_type text, machine text, rate double precision, hours double precision, sub_total double precision, plant_type_id integer, washdown boolean, plant_id integer);


ALTER TABLE wr_field_estimate_line OWNER TO postgres;

--
-- TOC entry 425 (class 1259 OID 70637)
-- Name: wr_gravel_water; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_gravel_water AS
 SELECT t1.gravel_water_id,
    t1.calloff_order_id,
    t1.crew_id,
    t1.area_id,
    t1.well_ids,
    t1.gravel_date,
    t1.docket_num,
    t1.loc_1_id,
    t1.gravel_rate,
    t1.gravel_units,
    t1.gravel_total,
    t1.pushed,
    t1.loc_2_id,
    t1.water_rate,
    t1.water_units,
    t1.water_total,
    t1.status,
    t1.enter_employee_id,
    t1.enter_time,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.emailed,
    t1.sub_crew_id,
    t1.site_instruction_id,
    t1.completion_cert_id
   FROM dblink('dbname=wr'::text, 'Select * from gravel_water'::text) t1(gravel_water_id integer, calloff_order_id integer, crew_id integer, area_id integer, well_ids text, gravel_date date, docket_num text, loc_1_id integer, gravel_rate double precision, gravel_units double precision, gravel_total double precision, pushed boolean, loc_2_id integer, water_rate double precision, water_units double precision, water_total double precision, status smallint, enter_employee_id integer, enter_time timestamp without time zone, qgc_approver_id integer, qgc_approval_date date, emailed boolean, sub_crew_id integer, site_instruction_id integer, completion_cert_id integer);


ALTER TABLE wr_gravel_water OWNER TO postgres;

--
-- TOC entry 426 (class 1259 OID 70642)
-- Name: wr_hour; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_hour AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=wr'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE wr_hour OWNER TO postgres;

--
-- TOC entry 427 (class 1259 OID 70647)
-- Name: wr_hour_audit; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_hour_audit AS
 SELECT t1.hour_id,
    t1.employee_id,
    t1.plant_id,
    t1.area_id,
    t1.well_id,
    t1.crew_id,
    t1.hour_date,
    t1.docket_hours_t1,
    t1.docket_num,
    t1.rate,
    t1.total_t1,
    t1.details,
    t1.invoice_num,
    t1.removed,
    t1.litres,
    t1.cubic_metres,
    t1.calloff_order_id,
    t1.status,
    t1.reject_text,
    t1.standdown_reason_id,
    t1.reason_text,
    t1.total_t2,
    t1.expense,
    t1.docket_hours_t2,
    t1.site_instruction_id,
    t1.price_type,
    t1.qgc_approver_id,
    t1.qgc_approval_date,
    t1.enter_time,
    t1.well_ids,
    t1.sub_crew_id,
    t1.completion_cert_id
   FROM dblink('dbname=wr'::text, 'Select
    hour_id, employee_id, plant_id, area_id, well_id, 
    crew_id, hour_date, docket_hours_t1, docket_num, rate, 
    total_t1, details, invoice_num, removed, litres, 
    cubic_metres, calloff_order_id, status, reject_text, 
    standdown_reason_id, reason_text, total_t2, expense, 
   docket_hours_t2, site_instruction_id, price_type ,qgc_approver_id,qgc_approval_date,enter_time,well_ids,sub_crew_id,completion_cert_id from hour_audit'::text) t1(hour_id integer, employee_id integer, plant_id integer, area_id integer, well_id integer, crew_id integer, hour_date date, docket_hours_t1 double precision, docket_num text, rate double precision, total_t1 double precision, details text, invoice_num text, removed boolean, litres double precision, cubic_metres double precision, calloff_order_id integer, status smallint, reject_text text, standdown_reason_id integer, reason_text text, total_t2 double precision, expense double precision, docket_hours_t2 double precision, site_instruction_id integer, price_type text, qgc_approver_id integer, qgc_approval_date date, enter_time timestamp without time zone, well_ids text, sub_crew_id integer, completion_cert_id integer);


ALTER TABLE wr_hour_audit OWNER TO postgres;

--
-- TOC entry 428 (class 1259 OID 70652)
-- Name: wr_plant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_plant AS
 SELECT t2.plant_id,
    t2.plant_name,
    t2.plant_type_id,
    t2.plant_unit,
    t2.removed,
    t2.approved
   FROM dblink('dbname=wr'::text, 'Select plant_id,plant_name,plant_type_id,plant_unit,removed,approved from plant'::text) t2(plant_id integer, plant_name text, plant_type_id integer, plant_unit text, removed boolean, approved boolean);


ALTER TABLE wr_plant OWNER TO postgres;

--
-- TOC entry 429 (class 1259 OID 70656)
-- Name: wr_site_instruction; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_site_instruction AS
 SELECT t2.site_instruction_id,
    t2.calloff_order_id,
    t2.field_estimate_id,
    t2.contractor_id,
    t2.employee_id,
    t2.well_ids,
    t2.instruction_no,
    t2.estimate_total,
    t2.instruction_date,
    t2.instruction_file,
    t2.comments,
    t2.removed,
    t2.contractor_sig,
    t2.qgc_sig,
    t2.qgc_approve_date,
    t2.contractor_signee,
    t2.qgc_signee,
    t2.contractor_sig_date,
    t2.crew_id,
    t2.sub_crew_ids
   FROM dblink('dbname=wr'::text, 'Select * from site_instruction'::text) t2(site_instruction_id integer, calloff_order_id integer, field_estimate_id integer, contractor_id integer, employee_id integer, well_ids text, instruction_no integer, estimate_total double precision, instruction_date date, instruction_file text, comments text, removed boolean, contractor_sig text, qgc_sig text, qgc_approve_date date, contractor_signee text, qgc_signee text, contractor_sig_date date, crew_id integer, sub_crew_ids text);


ALTER TABLE wr_site_instruction OWNER TO postgres;

--
-- TOC entry 430 (class 1259 OID 70660)
-- Name: wr_system; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_system AS
 SELECT t2.system_id,
    t2.sys_name,
    t2.sys_val
   FROM dblink('dbname=wr'::text, 'SELECT * from system'::text) t2(system_id integer, sys_name text, sys_val text);


ALTER TABLE wr_system OWNER TO postgres;

--
-- TOC entry 431 (class 1259 OID 70664)
-- Name: wr_upload; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW wr_upload AS
 SELECT t2.upload_id,
    t2.hour_id,
    t2.field_estimate_id,
    t2.timesheet_id,
    t2.completion_cert_id,
    t2.contractor_id,
    t2.upload_type_id,
    t2.employee_id,
    t2.upload_date,
    t2.upload_name,
    t2.coos,
    t2.h_date,
    t2.supervisor_id,
    t2.emailed,
    t2.gravel_water_id
   FROM dblink('dbname=wr'::text, 'Select * from upload'::text) t2(upload_id integer, hour_id integer, field_estimate_id integer, timesheet_id integer, completion_cert_id integer, contractor_id integer, upload_type_id integer, employee_id integer, upload_date date, upload_name text, coos text, h_date date, supervisor_id integer, emailed boolean, gravel_water_id integer);


ALTER TABLE wr_upload OWNER TO postgres;

--
-- TOC entry 432 (class 1259 OID 70668)
-- Name: zip; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE zip (
    zip_id integer NOT NULL,
    zip_name character varying(256),
    create_date date,
    employee_id integer,
    area_name character varying(64),
    well_name character varying(64)
);


ALTER TABLE zip OWNER TO postgres;

--
-- TOC entry 433 (class 1259 OID 70671)
-- Name: zip_zip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE zip_zip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zip_zip_id_seq OWNER TO postgres;

--
-- TOC entry 3595 (class 0 OID 0)
-- Dependencies: 433
-- Name: zip_zip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE zip_zip_id_seq OWNED BY zip.zip_id;


--
-- TOC entry 3061 (class 2604 OID 70673)
-- Name: address_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY address ALTER COLUMN address_id SET DEFAULT nextval('address_address_id_seq'::regclass);


--
-- TOC entry 3062 (class 2604 OID 70674)
-- Name: address_to_relation_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY address_to_relation ALTER COLUMN address_to_relation_id SET DEFAULT nextval('address_to_relation_address_to_relation_id_seq'::regclass);


--
-- TOC entry 3063 (class 2604 OID 70675)
-- Name: admin_menu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_menu ALTER COLUMN admin_menu_id SET DEFAULT nextval('admin_menu_admin_menu_id_seq'::regclass);


--
-- TOC entry 3064 (class 2604 OID 70676)
-- Name: area_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area ALTER COLUMN area_id SET DEFAULT nextval('area_area_id_seq'::regclass);


--
-- TOC entry 3067 (class 2604 OID 70677)
-- Name: atw_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw ALTER COLUMN atw_id SET DEFAULT nextval('atw_atw_id_seq'::regclass);


--
-- TOC entry 3071 (class 2604 OID 70678)
-- Name: atw_document_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw_document ALTER COLUMN atw_document_id SET DEFAULT nextval('atw_document_atw_document_id_seq'::regclass);


--
-- TOC entry 3072 (class 2604 OID 70679)
-- Name: atw_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw_type ALTER COLUMN atw_type_id SET DEFAULT nextval('atw_type_atw_type_id_seq'::regclass);


--
-- TOC entry 3073 (class 2604 OID 70680)
-- Name: batch_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY batch ALTER COLUMN batch_id SET DEFAULT nextval('batch_batch_id_seq'::regclass);


--
-- TOC entry 3075 (class 2604 OID 70681)
-- Name: budget_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY budget ALTER COLUMN budget_id SET DEFAULT nextval('budget_budget_id_seq'::regclass);


--
-- TOC entry 3077 (class 2604 OID 70682)
-- Name: calloff_order_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calloff_order ALTER COLUMN calloff_order_id SET DEFAULT nextval('calloff_order_calloff_order_id_seq'::regclass);


--
-- TOC entry 3080 (class 2604 OID 70683)
-- Name: certificate_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificate ALTER COLUMN certificate_id SET DEFAULT nextval('certificate_certificate_id_seq'::regclass);


--
-- TOC entry 3082 (class 2604 OID 70684)
-- Name: client_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY client ALTER COLUMN client_id SET DEFAULT nextval('client_client_id_seq'::regclass);


--
-- TOC entry 3084 (class 2604 OID 70685)
-- Name: company_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY company ALTER COLUMN company_id SET DEFAULT nextval('company_company_id_seq'::regclass);


--
-- TOC entry 3085 (class 2604 OID 70686)
-- Name: contact_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contact ALTER COLUMN contact_id SET DEFAULT nextval('contact_contact_id_seq'::regclass);


--
-- TOC entry 3088 (class 2604 OID 70687)
-- Name: contractor_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY contractor ALTER COLUMN contractor_id SET DEFAULT nextval('contractor_contractor_id_seq'::regclass);


--
-- TOC entry 3090 (class 2604 OID 70688)
-- Name: crew_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crew ALTER COLUMN crew_id SET DEFAULT nextval('crew_crew_id_seq'::regclass);


--
-- TOC entry 3093 (class 2604 OID 70689)
-- Name: email_attachment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_attachment ALTER COLUMN email_attachment_id SET DEFAULT nextval('email_attachment_email_attachment_id_seq'::regclass);


--
-- TOC entry 3096 (class 2604 OID 70690)
-- Name: email_log_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_log ALTER COLUMN email_log_id SET DEFAULT nextval('email_log_email_log_id_seq'::regclass);


--
-- TOC entry 3097 (class 2604 OID 70691)
-- Name: email_recipient_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_recipient ALTER COLUMN email_recipient_id SET DEFAULT nextval('email_recipient_email_recipient_id_seq'::regclass);


--
-- TOC entry 3098 (class 2604 OID 70692)
-- Name: email_result_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_result ALTER COLUMN email_result_id SET DEFAULT nextval('email_result_email_result_id_seq'::regclass);


--
-- TOC entry 3100 (class 2604 OID 70693)
-- Name: employee_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY employee ALTER COLUMN employee_id SET DEFAULT nextval('employee_employee_id_seq'::regclass);


--
-- TOC entry 3103 (class 2604 OID 70694)
-- Name: expense_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expense_type ALTER COLUMN expense_type_id SET DEFAULT nextval('expense_type_expense_type_id_seq'::regclass);


--
-- TOC entry 3106 (class 2604 OID 70695)
-- Name: ext_contractor_id; Type: DEFAULT; Schema: public; Owner: fil
--

ALTER TABLE ONLY ext_contractor ALTER COLUMN ext_contractor_id SET DEFAULT nextval('ext_contractor_ext_contractor_id_seq'::regclass);


--
-- TOC entry 3107 (class 2604 OID 70696)
-- Name: gravel_water_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gravel_water ALTER COLUMN gravel_water_id SET DEFAULT nextval('gravel_water_gravel_water_id_seq'::regclass);


--
-- TOC entry 3108 (class 2604 OID 70697)
-- Name: gravel_water_loc_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gravel_water_loc ALTER COLUMN gravel_water_loc_id SET DEFAULT nextval('gravel_water_loc_gravel_water_loc_id_seq'::regclass);


--
-- TOC entry 3113 (class 2604 OID 70698)
-- Name: landowner_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY landowner ALTER COLUMN landowner_id SET DEFAULT nextval('landowner_landowner_id_seq'::regclass);


--
-- TOC entry 3115 (class 2604 OID 70699)
-- Name: menu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menu ALTER COLUMN menu_id SET DEFAULT nextval('menu_menu_id_seq'::regclass);


--
-- TOC entry 3117 (class 2604 OID 70700)
-- Name: plant_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plant_type ALTER COLUMN plant_type_id SET DEFAULT nextval('plant_type_plant_type_id_seq'::regclass);


--
-- TOC entry 3122 (class 2604 OID 70701)
-- Name: price_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY price_type ALTER COLUMN price_type_id SET DEFAULT nextval('price_type_price_type_id_seq'::regclass);


--
-- TOC entry 3123 (class 2604 OID 70702)
-- Name: qgc_work_scope_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY qgc_work_scope ALTER COLUMN qgc_work_scope_id SET DEFAULT nextval('qgc_work_scope_qgc_work_scope_id_seq'::regclass);


--
-- TOC entry 3125 (class 2604 OID 70703)
-- Name: request_estimate_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY request_estimate ALTER COLUMN request_estimate_id SET DEFAULT nextval('request_estimate_request_estimate_id_seq'::regclass);


--
-- TOC entry 3130 (class 2604 OID 70704)
-- Name: site_ins_required_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY site_ins_required ALTER COLUMN site_ins_required_id SET DEFAULT nextval('site_ins_required_site_ins_required_id_seq'::regclass);


--
-- TOC entry 3133 (class 2604 OID 70705)
-- Name: standdown_reason_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY standdown_reason ALTER COLUMN standdown_reason_id SET DEFAULT nextval('standdown_reason_standdown_reason_id_seq'::regclass);


--
-- TOC entry 3135 (class 2604 OID 70706)
-- Name: state_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state ALTER COLUMN state_id SET DEFAULT nextval('state_state_id_seq'::regclass);


--
-- TOC entry 3136 (class 2604 OID 70707)
-- Name: sub_crew_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sub_crew ALTER COLUMN sub_crew_id SET DEFAULT nextval('sub_crew_sub_crew_id_seq'::regclass);


--
-- TOC entry 3138 (class 2604 OID 70708)
-- Name: supervisor_group_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supervisor_group ALTER COLUMN supervisor_group_id SET DEFAULT nextval('supervisor_group_supervisor_group_id_seq'::regclass);


--
-- TOC entry 3140 (class 2604 OID 70709)
-- Name: system_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY system ALTER COLUMN system_id SET DEFAULT nextval('system_system_id_seq'::regclass);


--
-- TOC entry 3141 (class 2604 OID 70710)
-- Name: tmp_message_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tmp_message ALTER COLUMN tmp_message_id SET DEFAULT nextval('tmp_message_tmp_message_id_seq'::regclass);


--
-- TOC entry 3143 (class 2604 OID 70711)
-- Name: upload_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY upload_type ALTER COLUMN upload_type_id SET DEFAULT nextval('upload_type_upload_type_id_seq'::regclass);


--
-- TOC entry 3144 (class 2604 OID 70712)
-- Name: well_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY well ALTER COLUMN well_id SET DEFAULT nextval('well_well_id_seq'::regclass);


--
-- TOC entry 3147 (class 2604 OID 70713)
-- Name: zip_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zip ALTER COLUMN zip_id SET DEFAULT nextval('zip_zip_id_seq'::regclass);


--
-- TOC entry 3176 (class 2606 OID 70716)
-- Name: Client_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY client
    ADD CONSTRAINT "Client_pkey" PRIMARY KEY (client_id);


--
-- TOC entry 3178 (class 2606 OID 70718)
-- Name: Company_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY company
    ADD CONSTRAINT "Company_pkey" PRIMARY KEY (company_id);


--
-- TOC entry 3149 (class 2606 OID 70720)
-- Name: address_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (address_id);


--
-- TOC entry 3151 (class 2606 OID 70722)
-- Name: address_to_relation_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY address_to_relation
    ADD CONSTRAINT address_to_relation_pkey PRIMARY KEY (address_to_relation_id);


--
-- TOC entry 3153 (class 2606 OID 70724)
-- Name: admin_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_menu
    ADD CONSTRAINT admin_menu_pkey PRIMARY KEY (admin_menu_id);


--
-- TOC entry 3155 (class 2606 OID 70726)
-- Name: area_area_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area
    ADD CONSTRAINT area_area_name_key UNIQUE (area_name, contractor_id);


--
-- TOC entry 3157 (class 2606 OID 70728)
-- Name: area_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area
    ADD CONSTRAINT area_pkey PRIMARY KEY (area_id);


--
-- TOC entry 3161 (class 2606 OID 70730)
-- Name: atw_document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw_document
    ADD CONSTRAINT atw_document_pkey PRIMARY KEY (atw_document_id);


--
-- TOC entry 3159 (class 2606 OID 70732)
-- Name: atw_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw
    ADD CONSTRAINT atw_pkey PRIMARY KEY (atw_id);


--
-- TOC entry 3164 (class 2606 OID 70734)
-- Name: atw_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw_type
    ADD CONSTRAINT atw_type_pkey PRIMARY KEY (atw_type_id);


--
-- TOC entry 3166 (class 2606 OID 70736)
-- Name: batch_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY batch
    ADD CONSTRAINT batch_pkey PRIMARY KEY (batch_id);


--
-- TOC entry 3168 (class 2606 OID 70738)
-- Name: budget_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY budget
    ADD CONSTRAINT budget_pkey PRIMARY KEY (budget_id);


--
-- TOC entry 3170 (class 2606 OID 70740)
-- Name: calloff_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY calloff_order
    ADD CONSTRAINT calloff_order_pkey PRIMARY KEY (calloff_order_id);


--
-- TOC entry 3174 (class 2606 OID 70742)
-- Name: ceritificate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY certificate
    ADD CONSTRAINT ceritificate_pkey PRIMARY KEY (certificate_id);


--
-- TOC entry 3180 (class 2606 OID 70744)
-- Name: contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (contact_id);


--
-- TOC entry 3182 (class 2606 OID 70746)
-- Name: contractor_name_key; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY contractor
    ADD CONSTRAINT contractor_name_key UNIQUE (name);


--
-- TOC entry 3184 (class 2606 OID 70748)
-- Name: contractor_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY contractor
    ADD CONSTRAINT contractor_pkey PRIMARY KEY (contractor_id);


--
-- TOC entry 3186 (class 2606 OID 70750)
-- Name: contractor_username_key; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY contractor
    ADD CONSTRAINT contractor_username_key UNIQUE (username);


--
-- TOC entry 3188 (class 2606 OID 70752)
-- Name: crew_crew_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crew
    ADD CONSTRAINT crew_crew_name_key UNIQUE (crew_name, contractor_id);


--
-- TOC entry 3190 (class 2606 OID 70754)
-- Name: crew_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY crew
    ADD CONSTRAINT crew_pkey PRIMARY KEY (crew_id);


--
-- TOC entry 3192 (class 2606 OID 70756)
-- Name: email_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_attachment
    ADD CONSTRAINT email_attachment_pkey PRIMARY KEY (email_attachment_id);


--
-- TOC entry 3197 (class 2606 OID 70758)
-- Name: email_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_log
    ADD CONSTRAINT email_log_pkey PRIMARY KEY (email_log_id);


--
-- TOC entry 3199 (class 2606 OID 70760)
-- Name: email_recipient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_recipient
    ADD CONSTRAINT email_recipient_pkey PRIMARY KEY (email_recipient_id);


--
-- TOC entry 3201 (class 2606 OID 70762)
-- Name: email_result_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_result
    ADD CONSTRAINT email_result_pkey PRIMARY KEY (email_result_id);


--
-- TOC entry 3203 (class 2606 OID 70764)
-- Name: email_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_rule
    ADD CONSTRAINT email_rule_pkey PRIMARY KEY (receiver_type);


--
-- TOC entry 3205 (class 2606 OID 70766)
-- Name: email_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_type
    ADD CONSTRAINT email_type_pkey PRIMARY KEY (receiver_type);


--
-- TOC entry 3207 (class 2606 OID 70768)
-- Name: employee_firstname_lastname_key; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_firstname_lastname_key UNIQUE (firstname, lastname);


--
-- TOC entry 3209 (class 2606 OID 70770)
-- Name: employee_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (employee_id);


--
-- TOC entry 3211 (class 2606 OID 70772)
-- Name: employee_username_key; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_username_key UNIQUE (username);


--
-- TOC entry 3216 (class 2606 OID 70774)
-- Name: expense_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_pkey PRIMARY KEY (expense_type_id);


--
-- TOC entry 3218 (class 2606 OID 70776)
-- Name: expense_type_contractor_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY expense_type
    ADD CONSTRAINT expense_type_contractor_id_key UNIQUE (type_expense, contractor_id);


--
-- TOC entry 3220 (class 2606 OID 70778)
-- Name: ext_contractor_con_name_key; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY ext_contractor
    ADD CONSTRAINT ext_contractor_con_name_key UNIQUE (con_name);


--
-- TOC entry 3222 (class 2606 OID 70780)
-- Name: ext_contractor_pkey; Type: CONSTRAINT; Schema: public; Owner: fil
--

ALTER TABLE ONLY ext_contractor
    ADD CONSTRAINT ext_contractor_pkey PRIMARY KEY (ext_contractor_id);


--
-- TOC entry 3226 (class 2606 OID 70782)
-- Name: gravel_water_loc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gravel_water_loc
    ADD CONSTRAINT gravel_water_loc_pkey PRIMARY KEY (gravel_water_loc_id);


--
-- TOC entry 3224 (class 2606 OID 70784)
-- Name: gravel_water_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gravel_water
    ADD CONSTRAINT gravel_water_pkey PRIMARY KEY (gravel_water_id);


--
-- TOC entry 3228 (class 2606 OID 70786)
-- Name: landowner_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY landowner
    ADD CONSTRAINT landowner_pkey PRIMARY KEY (landowner_id);


--
-- TOC entry 3230 (class 2606 OID 70788)
-- Name: landownr; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY landowner
    ADD CONSTRAINT landownr UNIQUE (owner_name);


--
-- TOC entry 3232 (class 2606 OID 70790)
-- Name: menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (menu_id);


--
-- TOC entry 3234 (class 2606 OID 70792)
-- Name: plant_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plant_type
    ADD CONSTRAINT plant_type_pkey PRIMARY KEY (plant_type_id);


--
-- TOC entry 3236 (class 2606 OID 70794)
-- Name: price_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY price_type
    ADD CONSTRAINT price_type_pkey PRIMARY KEY (price_type_id);


--
-- TOC entry 3238 (class 2606 OID 70796)
-- Name: profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (profile_id);


--
-- TOC entry 3240 (class 2606 OID 70798)
-- Name: qgc_work_scope_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY qgc_work_scope
    ADD CONSTRAINT qgc_work_scope_pkey PRIMARY KEY (qgc_work_scope_id);


--
-- TOC entry 3243 (class 2606 OID 70800)
-- Name: request_estimate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY request_estimate
    ADD CONSTRAINT request_estimate_pkey PRIMARY KEY (request_estimate_id);


--
-- TOC entry 3245 (class 2606 OID 70802)
-- Name: sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- TOC entry 3247 (class 2606 OID 70804)
-- Name: site_ins_required_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY site_ins_required
    ADD CONSTRAINT site_ins_required_pkey PRIMARY KEY (site_ins_required_id);


--
-- TOC entry 3249 (class 2606 OID 70806)
-- Name: standdown_reason_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY standdown_reason
    ADD CONSTRAINT standdown_reason_pkey PRIMARY KEY (standdown_reason_id);


--
-- TOC entry 3251 (class 2606 OID 70808)
-- Name: state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY state
    ADD CONSTRAINT state_pkey PRIMARY KEY (state_id);


--
-- TOC entry 3253 (class 2606 OID 70810)
-- Name: sub_crew_crew_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sub_crew
    ADD CONSTRAINT sub_crew_crew_name_key UNIQUE (sub_crew_name, crew_id);


--
-- TOC entry 3255 (class 2606 OID 70812)
-- Name: sub_crew_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sub_crew
    ADD CONSTRAINT sub_crew_pkey PRIMARY KEY (sub_crew_id);


--
-- TOC entry 3257 (class 2606 OID 70814)
-- Name: supervisor_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supervisor_group
    ADD CONSTRAINT supervisor_group_pkey PRIMARY KEY (supervisor_group_id);


--
-- TOC entry 3259 (class 2606 OID 70816)
-- Name: system_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY system
    ADD CONSTRAINT system_pkey PRIMARY KEY (system_id);


--
-- TOC entry 3261 (class 2606 OID 70818)
-- Name: tmp_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tmp_message
    ADD CONSTRAINT tmp_message_pkey PRIMARY KEY (tmp_message_id);


--
-- TOC entry 3263 (class 2606 OID 70820)
-- Name: upload_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY upload_type
    ADD CONSTRAINT upload_type_pkey PRIMARY KEY (upload_type_id);


--
-- TOC entry 3265 (class 2606 OID 70822)
-- Name: well_area_id_well_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY well
    ADD CONSTRAINT well_area_id_well_name_key UNIQUE (area_id, well_name, contractor_id, removed);


--
-- TOC entry 3267 (class 2606 OID 70824)
-- Name: well_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY well
    ADD CONSTRAINT well_pkey PRIMARY KEY (well_id);


--
-- TOC entry 3269 (class 2606 OID 70826)
-- Name: zip_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY zip
    ADD CONSTRAINT zip_pkey PRIMARY KEY (zip_id);


--
-- TOC entry 3241 (class 1259 OID 70827)
-- Name: cont; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cont ON request_estimate USING btree (contractor_id DESC, create_date DESC, removed DESC);


--
-- TOC entry 3171 (class 1259 OID 70828)
-- Name: contt; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contt ON calloff_order USING btree (contractor_id DESC, qgc_approve_date DESC, removed DESC);


--
-- TOC entry 3172 (class 1259 OID 70829)
-- Name: datx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX datx ON calloff_order USING btree (contractor_id, qgc_approve_date, commencement_date DESC, removed DESC);


--
-- TOC entry 3194 (class 1259 OID 70830)
-- Name: dt_dkt_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX dt_dkt_id ON email_log USING btree (dkt_date, dkt_id, receiver_id);


--
-- TOC entry 3195 (class 1259 OID 70831)
-- Name: email_log_dkt_id_dkt_date_receiver_id_receiver_type_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX email_log_dkt_id_dkt_date_receiver_id_receiver_type_idx ON email_log USING btree (dkt_id, dkt_date, receiver_id, receiver_type);


--
-- TOC entry 3212 (class 1259 OID 70832)
-- Name: employee_certificate_certificate_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX employee_certificate_certificate_id_idx ON employee_certificate USING btree (certificate_id);


--
-- TOC entry 3213 (class 1259 OID 70833)
-- Name: employee_certificate_contractor_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX employee_certificate_contractor_id_idx ON employee_certificate USING btree (contractor_id);


--
-- TOC entry 3214 (class 1259 OID 70834)
-- Name: employee_certificate_employee_id_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX employee_certificate_employee_id_idx ON employee_certificate USING btree (employee_id);


--
-- TOC entry 3162 (class 1259 OID 70835)
-- Name: fki_atw_del; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_atw_del ON atw_document USING btree (atw_id);


--
-- TOC entry 3193 (class 1259 OID 70836)
-- Name: fki_for_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_for_email ON email_attachment USING btree (email_log_id);


--
-- TOC entry 3272 (class 2620 OID 70837)
-- Name: calloff_audit; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER calloff_audit AFTER DELETE OR UPDATE ON calloff_order FOR EACH ROW EXECUTE PROCEDURE process_calloff_audit();


--
-- TOC entry 3273 (class 2620 OID 70838)
-- Name: plant_type_audit; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER plant_type_audit AFTER DELETE OR UPDATE ON plant_type FOR EACH ROW EXECUTE PROCEDURE process_plant_type_audit();


--
-- TOC entry 3270 (class 2606 OID 70839)
-- Name: atw_del; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atw_document
    ADD CONSTRAINT atw_del FOREIGN KEY (atw_id) REFERENCES atw(atw_id) ON DELETE CASCADE;


--
-- TOC entry 3271 (class 2606 OID 70844)
-- Name: for_email; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email_attachment
    ADD CONSTRAINT for_email FOREIGN KEY (email_log_id) REFERENCES email_log(email_log_id) ON DELETE CASCADE;


--
-- TOC entry 3551 (class 0 OID 0)
-- Dependencies: 8
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-08-02 11:39:33 AEST

--
-- PostgreSQL database dump complete
--

