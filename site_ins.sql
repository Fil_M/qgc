﻿-- Table: site_instruction

-- DROP TABLE site_instruction;

CREATE TABLE site_instruction
(
  site_instruction_id serial NOT NULL,
  instruction_no integer,
  instruction text,
  well_id integer,
  CONSTRAINT site_instruction_pkey PRIMARY KEY (site_instruction_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE site_instruction
  OWNER TO postgres;
