--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: plant_type; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--
DROP TABLE plant_type;
CREATE TABLE plant_type (
    plant_type_id integer NOT NULL,
    p_type character varying(64),
    removed boolean DEFAULT false
);


ALTER TABLE public.plant_type OWNER TO postgres;

--
-- Name: plant_type_plant_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plant_type_plant_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plant_type_plant_type_id_seq OWNER TO postgres;

--
-- Name: plant_type_plant_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE plant_type_plant_type_id_seq OWNED BY plant_type.plant_type_id;


--
-- Name: plant_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plant_type ALTER COLUMN plant_type_id SET DEFAULT nextval('plant_type_plant_type_id_seq'::regclass);


--
-- Data for Name: plant_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY plant_type (plant_type_id, p_type, removed) FROM stdin;
1	Horizontal Grinder - Morbark 4600	f
2	Horizontal Grinder - Morbark 3800	f
3	Dozer - D6R / D6T	f
4	Dozer - D7R / D85	f
5	Dozer - D8R / D8T	f
6	Excavator – 21t to 24t	f
7	Excavator – 30t	f
8	Single Side Tipper	f
9	Front End Loader - 966	f
10	Grader - 140	f
11	Body Truck	f
12	Roller	f
13	44000lt B-double tank trailer	f
15	Volvo Pump Truck water tank	f
14	Accommodation	f
16	825 Compactor with Trimble GPS Laser	f
17	Ancillaries	f
18	33000lt water tanker	f
19	Prime Mover and Quad Float	f
20	Site Supervisor	f
21	Bobcat	f
\.


--
-- Name: plant_type_plant_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('plant_type_plant_type_id_seq', 21, true);


--
-- Name: plant_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plant_type
    ADD CONSTRAINT plant_type_pkey PRIMARY KEY (plant_type_id);


--
-- PostgreSQL database dump complete
--

