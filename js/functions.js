function isNumber(n) {
  return !isNaN(n) && isFinite(n);
}

function isNumeric(sText) {
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}
function productSelect(arr) {
   var rt = $('#rate_pickup');
   rt.val(arr[1]);
 dv = $('#product');
   $.post("products.php",'supplier_id='+ arr[0],
      function(data) {
         $(dv).html(data);
      }
      ,"html");
}
function addressFill(id,ty) {
   $.post("autocomplete.php","type=" + ty + "&query="+  id ,
      function(data) {
			if (ty == "destform" ) {
         	$('#destline1').val(data['line_1']);
         	$('#destline2').val(data['line_2']);
			} 
			 else if (ty == "millform" ) {
         	$('#milline1').val(data['line_1']);
         	$('#milline2').val(data['line_2']);
			}   
      }
      ,"json");
}

function calcnett(gr,tr,nt,trd) {
   var ntt;
	$(nt).val('');
	var grs = $(gr).val();
	var tre = $(tr).val();
	if (! isNumber(grs)) {
		return false;
	}
	if (! isNumber(tre)) {
		return false;
	}
   $(trd).val(tre);
	ntt = grs - tre;
	if (ntt > 0 ) { 
		$(nt).val(ntt);
	}
}
function valNumber(numID,txt) {
  	var strn = $(numID); 
	var stvn = strn.val();
	if (stvn.length > 0 ) {
	 	if (! isNumber(stvn) ) {alert ('Please enter a valid number for ' + txt);strn.val('');strn.focus();return false;};  
   }

}
function calcit(stID,endID,totID) {
  	var st = $(stID).val(); 
  	var en = $(endID).val(); 
	var tot = $(totID);
	if (en.length > 0 && st.length > 0 ) {
	 	var diff = new Date( '01/01/2012 ' + en ) - new Date( '01/01/2012 ' + st );
		if (diff < 0) {return false;};
		var vhourDiff = String(Math.floor(diff/1000/60/60));  // in hours
   	diff -= vhourDiff*1000*60*60;
      var vminDiff = String(Math.floor(diff/1000/60)); // in minutes
		
    	if (vhourDiff.length == 1){vhourDiff = '0' + vhourDiff};
      if (vminDiff.length == 1){ vminDiff = '0' + vminDiff};
		tot.val(vhourDiff + ':' + vminDiff) ;
	}
}
function calcKM(strID,endrID,totrID,txt) {
  	var str = $(strID); 
	var stv = str.val();
	if (stv.length < 1 ) {
		return false;
   }

	 	//if (! isNumber(stv) ) {alert ('Please enter a valid number for ' + txt);str.val('');str.focus();return false;};  
  	var enr = $(endrID); 
	var env = enr.val();
	if (env.length  < 1 ) {
		return false;
   }
 		//if (! isNumber(env) ) {alert ('Please enter a valid number for '+ txt);enr.val('');enr.focus();return false;}; 
		//return false;

	var totr = $(totrID);
  	if (env - stv < 0 ) {totr.val('');return false;};

	totr.val(env - stv);
	   
}
// Risk Matrix accepts proability A-E  consequence 1-5 and returns span of risk ranking
function matrix(prob,con) {
   var ar = new Array();
   	ar["A"] = new Array(0,1,2,3,4,5);
   	ar["A"][1] = new Array("red","E1","white");
   	ar["A"][2] = new Array("red","E3","white");
   	ar["A"][3] = new Array("red","E6","white");
   	ar["A"][4] = new Array("orange","H10","white");
   	ar["A"][5] = new Array("orange","H15","white");
   	ar["B"] = new Array(0,1,2,3,4,5);
   	ar["B"][1] = new Array("red","E2","white");
   	ar["B"][2] = new Array("red","E5","white");
   	ar["B"][3] = new Array("orange","H9","white");
   	ar["B"][4] = new Array("orange","H14","white");
   	ar["B"][5] = new Array("yellow","M19","black");
   	ar["C"] = new Array(0,1,2,3,4,5);
   	ar["C"][1] = new Array("red","E4","white");
   	ar["C"][2] = new Array("red","E8","white");
   	ar["C"][3] = new Array("orange","H13","white");
   	ar["C"][4] = new Array("yellow","M18","black");
   	ar["C"][5] = new Array("green","L22","white");
   	ar["D"] = new Array(0,1,2,3,4,5);
   	ar["D"][1] = new Array("red","E7","white");
   	ar["D"][2] = new Array("orange","H12","white");
   	ar["D"][3] = new Array("yellow","M17","black");
   	ar["D"][4] = new Array("green","L21","white");
   	ar["D"][5] = new Array("green","L24","white");
   	ar["E"] = new Array(0,1,2,3,4,5);
   	ar["E"][1] = new Array("orange","H11","white");
   	ar["E"][2] = new Array("yellow","M16","black");
   	ar["E"][3] = new Array("yellow","M20","black");
   	ar["E"][4] = new Array("green","L23","white");
   	ar["E"][5] = new Array("green","L25","white");

		var bgrd = ar[prob][con][0];
		var fgrd = ar[prob][con][2];
		var rank = ar[prob][con][1];

		return '<input  READONLY class="rankLab"  name="rank[]" style="color:' + fgrd + ';background:' + bgrd + ';" value="' + rank + '" />';
}
function addload() {
  	var loaddiv = $('.lineload-div'); 
  	var cl = parseInt($("input#lineload:last").val()); 
	var tareval='';
	var prevtare = $('#tare'+cl).val();
	/*var prevdtare = $('#tared'+cl).val();  */
	if (isNumeric(prevtare)) {
		tareval = prevtare;
		taredval = prevtare;
	}v class="quanDiv LH" >Gross</div><div class="quanDiv H" >Tare</div>
      <div class="quanDiv H" >Nett</div><div class="quanDiv H" >Volume</div><div class="quanDiv H" >Hours</div>
      <div class="quanDiv LH" >Gross</div><div class="quanDiv H" >Tare</div>
      <div class="quanDiv H" >Nett</div><div class="quanDiv H" >Volume</div><div class="quanDiv H" >Hours</div>
 
	cl +=1;
    var loadtxt = '<div class="numDiv" >Load</div><div class="numDiv" ><input id="lineload" value="' + cl + '" READONLY /></div>';
		loadtxt += '<div class="photoDiv">Weighbridge Docket</div><div class="photoDiv"><input type="file" name="dkt[]" class="photoInput" /></div>';
		loadtxt += '<label class="lab wd" >Release No</label><input type="text"  name="releaseno[]" class="input sml" >';
		loadtxt += '<div style="clear:both;"></div>';
		loadtxt += '<div class="quanDiv LH" >Gross</div><div class="quanDiv H" >Tare</div>';
      loadtxt += '<div class="quanDiv H" >Nett</div><div class="quanDiv H" >Volume</div><div class="quanDiv H" >Hours</div>';
      loadtxt += '<div class="quanDiv LH" >Gross</div><div class="quanDiv H" >Tare</div>';
      loadtxt += '<div class="quanDiv H" >Nett</div><div class="quanDiv H" >Volume</div><div class="quanDiv H" >Hours</div>';
		loadtxt += '<div class="quanDiv L" ><input type="tel" class="input num no" id="gross' + cl + '" name="gross[]" onblur="calcnett(' + "'#gross" + cl + "','#tare" + cl + "','#nett" + cl + "'" + '); "/></div>';
		loadtxt += '<div class="quanDiv" ><input type="tel" class="input num no" value="'+tareval+ '" id="tare' + cl + '" name="tare[]" onblur="calcnett(' + "'#gross" + cl + "','#tare" + cl + "','#nett" + cl + "'" + '); "/></div>';
      loadtxt += '<div class="quanDiv" ><input type="tel" class="input num no" READONLY id="nett' + cl + '"/></div>';
      loadtxt += '<div class="quanDiv" ><input type="tel" class="input num no" name="volume[]" /></div>';
      loadtxt += '<div class="quanDiv" ><input type="text" name="hours[]" class="input sml no" value="00:00" id="hours' + cl + '"/></div>';
		loadtxt += '<div class="quanDiv L" ><input type="tel" class="input num no" id="grossd' + cl + '" name="gross[]" onblur="calcnett(' + "'#grossd" + cl + "','#tare" + cl + "','#nettd" + cl + "'" + '); "/></div>';
		loadtxt += '<div class="quanDiv" ><input type="tel" class="input num no" value="'+taredval+ '" id="tared' + cl + '" name="tare[]" onblur="calcnett(' + "'#grossd" + cl + "','#tare" + cl + "','#nettd" + cl + "'" + '); "/></div>';
      loadtxt += '<div class="quanDiv" ><input type="tel" class="input num no" READONLY id="nettd' + cl + '"/></div>';
      loadtxt += '<div class="quanDiv" ><input type="tel" class="input num no" name="volumed[]" /></div>';
      loadtxt += '<div class="quanDiv" ><input type="text" name="hours[]" class="input sml no" value="00:00" id="hoursd' + cl + '"/></div>';
		loadtxt += "<script> $('#hours" + cl +"').scroller({preset: 'time',timeFormat: 'HH:ii',timeWheels: 'HHii',stepMinute: 15,mode: 'mixed'});jQuery('.input.num').keyup(function () { this.value = this.value.replace(/[^0-9\.]/g,''); }); </script>";
		loadtxt += '<div style="clear:both;height:15px;" > &nbsp;</div>';
	loaddiv.append(loadtxt);
}

function togglewheel(el) {
  var e = $(el);
  if (e.val() == '' ) {
    e.val('X');
  }
  else if (e.val() == 'X') {
    e.val('');
  }

}

function toggle(el) {
  var e = $(el);

  if (e.val() == '' ) {
    e.val('\u2714');
  }
  else if (e.val() == '\u2714' ) {
    e.val('X');
  }
  else if (e.val() == 'X') {
    e.val('');
  }

}

function dSel(el) {
   var e = $(el);
      e.prop("checked",false)
      /* e.removeClass('checkbox:checked + label:after').addClass('checkbox:checked + label');  */
}

/**
 * Only Signee name is required. Parameters should be passed in as an object. See 'Example Usage' for usage
 * @param  String    signee         required      Name of person signing the form (displayed on sign button and used in element ids)
 * @param  String    signButton     optional      Id of button to open signature pad
 * @param  String    buttonClass    optional      css class for button to open signature pad 
 * @param  String    canvasId       optional      Id of html 5 canvas
 * @param  String    outputId       optional      Id output element
 * @param  String    date           optional      Current Date
 * @param  Boolean   nameRequired   optional      Is a name also required
 * @param  Boolean   dateRequired   optional      Is a date required
 * @param  Boolean   geolocation    optional      Is geolocation required
 * @param  String    clearSelector  optional      jQuery selector for clear element
 * @param  String    closeSelector  optional      jQuery selector for close element
 * @param  String    acceptSelector optional      jQuery selector for signature accept element
 * @param  String    html           do not use    Used internally for storing html of main div
 * @param  String    height         optional      Must match height used in PHP signature rendering code
 * @param  String    width          optional      Must match width used in PHP signature rendering code
 *
 * Written on 19/03/2013 by James Glover
 *
 * NOTES: 
 * Should be called within document dom ready function.
 * Until jQuery clone is fixed, for textareas to be copied properly, jquery.clone.fix needs to be included 
 * (https://raw.github.com/spencertipping/jquery.fix.clone/master/jquery.fix.clone.js)
 * True as at jQuery 1.9.0
 *
 * EXAMPLE USAGE:
 * <script type=\"text/javascript\">
 *    $(function(){ 
 *      Signature.init({signee: 'Driver'});
 *    });
 * </script> 
 * 
 **/

var s;
var Signature = {  
  settings: {
    signee:           "",
    signButton:       "sign",
    buttonClass:      "clean-grey",    
    canvasId:         "canvas",
    outputId:         "Output",
    date:             "", 
    nameRequired:     false,
    dateRequired:     false,
    geolocation:      true,
    clearSelector:    "[data-role='clearSignature']",
    closeSelector:    "[data-role='closeSignature']",
    acceptSelector:   "[data-role='acceptSignature']",
    html:             "",
    height:           400,
    width:            748
  },
  
  getDate: function() {
    var date = new Date();
    var m = ( (date.getMonth() + 1) > 10) ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1);
    var d = (date.getDate() > 10) ? date.getDate() : "0" + date.getDate();  
    var y = date.getFullYear();
    date = d + '/' + m + '/' + y;
    return date;
  },

  init: function(options) {
    this.settings = $.extend(this.settings, options);
    s = this.settings;

    $('<button>' + s.signee + ' Sign</button>')
      .attr({
        'id': s.signee + 'Sign',
        'name': s.signee + 'Sign'
      })      
      .css({
        'margin-bottom': '10px',
        'height': '50px'
      })
      .addClass('clean-grey')
      .data('role', 'ShowSignature')      
      .insertAfter('#main form');
      //.insertBefore('#main form button:last-child');
    if(s.geolocation) {
      //$('#'+s.signee+'Sign').attr('disabled', 'disabled');
      $.getScript("js/geoPosition.js");        
    }
    this.createSignature();    
  },

  createSignature: function() {
    $("#" + s.signee + "Sign").on("click", function(event) {  
      event.preventDefault();
      s.html = $('#main').clone(); // copy main content
      // canvas height and width must be set via html attributes, css will just adjust scale
      $("#" + s.signee +  s.canvasId).attr("width", s.width + "px");
      $("#" + s.signee +  s.canvasId).attr("height", s.height + "px");
      $('#main').html(
        "<div id=\"" + s.signee + "Wrapper\" style=\"width: "+ s.width +"px; height: "+ s.height +"px;\">"+
          "<canvas id=\"#"+ s.signee + s.canvasId +"\" class=\"signatureCanvas\" width=\"" + s.width + "\" height=\"" + s.height + "\"></canvas>"+
          "<input type=\"hidden\" id=\""+ s.signee + s.outputId +"\" name=\""+ s.signee + s.outputId +"\" class=\"signatureOutput\">"+
        "</div>"+
        "<hr style='margin-bottom: 10px;'/>"+
        "<button id='"+ s.signee +"ClearSignature' data-role='clearSignature' class='left "+ s.buttonClass +"'>Clear</button>"+
        "<button id='"+ s.signee +"AcceptSignature' data-role='acceptSignature' class='right "+ s.buttonClass +"'>Sign</button>"
      );
      $("#main").signaturePad({ 
        drawOnly: true, 
        errorMessageDraw: 'Please sign', 
        clear: s.clearSelector, 
        lineColour: '#FFF', 
        output: ('#' + s.signee + 'Output') 
      });    

      $(s.acceptSelector).on('click', function(event) {
        event.preventDefault();  
        /*
          if(geoPosition.init()){  // Geolocation Initialisation
          geoPosition.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
        } else {
          alert("Geolocation is not available on this device, unable to get GPS position");
        }
        function success_callback(p){
          console.log(p.latitude);
          console.log(p.longitude);
          $('#'+s.signee+'Sign').removeAttr('disabled');
        }
        function error_callback(p){
          console.log(p.message);
        }
        */
        var output = $("#" + s.signee + s.outputId).clone();
        $('#main').replaceWith(s.html);
        $('#main form').append(output);
        $('#'+s.signee+'Sign').css('box-shadow', 'inset 0 0 2px 0 #33CC33');
        $('#'+s.signee+'Sign').css({'background' : 'url("../images/button-tick.png") no-repeat top right', 'background-size' : '25px', 'background-position' : '114px 10px'});
      });
    });
  }
};
function makeSignature(id,output, date) {
    console.log(date)
    var signDiv = $(id);
    signDiv
      .css({
        float: "left"
      , width: "100%"
      })
      .html("<button class=\"sigOpen button\" style='margin:10px'> Sign </button>"
		+ "<button class='button' type='submit' id='submit_rsa' name='SUBMIT' style='float:right;margin: 10px'> Submit </button>"
        + "<div id=\"sign\" class=\"sigPad\" style=\"position: absolute; top: 0; left: 0;   background: white;z-index:100000\">"
            + "<div class=\"sig sigWrapper\" style=\"float: left; width:748px; height:400px;\">"
                + "<canvas width=\"748\" height=\"400\" ></canvas>"
                + "<input type=\"hidden\" name='"+output+"' class='"+output+"'>"
            + "</div> "
			+ "<div style='width:748px;margin-right: 10px;display: inline-block;'>"
            +	"<div class=\"clearDiv\" style=\"line-height: 50px;\"> <a class=\"clearButton\" href=\"#clear\" style=\"color: #fff;text-decoration: none;\">Clear</a></div>"
            +	"<div class=\"button close\" style=\"float: right; margin-left: 30px;\">Close </div>"
            +	"<div id=\"div_sign_date\" style=\"float: left;\">Date:<input type=\"text\" name=\"d_sign_date\"  class=\"inputRight\" value='"+date+"' readonly  size=\"9\" /></div> "
			+ "</div>"
        + "</div>"
      );
    var signPad = signDiv.children(".sigPad").css({
            top: 0
          , left: 0
          , display: "block"
        }).hide();
    
    signPad.find(".close").click(function() {
        signPad.hide();
		$("body,html").animate({scrollTop: $("#current_top").val()},0);
		$("#current_top").remove();
    }).css({
        cursor: "pointer"
    })
    
	$(".inputRight").css({"margin-top":"12px","margin-left":"12px","font-size":"100%","float":" right","height":"38px"});
	$("#div_sign_date").css({"float":"right","line-height":"65px"});
	$(".clearDiv").css({"width":"auto","display":"inline-block","margin-top":"6px"});
	$(".clearButton").css({"padding":"10px"});
    var ua = navigator.userAgent,
    event = (ua.match(/iPad/i)) ? "touchstart" : "click";

    signDiv.children(".sigOpen").click(function() {
        signPad.height($("#main").height()).show();
		$('<input type="hidden" id="current_top" value="'+$(window).scrollTop()+'">').appendTo($('#wrap'));
		$("body,html").animate({scrollTop: 0},0);
        return false;
    });
}
