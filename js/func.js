function makeSignature(id,output, date) {
    console.log(date)
    var signDiv = $(id);
    signDiv
      .css({
        float: "left"
      , width: "100%"
      })
      .html("<button class=\"sigOpen button\" style='margin:10px'> Sign </button>"
		+ "<button class='button' type='submit' id='submit_rsa' name='SUBMIT' style='float:right;margin: 10px'> Submit </button>"
        + "<div id=\"sign\" class=\"sigPad\" style=\"position: absolute; top: 0; left: 0; width: 100%;  background: white;z-index:100000\">"
            + "<div class=\"sig sigWrapper\" style=\"float: left; width:1198px; height:400px;\">"
                + "<canvas width=\"1198\" height=\"400\" ></canvas>"
                + "<input type=\"hidden\" name='"+output+"' class='"+output+"'>"
            + "</div> "
			+ "<div style='width:1180px;margin:0px 10px;'>"
            +	"<div class=\"clearDiv\" style=\"line-height: 50px; padding-right: 30px;\"> <a class=\"clearButton\" href=\"#clear\" style='color:#fff'>Clear</a></div>"
            +	"<div class=\"button close\" style=\"float: right; margin-left: 30px;\">Close </div>"
            +	"<div id=\"div_sign_date\" style=\"float: left;\">Date:<input type=\"text\" name=\"d_sign_date\"  class=\"inputRight\" value='"+date+"' readonly  size=\"9\" /></div> "
			+ "</div>"
        + "</div>"
      );
    var signPad = signDiv.children(".sigPad").css({
            top: 0
          , left: 0
          , display: "block"
        }).hide();
    
    signPad.find(".close").click(function() {
        signPad.hide();
		$("body,html").animate({scrollTop: $("#current_top").val()},0);
		$("#current_top").remove();
    }).css({
        cursor: "pointer"
    })
    
	$(".inputRight").css({"margin-top":"12px","margin-left":"12px","font-size":"100%","float":" right","height":"38px"});
	$("#div_sign_date").css({"float":"right","line-height":"65px"});
	$(".clearDiv").css({"width":"auto","display":"inline-block","margin-top":"6px"});
	$(".clearButton").css({"padding":"10px"});
    var ua = navigator.userAgent,
    event = (ua.match(/iPad/i)) ? "touchstart" : "click";

    signDiv.children(".sigOpen").click(function() {
        signPad.height($("#main").height()).show();
		$('<input type="hidden" id="current_top" value="'+$(window).scrollTop()+'">').appendTo($('#wrap'));
		$("body,html").animate({scrollTop: 0},0);
        return false;
    });
}

