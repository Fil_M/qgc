https://qgcdesign.qgc-app.net.au

Queensland Gas Corporation subsidary of Shell. Coal Seam Gas exploration in Queensland. Road building app to monitor and pay hours of machnery use and related expenses booked by multiple
Road building Contractors with all associated work. Including Fencing etc. This is a Distributed Database App. Single part of a Double App.

The Contractors have their own separate App and Database to log the hours/expenses which are inter-connected with the QGC database.  See qgc_contractor