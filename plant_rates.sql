﻿DROP FUNCTION plant_rates(text);

CREATE OR REPLACE FUNCTION plant_rates(IN query text,IN dat date)
  RETURNS TABLE(plant_id integer,plant_unit text,plant_name text,plant_rate double precision) AS
$BODY$
with data as
(SELECT plant_id,plant_unit,plant_name,plant_rate as rate from plant where upper(plant_unit) like $1||'%' and removed is false 
UNION
SELECT plant_id,plant_unit,plant_name,new_rate as rate from plant where upper(plant_unit) like $1||'%' and removed is false and effective_from <= $2
order by plant_unit)
select plant_id,plant_unit,plant_name,max(rate) from data group by plant_id,plant_unit,plant_name;
$BODY$

  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
