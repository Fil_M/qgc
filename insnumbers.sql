﻿-- Function: wells(integer)

-- DROP FUNCTION wells(integer);

CREATE OR REPLACE FUNCTION insnumbers(wellid integer)
  RETURNS SETOF text AS
$BODY$
select array_to_string(array(select instruction_no from site_instruction where well_id = $1  order by instruction_no),', ','*') as nums;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION insnumbers(integer)
  OWNER TO postgres;
