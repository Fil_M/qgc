<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("include/boot.php");
 $query=$_REQUEST['query'];
 $type=$_REQUEST['type'];

 	switch ($type) {
 	case "operator" :
		$ar = getEmployee($query);
		break;
 	case "unit" :
		$dat = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');
		$ar = getUnit($query,$dat);
		break;
 	case "area" :
		$ar = getArea($query);
		break;
 	case "plant" :
		$ar = getPlant($query);
		break;
 	case "othertxt" :
		$ar = otherTxt($query);
		break;
 	case "sitecond" :
		$ar = siteCond($query);
		break;
	case "planttype" :
      $ar = getPlantTypes($query);
      break;
	case "coo" :
      $ar = getCoos($query);
      break;

   }
 echo json_encode($ar);

function otherTxt($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT other_text , substring(other_text from 0 for 30) as txt from request_estimate where upper(other_text) like '$q%' and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['txt'];
      $dat[] = $val['other_text'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function siteCond($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT site_conditions , substring(site_conditions from 0 for 30) as txt from request_estimate where upper(site_conditions) like '$q%' and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['txt'];
      $dat[] = $val['site_conditions'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getEmployee($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT employee_id, coalesce(firstname,'') || ' ' || lastname as name from employee where upper(lastname)  like '$q%' and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['name'];
      $dat[] = $val['employee_id'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getUnit($query,$dat) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT * from plant_rates('$q','$dat')";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['plant_unit'];
      $dat[] =  array($val['plant_id'],$val['plant_name'],$val['plant_rate'],$val['stand_rate']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}

function getPlant($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT distinct(plant_name) as plant_name,plant_id from plant where  upper(plant_name) like '$q%' and removed is false";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['plant_name'];
      $dat[] = $val['plant_id'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getPlantTypes($query) {
   global $conn;
   global $conID;
   $q= strtoupper($query);
   $today = date('Y-m-d');
   $sql = "SELECT max(plant_type_id) ,p_type from plant_type where removed is false and upper(p_type) like '$q%' group by p_type";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['p_type'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions);
 return $ar;
}
function getCoos($query) {
   global $conn;
	$sql = "select co.calloff_order_id,co.contractor_id,a.area_name,w.well_name from calloff_order  co
	LEFT JOIN area a  on a.area_id = co.area_id
	LEFT JOIN well w  on w.well_id =  co.well_id
	where calloff_order_id::text like '$query%' order by calloff_order_id";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['calloff_order_id'];
		$dat[] = array($val['contractor_id'],$val['area_name'],$val['well_name']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}

function getArea($query) {
   global $conn;
	$q= strtoupper($query);
	$sql = "SELECT area_id,area_name from area where upper(area_name) like '$q%' and removed is false order by area_name";

	if (! $data = $conn->getAll($sql)) {
		if ($conn->ErrorNo()  != 0 ) {
			die($conn->ErrorMsg());
		}
	}
	$suggestions = array();
	$dat = array();
	foreach($data as $key=>$val) {
		$suggestions[] =  $val['area_name'];
		$dat[] = array($val['area_id']);
	}
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar; 
}  


?>
