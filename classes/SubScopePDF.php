<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class SubScopePDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
	public $areaName;
	public $wellName;
	public $crewName;
	public $contractorName;
	public $origEst;
	public $total;
	public $callOffID;
	public $hDate;
	public $dLink;
	public $shortName;
	public $conName;
	public $subData;


	public function __construct($action="print",$conID,$dat) {
		$this->conn = $GLOBALS['conn'];
		$this->conID = $conID;
		$this->hDate = $dat;
		$this->action = $action;
		$this->empID = $_SESSION['employee_id'];
		$dbarr=unserialize($_SESSION['dbarr']);
   	$this->dbLink = $dbarr[$conID][0];
   	$this->shortName = $dbarr[$conID][2];
   	$this->conName =   $dbarr[$conID][3];




		$this->getSubDetails();
      //exit;
		// create new PDF document
		$pdf = new MYPDFLAND("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Sub Scope Percentages as of $dat");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$Y = 29;
      $pdf->SetFont(PDF_FONT, '', 10);
		$Y = 16;
		$prevCoo = -1;
		foreach ($this->subData as $key=>$val) {
		 if ($Y > 145 ) {
            $pdf->SetFont(PDF_FONT, 'I', 11);
				$Y += 9;
            $pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
            $pdf->AddPage();
				//$this->headingTitle($Y,$pdf,"",true);
				$this->heading($pdf);
				$pdf->SetFont(PDF_FONT, '', 10);
				$Y=16;
				//$this->headingTitle($Y,$pdf,"",true);

       }
			if ($prevCoo != $val[0]) {
            $Y+=13.5;
				$this->headingTitle($Y,$pdf,"",true);
				$Y+=9.5;
         	$pdf->MultiCell(15,0,'C.O.O.',0,'L',false,0,1,$Y);
				$pdf->SetTextColor(0,0,255);
         	$pdf->MultiCell(64,0,$val[0],0,'L',false,0,16,$Y);
				$pdf->SetTextColor(0,0,0);
         	$pdf->MultiCell(15,0,'Area',0,'L',false,0,1,$Y +4.5);
				$pdf->SetTextColor(0,0,255);
         	$pdf->MultiCell(64,0,$val[7],0,'L',false,0,16,$Y +4.5);
				$pdf->SetTextColor(0,0,0);
         	$pdf->MultiCell(15,0,'Well/s',0,'L',false,0,1,$Y +9);
				$pdf->SetTextColor(0,0,255);
         	$pdf->MultiCell(64,0,$val[8],0,'L',false,0,16,$Y +9);
				$pdf->SetTextColor(0,0,0);

				$prevCoo = $val[0];

			}
			$sheight = ceil(strlen($val[2]) /19) * 4.65;
         $sheight = $sheight >= 4.5 ? $sheight : 4.65;

			$height = ceil(strlen($val[1]) /19) * 4.65;
         $height = $height >= 4.5 ? $height : 4.65;
  
			$height = $height >= $sheight ? $height : $sheight;

			if ($val[4] > 80 ) {
				 $pdf->SetFillColor(244,68,68);


         	$pdf->MultiCell(21,$height,$this->hDate,array('LTB'=>$this->borderStyle),'L',true,0,80,$Y);
         	$pdf->MultiCell(45,$height,$val[1],array('LTB'=>$this->borderStyle),'L',true,0,101,$Y);
         	$pdf->MultiCell(45,$height,$val[2],array('LTB'=>$this->borderStyle),'L',true,0,146,$Y);
         	$pdf->MultiCell(25,$height,$val[3],array('LTB'=>$this->borderStyle),'C',true,0,191,$Y);
         	$pdf->MultiCell(25,$height,$val[4]."%",array('LTB'=>$this->borderStyle),'C',true,0,216,$Y);
         	$pdf->MultiCell(25,$height,$val[5],array('LTB'=>$this->borderStyle),'R',true,0,241,$Y);
         	$pdf->MultiCell(30,$height,$val[6],array('LTRB'=>$this->borderStyle),'R',true,1,266,$Y);

			}
			else {
         	$pdf->MultiCell(21,$height,$this->hDate,array('LTB'=>$this->borderStyle),'L',false,0,80,$Y);
         	$pdf->MultiCell(45,$height,$val[1],array('LTB'=>$this->borderStyle),'L',false,0,101,$Y);
         	$pdf->MultiCell(45,$height,$val[2],array('LTB'=>$this->borderStyle),'L',false,0,146,$Y);
         	$pdf->MultiCell(25,$height,$val[3],array('LTB'=>$this->borderStyle),'C',false,0,191,$Y);
         	$pdf->MultiCell(25,$height,$val[4]."%",array('LTB'=>$this->borderStyle),'C',false,0,216,$Y);
         	$pdf->MultiCell(25,$height,$val[5],array('LTB'=>$this->borderStyle),'R',false,0,241,$Y);
         	$pdf->MultiCell(30,$height,$val[6],array('LTRB'=>$this->borderStyle),'R',false,1,266,$Y);
			}

		 $Y+=$height;
		 /*if ($Y > 155 ) {
            $pdf->SetFont(PDF_FONT, 'I', 11);
				$Y += 9;
            $pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
            $pdf->AddPage();
				$this->headingTitle($Y,$pdf,"",true);
				$Y=18;
       }  */


		}

		$name= "tmp/subscope_percentage_{$this->hDate}_{$this->conID}_{$this->empID}.pdf";

      if ($action == "print" ) {
         $pdf->Output($name, 'I');
      }
      else {  
         $dat = date('d-m-Y');
         $pdf->Output($name, 'F');
      }

   }		
	 private function getSubDetails() {
   	$sql = "SELECT  distinct(calloff_order_id),area_name,wells_from_ids(co.well_ids) as well_name  from {$this->shortName}_hour
   	LEFT JOIN area a using (area_id) 
   	LEFT JOIN calloff_order co using(calloff_order_id)
   	where hour_date = '$this->hDate'
   	order by calloff_order_id";


   	if (!$data = $this->conn->getAll($sql)) {
      	if ($this->conn->ErrorNo() != 0 ) {
         	die($this->conn->ErrorMsg());
      	}
   	}
		foreach($data as $key=>$val) {
         extract($val);
         $sql = "SELECT * from percent_est($calloff_order_id,'$this->hDate',true)";
         $sql = pg_escape_string($sql);
         $stmt = "SELECT * from dblink('$this->dbLink','$sql',true) AS t1(crew_name text,sub_crew_name text,tce_est double precision,subtot_to_date double precision,percentage_complete double precision)" ;

         if (! $subs = $this->conn->getAll($stmt)) {
            if($this->conn->ErrorNo() != 0) {
               echo $stmt;
               die($this->conn->ErrorMsg());
            }
         }
         foreach($subs as $k=>$v) {
            extract($v);
            $perc= $percentage_complete . "%";
            $tcePerc = round($subtot_to_date / $tce_est,2) * 100;
            $subtot_to_date = number_format($subtot_to_date);
            $tce_est = number_format($tce_est);
				$this->subData[] = array($calloff_order_id,$crew_name,$sub_crew_name,$perc,$tcePerc,$subtot_to_date,$tce_est,$area_name,$well_name);

         }
      }


    }

	 private function heading($pdf){
      // Heading Title 
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(230, 0, "Sub Scope Percentages for $this->conName", 0, 'C', false, 2, 20,3);
      $pdf->SetFont(PDF_FONT, 'B', 12);
      $pdf->MultiCell(230, 0, "For Date: $this->hDate", 0, 'C', false, 2, 20,9);
   }

	private function headingTitle($Y,$pdf,$title,$showHeadings=true) {
      if ($showHeadings) {
         $pdf->SetFillColor(0,150,200);
         $pdf->SetTextColor(0,0,0);
         $pdf->SetFont(PDF_FONT, '', 11);
         $pdf->SetFont(PDF_FONT, '', 11);
         //$pdf->MultiCell(21,0,"$table",array('LTBR'=>$this->borderStyle),'L',true,1,4,$Y);
         //$Y += 5;
// $pdf->MultiCell(90, 8, "$crew" , 0, 'L', true, 0, 2,36,true,0,false,true,8,'M');

         $pdf->SetFont(PDF_FONT, '', 10);
         $pdf->MultiCell(79,9,'C.O.O. / Area / Wells',array('LTB'=>$this->borderStyle),'C',true,0,1,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(21,9,'Date',array('LTB'=>$this->borderStyle),'C',true,0,80,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(45,9,'Work Scope',array('LTB'=>$this->borderStyle),'C',true,0,101,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(45,9,'Sub Scope',array('LTB'=>$this->borderStyle),'C',true,0,146,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(25,9,'Percentage Complete',array('LTB'=>$this->borderStyle),'C',true,0,191,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(25,9,'Percentage of TCE',array('LTB'=>$this->borderStyle),'C',true,0,216,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(25,9,'Total to Date',array('LTB'=>$this->borderStyle),'C',true,0,241,$Y,true,0,false,true,9,'M');
         $pdf->MultiCell(30,9,'Original Estimate',array('LTB'=>$this->borderStyle),'C',true,0,266,$Y,true,0,false,true,9,'M');

      }

   }




}
?>
