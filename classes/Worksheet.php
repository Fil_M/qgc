<?php
 	class Worksheet {
 		public $ID;
 		public $checklistID;
		public $conn;
		public $page;
		public $pageType	= 'worksheet';
		public $questions 	= array();  // db array of same
		public $headings 	= array();  // db array of same
		public $employeeID;
		public $vehicleID;
		public $driverName;
		public $tDate;
		public $finishTimeRequired = false;
		public $unit;
		public $startTime;
		public $endTime;
		public $hourStart;
		public $kmStart;

		public function	__construct($ID = 0,$checklistID=0) {
			$this->conn = $GLOBALS['conn'];
			$this->tDate = date('d-m-Y');
			$this->checklistID=$checklistID;
			$this->ID=$ID;
		 	if (!isset($_POST['SUBMIT'] )) {
       		$this->driverName = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];	         	
       		$this->employeeID = $_SESSION['employee_id'];
				$pg = new Page('worksheet');
				$this->page = $pg->page;
				if($this->ID> 0) {
					$this->getWorksheetDetails();
				}  
		 		$this->setHeaderText();	
		 		$this->setContent();	
				echo $this->page;
			}
			else {
				$this->submitForm();
			}
		}

		/**
		 * Sets header text for page
		 */
		private function setHeaderText() {
			$header_text = "Pre-Shift worksheet";
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function getWorksheetDetails() {
			$sql = "SELECT w.*,v.unit_no from worksheet w LEFT JOIN vehicle v using (vehicle_id) where worksheet_id = $this->ID";
			if (! $data = $this->conn->getRow($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			if (count($data) > 0 ) {
				$this->tDate = $data['worksheet_date'];
				$this->startTime = substr($data['driver_start'],0,5);
				$this->endTime = substr($data['driver_finish'],0,5);
				$this->unit = $data['unit_no'];
				$this->kmStart = $data['km_start'];
				$this->hourStart = $data['truck_hour_start'];
				$this->vehicleID = $data['vehicle_id'];
			}
		}
		private function setContent() {	
		$content = <<<FIN
			<div style="width:100%;float:left;height:10px ! important;"> &nbsp;</div>
      	<form name="worksheet" action="worksheet.php" method="POST" enctype="multipart/form-data">
			<input type="hidden" id="checklist_id" name="checklistID" value="$this->checklistID"/>
			<input type="hidden" id="worksheet_id" name="ID" value="$this->ID"/>
			<input type="hidden" id="employee_id" name="employeeID" value="$this->employeeID"/>
			<input type="hidden" id="vehicle_id" name="vehicleID" value="$this->vehicleID"/>
				<div class="heading" >Driver</div>
				<div class="cell60"><label class="lab wd" >Name:</label><input type="text" id="drivername" name="drivername" class="input driver" value="$this->driverName"></div>
				<div class="cell40"><label class="lab wd" >Date: </label><input type="text" id="worksheet_date" class="input sml" name="worksheet_date" value="$this->tDate"></div>
				<div class="heading" >Unit Details</div>
				<div class="cell60"><label class="lab wd" >Unit</label><input type="text" id="unit" name="unit" class="input time" value="$this->unit" ></div>
				<div class="cell40"></div>
				<div class="cell60"><label class="lab wd" >Truck Km start: </label><input type="tel" id="km_start" name="km_start" class="input num" value="$this->kmStart" ></div>
				<div class="cell40"><label class="lab wd" >Truck Hrs start: </label><input type="tel" id="truck_start" name="truck_start" class="input num"  value="$this->hourStart"></div>
				<div class="cell60"><label class="lab wd" >Start Time:</label><input type="text" id="startTime" name="startTime" class="input time" value="$this->startTime"></div>
				<div class="cell40"><label class="lab wd" >Finish Time:</label><input type="text" id="endTime" name="endTime" class="input time"  value="$this->endTime"></div>
			<button class="button" style="float: left; width:200px; margin-left: 280px;" id="submit" name="SUBMIT" type="submit"/>NEXT</button><br/><br/>
			</form>
			<script>  
$(function(){
   $('#startTime').scroller({preset: 'time',timeFormat: 'HH:ii',timeWheels: 'HHii',stepMinute: 15,mode: 'mixed'});
   $('#endTime').scroller({preset: 'time',timeFormat: 'HH:ii',timeWheels: 'HHii', stepMinute: 15,mode: 'mixed'});
   jQuery('.input.num').keyup(function () { this.value = this.value.replace(/[^0-9]/g,''); });
 	$('#unit').autocomplete({
       width: 400,
       delimiter: /(,|;)\s*/,
       serviceUrl:'autocomplete.php?type=unit',
       minChars:2,
       onSelect: function(value, data){ $('#vehicle_id').val(data);}
   });
});
</script>
FIN;

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}

		
		private function submitForm() {
			$this->employeeID = $_POST['employeeID'];
			$this->vehicleID = intval($_POST['vehicleID']) > 0 ? $_POST['vehicleID'] : "NULL";
			$this->checklistID = intval($_POST['checklistID']) > 0 ? $_POST['checklistID'] : "NULL";
			$this->ID = intval($_POST['ID']);
			$endTime 	= strlen($_POST['endTime']) > 0 ? "'" .$_POST['endTime'] . "'" : "NULL";  
			$worksheet_date = $_POST['worksheet_date'];
			$startTime 	= strlen($_POST['startTime']) >0  ? "'" .$_POST['startTime'] . "'" : "NULL"; 
			$truck_start 		= strlen($_POST['truck_start']) > 0 ? intval($_POST['truck_start']) : "NULL";
			$truck_finish 	= "NULL";
			$unit_id 				= isset($_POST['unit']) ? $_POST['unit'] : "";
			$km_start 		= strlen($_POST['km_start']) > 0  ? intval($_POST['km_start']) : "NULL";
			$km_finish			=  "NULL";

			if ($ID == 0 ) { // Insert
				$sql = 	"INSERT INTO worksheet (worksheet_date, employee_id,  driver_start, driver_finish,vehicle_id, km_start, km_finish,truck_hour_start,truck_hour_finish,checklist_id ) 
						VALUES ('$worksheet_date',$this->employeeID,$startTime,$endTime,$this->vehicleID,$km_start,$km_finish,$truck_start,$truck_finish,$this->checklistID) returning worksheet_id;";
				if (!$this->ID = $this->conn->GetOne($sql)) {
					die($this->conn->ErrorMsg() );
				}    
				$_SESSION['worksheet_done'] = "$this->ID";
			}
			else {
				$sql = "UPDATE worksheet set worksheet_date = '$worksheet_date',driver_start = $startTime,driver_finish = $endTime,vehicle_id = $this->vehicleID,km_start = $truck_start,
				truck_hour_start = $truck_start where worksheet_id = $this->ID";

				if (!$this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg() );
				}    
			}
			
			if(!$this->checklistID=Functions::checklistDone($this->employeeID)) {
				header("Location: checklist.php?worksheet_id=$this->ID");
			}
			else {
				header("Location: index.php");
			}
		}
		
		/**
		 * Used to redirect and set any session vars that may be required 
		 */
		/*private function createRepairRequest() {
			$sql = "INSERT INTO repair_request (company_id, employee_id, worksheet_id) VALUES (1, ".$_SESSION['employee_id'].", $this->ID) returning request_id";
			$rs = $this->conn->Execute($sql);
			$_SESSION['requestID'] = $rs->fields['request_id'];			
			$_SESSION['nextPage'] = "worksheetTruck.php";
		}  */
	}
?>
