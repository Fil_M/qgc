<?php  // QGC
error_reporting(E_ALL);
ini_set('display_errors','1');
class InvoicePDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $startDate;
	public $endDate;
	public $cooID;
	public $conID;
	public $areaID;
	public $wellID;
	public $wellName;
	public $contractorName;
	public $shortName;
	public $estimate;
	public $totalAll;
	public $numWells;
	public $percComplete;
	public $dbLink;
	public $totalsArr = array();
 	public $expenseTot;
	public $color=array("0"=>array(255.0,238.0,65.0),"1"=>array(65.0,119.0,255.0),"2"=>array(255.0,152.0,65.0),"3"=>array(255.0,65.0,181.0),"4"=>array(160.0,65.0,255.0),"5"=>array(65.0,255.0,243.0),
   "6"=>array(197.0,255.0,65.0),"7"=>array(110.0,80.0,80.0),"8"=>array(127.0,127.0,127.0),"9"=>array(205.0,229.0,229.0),"10"=>array(255.0,65.0,65.0),"11"=>array(86.0,255.0,65.0));



	public function __construct($action="",$startDate,$endDate,$cooID,$conID,$areaID,$wellID) {
		$this->conn = $GLOBALS['conn'];
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->cooID = $cooID;
		$this->conID = $conID;
		$this->areaID = $areaID;
		$this->wellID = trim($wellID);

		$this->getDetails();
		// create new PDF document
		$pdf = new MYPDFLAND("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Works Records $this->startDate - $this->endDate");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$prevareaName = $prevwellName = $precrewName = $prevCOOID = -1;
		$details = "";
		//T1
		$table = "Table 1";
		$printHeading = true;
		foreach ($this->data as $ind =>$val ) {
			$cooID = $val['calloff_order_id'];
			$areaID = $val['area_id'];
			$wellName = $val['well_name'];
			$areaName = $val['area_name'];
			$crewName = $val['crew_name'];
			$details = $val['details'];
			$id = $val['hour_id'];
			if (intval($cooID) < 1 ) {
				die("No Call Off Order Found for  $id");
			}
			

			if ($cooID != $prevCOOID  && $prevCOOID != -1) {
				$this->doTotals($pdf,$subTotal,$Y,$prevwellName,$prevareaName,$precrewName,$hDate,$id,$prevCOOID);
				$printHeading = true;
			}  
			$prevwellName = $wellName;
			$prevareaName = $areaName;
			$prevcrewName = $crewName;
			$prevCOOID = $cooID;

			if ($printHeading) {	
				$pdf->AddPage();
				$this->getEsts($cooID);
				$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
				$printHeading = false;
				$subTotal = $gst = $gTotal = 0;
				$Y = 39.5;
			}

			$emp_name = "";	
			$plant_name = $val['p_type'];	
			$hDate = Functions::dbdate($val['hour_date']);
			$rate = $val['rate'];	
			$dktHours = $val['docket_hours_t1'];	
			$subcrew = $val['sub_crew_name'];
			$total = $val['total_t1'];	
			if (floatval($total) > 0 ) {
            $subheight = ceil(strlen($subcrew) /22) * 4.5;
            $subheight = $subheight >= 6 ? $subheight :4.5;
			 	$details = trim($val['details']);
            $height = ceil(strlen($details) /30) * 4.5;
            $height = $height >= 4.5 ? $height : 4.5;

				$height = $height > $subheight ? $height : $subheight;

				$pdf->SetFont(PDF_FONT, '', 10);
				$subTotal += $total;
				$total = "$" .number_format($total,2);	
				$pdf->setCellHeightRatio(1.2);
      		$pdf->SetFont(PDF_FONT, '', 10);
				$pdf->MultiCell(21,$height,$hDate,array('LR'=>$this->borderStyle),'L',false,0,4,$Y);
				$pdf->MultiCell(50,$height,$subcrew,array('LR'=>$this->borderStyle),'L',false,0,25,$Y);
				$pdf->MultiCell(80,$height,$plant_name,array('R'=>$this->borderStyle),'L',false,0,75);
				$pdf->MultiCell(70,$height,$details,array('R'=>$this->borderStyle),'L',false,0,155);
				$pdf->MultiCell(18,$height,$dktHours,array('R'=>$this->borderStyle),'C',false,0,225);
				$pdf->MultiCell(23,$height,$rate,array('R'=>$this->borderStyle),'C',false,0,243);
				$pdf->MultiCell(27,$height,$total,array('R'=>$this->borderStyle),'R',false,1,266);
				$Y += $height;
				if ($Y > 165 ) {
					$pdf->Line(4,$Y,293,$Y);
      			$pdf->SetFont(PDF_FONT, 'I', 12);
					$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
					$pdf->AddPage();
					$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
					$Y = 39.5;
				}
			}
		}
		$this->doTotals($pdf,$subTotal,$Y,$wellName,$areaName,$crewName,$hDate,$id,$cooID);  // Last page
		// print it
		$name = "tmp/Worksheet.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
	}
	private function tableT2T3($pdf,$Y,$wellName,$areaName,$crewName,$cooID) {
		$t2arr = $this->data;
		reset($t2arr);
		//T2
		$details = "";
		$subTotal = $gst = $gTotal = 0;
		$table = "Table 2";
		$Y += 2;
		if ($Y > 165 ) {
			$pdf->Line(5,$Y,293,$Y);
     		$pdf->SetFont(PDF_FONT, 'I', 12);
			$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
			$pdf->AddPage();
			$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
			$Y = 40;
		}
		$printHeading = true;
		$printTotals = false;
		foreach ($t2arr as $ind =>$val ) {
			$coo_id = $val['calloff_order_id'];
			$area_id = $val['area_id'];
			$well_name = $val['well_name'];
			$hDate = Functions::dbdate($val['hour_date']);
			$id = $val['hour_id'];
			$dktHours = $val['docket_hours_t2'];	
			if ( $cooID == $coo_id  && floatval($dktHours) > 0 ) {
				if ($printHeading) {
					$this->headingTitle($Y,$pdf,$table);
					$Y = $pdf->getY();
					$Y += 5;
					$printHeading = false;
					$printTotals = true;
				}
				$wellName = $val['well_name'];
				$areaName = $val['area_name'];
				$crewName = $val['crew_name'];
				$plant_name = $val['p_type'];	
				$subcrew = $val['sub_crew_name'];
            $subheight = ceil(strlen($subcrew) /22) * 4.5;
            $subheight = $subheight >= 6 ? $subheight : 4.5;
				$rate = $val['rate'];	
				$total = $val['total_t2'];	
				$details .= $val['details'] . ", ";
				$subTotal += $total;
				$total = "$" .number_format($total,2);	
				$pdf->setCellHeightRatio(1.2);
				$details = trim($val['details']);
            $hgt = ceil(strlen($details) /30) * 4.5;
            $hgt = $hgt >= 4.5 ? $hgt : 4.5;

				//echo "sub = $subheight    height = $height ";
            $height = $hgt > $subheight ? $hgt : $subheight;

				//echo "now $height";

      		$pdf->SetFont(PDF_FONT, '', 10);
				$pdf->MultiCell(21,$height,$hDate,array('LR'=>$this->borderStyle),'L',false,0,4,$Y);
            $pdf->MultiCell(50,$height,$subcrew,array('LR'=>$this->borderStyle),'L',false,0,25,$Y);
            $pdf->MultiCell(80,$height,$plant_name,array('R'=>$this->borderStyle),'L',false,0,75,$Y);
            $pdf->MultiCell(70,$height,$details,array('R'=>$this->borderStyle),'L',false,0,155,$Y);
            $pdf->MultiCell(18,$height,$dktHours,array('R'=>$this->borderStyle),'C',false,0,225,$Y);
            $pdf->MultiCell(23,$height,$rate,array('R'=>$this->borderStyle),'C',false,0,243,$Y);
            $pdf->MultiCell(27,$height,$total,array('R'=>$this->borderStyle),'R',false,1,266,$Y);

				$Y += $height;
				if ($Y > 165 ) {
					$pdf->Line(5,$Y,293,$Y);
      			$pdf->SetFont(PDF_FONT, 'I', 12);
					$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
					$pdf->AddPage();
					$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID,true);
					$Y = 40;
				}
			}
			else {
				continue;
			}
		}
		if ($printTotals) {
			$details = substr($details,0,277);
			$this->doT2Totals($pdf,$subTotal,$Y,$details);  // Last page
			$Y = $pdf->getY();
         $Y += 1;
         if ($Y > 165 ) {
            $pdf->Line(4,$Y,293,$Y);
            $pdf->SetFont(PDF_FONT, 'I', 12);
            $pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
            $pdf->AddPage();
            $this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID,false);
            $Y = 30;
         }

		}
		$this->pieChart($pdf,$wellName,$areaName,$table,$cooID);

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
	}
	private function doT2Totals($pdf,$subTotal,$Y,$details) {
		$pdf->Line(4,$Y,293,$Y);
      $pdf->SetFont(PDF_FONT, '', 10);
      //$Y +=2;
      $pdf->MultiCell(34,5,'Sub Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,8,'B');
      $gst = $subTotal * 0.1;
      $total = $subTotal + $gst;
      $subTotal = "$" .number_format($subTotal,2);
      $gst = "$" . number_format($gst,2);
      $total = "$" . number_format($total,2);
      $pdf->MultiCell(27,5,$subTotal,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,8,'B');
      $Y += 5;
      $pdf->MultiCell(34,5,'G.S.T.',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
      $pdf->MultiCell(27,5,$gst,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
      $Y += 5;
      $pdf->Line(225,$Y+5,293,$Y+5);
      $Y += 3;
      $pdf->MultiCell(34,8,'Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
      $pdf->MultiCell(27,8,$total,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
      $Y+=10;
      $pdf->SetFillColor(0,0,0);
      $pdf->MultiCell(68,1,'',array('LRTB'=>$this->borderStyle),'R',true,0,225,$Y,true,0,false,true,1,'B');
      $pdf->SetFillColor(241,121,61);
	}
	private function doTotals($pdf,$subTotal,$Y,$wellName,$areaName,$crewName,$hDate,$id,$cooID) {
		$pdf->Line(4,$Y,293,$Y);
     	$pdf->SetFont(PDF_FONT, '', 10);
      //$Y +=2;
		$pdf->MultiCell(34,5,'Sub Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,8,'B');
		$gst = $subTotal * 0.1;
		$total = $subTotal + $gst;
		$subTotal = "$" .number_format($subTotal,2);	
		$gst = "$" . number_format($gst,2);	
		$total = "$" . number_format($total,2);	
		$pdf->MultiCell(27,5,$subTotal,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,8,'B');
		$Y += 5;
		$pdf->MultiCell(34,5,'G.S.T',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
		$pdf->MultiCell(27,5,$gst,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
		$Y += 5;
		$pdf->Line(225,$Y+5,293,$Y+5);
		$Y += 3;
		$pdf->MultiCell(34,8,'Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
		$pdf->MultiCell(27,8,$total,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
		$Y+=10;
		$pdf->SetFillColor(0,0,0);
		$pdf->MultiCell(68,1,'',array('LRTB'=>$this->borderStyle),'R',true,0,225,$Y,true,0,false,true,1,'B');
		$pdf->SetFillColor(241,121,61);
		$this->tableT2T3($pdf,$Y,$wellName,$areaName,$crewName,$cooID) ; // copy data array and do t2  same page if possible
	}
	private function pieChart($pdf,$wellName,$areaName,$table,$cooID) {
		$this->getEsts($cooID);
       //echo "back from est  $cooID";
		$this->getTotToDate($cooID);
       //echo "back from tottodate  $cooID";
		$Y = $pdf->getY();
		if ($Y > 165 ) {
			$pdf->Line(4,$Y,293,$Y);
      	$pdf->SetFont(PDF_FONT, 'I', 11);
			$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
			$pdf->AddPage();
			$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
			$Y = 40;
		}
		$Y+= 5;
		$pdf->Line(153,$Y,205,$Y);
     	$pdf->SetFont(PDF_FONT, '', 11);
		$pdf->MultiCell(75,10,"Estimate of Work completed ",array('LRTB'=>$this->borderStyle),'C',true,0,5,$Y);
		$pdf->MultiCell(70,10,"Percentage of Total Costs to Date against Original Estimate",array('LRTB'=>$this->borderStyle),'C',true,0,80,$Y);
		$pdf->MultiCell(75,10,"Percentages Legend",array('LRTB'=>$this->borderStyle),'C',true,0,150,$Y);
		$pdf->MultiCell(68,10,"Job Total to date (Ex G.S.T.)",array('LTRB'=>$this->borderStyle),'C',true,0,225,$Y);
		$Y+= 11;
		$xc = 33;
		$r = 20;
		$actualEst = $this->estimate/$this->numWells;
		if ($actualEst > 0 ) {
			$perc =round((($this->totalAll / $actualEst) * 100),2) ;
		}
		else {
			$perc = "UNKNOWN";
		}
      $pdf->SetFont(PDF_FONT, '', 11);
		$pdf->MultiCell(75,0,$this->percComplete ."% Complete",0,'C',false,0,5,$Y);
		$tott = '$' .number_format($this->totalAll,2);
		$totPlusExpense = '$' .number_format($this->totalAll + $this->expenseTot,2);
		$expense = '$' .number_format($this->expenseTot,2);
		$pdf->SetFillColor(0,0,0);
		$pdf->MultiCell(68,5,$tott,array('B'=>$this->borderStyle),'R',false,1,225,$Y);
		// Job Total Plus Expenses
		$pdf->MultiCell(28,5,'Expenses', 0,'L',false,1,237,$Y +6);
		$pdf->MultiCell(68,5,$expense, 0,'R',false,1,225,$Y +6);
      $pdf->SetFont(PDF_FONT, '', 11);
		$pdf->MultiCell(68,5,$totPlusExpense,array('B'=>$this->borderStyle),'R',false,1,225,$Y+12);
		$pdf->MultiCell(68,1,'',array('LRTB'=>$this->borderStyle),'R',true,1,225,$Y+17,true,0,false,true,1,'B');


		// Estimate Complete
		$Y += 30;
		$xc = 40;
			$rotate  =  (360  *   ($this->percComplete / 100 )) - 20 ;
		if ($this->percComplete <= 50 ) {
			$pdf->SetFillColor(101, 251, 112);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 0, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 0, 2);
		}
		else if ($this->percComplete < 100 ){
			$pdf->SetFillColor(101, 251, 112);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 180, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 180, 2);
		}
		else {  // 100%  done
			$pdf->SetFillColor(101, 251, 112);    // 
			$pdf->Circle($xc, $Y, $r,0,360,'F',null,array(255, 86, 86));
		}
		$totWell = $this->totalAll;
		if ($this->numWells > 1 ) {
      	$actualEst = $this->estimate/$this->numWells;
      	$totWell = $this->totalAll/$this->numWells;
     		$pdf->SetFont(PDF_FONT, 'I', 10);
			$pdf->Text(88, $Y-25, "$this->numWells Wells. Estimate per Well: ".number_format($actualEst,2) );
		}
		 // Pie Chart percecnt against est
      $xc = 110;
      $r = 20;      //Percent complete

      $totperc =round((($totWell / $actualEst) * 100),2) ;  // Total T1  and  T2
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(80,0,"{$totperc}% Percent of Estimate",0,'L',false,0,90,$Y-29);
      // Costs against Estimate
      $colr = $this->color[10];
      $pdf->Circle($xc, $Y, $r,0,360,null,$this->borderStyle,array(255,255,255));
      $percArr=array();
      $cnt = count($this->totalsArr)-1 ;

      for($x=$cnt;$x>=0;$x-- ) {
         $iPerc = $invPerc =$rotate = $tot = 0;
         $itot = $this->totalsArr[$x]['total'];
         if (floatval($itot) > 0 ) {
            $colr = $this->color[$x];
            $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
            for ($y = $x;$y>=0;$y--) {
               $tot += $this->totalsArr[$y]['total'];
            }
         // Do Total T1  
            $iPerc = round((($tot  / $this->totalAll) * $totperc ) ,2) ;
            $invPerc = round((($itot / $this->totalAll) * $totperc ) ,2) ;
            $percArr[] = array('color'=>$x,'percent' => $invPerc,'name'=>$this->totalsArr[$x]['name']);
            $rotate  =  (360  *   ($iPerc / 100 )) ;
            $pdf->PieSector($xc, $Y, $r, 0, $rotate, 'FD', true, 90);
         }

      }
		$oldY = $Y;
		$Y -= 27.5;
      $pdf->SetFont(PDF_FONT, '', 10);
      $percArr = array_reverse($percArr);
      foreach ($percArr as $ind => $val) {
         $colr = $this->color[$val['color']];
         $name = $val['name'];
         $perc = $val['percent'];
         $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
			$pdf->MultiCell(56,0,$name,array('LTB'=>$this->borderStyle),'L',true,0,152,$Y);
         $pdf->MultiCell(16,0,$perc ."%",array('RTB'=>$this->borderStyle),'R',true,0,206,$Y);

         $Y+=4.5;
      }
		$Y = $oldY + 24;

		$pdf->Line(4,$Y,293,$Y);  

	}
	private function getDetails(){
	   $connArr = Functions::getConName($this->conID);
      $this->contractorName = $connArr['con_name'];
      $this->shortName = $connArr['name'];
		$this->dbLink = $connArr['db_link'];
		$whereClause =  !is_null($this->cooID)   ? "WHERE h.calloff_order_id = $this->cooID " : "WHERE 1 = 1 ";
		$whereClause .= !is_null($this->areaID)  ? " AND h.area_id = $this->areaID " : "";
      $whereClause .= !is_null($this->wellID) && strlen($this->wellID) > 0 ? " AND $this->wellID  in  (select (unnest(string_to_array(h.well_ids::text,'|')::int[]))) " : "";
		$sql = " with tab as (select  cp.plant_type_id,h.hour_id,h.hour_date,h.docket_hours_t1,docket_hours_t2,h.rate,h.total_t1,total_t2,h.area_id,h.details,h.calloff_order_id,h.well_ids,h.crew_id,h.sub_crew_id 
			  FROM {$this->shortName}_hour h LEFT JOIN {$this->shortName}_plant cp using (plant_id)
			 $whereClause and h.hour_date between '$this->startDate'  and '$this->endDate' and h.status >= 5 and h.removed is false ) 
		Select tab.*,a.area_name,pt.p_type,sc.sub_crew_name,c.crew_name, wells_from_ids(tab.well_ids) as well_name
		from tab
      JOIN calloff_order co  on co.calloff_order_id = tab.calloff_order_id
      LEFT JOIN plant_type pt on pt.plant_type_id = tab.plant_type_id 
      LEFT JOIN area  a  on a.area_id = tab.area_id  
      LEFT JOIN crew c  on c.crew_id = tab.crew_id  
      LEFT JOIN sub_crew sc  using (sub_crew_id)
      order by tab.calloff_order_id,well_name,a,area_name,tab.hour_date ";


		//echo $sql;

		if (! $this->data = $this->conn->getAll($sql)) {
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
			else {
				die("No data for this date/range found");
				die($sql);
			}
		}

		//var_dump($this->data);
		if (count($this->data) < 1 ) {
			die("<h1> NO hour records found for this date - $this->startDate - $this->endDate");
		}
	}
	private function getEsts($cooID) {
		$this->estimate = Functions::getConEst($cooID,$this->dbLink);
      $sql = "SELECT well_ids from calloff_order where calloff_order_id = $cooID";
      if (! $wells = $this->conn->getOne($sql)) {
         if ($this->conn->ErrorNo() != 0 ) {
            die($this->conn->ErrorMsg());
          }
          else {
				 die("dies in select wells  $cooID");
             die($sql);
          }
      }
      $arr = explode("|",$wells);
      $this->numWells  = count($arr);
      $sql = "select max(percentage_complete) as percentage_complete from {$this->shortName}_docket_day where docket_date <= '$this->endDate' and calloff_order_id = $cooID and percentage_complete is not null";
      if (! $data = $this->conn->getRow($sql)) {
         if ($this->conn->ErrorNo() != 0 ) {
				die("no percentage");
            die($this->conn->ErrorMsg());
			}
			else {
				var_dump($data);
            die($sql);
			}
      }
		
      $this->percComplete = intval($data['percentage_complete']);
	}
	private function getTotToDate($cooID) {
		$this->totalsArr = array();
      $this->totalAll = 0;

      $sql = "SELECT sum(coalesce(total_t1,0)) as tot_t1 , sum(coalesce(total_t2,0)) as tot_t2 , sum(coalesce(expense,0))  as expense
      from {$this->shortName}_hour where calloff_order_id = $cooID and hour_date <= '$this->endDate' and site_instruction_id is NULL";
      if (! $totalsArr = $this->conn->getRow($sql)) {
            if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
            else {
               $totalsArr = array("tot_t1"=>"0","tot_t2"=>0);
            }
      }
      $this->expenseTot = $totalsArr['expense'];
      $this->totalsArr[] = array("name"=>"Original Estimate          T1","total"=>$totalsArr['tot_t1']);
      $this->totalsArr[] = array("name"=>"Original Estimate          T2","total"=>$totalsArr['tot_t2']);
      $this->totalAll = $totalsArr['tot_t1'] + $totalsArr['tot_t2'];
      // Now all Site  instructions if any
      $sql = "SELECT sum(coalesce(total_t1,0)) as tot_t1 , sum(coalesce(total_t2,0)) as tot_t2 ,  sum(coalesce(expense,0))  as expense, si.instruction_no
      from {$this->shortName}_hour h
      LEFT JOIN {$this->shortName}_site_instruction si using (site_instruction_id)
      where h.calloff_order_id = $cooID and h.site_instruction_id is NOT NULL and hour_date <= '$this->endDate' GROUP BY instruction_no";
      if (! $totalsArr = $this->conn->getAll($sql)) {
            if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
				//else {
				//	die("dies in  sum get instruction");
            //	die($sql);
			//	}
      }
      if (count($totalsArr) > 0 ) {
         foreach($totalsArr as $ind=>$val) {
            $key = "Site Instruction ".$val['instruction_no'] . "   T1";
            $this->totalsArr[] = array("name"=>$key,"total"=>$val['tot_t1']);
            $key = "Site Instruction " .$val['instruction_no'] . "   T2";
            $this->totalsArr[] = array("name"=>$key,"total"=>$val['tot_t2']);
            $this->totalAll += $val['tot_t1'] + $val['tot_t2'];
      		$this->expenseTot += $val['expense'];
         }
      }

		//var_dump($this->totalAll);
	}
	 private function heading($pdf,$pDate,$area,$well,$crew,$table,$cooID,$showHeadings=true) {
      // Heading Title 
		$est = '$' .number_format($this->estimate);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
      $pdf->MultiCell(60, 0, 'Works Record', 0, 'C', false, 2, 120,1);
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(25, 0, 'From:', 0, 'L', false, 0, 32,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'C.O.O:', 0, 'L', false, 0, 134,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(50, 0, $cooID, 0, 'L', false,0, 155);
      $pdf->SetTextColor(255,0,0);
      $pdf->MultiCell(50, 0, "($this->numWells)", 0, 'L', false,1, 166);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Contractor:' ,0,'L', false, 0, 32,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $this->contractorName, 0, 'L', false, 2, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Work Scope:' ,0,'L', false, 0, 134,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $crew, 0, 'L', false, 2, 155);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(15, 0, 'Area:', 0, 'L', false, 0, 32,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(0, 0, $area, 0, 'L', false, 0, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Estimate:' ,0,'L', false, 0, 134,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $est, 0, 'L', false, 2, 155);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Well/s:', 0, 'L', false, 0, 32,23.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(0, 0, substr($well,0,65), 0, 'L', false, 2, 53);
      $pdf->SetTextColor(0,0,0);
		$this->headingTitle(29,$pdf,$table,$showHeadings);
     	$pdf->SetTextColor(0,0,0);
	}
	private function headingTitle($Y,$pdf,$table,$showHeadings=true) {
		if ($showHeadings) {
	   	$pdf->SetFillColor(241,121,61);
      	$pdf->SetTextColor(0,0,0);
			$pdf->SetFont(PDF_FONT, '', 11);
			$pdf->MultiCell(21,0,"$table",array('LTBR'=>$this->borderStyle),'L',true,1,4,$Y);
			$Y += 5;
      	$pdf->SetFont(PDF_FONT, '', 11);
			$pdf->MultiCell(21,0,'Date',array('LTB'=>$this->borderStyle),'C',true,0,4,$Y);
			$pdf->MultiCell(50,0,'Sub Scope',array('LTB'=>$this->borderStyle),'C',true,0,25);
			$pdf->MultiCell(80,0,'Machine/Equipment/Material',array('LTB'=>$this->borderStyle),'C',true,0,75);
			$pdf->MultiCell(70,0,'Details',array('LTB'=>$this->borderStyle),'C',true,0,155);
			$pdf->MultiCell(18,0,'Hours',array('LTB'=>$this->borderStyle),'C',true,0,225);
			$pdf->MultiCell(23,0,'Rate',array('LTB'=>$this->borderStyle),'C',true,0,243);
			$pdf->MultiCell(27,0,'Amount',array('LTRB'=>$this->borderStyle),'C',true,0,266);
		}

   }

}
?>
