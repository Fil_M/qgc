<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class FieldEstimatePDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
   public $costID;
   public $siteInsID;
	public $areaName;
	public $wellName;
	public $crewName;
	public $contractorName;
	public $shortName;
	public $conDomain;
	public $fieldType;
	public $headerStr;
	public $fieldData;
	public $parentID;
	public $fileName;
   public $contractorPath;


	public function __construct($action="",$conID,$costID,$siteInsID,$fieldType="cost",$filename=NULL) {
		$this->conn = $GLOBALS['conn'];
		$this->conID = $conID;
		$this->costID = $costID;
		$this->siteInsID = $siteInsID;
		$this->fieldType = $fieldType;
		$this->fileName = $filename;
		$this->action = $action;


		if ($this->fieldType == "cost" ) {
			$this->getCostDetails($this->costID);
			$this->headerStr = " for TCE $this->costID";
			$this->parentID = $this->costID;
		}
		else  {
			$this->getSiteDetails($this->siteInsID);
			$this->headerStr = " for Site Ins $this->siteInsID";
			$this->parentID = $this->siteInsID;
		}
		extract($this->data);
		$this->crewName = $crew_name;
		if ($this->fieldType == "cost" ) {
			$field_estimate_ids = preg_replace('/\|/',",",$field_estimate_ids);
		}
		else { // Need to get these from filedData for a Site Instruction
			$field_estimate_ids="";
			foreach($this->fieldData as $ind=>$val ) {
				extract($val);
				$field_estimate_ids .= "$field_estimate_id,";
			}
			$field_estimate_ids = preg_replace('/,$/',"",$field_estimate_ids);

		}
		//$qgc_approve_date = Functions::dbDate($qgc_approve_date);
      //$cost_estimate = "$".number_format($cost_estimate,2);
      //$estimate_total = "$".number_format($estimate_total,2);
		//$this->insNo = $instruction_no;

		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Field Estimates $this->contractorName $area_name - $well_name");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$Y = 30;
		$pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(60, 0, 'Well Pad Location/s:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(110, 0, "$area_name / $well_name", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Contractor:', 0, 'L', false, 0, 150,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(35, 0, $this->contractorName, array('B'=>$this->lineStyle), 'L', false,1, 170);
      $Y+=10 ;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(60, 0, 'Workscope:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(55, 0, "$crew_name", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Sub Scopes:', 0, 'L', false, 0, 100,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(80, 0, "$subcrews", array('B'=>$this->lineStyle), 'L', false, 2, 125,$Y);
		$Y = $pdf->getY();
      $Y+=10 ;
		$pdf->SetFillColor(230, 133, 93);
      $pdf->SetTextColor(0,0,0);
		foreach($this->fieldData as $ind=>$val) {
			extract($val);
			$estimate_total = number_format($estimate_total,2);
			$Y = $this->checkFits($field_estimate_id,$sub_crew_name,$total_hours,$estimate_total,$pdf,$Y);
			$Y =$this->showLines($field_estimate_id,$pdf,$Y);
			$Y+=4;
		}
      //$pdf->MultiCell(165, 0, $comments, array('B'=>$this->lineStyle), 'L', false,0, 40);
      //$Y = $pdf->getY();
      $Y += 10;
      //$pdf->MultiCell(10,0,$Y,0,'L',false,0,3,$Y);
      //$Y += 10;
		if ($Y >= 250 ) {
			$Y = $this->addNewPage($pdf,-1,$Y);
		}
      $pdf->SetTextColor(0,0,0);
		$pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(50,0,"Contractor Signed:",0,'L',false,0,3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55,0,$contractor_signee,array('B'=>$this->lineStyle),'L',false,0,40);
      $pdf->SetTextColor(0,0,0);
		
      $image_file = $this->contractorPath .$contractor_sig;
		if (!file_exists($image_file)) {
			$image_file="images/nosig.png";
		}
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,70 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

      $Y+=35;
      $pdf->SetFont(PDF_FONT, 'B', 10);
      $pdf->MultiCell(120,0,"Company Representative or Delegate:",0,'L',false,0,40,$Y);
-
      $Y+=10;
		if ($Y >= 250) {
			$Y = $this->addNewPage($pdf,-2,$Y);

		}
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(50,0,"Approved for QGC:",0,'L',false,0,3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55,0,$qgc_signee,array('B'=>$this->lineStyle),'L',false,0,40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(25, 0, 'Date:', 0, 'L', false, 0, 100);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(22, 0, $qgc_approve_date, array('B'=>$this->lineStyle), 'L', false,1, 115);
      $image_file = $qgc_sig;
		//var_dump($this->qgcSignature);
		if (!file_exists($image_file)) {
			$image_file="images/nosig.png";
		}
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,0 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

      $name = !is_null($this->fileName) ? $this->fileName : "tmp/FieldEstimate_{$this->conID}_{$this->parentID}.pdf";

		var_dump($name);
      if ($action == "print" ) {
         $pdf->Output($name, 'I');
      }
      else {  
         $pdf->Output($name, 'F');
      }

   }		
	private function addNewPage($pdf,$fieldEstID,$Y) {
      	$pdf->MultiCell(50,0,"continued....",0,'L',false,0,3,$Y+5);
			$pdf->AddPage();
			$this->heading($pdf);
			return 30;

	}
		private function checkFits ($field_estimate_id,$sub_crew_name,$total_hours,$estimate_total,$pdf,$Y) {
			$sql = "select count(field_estimate_line_id) from  {$this->shortName}_field_estimate_line where field_estimate_id = $field_estimate_id";
			if (! $cnt = $this->conn->getOne($sql)) {
         	die($this->conn->errorMsg());
      	}
			$total = $Y + 18 + (($cnt * 4 )) + 5; 
			if ($total >= 290) {
				$Y = $this->addNewPage($pdf,$field_estimate_id,$Y);
			} 
			
			$pdf->SetFillColor(230, 133, 93);
      	$pdf->SetTextColor(0,0,0);
      	$pdf->SetFont(PDF_FONT, '', 10);
      	$pdf->MultiCell(30, 5, 'Field Estimate', array('LB'=>$this->borderStyle), 'C', true, 0, 3,$Y);
      	$pdf->MultiCell(112, 5, 'Sub Scope',array('LB'=>$this->borderStyle) , 'C', true, 0, 33,$Y);
      	$pdf->MultiCell(30, 5, 'Total Units',array('LB'=>$this->borderStyle) , 'C', true, 0, 145,$Y);
      	$pdf->MultiCell(30, 5, 'Estimate Total',array('LBR'=>$this->borderStyle) , 'C', true, 1, 175,$Y);
			$Y+=5;
      	$pdf->MultiCell(30, 5, $field_estimate_id, array('LB'=>$this->borderStyle), 'C', false, 0, 3,$Y);
      	$pdf->MultiCell(112, 5, $sub_crew_name,array('LB'=>$this->borderStyle) , 'L', false, 0, 33,$Y);
      	$pdf->MultiCell(30, 5, $total_hours,array('LB'=>$this->borderStyle) , 'C', false, 0, 145,$Y);
      	$pdf->MultiCell(30, 5, $estimate_total,array('LBR'=>$this->borderStyle) , 'C', false, 1, 175,$Y);
			$Y+=5;
			$pdf->SetFillColor(157, 145, 145);
      	$pdf->SetTextColor(255,255,255);
      	$pdf->SetFont(PDF_FONT, '', 9);
      	$pdf->MultiCell(60, 4, 'Plant Type', array('LB'=>$this->borderStyle), 'C', true, 0, 3,$Y);
      	$pdf->MultiCell(60, 4, 'Machine',array('LB'=>$this->borderStyle) , 'C', true, 0, 63,$Y);
      	$pdf->MultiCell(22, 4, 'Rate',array('LB'=>$this->borderStyle) , 'C', true, 0, 123,$Y);
      	$pdf->MultiCell(30, 4, 'Estimate of Units',array('LB'=>$this->borderStyle) , 'C', true, 1, 145,$Y);
      	$pdf->MultiCell(30, 4, 'Sub Total',array('LBR'=>$this->borderStyle) , 'C', true, 1, 175,$Y);
      	$pdf->SetTextColor(0,0,0);
				return $Y;
		}
	private function showLines($fieldEstID,$pdf,$Y) {
		$sql = "SELECT plant_type,machine,rate,hours,sub_total from {$this->shortName}_field_estimate_line where field_estimate_id = $fieldEstID order by machine";
		if (! $lineData = $this->conn->getAll($sql)) {
			die($this->conn->errorMsg());
		}
		$Y += 4;
		foreach($lineData as $ind=>$val) {
			extract($val);
			$sub_total = number_format($sub_total,2);
			$plant_type = substr($plant_type,0,36);
			$machine = substr($machine,0,36);
			$pdf->SetFont(PDF_FONT, '', 9);
         $pdf->MultiCell(60, 4, $plant_type, array('LB'=>$this->borderStyle), 'L', false, 0, 3,$Y);
         $pdf->MultiCell(60, 4, $machine,array('LB'=>$this->borderStyle) , 'L', false, 0, 63,$Y);
         $pdf->MultiCell(22, 4, $rate,array('LB'=>$this->borderStyle) , 'C', false, 0, 123,$Y);
         $pdf->MultiCell(30, 4, $hours,array('LB'=>$this->borderStyle) , 'C', false, 1, 145,$Y);
         $pdf->MultiCell(30, 4, $sub_total,array('LBR'=>$this->borderStyle) , 'C', false, 1, 175,$Y);
			$Y += 4;


		}
		$Y += 5;
		return $Y;

	}
	 private function getSiteDetails($siteInsID) {
			$connArr = Functions::getConName($this->conID);
      	$this->contractorName = $connArr['con_name'];
      	$this->shortName = $connArr['name'];
			$this->conDomain = $connArr['domain'] . "/";
			$this->contractorPath = "/home/https/{$this->shortName}/";

			 $sql = "SELECT si.*,a.area_name,crew_name,wells_from_ids(si.well_ids) as well_name,
			subscopes_from_ids(si.sub_crew_ids) as subcrews
         from {$this->shortName}_site_instruction si 
         LEFT JOIN calloff_order co using (calloff_order_id)
         LEFT JOIN area a using(area_id) 
         LEFT JOIN crew c on c.crew_id = si.crew_id 
         where si.site_instruction_id = $siteInsID";
      // echo $sql;
			if (! $this->data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }
			$sql = "select field_estimate_id,sub_crew_name,total_hours,estimate_total from {$this->shortName}_field_estimate  fe
			LEFT JOIN sub_crew using(sub_crew_id)
 			where site_instruction_id = $siteInsID";
         if (! $this->fieldData = $this->conn->getAll($sql)) {
            die($this->conn->ErrorMsg());
         }



      }
		 private function getCostDetails($costID) {
         $connArr = Functions::getConName($this->conID);
         $this->contractorName = $connArr['con_name'];
         $this->shortName = $connArr['name'];
         $this->conDomain = $connArr['domain'] . "/";
			$this->contractorPath = "/home/https/$this->shortName/";
         $sql = "SELECT a.area_name,c.crew_name,wells_from_ids(re.well_ids) as well_name,re.*,subscopes_from_ids(re.sub_crew_ids) as subcrews 
         from request_estimate re
         LEFT JOIN area a using(area_id) 
         LEFT JOIN crew c using(crew_id) 
         where re.request_estimate_id = $costID";
         if (! $this->data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }

			$sql = "select field_estimate_id,sub_crew_name,total_hours,estimate_total from {$this->shortName}_field_estimate  fe
			LEFT JOIN sub_crew using(sub_crew_id)
 			where request_estimate_id = $costID";
         if (! $this->fieldData = $this->conn->getAll($sql)) {
				echo "No Field Estimate Data";
				if ($this->conn->ErrorNo() != 0 ) {
            	die($this->conn->ErrorMsg());
				}
         }
      }


		 private function heading($pdf){
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, 'QGC Well Engineering Construction', 0, 'C', false, 2, 50,3);
      $pdf->SetFont(PDF_FONT, '', 12);
      $pdf->SetTextColor(255,0,0);
      $pdf->MultiCell(100, 0, $this->crewName, 0, 'C', false, 0, 50,10);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, "Field Estimates $this->headerStr", 0, 'C', false, 0, 50,17);
   }



}
?>
