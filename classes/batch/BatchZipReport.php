<?php    // All scripts need to be  run  from  batch
error_reporting(E_ALL);
ini_set('display_errors','1');

class BatchZipReport  {
   public $data = array();
   public $conn;
   public $csv;
	public $employeeID;
	public $fileData;
	public $csvFile;
	public $fp;
	public $lineCount;
	public $wellName;
	public $areaName;
	public $shortNames;
	public $baseNames = array("1"=>"/home/https/njcontracting/admin/","5"=>"/home/https/bruhl/admin/","7"=>"/home/https/mikejones/admin/","8"=>"/home/https/clien/admin/","9"=>"home/https/ostwaldbros/admin/",
								"11"=>"/home/https/amcor/admin/","12"=>"home/https/rby/admin/","13"=>"/home/https/hem/admin/","14"=>"/home/https/corbets/admin/","15"=>"/home/https/wr/admin/","16"=>"/home/https/wad/admin/");
	public $zipDir;
	public $zipID;


	public function   __construct($empID,$areaName,$wellName) {
		global $BASE;
		echo "start " . date('d-m-Y H:i:s'). "\n";
      $this->conn = $GLOBALS['conn'];
      $this->employeeID = $empID;
      $this->wellName = $wellName;
      $this->areaName = $areaName;
		//echo "getnames";
		$this->getNames();
		//echo "do csv";E
		echo "Starts CSV  ".date('Y-m-d H:i:s') . "\n";
		$this->doCSV();
		//echo "doExpewnses";
		echo "Starts Expenses  ".date('Y-m-d H:i:s') . "\n";
		$this->doExpenses();
		//echo "do Invoices";
		echo "Starts Invoices  ".date('Y-m-d H:i:s') . "\n";
		$this->doInvoices();
		//echo "do pdfs";
		echo "Starts PDF's  ".date('Y-m-d H:i:s') . "\n";
		$this->doPDFs();
		//echo "do zip";
		echo "Starts Zip  ".date('Y-m-d H:i:s') . "\n";
		$this->doZip();

		echo "finish " . date('d-m-Y H:i:s') . "\n";
		$eNum = Functions::insertEmailLog($this->employeeID,0,"NULL","Zip Report for $this->areaName / $this->wellName",'ZIP',$this->zipID,date('d-m-Y'),"NULL","NULL");
      $em = new BatchMail($eNum);
      echo "finish email " .date('d-m-Y H:i:s') . "\n";


	}
	private function doZip() {
		global $BASE; 
		require_once("{$BASE}classes/Functions.php");
		$sql = "SELECT zipdir from batch where area_name = '$this->areaName' and well_name = '$this->wellName' limit 1";
		if (!$this->zipDir = $this->conn->getOne($sql)) {
			die($sql);
			die($this->conn->ErrorMsg());
		}
		$fromDir = "{$BASE}admin/$this->zipDir";
		$toZip = "{$BASE}admin/{$this->zipDir}.zip";
		if (!Functions::Zip($fromDir,$toZip,true)) {
			die("Can't Zip");
		}
		//Only Insert  once
		$sql = "SELECT zip_id from zip where create_date = current_date and area_name = '$this->areaName' and well_name = '$this->wellName'";
		if (!$this->zipID = $this->conn->getOne($sql)) {
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
			else {
				$sql = "INSERT into zip values(nextval('zip_zip_id_seq'),'$toZip',current_date,$this->employeeID,'$this->areaName','$this->wellName') returning zip_id";
				if (!$this->zipID = $this->conn->getOne($sql)) {
					die($sql);
					die($this->conn->ErrorMsg());
				}

			}
		}
	}
	private function doPDFs() {
		global $BASE;  //  BASE  includes trailing seaparator
		require_once("{$BASE}classes/CallOffOrderPDF.php");
		require_once("{$BASE}classes/FieldEstimatePDF.php");
		require_once("{$BASE}classes/TargetCostPDF.php");
		require_once("{$BASE}classes/SiteInstructionPDF.php");
		require_once("{$BASE}classes/CompletePDF.php");
		require_once("{$BASE}classes/MYPDF.php");
		foreach($this->shortNames as $ind=>$name) {  // Call Off Orders
		//	echo "does calloff";
			echo "Starts Calloff Orders  ".date('Y-m-d H:i:s') . "\n";
			$sql = "SELECT b.contractor_id,b.calloff_order_id,filename from batch b
			WHERE batch_type = 'CALLOFF'
			and area_name = '$this->areaName' and well_name = '$this->wellName'
			and b.contractor_id = $ind";
         if (!$data = $this->conn->getAll($sql)) {
            if ($this->conn->ErrorNo()!=0 ) {
					die($sql);
               die($this->conn->ErrorMsg());
            }
         }
			if (count($data) > 0 ) {
				foreach($data as $i=>$v ) {
					extract($v);
					echo "doing COO PDF for $contractor_id, $calloff_order_id $filename \n";
					$obj = new CallOffOrderPDF("file",$contractor_id,$calloff_order_id,$filename);
				}
			}
		}
		echo "ends  COOs \n";
		foreach($this->shortNames as $ind=>$name) {  // TCE & Field Estimates 
			//echo "does tce";
			echo "Starts TCE  ".date('Y-m-d H:i:s') . "\n";
			$sql = "SELECT b.contractor_id,b.calloff_order_id,table_id,filename from batch b
			WHERE batch_type = 'TCE'
			and area_name = '$this->areaName' and well_name = '$this->wellName'
			and b.contractor_id = $ind";
         if (!$data = $this->conn->getAll($sql)) {
            if ($this->conn->ErrorNo()!=0 ) {
					die($sql);
               die($this->conn->ErrorMsg());
            }
         }
			if (count($data) > 0 ) {
				foreach($data as $i=>$v ) {
					extract($v);
					$fieldFile = preg_replace('/Target_Cost_Estimate/',"Field_Estimate",$filename);  // This  works  becaused parent dir  is  TCE
					echo "doing TCE PDF for $contractor_id, $table_id $filename \n";
					$obj = new TargetCostPDF("file",$contractor_id,$table_id,$filename);
					echo "doing Field Est  PDF for $contractor_id, $table_id $fieldFile \n";
					$obj1 = new FieldEstimatePDF("file",$contractor_id,$table_id,NULL,"cost",$fieldFile);
				}
			}
		}
		echo "ends  TCE \n";
		foreach($this->shortNames as $ind=>$name) {  // Site Ins & Field Estimates 
			//echo "does site ins";
			echo "Starts Site Instructions  ".date('Y-m-d H:i:s') . "\n";
			$sql = "SELECT b.contractor_id,b.calloff_order_id,table_id,filename from batch b
			WHERE batch_type = 'SITE'
			and area_name = '$this->areaName' and well_name = '$this->wellName'
			and b.contractor_id = $ind";
         if (!$data = $this->conn->getAll($sql)) {
            if ($this->conn->ErrorNo()!=0 ) {
					die($sql);
               die($this->conn->ErrorMsg());
            }
         }
			if (count($data) > 0 ) {
				foreach($data as $i=>$v ) {
					extract($v);
					$sql1 = "SELECT filename from batch where table_id = $table_id and batch_type = 'SITE_FIELD'";  // get the  correct field estimate  name
					if (!$fieldFile = $this->conn->getOne($sql1)) {
						echo "$sql1";
						echo "dies  here";
						var_dump($fieldFile);
						die($this->conn->ErrorMsg());
					}
					$obj = new SiteInstructionPDF("file",$contractor_id,$table_id,$filename);
				}
					$obj1 = new FieldEstimatePDF("file",$contractor_id,NULL,$table_id,"site",$fieldFile);
			}
		}
		echo "ends  Site Instructions\n";
		foreach($this->shortNames as $ind=>$name) {  // Completions
			echo "Starts Completions  ".date('Y-m-d H:i:s') . "\n";
			$sql = "SELECT b.contractor_id,b.calloff_order_id,table_id,filename from batch b
			WHERE batch_type = 'COMPLETION'
			and area_name = '$this->areaName' and well_name = '$this->wellName'
			and b.contractor_id = $ind";
         if (!$data = $this->conn->getAll($sql)) {
            if ($this->conn->ErrorNo()!=0 ) {
					die($sql);
               die($this->conn->ErrorMsg());
            }
         }
			if (count($data) > 0 ) {
				foreach($data as $i=>$v ) {
					extract($v);
					$obj = new CompletePDF("file",$contractor_id,$table_id,$filename);
				}
			}
		}
		echo "ends  Completions\n";
	}
	private function getNames(){
		$sql = "SELECT contractor_id,name from contractor 
		where contractor_id in (select distinct(contractor_id) from batch  where area_name = '$this->areaName' and well_name = '$this->wellName')  and removed is false";
		if (!$this->shortNames = $this->conn->getAssoc($sql)) {
			die($sql);
			die($this->conn->ErrorMsg());
		}
	}

	private function doExpenses() {
		foreach($this->shortNames as $ind=>$name) {
			$sql = "SELECT b.contractor_id,upload_name,filename from batch b
         LEFT JOIN {$name}_upload u on b.contractor_id = u.contractor_id and b.batch_type = 'EXPENSE' 
         where u.upload_type_id = 1
			and area_name = '$this->areaName' and well_name = '$this->wellName'
         and length(u.coos) > 0
         and b.calloff_order_id = u.coos::integer";
			if (!$data = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo()!=0 ) {
					die($sql);
					die($this->conn->ErrorMsg());
				}
			}
			if (count($data) > 0 ) {
				foreach($data as $ind=>$val) {
					extract($val);
					$fromFile = $this->baseNames[$contractor_id] . $upload_name;
					if (!file_exists($fromFile)) {
						continue;
					}
					else {
						$filename = preg_replace('/USE_NAME_FROM_UPLOAD.*$/',"",$filename);
						$toFile = $filename . basename($upload_name); 
						if (!copy($fromFile, $toFile)) {
    						die("failed to copy $fromFile to $toFile\n");
						}
					}
				}
			}
		}
	}
	private function doInvoices() {
		foreach($this->shortNames as $ind=>$name) {
			$sql = "SELECT b.contractor_id,upload_name,filename from batch b
         LEFT JOIN {$name}_upload u on b.contractor_id = u.contractor_id and b.batch_type = 'INVOICE' 
         where u.upload_type_id = 13
			and area_name = '$this->areaName' and well_name = '$this->wellName'
         and length(u.coos) > 0
         and b.calloff_order_id = u.coos::integer";
			if (!$data = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo()!=0 ) {
					die($sql);
					die($this->conn->ErrorMsg());
				}
			}
			if (count($data) > 0 ) {
				foreach($data as $ind=>$val) {
					extract($val);
					$fromFile = $this->baseNames[$contractor_id] . $upload_name;
					if (!file_exists($fromFile)) {
						continue;
					}
					else {
						$filename = preg_replace('/USE_NAME_FROM_UPLOAD.*$/',"",$filename);
						$toFile = $filename . basename($upload_name); 
						if (!copy($fromFile, $toFile)) {
    						die("failed to copy $fromFile to $toFile\n");
						}
					}
				}
			}
		}
	}

	private function doCSV() {
		// get filenames and contarctors
		$sql = "select max(filename) as filename,contractor_id from batch where batch_type = 'CSV' and area_name = '$this->areaName' and well_name = '$this->wellName' group by contractor_id order by contractor_id";
		if (!$this->fileData = $this->conn->getAll($sql)) {
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
		}
		foreach($this->fileData as $ind=>$val) {
			extract($val);
			$this->csvFile = $filename;
			$this->fp = fopen($this->csvFile,'w');
      	fputcsv($this->fp,array("Area","Well"));
      	fputcsv($this->fp,array("$this->areaName","$this->wellName"));
      	fputcsv($this->fp,array());
			$headings = array('Contractor','Date','C.O.O.','Budget','Docket','Operator','Unit','Plant/Type','Work Scope','Sub Scope','Area','Well','Docket Hours','Rate','Total','Number Wells');
			fputcsv($this->fp,$headings);
			$this->dumpData($contractor_id);
			fclose($this->fp);
		}
	}
	private function dumpData($conID) {
		$sql = "SELECT con_name,name from contractor where contractor_id = $conID";
		if (! $names = $this->conn->getRow($sql)) {
			die($sql);
			die($this->conn->ErrorMsg());
		}
		extract($names);
			$sql = "SELECT h.*,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,pt.p_type,plant_unit,a.area_name,c.crew_name,sub_crew_name
        	from {$name}_hour h
        	LEFT JOIN {$name}_plant p using (plant_id)
        	LEFT JOIN plant_type pt using (plant_type_id)
        	LEFT JOIN area a using (area_id)
        	LEFT JOIN {$name}_employee e using (employee_id)
        	LEFT JOIN crew c  using (crew_id)
        	LEFT JOIN sub_crew sc  using (sub_crew_id)
        	where calloff_order_id in (select calloff_order_id from batch where contractor_id = $conID and batch_type = 'CALLOFF') and  h.removed is false and  h.status >= 5
        	order by calloff_order_id,hour_date";
			if (!$hourData = $this->conn->getAll($sql)) {
        		if ($this->conn->ErrorNo() != 0 ) {
        			echo "no data for coo ??? ";
					die($sql);
           		die($this->conn->ErrorMsg());
        		}
      	}

      	$this->lineCount =4;
      	$loopCount = 1;
			$prevCoo = -1;
      	foreach($hourData as $ind=>$val) {
         	extract($val);
         	if ($loopCount == 1 || $prevCoo != $calloff_order_id ) {
            	$cntArr = explode("|",$well_ids);
            	$wellCount = count($cntArr);
            	$budgetName = $this->getBudget($calloff_order_id);
					$prevCoo = $calloff_order_id;
         	}
         	$hour_date = Functions::dbDate($hour_date);
         	$total_t1 = floatval($total_t1);
         	$total_t2 = floatval($total_t2);
         	$expense = floatval($expense);
         	if ($docket_hours_t1 > 0 ) {
            	$dktHours =  $docket_hours_t1;
         	}
         	else {
            	$dktHours =  $docket_hours_t2;
         	}
         	if ($total_t1 > 0 ) {
            	$total = $total_t1;
         	}
         	else if ($total_t2 > 0 ) {
            	$total = $total_t2;
         	}
         	else if ($expense > 0 ) {
             	$total = $expense;
         	}
         	$dktHours = $dktHours / $wellCount;
         	$total = round(($total / $wellCount),2);
         	$tmpArr = array($con_name,$hour_date,$calloff_order_id,$budgetName,$docket_num,$opname,$plant_unit,$p_type,$crew_name,$sub_crew_name,$area_name,$this->wellName,$dktHours,$rate,$total,$wellCount);
         	fputcsv($this->fp,$tmpArr,",",'"');
         	$this->lineCount ++;
         	$loopCount ++;
      	}
      	fputcsv($this->fp,array());
         fputcsv($this->fp,array("","","","","","","","","","","","",'=ROUND(SUM(M5'.':M'.$this->lineCount.'),2)',"",'=ROUND(SUM(O5'.':O'.$this->lineCount .'),2)'));
         //$this->lineCount ++;
         //$this->cooTotalsStr .="P$this->lineCount,";


	}
	 private function getBudget($coo) {
      $sql = "SELECT budget_name from budget join calloff_order using(budget_id) where calloff_order_id = $coo";
      if (! $budget_name = $this->conn->getOne($sql)) {
         if ( $this->conn->ErrorNo() != 0 ) {
            die($this->conn->ErrorMsg());
         }
         else {
            $budget_name = "";

         }
      }
      return $budget_name;
   }

}
?>
