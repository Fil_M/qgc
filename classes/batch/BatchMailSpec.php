<?php     // QGC 
 ini_set('display_errors','1');
 error_reporting(E_ALL);
 class BatchMailSpec {
	public $fromHeader;  // subdomains
	public $company; // 
	public $conn;
	public $attachments;  // array of pathnames
	public $message;
	public $emailLogRow;   // 
   public $emailLogID;
	public $contacts=array(); //  array of actual emails sent
	public $messageID; // of sent email 
	public $noCompanyEmail = false;
	public $fault;
	public $cooLine;
	public $ID;
	public $recID;  // Uswed when  ID doesn't equal primary Key of  Table
	public $QGCID;
	public $QGCDomain;
	public $fromAddress;
	public $companyEmail;
	public $conName;
	public $emailType;
	public $requestEstimateID;
	public $dktDate;
	public $contractorID;
	public $origSubject;
	public $dblink;
	public $sentCompanyEmails;
	public $sentReceiverEmails;
	public $patterns= array('XXXCompanyDomainXXX');
	public $messText;
	public $BASE;
	public $employeeID;
	public $emailTypeDesc;

	public function __construct($email_log_id) {
		global $conn, $BASE;
		$this->conn=$conn;
		$this->BASE=$BASE;
		$this->emailLogID = $email_log_id;
		$this->getEmailLog();  // gets  and  sets  Email public members	
		$this->messText  =  "<div style=\"clear:both;\"></div>\n<div class=\"email_div\"  >\n";
      $this->messText .= "\t<div class=\"div9 line1\" >Email ID</div><div class=\"div90 padl\" >$this->emailLogID - $this->emailTypeDesc\n\t</div>\n";
		$arr=Functions::getLogo();  // compnay  domain  from sub_domain  in  Company Table
		$this->QGCDomain =$arr['domain'];
		$this->doEmail("company");  // being  QGC
		//$this->doEmail("receiver");  //  being  everyone else
		$this->makeDiv();
		echo "$this->emailLogID\n";
	}

	public function doEmail($recType) {
		global $BASE;
		$rec = ucfirst($recType);
		$this->messText  .=  "<div style=\"clear:both;\" ></div><div class=\"div9 line1\" >Sent $rec Emails</div><div class=\"div90 padl\" >";
		$this->getRule($this->emailType);  // for  bodytext  etc   Part 1  Rule
		if(! $this->contacts = getEmailContacts($recType,$this)) { // from table email_recipient   ?? Part two Rule
         $this->setResult(false,"No $rec Email Contacts Found");
			return false;
      }
		$this->messText  .=  "</div>\n";  // close div


      $attachments = $this->getAttachments($recType);    ///  // gets at least array(1)   except  APP
		$loop =1;
      if (!is_null($attachments)) {
      	foreach($attachments as $key=>$attach ) {
         	$this->message = Swift_Message::newInstance();   //  i only  one email to  contractor
         	$this->attachments = $attach;
				$this->contacts = $this->contacts === true ? explode("|",$key) : $this->contacts;  //  Timesheest return  true  so  use  attachment array  for recipients
         	if ( $key == "all" ) {  // send  to all
            	$this->sentCompanyEmails = pg_escape_string(implode("|",$this->contacts));  // str for sent_comapny_emails
            	$this->message->setTo($this->contacts);
            	if (!empty($attach)) {
              		$attachArr = explode ("|",$attach);
               	foreach ($attachArr as $toAttach) {
                  		$this->message->attach(Swift_Attachment::fromPath($toAttach)->setDisposition('inline'));
               	}
            	}
         	}
         	else {  // send  to individual
					$this->contacts = explode("|",$key); 
            	$this->sentCompanyEmails = pg_escape_string($key);  // str for sent_comapny_emails
            	$this->message->setTo($this->contacts);
            	if (!empty($attach) ) {
              		$attachArr = explode ("|",$attach);
               	foreach ($attachArr as $toAttach) {
                  		$this->message->attach(Swift_Attachment::fromPath($toAttach)->setDisposition('inline'));
               	}
            	}
         	}
         	$bodytxt = bodyText($recType,$this);  // may alter Subject   ********  dynamic include rule  ***********

         	$this->message->setSubject($this->origSubject);
				$this->message->setFrom(array($this->fromAddress=>$this->company['company_name'] ));


         	$this->message->setBody($bodytxt,'text/html');
         	$this->setEmailHeaderID();

         	$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
         	$mailer = Swift_Mailer::newInstance($transport);
         	$result = $mailer->send($this->message); 

				//$result = 1;
         	if (intval($result) >= 1 ) {
            	$this->setResult(true,"Sent ($result) successfully to: ". str_replace("|",", ",$this->sentCompanyEmails) .".");
         	}
         	else {
            	$this->setResult(false,"Email failure ???");
            	return false;
         	}

         	$this->doEmailLog($recType);
				$loop++;
      	}
			$this->updateEmailLog("OK"); //  need historical  "OK"  in email_log for QGC  "emailed" status
		}
   }

	public function getRule() {
		$sql = "SELECT email_rule_include from email_rule where receiver_type = '$this->emailType'";
		if (! $inc = $this->conn->getOne($sql)) {
			die($sql);
		}
		require_once("{$this->BASE}$inc");
	}  

	public function getEmailLog() {  // get  email  log and  contractor details
		$sql = "SELECT el.*,c.con_name,c.domain,c.db_link,sir.site_ins_required_id ,er.full_description
		from email_log el 
		LEFT JOIN contractor c on c.contractor_id = el.receiver_id
		LEFT JOIN site_ins_required sir on sir.instruction_no = el.dkt_id 
		LEFT JOIN email_rule er using(receiver_type) 
		where email_log_id = $this->emailLogID";
		if (! $this->emailLogRow = $this->conn->getRow($sql)) {
				echo "fails here $sql\n";
				die($this->conn->ErrorMsg());
		}
		$this->getCompanyDetails($this->emailLogRow['company_id']);  // Sets  domain  and  QGC Name
      $this->ID = $this->emailLogRow['dkt_id']; // generic
      $this->recID = $this->emailLogRow['site_ins_required_id']; // generic
      $this->emailType = $this->emailLogRow['receiver_type'];
      $this->contractorID = $this->emailLogRow['receiver_id'];
      $this->origSubject = $this->emailLogRow['subject'];
      $this->dktDate = Functions::dbDate($this->emailLogRow['dkt_date']);
		$this->conName = $this->emailLogRow['con_name'];
		$this->conDomain = $this->emailLogRow['domain'];
		$this->dbLink = $this->emailLogRow['db_link'];
		$this->cooLine = $this->emailLogRow['coos'];
		$this->employeeID = $this->emailLogRow['employee_id'];
		$this->emailTypeDesc = $this->emailLogRow['full_description'];
	}

	public function getCompanyDetails($companyID) {
		$sql = "SELECT  company_id,company_name,company_logo,a.email,sub_domain from company c 	
		JOIN address_to_relation using (company_id)
      JOIN address a using (address_id)
		where company_id = $companyID";
		if (! $this->company = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
		}
		$this->fromAddress = $this->company['sub_domain'];
      $this->conName = $this->company['company_name'];
		$this->companyEmail = $this->company['email'];
	}

	public function setResult($type,$text) {
		   $this->messText  .=  "$text ";
         $this->fault =  $text;
	}
	public function setEmailHeaderID() {  // sets this->messageID
      $messageID = $this->message->getHeaders()->get('Message-ID');
      $this->messageID = $messageID->toString();

   }
	public function doEmailLog($type) {   // actually now  insert
      if ($type == "company" ) {
         $sql = "INSERT into email_result (email_log_id,sent_company_emails,company_message_id,attachments,company_fault) 
         values($this->emailLogID,E'$this->sentCompanyEmails','$this->messageID','$this->attachments','$this->fault')";
      }
		else {
         $sql = "INSERT into email_result (email_log_id,sent_receiver_emails,receiver_message_id,attachments,receiver_fault )
         values($this->emailLogID,E'$this->sentCompanyEmails','$this->messageID','$this->attachments','$this->fault')";
		}
      if (! $res= $this->conn->Execute($sql)) {
         die($this->conn->ErrorMsg());
      }
		// update email_attachment as having been sent  !!!! may need  undoing ONLY FOR ATW !!!!!
		if ( $this->emailType == "ATW" || $this->emailType == "CPL"  ) {
			$sql = "UPDATE email_attachment set email_sent = true,attach_date = current_date where email_log_id = $this->emailLogID and email = E'$this->sentCompanyEmails'";
      	if (! $res= $this->conn->Execute($sql)) {
         	die($this->conn->ErrorMsg());
      	}
		}
   }
	public function updateEmailLog($text) {  // Need OK  in  fault  for  QGC
		$sql = "UPDATE email_log set fault = '$text' where email_log_id = $this->emailLogID";
      if (! $res= $this->conn->Execute($sql)) {
         die($this->conn->ErrorMsg());
      }
	}
	public function makeDiv() {
		$this->messText  .=  "\t</div><!-- close email div -->\n";  // close email_div
		$sql = "INSERT into tmp_message values(nextval('tmp_message_tmp_message_id_seq'), now() + interval '1 day',$this->employeeID,'$this->messText',false,$this->emailLogID) returning tmp_message_id";
      if (! $tmpMessID= $this->conn->getOne($sql)) {
         die($this->conn->ErrorMsg());
      }
	}
	
	 public function getAttachments($type) {  // determines number of emails  to be  sent
      $attachment= array();
		// Attachment excptions  ie  send  no  email to
		if ($type == "company" && ($this->emailType == 'APP' || $this->emailType == "ST2" || $this->emailType == "TB2"  )) {
      	return NULL;   //  No  contacts  no  bodytext no attachments means  no email   for hour approval  and  superintendent table 2  internal and external
		}
		else if ( $type == "receiver"  &&  $this->emailType == "ATW" ) {
      	return NULL;   //  No  contacts  no  bodytext no attachments means  no email   ATW  receiver 
		}

		if ($this->contacts) {  // means  get  from  email_attach
			$emailStr = "";
		}
		else {   // $this->contacts  is  array
      	$emailStr =  " and email in ('" . implode("','",$this->contacts). "','all')";  ///  make  in specific string 
		}
      $sql = "SELECT email,attachment_name from email_attachment where email_log_id = $this->emailLogID  and recipient_type = '$type' and email_sent is false $emailStr  order by email";
      if (! $data= $this->conn->getAll($sql)) {
         if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
         else {
            return array("all"=>"");
         }
      }
      $prevEmail = "";
      foreach ($data as $key=>$val) {
         if ($val['email'] != $prevEmail ) {
            $attachment[$val['email']] = $val['attachment_name'];
            $prevEmail = $val['email'];
         }
         else {
            $attachment[$val['email']] .= "|" . $val['attachment_name'];
         }

      }
      return $attachment;
   }
}
?>
