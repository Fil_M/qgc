<?php    // All scripts need to be  run  from  batch
   error_reporting(E_ALL);
   ini_set('display_errors','1');
	if (count($argv)  < 4 ) {
      die("Not enough parameters\n StartDate EndDate Email EmpID ");
   }
   else {
      $startDate = $argv[1];
      $endDate = $argv[2];
      $email = $argv[3];
      $empID = $argv[4];
   }
	
	$_SESSION=array();  // Dummy  for  batch 
   $curDir = getcwd();
	$BASE = dirname(dirname($curDir)) ."/";
	require_once("{$BASE}include/database/dbconfig.php");   //  $_SESSION['dbarr']  containd  contractor connect info serialized
	require_once("{$BASE}classes/Functions.php");   //  $_SESSION['dbarr']  containd  contractor connect info serialized
	require_once("{$BASE}email/lib/swift_required.php");
	require_once("BatchMail.php");
	require_once("BatchWellReport.php");

	$obj = new BatchWellReport($startDate,$endDate,$email,$empID);

?>
