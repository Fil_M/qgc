<?php    // All scripts need to be  run  from  batch
   error_reporting(E_ALL);
   ini_set('display_errors','1');
	if (count($argv)  < 3 ) {
      die("Not enough parameters\n  EmpID Area Well");
   }
   else {
      $empID = $argv[1];
      $areaname = $argv[2];
      $wellname = $argv[3];
   }
	
	$_SESSION=array();  // Dummy  for  batch 
	$_SESSION['HTTP_HOST'] = 'N/A';  // Dummy  for  batch 
   $curDir = getcwd();
	$BASE = $curDir ."/";
	require_once("{$BASE}include/database/dbconfig.php");   //  $_SESSION['dbarr']  containd  contractor connect info serialized
	require_once("{$BASE}classes/Functions.php");   //  $_SESSION['dbarr']  containd  contractor connect info serialized
	require_once("{$BASE}email/lib/swift_required.php");
	require_once("{$BASE}classes/batch/BatchMail.php");
	require_once("{$BASE}classes/batch/BatchZipReport.php");

	$obj = new BatchZipReport($empID,$areaname,$wellname);

?>
