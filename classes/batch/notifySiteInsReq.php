<?php    // All scripts need to be  run  from  batch
	$_SESSION=array();  // Dummy  for  batch 
   $curDir = getcwd();
	$BASE = dirname(dirname($curDir)) ."/";
	require_once("{$BASE}include/database/dbconfig.php");   //  $_SESSION['dbarr']  containd  contractor connect info serialized
	require_once("{$BASE}classes/Functions.php");   //  $_SESSION['dbarr']  containd  contractor connect info serialized
	require_once("{$BASE}email/lib/swift_required.php");
	require_once("BatchMail.php");

   error_reporting(E_ALL);
   ini_set('display_errors','1');
	$conArr = unserialize($_SESSION['dbarr']); //  $_SESSION['dbarr']  containd  contractor connect info serialized

																// index  contractor_id =>  [0] = dbLink [1] = db Name [2] = shortName [3] = Contractor Full Name 

   // notification Date in site_ins_required
	$sql = "Select site_ins_required_id,contractor_id,calloff_order_id,instruction_no,create_date,notification_date,emailed_notification
		from site_ins_required 
		where notification_date <= current_date 
		and emailed_notification is false";

	if (! $data = $conn->getAll($sql)) {
		if ($conn->ErrorNo() != 0 ) {
			die($conn->ErrorMsg());
		}
		else {
			die("No OVERDUE Required Site Instructions Found\n");
		}

	}
	// Found some  interrogate Contractor database for made site instruction No,
	foreach($data as $ind=>$val) {
		extract($val);
		$dbLink = $conArr[$contractor_id][0];
		$sql = "SELECT 'TRUE' from site_instruction where calloff_order_id = $calloff_order_id and instruction_no = '$instruction_no'";
		$res = Functions::execCon($dbLink,$sql);

		if ($res == "TRUE" ) {
			echo "Site Instruction Found. $instruction_no Updating ...\n";
			updateSiteInsReq($site_ins_required_id);  // set emailed_notification to True prevents sql above  finding  same
		}
		else {
			echo "Site Instruction NOT Found. $instruction_no Emailing ...\n";
			emailNotification($instruction_no,$calloff_order_id,$contractor_id,$create_date);
			updateSiteInsReq($site_ins_required_id);  // set emailed_notification to True prevents sql above  finding  same
		}


	}

	function updateSiteInsReq($sir_id) {
		global $conn;
		$sql = "UPDATE site_ins_required set emailed_notification = true where site_ins_required_id = $sir_id";
		if (! $res = $conn->Execute($sql)) {
			die($conn->ErrorMsg());
		}

	}

	function emailNotification($ins_no,$coo_id,$con_id,$cre_date) {
	 	$eNum = Functions::insertEmailLog(-1,0,$con_id,"OVERDUE Request for Site Instruction Estimate for Instruction Number $ins_no",'REO',$ins_no,$cre_date,$coo_id);
		$em = new BatchMail($eNum);
	}

?>
