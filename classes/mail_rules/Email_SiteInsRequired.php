<?php
	// Target Cost Estimate
	function bodyText($type,$obj) {
			$html = "<html>";
			if ($type == "company" ) {  //Sender in this case QGC 
				$html .= "A Request for a Site Instruction - Number: $obj->ID has been issued to $obj->conName<br />";
            $html .= "You will be notified on $obj->dktDate if $obj->conName has NOT responded to this request.";
         } 
			else {
			   $html .= "QGC has issued a Request for a Site Instruction -  Number: $obj->ID, please login to your portal to respond <br />";
            $html .= "<a href=\"https://$obj->QGCDomain/admin/site_instruction.php?action=new&site_num=$obj->ID&coo_id=$obj->cooLine\" >Add this Site Instruction in the Admin Portal</a>";
			}
			$html .= "</html>";
			return $html;


	}

	function getEmailContacts($type,$obj) {
      $contactArr = array();//cont2.qgc-app.net.au/admin/index.php;
		if ($type == "company") {
			$res = Functions::emailsFromSiteReq($obj->ID,$obj->cooLine);
		}
		else {  // CONTRACTOR !!!!! across dblink get data from contractors email_recipient table
     		$sql = "SELECT emails from email_recipient where recipient_type = 'company' and form_type = '$obj->emailType'";  // No  exclusions  will fail  if  no email_recipient record  found
			$res = Functions::execCon($obj->dbLink,$sql);
		}
     	$contacts =  explode("|",$res);
     	foreach ($contacts as $key=> $val ) {
        	if (strlen($val) > 2 ) {
           	$contactArr[]  = $val;
        	}
     	}
     	if (count($contactArr) > 0 ) {
        	return $contactArr;
     	}
     	else {
        	return false;
     	}
	}

	
?>
