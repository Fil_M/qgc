<?php
	// QGC ATW 
	function bodyText($type,$obj) {
			$html = "<html>";
			$URL="https://qgc-app.net.au";
		//  find indiviualised  username/password  for third party  contacts	

			$sql = "SELECT at.*,to_char(meeting_time,'HH12:MIam DayDD MonthYYYY') as dat,a.area_name,wells_from_ids(at.well_ids) as well_name  from atw at
          LEFT JOIN area a using(area_id)
          where atw_id = $obj->ID";
			 if (! $data = $obj->conn->getRow($sql)) {
				echo $sql;
				die($obj->conn->ErrorMsg());
			 }
			 $areaName = $data['area_name'];
			 $wellStr = $data['well_name'];
			 $meetDate = preg_replace('/\ \ /'," ",$data['dat']);  // clean up DB  padding  a bit
			 $siteCon = $data['site_contact'];
			 $siteConFone = $data['site_contact_fone'];
			 $atwID = $data['atw_id'];
			 $atwNumber = $data['atw_number'];
			 $notes = $data['notes'];


			$sql = "SELECT * from atw_document where atw_id = $atwID";
         if (! $documentLines = $obj->conn->getAll($sql)) {
            if ($obj->conn->ErrorNo() != 0 ) {
               die($obj->conn->ErrorMsg());
            }
         }
			$html = "Authority To Work Pre-construction Meeting: &nbsp;<span style=\"font-weight:bold;\" >$areaName $wellStr</span><br />";
			$html .= "At: &nbsp;<span style=\"font-weight:bold;\" >$meetDate</span><br />Site Contact: &nbsp;<span style=\"font-weight:bold;\" >$siteCon</span>";
			$html .= " &nbsp;Phone: &nbsp;<span style=\"font-weight:bold;\" >$siteConFone</span><br /><br />";
			$html .= "Notes: &nbsp;<span style=\"font-style:italic;\" >$notes</span><br /><br />";
			$html .= "Documents include:<br />";
			$x=0;
			if (count($documentLines) > 0 )  {
				foreach($documentLines as $ind=>$val) {
					$sub = chr(97 + $x). " ) ";
					$doc = $val['document_type']; // preceeding  atw_  
					$pos = strpos($doc,"_");
					$docName =  substr($doc,$pos,strlen($doc) - $pos);
					$html .= " &nbsp;$sub<span style=\"font-weight:bold;\" > &nbsp;$docName</span><br />";
					$x+=1;
				}
			}
			$html .="<br /><br /><div style=\"width:100%;text-align:center;\" >";
			$html .= "<div style=\"margin:auto:width:300px;\" >To access this site, you must use either Google Chrome, Firefox or Safari to view the documents.<br />
			You can right click and copy this link and then paste into the browser bar of Chrome, Firefox or Safari if these browsers aren’t set as your default browser.</div><br />";
			$html .= "<div style=\"margin:auto:width:200px;\" ><a href=\"{$URL}/admin/atw.php?action=show&atw_id=$atwID\" >To View Documents and Download</a></div></div>\n"; 

			$html .= "</html>";
			return $html;
	}


	function getEmailContacts($type,$obj) {
      return true;  // Use  attachments
	}
	
?>
