<?php
	// Calloff Order
	function bodyText($type,$obj) {
			$html = "<html>";
			$data =  Functions::tceFromCOO($obj->ID);
			extract($data);
			$obj->requestEstimateID = $request_estimate_id;
			if ($type == "company" ) {  //Sender in this case QGC 
				$html .= "<div style=\"width:100%;\" >";
				$html .= "QGC has issued a C.O.O. number $obj->ID, in response to Cost Estimate number $request_estimate_id<br /><br />";
				$html .= "<div><div style=\"font-weight:bold;width:150px;float:left;\" >Work Scope:</div><div style=\"margin-left:10px;float:left;\" >$crew_name</div></div><br />";
				$html .= "<div><div style=\"font-weight:bold;width:150px;float:left;\" >Sub Scopes:</div><div style=\"margin-left:10px;float:left;\" >$subscopes</div></div><br />";
				$html .= "<div><div style=\"font-weight:bold;width:150px;float:left;\" >Area:</div><div style=\"margin-left:10px;float:left;\" >$area_name</div></div><br />";
				$html .= "<div><div style=\"font-weight:bold;width:150px;float:left;\" >Well/s:</div><div style=\"margin-left:10px;float:left;\" >$well_name</div></div><br />";
				$html .= "<div><div style=\"font-weight:bold;width:150px;float:left;\" >Services:</div><div style=\"margin-left:10px;float:left;\" >$services</div></div><br />";
				$html .= "<div><div style=\"font-weight:bold;width:150px;float:left;\" >Remarks:</div><div style=\"margin-left:10px;float:left;\" >$remarks</div></div><br /><br />";
				$html .= "Contractor: $obj->conName may now start entering hours against this C.O.O.<br /><br />";
            $html .= "<a href=\"https://$obj->QGCDomain/admin/calloff.php?action=find&calloff_id=$obj->ID\" >View this C.O.O. in Admin</a>";
				$html .= "</div>\n";
         } 
			else {
				$html .= "QGC has issued a C.O.O. number $obj->ID, in response to Cost Estimate number $obj->requestEstimateID. You may now start entering hours against this C.O.O.<br />";
            $html .= "<a href=\"$obj->conDomain/admin/calloff.php?action=find&calloff_id=$obj->ID\" >View this C.O.O. in Admin</a>";

			}
			$html .= "</html>";
			return $html;


	}

	function getEmailContacts($type,$obj) {
      $contactArr = array();
		if ($type == "company") {
			$res = Functions::emailFromCOOIDs($obj->ID);
		}
		else {  // across dblink get data from contractors email_recipient table
     		$sql = "SELECT emails from email_recipient where recipient_type = 'company' and form_type = '$obj->emailType'";  // No  exclusions  will fail  if  no email_recipient record  found
			$res = Functions::execCon($obj->dbLink,$sql);
		}
     	$contacts =  explode("|",$res);
     	foreach ($contacts as $key=> $val ) {
        	if (strlen($val) > 2 ) {
           	$contactArr[]  = $val;
        	}
     	}
     	if (count($contactArr) > 0 ) {
        	return $contactArr;
     	}
     	else {
        	return false;
     	}
	}

	
?>
