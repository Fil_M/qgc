<?php
	// QGC Completion Certificate
	function bodyText($type,$obj) {
			$html = "<html>";
			if ($type == "company" ) {  //Sender in this case ContractorM
				$html .= "Completion Certificate $obj->ID for {$obj->conName}. <br />";
         } 
			else {
				$obj->origSubject = "QGC has approved Completion Certificate $obj->ID";  // Changes  from Invoice line  sent  to QGC Invoices  and  attaches  Completion  Cert  not  invoice
				$html .= "QGC has approved Completion Certificate $obj->ID <br />";
            $html .= "<a href=\"$obj->conDomain/admin/complete.php?action=find&comp_id=$obj->ID\" >View Certificate in  Admin</a>";
			}
			$html .= "</html>";
			return $html;


	}

	function getEmailContacts($type,$obj) {
      $contactArr = array();
      if ($type == "company") {
         return true;  // Use  attachments
      }
      else {  // across dblink get data from contractors email_recipient table
         $sql = "SELECT emails from email_recipient where recipient_type = 'company' and form_type = '$obj->emailType'";  // No  exclusions  will fail  if  no email_recipient record  found
         $res = Functions::execCon($obj->dbLink,$sql);
      }
      $contacts =  explode("|",$res);
      foreach ($contacts as $key=> $val ) {
         if (strlen($val) > 2 ) {
            $contactArr[]  = $val;
         }
      }
      if (count($contactArr) > 0 ) {
         return $contactArr;
      }
      else {
         return false;
      }
   }

	
?>
