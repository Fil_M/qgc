<?php
	// Target Cost Estimate
	function bodyText($type,$obj) {
			$html = "<html>";
			if ($type == "company" ) {  //Sender in this case QGC 
				$html .= "An <span style=\"color:red;\" >OVERDUE</span> Request for a Site Instruction - Number: $obj->ID has been issued to {$obj->conName}. This has <b>NOT</b> been completed!<br />";
            $html .= "This Site Instruction was originally requested on $obj->dktDate and has <b>NOT</b> yet been responded to.";
            $html .= "<br /><br /><a href=\"https://{$obj->QGCDomain}/admin/site_ins_req.php?action=find&sitereq_id=$obj->recID\"  >View Required Site Instruction</a>\n";
         } 
			else {
			   $html .= "<span style=\"color:red;\" >QGC has issued a Request for a Site Instruction -  Number: $obj->ID, originally issued to you on $obj->dktDate, please login to your portal to respond </span><br />";
            $html .= "<a href=\"$obj->conDomain/admin/site_instruction.php?action=new&site_num=$obj->ID&coo_id=$obj->cooLine\" >Add this Site Instruction in the Admin Portal</a>";
			}
			$html .= "</html>";
			return $html;


	}

	function getEmailContacts($type,$obj) {
      $contactArr = array();//qgc-app.net.au/admin/index.php;
		if ($type == "company") {
			$res = Functions::emailsFromSiteReq($obj->ID,$obj->cooLine);
		}
		else {  // CONTRACTOR !!!!! across dblink get data from contractors email_recipient table
     		$sql = "SELECT emails from email_recipient where recipient_type = 'company' and form_type = '$obj->emailType'";  // No  exclusions  will fail  if  no email_recipient record  found
			$res = Functions::execCon($obj->dbLink,$sql);
		}
     	$contacts =  explode("|",$res);
     	foreach ($contacts as $key=> $val ) {
        	if (strlen($val) > 2 ) {
           	$contactArr[]  = $val;
        	}
     	}
     	if (count($contactArr) > 0 ) {
        	return $contactArr;
     	}
     	else {
        	return false;
     	}
	}

	
?>
