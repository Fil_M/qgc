<?php
	// Target Cost Estimate
	function bodyText($type,$obj) {
			$html = "<html>";
			if ($type == "company" ) {  //Sender in this case QGC 
				$html .= "A Request for a Target Cost Estimate number $obj->ID has been issued to $obj->conName<br />";
            $html .= "<a href=\"https://$obj->QGCDomain/admin/cost_estimate.php?action=find&request_id=$obj->ID\" >View this Cost Estimate Request in Admin</a>";
         } 
			else {
			   $html .= "QGC has issued a Request for Cost Estimate number $obj->ID, please login to your portal to respond <br />";
            $html .= "<a href=\"$obj->conDomain/admin/field_estimate.php?action=new&request_id={$obj->ID}&type=cost\" >View this Cost Estimate Request in Admin</a>";
			}
			$html .= "</html>";
			return $html;


	}

	function getEmailContacts($type,$obj) {
      $contactArr = array();//cont2.qgc-app.net.au/admin/index.php;
		if ($type == "company") {
			$res = Functions::emailFromTCEIDs($obj->ID);
		}
		else {  // across dblink get data from contractors email_recipient table
     		$sql = "SELECT emails from email_recipient where recipient_type = 'company' and form_type = '$obj->emailType'";  // No  exclusions  will fail  if  no email_recipient record  found
			$res = Functions::execCon($obj->dbLink,$sql);
		}
     	$contacts =  explode("|",$res);
     	foreach ($contacts as $key=> $val ) {
        	if (strlen($val) > 2 ) {
           	$contactArr[]  = $val;
        	}
     	}
     	if (count($contactArr) > 0 ) {
        	return $contactArr;
     	}
     	else {
        	return false;
     	}
	}

	
?>
