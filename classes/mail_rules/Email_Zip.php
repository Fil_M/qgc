<?php
	// QGC ATW 
 ini_set('display_errors','1');
 error_reporting(E_ALL);

	function bodyText($type,$obj) {
			$html = "<html>";
         if ($type == "company" ) {  //Sender in this case QGC 
         	$sql = "SELECT area_name,well_name from zip where zip_id = $obj->ID";
          	if (! $data = $obj->conn->getRow($sql)) {
            	echo $sql;
            	die($obj->conn->ErrorMsg());
          	}
				extract($data);
            $html .= "A Zip File is available for Download for Area: $area_name / $well_name <br />";
            $html .= "<a href=\"https://$obj->QGCDomain/admin/zip.php?action=list&zip_id=$obj->ID\" >Download this zip file</a>";
         }

			$html .= "</html>";
			return $html;
	}


	function getEmailContacts($type,$obj) {
		// needs an array of ONE employee email
		$sql = "SELECT email_from_ids('$obj->employeeID')";
		if (!$empEmail = $obj->conn->getOne($sql)) {
			echo "No Employee email found for $obj->employeeID\n";
			die($obj->conn->ErrorMsg());
		}
		$empEmail = trim(preg_replace('/,/',"",$empEmail));
		return array($empEmail);
	}
	
?>
