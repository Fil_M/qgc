<?php

class Functions {
		static function dbDate($date) {
         $dbdate= NULL;
         if ( strlen($date) > 0 ) {
            $arr = explode("-",$date);
            $dbdate = $arr[2] . "-" .$arr[1] . "-" .$arr[0];
         }
         return $dbdate;
      }
		static function dbTime($time) {
         $dbtime= NULL;
         if ( strlen($time) > 0 ) {
				$dtPart = explode(" ",$time);
				$dbtime = AdminFunctions::dbDate($dtPart[0]) . " " . substr($dtPart[1],0,8);
         }
         return $dbtime;
		}
		static function beginMonth() {
			 $now = time();
   		while (date('d',$now) != '01') {
      		$now -= 86400;
   		}
  			 return date('d-m-Y',$now);
		}
		static function yesterday() {
			return  date('d-m-Y',time() - 86400);
		}
		static function endMonth() {
			$now = time();
   		while (date('d',$now) != '01') {
      		$now += 86400;
   		}
         $now -= 86400;
  			return date('d-m-Y',$now);
		}
		static function beginLastMonth() {
          $now = time();
         while (date('d',$now) != '01') {
            $now -= 86400;
         }
          $now -= 86400;
         while (date('d',$now) != '01') {
            $now -= 86400;
         }
          return date('d-m-Y',$now);
      }
      static function endLastMonth() {
          $now = time();
         while (date('d',$now) != '01') {
            $now -= 86400;
         }
         $now -= 86400;
          return date('d-m-Y',$now);
      }
	static function getQuarterTime() {
		$hr = date('H',time());
		$tm = intval(date('i',time()));
		if ($tm < 8 || $tm > 52 ) {
			return "$hr:00";
		}
		elseif ($tm > 7 && $tm < 23) {
			return "$hr:15";
		}
		elseif($tm > 22 && $tm < 38 ) {
			return "$hr:30";
		}
		elseif ($tm > 37 && $tm < 53 ) {
			return "$hr:45";
		} 

	}
	static function Zip($source, $destination, $include_dir = false) {
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }
    if (file_exists($destination)) {
        unlink ($destination);
    }
    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }
    $source = str_replace('\\', '/', realpath($source));
    if (is_dir($source) === true) {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
        if ($include_dir) {
            $arr = explode("/",$source);
            $maindir = $arr[count($arr)- 1];
            $source = "";
            for ($i=0; $i < count($arr) - 1; $i++) {
                $source .= '/' . $arr[$i];
            }
            $source = substr($source, 1);
            $zip->addEmptyDir($maindir);
        }
        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);
            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) ) {
                continue;
            }
            $file = realpath($file);
            if (is_dir($file) === true) {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true) {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true) {
        $zip->addFromString(basename($source), file_get_contents($source));
    }
    return $zip->close();
	}
	 static function showError($message) {
          $len = strlen($message);
          $width = "width:" .$len * 0.4 . "rem;";
          $_SESSION['email_message']  =  "<div style=\"clear:both;\"></div>\n<div class=\"email_div\"  >\n";
          $_SESSION['email_message'] .= "<div style=\"margin-left:auto;margin-right:auto;margin-top:0.75rem;$width\" >\n";
          $_SESSION['email_message'] .= "<span class=\"error cntr\"  >$message</span>\n";
          $_SESSION['email_message'] .= "</div>\n";
          $_SESSION['email_message'] .= "</div>\n";
    }
	 static function getConfig($sysVal) {
         $conn = $GLOBALS['conn'];
         $confArr = array();
         if (! isset($_SESSION['config'])) {
            $sql = "SELECT sys_name,sys_val from system";
            if (! $confArr = $conn->getAssoc($sql)) {
               die($conn->ErrorMsg());
            }
            $_SESSION['config'] = serialize($confArr);
            return $confArr[$sysVal];
         }
         else {
            $dat = unserialize($_SESSION['config']);
            return $dat[$sysVal];
         }
    }
	static function getQGCURL() {
      global $conn;
		$sql = "SELECT sys_val from system where sys_name = 'QGC_URL'";
      if (! $URL = $conn->getOne($sql)) {
         	die($conn->ErrorMsg());
		}
		return $URL;
	}

	static function getConEst($cooID,$dblink) {
      global $conn;
      $sql = "with st as (select coalesce(sum(si.estimate_total),0) as sitest from site_instruction si 
		LEFT JOIN field_estimate fe using(site_instruction_id)
		where si.calloff_order_id = $cooID
		and fe.qgc_approve_date is not null)
		select sitest + coalesce(cost_estimate,0) as totest from calloff_order co ,st  
		where co.calloff_order_id = $cooID";
		$sql = pg_escape_string($sql);
      $stmt = "SELECT * from dblink('$dblink','$sql',true) AS t1(prime double precision)" ;
      if (! $cooTot = floatval($conn->getOne($stmt))) {
			if ($conn->ErrorNo() != 0 ) {
         	die($conn->ErrorMsg());
			}
			else {
				$cooTot = 0;
			}
      }

      return $cooTot;
   }
	 static function getSig($empID) {
         $conn = $GLOBALS['conn'];
         $sql = "SELECT signature from employee where employee_id = $empID";
         if (! $signature = $conn->getOne($sql)) {
            if ($conn->errorNo() != 0 ) {
               die($conn->ErrorMsg());
            }
            else {
               $signature = "";
            }
         }
         return $signature;
      }
	 static function getEmpName($empID) {
         $conn = $GLOBALS['conn'];
         $sql = "SELECT firstname|| ' ' || lastname   as emp_name from employee where employee_id = $empID";
         if (! $empName = $conn->getOne($sql)) {
            if ($conn->errorNo() != 0 ) {
               die($conn->ErrorMsg());
				}
				else {
					die("not found");
				}
         }
         return $empName;
      }

	 static function getConName($conID) {
         global $conn;
         $sql = "SELECT name,con_name,domain,db_link from contractor where contractor_id = $conID";
         if (! $conarr=$conn->getRow($sql)) {
            if ($conn->ErrorNo() != 0 ) {
               die ($conn->ErrorMsg());
            }
            else {
               $conarr = array("name"=>NULL,"con_name"=>"UNKNOWN ??","domain"=>NULL,"db_link"=>NULL);
            }
         }
         return $conarr;
    }

	static function sysValFromShortname($shortName,$sysName) {
         global $conn;
         $sql = "SELECT sys_val from {$shortName}_system where sys_name = '$sysName'";
			return $conn->getOne($sql);
	}
	static function addAttachWorkRecord($pdfName,$startDate) {
      global $conn;
		$sql = "SELECT email_log_id from email_log where dkt_date = '$startDate' and receiver_type = 'WRS'";
		$emailID = intval($conn->getOne($sql));
		if ($emailID == 0 ) {
			$sql = "INSERT into email_log (company_id,receiver_type,subject,attachments,dkt_date) values (0,'WRS','Work Records All Contractors - $startDate','$pdfName','$startDate') returning email_log_id";
			if (! $res = $conn->Execute($sql)) {
				die($conn->ErrorMsg());
			}
		  $emailID = $res->fields['email_log_id'];
		}
		else {
			$sql = "SELECT attachments like '%$pdfName%' from email_log where email_log_id = $emailID";  // only add if  not  there
			$res = $conn->getOne($sql);
			if ($res == "f" ) {
				$sql = "UPDATE email_log set attachments = attachments ||'|'||'$pdfName' where email_log_id = $emailID";
				if (! $res = $conn->Execute($sql)) {
					die($conn->ErrorMsg());
				}
			}
		}
		return $emailID;
	}
	static function getAllCrew() {
		global $conn;
		$crew = array();
		$sql = "select distinct(substring(crew_name from 0 for 6)) as crewpart  from crew where removed is false order by crewpart";
      if (! $crewarr=$conn->getAll($sql)) {
          if ($conn->ErrorNo() != 0 ) {
             die ($conn->ErrorMsg());
           }
      }
		foreach($crewarr as $ind=>$val) {
			$part = $val['crewpart'];
			$sql = "select crew_id,crew_name from crew where crew_name like '$part%' order by crew_id";
			$firstCrew = $conn->getRow($sql);
			$crew[$firstCrew['crew_id']] = array("crew_name"=>$firstCrew['crew_name'],"crew_in_clause"=>"");
			$sql = "select array_to_string(array(select crew_id from crew where crew_name like '$part%' and removed is false order by 1),',','*') as crews";
			$instr = $conn->getOne($sql);
			$crew[$firstCrew['crew_id']]['crew_in_clause'] = $instr;
		}

		return $crew;

	}
	static function getNewCrew() {
		global $conn;
		$crew = array();
		$sql = "select crew_id,crew_name from crew where removed is false order by crew_id";
      if (! $crewarr=$conn->getAll($sql)) {
          if ($conn->ErrorNo() != 0 ) {
             die ($conn->ErrorMsg());
           }
      }
		foreach($crewarr as $ind=>$val) {
			extract($val);
			$sql = "select array_to_string(array(select sub_crew_id from sub_crew where crew_id = $crew_id and removed is false order by 1),',','*') as crews";
			$instr = $conn->getOne($sql);
			$crew[$crew_id]= array("crew_name"=>$crew_name,"crew_in_clause" => $instr);
		}

		return($crew);
	}
	static function areaNameFromID($areaID) {
         global $conn;
			$sql = "SELECT area_name from area where area_id = $areaID";
			return $conn->getOne($sql);
	}
	static function getAllCon($conID=0,$override=false) {
         global $conn;
			$removedClause = " WHERE 1 = 1";
			if (!$override) {
				$removedClause = " WHERE removed is false ";
			}
			$andClause = !empty($conID) ? " and contractor_id = $conID " : "";
         $sql = "SELECT contractor_id,name,con_name from contractor $removedClause $andClause order by con_name";
         if (! $conarr=$conn->getAll($sql)) {
            if ($conn->ErrorNo() != 0 ) {
               die ($conn->ErrorMsg());
            }
         }
         return $conarr;
   }
	static function getExtCon() {
      global $conn;
		$sql = "SELECT contractor_id,name,con_name from contractor  where removed is false
				  UNION
				  SELECT ext_contractor_id,'EXTERNAL',con_name from ext_contractor
				  order by con_name";
      if (! $conarr=$conn->getAll($sql)) {
         if ($conn->ErrorNo() != 0 ) {
            die ($conn->ErrorMsg());
         }
      }

         return $conarr;
	}
	static function insCon($dblink,$sql) { // dblink single record insert with primary key return
      global $conn;
      //SELECT * from dblink('qgc_cor','Insert into request_estimate (contractor_id,area_id,well_id,created_by,other_text) values (2,22,14,2,''This is a test'') returning request_estimate_id') AS t1(request_estimate_id integer)
      $sql = pg_escape_string($sql);
      $stmt = "SELECT * from dblink('$dblink','$sql',true) AS t1(prime integer)" ;
      if (! $resID = $conn->getOne($stmt)) {
         die($conn->ErrorMsg());
      }
      return $resID;
   }
   static function execCon($dblink,$sql) { // dblink  with rows affected/result returned 
      global $conn;
      $sql = pg_escape_string($sql);
      $stmt = "SELECT * from dblink('$dblink','$sql',true) AS t1(answer text)" ;
      if (! $res = $conn->getOne($stmt)) {
			if(intval($res) == 0 ) {
				return null;
			}
         echo $stmt;
         die($conn->ErrorMsg());
      }
      return $res;
   }
   static function execConFloat($dblink,$sql) { // dblink  with result affected returned 
      global $conn;
      $sql = pg_escape_string($sql);
      $stmt = "SELECT * from dblink('$dblink','$sql',true) AS t1(tce_est double precision,perc_est double precision)" ;
		
      if (! $res = $conn->getRow($stmt)) {
			if(floatval($res) == 0 ) {
				return 0;
			}
         echo $stmt;
         die($conn->ErrorMsg());
      }
      return $res;
   }
   static function exec2Con($dblink,$sql) { // dblink_exec with NO rows returned 
      global $conn;
      $sql = pg_escape_string($sql);
      $stmt = "SELECT dblink_exec('$dblink','$sql',true)" ;
      if (! $res = $conn->getOne($stmt)) {
         die($conn->ErrorMsg());
      }
      return $res;
   }
	 static function getDbLink($conID) {
	   global $conn;
		$sql = "SELECT db_link,domain,name from contractor where contractor_id = $conID";
		if (! $dblnk = $conn->getRow($sql)) {
			die($conn->ErrorMsg());
		}
		return $dblnk;
	 }
   static function postToString($post) {
		// flatten a multi ..[] $_POST to a bar separated string for compacted index values
		$st = "";
		foreach($post as $val) {
			$st .= $val ."|";
		}
		$st = preg_replace('/\|$/',"",$st);
		return $st;
	}
	static function verify() {
		 if (!isset($_SESSION['verified'])) { $_SESSION['verified'] = "false"; }
		if ($_SESSION['verified'] !== 'true')  {
      	$_SESSION['user_unknown'] = 'false';
      	$_SESSION['pwd_unknown'] = 'false';
			$_SESSION['last_request'] = $_SERVER['REQUEST_URI'];
      	header("Location: login.php");
      	exit;
   	}	
	}
	static function processImages($arr,$key,$name) {
		if (intval($arr['error'][$key]) > 0) {
			return $arr['error'][$key];
		}
		if (!move_uploaded_file($arr['tmp_name'][$key], $name)) {
       return "Can't upload file";
		}
			return "OK";
   }
	static function states() {
      $arr=array("1"=>"A.C.T.","2"=>"N.S.W.","3"=>"N.T.","4"=>"QLD","5"=>"S.A.","6"=>"TAS","7"=>"VIC","8"=>"W.A");
      return $arr;
   }

	static function getLogo() {
		global $conn;
		$arr = array();
		$sql = "Select company_name,company_logo,sub_domain from company where company_id = 0";
      $rs = $conn->Execute($sql);
		$sub = $rs->fields['sub_domain'];
		$domain = str_replace("info@","",$sub);  // psuedo email
      $arr['title'] =  ucfirst($rs->fields['company_name']);
		$arr['logo'] =  $rs->fields['company_logo'];
		$arr['domain'] =  $domain;
		return $arr;
	}
	static function getAddress($ID=0,$addressType="company") {
		global $conn;
		$addr = new Address;
		$sql = "select ad.*,s.state_name from address ad join address_to_relation ar using (address_id) LEFT JOIN state s using (state_id) where ar.company_id = $ID";
      $rs = $conn->Execute($sql);
      $addr->address1 = $rs->fields['address_line_1'];
      $addr->address2 = $rs->fields['address_line_2'];
      $addr->suburb = $rs->fields['suburb'];
      $addr->postcode = $rs->fields['postcode'];
      $addr->phone = trim($rs->fields['telephone']);
      $addr->fax = trim($rs->fields['fax']);
      $addr->state_name = $rs->fields['state_name'];
		return $addr;
	}
	static function rateTypes() {
		$conn = $GLOBALS['conn'];
		$sql = "select * from rate_type";
		$data = $conn->getAll($sql);
		return $data;
	}
	static function employees() {
		$conn = $GLOBALS['conn'];  // Hardcoded company 2 !!
		$sql = "select employee_id,firstname || ' ' || lastname as fullname  from employee where removed is false  and company_id in (0,2)  and employee_id > 2 order by lastname";
		return $conn->getAll($sql);
	}
	static function profiles() {
		$conn = $GLOBALS['conn'];
		$sql = "select * from profile order by profile_id";
		return $conn->getAll($sql);
	}
	static function plant_types() {
		$conn = $GLOBALS['conn'];
		$sql = "select * from plant_type order by p_type";
		return $conn->getAll($sql);
	}
	static function tceFromCOO($coo) {
		$conn = $GLOBALS['conn'];
		$sql = "select services,remarks,crew_name,subscopes_from_ids(co.sub_crew_ids) as subscopes,area_name,wells_from_ids(co.well_ids) as well_name,request_estimate_id
		FROM calloff_order co
		LEFT JOIN crew c using(crew_id)
		LEFT JOIN area a using(area_id)
		WHERE calloff_order_id =  $coo";
		return $conn->getRow($sql);
	}
	static function emailFromEmpID($empID) {  // returns  any give QGC  employee's email
		$conn = $GLOBALS['conn'];
		$sql ="SELECT  email from employee
         LEFT JOIN address_to_relation ar using(employee_id)
         LEFT JOIN address a using (address_id)
         Where employee_id  = $empID";

      if (! $email = $conn->getOne($sql)) {
			if ($conn->ErrorNo() != 0) {
         	die($conn->ErrorMsg());
			}
			else {
				return false;
			}
      }
      return $email;
	}
	static function supgroupFromCOO($coo) {
		$conn = $GLOBALS['conn'];
		$sql = "SELECT supervisor_group_id from calloff_order where calloff_order_id = $coo";
		return $conn->getOne($sql);
	}
	static function emailFromSupGroupID($supGroupID) {
		$conn = $GLOBALS['conn'];
		$sql = "SELECT group_emails from supervisor_group where supervisor_group_id = $supGroupID";
		return $conn->getOne($sql);
	}
	static function emailFromTCEIDs($reqID) {
		$conn = $GLOBALS['conn'];
		$sql = "SELECT  email_from_ids(email_emp_ids) as emails  from request_estimate where request_estimate_id = $reqID";
		return preg_replace('/,\ /',"|" ,$conn->getOne($sql)); // need bar  separated emails
	}
	static function emailFromCOOIDs($cooID) {
		$conn = $GLOBALS['conn'];
		$sql = "SELECT  email_from_ids(email_emp_ids) as emails  from calloff_order where calloff_order_id = $cooID";
		return  preg_replace('/,\ /',"|" ,$conn->getOne($sql));
	}
	static function emailsFromSiteReq($siteNum,$cooID) {
		$conn = $GLOBALS['conn'];
		//  1. Get emails  allocated  when making  request
		$sql = "SELECT  email_from_ids(supervisor_ids) as emails from site_ins_required where instruction_no = $siteNum";
		if (! $res=$conn->getOne($sql)) {
			if ($conn->ErrorNo() != 0 ) {
				die($conn->ErrorMsg());
			}

		}
		$reqStr = preg_replace('/,\ /',"|" ,$res);
		// 2.  Add emailRecipient
		$sql = "SELECT emails from email_recipient where recipient_type = 'company' and form_type = 'REQ'"; // Site ins Req
		if (! $res1=$conn->getOne($sql)) {
			if ($conn->ErrorNo() != 0 ) {
				die($conn->ErrorMsg());
			}

		}
		return $reqStr . "|" . $res1; // add the  two  together
	}
	static function plant_rates($plantID,$date) {
		$conn = $GLOBALS['conn'];
		$sql = "select * from plant_rates($plantID,$date)";
		if (! $data = $conn->getRow($sql)) {
			if ($conn->ErrorNo() != 0 ) {
				die($conn->ErrorMsg());
			}
		}
		if (count($data) > 0 ) {
			$ar = array(array('type'=>'Normal','rate'=>$data['plant_rate']),array('type'=>'StandDown','rate'=>$data['stand_rate']));
			return $ar;
		}
		else {
			return NULL;
		}

	}
	static function insertEmailLog($employee_id,$company_id,$receiver_id,$subject,$type,$dktID,$dktDate=NULL,$cooLine=NULL,$supGroupID="NULL") {
		$dateStr = !is_null($dktDate) ? "'".$dktDate ."'" : "NULL";
		$cooStr = !is_null($cooLine) ? "'".$cooLine ."'" : "NULL";
      $conn = $GLOBALS['conn'];
		$subject = pg_escape_string($subject);
      $sql = "INSERT into email_log (employee_id,company_id,receiver_id,receiver_type,subject,dkt_id,dkt_date,coos,supervisor_group_id) 
      values ( $employee_id,$company_id,$receiver_id,'$type',E'$subject',$dktID,$dateStr,$cooStr,$supGroupID) returning email_log_id";

      if (! $rs=$conn->Execute($sql)) {
         echo "$sql <br />";
         die($conn->ErrorMsg());
      }
      return $rs->fields['email_log_id'];
   }


}
?>
