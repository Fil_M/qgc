<?php
 	class Page {
		public $page;
		public $pageType;
		public $level;
		public $logo;

		public function	__construct($pageType,$level=1) {
			$this->pageType = $pageType;
			$this->level = $level;
			$this->setHead();
			$this->setHeader();
			$this->setContent();
			$this->setMenu();
			$this->setFooter();
		}


		private function setHead() {
   //<link rel=\"stylesheet\" type=\"text/css\" href=\"css/mobiscroll.ios.css\" media=\"screen\" /> 
			$arr = Functions::getLogo();
			$pTitle = $arr['title'];
			$this->logo = $arr['logo'];
			$extraCSS = "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/{$this->pageType}.css\" media=\"screen\" /> ";

			$head = "<!DOCTYPE html>\n
			<head>
			   <title>$pTitle</title>
			   <meta charset=\"UTF-8\">
			   <meta name=\"viewport\" content=\"width=768, minimum-scale=1.0, maximum-scale=2.0\" />
			   <link rel=\"stylesheet\" type=\"text/css\" href=\"css/ipad.css\" media=\"screen\" /> 
			   <link rel=\"stylesheet\" type=\"text/css\" href=\"css/mobiscroll.custom-2.4.4.min.css\" media=\"screen\" /> 
			   <link rel=\"stylesheet\" type=\"text/css\" href=\"css/jquery.keypad.css\" media=\"screen\" /> 
				$extraCSS
				<script src=\"js/jquery-1.9.0.js\" type=\"text/javascript\"></script>
				<script src=\"js/jquery.signaturepad.min.js\" type=\"text/javascript\"></script>
				<script src=\"js/json2.min.js\" type=\"text/javascript\"></script>
				<script src=\"js/mobiscroll-2.4.4.custom.min.js\" type=\"text/javascript\"></script>
				<script src=\"js/jquery.autocomplete.js\"></script>
				<script src=\"js/functions.js\" type=\"text/javascript\"></script>
			</head>";
			$this->page=$head;
		}

		private function setHeader() {
			$addr = Functions::getAddress();
			$header = "\n<body>\n<div id=\"wrap\">\n<div id=\"header\">\n\t<div id=\"header_logo\"><img src=\"$this->logo\" alt=\"logo\"/></div>";
			$header .= "\n\t<div class=\"header_title_div\"><div class=\"header_text\">##HEADER_TEXT##</div></div>";
      	$header .= "\n\t<div id=\"header_address\">";
         $header .= "\n\t\t<div class=\"address_line\">$addr->address1</div>";
         $header .= "\n\t\t<div class=\"address_line\">$addr->address2</div>";
         $header .= "\n\t\t<div class=\"address_line\">$addr->suburb&nbsp;$addr->state_name&nbsp;$addr->postcode</div>";
         $header .= "\n\t\t<div class=\"address_line\">&nbsp;</div>";
         $header .= "\n\t\t<div class=\"address_line left\">PHONE:</div><div class=\"address_line right\"><pre>$addr->phone</pre></div>";
         $header .= "\n\t\t<div class=\"address_line left\">FAX:</div><div class=\"address_line right\"><pre>$addr->fax</pre></div>";
      	$header .= "\n\t</div><!-- header_address -->
			 \n</div> <!-- header -->\n";
			$this->page .= $header;
		}

		private function setContent() {
        $content = "\n<div id=\"content\">\n<div id=\"main\">\n##MAIN##\n</div> <!-- main -->";
		  $this->page .= $content;

		}
		private function setMenu() {
			$menu = new Menu($this->pageType,$this->level);
		   $this->page .= $menu->div;
		}
	
		private function setFooter() {
			$footer = "\n<div id=\"footer\"></div>\n</div> <!-- wrap -->\n</body>\n</html>";
			$this->page .= $footer;
		}

	}
?>
