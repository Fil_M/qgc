<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class SigPDF {
	public $conn;
	public $data;

	public function __construct() {
		$this->conn = $GLOBALS['conn'];
		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Signature");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
		$Y = 40;
		$image_file = 'images/simpson1.png';
      $pdf->Image($image_file, 30, $Y,0,36 , 'PNG', '', 'T', true, 300, '', false, false, 0, false, false, false);
		$image_file = 'images/aisling1.png';
		$Y += 40;
      $pdf->Image($image_file, 30, $Y,0,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$image_file = 'images/gesler1.png';
		$Y += 40;
      $pdf->Image($image_file, 30, $Y,0,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$image_file = 'images/drummond1.png';
		$Y += 40;
      $pdf->Image($image_file, 30, $Y,0,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

		$name = "tmp/sig.pdf";
		$pdf->Output($name, 'I');
   }		
}
?>
