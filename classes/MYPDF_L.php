<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once('classes/tcpdf/config/lang/eng.php');
require_once('classes/tcpdf/tcpdf.php');
require_once('classes/Functions.php');

class MYPDF_L extends TCPDF {

    //Page header
    public function Header() {
		$arr = Functions::getLogo();
		$addr = Functions::getAddress();
      // Logo
      $this->Image($arr['logo'], 0, 5,36 , '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
      // Set font
      $this->SetFont('helvetica', '', 10);
      // Title
      $this->MultiCell(0, 0, $addr->address1, 0, 'L', false, 2, 250, 0);
      $this->MultiCell(0, 0, $addr->address2, 0, 'L', false, 2, 250);
      $this->MultiCell(0, 0, $addr->suburb.' '.$addr->postcode, 0, 'L', false, 2, 250);
      $this->Ln();
      $this->MultiCell(15, 0, 'PHONE:', 0, 'L', false, 0, 250);
      $this->MultiCell(30, 0, $addr->phone, 0, 'L', false, 2, 265);
      $this->MultiCell(15, 0, 'FAX:', 0, 'L', false, 0, 250);
      $this->MultiCell(30, 0, $addr->fax, 0, 'L', false, 2, 265);
      $this->SetLineStyle(array("width"=>0.5));
      $this->Line(0, 28, 296, 28);
		$this->SetY(29);
    }
}
?>
