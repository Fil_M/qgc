<?php
 	class Index {
		public $conn;
		public $page;
		public $pageType='index';

		public function	__construct() {
			$this->conn = $GLOBALS['conn'];
			$pg = new Page('index');
			$this->page= $pg->page;
		 	$this->setHeaderText();	
		 	$this->setContent();	
			echo $this->page;
		}

		private function setHeaderText() {
			global $header_text;
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent() {
		 $content = "";
         if (isset($_SESSION['email_message'])) {
            $content .= "<div id=\"fullwidth\" >".$_SESSION['email_message'] ."</div>";
            unset($_SESSION['email_message']);
         }
         $content .= "  <div id=\"backImg\"></div>";
         $this->page = str_replace('##MAIN##',$content,$this->page);
         
		}

	}
?>
