<?php

 	class Login {
		public $conn;
		public $page;
		public $pageType='login';

		public function	__construct() {
			$this->conn = $GLOBALS['conn'];
			$pg = new Page('login');
			$this->page= $pg->page;
		 	$this->setHeaderText();	
		 	$this->setContent();	
         echo $this->page;
		}

		private function setHeaderText() {
			$this->page = str_replace('##HEADER_TEXT##','Login',$this->page);
		}

		private function setContent() {
			$content = '';
			if ($_SESSION['locked'] === 'true' ) {
				$_SESSION['locked'] = 'false';
            $content .= '<span style="color:red">This account shown as Locked</span>';
			}
			else if($_SESSION['user_unknown'] === 'true'){
            $_SESSION['user_unknown'] = 'false';
            $content .=  '<span style="color:red">Username unknown. Please try again</span>';
         }
         else if ($_SESSION['pwd_unknown'] === 'true') {
            $_SESSION['pwd_unknown'] = 'false';
            $content .= '<span style="color:red">Password Incorrect. Please try again</span>';
         }

			$content .= "\t<div id=\"main_login\">
       <form name=\"input\" action=\"login.php\" method=\"post\">
       <label for=\"name\" class=\"label\" style=\"margin-left:40px;font-size:110%;\">Username:</label> 
		<div style=\"clear:both;\"></div>
		<div style=\"width:100%;text-align:center;\"><input type=\"text\" name=\"user\" class=\"input\"  style=\"margin:auto;\" size=\"20\" /></div>
		<div style=\"clear:both;height:20px;\"></div>
       <label for=\"pwd\" class=\"label\" style=\"margin-left:40px;font-size:110%;\">Password:</label> 
		<div style=\"clear:both;\"></div>
		<div style=\"width:100%;text-align:center;\"><input type=\"password\" name=\"pwd\" class=\"input\" size=\"20\"/></div>
       <div id=\"login_submit\"><input  type=\"submit\" value=\"Log In\" class=\"button_login\" /></div>
       </form>
     </div>";

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	}
?>
