<?php  // QGC
error_reporting(E_ALL);
ini_set('display_errors','1');
class SupWorkRecordPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $startDate;
	public $endDate;
	public $cooID;
	public $conID;
	public $areaID;
	public $wellID;
	public $wellName;
	public $contractorName;
	public $shortName;
	public $estimate;
	public $totalAll;
	public $numWells;
	public $percComplete;
	public $dbLink;
	public $totToDate;
	public $color=array("0"=>array(255.0,238.0,65.0),"1"=>array(65.0,119.0,255.0),"2"=>array(255.0,152.0,65.0),"3"=>array(255.0,65.0,181.0),"4"=>array(160.0,65.0,255.0),"5"=>array(65.0,255.0,243.0),
   "6"=>array(197.0,255.0,65.0),"7"=>array(110.0,80.0,80.0),"8"=>array(127.0,127.0,127.0),"9"=>array(205.0,229.0,229.0),"10"=>array(255.0,65.0,65.0),"11"=>array(86.0,255.0,65.0));



	public function __construct($action="",$startDate,$endDate,$conID) {
		$this->conn = $GLOBALS['conn'];
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->conID = $conID;

		$this->getDetails($conID);
		// create new PDF document
		$pdf = new MYPDFLAND("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Works Records $this->startDate - $this->endDate");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$prevareaName = $prevwellName = $precrewName = $prevCOOID = -1;
		$details = "";
		//T1
		$table = "Table 1";
		$printHeading = true;
		foreach ($this->data as $ind =>$val ) {
			$cooID = $val['calloff_order_id'];
			$areaID = $val['area_id'];
			$wellName = $val['well_name'];
			$areaName = $val['area_name'];
			$crewName = $val['crew_name'];
			$details = $val['details'];
			$id = $val['hour_id'];
			if (intval($cooID) < 1 ) {
				die("No Call Off Order Found for  $id");
			}
			

			if ($cooID != $prevCOOID  && $prevCOOID != -1) {
				$this->doTotals($pdf,$subTotal,$Y,$prevwellName,$prevareaName,$precrewName,$hDate,$id,$prevCOOID);
				$printHeading = true;
			}  
			$prevwellName = $wellName;
			$prevareaName = $areaName;
			$prevcrewName = $crewName;
			$prevCOOID = $cooID;

			if ($printHeading) {	
				$pdf->AddPage();
				$this->getEsts($cooID);
				$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
				$printHeading = false;
				$subTotal = $gst = $gTotal = 0;
				$Y = 39.5;
			}

			$emp_name = "";	
			$plant_name = $val['p_type'];	
			$hDate = Functions::dbdate($val['hour_date']);
			$rate = $val['rate'];	
			$dktHours = $val['docket_hours_t1'];	
			$subcrew = $val['sub_crew_name'];
			$total = $val['total_t1'];	
			if (floatval($total) > 0 ) {
			 	$details = trim($val['details']);
            $height = ceil(strlen($details) /44) * 6;
            $height = $height >= 6 ? $height : 6;

				$pdf->SetFont(PDF_FONT, '', 10);
				$subTotal += $total;
				$total = "$" .number_format($total,2);	
				$pdf->setCellHeightRatio(1.2);
      		$pdf->SetFont(PDF_FONT, '', 10);
				$pdf->MultiCell(21,$height,$hDate,array('LR'=>$this->borderStyle),'L',false,0,4,$Y);
				$pdf->MultiCell(50,$height,$subcrew,array('LR'=>$this->borderStyle),'L',false,0,25,$Y);
				$pdf->MultiCell(80,$height,$plant_name,array('R'=>$this->borderStyle),'L',false,0,75);
				$pdf->MultiCell(70,$height,$details,array('R'=>$this->borderStyle),'L',false,0,155);
				$pdf->MultiCell(18,$height,$dktHours,array('R'=>$this->borderStyle),'C',false,0,225);
				$pdf->MultiCell(23,$height,$rate,array('R'=>$this->borderStyle),'C',false,0,243);
				$pdf->MultiCell(27,$height,$total,array('R'=>$this->borderStyle),'R',false,1,266);
				$Y += $height;
				if ($Y > 165 ) {
					$pdf->Line(4,$Y,293,$Y);
      			$pdf->SetFont(PDF_FONT, 'I', 12);
					$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
					$pdf->AddPage();
					$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
					$Y = 39.5;
				}
			}
		}
		$this->doTotals($pdf,$subTotal,$Y,$wellName,$areaName,$crewName,$hDate,$id,$cooID);  // Last page
		// print it
		$name = "tmp/Worksheet.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
	}
	private function tableT2T3($pdf,$Y,$wellName,$areaName,$crewName,$cooID) {
		$t2arr = $this->data;
		reset($t2arr);
		//T2
		$details = "";
		$subTotal = $gst = $gTotal = 0;
		$table = "Table 2";
		$Y += 2;
		if ($Y > 165 ) {
			$pdf->Line(5,$Y,293,$Y);
     		$pdf->SetFont(PDF_FONT, 'I', 12);
			$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
			$pdf->AddPage();
			$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID);
			$Y = 40;
		}
		$printHeading = true;
		$printTotals = false;
		foreach ($t2arr as $ind =>$val ) {
			$coo_id = $val['calloff_order_id'];
			$area_id = $val['area_id'];
			$well_name = $val['well_name'];
			$hDate = Functions::dbdate($val['hour_date']);
			$id = $val['hour_id'];
			$dktHours = $val['docket_hours_t2'];	
			if ( $cooID == $coo_id  && floatval($dktHours) > 0 ) {
				if ($printHeading) {
					$this->headingTitle($Y,$pdf,$table);
					$Y = $pdf->getY();
					$Y += 5;
					$printHeading = false;
					$printTotals = true;
				}
				$wellName = $val['well_name'];
				$areaName = $val['area_name'];
				$crewName = $val['crew_name'];
				$plant_name = $val['p_type'];	
				$subcrew = $val['sub_crew_name'];
				$rate = $val['rate'];	
				$total = $val['total_t2'];	
				$details .= $val['details'] . ", ";
				$subTotal += $total;
				$total = "$" .number_format($total,2);	
				$pdf->setCellHeightRatio(1.2);
				$details = trim($val['details']);
            $height = ceil(strlen($details) /44) * 6;
            $height = $height >= 6 ? $height : 6;

      		$pdf->SetFont(PDF_FONT, '', 10);
				$pdf->MultiCell(21,$height,$hDate,array('LR'=>$this->borderStyle),'L',false,0,4,$Y);
            $pdf->MultiCell(50,$height,$subcrew,array('LR'=>$this->borderStyle),'L',false,0,25,$Y);
            $pdf->MultiCell(80,$height,$plant_name,array('R'=>$this->borderStyle),'L',false,0,75);
            $pdf->MultiCell(70,$height,$details,array('R'=>$this->borderStyle),'L',false,0,155);
            $pdf->MultiCell(18,$height,$dktHours,array('R'=>$this->borderStyle),'C',false,0,225);
            $pdf->MultiCell(23,$height,$rate,array('R'=>$this->borderStyle),'C',false,0,243);
            $pdf->MultiCell(27,$height,$total,array('R'=>$this->borderStyle),'R',false,1,266);

				$Y += $height;
				if ($Y > 165 ) {
					$pdf->Line(5,$Y,293,$Y);
      			$pdf->SetFont(PDF_FONT, 'I', 12);
					$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
					$pdf->AddPage();
					$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID,true);
					$Y = 40;
				}
			}
			else {
				continue;
			}
		}
		if ($printTotals) {
			$details = substr($details,0,277);
			$this->doT2Totals($pdf,$subTotal,$Y,$details);  // Last page
			$Y = $pdf->getY();
         $Y += 1;
         if ($Y > 165 ) {
            $pdf->Line(4,$Y,293,$Y);
            $pdf->SetFont(PDF_FONT, 'I', 12);
            $pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
            $pdf->AddPage();
            $this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$crewName,$table,$cooID,false);
            $Y = 30;
         }

		}
		$this->pieChart($pdf,$wellName,$areaName,$table,$cooID);

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
	}
	private function doT2Totals($pdf,$subTotal,$Y,$details) {
		$pdf->Line(4,$Y,293,$Y);
      $pdf->SetFont(PDF_FONT, '', 10);
      //$Y +=2;
      $pdf->MultiCell(34,5,'Sub Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,8,'B');
      $gst = $subTotal * 0.1;
      $total = $subTotal + $gst;
      $subTotal = "$" .number_format($subTotal,2);
      $gst = "$" . number_format($gst,2);
      $total = "$" . number_format($total,2);
      $pdf->MultiCell(27,5,$subTotal,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,8,'B');
      $Y += 5;
      $pdf->MultiCell(34,5,'G.S.T.',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
      $pdf->MultiCell(27,5,$gst,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
      $Y += 5;
      $pdf->Line(225,$Y+5,293,$Y+5);
      $Y += 3;
      $pdf->MultiCell(34,8,'Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
      $pdf->MultiCell(27,8,$total,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
      $Y+=10;
      $pdf->SetFillColor(0,0,0);
      $pdf->MultiCell(68,1,'',array('LRTB'=>$this->borderStyle),'R',true,0,225,$Y,true,0,false,true,1,'B');
      $pdf->SetFillColor(241,121,61);
	}
	private function doTotals($pdf,$subTotal,$Y,$wellName,$areaName,$crewName,$hDate,$id,$cooID) {
		$pdf->Line(4,$Y,293,$Y);
     	$pdf->SetFont(PDF_FONT, '', 10);
      //$Y +=2;
		$pdf->MultiCell(34,5,'Sub Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,8,'B');
		$gst = $subTotal * 0.1;
		$total = $subTotal + $gst;
		$subTotal = "$" .number_format($subTotal,2);	
		$gst = "$" . number_format($gst,2);	
		$total = "$" . number_format($total,2);	
		$pdf->MultiCell(27,5,$subTotal,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,8,'B');
		$Y += 5;
		$pdf->MultiCell(34,5,'G.S.T',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
		$pdf->MultiCell(27,5,$gst,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
		$Y += 5;
		$pdf->Line(225,$Y+5,293,$Y+5);
		$Y += 3;
		$pdf->MultiCell(34,8,'Total',array('L'=>$this->borderStyle),'R',false,0,225,$Y,true,0,false,true,10,'B');
		$pdf->MultiCell(27,8,$total,array('LR'=>$this->borderStyle),'R',false,1,266,$Y,true,0,false,true,10,'B');
		$Y+=10;
		$pdf->SetFillColor(0,0,0);
		$pdf->MultiCell(68,1,'',array('LRTB'=>$this->borderStyle),'R',true,0,225,$Y,true,0,false,true,1,'B');
		$pdf->SetFillColor(241,121,61);
		$this->tableT2T3($pdf,$Y,$wellName,$areaName,$crewName,$cooID) ; // copy data array and do t2  same page if possible
	}
	private function pieChart($pdf,$areaName,$wellName,$table,$cooID) {
		$this->getEsts($cooID);
		$this->getTotToDate($cooID);
		$Y = $pdf->getY();
		if ($Y > 245 ) {
			$pdf->Line(5,$Y,205,$Y);
      	$pdf->SetFont(PDF_FONT, 'I', 11);
			$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
			$pdf->AddPage();
			$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$table,$cooID);
			$Y = 40;
		}
		$Y+= 1;
		//$pdf->MultiCell(51,0.25,'',array('LRTB'=>$this->borderStyle),'C',false,0,153,$Y);
		$pdf->Line(153,$Y,293,$Y);
     	$pdf->SetFont(PDF_FONT, '', 11);
		$pdf->MultiCell(120,9,"Percentage of Total Costs to Date against Contractor Estimate",array('LRTB'=>$this->borderStyle),'C',true,0,5,$Y);
		$pdf->MultiCell(100,9,"Estimate of Work completed by Contractor",array('LRTB'=>$this->borderStyle),'C',true,0,125,$Y);
		$pdf->MultiCell(68,9,"Job Total  to  date",array('LTRB'=>$this->borderStyle),'C',true,0,225,$Y);
		$Y+= 11;
		$xc = 65;
		$r = 20;
		

		$perc =round((($this->totToDate / $this->estimate) * 100),2) ;
		$actualEst = $this->estimate/$this->numWells > 0 ?  $this->estimate/$this->numWells : 1;
      $pdf->SetFont(PDF_FONT, '', 11);
		$pdf->MultiCell(80,0,"{$perc}% Percent of Estimate",0,'L',false,0,45,$Y);
		$pdf->MultiCell(75,0,$this->percComplete ."% Complete",0,'C',false,0,130,$Y);
		//$pdf->Text(5, $Y, "{$perc}% Percent of Estimate" );
		if ($this->numWells > 1 ) {
			$Y+= 4;
     		$pdf->SetFont(PDF_FONT, 'I', 10);
			$pdf->Text(45, $Y, "$this->numWells Wells. Estimate per Well: ".number_format($actualEst,2) );
		}
      $pdf->SetFont(PDF_FONT, 'B', 11);
		$tott = '$' .number_format($this->totToDate,2);
		$pdf->MultiCell(40,5,$tott,array('B'=>$this->borderStyle),'R',false,1,253,$Y);
		$Y+=25;
		// Costs against Estimate
			$rotate  =  (360  *   ($perc / 100 )) - 20 ;
		if ($perc <= 50 ) {
			$pdf->SetFillColor(255, 0, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 0, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 0, 2);
		}
		else if ($perc < 100 ){
			$pdf->SetFillColor(255, 0, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 180, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 180, 2);
		}
		else {  // 100%  done
			$pdf->SetFillColor(255, 0, 0);    // 
			$pdf->Circle($xc, $Y, $r,0,360,'F',null,array(255,0,0));
		}

		// Estimate Complete
		$xc = 170;
			$rotate  =  (360  *   ($this->percComplete / 100 )) - 20 ;
		if ($this->percComplete <= 50 ) {
			$pdf->SetFillColor(0, 255, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 0, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 0, 2);
		}
		else if ($this->percComplete < 100 ){
			$pdf->SetFillColor(0, 255, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 180, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 180, 2);
		}
		else {  // 100%  done
			$pdf->SetFillColor(0, 255, 0);    // 
			$pdf->Circle($xc, $Y, $r,0,360,'F',null,array(255,0,0));
		}
		$Y += 23;

		$pdf->Line(5,$Y,293,$Y);  

	}
	private function getDetails($conID){
	   $connArr = Functions::getConName($conID);
      $this->contractorName = $connArr['con_name'];
      $this->shortName = $connArr['name'];
		$this->dbLink = $connArr['db_link'];
		$whereClause = $this->startDate == $this->endDate ? "where h.hour_date = '$this->startDate' " :  "where h.hour_date between '$this->startDate'  and '$this->endDate' ";

		$sql = "Select a.area_name,pt.p_type,sc.sub_crew_name,c.crew_name,
      h.hour_id,h.hour_date,h.docket_hours_t1,docket_hours_t2,h.rate,h.total_t1,total_t2,h.area_id,h.details,h.calloff_order_id,wells_from_ids(h.well_ids) as well_name
		FROM {$this->shortName}_hour h
      LEFT JOIN {$this->shortName}_plant p  on h.plant_id = p.plant_id
      LEFT JOIN plant_type pt using(plant_type_id) 
      LEFT JOIN area a using (area_id)
      LEFT JOIN crew c  using (crew_id)
      LEFT JOIN sub_crew sc  using (sub_crew_id)
      $whereClause and h.hour_date between '$this->startDate'  and '$this->endDate' and h.status >= 5 and h.removed is false 
      order by h.calloff_order_id,well_name,a,area_name,h.hour_date ";


		//echo $sql;

		if (! $this->data = $this->conn->getAll($sql)) {
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
			else {
				die("dies in get details");
				die($sql);
			}
		}

		//var_dump($this->data);
		if (count($this->data) < 1 ) {
			die("<h1> NO hour records found for this date - $this->startDate - $this->endDate");
		}
	}
	private function getEsts($cooID) {
		$this->estimate = Functions::getConEst($cooID,$this->dbLink);
      $sql = "SELECT well_ids from calloff_order where calloff_order_id = $cooID";
      if (! $wells = $this->conn->getOne($sql)) {
         if ($this->conn->ErrorNo() != 0 ) {
            die($this->conn->ErrorMsg());
          }
          else {
				 die("dies in select wells  $cooID");
             die($sql);
          }
      }
      $arr = explode("|",$wells);
      $this->numWells  = count($arr);
      $sql = "select max(percentage_complete) as percentage_complete from {$this->shortName}_docket_day where docket_date <= '$this->endDate' and calloff_order_id = $cooID and percentage_complete is not null";
      if (! $data = $this->conn->getRow($sql)) {
         if ($this->conn->ErrorNo() != 0 ) {
				die("no percentage");
            die($this->conn->ErrorMsg());
			}
			else {
				var_dump($data);
            die($sql);
			}
      }
		
      $this->percComplete = intval($data['percentage_complete']);
	}
	private function getTotToDate($cooID) {
		$this->totalsArr = array();
      $this->totalAll = 0;

      $sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) + sum(coalesce(expense,0))  as tot_to_date 
      from {$this->shortName}_hour where calloff_order_id = $cooID and hour_date <= '$this->endDate'";
      if (! $this->totToDate = $this->conn->getOne($sql)) {
            if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
            else {
               $this->totToDate = 0; 
            }
      }
		//var_dump($this->totalAll);
	}
	 private function heading($pdf,$pDate,$area,$well,$crew,$table,$cooID,$showHeadings=true) {
      // Heading Title 
		$est = '$' .number_format($this->estimate);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
      $pdf->MultiCell(80, 0, 'SuperIntendent Works Record', 0, 'C', false, 2, 110,1);
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(25, 0, 'From:', 0, 'L', false, 0, 32,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'C.O.O:', 0, 'L', false, 0, 134,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(50, 0, $cooID, 0, 'L', false,0, 155);
      $pdf->SetTextColor(255,0,0);
      $pdf->MultiCell(50, 0, "($this->numWells)", 0, 'L', false,1, 166);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Contractor:' ,0,'L', false, 0, 32,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $this->contractorName, 0, 'L', false, 2, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Work Scope:' ,0,'L', false, 0, 134,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $crew, 0, 'L', false, 2, 155);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(15, 0, 'Area:', 0, 'L', false, 0, 32,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(0, 0, $area, 0, 'L', false, 0, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Estimate:' ,0,'L', false, 0, 134,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $est, 0, 'L', false, 2, 155);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Well/s:', 0, 'L', false, 0, 32,23.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(0, 0, substr($well,0,65), 0, 'L', false, 2, 53);
      $pdf->SetTextColor(0,0,0);
		$this->headingTitle(29,$pdf,$table,$showHeadings);
     	$pdf->SetTextColor(0,0,0);
	}
	private function headingTitle($Y,$pdf,$table,$showHeadings=true) {
		if ($showHeadings) {
	   	$pdf->SetFillColor(241,121,61);
      	$pdf->SetTextColor(0,0,0);
			$pdf->SetFont(PDF_FONT, '', 11);
			$pdf->MultiCell(21,0,"$table",array('LTBR'=>$this->borderStyle),'L',true,1,4,$Y);
			$Y += 5;
      	$pdf->SetFont(PDF_FONT, '', 11);
			$pdf->MultiCell(21,0,'Date',array('LTB'=>$this->borderStyle),'C',true,0,4,$Y);
			$pdf->MultiCell(50,0,'Sub Scope',array('LTB'=>$this->borderStyle),'C',true,0,25);
			$pdf->MultiCell(80,0,'Machine/Equipment/Material',array('LTB'=>$this->borderStyle),'C',true,0,75);
			$pdf->MultiCell(70,0,'Details',array('LTB'=>$this->borderStyle),'C',true,0,155);
			$pdf->MultiCell(18,0,'Hours',array('LTB'=>$this->borderStyle),'C',true,0,225);
			$pdf->MultiCell(23,0,'Rate',array('LTB'=>$this->borderStyle),'C',true,0,243);
			$pdf->MultiCell(27,0,'Amount',array('LTRB'=>$this->borderStyle),'C',true,0,266);
		}

   }

}
?>
