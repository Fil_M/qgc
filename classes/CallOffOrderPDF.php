<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class CallOffOrderPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
   public $cooID;
	public $crewName;
	public $fileName;


	public function __construct($action="",$conID,$cooID,$fileName=NULL) {
		$this->conn = $GLOBALS['conn'];
		$this->cooID = $cooID;
		$this->conID = $conID;
		$this->fileName = $fileName;
		$this->action = $action;
		//$this->empID = $_SESSION['employee_id'];


		$this->getCOODetails($this->cooID);
		extract($this->data);
		$this->crewName = $crew_name;
		$est_compl_date = Functions::dbDate($est_compl_date);
		$commencement_date = Functions::dbDate($commencement_date);
		$qgc_approve_date = Functions::dbDate($qgc_approve_date);
		$cost_estimate = "$".number_format($cost_estimate,2);
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Call Off Order $this->cooID");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$Y = 35;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(60, 0, 'Well Pad Location/s:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(165, 0, "$area_name / $well_name", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
		$Y+=10 ;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(60, 0, 'Workscope:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(55, 0, "$crew_name", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Sub Scopes:', 0, 'L', false, 0, 100,45);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(80, 0, $subcrews, array('B'=>$this->lineStyle), 'L', false,1, 125);
      $Y = $pdf->getY();
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Contractor:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55, 0, $con_name, array('B'=>$this->lineStyle), 'L', false,0, 40);
		$pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Field Estimates:', 0, 'L', false, 0, 100,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(75, 0, $field_ids, array('B'=>$this->lineStyle), 'L', false,0, 130);
		$Y+=10;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(55, 0, 'Contract Num:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(30, 0, "$contract_num", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(25, 0, 'Budget:', 0, 'L', false, 0, 75);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(50, 0, $budget_name, array('B'=>$this->lineStyle), 'L', false,0, 90);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(40, 0, 'Target Cost Estimate', 0, 'L', false, 0, 145,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(12, 0, $request_estimate_id, array('B'=>$this->lineStyle), 'L', false,1, 190);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
		$Y += 10;
      $pdf->MultiCell(40, 0, 'Commencement Date:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(30, 0, $commencement_date, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(40, 0, 'Est Completion Date:', 0, 'L', false, 0, 75);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(26, 0, $est_compl_date, array('B'=>$this->lineStyle), 'L', false,0, 115);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
		$pdf->MultiCell(40, 0, 'Contractor Estimate:', 0, 'L', false, 0, 145,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(27, 0, "$cost_estimate", array('B'=>$this->lineStyle), 'R', false, 1, 181,$Y);
      $pdf->SetTextColor(0,0,0);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(40, 0, 'Services:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(165, 0, $services, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $Y = $pdf->getY();
		$Y += 10;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(40, 0, 'Remarks:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(165, 0, $remarks, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $Y = $pdf->getY();
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(40, 0, 'Gravel Landowner:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(50, 0, $gravel_owner, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(40, 0, 'Gravel Location:', 0, 'L', false, 0, 90,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(50, 0, $gravel_location, array('B'=>$this->lineStyle), 'L', false,0, 120);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(40, 0, 'Return Distance:', 0, 'L', false, 0, 170,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(8, 0, $gravel_return, array('B'=>$this->lineStyle), 'R', false,0, 200);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(40, 0, 'Water Landowner:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(50, 0, $water_owner, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(40, 0, 'Water Location:', 0, 'L', false, 0, 90,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(50, 0, $water_location, array('B'=>$this->lineStyle), 'L', false,0, 120);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(40, 0, 'Return Distance:', 0, 'L', false, 0, 170,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(8, 0, $water_return, array('B'=>$this->lineStyle), 'R', false,0, 200);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(40, 0, 'Supervisors:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(165, 0, $supervisors, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetTextColor(0,0,0);
		$Y += 10;
      $image_file = $qgc_sig;
		if (!file_exists($image_file)) {
         $image_file="images/nosig.png";
      }
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,70 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

-
      $Y+=30;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(50,0,"Approved for QGC:",0,'L',false,0,3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(92,0,$qgc_signee,array('B'=>$this->lineStyle),'L',false,0,45);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(25, 0, 'Date:', 0, 'L', false, 0, 150);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(22, 0, $qgc_approve_date, array('B'=>$this->lineStyle), 'L', false,1, 165);


      $name = !is_null($this->fileName) ? $this->fileName : "tmp/Calloff_Order_{$contractor_id}_{$this->cooID}.pdf";
      if ($action == "print" ) {
         $pdf->Output($name, 'I');
      }
      else {  
         $pdf->Output($name, 'F');
      }

   }		
	 private function getCOODetails($cooID) {
			$connArr = Functions::getConName($this->conID);
         $this->contractorName = $connArr['con_name'];
         $this->shortName = $connArr['name'];
         $this->conDomain = $connArr['domain'] . "/";
         $sql = "SELECT gwl.location as gravel_location,gww.location as water_location,ldg.owner_name as gravel_owner,ldw.owner_name as water_owner,
			con_name,a.area_name,c.crew_name,wells_from_ids(co.well_ids) as well_name,subscopes_from_ids(co.sub_crew_ids) as subcrews,supervisors_from_ids(email_emp_ids) as supervisors,budget_name,co.* 
			,array_to_string(array(select field_estimate_id from {$this->shortName}_field_estimate where calloff_order_id = $cooID and removed is false order by field_estimate_id),', ','*') as field_ids
			from calloff_order co 
         LEFT JOIN area a using(area_id) 
         LEFT JOIN crew c using(crew_id) 
         LEFT JOIN budget using(budget_id) 
			LEFT JOIN contractor con on con.contractor_id = co.contractor_id
			LEFT JOIN gravel_water_loc gwl on gwl.gravel_water_loc_id = gravel_loc_id_1
			LEFT JOIN gravel_water_loc gww on gww.gravel_water_loc_id = water_loc_id_2
			LEFT JOIN landowner ldg on ldg.landowner_id = landowner_id_1
			LEFT JOIN landowner ldw on ldw.landowner_id = landowner_id_2
         where co.calloff_order_id = $cooID";
         if (! $this->data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }


      }

		private function heading($pdf){
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, 'QGC Well Engineering Construction', 0, 'C', false, 2, 50,3);
      $pdf->SetFont(PDF_FONT, '', 12);
      $pdf->SetTextColor(255,0,0);
      $pdf->MultiCell(100, 0, $this->crewName, 0, 'C', false, 0, 50,10);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, "CALLOFF ORDER $this->cooID", 0, 'C', false, 0, 50,17);
   }



}
?>
