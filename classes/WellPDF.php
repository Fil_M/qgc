<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class WellPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
	public $areaID;
	public $wellID;
	public $numWells;
	public $areaName;
	public $wellName;
	public $contractorName;
	public $shortName;
	public $estimate;
	public $totals=array();
	public $tmp_totals=array();
	public $totalAll= 0;
	public $callOffID;
	public $callOffStr="";
	public $startDate;
	public $endDate;
	public $by;
	public $split;
	public $color=array("0"=>array(255.0,65.0,65.0),"1"=>array(255.0,238.0,65.0),"2"=>array(86.0,255.0,65.0),"3"=>array(65.0,119.0,255.0),"4"=>array(255.0,152.0,65.0),"5"=>array(255.0,65.0,181.0),
	"6"=>array(160.0,65.0,255.0),"7"=>array(65.0,255.0,243.0),"8"=>array(197.0,255.0,65.0),"9"=>array(110.0,80.0,80.0),"10"=>array(127.0,127.0,127.0),"11"=>array(205.0,229.0,229.0));

	public function __construct($action="",$conID,$areaID=0,$wellID=0,$split,$startDate,$endDate) {
		$this->conn = $GLOBALS['conn'];
		$this->conID = $conID;
		$this->split = strlen($split) > 0 ? $split : "con";
		if ($this->conID > 0 ) {
			$this->by = "By Workscope";
			$this->split="crew";
		}
		else {
			$this->contractorName = "All Contractors";
			if ($this->split == "con" ) {
				$this->by = "By Contractor";
			}
			else {
			  $this->by = "By Workscope";
			}

		}
		$this->areaID = $areaID;
		$this->wellID = $wellID;
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		if ($this->areaID == 0 || $this->wellID == 0 ) {
			return false;
		}
		$this->getDetails();
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Well Report $this->by $this->areaName - $this->wellName");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$this->pieChart($pdf);

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
		$name = "tmp/Worksheet.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
   }		
	private function pieChart($pdf) {
		$Y = 110;
      $xc = 105;
      $r = 50;

      $cnt = count($this->totals)-1 ;
      for ($x = $cnt;$x>=0;$x-- ) {
         $perc = $rotate = $tot = 0;
         if ($x < 12 ) {
         	$colr = $this->color[$x];
			}
			else {
				$colr = array(255.0,255.0,255.0);
			}
         $itot = $this->totals[$x]['tot_to_date'];
         $iPerc = round((($itot / $this->totalAll ) * 100),2) ;
         $this->totals[$x]['ind_percent'] = $iPerc;
         $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
         for ($y = $x;$y>=0;$y--) {
            $tot += $this->totals[$y]['tot_to_date'];
         }
         $perc =round((($tot / $this->totalAll ) * 100),2) ;
         $rotate  =  (360  *   ($perc / 100 )) ;
         $pdf->PieSector($xc, $Y, $r, 0, $rotate, 'FD', true, 90);
      }


		$Y += 70;
      $pdf->SetFont(PDF_FONT, 'B', 12);
      $pdf->SetTextColor(0,0,0);
		reset($this->totals);
		if ($this->conID > 0  || ($this->conID == 0 && $this->split == "crew")) {  // single contractor
			$pdf->MultiCell(50,6,"WORKSCOPE",array('LTR'=>$this->borderStyle),'C',false,0,5,$Y);
		}
		else {
			$pdf->MultiCell(50,6,"CONTRACTOR",array('LTR'=>$this->borderStyle),'C',false,0,5,$Y);
		}
		$subTitle = $this->split == "crew" ? "SUB SCOPE" : ""; 
		$pdf->MultiCell(50,5,$subTitle,array('LT'=>$this->borderStyle),'C',false,0,55,$Y);
		$pdf->MultiCell(60,6,"PERCENTAGE of TOTAL",array('LTB'=>$this->borderStyle),'C',false,0,105,$Y);
		$pdf->MultiCell(40,6,"TOTAL",array('LTRB'=>$this->borderStyle),'C',false,0,165,$Y);
      $pdf->SetFont(PDF_FONT, '', 12);
		$Y +=6;
		$x = 0;
		$prevCrew="NONE";
      $prevCOO="";
		foreach ($this->totals as $ind=>$val) {
			$height = 5.5;
         if ($x < 12 ) {
         	$colr = $this->color[$x];
			}
			else {
				$colr = array(255.0,255.0,255.0);
			}
			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
			$iPerc = number_format($val['ind_percent'],2) ."%";
			$tot =  "$" . number_format($val['tot_to_date'],2);
			$sub_crew =  $this->split == "crew" ? $val['sub_crew_name'] : "";
			if (strlen($sub_crew) > 24) {
					$height=11;
			}

			if ($this->conID > 0  || ($this->conID == 0 && $this->split == "crew")) {
				$crew = $val['crew_name'];
				if (strlen($crew) > 24) {
					$height=11;
				}
				if ($crew != $prevCrew ) {
            	$prevCrew = $crew;
        	 	}
         	else {
            	$crew = "";
         	}
				$pdf->MultiCell(50,$height,$crew,array('LTRB'=>$this->borderStyle),'L',true,0,5,$Y,true,0,false,true,$height,'M');
			}
			else {
				$contractor = $this->totals[$x]['contractor_name'];
				$pdf->MultiCell(50,$height,$contractor,array('LTRB'=>$this->borderStyle),'L',true,0,5,$Y,true,0,false,true,$height,'M');
				
			}
			$pdf->MultiCell(50,$height,$sub_crew,array('LTRB'=>$this->borderStyle),'L',true,0,55,$Y,true,0,false,true,$height,'M');
			$pdf->MultiCell(60,$height,$iPerc,array('LTRB'=>$this->borderStyle),'C',true,0,105,$Y,true,0,false,true,$height,'M');
			$pdf->MultiCell(40,$height,$tot,array('LTRB'=>$this->borderStyle),'R',true,0,165,$Y,true,0,false,true,$height,'M');
			$Y +=$height;
			$x++;


		}
		

      $pdf->SetFont(PDF_FONT, 'B', 12);
		$pdf->MultiCell(40,0,'TOTAL',0,'R',false,0,120,$Y,true,0,false,true,10,'B');
		$tot = '$' .number_format($this->totalAll,2);
		$pdf->MultiCell(40,10,$tot,array('B'=>$this->borderStyle),'R',false,1,165,$Y,true,0,false,true,10,'B');
		$Y += 11;
		$pdf->Line(165,$Y,205,$Y);


	}
	private function getDetails(){
		$dateClause = "";
		$sql = "SELECT area_name from area where area_id = $this->areaID";

		if (! $nameArr = $this->conn->getRow($sql)) {
				echo "dies here  $sql";
				die($this->conn->ErrorMsg());
		}
		$this->areaName = $nameArr['area_name'];


		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$dateClause = " and hour_date between '$this->startDate'  and '$this->endDate' ";

		}
		if ($this->conID > 0 ) {  // single contractor
	   	$Arr = Functions::getConName($this->conID);
      	$this->contractorName = $Arr['con_name'];
			$connArr[] = array("name"=>$Arr['name'],"con_name"=>$Arr['con_name']);
      	//$this->shortName = $connArr['name'];
			//$conArr = array("name"=>$Arr['con_name']);
		}
		else {
			$connArr = Functions:: getAllCon(NULL,true);  // override show removed contractors
		}
		$this->totalAll = 0; //  add wellnum values
		if ($this->split == "crew") {
			foreach($connArr as $ind=>$val) {
				$conID = $val['contractor_id'];
			   $shortName = $val['name'];
				$conName = $val['con_name'];
				$cooArr = $this->getCOOs($conID,$this->areaID,$this->wellID);
				if (count($cooArr) > 0 )  {
					foreach($cooArr as $i=>$v) {
						extract($v);
						$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0))  + sum(coalesce(expense,0)) as tot_to_date,
         			ch.crew_id,crew_name, ch.sub_crew_id,sub_crew_name, wells_from_ids(ch.well_ids) as well_name,ch.calloff_order_id
         			from {$shortName}_hour ch
         			LEFT JOIN crew c using(crew_id)
         			LEFT JOIN sub_crew sb using(sub_crew_id)
         			where status >= 5 and ch.removed is false
         			and calloff_order_id = $calloff_order_id 
         			$dateClause 
         			group by ch.crew_id,crew_name,well_name,ch.sub_crew_id,sub_crew_name,ch.calloff_order_id
         			order by ch.crew_id";


						if (! $totals = $this->conn->getAll($sql)) {
							if($this->conn->ErrorNo() != 0 ) {
								die($this->conn->ErrorMsg());
							}
						}
						foreach($totals as $ind=>$val) {
							$crew = intval($val['crew_id']);
							$sub_crew = intval($val['sub_crew_id']);
							$subCrewName = $val['sub_crew_name'];
							$tmpval = $val['tot_to_date'];
							$this->wellName = $val['well_name'];
							$wellArr = explode(",",$this->wellName);
							$numWell = count($wellArr);
				   		$newval = $tmpval / $numWell;	
							if (array_key_exists($sub_crew,$this->tmp_totals)) {
								$this->tmp_totals[$sub_crew]['tot_to_date'] += $newval;
							}
							else {
								$this->tmp_totals[$sub_crew] = array("crew"=>$crew,"tot_to_date"=>$newval,"crew_name"=>$val['crew_name'],"sub_crew_name"=>$subCrewName,"contractor_name"=>$conName);
								if (!stristr($this->callOffStr,$val['calloff_order_id'])) {
									$this->callOffStr .= $val['calloff_order_id'] .",";
								}
							}
							$this->totalAll +=   $newval;
						}
	
					}  // foreach Call Off Order


				}  // if  COO's
			}  ///foreach  contractor 
			foreach ($this->tmp_totals as $sub_crew=>$v) {
					$this->totals[] = array("tot_to_date"=>$v['tot_to_date'],"crew_name"=>$v['crew_name'],"crew_id"=>$v['crew'],"sub_crew_id"=>$sub_crew,"sub_crew_name"=>$v['sub_crew_name'],
					"contractor_name"=>$v['contractor_name']);
			}
			//var_dump($this->totals);
		}
		else {  // Contractor Split
			foreach($connArr as $ind=>$val) {
				$conID = $val['contractor_id'];
			   $shortName = $val['name'];
				$conName = $val['con_name'];
				$cooArr = $this->getCOOs($conID,$this->areaID,$this->wellID);
				if (count($cooArr) > 0 )  {
					foreach($cooArr as $i=>$v) {
						extract($v);
						$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0))  + sum(coalesce(expense,0)) as tot_to_date, ch.calloff_order_id,wells_from_ids(ch.well_ids) as well_name
            		from {$shortName}_hour ch
            		where status >= 5 and ch.removed is false
            		and calloff_order_id = $calloff_order_id 
            		$dateClause 
            		group by ch.calloff_order_id,well_name
            		order by ch.calloff_order_id";

						if (! $totals = $this->conn->getAll($sql)) {
							if($this->conn->ErrorNo() != 0 ) {
								die($this->conn->ErrorMsg());
							}
						}
						foreach($totals as $ind=>$val) {
						$tmpval = $val['tot_to_date'] ;
						$this->wellName = $val['well_name'];
						$wellArr = explode(",",$this->wellName);
						$numWell = count($wellArr);
				   	$newval = $tmpval / $numWell;	
						if (array_key_exists($shortName,$this->tmp_totals)) {
							$this->tmp_totals[$shortName]['tot_to_date'] += $newval;
						}
						else {
							$this->tmp_totals[$shortName] = array("tot_to_date"=>$newval,"contractor_name"=>$conName);
							if (!stristr($this->callOffStr,$val['calloff_order_id'])) {
								$this->callOffStr .= $val['calloff_order_id'] .",";
							}
						}
						if (!stristr($this->callOffStr,$val['calloff_order_id'])) {
							$this->callOffStr .= $val['calloff_order_id'] .",";
				   	}
						$this->totalAll +=   $newval;
					}
	
				}  // foreachCOO
			}  // if COO's
		}  // foreach Contractor
		foreach ($this->tmp_totals as $short=>$v) {
			$this->totals[] = array("tot_to_date"=>$v['tot_to_date'],"contractor_name"=>$v['contractor_name'],"sub_crew_name"=>"");
		}
	 } // end  contractor  split

	}
	 private function getCOOs($conID,$areaID,$wellID) {
		$sql = "SELECT calloff_order_id from calloff_order where contractor_id = $conID and area_id = $areaID and $wellID in (select (unnest(string_to_array(well_ids::text,'|')::int[])))";
		if (! $cooArr = $this->conn->getAll($sql)) {
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
			else {
				return array();
			}
		}

		return $cooArr;
	}
	 private function heading($pdf) {
      // Heading Title 
		$this->callOffStr = preg_replace('/,$/',"",$this->callOffStr);	
		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$pDate = "$this->startDate  to $this->endDate";
		}
		else {
			$pDate = "ALL DATES";
		}
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
      $pdf->MultiCell(120, 0, "Well Report - $this->by", 0, 'C', false, 2, 45,1);
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(25, 0, 'From:', 0, 'L', false, 0, 37,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'C.O.O:', 0, 'L', false, 0, 37,23.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $this->callOffStr, 0, 'L', false,0,58 );
		//$pdf->SetTextColor(255,0,0);
      //$pdf->MultiCell(50, 0, "($this->numWells)", 0, 'L', false,1, 142);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Contractor:' ,0,'L', false, 0, 37,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $this->contractorName, 0, 'L', false, 2, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Total:' ,0,'L', false, 0, 115,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, number_format($this->totalAll,2), 0, 'L', false, 2, 130);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(0, 0, 'Area:', 0, 'L', false, 0, 37,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $this->areaName, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Well:', 0, 'L', false, 0, 115,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(0, 0, $this->wellName, 0, 'L', false, 2,130 );
      $pdf->SetTextColor(0,0,0);

	}

}
?>
