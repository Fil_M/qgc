<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class GravelWaterPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID=0;
	public $areaID;
	public $wellID;
	public $areaName;
	public $wellName;
	public $contractorName;
	public $shortName;
	public $estimate;
	public $totals=array();
	public $tmp_totals=array();
	public $totalAll= 0;
	public $callOffID;
	public $startDate;
	public $endDate;
	public $by;
	public $Type;
	public $split;
	public $sub = 0;
	public $tot = 0;
	public $subCost = 0;
	public $totCost = 0;
	public $contractors = array();
	public $showDate;
   public $landOwner;
	public $xOffset = 0;

	public $color=array("0"=>array(255.0,65.0,65.0),"1"=>array(255.0,238.0,65.0),"2"=>array(86.0,255.0,65.0),"3"=>array(65.0,119.0,255.0),"4"=>array(255.0,152.0,65.0),"5"=>array(255.0,65.0,181.0),
	"6"=>array(160.0,65.0,255.0),"7"=>array(65.0,255.0,243.0),"8"=>array(197.0,255.0,65.0),"9"=>array(110.0,80.0,80.0),"10"=>array(127.0,127.0,127.0),"11"=>array(205.0,229.0,229.0));

	public function __construct($action="",$startDate,$endDate,$type,$conID,$areaID,$landowner,$show_date) {
		$this->conn = $GLOBALS['conn'];
		$this->areaID = $areaID;
		$this->conID =  $conID;
		$this->landOwner =  $landowner;
		$this->showDate =  $show_date;
		$this->xOffset = $this->showDate == "summ" ?  -10 : 0;
		$this->Type = $type;
		if ($this->areaID > 0 ) {
			$this->areaName = Functions::areaNameFromID($this->areaID);
			$this->by = " - $this->areaName";
		}
		else if ($this->conID == 0 ) {
			$this->by = " - All Contractors";
		}
		else {
			$connArr = Functions::getConName($this->conID);
         $conName = $connArr['con_name'];
			$this->by = " - $conName";
		}

		$this->startDate = $startDate;
		$this->endDate = $endDate;

		$this->getDetails();
		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDFLAND("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("$this->Type Report $this->by ");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$this->headingTitle(29,$pdf);
		$this->showLines(34,$pdf);

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
		$name = "tmp/greavel_water.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
   }		
	private function doTotals($Y,$pdf) {
		 if ($Y>180) {
                  $pdf->AddPage();
                  $this->heading($pdf);
                  $this->headingTitle(29,$pdf);
                  $Y = 34;
      }
		//$pdf->SetFillColor(241,196,173);
      //$pdf->SetFont(PDF_FONT, 'B', 10);
      $pdf->SetFillColor(241,121,61);
     	$pdf->MultiCell(21,5,number_format($this->tot,2),array('LTRB'=>$this->borderStyle),'R',true,0,250 + $this->xOffset, $Y);
     	$pdf->MultiCell(25,5,number_format($this->totCost,2),array('LTRB'=>$this->borderStyle),'R',true,0,271 + $this->xOffset,$Y);
		$Y +=6;
		$pdf->Line(250 + $this->xOffset, $Y, 296 + $this->xOffset, $Y);
	}

	private function doSubs($Y,$pdf) {
			$pdf->SetFillColor(241,196,173);
      	$pdf->MultiCell(21,5,number_format($this->sub,2),array('LB'=>$this->borderStyle),'R',true,0,250 + $this->xOffset, $Y);
      	$pdf->MultiCell(25,5,number_format($this->subCost,2),array('LRB'=>$this->borderStyle),'R',true,0,271 + $this->xOffset,$Y);

	}
	private function showLines($Y,$pdf) {
     	$pdf->SetFont(PDF_FONT, '', 10);
		$pOwner = -1;
		//var_dump($this->totals);
		foreach($this->totals as $i=>$arr) {
			foreach($arr as $ind=>$val){
			extract($val);
			$this->sub += $unit_tot;
			$this->subCost += $tot_to_date;
			$this->tot  += $unit_tot;
			$this->totCost += $tot_to_date;
			$owner_name = trim($owner_name);
			$date=$this->showDate == "show" ? Functions::dbDate($gravel_date) : "";
			if ($owner_name != $pOwner ) {
				if ($pOwner ==  -1 ) {
					$pOwner = $owner_name;
				}
				else {
					$this->sub -= $unit_tot;
					$this->subCost -= $tot_to_date;
					$this->doSubs($Y,$pdf);
					$pOwner = $owner_name;
					$this->sub = $unit_tot;
					$this->subCost = $tot_to_date;
					$Y += 10;
					$lineStart = $this->showDate == "summ" ? 12 : 1;
					$pdf->Line($lineStart, $Y, 296 + $this->xOffset, $Y);

					if ($Y>180) {
						$pdf->AddPage();
						$this->heading($pdf);
						$this->headingTitle(29,$pdf);
						$Y = 34;
					}
				}
			}
			$ht = strlen($well_name) <=35 ? 5 : 10;
			if (strlen($well_name) > 70 ) {
				$ht = 15;
			}
			if (strlen($owner_name) > 30 && $ht < 10 ) {
				$ht = 10;
			}
			if (strlen($area_name) > 25 && $ht < 10 ) {
				$ht = 10;
			}
			$conName = $this->contractors[$contractor_id];
      	$pdf->SetTextColor(0,0,0);
			if ($this->showDate == "show" ) {
      		$pdf->MultiCell(21,$ht,$date,array('LB'=>$this->borderStyle),'C',false,0,1,$Y);
			}
      	$pdf->MultiCell(14,$ht,$calloff_order_id,array('LB'=>$this->borderStyle),'C',false,0,22 + $this->xOffset,$Y);
      	$pdf->MultiCell(45,$ht,$conName,array('LB'=>$this->borderStyle),'L',false,0,36 + $this->xOffset);
      	$pdf->MultiCell(40,$ht,$area_name,array('LB'=>$this->borderStyle),'L',false,0,81 + $this->xOffset);
      	$pdf->MultiCell(58,$ht,$well_name,array('LB'=>$this->borderStyle),'L',false,0,121 + $this->xOffset);
      	$pdf->MultiCell(58,$ht,$owner_name,array('LB'=>$this->borderStyle),'L',false,0,179 + $this->xOffset);
      	$pdf->MultiCell(13,$ht,$rate,array('LB'=>$this->borderStyle),'C',false,0,237 + $this->xOffset);
      	$pdf->MultiCell(21,$ht,number_format($unit_tot,2),array('LB'=>$this->borderStyle),'R',false,0,250 + $this->xOffset);
      	$pdf->MultiCell(25,$ht,number_format($tot_to_date,2),array('LRB'=>$this->borderStyle),'R',false,0,271 + $this->xOffset);
			$Y+=$ht;
			if ($Y>180) {
				$pdf->AddPage();
				$this->heading($pdf);
				$this->headingTitle(29,$pdf);
				$Y = 34;
				$pdf->SetFont(PDF_FONT, '', 10);
			}
		} // end  foreach
     }
		$this->doSubs($Y,$pdf);
		$Y += 10;
		$this->doTotals($Y,$pdf);
	}
	private function getDetails(){
		$dateClause = "1 = 1 ";
		$areaClause = "";
		$ownerLine = $this->landOwner == -1 ? "" : " and l.landowner_id = $this->landOwner ";
		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$dateClause = "gravel_date between '$this->startDate'  and '$this->endDate' ";

		}
		if ($this->areaID > 0 ) {
			$areaClause = "and gw.area_id = $this->areaID ";
		}
		if ($this->conID > 0 ) {  // single contractor
	   	$connArr = Functions::getAllCon($this->conID);
		}
		else {
			$connArr = Functions::getAllCon(NULL);
		}
		foreach($connArr as $ind=>$val) { 
			$shortName = $val['name'];
			$conName = $val['con_name'];
			$conID = $val['contractor_id'];
			$this->contractors[$conID] = $conName; // assoc aray  con IDs to names
			if ($this->Type == "Gravel" ) {
				if ($this->showDate == "show" ) {
					$sql = "SELECT l.owner_name,'$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,gravel_date,calloff_order_id,$conID as contractor_id,
					gw.gravel_rate as rate,coalesce(gravel_units,0) as unit_tot, coalesce(gravel_total,0) as tot_to_date
 					from {$shortName}_gravel_water gw
 					LEFT JOIN area a using(area_id)
 					LEFT join gravel_water_loc gwl on loc_1_id = gwl.gravel_water_loc_id
 					LEFT JOIN landowner l using(landowner_id)
  					where $dateClause 
					$areaClause
					$ownerLine
  					and coalesce(gravel_units,0) + coalesce(gravel_total,0) > 0
					order by owner_name,con_name ,calloff_order_id,gravel_date,well_name";


				}
				else {
					$sql = "SELECT l.owner_name,'$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,calloff_order_id,$conID as contractor_id,
					gw.gravel_rate as rate,sum(coalesce(gravel_units,0)) as unit_tot, sum(coalesce(gravel_total,0)) as tot_to_date
 					from {$shortName}_gravel_water gw
 					LEFT JOIN area a using(area_id)
 					LEFT join gravel_water_loc gwl on loc_1_id = gwl.gravel_water_loc_id
 					LEFT JOIN landowner l using(landowner_id)
  					where $dateClause 
					$areaClause
					$ownerLine
  					and coalesce(gravel_units,0) + coalesce(gravel_total,0) > 0
  					group by owner_name,con_name,area_name,well_name,calloff_order_id,contractor_id,rate
					order by owner_name,con_name ,area_name,well_name";
			  }
					//echo "<br />";
					//echo $sql;
					//echo "<br />";
			}
			else {  // Water
				if ($this->showDate == "show" ) {
					$sql = "SELECT l.owner_name, '$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,gravel_date,calloff_order_id,$conID as contractor_id,
            	gw.water_rate as rate, coalesce(water_units,0) as unit_tot, coalesce(water_total,0) as tot_to_date
            	from {$shortName}_gravel_water gw
            	LEFT JOIN area a using(area_id)
            	LEFT join gravel_water_loc gwl on loc_2_id = gwl.gravel_water_loc_id
            	LEFT JOIN landowner l using(landowner_id)
            	where $dateClause 
					$areaClause
					$ownerLine
            	and coalesce(water_units,0) + coalesce(water_total,0) > 0
					order by owner_name,con_name,calloff_order_id,gravel_date,well_name";
				}
				else {
					$sql = "SELECT l.owner_name, '$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,calloff_order_id,$conID as contractor_id,
            	gw.water_rate as rate, sum(coalesce(water_units,0)) as unit_tot, sum(coalesce(water_total,0)) as tot_to_date
            	from {$shortName}_gravel_water gw
            	LEFT JOIN area a using(area_id)
            	LEFT join gravel_water_loc gwl on loc_2_id = gwl.gravel_water_loc_id
            	LEFT JOIN landowner l using(landowner_id)
            	where $dateClause 
					$areaClause
					$ownerLine
            	and coalesce(water_units,0) + coalesce(water_total,0) > 0
  					group by owner_name,con_name,area_name,well_name,calloff_order_id,contractor_id,rate
					order by owner_name,con_name,area_name,well_name";
				}

					//echo $sql;
			}

		   	//echo "<br />$sql<br />";

			if (! $totAll = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			else {
				//	var_dump($totAll);
				//	echo "<br /> ---- ";
				$this->totals[] = $totAll;
					//var_dump($totAll);
			}
				//$this->totals[] = array("calloff_order_id"=>"3265","contractor_id"=>"5","well_name"=>'344',"area_name"=>'name',"rate"=>'0.55',"unit_tot"=>'100',"tot_to_date"=>'5.50',"owner_name"=>'Lanndy');
		}  //foreach contractor 
		//array_multisort($this->totals);

		//echo "<br />XXXXXXXXXXXXXXXXXXX<br />";
		//var_dump($this->totals);
		//exit;

	}
	 private function heading($pdf) {
      // Heading Title 
		
		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$pDate = "$this->startDate  to $this->endDate";
		}
		else {
			$pDate = "ALL DATES";
		}
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
      $pdf->MultiCell(100, 0, "$this->Type Report  $this->by", 0, 'C', false, 2, 100,5);
      $pdf->SetFont(PDF_FONT, '', 12);
		if ($this->showDate == "show" ) {
      	$pdf->MultiCell(100,0,"All Dates",0,'C',false,2,100,12);
		}
		else {
      	$pdf->MultiCell(100,0,"Summarized",0,'C',false,2,100,12);

		}
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(15, 0, 'From :', 0, 'C', false, 0, 118,19);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(100, 0, $pDate, 0, 'C', false, 0, 108);

	}

	  private function headingTitle($Y,$pdf) {
		$uom = $this->Type == "Gravel" ? "m3" : "kL";
		$show = $this->showDate == "show" ? "Date" : "";
      $pdf->SetFillColor(241,121,61);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 11);
		if ($this->showDate == "show" ) {
      	$pdf->MultiCell(21,0,$show,array('LTB'=>$this->borderStyle),'C',true,0,1,$Y);
		}
      $pdf->MultiCell(14,0,'C.O.O',array('LTB'=>$this->borderStyle),'C',true,0,22 + $this->xOffset,$Y);
      $pdf->MultiCell(45,0,'Contractor',array('LTB'=>$this->borderStyle),'C',true,0,36 + $this->xOffset);
      $pdf->MultiCell(40,0,'Area',array('LTB'=>$this->borderStyle),'C',true,0,81 + $this->xOffset);
      $pdf->MultiCell(58,0,'Well/s',array('LTB'=>$this->borderStyle),'C',true,0,121 + $this->xOffset);
      $pdf->MultiCell(58,0,'Land Owner',array('LTB'=>$this->borderStyle),'C',true,0,179 + $this->xOffset);
      $pdf->MultiCell(13,0,'Rate',array('LTB'=>$this->borderStyle),'C',true,0,237 + $this->xOffset);
      $pdf->MultiCell(21,0,"Quan $uom",array('LTB'=>$this->borderStyle),'C',true,0,250 + $this->xOffset);
      $pdf->MultiCell(25,0,'Cost',array('LTRB'=>$this->borderStyle),'C',true,0,271 + $this->xOffset);

   }



}
?>
