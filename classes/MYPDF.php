<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
global $BASE;
require_once("{$BASE}classes/tcpdf/config/lang/eng.php");
require_once("{$BASE}classes/tcpdf/tcpdf.php");
$BASE = $BASE . "/"; ///  For some F!!!! reason  tcpdf  screws  BASE ??
require_once("{$BASE}classes/Address.php");
require_once("{$BASE}classes/Functions.php");
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
		$arr = Functions::getLogo();
		$addr = Functions::getAddress();
      // Logo
      $this->Image($arr['logo'], 0, 5,36 , '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
      // Set font
      $this->SetFont(PDF_FONT, '', 10);
      // Title
      $this->MultiCell(0, 0, $addr->address1, 0, 'L', false, 2, 165, 0);
      $this->MultiCell(0, 0, $addr->address2, 0, 'L', false, 2, 165);
      $this->MultiCell(0, 0, $addr->suburb.' '.$addr->postcode, 0, 'L', false, 2, 165);
      $this->Ln();
      $this->MultiCell(15, 0, 'PHONE:', 0, 'L', false, 0, 165);
      $this->MultiCell(30, 0, $addr->phone, 0, 'L', false, 2, 180);
      $this->MultiCell(15, 0, 'FAX:', 0, 'L', false, 0, 165);
      $this->MultiCell(30, 0, $addr->fax, 0, 'L', false, 2, 180);
      $this->SetLineStyle(array("width"=>0.5));
      $this->Line(0, 28, 210, 28);
		$this->SetY(29);
    }
}
?>
