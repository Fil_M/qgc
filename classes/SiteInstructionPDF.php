<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class SiteInstructionPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
   public $siteInsID;
	public $areaName;
	public $wellName;
	public $crewName;
	public $contractorName;
	public $shortName;
	public $conDomain;
	public $insNo;
	public $fileName;
	public $contractorPath;


	public function __construct($action="",$conID,$siteInsID,$filename=NULL) {
		$this->conn = $GLOBALS['conn'];
		$this->conID = $conID;
		$this->siteInsID = $siteInsID;
		$this->action = $action;
		$this->fileName = $filename;
		$this->getSiteDetails($this->siteInsID);
		extract($this->data);
		$this->crewName = $crew_name;
		$qgc_approve_date = Functions::dbDate($qgc_approve_date);
      $cost_estimate = "$".number_format($cost_estimate,2);
      $estimate_total = "$".number_format($estimate_total,2);
		$this->insNo = $instruction_no;

		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Site Instruction $this->insNo  $this->contractorName $area_name - $well_name");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$Y = 35;
		$pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(60, 0, 'Well Pad Location/s:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(165, 0, "$area_name / $well_name", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
      $Y+=10 ;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(60, 0, 'Workscope:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(55, 0, "$crew_name", array('B'=>$this->lineStyle), 'L', false, 0, 40,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Sub Scopes:', 0, 'L', false, 0, 100,45);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(70, 0, $subcrews, array('B'=>$this->lineStyle), 'L', false,2, 135);
      $Y = $pdf->getY();
      $Y += 10;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Contractor:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55, 0, $this->contractorName, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Field Estimates:', 0, 'L', false, 0, 100,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(70, 0, $field_ids, array('B'=>$this->lineStyle), 'L', false,0, 135);
      $Y += 10;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(35, 0, 'Site Instruction No:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55, 0, $instruction_no, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(35, 0, 'Instruction Estimate:', 0, 'L', false, 0, 100,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(30, 0, $estimate_total, array('B'=>$this->lineStyle), 'R', false,0, 135);
      $Y += 10;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(35, 0, 'CallOff Order:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55, 0, $calloff_order_id, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(35, 0, 'Original Estimate:', 0, 'L', false, 0, 100,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(30, 0, $cost_estimate, array('B'=>$this->lineStyle), 'R', false,0, 135);
      $Y += 10;
		$pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(40, 0, 'Comments:', 0, 'L', false, 0, 3,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(165, 0, $comments, array('B'=>$this->lineStyle), 'L', false,0, 40);
      $Y = $pdf->getY();
      $Y += 10;
      $pdf->SetTextColor(0,0,0);
		$pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(50,0,"Contractor Signed:",0,'L',false,0,3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55,0,$contractor_signee,array('B'=>$this->lineStyle),'L',false,0,40);
      $pdf->SetTextColor(0,0,0);
      $image_file = $contractor_sig;
		$image_file = $this->contractorPath .$contractor_sig;
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,70 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

      $Y+=35;
      $pdf->SetFont(PDF_FONT, 'B', 10);
      $pdf->MultiCell(120,0,"Company Representative or Delegate:",0,'L',false,0,40,$Y);
-
      $Y+=20;
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(50,0,"Approved for QGC:",0,'L',false,0,3,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(55,0,$qgc_signee,array('B'=>$this->lineStyle),'L',false,0,40);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 10);
      $pdf->MultiCell(25, 0, 'Date:', 0, 'L', false, 0, 100);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(22, 0, $qgc_approve_date, array('B'=>$this->lineStyle), 'L', false,1, 115);
      $image_file = $qgc_sig;
		if (!file_exists($image_file)) {
         $image_file="images/nosig.png";
      }
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,0 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

      $name = !is_null($this->fileName) ? $this->fileName : "tmp/SiteInstruction_{$this->conID}_{$this->siteInsID}.pdf";
      if ($action == "print" ) {
         $pdf->Output($name, 'I');
      }
      else {  
         $pdf->Output($name, 'F');
      }

   }		
	 private function getSiteDetails($siteInsID) {
			$connArr = Functions::getConName($this->conID);
      	$this->contractorName = $connArr['con_name'];
      	$this->shortName = $connArr['name'];
			$this->conDomain = $connArr['domain'] . "/";
			$this->contractorPath = "/home/https/$this->shortName/";


			 $sql = "SELECT si.*,a.area_name,crew_name,co.area_id,co.cost_estimate,wells_from_ids(si.well_ids) as well_name,subscopes_from_ids(si.sub_crew_ids) as subcrews,
         array_to_string(array(select field_estimate_id from {$this->shortName}_field_estimate where site_instruction_id = $siteInsID and removed is false order by field_estimate_id),', ','*') as field_ids
         from {$this->shortName}_site_instruction si 
         LEFT JOIN calloff_order co using (calloff_order_id)
         LEFT JOIN area a using(area_id) 
         LEFT JOIN crew c on c.crew_id = si.crew_id 
         where si.site_instruction_id = $siteInsID";
      // echo $sql;
			if (! $this->data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }



      }

		 private function heading($pdf){
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, 'QGC Well Engineering Construction', 0, 'C', false, 2, 50,3);
      $pdf->SetFont(PDF_FONT, '', 12);
      $pdf->SetTextColor(255,0,0);
      $pdf->MultiCell(100, 0, $this->crewName, 0, 'C', false, 0, 50,10);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, "Site Instruction $this->insNo", 0, 'C', false, 0, 50,17);
   }



}
?>
