<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class WellPPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
	public $areaID;
	public $wellID;
	public $numWells;
	public $areaName;
	public $wellName;
	public $contractorName;
	public $shortName;
	public $estimate;
	public $totals=array();
	public $tmp_totals=array();
	public $totalAll= 0;
	public $callOffID;
	public $callOffStr="";
	public $startDate;
	public $endDate;
	public $by;
	public $split;
	public $color=array("0"=>array(255.0,65.0,65.0),"1"=>array(255.0,238.0,65.0),"2"=>array(86.0,255.0,65.0),"3"=>array(65.0,119.0,255.0),"4"=>array(255.0,152.0,65.0),"5"=>array(255.0,65.0,181.0),
	"6"=>array(160.0,65.0,255.0),"7"=>array(65.0,255.0,243.0),"8"=>array(197.0,255.0,65.0),"9"=>array(110.0,80.0,80.0),"10"=>array(127.0,127.0,127.0),"11"=>array(205.0,229.0,229.0));

	public function __construct($action="",$conID,$areaID=0,$wellID=0,$split,$startDate,$endDate) {
		$this->conn = $GLOBALS['conn'];
		$this->conID = $conID;
		$this->split = strlen($split) > 0 ? $split : "con";
		if ($this->conID > 0 ) {
			$this->by = "By Crew";
		}
		else {
			$this->contractorName = "All Contractors";
			if ($this->split == "con" ) {
				$this->by = "By Contractor";
			}
			else {
			  $this->by = "By Crew";
			}

		}
		$this->areaID = $areaID;
		$this->wellID = $wellID;
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		if ($this->areaID == 0 || $this->wellID == 0 ) {
			return false;
		}
		$this->getDetails();
		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Well Report $this->by $this->areaName - $this->wellName");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$this->pieChart($pdf);

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
		$name = "tmp/Worksheet.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
   }		
	private function pieChart($pdf) {
		$Y = 110;
		//$pdf->MultiCell(51,0.25,'',array('LRTB'=>$this->borderStyle),'C',false,0,153,$Y);

		$xc = 105;
		$r = 50;
		$cnt = count($this->totals)-1 ;
		for ($x = $cnt;$x>=0;$x-- ) {
			$perc = $rotate = $tot = 0;
			$colr = $this->color[$x];
			$itot = $this->totals[$x]['tot_to_date'];
			$iPerc = round((($itot / $this->totalAll ) * 100),2) ;
			$this->totals[$x]['ind_percent'] = $iPerc;
			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
			for ($y = $x;$y>=0;$y--) {
				$tot += $this->totals[$y]['tot_to_date'];
			}
			$perc =round((($tot / $this->totalAll ) * 100),2) ;
			$rotate  =  (360  *   ($perc / 100 )) ;
			$pdf->PieSector($xc, $Y, $r, 0, $rotate, 'FD', true, 90);
		}

		$Y += 70;
      $pdf->SetFont(PDF_FONT, 'B', 12);
      $pdf->SetTextColor(0,0,0);
		reset($this->totals);
		if ($this->conID > 0  || ($this->conID == 0 && $this->split == "crew")) {  // single contractor
			$pdf->MultiCell(80,6,"CREWS",array('LTR'=>$this->borderStyle),'C',false,0,5,$Y);
		}
		else {
			$pdf->MultiCell(80,6,"CONTRACTOR",array('LTR'=>$this->borderStyle),'C',false,0,5,$Y);
		}
		$pdf->MultiCell(60,6,"PERCENTAGE of TOTAL",array('LTB'=>$this->borderStyle),'C',false,0,85,$Y);
		$pdf->MultiCell(60,6,"TOTAL",array('LTRB'=>$this->borderStyle),'C',false,0,145,$Y);
      $pdf->SetFont(PDF_FONT, '', 12);
		$Y +=6;
		for ($x = 0;$x<=$cnt;$x++) {
			$colr = $this->color[$x];
			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
			$iPerc = number_format($this->totals[$x]['ind_percent'],2) ."%";
			$tot =  "$" . number_format($this->totals[$x]['tot_to_date'],2);
			if ($this->conID > 0  || ($this->conID == 0 && $this->split == "crew")) {
				$crew = $this->totals[$x]['crew_name'];
				$pdf->MultiCell(80,6,$crew,array('LTRB'=>$this->borderStyle),'L',true,0,5,$Y);
			}
			else {
				$contractor = $this->totals[$x]['contractor_name'];
				$pdf->MultiCell(80,6,$contractor,array('LTRB'=>$this->borderStyle),'L',true,0,5,$Y);
				
			}
			$pdf->MultiCell(60,6,$iPerc,array('LTRB'=>$this->borderStyle),'C',true,0,85,$Y);
			$pdf->MultiCell(60,6,$tot,array('LTRB'=>$this->borderStyle),'R',true,0,145,$Y);
			$Y +=6;


		}
		

      $pdf->SetFont(PDF_FONT, 'B', 12);
		$pdf->MultiCell(40,0,'TOTAL',0,'R',false,0,120,$Y,true,0,false,true,10,'B');
		$tot = '$' .number_format($this->totalAll,2);
		$pdf->MultiCell(40,10,$tot,array('B'=>$this->borderStyle),'R',false,1,165,$Y,true,0,false,true,10,'B');
		$Y += 11;
		$pdf->Line(165,$Y,205,$Y);


	}
	private function getDetails(){
		$dateClause = "";
		$sql = "SELECT a.area_name,w.well_name from area a,well w  where a.area_id = $this->areaID and w.well_id = $this->wellID";

		if (! $nameArr = $this->conn->getRow($sql)) {
				echo "dies here  $sql";
				die($this->conn->ErrorMsg());
		}
		$this->areaName = $nameArr['area_name'];
		$this->wellName = $nameArr['well_name'];


		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$dateClause = " and hour_date between '$this->startDate'  and '$this->endDate' ";

		}
		if ($this->conID > 0 ) {  // single contractor
	   	$connArr = Functions::getConName($this->conID);
      	$this->contractorName = $connArr['con_name'];
      	$this->shortName = $connArr['name'];

			$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0))  as tot_to_date,ch.crew_id,crew_name,well_ids,calloff_order_id
			from {$this->shortName}_hour  ch
			LEFT JOIN crew using(crew_id)
			 where status >= 5 and ch.removed is false and area_id = $this->areaID and $this->wellID   in (select(unnest(string_to_array(ch.well_ids::text,'|')::int[])))  $dateClause 
			group by crew_id,crew_name,well_ids,calloff_order_id";
		//echo $sql;
			if (! $this->totals = $this->conn->getAll($sql)) {
				if($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
				else {
					die("No records found for this date range: $this->startDate - $this->endDate");
				}
			}
			foreach($this->totals as $ind=>$val) {
				$arr = explode("|",$val['well_ids']);
				$numWells = count($arr);
				$this->callOffStr .= $val['calloff_order_id'] .",";
				$this->totals[$ind]['tot_to_date'] = $val['tot_to_date'] / $numWells;
			}

			$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) as tot_to_date 
			from {$this->shortName}_hour h where status >= 5 and removed is false and area_id = $this->areaID and $this->wellID  in (select(unnest(string_to_array(h.well_ids::text,'|')::int[]))) $dateClause";


			if (! $this->totalAll = $this->conn->getOne($sql)) {
				die($this->conn->ErrorMsg());
			}
			else {
				$this->totalAll =   $this->totalAll / $numWells;
			}
		}
		else {
			$connArr = Functions:: getAllCon();
			if ($this->split == "crew" ) {
				$sql = "SELECT crew_id, crew_name from crew where removed is false order by crew_id";
				if (! $data = $this->conn->getAll($sql)) {
					die($this->conn->ErrorMsg());
				}
				foreach ( $data as $i=>$v ) {  // set up indexed totals  array
					$this->tmp_totals[$v['crew_id']] = array("tot_to_date"=>0,"crew_name"=>$v['crew_name']);
				}
			}
			foreach($connArr as $ind=>$val) {
				$shortName = $val['name'];
				$conName = $val['con_name'];
				$numWells = 1;
					$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) as tot_to_date,h.well_ids 
					from {$shortName}_hour h where status >= 5 and removed is false and area_id = $this->areaID and $this->wellID   in (select(unnest(string_to_array(h.well_ids::text,'|')::int[]))) $dateClause group by well_ids";



					if (! $res = $this->conn->getRow($sql)) {
						if ($this->conn->ErrorNo() != 0 ) {
							die($this->conn->ErrorMsg());
						}
						else {
							$res['tot_to_date'] = 0;
						}
					}
					else {
					 	$arr = explode("|",$res['well_ids']);
         			$numWells = count($arr);
					}

         		$this->numWells = $numWells > 0 ? $numWells : 1;
					$totAll = $res['tot_to_date'] / $this->numWells;
					$this->totalAll += $totAll ;

				if ($this->split == "con" ) {  // contractor  split
					$this->totals[$ind] = array("tot_to_date"=>$totAll,"contractor_name"=>$conName);
				}	
				else { // crew split
						// All crews
					$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) as tot_to_date,ch.crew_id,crew_name,ch.well_ids
					from {$shortName}_hour  ch
					LEFT JOIN crew using(crew_id)
			 		where status >= 5 and ch.removed is false and area_id = $this->areaID and  $this->wellID  in (select(unnest(string_to_array(ch.well_ids::text,'|')::int[])))  $dateClause group by crew_id,crew_name,well_ids";
					if (! $totals = $this->conn->getAll($sql)) {
						if ($this->conn->ErrorNo() != 0 ) {
							die($this->conn->ErrorMsg());
						}
						else {
							$totals = NULL;
						}
					}
					if (!is_null($totals)) {
						$arr = explode("|",$totals[0]['well_ids']);
         			$numWells = count($arr);
         			$this->numWells = $numWells > 0 ? $numWells : 1;
         			$this->callOffID = "";
         			$totals[0]['tot_to_date'] = $totals[0]['tot_to_date'] / $this->numWells;


						foreach($totals as $indx=>$va) {   // load into $this->totals array
							if (intval($va['tot_to_date']) > 0 ) {
								$this->tmp_totals[$va['crew_id']]['tot_to_date'] += $va['tot_to_date'];
							}
				   	}
					}

				}

			}
         //var_dump($this->totalAll);
			if ($this->split == "crew" ) {  // contractor  split  clean and rebuild
				reset($this->tmp_totals);
				foreach($this->tmp_totals as $indx=>$vall) {   // load into $this->totals array
					if($vall['tot_to_date'] == 0) {
						unset($this->tmp_totals[$indx]);
					} 
			   }
			//  rebiuld $this totals as indexed array
				foreach($this->tmp_totals as $indx=>$vall) {   // load into $this->totals array
					$this->totals[] = array('tot_to_date'=>$vall['tot_to_date'],'crew_id'=>$indx,'crew_name'=>$vall['crew_name']);
			   }
			}
			else { // clean not used totals
				reset($this->totals);
				$this->tmp_totals = array();
				foreach($this->totals as $indx=>$vall) {   // load into $this->totals array
					if($vall['tot_to_date'] > 0) {
						$this->tmp_totals[] = array("tot_to_date"=>$vall['tot_to_date'],"contractor_name"=>$vall['contractor_name']);
					} 
			   }
				$this->totals = $this->tmp_totals;

			}  
         //var_dump($this->totals);
		}

	}
	 private function heading($pdf) {
      // Heading Title 
		
		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$pDate = "$this->startDate  to $this->endDate";
		}
		else {
			$pDate = "ALL DATES";
		}
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 16);
      $pdf->MultiCell(120, 0, "Well Report - $this->by", 0, 'C', false, 2, 45,0);
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(25, 0, 'From:', 0, 'L', false, 0, 37,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'C.O.O:', 0, 'L', false, 0, 115,7);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $this->callOffStr, 0, 'L', false,0, 130);
		//$pdf->SetTextColor(255,0,0);
      //$pdf->MultiCell(50, 0, "($this->numWells)", 0, 'L', false,1, 142);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Contractor:' ,0,'L', false, 0, 37,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $this->contractorName, 0, 'L', false, 2, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55, 0, 'Total:' ,0,'L', false, 0, 115,12.5);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, number_format($this->totalAll,2), 0, 'L', false, 2, 130);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(0, 0, 'Area:', 0, 'L', false, 0, 37,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $this->areaName, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Well:', 0, 'L', false, 0, 115,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(0, 0, $this->wellName, 0, 'L', false, 2,130 );
      $pdf->SetTextColor(0,0,0);

	}

}
?>
