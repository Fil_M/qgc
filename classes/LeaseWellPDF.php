<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class LeaseWellPDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $crewID;
	public $crewName;
	public $shortName;
	public $estimate;
	public $totals=array();
	public $tmp_totals=array();
	public $totalAll= 0;
	public $totalAllExt= 0;
	public $totalWellCount = 0;
	public $startDate;
	public $avArea = array();
	public $areaCount = array();
	public $areaWellCount = array();
	public $endDate;
	public $type;
	public $split;
	public $splitArr = array();
	public $crewArr;
	public $consCrewID;
	public $rehabCrewID;
	public $wellConCount;
	public $conNames;
	public $crewStr;
	public $color=array("0"=>array(255.0,65.0,65.0),"1"=>array(255.0,238.0,65.0),"2"=>array(86.0,255.0,65.0),"3"=>array(65.0,119.0,255.0),"4"=>array(255.0,152.0,65.0),"5"=>array(255.0,65.0,181.0),
	"6"=>array(160.0,65.0,255.0),"7"=>array(65.0,255.0,243.0),"8"=>array(197.0,255.0,65.0),"9"=>array(110.0,80.0,80.0),"10"=>array(127.0,127.0,127.0),"11"=>array(205.0,229.0,229.0));

	public function __construct($action="print",$startDate,$endDate,$type="contractor",$crewID) {
		$this->conn = $GLOBALS['conn'];
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->type = $type;
		$this->crewID = $crewID;
		//$typeTitle = $type == "contractor" ? "Contractor" : "All Work Scopes";
		//$typeTitle = $type == "contractor" ? "Contractor" : "All Work Scopes";
		if ($type == "contractor" ) {
			$typeTitle =  "Lease Totals for period $this->startDate to $this->endDate";
		}
		else {
			$typeTitle =  "Area Report Well Costs & Averages $this->startDate to $this->endDate";
		}

		$this->getDetails();
		// create new PDF document
		$pdf = new MYPDFLAND("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle($typeTitle);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT,PDF_MARGIN_BOTTOM);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(FALSE, 0);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
  
	   // Output to file system , load email_log db table  and email
		if ($this->type == "contractor" ) {
			$prevConID = -1;
			$prevName = "";
			$Y=27;
			foreach($this->tmp_totals as $ind=>$val) {
				if ($prevConID != $val['con_id'] ) {
				 	if ($Y > 185) {
						$pdf->AddPage();
						$this->heading($pdf);
						$this->contractorHeading($pdf,$val['contractor_name'],$val['con_id'],$Y);
						$Y=27;
				 	}
					$prevConID = $val['con_id'];
					$prevName = $val['contractor_name'];
					$Y+=2;
					$this->contractorHeading($pdf,$val['contractor_name'],$val['con_id'],$Y);
					$Y += 9.5;
				}
				$height = ceil(strlen($val['well']) /30) * 4.5;
         	$height = $height >= 4.5 ? $height : 4.5;

				$wellPerc = round(($val['tot_to_date'] / $this->currTot ) * 100,2);
				$pdf->SetTextColor(0,0,0);
      		$pdf->MultiCell(35,$height,$val['coo_id'],array('LTB'=>$this->borderStyle),'L',false,0,74,$Y);
      		$pdf->MultiCell(35,$height,$val['comp_id'],array('LTB'=>$this->borderStyle),'L',false,0,109,$Y);
      		$pdf->MultiCell(45,$height,$val['area'],array('LTB'=>$this->borderStyle),'L',false,0,144,$Y);
      		$pdf->MultiCell(55,$height,$val['well'],array('LTB'=>$this->borderStyle),'L',false,0,189,$Y);
      		$pdf->MultiCell(25,$height,$wellPerc."%",array('LTB'=>$this->borderStyle),'C',false,0,244,$Y);
      		$pdf->MultiCell(26,$height,number_format($val['tot_to_date'],2),array('LTRB'=>$this->borderStyle),'R',false,2,269,$Y);
				$Y += $height;
				if ($Y > 190) {
					 $Y += 5;
      			$pdf->SetFont(PDF_FONT, '', 11);
            	$pdf->MultiCell(45,0,'Continued ...',0,'L',false,1,3,$Y);
      			$pdf->SetFont(PDF_FONT, '', 10);
					$pdf->AddPage();
					$this->heading($pdf);
					$Y=29;
					if ($this->tmp_totals[$ind +1]['con_id'] == $prevConID ) {
						$this->contractorHeading($pdf,$prevName,$prevConID,$Y);
						$Y += 9.5;
					}
      			$pdf->SetFont(PDF_FONT, '', 10);
				}  

			}
		}  // end type  total
		else {  // type == average
			$prevArea = $prevName = "None";
			$prevConID = -1;
			reset($this->avArea);
			$Y=29;
			foreach ($this->avArea as $area=>$wells ) {
				if($area != $prevArea) {
					$prevConID = -1;
					$prevArea = $area;
					$Y = $this->areaHeading($pdf,$area,$Y,$wells);
					$Y += 5.5;
					$this->conAvHeading($pdf,$wells,$Y);
					$Y = $pdf->getY();
					$Y +=10;
					$Y = $Y >= 29 ? $Y : 29;
				}
			}
			/*$Y = $this->areaHeading($pdf,$area,$Y,$con);
			$Y += 5.5;
			$prevConID = -1;
			foreach ($con as $conID=>$wells) {
				if ($conID != $prevConID ) {
					$prevConID = $conID;
						//$Y+=2;
					$this->conAvHeading($pdf,$conID,$wells,$Y);
					$Y = $pdf->getY();
					$Y = $Y >= 29 ? $Y : 29;
				}
		  }*/	
		//
		}
		$name = "tmp/Worksheet.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
   }		
	private function getDetails(){
		// get conNames
		$sql = "select contractor_id,con_name from contractor";
		if (! $this->conNames = $this->conn->getAssoc($sql) ) {
			die($this->conn->ErrorMsg());
		}
		$this->crewArr = Functions::getAllCrew();
		$this->totallAll = 0;
		$conArr = Functions::getAllCon();
		if (intval($this->crewID) > 0 ) {
            $crewClause = " and crew_id in (".$this->crewArr[$this->crewID]['crew_in_clause'] . ")";
            $this->crewName = $this->crewArr[$this->crewID]['crew_name'];
      }
      else {
            $crewClause = "";
      }
		$this->crewStr = $this->crewID > 0 ? $this->crewName : "All Workscopes";

		foreach($conArr as $ind=>$val) {
			$shortName = $val['name'];
			$conName = $val['con_name'];
			$conID = $val['contractor_id'];
			/*$sql = "with tab as (select distinct(well_id),area_id from {$shortName}_hour  where hour_date between '$this->startDate' and '$this->endDate'  and status >= 5 )
			select round(coalesce(sum(total_t1),0)::numeric + coalesce(sum(total_t2),0)::numeric + coalesce(sum(expense),0)::numeric,2) as tot,area_name,well_name from {$shortName}_hour h
			JOIN tab on h.area_id = tab.area_id and h.well_id = tab.well_id
			LEFT JOIN area ar on ar.area_id = tab.area_id
			LEFT JOIN well wl on wl.well_id = tab.well_id
			where hour_date between '$this->startDate' and '$this->endDate'
			group by area_name,well_name
			order by area_name,well_name ";   */
			$sql = "with tab as (select cc.calloff_order_id,cc.completion_cert_id,a.area_name,cc.well_ids from {$shortName}_completion_cert cc
			LEFT JOIN area a using(area_id)
 			where completion_date between '$this->startDate' and '$this->endDate' and qgc_approve_date is not null $crewClause  and percentage_complete = 100)
			select round(coalesce(sum(total_t1),0)::numeric + coalesce(sum(total_t2),0)::numeric + coalesce(sum(expense),0)::numeric,2) as tot,
			tab.completion_cert_id,tab.calloff_order_id,tab.area_name,wells_from_ids(h.well_ids) as well_name from {$shortName}_hour h 
			JOIN tab on tab.calloff_order_id = h.calloff_order_id
			group by tab.calloff_order_id,tab.completion_cert_id,tab.area_name,well_name
 			order by tab.area_name,well_name";

			//echo $sql;


			if (! $totArr = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			foreach($totArr as $indx=>$vall) {
				$this->tmp_totals[] =  array("con_id"=>$conID, "contractor_name"=>$conName, "area"=>$vall['area_name'],"well"=>$vall['well_name'],
				 "coo_id"=>$vall['calloff_order_id'],"comp_id"=>$vall['completion_cert_id'], "tot_to_date"=>$vall['tot']); 
			}


			$sql = "select round(coalesce(sum(total_t1),0)::numeric + coalesce(sum(total_t2),0)::numeric + coalesce(sum(expense),0)::numeric,2) as total 
			from  {$shortName}_completion_cert  where completion_date between '$this->startDate' and '$this->endDate'  and qgc_approve_date is not null $crewClause and percentage_complete = 100"; 
			if (! $total = $this->conn->getOne($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			$this->totals[] = array("con_id"=>$conID,"total"=>$total);
			$this->totalAll += $total;
				
		}
		//var_dump($this->tmp_totals);
		//Well  counts
		$prevConID = -1;
		$wellCount = array();
		foreach ($this->tmp_totals as $i=>$v) {
         $arwell = explode(',',$v['well']);
         $numWells = count($arwell);
			if ($this->type == "contractor" ) {
				if (! isset($wellCount[$v['area']][$v['well']][$v['con_id']])) {  // Remove  Dupes
					$wellCount[$v['area']][$v['well']][$v['con_id']] = $numWells;
					$this->totalWellCount += $numWells;
					if ($v['con_id'] != $prevConID) {
			   		$prevConID =$v['con_id'];
						$this->wellConCount[$prevConID] = $numWells;
					}
					else {
						$this->wellConCount[$prevConID] += $numWells;
					}
				}
				else {                       // multiple C.O.O.s  merge with previous
					extract($v);
					$this->tmp_totals[$i -1]['coo_id'] .= ",$coo_id"; 
					$this->tmp_totals[$i -1]['comp_id'] .= ",$comp_id"; 
					$this->tmp_totals[$i -1]['tot_to_date'] += $tot_to_date; 

				 	unset($this->tmp_totals[$i]);
				// Average  array  merge
				}
			} // end  contractor
			else {
				if (! isset($wellCount[$v['area']][$v['well']])) {  // Remove  Dupes
					$wellCount[$v['area']][$v['well']] = $numWells;
					$this->avArea[$v['area']][$v['well']] = array("tot_to_date"=>$v['tot_to_date'],"con_names"=>$this->conNames[$v['con_id']]);
					$this->totalWellCount += $numWells;
				}
				else {
					extract($v);
					$this->tmp_totals[$i -1]['coo_id'] .= ",$coo_id"; 
					$this->tmp_totals[$i -1]['comp_id'] .= ",$comp_id"; 
					$this->tmp_totals[$i -1]['tot_to_date'] += $tot_to_date; 

				 	unset($this->tmp_totals[$i]);
				// Average  array  merge
					$this->avArea[$v['area']][$v['well']]['tot_to_date'] += $v['tot_to_date'];
					$conName = $this->conNames[$v['con_id']];
					if (! stristr($conName,  $this->avArea[$v['area']][$v['well']]['con_names'] )) {
						$this->avArea[$v['area']][$v['well']]['con_names'] .= " , " . $conName;
					}


				}
			}  // end  average

		} // end  foreach

		//echo "<br /> <br />";


		$arr = ksort($this->avArea);
		//var_dump($this->tmp_totals);

		//if ($this->type == "average" ) {
			foreach ($this->avArea as $area=>$vall ) {
					foreach($vall as $i=>$vl) {
         			$arwell = explode(',',$i);
          			$numWells = count($arwell);
						if (!isset($this->areaWellCount[$area])) {
							//echo "creating $area  for $i  $numWells";
							$this->areaWellCount[$area] = $numWells;     
						}
						else {
							//echo "adding $area  for $i $numWells";
							$this->areaWellCount[$area] += $numWells;     
						}
					}
			}
		//}
	   //var_dump($this->avArea);

	}
	 private function heading($pdf) {
      // Heading Title 
		$pDate = "$this->startDate  to $this->endDate";
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
		if ($this->type == "contractor" ) {
      	$pdf->MultiCell(150, 0, "Well Lease Report - Completed", 0, 'C', false, 2, 75,2);
		}
		else {
      	$pdf->MultiCell(150, 0, "Area Report Well Costs & Averages", 0, 'C', false, 2, 75,2);
		}
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(80, 0, 'Period of Report:', 0, 'L', false, 0, 35,11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 75);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Total Cost:' ,0,'L', false, 0, 150);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, number_format($this->totalAll,2), 0, 'L', false, 2, 175);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(80, 0, 'Total Wells for Period', 0, 'L', false, 0, 35,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, $this->totalWellCount, 0, 'L', false, 0, 75);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Work Scope:' ,0,'L', false, 0, 150);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $this->crewStr, 0, 'L', false, 0, 175);

	}

	public function getTotAll($crewID) {
		reset($this->totals);
		foreach($this->totals as $ind => $val ) {
			if ($val['crew_id'] == $crewID) {
				return $val['tot_to_date'];
			}
		}

	}
	public function getColor($value) {
      reset($this->totals);
		if ($this->type == "crew" ) {    // ????
      	foreach($this->totals as $ind => $val ) {
         	if ($val['crew_id'] == $value) {
            	return $ind;
         	}
      	}
		}
		else {
      	foreach($this->totals as $ind => $val ) {
         	if ($val['contractor_id'] == $value) {
            	return $ind;
         	}
      	}
		}
   }
	public function getConTotAll($conID) {
		reset($this->totals);
		foreach($this->totals as $ind => $val ) {
			if ($val['contractor_id'] == $conID) {
				return array("tot_to_date"=>$val['tot_to_date'],"contractor_name"=>$val['contractor_name']);
			}
		}

	}
   public function contractorHeading($pdf,$conName,$conID,$Y) {
		$count = $this->wellConCount[$conID];
		reset($this->totals);
		foreach($this->totals as $i=>$v) {
			if ($v['con_id'] == $conID) {
				$tot = $v['total'];
				break;
			}
		}
		$this->currTot = $tot;
		$percTot = round(($tot / $this->totalAll ) * 100,2);
		$pdf->SetFillColor(241,121,61);
		$pdf->SetFont(PDF_FONT, '', 11);
		$pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(50,0,'Contractor',array('LTB'=>$this->borderStyle),'C',true,0,2,$Y);
      $pdf->MultiCell(22,0,'Total Wells',array('LTB'=>$this->borderStyle),'C',true,0,52,$Y);
      $pdf->MultiCell(35,0,'C.O.O/s',array('LTB'=>$this->borderStyle),'C',true,0,74,$Y);
      $pdf->MultiCell(35,0,'Completion Cert/s',array('LTB'=>$this->borderStyle),'C',true,0,109,$Y);
      $pdf->MultiCell(45,0,'Area',array('LTB'=>$this->borderStyle),'C',true,0,144,$Y);
      $pdf->MultiCell(55,0,'Well/s',array('LTB'=>$this->borderStyle),'C',true,0,189,$Y);
      $pdf->MultiCell(25,0,'Percentage',array('LTB'=>$this->borderStyle),'C',true,0,244,$Y);
      $pdf->MultiCell(26,0,'Total Cost',array('LTRB'=>$this->borderStyle),'C',true,2,269,$Y);
		$pdf->SetFont(PDF_FONT, '', 10);
		$pdf->SetTextColor(0,0,255);
		$Y+=5;
      $pdf->MultiCell(50,0,$conName,array('LRB'=>$this->borderStyle),'C',false,0,2,$Y);
      $pdf->MultiCell(22,0,$count,array('LRB'=>$this->borderStyle),'C',false,0,52,$Y);
      $pdf->MultiCell(170,0,'Percentage of Total',array('LRB'=>$this->borderStyle),'C',false,0,74,$Y);
      $pdf->MultiCell(25,0,$percTot."%",array('LRB'=>$this->borderStyle),'C',false,0,244,$Y);
      $pdf->MultiCell(26,0,number_format($tot,2),array('LRB'=>$this->borderStyle),'R',false,0,269,$Y);

	}

	public function  areaHeading($pdf,$area,$Y,$wells) {
// TODO this		 $w = array_slice($con,0,1);  // chop out  first  wells
 // TODO 		 $Y = $this->pageWells($pdf,count($w[0]),$Y,11);
	/*	if ( $Y > 265 ) {
			$pdf->AddPage();
			$this->heading($pdf);
			$Y=29;
			$pdf->SetY($Y);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont(PDF_FONT, '', 10);
		}  */
		$totalAll = $numWells = $numLines = 0;
		 foreach($wells as $well =>$val) {
         $totalAll += $val['tot_to_date'];
		 	$arr = explode(',',$well);
		 	$numWells += count($arr);
			$numLines ++;
       }
      $Y = $this->pageWells($pdf,$numLines,$Y,-20);
      $pdf->SetY($Y);
      /*if ( $Y + 5 + ($count  * 4.5) > 265 ) {
         $pdf->AddPage();
         $this->heading($pdf);
         $Y=29;
         $pdf->SetY($Y);
         $pdf->SetTextColor(0,0,0);
         $pdf->SetFont(PDF_FONT, '', 10);
      }   */
      // show average
      //$pdf->MultiCell(25,5,'Total Wells',array('LTB'=>$this->borderStyle),'C',true,0,175,$Y);
      //$pdf->MultiCell(95,5,"$this->crewStr",array('LTRB'=>$this->borderStyle),'C',true,2,200,$Y);
      $average = number_format(round($totalAll / $numWells ,2),2);

		$Y += 1;
		$count = $this->areaWellCount[$area];     
		$pdf->SetFillColor(241,121,61);
		$pdf->SetFont(PDF_FONT, '', 11);
		$pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(55,5,'Area',array('LTB'=>$this->borderStyle),'C',true,0,2,$Y);
      $pdf->MultiCell(103,5,'',array('LTB'=>$this->borderStyle),'C',true,0,57,$Y);
      $pdf->MultiCell(23,5,'Total Wells',array('LTB'=>$this->borderStyle),'C',true,0,160,$Y);
      $pdf->MultiCell(42,5,'',array('LTB'=>$this->borderStyle),'C',true,0,183,$Y);
      $pdf->MultiCell(35,0,'Average Well Cost',array('LTRB'=>$this->borderStyle),'C',true,2,225,$Y);
      $pdf->MultiCell(35,0,'Area Total Cost',array('LTRB'=>$this->borderStyle),'C',true,2,260,$Y);
		//$pdf->SetFont(PDF_FONT, 'B', 10);
      //$pdf->MultiCell(112,5,"$this->crewStr",array('LTRB'=>$this->borderStyle),'C',true,2,183,$Y);
		//$pdf->SetFont(PDF_FONT, 'B', 10);
		//$pdf->SetTextColor(0,0,255);
		$Y+=5;
		$pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(55,5,$area,array('LRB'=>$this->borderStyle),'L',false,0,2,$Y);
      $pdf->MultiCell(23,5,$numWells,array('LR'=>$this->borderStyle),'C',false,2,160,$Y);
     	$pdf->MultiCell(35,0,$average,array('LTR'=>$this->borderStyle),'R',false,2,225,$Y);
     	$pdf->MultiCell(35,0,number_format($totalAll,2),array('LTR'=>$this->borderStyle),'R',false,2,260,$Y);
		$pdf->SetTextColor(0,0,0);
		return $Y;

	}
	 public function pageWells($pdf,$count,$Y,$offset=0) {  //  offset if splitting with areaHeading
      if ( $Y + $offset + 5 + ($count  * 4.5) > 165 ) {
         $pdf->AddPage();
         $this->heading($pdf);
         $Y=29;
         $pdf->SetY($Y);
         $pdf->SetTextColor(0,0,0);
         $pdf->SetFont(PDF_FONT, '', 10);
      }
		return $Y;
   }

   public function conAvHeading($pdf,$wells,$Y) {
		//var_dump($wells);
 // TODO		$Y = $this->pageWells($pdf,$numWells,$Y,-20);
 //TODO       $pdf->SetY($Y);
		/*if ( $Y + 5 + ($count  * 4.5) > 265 ) {
			$pdf->AddPage();
			$this->heading($pdf);
			$Y=29;
			$pdf->SetY($Y);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont(PDF_FONT, '', 10);
		}   */
		// show average
      //$pdf->MultiCell(25,5,'Total Wells',array('LTB'=>$this->borderStyle),'C',true,0,175,$Y);
      //$pdf->MultiCell(95,5,"$this->crewStr",array('LTRB'=>$this->borderStyle),'C',true,2,200,$Y);
		$pdf->SetFillColor(241,175,141);
		$pdf->SetFont(PDF_FONT, '', 11);
		$pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(103,0,'Contractor/s',array('LTB'=>$this->borderStyle),'C',true,0,57,$Y);
      $pdf->MultiCell(23,0,'Num Wells',array('LTB'=>$this->borderStyle),'C',true,0,160,$Y);
      $pdf->MultiCell(77,0,'Well/s',array('LTB'=>$this->borderStyle),'C',true,0,183,$Y);
      $pdf->MultiCell(35,0,' Well/s Total Cost',array('LTB'=>$this->borderStyle),'C',true,0,260,$Y);
		//$pdf->SetTextColor(0,0,255);
		$Y+=5;
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont(PDF_FONT, '', 10);
		foreach($wells as $name =>$val) {
			$arr = explode(',',$name);
			$numWells =  count($arr);
			$height = ceil(strlen($name) /40) * 4.5;
         $height = $height >= 4.5 ? $height : 4.5;

      	$pdf->MultiCell(103,$height,$val['con_names'],array('LRB'=>$this->borderStyle),'L',false,0,57,$Y);
      	$pdf->MultiCell(23,$height,$numWells,array('LRB'=>$this->borderStyle),'C',false,0,160,$Y);
			$pdf->SetTextColor(0,0,0);
      	$pdf->MultiCell(77,$height,$name,array('LTB'=>$this->borderStyle),'L',false,0,183,$Y);
      	$pdf->MultiCell(35,$height,number_format($val['tot_to_date'],2),array('LTRB'=>$this->borderStyle),'R',false,2,260,$Y);
			$Y+= $height;
			if ($Y >= 180) {
				$Y += 5;
      		$pdf->SetFont(PDF_FONT, '', 11);
				$pdf->MultiCell(45,0,'Continued ...',0,'L',false,1,3,$Y);
      		$pdf->SetFont(PDF_FONT, '', 10);
				$pdf->AddPage();
         	$this->heading($pdf);
         	$Y=29;
			}
		}

	}

}
?>
