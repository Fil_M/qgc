<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once('classes/tcpdf/config/lang/eng.php');
require_once('classes/tcpdf/tcpdf.php');
require_once('classes/Functions.php');

class MYPDFLAND extends TCPDF {
	public $arr;
	public $addr;
    //Page header
    public function Header() {
		$this->arr = Functions::getLogo();
		$this->addr = Functions::getAddress();
      // Logo
      $this->Image($this->arr['logo'], 1, 3,30 , '', 'PNG', '', 'T', false, 72, '', false, false, 0, false, false, false);
      // Set font
      $this->SetFont(PDF_FONT, '', 9);
      // Title
      $this->MultiCell(0, 0, $this->addr->address1, 0, 'L', false, 2, 235, 2);
      $this->MultiCell(0, 0, $this->addr->address2, 0, 'L', false, 2, 235);
      $this->MultiCell(0, 0, $this->addr->suburb.' '.$this->addr->postcode, 0, 'L', false, 2, 235);
      $this->Ln();
      $this->MultiCell(15, 0, 'PHONE:', 0, 'L', false, 0, 235);
      $this->MultiCell(0, 0, $this->addr->phone, 0, 'L', false, 2, 250);
      $this->MultiCell(15, 0, 'FAX:', 0, 'L', false, 0, 235);
      $this->MultiCell(0, 0, $this->addr->fax, 0, 'L', false, 2, 250);
      $this->SetLineStyle(array("width"=>0.5));
      $this->Line(1, 29, 296, 29);
    }

}
?>
