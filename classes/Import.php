<?php
 // include the TCPDF class

require_once('classes/tcpdf/config/lang/eng.php');
require_once('classes/tcpdf/tcpdf.php');
require_once('classes/tcpdf/tcpdf_parser.php');

 /**
51 * @class TCPDF_IMPORT
52 * !!! THIS CLASS IS UNDER DEVELOPMENT !!!
53 * PHP class extension of the TCPDF (http://www.tcpdf.org) library to import existing PDF documents.<br>
54 * @package com.tecnick.tcpdf
55 * @brief PHP class extension of the TCPDF library to import existing PDF documents.
56 * @version 1.0.001
57 * @author Nicola Asuni - info@tecnick.com
58 */
class TCPDF_IMPORT extends TCPDF {

 /**
62 * Import an existing PDF document
63 * @param $filename (string) Filename of the PDF document to import.
64 * @return true in case of success, false otherwise
65 * @public
66 * @since 1.0.000 (2011-05-24)
67 */
	public function importPDF($filename) {
 // load document
 	$rawdata = file_get_contents($filename);
 	if ($rawdata === false) {
 		$this->Error('Unable to get the content of the file: '.$filename);
 	}
 // configuration parameters for parser
 	$cfg = array(
 	'die_for_errors' => false,
 	'ignore_filter_decoding_errors' => true,
 	'ignore_missing_filter_decoders' => true,
 	);
 	try {
 // parse PDF data
 	$pdf = new TCPDF_PARSER($rawdata, $cfg);
	} catch (Exception $e) {
 		die($e->getMessage());
 	}
 // get the parsed data
 	$data = $pdf->getParsedData();
 // release some memory
	unset($rawdata);

 // ...


 	print_r($data); // DEBUG


 	unset($pdf);
 	}

} // END OF CLASS
?>
