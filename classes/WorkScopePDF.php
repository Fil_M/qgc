<?php //QGC   New Sub scopes
error_reporting(E_ALL);
ini_set('display_errors','1');
class WorkScopePDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $crewID;
	public $conID;
	public $crewName;
	public $shortName;
	public $estimate;
	public $totals=array();
	public $tmp_totals=array();
	public $totalAll= 0;
	public $totalAllExt= 0;
	public $startDate;
	public $endDate;
	public $type;
	public $split;
	public $conArr;
	public $splitArr = array();
	public $crewArr;
	public $color=array("0"=>array(255.0,65.0,65.0),"1"=>array(255.0,238.0,65.0),"2"=>array(86.0,255.0,65.0),"3"=>array(65.0,119.0,255.0),"4"=>array(255.0,152.0,65.0),"5"=>array(255.0,65.0,181.0),
	"6"=>array(160.0,65.0,255.0),"7"=>array(65.0,255.0,243.0),"8"=>array(222.0,255.0,147.0),"9"=>array(110.0,80.0,80.0),"10"=>array(127.0,127.0,127.0),"11"=>array(205.0,229.0,229.0),"12"=>array(191.0,191.0,191.0));

	public $sub_color=array("8"=>array(255.0,65.0,65.0),"7"=>array(255.0,238.0,65.0),"6"=>array(86.0,255.0,65.0),"5"=>array(65.0,119.0,255.0),"4"=>array(255.0,152.0,65.0),"3"=>array(255.0,65.0,181.0),
	"2"=>array(160.0,65.0,255.0),"1"=>array(65.0,255.0,243.0),"0"=>array(222.0,255.0,147.0),"9"=>array(110.0,80.0,80.0),"10"=>array(127.0,127.0,127.0),"11"=>array(205.0,229.0,229.0),"12"=>array(191.0,191.0,191.0));

	public function __construct($action="print",$type="contractor",$split=true,$crewID,$conID,$startDate,$endDate) {
		$this->conn = $GLOBALS['conn'];
		$this->crewID = $crewID;
		$this->conID = $conID;
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->type = $type;
		$this->split = $split;
		$typeTitle = $type == "contractor" ? "Contractor" : "All Work Scopes";

		$this->getDetails();
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Work Scope Report by $typeTitle $startDate - $endDate");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$this->pieChart($pdf);
		if ($this->split && intval($this->crewID == 0 )) {
			$pdf->AddPage();
			$this->heading($pdf);
			$Y=22;
			$this->showSplit($pdf,$Y);
		}

		// Add a page
		
		// ---------------------------------------------------------
  
	   // Output to file system , load email_log db table  and email
		$dat = preg_replace('/-/',"",Functions::dbDate($this->startDate));
		$name = "tmp/WorkScope_{$dat}.pdf";
		if ($action == "print" ) {
			$pdf->Output($name, 'I');
		}
   }		
	private function pieChart($pdf) {
		if ($this->type == "contractor" ) {
			if ($this->crewID == 0 ) {
				$pieTitle = "All Workscopes";
			}
			else {
				$pieTitle = "Workscope - " . $this->crewArr[$this->crewID]['crew_name'];
			}

		}
		else {
			if ($this->conID == 0 ) {
				$pieTitle = "All Contractors";
			}
			else {
				$pieTitle = "Contractor - " .$this->conArr[0]['con_name'];  // only oone contractor in array
			}
		}
      $pdf->SetFont(PDF_FONT, '', 12);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(50,6,$pieTitle,0,'C',false,0,80,32);
		$Y = 92;
		//$pdf->MultiCell(51,0.25,'',array('LRTB'=>$this->borderStyle),'C',false,0,153,$Y);

		$xc = 105;
		$r = 50;
		$cnt = count($this->totals)-1 ;
		for ($x = $cnt;$x>=0;$x-- ) {
			$perc = $rotate = $tot = 0;
			if (isset($this->color[$x])) {
				$colr = $this->color[$x];
			}
			else {
				$colr = array(255.0,255.0,255.0); // default  white  no colurs  left
			}
			$itot = $this->totals[$x]['tot_to_date'];
			$iPerc = round((($itot / $this->totalAll ) * 100),2) ;
			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
			for ($y = $x;$y>=0;$y--) {
				$tot += $this->totals[$y]['tot_to_date'];
			}
			$perc =round((($tot / $this->totalAll ) * 100),2) ;
			$rotate  =  (360  *   ($perc / 100 )) ;
			$pdf->PieSector($xc, $Y, $r, 0, $rotate, 'FD', true, 90);
		}

		$Y += 70;
      $pdf->SetFont(PDF_FONT, 'B', 11);
      $pdf->SetTextColor(0,0,0);
		reset($this->totals);
		if ($this->type == "contractor" || intval($this->crewID) > 0 ) {
			$pdf->MultiCell(80,6,"CONTRACTOR",array('LTRB'=>$this->borderStyle),'C',false,0,5,$Y);
		}
		else {
			$pdf->MultiCell(80,6,"WORK SCOPE",array('LTR'=>$this->borderStyle),'C',false,0,5,$Y);

		}
		$pdf->MultiCell(60,6,"PERCENTAGE of TOTAL",array('LTB'=>$this->borderStyle),'C',false,0,85,$Y);
		$pdf->MultiCell(60,6,"TOTAL",array('LTRB'=>$this->borderStyle),'C',false,0,145,$Y);
     	$pdf->SetFont(PDF_FONT, '', 12);
		$Y +=6;
		for ($x = 0;$x<=$cnt;$x++) {
			if (isset($this->color[$x])) {
				$colr = $this->color[$x];
			}
			else {
				$colr = array(255.0,255.0,255.0); // default  white  no colours  left
			}
			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
			$tot =  "$" . number_format($this->totals[$x]['tot_to_date'],2);
			if ($this->type == "contractor" ) {
				$name = $this->totals[$x]['contractor_name'];
			}
			else {
				$name = $this->totals[$x]['crew_name'];
			}
			$itot = $this->totals[$x]['tot_to_date'];
			$iPerc = number_format(round((($itot / $this->totalAll ) * 100),2),2) ;
			$pdf->MultiCell(80,6,$name,array('LTRB'=>$this->borderStyle),'L',true,0,5,$Y);
			$pdf->MultiCell(60,6,$iPerc,array('LTRB'=>$this->borderStyle),'C',true,0,85,$Y);
			$pdf->MultiCell(60,6,$tot,array('LTRB'=>$this->borderStyle),'R',true,0,145,$Y);
			$Y +=6;
		}  
/*
      $pdf->SetFont(PDF_FONT, 'B', 12);
		$pdf->MultiCell(40,0,'TOTAL',0,'R',false,0,120,$Y,true,0,false,true,10,'B');
		$tot = '$' .number_format($this->totalAll,2);
		$pdf->MultiCell(40,10,$tot,array('B'=>$this->borderStyle),'R',false,1,165,$Y,true,0,false,true,10,'B');
		$Y += 11;
		$pdf->Line(165,$Y,205,$Y);

*/
	}
	private function getDetails(){
		$this->crewArr = Functions::getNewCrew();
		$dateClause = $gravDateClause = "";

		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$dateClause = " and hour_date between '$this->startDate'  and '$this->endDate' ";
			$gravDateClause = " and gravel_date between '$this->startDate'  and '$this->endDate' ";

		}
		//$this->conArr = Functions::getExtCon();  // includes externals  no longer used ???
		$this->conArr = Functions::getAllCon($this->conID);  // > 0 returns one contractor only 


		if ($this->type == "contractor" ) {
			$this->doContractor($dateClause,$gravDateClause);
		}
		else {
			$this->doCrew($this->conArr,$dateClause,$gravDateClause);
		}
	}
	 private function heading($pdf) {
      // Heading Title 
		
		if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
			$pDate = "$this->startDate  to $this->endDate";
		}
		else {
			$pDate = "ALL DATES";
		}
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
      $pdf->MultiCell(150, 0, "Workscope Report", 0, 'C', false, 2, 30,2);
      $pdf->SetFont(PDF_FONT, '', 12);
		if ($this->type == "contractor" ) {
      	$pdf->MultiCell(40, 0, 'By Contractor', 0, 'C', false, 0, 85,9);
		}
		else {
      	$pdf->MultiCell(40, 0, 'All Work Scopes', 0, 'C', false, 0, 85,9);
		}
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(15, 0, 'From:', 0, 'L', false, 0, 40,18);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 53);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(15, 0, 'Total:' ,0,'L', false, 0, 110);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, number_format($this->totalAll,2), 0, 'L', false, 2, 130);

	}

   private function showSplit($pdf,$Y) {
		$newSplit = array();
		$Y = 31;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 11);
      $Y += 5;
		if (count($this->splitArr) > 0 ) {
			if ($this->type == "crew" ) {
      		$pdf->MultiCell(100, 0, "Sub Scope BreakDown", 0, 'C', false, 2, 58,30);
				$this->showSplitCrew($pdf,$Y);
			}
         else {
      		$pdf->MultiCell(100, 0, "Work Scope BreakDown", 0, 'C', false, 2, 58,30);
				$this->showSplitCont($pdf,$Y);
			}
		} // end  count  splitArr
	}

	public function getTotAll($crewID) {
		reset($this->totals);
		foreach($this->totals as $ind => $val ) {
			if ($val['crew_id'] == $crewID) {
				return $val['tot_to_date'];
			}
		}

	}
	public function getColor($value) {
      reset($this->totals);
		if ($this->type == "crew" ) {
      	foreach($this->totals as $ind => $val ) {
         	if ($val['crew_id'] == $value) {
            	return $ind;
         	}
      	}
		}
		else {
      	foreach($this->totals as $ind => $val ) {
         	if ($val['contractor_id'] == $value) {
            	return $ind;
         	}
      	}
		}
   }
	public function getConTotAll($conID) {
		reset($this->totals);
		foreach($this->totals as $ind => $val ) {
			if ($val['contractor_id'] == $conID) {
				return array("tot_to_date"=>$val['tot_to_date'],"contractor_name"=>$val['contractor_name']);
			}
		}

	}
	public function newPage($pdf,$title) {
		$pdf->addPage();
		$this->heading($pdf);
		$pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 12);
      $pdf->MultiCell(100, 0, $title, 0, 'C', false, 2, 58,30);
      $pdf->SetFont(PDF_FONT, '', 10);
	}

	private function doContractor($dateClause,$gravDateClause) {
		if (intval($this->crewID) > 0 ) {
			$this->crewName = $this->crewArr[$this->crewID]['crew_name'];
			$crewClause = " and crew_id = $this->crewID";
         $gravDateClause .= $crewClause;
		}
		else {
			$crewClause = "";
		}
		foreach ( $this->conArr as $i=>$v ) {  // set up indexed totals  array
				$this->tmp_totals[$v['contractor_id']] = array("tot_to_date"=>0,"contractor_name"=>$v['con_name']);
		}
		reset($this->conArr);
		foreach($this->conArr as $ind=>$val) {
			$shortName = $val['name'];
			$conName = $val['con_name'];
			$conID = $val['contractor_id'];
			if ($shortName != "EXTERNAL" ) {
				$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) + sum(coalesce(expense,0)) as tot_to_date 
				from {$shortName}_hour  where status >= 5 and removed is false $crewClause  $dateClause";
			}
        //  echo "<br />$sql</br />";
/*				else if (intval($this->crewID) == 0 ) {  // External Contractors
					$sql = "select amount as tot_to_date from qgc_work_scope where  ext_contractor_id = $conID  and end_date = '$this->endDate' ";
				}    */
				if (intval($this->crewID) == 0 || ($shortName != "EXTERNAL" && intval($this->crewID) > 0)  ) {  //  all contractors  single  work  scope  no  externals
            //    echo "here";
					if (! $totAll = $this->conn->getOne($sql)) {
						if ($this->conn->ErrorNo() != 0 ) {
							die($this->conn->ErrorMsg());
						}
						else {
							$this->totalAll += 0;
							$this->tmp_totals[$conID]['tot_to_date'] = 0;
				//			echo "not finished";
							//echo $sql;
						}
					}
					else {
						//echo "adding $totAll to $shortName";
						$this->totalAll += $totAll;
						$this->tmp_totals[$conID]['tot_to_date'] = $totAll;
					}
				}
				 // water/Gravel get totals
            if ($shortName != "EXTERNAL" ) {
               $sql = "SELECT sum(coalesce(gravel_total,0)) + sum(coalesce(water_total,0)) as tot_to_date 
               from {$shortName}_gravel_water  where 1 = 1   $gravDateClause";
						//echo $sql;
               if (! $totAll = $this->conn->getOne($sql)) {
                  if ($this->conn->ErrorNo() != 0 ) {
                     die($this->conn->ErrorMsg());
                  }
						else {
							//echo $sql;
						}
               }
               else {
                  $this->totalAll += $totAll;
                  $this->tmp_totals[$conID]['tot_to_date'] += $totAll;    // add water gravel  Totals per contractor
               }
            }

////     Patch in Sub Scopes ???
				if ($this->split && intval($this->crewID) == 0) {
					foreach($this->crewArr as $crewInd=>$val) {
						if ($shortName != "EXTERNAL" ) {
							$crewName = $this->crewArr[$crewInd]['crew_name'];
							$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) + sum(coalesce(expense,0)) as tot_to_date ,crew_name
               		from {$shortName}_hour h LEFT JOIN crew using(crew_id) where status >= 5 and h.removed is false and crew_id = $crewInd  $dateClause group by crew_name";
                         //echo "<br />$crewInd $sql</br />";
            			if (! $totArr = $this->conn->getRow($sql)) {
               			if ($this->conn->ErrorNo() != 0 ) {
                 				die($this->conn->ErrorMsg());
								}
								else {
									$this->splitArr[$conID][$crewInd] = array("tot_to_date"=>0,"crew_name"=>$crewName,"contractor"=>$conName,"contractor_id"=>$conID,
										"water_volume"=>0,"water_tot"=>0,"grav_volume"=>0,"grav_tot"=>0);
								}
							}
							else { 
								$this->splitArr[$conID][$crewInd] = array("tot_to_date"=>$totArr['tot_to_date'],"crew_name"=>$totArr['crew_name'],"contractor"=>$conName,"contractor_id"=>$conID,
								"water_volume"=>0,"water_tot"=>0,"grav_volume"=>0,"grav_tot"=>0);
							}
						} //  External Contractor

					// add water and gravel 
					if ($shortName != "EXTERNAL" ) {
            		$sql = "SELECT sum(coalesce(gravel_total,0))  as grav_tot,  sum(coalesce(water_total,0)) as water_tot,sum(coalesce(gravel_units,0)) as grav_volume,
						sum(coalesce(water_units,0)) as water_volume
            		from {$shortName}_gravel_water  where crew_id = $crewInd $gravDateClause";
						//echo $sql;
            		if (! $totArr = $this->conn->getRow($sql)) {
               		if ($this->conn->ErrorNo() != 0 ) {
                 			die($this->conn->ErrorMsg());
               		}
							else {
								//echo $sql;
							}
            		}
						else {
							$this->splitArr[$conID][$crewInd]['tot_to_date'] += ($totArr['water_tot'] + $totArr['grav_tot'] );
							$this->splitArr[$conID][$crewInd]['water_volume'] = $totArr['water_volume']; 
							$this->splitArr[$conID][$crewInd]['grav_volume'] = $totArr['grav_volume']; 
							$this->splitArr[$conID][$crewInd]['water_tot'] = $totArr['water_tot']; 
							$this->splitArr[$conID][$crewInd]['grav_tot'] = $totArr['grav_tot']; 
						}
					}  // end exclude external
				}  // end loop thru crewArr
			}  // end  this  split
		} // end  each contractor
         // cleanup split remove blank totals water gravel or tot_to_date
			$this->clean_splitArr();
			//var_dump($this->splitArr);
			reset($this->tmp_totals);

			//var_dump($this->tmp_totals);
			foreach($this->tmp_totals as $indx=>$vall) {   // load into $this->totals array
				if($vall['tot_to_date'] == 0) {
					unset($this->tmp_totals[$indx]);
				} 
			}  
			//  rebiuld $this totals as indexed array
			reset($this->tmp_totals);
			foreach($this->tmp_totals as $indx=>$vall) {   // load into $this->totals array
				$this->totals[] = array('tot_to_date'=>$vall['tot_to_date'],'contractor_id'=>$indx,'contractor_name'=>$vall['contractor_name']);
		   }
	}  // end do contractor
	private function doCrew($conArr,$dateClause,$gravDateClause) {
		foreach ( $this->crewArr as $i=>$v ) {  // set up indexed totals  array
			$this->tmp_totals[$i] = array("tot_to_date"=>0,"crew_name"=>$v['crew_name']);
		}
		reset($this->crewArr);
		foreach($this->crewArr as $ind=>$val) {
			extract($val);
			reset($conArr);
     		foreach($conArr as $i=>$v) {
           	$shortName = $v['name'];
           	$conName = $v['con_name'];
           	$conID = $v['contractor_id'];
				if ($shortName != "EXTERNAL" ) {   // get total ALL
					$this->getTotalAllCrew($shortName,$dateClause,$gravDateClause,$ind);
				}
				// split work scope
				if ($this->split) {
					if ($shortName != "EXTERNAL" ) {
						$this->getSubCrewTotals($shortName,$conID,$crew_in_clause,$dateClause,$gravDateClause,$ind);
					}
            }  // end if split

			} // end foreach conarr
		} // end forach  crew
		// clean 0  totals in spliArr

		$this->clean_splitArr();

				//var_dump($this->tmp_totals);
		reset($this->tmp_totals);
		foreach($this->tmp_totals as $indx=>$vall) {   // load into $this->totals array
			if($vall['tot_to_date'] == 0) {
				unset($this->tmp_totals[$indx]);
			} 
		} 
			//  rebiuld $this totals as indexed array
		reset($this->tmp_totals);
		foreach($this->tmp_totals as $indx=>$vall) {   // load into $this->totals array
			$this->totals[] = array('tot_to_date'=>$vall['tot_to_date'],'crew_id'=>$indx,'crew_name'=>$vall['crew_name']);
	  	}
			
	} // end func 
	private function getTotalAllCrew($shortName,$dateClause,$gravDateClause,$ind) {
  		$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) + sum(coalesce(expense,0)) as tot_to_date 
  		from {$shortName}_hour where status >= 5 and removed is false and crew_id  = $ind $dateClause";

		//if ($ind == 2) {
		//	echo "<br />$sql<br />";
	//	}
	

     	if (! $totAll = $this->conn->getOne($sql)) {
        	if ($this->conn->ErrorNo() != 0 ) {
           	die($this->conn->ErrorMsg());
        	}
      }
		else {
        	$this->totalAll += $totAll;
        	$this->tmp_totals[$ind]['tot_to_date'] += $totAll;
		}
		// water/Gravel get totals
     		$sql = "SELECT sum(coalesce(gravel_total,0)) + sum(coalesce(water_total,0)) as tot_to_date 
     		from {$shortName}_gravel_water where crew_id = $ind $gravDateClause";
     	if (! $totAll = $this->conn->getOne($sql)) {
     		if ($this->conn->ErrorNo() != 0 ) {
        		die($this->conn->ErrorMsg());
     		}
     	}
		else {
     		$this->totalAll += $totAll;
     		$this->tmp_totals[$ind]['tot_to_date'] += $totAll;
		}

	}
	private function getTotalAllExt($conID,$subCrewClause) {
		$sql = "select amount as tot_to_date from qgc_work_scope where  ext_contractor_id = $conID  and end_date = '$this->endDate' and crew_id in $subCrewClause";
     	if (! $totAll = $this->conn->getOne($sql)) {
        	if ($this->conn->ErrorNo() != 0 ) {
           	die($this->conn->ErrorMsg());
        	}
			else {
							//echo $sql;
			}
      }
		else {
        	$this->totalAll += $totAll;
        	$this->tmp_totals[$ind]['tot_to_date'] += $totAll;
		}
						//echo $sql;
  	}
	private function getSubCrewTotals($shortName,$conID,$subCrewClause,$dateClause,$gravDateClause,$ind) {
		if (strlen($subCrewClause) > 0 ) {
      	$subArr = explode(",",$subCrewClause);
        	foreach ($subArr  as $subCrew ) {  // Individual Sub Crew totals
        		$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0))  + sum(coalesce(expense,0)) as tot_to_date ,sub_crew_name
        		from {$shortName}_hour h LEFT JOIN sub_crew using(sub_crew_id) where status >= 5 and h.removed is false and sub_crew_id = $subCrew  $dateClause group by sub_crew_name";
        		if (! $totArr = $this->conn->getRow($sql)) {
           		if ($this->conn->ErrorNo() != 0 ) {
              		die($this->conn->ErrorMsg());
           		}
           	}
           	else {
           		$found = false;
           		if (array_key_exists($ind,$this->splitArr)) {
              		foreach($this->splitArr[$ind] as $ky=>$hh ) {
                 		if($subCrew == $ky) {
                    		$this->splitArr[$ind][$subCrew]['tot_to_date'] += $totArr['tot_to_date'];
                    		$found = true;
                    		break;
                		}
             		}
              }
              else {
              		$this->splitArr[$ind][$subCrew] = array("tot_to_date"=>$totArr['tot_to_date'],"crew_name"=>$totArr['sub_crew_name'],
						"grav_volume"=>NULL,"grav_tot"=>NULL,"water_volume"=>NULL,"water_tot"=>NULL);
              		$found = true;
              	}
              if (! $found ) {
              		$this->splitArr[$ind][$subCrew] = array("tot_to_date"=>$totArr['tot_to_date'],"crew_name"=>$totArr['sub_crew_name'],
						"grav_volume"=>NULL,"grav_tot"=>NULL,"water_volume"=>NULL,"water_tot"=>NULL);
              }
           	} // end if  found  sql


								// add water and gravel 
        		$sql = "SELECT sum(coalesce(gravel_total,0))  as grav_tot,  sum(coalesce(water_total,0)) as water_tot,
				sum(coalesce(gravel_units,0)) as grav_volume,sum(coalesce(water_units,0)) as water_volume,sub_crew_name 
        		from {$shortName}_gravel_water
				LEFT JOIN sub_crew using(sub_crew_id) 
			  where sub_crew_id = $subCrew  $gravDateClause group by sub_crew_name";
							//echo "<br />$sql <br />";
        		if (! $totArr = $this->conn->getRow($sql)) {
        			if ($this->conn->ErrorNo() != 0 ) {
            		die($this->conn->ErrorMsg());
           		}
        		}
				else {
           		$found = false;
           		if (!is_null($totArr['grav_volume']) ) {      // Gravel
           			if (array_key_exists($ind,$this->splitArr)) {
							foreach($this->splitArr[$ind] as $ky=>$hh ) {
                     	if($subCrew == $ky) {
        							$this->splitArr[$ind][$subCrew]['tot_to_date'] += $totArr['grav_tot'];
        							$this->splitArr[$ind][$subCrew]['grav_volume'] += $totArr['grav_volume'];
        							$this->splitArr[$ind][$subCrew]['grav_tot'] += $totArr['grav_tot'];
                        	$found = true;
                        	break;
                     	}
                  	}
						

						}
						else {
              			$this->splitArr[$ind][$subCrew] = array("tot_to_date"=>$totArr['grav_tot'],"crew_name"=>$totArr['sub_crew_name']
							,"grav_volume"=>$totArr['grav_volume'],"grav_tot"=>$totArr['grav_tot'],"water_volume"=>NULL,"water_tot"=>NULL);
						}
						if (! $found ) {
              			$this->splitArr[$ind][$subCrew] = array("tot_to_date"=>$totArr['grav_tot'],"crew_name"=>$totArr['sub_crew_name']
							,"grav_volume"=>$totArr['grav_volume'],"grav_tot"=>$totArr['grav_tot'],"water_volume"=>NULL,"water_tot"=>NULL);
              		}

					}
           		$found = false;
					if (!is_null($totArr['water_volume'])) { // Water
           			if (array_key_exists($ind,$this->splitArr)) {
							 foreach($this->splitArr[$ind] as $ky=>$hh ) {
                        if($subCrew == $ky) {
     								$this->splitArr[$ind][$subCrew]['tot_to_date'] += $totArr['water_tot'];
     								$this->splitArr[$ind][$subCrew]['water_volume'] += $totArr['water_volume'];
     								$this->splitArr[$ind][$subCrew]['water_tot'] += $totArr['water_tot'];
                           $found = true;
                           break;
                        }
                     }

						}
						else {
                 		$this->splitArr[$ind][$subCrew] = array("tot_to_date"=>$totArr['water_tot'],"crew_name"=>$totArr['sub_crew_name']
						,"grav_volume"=>NULL,"grav_tot"=>NULL,"water_volume"=>$totArr['water_volume'],"water_tot"=>$totArr['water_tot']);
						}
						if (! $found ) {
                 		$this->splitArr[$ind][$subCrew] = array("tot_to_date"=>$totArr['water_tot'],"crew_name"=>$totArr['sub_crew_name']
						,"grav_volume"=>NULL,"grav_tot"=>NULL,"water_volume"=>$totArr['water_volume'],"water_tot"=>$totArr['water_tot']);
                  }
	
					}
				}   
          
	   	} // end  FOREACH subcrew 
		}  //  end no sub clasuse
		// get all crew data without sub crew specs
		$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0))  + sum(coalesce(expense,0)) as tot_to_date 
            from {$shortName}_hour h  where status >= 5 and h.removed is false and h.crew_id = $ind and h.sub_crew_id is null  $dateClause";
   	if (! $totArr = $this->conn->getRow($sql)) {
   		if ($this->conn->ErrorNo() != 0 ) {
     			die($this->conn->ErrorMsg());
			}
     	}
        	$this->splitArr[$ind][-1] = array("tot_to_date"=>$totArr['tot_to_date'],"crew_name"=>"No Sub Scope",
			"grav_volume"=>NULL,"grav_tot"=>NULL,"water_volume"=>NULL,"water_tot"=>NULL);

		$sql = "SELECT sum(coalesce(gravel_total,0))  as grav_tot,  sum(coalesce(water_total,0)) as water_tot,
            sum(coalesce(gravel_units,0)) as grav_volume,sum(coalesce(water_units,0)) as water_volume 
            from {$shortName}_gravel_water gw
           where gw.crew_id = $ind and gw.sub_crew_id is null $gravDateClause ";
   	if (! $totArr = $this->conn->getRow($sql)) {
   		if ($this->conn->ErrorNo() != 0 ) {
     			die($this->conn->ErrorMsg());
			}
     	}
		else {
			if( $totArr['water_tot'] + $totArr['grav_tot'] > 0 ) { 	
        		$this->splitArr[$ind][-1]['tot_to_date'] += ($totArr['water_tot'] + $totArr['grav_tot']); 
        		$this->splitArr[$ind][-1]['grav_tot'] = $totArr['grav_tot']; 
        		$this->splitArr[$ind][-1]['water_tot'] = $totArr['water_tot']; 
        		$this->splitArr[$ind][-1]['grav_volume'] = $totArr['grav_volume']; 
        		$this->splitArr[$ind][-1]['water_volume'] = $totArr['water_volume']; 
			}
		}


	} // end  func
	private function getExtSubTotals() {
		if (intval($totAll) > 0 ) {
        	if (array_key_exists($ind,$this->splitArr)) {
        		$this->splitArr[$ind][$ind]['tot_to_date'] += $totAll;
			}
			else {
        		$this->splitArr[$ind][$ind] = array("tot_to_date"=>$totAll,"crew_name"=>$this->crewArr[$ind]['crew_name'],"grav_volume"=>NULL,"grav_tot"=>NULL,"water_volume"=>NULL,"water_tot"=>NULL);
			}
		}
	} // end  func
	private function showSplitCrew($pdf,$Y) {	
 //echo "<br />  <br />  <br />";
		$prevCrewID = -1;
		foreach($this->splitArr as $i=>$v) {
			$crewID = $i;
			if (count($v) > 1 ) {
					foreach($v as $ind=>$val) {
					extract($val);
					$water_tot = !is_null($water_tot) ?  number_format($water_tot,2) : "";
					$grav_tot = !is_null($grav_tot) ?  number_format($grav_tot,2) : "";
					$volume="";
					if ($crewID != $prevCrewID ) {
						$x = 0;
						if($prevCrewID != -1 ) {
               		$Y += 5;
							$pdf->Line(2,$Y,206,$Y);
							$this->newPage($pdf,"Sub Scope BreakDown");
							$Y = 40;
						}
						$this->subPieChart($pdf,$crewID,$v);
						$Y = 141;
						$totalAll = $this->getTotAll($crewID);
						$prevCrewID = $crewID;
						$Y += 5;
      				$pdf->SetFont(PDF_FONT, '', 10);
               	$pdf->SetFillColor(255,255,255);    // sub scope headings
						$Y += 5;
      				$pdf->MultiCell(50, 5, "Sub Scope", array('LTR'=>$this->borderStyle), 'C', false, 0, 2,$Y);
      				$pdf->MultiCell(25, 5, 'Water Volume', array('TR'=>$this->borderStyle), 'C', false, 2, 52,$Y);
      				$pdf->MultiCell(25, 5, 'Water Cost', array('TR'=>$this->borderStyle), 'C', false, 2, 77,$Y);
      				$pdf->MultiCell(25, 5, 'Gravel Volume', array('TR'=>$this->borderStyle), 'C', false, 2, 102,$Y);
      				$pdf->MultiCell(25, 5, 'Gravel Cost', array('TR'=>$this->borderStyle), 'C', false, 2, 127,$Y);
      				$pdf->MultiCell(22, 5, "Percentage", array('TR'=>$this->borderStyle), 'C', false, 2, 152,$Y);
      				$pdf->MultiCell(32, 5, "Total", array('TR'=>$this->borderStyle), 'C', false, 2, 174,$Y);
						$Y += 5;
						$totalAll = $totalAll > 0 ? $totalAll : 1;
						$iPerc = round((($tot_to_date / $totalAll ) * 100),2) ;
						// colors
						if (isset($this->sub_color[$x])) {
            			$colr = $this->sub_color[$x];
         			}
         			else {
            			$colr = array(255.0,255.0,255.0); // default  white  no colours  left
         			}
         			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 

      				$pdf->MultiCell(50, 5, $crew_name, array('LTR'=>$this->borderStyle), 'L', true, 0, 2,$Y);
      				$pdf->MultiCell(25, 5, $water_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 52,$Y);
      				$pdf->MultiCell(25, 5, $water_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 77,$Y);
      				$pdf->MultiCell(25, 5, $grav_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 102,$Y);
      				$pdf->MultiCell(25, 5, $grav_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 127,$Y);
      				$pdf->MultiCell(22, 5, $iPerc, array('TR'=>$this->borderStyle), 'C', true, 2, 152,$Y);
      				$pdf->MultiCell(32, 5, "$".number_format($tot_to_date,2), array('TR'=>$this->borderStyle), 'R', true, 2, 174,$Y);
					} 
					else {
						$x++;
						// colors
						if (isset($this->sub_color[$x])) {
            			$colr = $this->sub_color[$x];
         			}
         			else {
            			$colr = array(255.0,255.0,255.0); // default  white  no colours  left
         			}
         			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
						if ( $tot_to_date > 0 ) {
							$Y +=5;
							$iPerc = number_format(round((($tot_to_date / $totalAll ) * 100),2),2) ;
							$grav_volume = number_format($grav_volume);
							$grav_tot = number_format(round($grav_tot));
							$water_volume = number_format($water_volume);
							$water_tot = number_format(round($water_tot));
      					$pdf->MultiCell(50, 5, $crew_name, array('LTR'=>$this->borderStyle), 'L', true, 0, 2,$Y);
      					$pdf->MultiCell(25, 5, $water_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 52,$Y);
      					$pdf->MultiCell(25, 5, $water_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 77,$Y);
      					$pdf->MultiCell(25, 5, $grav_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 102,$Y);
      					$pdf->MultiCell(25, 5, $grav_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 127,$Y);
      					$pdf->MultiCell(22, 5, $iPerc, array('TR'=>$this->borderStyle), 'C', true, 2, 152,$Y);
      					$pdf->MultiCell(32, 5, "$".number_format($tot_to_date,2), array('TR'=>$this->borderStyle), 'R', true, 2, 174,$Y);
						}
					}
					if ($Y > 265) {
               	$Y += 5;
						$pdf->Line(2,$Y,206,$Y);
						$Y +=3;
      				$pdf->MultiCell(55, 5, "Continued...", 0, 'L', false, 0, 5,$Y);
						$this->newPage($pdf,"Sub Scope BreakDown");
						$Y = 40;
					}

				} // end foreach sub crew
			} // end if no vals
		} // end foreach crew
				$Y += 5;
				$pdf->Line(2,$Y,206,$Y);

	} // end func 
	private function showSplitCont($pdf,$Y) {  // type = "contractor"
		$prevConID = -1;
		foreach($this->splitArr as $con=>$crew) {
			foreach ($crew as $ind=>$val) {
					extract($val);
					$water_tot = !is_null($water_tot) ?  number_format($water_tot,2) : "";
					$grav_tot = !is_null($grav_tot) ?  number_format($grav_tot,2) : "";
					$conID = $val['contractor_id'];
					$crewName = $val['crew_name'];
					$tot = $val['tot_to_date'];
					$volume = "";
					if ($conID != $prevConID ) {
						$x = 0;
                  if($prevConID != -1 ) {
                     $Y += 5;
                     $pdf->Line(2,$Y,206,$Y);
                     $this->newPage($pdf, "Work Scope BreakDown");
                  }
						$this->crewPieChart($pdf,$conID,$crew);
                  $Y = 150;
						$fill = "false";
						$prevConID = $conID;
						if ($Y > 245) {
							$Y +=3;
      					$pdf->MultiCell(55, 5, "Continued...", 0, 'L', false, 0, 5,$Y);
							$this->newPage($pdf,"Work Scope BreakDown");
							$Y = 40;
						}
						$pdf->SetFillColor(255,255,255);
						$conArr =  $this->getConTotAll($conID);
      				$itot =  $conArr['tot_to_date'];

      				$pdf->SetFont(PDF_FONT, '', 10);
      				$pdf->MultiCell(50, 5, "Work Scope", array('LTR'=>$this->borderStyle), 'C', false, 0, 2,$Y);
      				$pdf->MultiCell(25, 5, 'Water Volume', array('TR'=>$this->borderStyle), 'C', false, 2, 52,$Y);
      				$pdf->MultiCell(25, 5, 'Water Cost', array('TR'=>$this->borderStyle), 'C', false, 2, 77,$Y);
      				$pdf->MultiCell(25, 5, 'Gravel Volume', array('TR'=>$this->borderStyle), 'C', false, 2, 102,$Y);
      				$pdf->MultiCell(25, 5, 'Gravel Cost', array('TR'=>$this->borderStyle), 'C', false, 2, 127,$Y);
      				$pdf->MultiCell(22, 5, "Percentage", array('TR'=>$this->borderStyle), 'C', false, 2, 152,$Y);
      				$pdf->MultiCell(32, 5, "Total", array('TR'=>$this->borderStyle), 'C', false, 2, 174,$Y);
						$Y += 5;
						// colors
						if (isset($this->sub_color[$x])) {
            			$colr = $this->sub_color[$x];
         			}
         			else {
            			$colr = array(255.0,255.0,255.0); // default  white  no colours  left
         			}
         			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 

						$iPerc = round((($tot_to_date / $itot ) * 100),2) ;
      				$pdf->MultiCell(50, 5, $crew_name, array('LTR'=>$this->borderStyle), 'L', true, 0, 2,$Y);
      				$pdf->MultiCell(25, 5, $water_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 52,$Y);
      				$pdf->MultiCell(25, 5, $water_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 77,$Y);
      				$pdf->MultiCell(25, 5, $grav_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 102,$Y);
      				$pdf->MultiCell(25, 5, $grav_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 127,$Y);
      				$pdf->MultiCell(22, 5, $iPerc, array('TR'=>$this->borderStyle), 'C', true, 2, 152,$Y);
      				$pdf->MultiCell(32, 5, "$".number_format($tot_to_date,2), array('TR'=>$this->borderStyle), 'R', true, 2, 174,$Y);
					} 
					else {
						$x++;
						// colors
						if (isset($this->sub_color[$x])) {
            			$colr = $this->sub_color[$x];
         			}
         			else {
            			$colr = array(255.0,255.0,255.0); // default  white  no colours  left
         			}
         			$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
							$Y +=5;
							$iPerc = number_format(round((($tot_to_date / $itot ) * 100),2),2) ;
      					$pdf->MultiCell(50, 5, $crew_name, array('LTR'=>$this->borderStyle), 'L', true, 0, 2,$Y);
      					$pdf->MultiCell(25, 5, $water_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 52,$Y);
      					$pdf->MultiCell(25, 5, $water_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 77,$Y);
      					$pdf->MultiCell(25, 5, $grav_volume, array('TR'=>$this->borderStyle), 'R', true, 2, 102,$Y);
      					$pdf->MultiCell(25, 5, $grav_tot, array('TR'=>$this->borderStyle), 'R', true, 2, 127,$Y);
      					$pdf->MultiCell(22, 5, $iPerc, array('TR'=>$this->borderStyle), 'C', true, 2, 152,$Y);
      					$pdf->MultiCell(32, 5, "$".number_format($tot_to_date,2), array('TR'=>$this->borderStyle), 'R', true, 2, 174,$Y);
					}
					if ($Y > 265) {
               	$Y += 5;
						$pdf->Line(2,$Y,206,$Y);
						$Y +=3;
      				$pdf->MultiCell(55, 5, "Continued...", 0, 'L', false, 0, 5,$Y);
						$this->newPage($pdf,"Work Scope BreakDown");
						$Y = 40;
					}
			} // end forach crew
		} // end foreach contractor
				$Y += 5;
				$pdf->Line(2,$Y,206,$Y);
	}  // end  func
	 private function subPieChart($pdf,$crewID,$arr) {
		$pos = intval($this->getColor($crewID));
      $colr = $this->color[$pos];
      $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);
		$totalAll = $this->getTotAll($crewID);
     	$crewPerc = number_format(round((($totalAll / $this->totalAll ) * 100),2),2) ."%" ;
		$crew = $this->crewArr[$crewID]['crew_name'];
     	$pdf->SetFont(PDF_FONT, '', 11);
     	$pdf->MultiCell(90, 8, "$crew" , 0, 'L', true, 0, 2,36,true,0,false,true,8,'M');
     	$pdf->MultiCell(55, 8, "$crewPerc of Total" , 0, 'L', true, 0, 92,36,true,0,false,true,8,'M');
     	$pdf->MultiCell(59, 8, "$".number_format($totalAll,2), 0, 'R', true, 2, 147,36,true,0,false,true,8,'M');
		$pos = intval($this->getColor($crewID));
		$colr = $this->color[$pos];
     	$pdf->SetFillColor($colr[0],$colr[1],$colr[2]);  
     	$pdf->SetFont(PDF_FONT, '', 11);
		$totalCrew = $this->getTotAll($crewID);
		$totArr = array();
		foreach($arr as $sub=>$val) {  // reduce array
			$totArr[] = $val;
		}
      $Y = 97;
      //$pdf->MultiCell(51,0.25,'',array('LRTB'=>$this->borderStyle),'C',false,0,153,$Y);

      $xc = 105;
      $r = 50;
      $cnt = count($totArr)-1 ;
      for ($x = $cnt;$x>=0;$x-- ) {
         $perc = $rotate = $tot = 0;
         if (isset($this->sub_color[$x])) {
            $colr = $this->sub_color[$x];
         }
         else {
            $colr = array(255.0,255.0,255.0); // default  white  no colurs  left
         }
         $itot = $totArr[$x]['tot_to_date'];
         $iPerc = round((($itot / $totalCrew ) * 100),2) ;
         $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
         for ($y = $x;$y>=0;$y--) {
            $tot += $totArr[$y]['tot_to_date'];
         }
         $perc =round((($tot / $totalCrew ) * 100),2) ;
         $rotate  =  (360  *   ($perc / 100 )) ;
         $pdf->PieSector($xc, $Y, $r, 0, $rotate, 'FD', true, 90);
      }
	}
	 private function crewPieChart($pdf,$conID,$arr) {
		$pos = intval($this->getColor($conID));
      $colr = $this->color[$pos];
      $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);
		$conArr =  $this->getConTotAll($conID);
		$totalCon =  $conArr['tot_to_date'];
		$conName =  $conArr['contractor_name'];
		$iPerc = round((($totalCon/$this->totalAll) * 100),2);
     	$pdf->SetFont(PDF_FONT, '', 12);
     	$pdf->MultiCell(110, 8, "$conName", 0, 'L', true, 0, 2,36,true,0,false,true,8,'M');
     	$pdf->MultiCell(115, 8, $iPerc ."% of Total", 0, 'C', true, 2, 52,36,true,0,false,true,8,'M');
     	$pdf->MultiCell(40, 8, "$".number_format($totalCon,2), 0, 'R', true, 2, 166,36,true,0,false,true,8,'M');
		$totArr = array();
		foreach($arr as $sub=>$val) {  // reduce array
			$totArr[] = $val;
		}
      $Y = 97;
      //$pdf->MultiCell(51,0.25,'',array('LRTB'=>$this->borderStyle),'C',false,0,153,$Y);

      $xc = 105;
      $r = 50;
      $cnt = count($totArr)-1 ;
      for ($x = $cnt;$x>=0;$x-- ) {
         $perc = $rotate = $tot = 0;
         if (isset($this->sub_color[$x])) {
            $colr = $this->sub_color[$x];
         }
         else {
            $colr = array(255.0,255.0,255.0); // default  white  no colurs  left
         }
         $itot = $totArr[$x]['tot_to_date'];
         $iPerc = round((($itot / $totalCon ) * 100),2) ;
         $pdf->SetFillColor($colr[0],$colr[1],$colr[2]);    // 
         for ($y = $x;$y>=0;$y--) {
            $tot += $totArr[$y]['tot_to_date'];
         }
         $perc =round((($tot / $totalCon ) * 100),2) ;
         $rotate  =  (360  *   ($perc / 100 )) ;
         $pdf->PieSector($xc, $Y, $r, 0, $rotate, 'FD', true, 90);
      }
	}
	private function clean_splitArr() {
			reset($this->splitArr);
			foreach($this->splitArr as $con=>$crew) {
				foreach($crew as $id =>$vl) {
					if ($vl['tot_to_date'] + $vl['water_tot'] + $vl['grav_tot'] == 0 ) {
						unset($this->splitArr[$con][$id]);
					}
				}
			}
	}

}
?>
