<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class CompOneEmployee {
		public $page;
		public $ID;
		public $firstName;
		public $lastName;
		public $userName;
		public $password;
		public $nokName;
		public $nokFone;
		public $profileID;
		public $address_type;
		public $action;
		public $sessionProfile;
		public $certificateObj;
		public $companyID = 1;
		public $conn;
		public $email;
		public $empType;

		public function	__construct($action,$ID=0,$search="") {
			$this->ID = $ID;
			$this->action = $action;
			$this->companyID = 1;
			$this->conn= $GLOBALS['conn'];
			$this->address_type = "emp";
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('com_employee');
				$this->page= $pg->page;
				$heading_text = "Administration<BR />";
         	switch($action) {
					case "new" :
						$heading_text .= "Add New Third Party Login";
						break;
					case "update" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text .= "Updating Third Party Login $ID";
						$this->getEmployeeDetails($ID);
						break;
					case "delete" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text .= "Delete Third Party Login $ID";
						$this->getEmployeeDetails($ID);
						break;
					case "list" :
						$heading_text .=  strlen($search) > 0 ? "List Logins - $search" : "List Logins - All";
						$this->getAllEmployees($search) ;
						break;
					case "find" : 
						$heading_text .= "Third Party Login $ID";
						$this->getEmployeeDetails($ID);
						break;
					default:
						$heading_text .= "Add New Third Party Login";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->ID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Login";
						break;
					case "update":
						$button = "Update Login";
						break;
					case "delete":
						$button = "Delete Login";
						$DISABLED='disabled';
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Login";
						break;
				}
			$content = <<<FIN
<div style="width:1800px;text-align:center;"><div style="width:1450px;margin:auto;">
<form name="comp_one_emp" id="employee" method="post" action="comp_one_employee.php?SUBMIT=SUBMIT">
<input type="hidden" name="id" value="$this->ID" />
<input type="hidden" name="prev_password" value="$this->password" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="emp_type" value="$this->empType" />
<fieldset style="margin-top:15px;"><legend style="font-size:130%;margin-left:560px;">Third Party Login</legend>
<label for="firstname" class="label wide" >First Name</label><input type="text" name="firstname" id="firstname" class="input  $DISABLED" value="$this->firstName" $DISABLED />
<label for="lastname" class="label wide" >Last Name</label><input type="text" name="lastname" id="lastname" value="$this->lastName" class="input  $DISABLED" $DISABLED />
<div style="clear:both;height:25px;"> </div>
<label for="username" class="label wide" >User Name</label><input type="text" name="username" id="username" class="input  $DISABLED" value="$this->userName" $DISABLED />
<label for="password" class="label wide" >Password</label><input type="text" name="password" id="password" value="" class="input  $DISABLED" $DISABLED />
<label for="email" class="label" >Email</label><input type="text" name="email" id="email" class="input  $DISABLED" $DISABLED value="$this->email" />
<script>
$("#commp_one_employee").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
</script>
FIN;
				$content .= "</fieldset>\n";
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"submitbutton\" />"; // no submit for find employee 
				}


				$content .= "\n</form></div></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllEmployees($search="") {
			$whereClause="";
			if (strlen($search) > 0 ) {
				$whereClause = " and upper(firstname) like '$search%' ";

			}
			$sql = "SELECT e.employee_id,e.firstname || ' ' || coalesce(e.lastname,'') as employee_name,e.username,e.emp_type,
			ad.address_line_1 || ' ' || ad.address_line_2 || ' ' || ad.suburb || ' ' || st.state_name as address,telephone,mobile,email
			from employee e 
			LEFT JOIN address_to_relation ar using (employee_id) 
			LEFT JOIN address ad using (address_id) 
			LEFT JOIN state st using (state_id)
			where e.removed is false  and e.company_id = $this->companyID $whereClause  and employee_id > 2 order by e.lastname LIMIT 100";
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content = "<div style=\"width:1800px;text-align:center;\"><div style=\"width:1366px;margin:auto;\">\n";
			$content .= AdminFunctions::AtoZ("comp_one_employee.php??action=list",$search);
			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" style=\"width:1366px;\">\n<div class=\"cli hd\">Login Name</div>\n<div class=\"usr hd\">Username</div>\n<div class=\"addr hd\">Address</div>\n";
			$content .= "<div class=\"fone hd\">Telephone</div>\n<div class=\"fone hd\">Mobile</div>\n";
			$content .= "<div class=\"email hd\">Email</div>\n<div class=\"links hd\">Actions</div></div>\n";
			$content .= "<div style=\"clear:both;\"></div>";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln = $lineNo % 2 == 0  ? "line1" : "";
					$id = $rs->fields['employee_id'];
				$employee_name = strlen($rs->fields['employee_name']) > 2 ? $rs->fields['employee_name'] : " ";
				$user_name = strlen($rs->fields['username']) > 2 ? $rs->fields['username'] : " ";
				$user_name = $user_name ;
				$address = strlen($rs->fields['address']) > 2 ? $rs->fields['address'] : " ";
				$telephone = strlen($rs->fields['telephone']) > 2 ? $rs->fields['telephone'] : " ";
				$mobile = strlen($rs->fields['mobile']) > 2 ? $rs->fields['mobile'] : " ";
				$email = strlen($rs->fields['email']) > 2 ? $rs->fields['email'] : " ";
            if ($rs->fields['emp_type'] == 'EXT') {
					$email=AdminFunctions::contactEmail($id);
				}
				$content .= "<div class=\"cli $ln\"> <a href=\"employee.php?action=find&id=$id\" title=\"View Login\" class=\"name $ln\" >$employee_name</a></div>";
            $content .= "<div class=\"usr $ln\">$user_name</div><div class=\"addr $ln\">$address</div><div class=\"fone $ln\">$telephone</div><div class=\"fone $ln\">$mobile</div>";
				$content .= "<div class=\"email $ln\">$email</div>";
				if ($this->sessionProfile > 2 ) {
				$content .= "<div class=\"links $ln\"><a href=\"comp_one_employee.php?action=update&id=$id\" title=\"Edit Login\" class=\"linkbutton\" >EDIT</a>";
				$content .= "<a href=\"comp_one_employee.php?action=delete&id=$id\" title=\"Delete Login\" class=\"linkbutton d\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
				}
				else {
					$content .="<div class=\"links $ln\" > &nbsp;</div>";
				}
				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "</div></div><hr style=\"margin-top:0px;\" />";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getEmployeeDetails($ID) {
			$sql = "SELECT e.*,con.contact_email,adr.address_id,a.email from employee e 
			LEFT JOIN address_to_relation adr on adr.employee_id = e.employee_id  
			LEFT JOIN address a using(address_id)
			LEFT JOIN contact con on con.employee_id = e.employee_id
			where e.employee_id = $ID";

			if (! $data = $this->conn->getRow($sql)) {
				echo $sql;
				die($this->conn->ErrorMsg());
			}

			$this->ID = $data['employee_id'];
			$empType = $data['emp_type'];
			if ($empType == "EXT" ) {
				$this->email = $data['contact_email'];
			}
			else {
				$this->email = $data['email'];
			}
			$this->empType = $data['emp_type'];
			$this->firstName = $data['firstname'];
			$this->lastName = $data['lastname'];
			$this->userName =   $data['username'];
			$this->password = $data['password'];
			$this->addressID = intval($data['address_id']);
			$this->profileID = intval($data['profile_id']);

		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteEmployee();
			}
			$this->ID = $_POST['id'];
			$this->firstName = ucfirst(trim($_POST['firstname']));
			$this->lastName = ucfirst(trim(addslashes($_POST['lastname'])));
			$this->userName = trim($_POST['username']);
			$this->userName = strlen($this->userName) > 0 ? $this->userName : uniqid(true);
		 	$this->password =  strlen(trim($_POST['password'])) > 1  ? md5(trim($_POST['password'])) : $_POST['prev_password']; // use previous if not given
		 	$this->profileID =  intval($_POST['profile']);
		 	$this->empType =  ($_POST['emp_type']);
		 	$this->email = trim(addslashes($_POST['email']));


			
			$this->submitEmployee();
		}
		private function deleteEmployee() {
			$sql = "UPDATE contact set removed = TRUE where employee_id = $this->ID";
			if (! $this->conn->Execute($sql)) {
				die( $this->conn->ErrorMsg());
			}
			$sql = "UPDATE employee set removed = TRUE where employee_id = $this->ID";
			if (! $this->conn->Execute($sql)) {
				die( $this->conn->ErrorMsg());
			}
			header("LOCATION: comp_one_employee.php?action=list");
			exit;
		}
		private function submitEmployee() {
			if ($this->ID == 0 ) {
				$sql = "INSERT into employee values (nextval('employee_employee_id_seq'),'$this->userName','$this->password','$this->firstName',E'$this->lastName',NULL,FALSE,
				3,E'$this->nokName','$this->nokFone',$this->companyID) returning employee_id";
				try {
					if (! $rs =  $this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
					$this->ID = $rs->fields['employee_id'];
			}
			else { // Update
					$sql = "UPDATE employee set username = '$this->userName',password = '$this->password',firstname= '$this->firstName',lastname = E'$this->lastName',
					profile_id=3,next_of_kin_name = E'$this->nokName',next_of_kin_fone = '$this->nokFone'  where employee_id = $this->ID";
				try {
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
				if ($this->empType == "CON" ) {
					$sql = "update address set email = '$this->email'  where address_id = ( select address_id from address_to_relation where employee_id = $this->ID)";
				}
				else { // EXT
					$sql = "update contact set contact_email = '$this->email',contact_firstname = '$this->firstName',contact_lastname= '$this->lastName'  where  employee_id = $this->ID";
				}
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
	
			}
			header("LOCATION: comp_one_employee.php?action=list");
		}
	}
?>
