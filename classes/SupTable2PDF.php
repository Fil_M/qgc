<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class SupTable2PDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $startDate;
	public $endDate;
	public $cooID;
	public $contractorName;
	public $contractorID;
	public $shortName;
	public $qgcSignature;
	public $empID;

	public function __construct($action="",$startDate,$endDate,$conID,$cooID=NULL) {
		global $BASE;
		$this->conn = $GLOBALS['conn'];
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$this->contractorID = $conID ;
		$this->cooID = $cooID ;
		$this->empID = $_SESSION['employee_id'];
		$connArr = Functions::getConName($conID);
      $this->contractorName = $connArr['con_name'];
      $this->shortName = $connArr['name'];
		$this->getDetails();


		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDFLAND("L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Table 2 Summary Approved Claims $this->startDate - $this->endDate");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$prevAreaID = $prevwellName = $prevCOOID =  $prevcrewID =  $prevApproverID = -1;
		$details = "";
		//T2
		$Y = 0;
		$details = "";
		$subTotal = $gst = $gTotal = 0;
		$table = "Table 2";
		$printHeading = true;

		foreach ($this->data as $ind =>$val ) {
			$cooID = $val['calloff_order_id'];
			$areaID = $val['area_id'];
			$wellName = $val['well_name'];
			$hDate = Functions::dbdate($val['hour_date']);
			$id = $val['hour_id'];
			$dktHours = $val['docket_hours_t2'];	
			$total = $val['total_t2'];	
			if (( $cooID != $prevCOOID)  && $prevCOOID != -1 && floatval($total) > 0 ) {
					$approvalDate = Functions::dbDate($val['qgc_approval_date']);
               $this->doTotals($pdf,$subTotal,$Y,$details,$prevwellName,$prevAreaID,$hDate,$id,$prevCOOID,$prevApproverID,$approvalDate);
					$printHeading = true;
			}
				$prevwellName = $wellName;
            $prevAreaID = $areaID;
            $prevCOOID = $cooID;
				$prevApproverID = intval($val['qgc_approver_id']);
				$wellName = $val['well_name'];
				$areaName = $val['area_name'];
				$crewName = $val['crew_name'];
			if ($printHeading) {
				$pdf->AddPage();
            $this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$table,$cooID,$crewName);
				$subTotal = $gst = $gTotal = 0;
            $details = "";
            $Y = 39.5;
				$printHeading = false;
			}
//			$pdf->MultiCell(21,5.25,'Date',array('LTB'=>$this->borderStyle),'C',true,0,5,$Y);
//      $pdf->MultiCell(40,5.25,'Operator',array('LTB'=>$this->borderStyle),'C',true,0,26,$Y);
//      $pdf->MultiCell(40,5.25,'Sub Scope',array('LTB'=>$this->borderStyle),'C',true,0,66,$Y);
//      $pdf->MultiCell(20,5.25,'Tag',array('LTB'=>$this->borderStyle),'C',true,0,106,$Y);
//      $pdf->MultiCell(65,5.25,'Machine/Equip/Material',array('LTB'=>$this->borderStyle),'C',true,0,126);
//      $pdf->MultiCell(30,5.25,'Reason',array('LTB'=>$this->borderStyle),'C',true,0,191);
//      $pdf->MultiCell(44,5.25,'Details',array('LTB'=>$this->borderStyle),'C',true,0,221);
//      $pdf->MultiCell(14,5.25,'Hours',array('LTB'=>$this->borderStyle),'C',true,0,265);
//      $pdf->MultiCell(14,5.25,'Rate',array('LTRB'=>$this->borderStyle),'C',true,0,279);


        	$plant_name = substr($val['p_type'],0,35);
        	$tag = substr($val['plant_unit'],0,7);
			$opname = $val['opname'];
			$sub_crew = $val['sub_crew_name'];
			$rate = $val['rate'];	
			$details = $val['details'];
			$height = ceil(strlen($details) /35) * 5.5;
         $height = $height >= 5.5 ? $height : 5.5;
			$subTotal += $total;
			$total = "$" .number_format($total,2);	
			$sreason = $val['reason'];
			$pdf->setCellHeightRatio(1.2);
      	$pdf->SetFont(PDF_FONT, '', 10);
			$pdf->MultiCell(21,$height,$hDate,array('LR'=>$this->borderStyle),'L',false,0,5,$Y);
			$pdf->MultiCell(40,$height,$opname,array('R'=>$this->borderStyle),'L',false,0,26,$Y);
			$pdf->MultiCell(40,$height,$sub_crew,array('R'=>$this->borderStyle),'L',false,0,66,$Y);
			$pdf->MultiCell(20,$height,$tag,array('R'=>$this->borderStyle),'L',false,0,106,$Y);
			$pdf->MultiCell(65,$height,$plant_name,array('R'=>$this->borderStyle),'L',false,0,126);
			$pdf->MultiCell(30,$height,$sreason,array('R'=>$this->borderStyle),'L',false,0,191);
			$pdf->MultiCell(44,$height,$details,array('R'=>$this->borderStyle),'L',false,0,221);
			$pdf->MultiCell(14,$height,$dktHours,array('R'=>$this->borderStyle),'C',false,0,265);
			$pdf->MultiCell(14,$height,$rate,array('R'=>$this->borderStyle),'C',false,1,279);
			$Y += $height;
			if ($Y > 245 ) {
					$pdf->Line(7,$Y,207,$Y);
      			$pdf->SetFont(PDF_FONT, 'I', 12);
					$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
					$pdf->AddPage();
					$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$table,$cooID,$crewName);
					$Y = 40;
			}
			else {
				continue;
			}
		}
		$pdf->Line(5,$Y,133,$Y);
      $details = preg_replace('/,,/',"",$details);
      $details = substr($details,0,277);
		$approverID = $val['qgc_approver_id'];
		$approvalDate = Functions::dbDate($val['qgc_approval_date']);
      $this->doTotals($pdf,$subTotal,$Y,$details,$wellName,$areaID,$hDate,$id,$cooID,$approverID,$approvalDate);


	//	if ($printTotals) {
	//		$details = substr($details,0,277);
	//		$details = preg_replace('/\, \, /',"",$details);
	//		$this->doT2Totals($pdf,$subTotal,$Y,$details);  // Last page
	//		$Y += 30;
	//	}
		$dat = preg_replace('/-/',"",Functions::dbDate($this->startDate));
      $name = "{$dat}_{$this->contractorID}_Table2Summary.pdf";
      if ($action == "print" ) {
         $pdf->Output($name, 'I');
      }
		else {
			$name = "tmp/$name";
         $pdf->Output($name, 'F');
			$pdfs = array($name);
			$eNum = Functions::insertEmailLog($this->empID,0,$this->contractorID,"Approval Summary Of Table 2 Hours for $this->startDate",'TB2',0,$this->startDate);
			$sql = "INSERT into email_attachment (email_log_id,attachment_name,recipient_type,email,attach_date )    
         values ($eNum,'$name','receiver','all',current_date)";
      	if (! $res = $this->conn->Execute($sql)) {
         	echo $sql;
         	die($this->conn->ErrorMsg());
      	}
			  exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");



			
			header("LOCATION: index.php");
			
		}

		return true;
	}
	private function doT2Totals($pdf,$subTotal,$Y,$details) {
		$pdf->Line(5,$Y,207,$Y);
      $pdf->SetFont(PDF_FONT, '', 12);
      $pdf->MultiCell(155,0,'Details',array('LTRB'=>$this->borderStyle),'C',true,0,5,$Y);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(155,23,$details,array('LRB'=>$this->borderStyle),'L',false,0,5,$Y+6);
      //$Y +=2;
      $pdf->MultiCell(22,8,'Sub Total',array('L'=>$this->borderStyle),'R',false,0,160,$Y,true,0,false,true,8,'B');
      $gst = $subTotal * 0.1;
      $total = $subTotal + $gst;
      $subTotal = "$" .number_format($subTotal,2);
      $gst = "$" . number_format($gst,2);
      $total = "$" . number_format($total,2);
      $pdf->MultiCell(27,8,$subTotal,array('LR'=>$this->borderStyle),'R',false,1,183,$Y,true,0,false,true,8,'B');
      $Y += 8;
      $pdf->MultiCell(22,10,'G.S.T.',array('L'=>$this->borderStyle),'R',false,0,160,$Y,true,0,false,true,10,'B');
      $pdf->MultiCell(27,10,$gst,array('LR'=>$this->borderStyle),'R',false,1,183,$Y,true,0,false,true,10,'B');
      $Y += 10;
      $pdf->MultiCell(22,0,'Total',array('L'=>$this->borderStyle),'R',false,0,160,$Y,true,0,false,true,10,'B');
      $pdf->MultiCell(27,10,$total,array('LR'=>$this->borderStyle),'R',false,1,183,$Y,true,0,false,true,10,'B');
      $pdf->Line(155,$Y,207,$Y);
      $Y+=10;
      $pdf->SetFillColor(0,0,0);
      $pdf->MultiCell(50,1,'',array('LRTB'=>$this->borderStyle),'R',true,0,155,$Y,true,0,false,true,1,'B');
      $pdf->SetFillColor(241,121,61);
	}
	private function doTotals($pdf,$subTotal,$Y,$details,$wellName,$areaID,$hDate,$id,$cooID,$approverID,$approvalDate) {
		//$sql = "SELECT a.area_name from  area where a.area_id = $areaID ";
		//$row = $this->conn->getRow($sql);
		//if (count($row) < 2 ) {
		//	if ($this->conn->ErrorNo() != 0 ) {
		//		die($this->conn->ErrorMsg());
		//	}
		//	else {
		//		echo "no name   ||| $hDate   ||| $areaID  ||| $wellID  |||$id <br />";
		//	}
		//}
		$pdf->Line(5,$Y,293,$Y);
     	$pdf->SetFont(PDF_FONT, '', 10);
      //$Y +=2;
		$pdf->MultiCell(22,8,'Sub Total',array('L'=>$this->borderStyle),'R',false,0,237,$Y,true,0,false,true,8,'B');
		$gst = $subTotal * 0.1;
		$total = $subTotal + $gst;
		$subTotal = "$" .number_format($subTotal,2);	
		$gst = "$" . number_format($gst,2);	
		$total = "$" . number_format($total,2);	
		$pdf->MultiCell(28,8,$subTotal,array('LR'=>$this->borderStyle),'R',false,1,265,$Y,true,0,false,true,8,'B');
		$Y += 8;
		$pdf->MultiCell(22,10,'G.S.T',array('L'=>$this->borderStyle),'R',false,0,237,$Y,true,0,false,true,10,'B');
		$pdf->MultiCell(28,10,$gst,array('LR'=>$this->borderStyle),'R',false,1,265,$Y,true,0,false,true,10,'B');
		$Y += 10;
		$pdf->MultiCell(22,0,'Total',array('L'=>$this->borderStyle),'R',false,0,237,$Y,true,0,false,true,10,'B');
		$pdf->MultiCell(28,10,$total,array('LR'=>$this->borderStyle),'R',false,1,265,$Y,true,0,false,true,10,'B');
		$pdf->Line(237,$Y,293,$Y);
		$Y+=10;
		$pdf->SetFillColor(0,0,0);
		$pdf->MultiCell(56,1,'',array('LRTB'=>$this->borderStyle),'R',true,0,237,$Y,true,0,false,true,1,'B');
		$pdf->SetFillColor(241,121,61);
		$Y += 3;
		if (intval($approverID) > 0 ) {
			$image_file = Functions::getSig($approverID); 
			//$image_file = $data['signature'];
			$empName = Functions::getEmpName($approverID);
			if (strlen($image_file) > 5 ) {
     			$pdf->SetFont(PDF_FONT, 'B', 12);
				$pdf->MultiCell(150, 0, 'I hereby approve the Stand Down claim above', 0, 'L', false, 0, 5,$Y);
        		$pdf->Image($image_file, 5, $Y,0 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
				$Y += 35;
     			$pdf->SetFont(PDF_FONT, '', 11);
      		$pdf->SetTextColor(0,0,0);
      		$pdf->MultiCell(50, 0, 'Approved for QGC by:', 0, 'L', false, 0, 5,$Y);
      		$pdf->SetTextColor(0,0,255);
      		$pdf->MultiCell(50, 0, $empName, 0, 'L', false,0, 47);
      		$pdf->SetTextColor(0,0,0);
				$pdf->MultiCell(35, 0, 'Approved On:', 0, 'L', false, 0, 105,$Y);
      		$pdf->SetTextColor(0,0,255);
      		$pdf->MultiCell(25, 0, $approvalDate, 0, 'L', false, 0, 135 );
      	}
		}
	
	}
	private function pieChart($pdf,$areaID,$wellID,$areaName,$wellName,$table,$cooID,$crewName) {
		$this->getEsts($cooID);
		$this->getTotToDate($areaID,$wellID,$cooID);
		$Y = $pdf->getY();
		if ($Y > 175 ) {
			$pdf->Line(7,$Y,207,$Y);
      	$pdf->SetFont(PDF_FONT, 'I', 12);
			$pdf->MultiCell(37,7,"continued ...",0,'L',false,0,5,$Y+4);
			$pdf->AddPage();
			$this->heading($pdf,"$this->startDate  to  $this->endDate",$areaName,$wellName,$table,$cooID,$crewName);
			$Y = 39;
		}
		$Y+= 1;
		//$pdf->MultiCell(51,0.25,'',array('LRTB'=>$this->borderStyle),'C',false,0,153,$Y);
		$pdf->Line(155,$Y,293,$Y);
     	$pdf->SetFont(PDF_FONT, '', 10);
		$pdf->MultiCell(75,9,"Percentage of Total Costs to Date against Original Estimate",array('LRTB'=>$this->borderStyle),'C',true,0,5,$Y);
		$pdf->MultiCell(75,9,"Estimate of Work completed ",array('LRTB'=>$this->borderStyle),'C',true,0,80,$Y);
		$pdf->MultiCell(50,9,"Job Total  to  date",array('TRB'=>$this->borderStyle),'C',true,0,155,$Y);
		$Y+= 11;
		$xc = 35;
		$r = 20;
		$actualEst = $this->estimate/$this->numWells;
		$perc =round((($this->totToDate / $actualEst) * 100),2) ;
      $pdf->SetFont(PDF_FONT, 'B', 12);
		$pdf->MultiCell(80,0,"{$perc}% Percent of Estimate",0,'L',false,0,5,$Y);
		$pdf->MultiCell(75,0,$this->percComplete ."% Complete",0,'C',false,0,80,$Y);
		$tott = '$' .number_format($this->totToDate,2);
		$pdf->MultiCell(40,5,$tott,array('B'=>$this->borderStyle),'R',false,1,165,$Y);
		//$pdf->Text(5, $Y, "{$perc}% Percent of Estimate" );
		if ($this->numWells > 1 ) {
			$Y+= 4;
     		$pdf->SetFont(PDF_FONT, 'I', 10);
			$pdf->Text(5, $Y, "$this->numWells Wells. Estimate per Well: ".number_format($actualEst,2) );
		}
		$Y+=25;
		// Costs against Estimate
			$rotate  =  (360  *   ($perc / 100 )) - 20 ;
		if ($perc <= 50 ) {
			$pdf->SetFillColor(255, 0, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 0, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 0, 2);
		}
		else if ($perc < 100 ){
			$pdf->SetFillColor(255, 0, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 180, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 180, 2);
		}
		else {  // 100%  done
			$pdf->SetFillColor(255, 0, 0);    // 
			$pdf->Circle($xc, $Y, $r,0,360,'F',null,array(255,0,0));
		}

		// Estimate Complete
		$xc = 120;
			$rotate  =  (360  *   ($this->percComplete / 100 )) - 20 ;
		if ($this->percComplete <= 50 ) {
			$pdf->SetFillColor(0, 255, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 0, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 0, 2);
		}
		else if ($this->percComplete < 100 ){
			$pdf->SetFillColor(0, 255, 0);    // 
			$pdf->PieSector($xc, $Y, $r, 340, $rotate, 'FD', false, 180, 2);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->PieSector($xc, $Y, $r, $rotate, 340, 'FD', false, 180, 2); 
		}
		else {  // 100%  done
			$pdf->SetFillColor(0, 255, 0);    // 
			$pdf->Circle($xc, $Y, $r,0,360,'F',null,array(255,0,0));
		}

		$pdf->Line(7,$Y,207,$Y);  

	}
	private function getDetails(){
		$whereClause = !is_null($this->cooID) ? "WHERE h.calloff_order_id = $this->cooID " : "WHERE 1 = 1 ";
		$sql = "Select a.area_name,wells_from_ids(h.well_ids) as well_name,pt.p_type,p.plant_unit,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,sc.sub_crew_name,
      h.hour_id,h.hour_date,docket_hours_t2,h.rate,total_t2,h.well_id,h.area_id,h.crew_id,h.details,h,calloff_order_id,h.qgc_approver_id,qgc_approval_date,c.crew_name,h.price_type,sr.reason
		FROM {$this->shortName}_hour h
      LEFT JOIN {$this->shortName}_plant p  on h.plant_id = p.plant_id
      LEFT JOIN plant_type pt using(plant_type_id) 
      LEFT JOIN {$this->shortName}_employee e using(employee_id) 
      LEFT JOIN area a using (area_id)
      LEFT JOIN well w on w.well_id = h.well_id
      LEFT JOIN crew c on c.crew_id = h.crew_id
      LEFT JOIN sub_crew sc on sc.sub_crew_id = h.sub_crew_id
      LEFT JOIN standdown_reason sr using(standdown_reason_id)
      $whereClause and h.hour_date = '$this->startDate'  and total_t2 > 0  and h.removed is false 
      order by h.calloff_order_id,a,area_name,well_name,h.hour_date ";


      //$whereClause and h.hour_date between '$this->startDate'  and '$this->endDate' and total_t2 > 0  and h.removed is false 
			//all stes

		if (! $this->data = $this->conn->getAll($sql)) {
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
		}

		//var_dump($this->data);
		if (count($this->data) < 1 ) {
			//die("<h1> NO hour records found for this date - $this->startDate - $this->endDate");
			return false;
		}

		return true;

	}
	  private function getSig($approverID) {
			if (intval($approverID) <= 0 ){
				return false;
			} 
         $sql = "SELECT signature,firstname||' ' || lastname as emp_name from employee where employee_id = $approverID";
         if (! $res = $this->conn->getRow($sql)) {
            echo $sql;
            die( $this->conn->ErrorMsg());
         }
			return $res;
      }

	private function getEsts($cooID) {
		$sql = "SELECT coalesce(cost_estimate,1) as estimate,well_ids  from calloff_order where calloff_order_id = $cooID";
		if (! $data = $this->conn->getRow($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
				else {
					die($sql);
				}
		}
		$this->estimate = $data['estimate'];
		$arr = explode("|",$data['well_ids']);
		$this->numWells  = count($arr);
		$sql = "select max(percentage_complete) as percentage_complete from docket_day where docket_date <= '$this->endDate' and calloff_order_id = $cooID and percentage_complete is not null";
		if (! $data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
      }
      $this->percComplete = intval($data['percentage_complete']);

	}
	private function getTotToDate($areaID,$wellID,$cooID) {
		$sql = "SELECT sum(coalesce(total_t1,0)) + sum(coalesce(total_t2,0)) + sum(coalesce(expense,0)) as tot_to_date 
		from hour where area_id = $areaID and well_id = $wellID  and calloff_order_id = $cooID and hour_date <= '$this->endDate'";
		if (! $this->totToDate = $this->conn->getOne($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
				else {
					//die($sql);
					$this->totToDate = 0;
				}
		}
		//var_dump($this->totToDate);
	}
	 private function heading($pdf,$pDate,$area,$well,$table,$cooID,$crew) {
      // Heading Title 
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, '', 14);
      $pdf->MultiCell(80, 0, 'Table 2 Summary', 0, 'C', false, 2, 100,1);
      //$pdf->SetTextColor(255,0,0);
      $pdf->SetFont(PDF_FONT, '', 10);
      $pdf->MultiCell(25, 0, 'From:', 0, 'L', false, 0, 36,10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $pDate, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'C.O.O:', 0, 'L', false, 0, 158,10);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(50, 0, $cooID, 0, 'L', false,0, 178);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(15, 0, 'Area:', 0, 'L', false, 0, 36,15);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $area, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(25, 0, 'Well/s:', 0, 'L', false, 0, 158,15);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(60, 0, $well, 0, 'L', false, 2, 178);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(35, 0, 'Work Scope:', 0, 'L', false, 0, 36,20);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(80, 0, $crew, 0, 'L', false, 0, 58);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(35, 0, 'Contractor:', 0, 'L', false, 0, 158);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(50, 0, $this->contractorName, 0, 'L', false,0, 178);
		$this->headingTitle(29,$pdf,$table);
	}
	private function headingTitle($Y,$pdf,$table) {
	   $pdf->SetFillColor(241,121,61);
      $pdf->SetTextColor(0,0,0);

		$pdf->SetFont(PDF_FONT, '', 11);
		$pdf->MultiCell(21,0,"$table",array('LTB'=>$this->borderStyle),'L',true,1,5,$Y);
		$Y += 5;
      $pdf->SetFont(PDF_FONT, '', 10);
		$pdf->MultiCell(21,5.25,'Date',array('LTB'=>$this->borderStyle),'C',true,0,5,$Y);
		$pdf->MultiCell(40,5.25,'Operator',array('LTB'=>$this->borderStyle),'C',true,0,26,$Y);
		$pdf->MultiCell(40,5.25,'Sub Scope',array('LTB'=>$this->borderStyle),'C',true,0,66,$Y);
		$pdf->MultiCell(20,5.25,'Tag',array('LTB'=>$this->borderStyle),'C',true,0,106,$Y);
		$pdf->MultiCell(65,5.25,'Machine/Equip/Material',array('LTB'=>$this->borderStyle),'C',true,0,126);
		$pdf->MultiCell(30,5.25,'Reason',array('LTB'=>$this->borderStyle),'C',true,0,191);
		$pdf->MultiCell(44,5.25,'Details',array('LTB'=>$this->borderStyle),'C',true,0,221);
		$pdf->MultiCell(14,5.25,'Hours',array('LTB'=>$this->borderStyle),'C',true,0,265);
		$pdf->MultiCell(14,5.25,'Rate',array('LTRB'=>$this->borderStyle),'C',true,0,279);

   }

}
?>
