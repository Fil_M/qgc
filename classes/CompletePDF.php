<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
class CompletePDF {
	public $conn;
	public $data;
   public $borderStyle = array('width'=>'0.2','dash'=>'0','color'=>array(0,0,0,));
   public $lineStyle = array('width'=>'0.3','dash'=>'1,1','color'=>array(0,0,0,));
	public $conID;
   public $compID;
	public $areaName;
	public $wellName;
	public $crewName;
	public $contractorName;
	public $shortName;
	public $origEst;
	public $total;
	public $callOffID;
	public $complDate;
	public $approvalDate;
   public $contractNum;
   public $comments;
   public $table1; 
   public $table2; 
   public $totExp; 
   public $conSignName; 
   public $conSignature;
   public $qgcSignName;
   public $empID;
	public $emailLogID;
	public $waterTot;
	public $gravelTot;
	public $percentCompl;
	public $partial;
	public $conDomain;
	public $fileName;


	public function __construct($action="",$conID,$compID,$filename=NULL) {
		$this->conn = $GLOBALS['conn'];
		$this->conID = $conID;
		$this->compID = $compID;
		$this->action = $action;
		$this->fileName = $filename;


		$this->getCompDetails($this->compID);
		//var_dump($this->row);
      //exit;
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->setPrintFooter(false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('QGC');
		$pdf->SetTitle("Completion Certificate $this->contractorName $this->areaName - $this->wellName");

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);


		// Main LOgic
		$pdf->AddPage();
		$this->heading($pdf);
		$Y = 40;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(60, 0, 'Well Pad Location/s:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(95, 0, "$this->areaName / $this->wellName", array('B'=>$this->lineStyle), 'L', false, 0, 45,$Y);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->MultiCell(25, 0, 'Date:', 0, 'L', false, 0, 145);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(22, 0, $this->complDate, array('B'=>$this->lineStyle), 'L', false,1, 175);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(55, 0, 'Contract Num:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(65, 0, "$this->contractNum", array('B'=>$this->lineStyle), 'L', false, 0, 45,$Y);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->MultiCell(35, 0, 'Call Off Order:', 0, 'L', false, 0, 145);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(22, 0, $this->callOffID, array('B'=>$this->lineStyle), 'C', false,1, 175);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(30, 0, 'Contractor:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(65, 0, $this->contractorName, array('B'=>$this->lineStyle), 'L', false,1, 45);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(55, 0, 'Comments:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(160, 0, "$this->comments", 0, 'L', false, 0, 45,$Y);
		$Y += 30;
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 11);
		$pdf->MultiCell(30, 0, 'Table 1:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->table1", 0, 'R', false, 0, 65,$Y);
		// Gravel
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 11);
		$pdf->MultiCell(30, 0, 'Gravel:', 0, 'L', false, 0, 115,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->gravelTot", 0, 'R', false, 2, 155,$Y);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(30, 0, 'Table 2:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->table2", 0, 'R', false, 0, 65,$Y);
		// Water 
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 11);
		$pdf->MultiCell(30, 0, 'Water:', 0, 'L', false, 0, 115,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->waterTot", 0, 'R', false, 2, 155,$Y);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(30, 0, 'Expenses:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->totExp", 0, 'R', false, 0, 65,$Y);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(60, 0, 'Final Call Off Order Value is:', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->total", array('B'=>$this->borderStyle), 'R', false, 0, 65,$Y);
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(40, 0, 'Original Estimate:', 0, 'L', false, 0, 115,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->origEst", array('B'=>$this->borderStyle), 'R', false, 0, 155,$Y);
		 if (! is_null($this->partial)) {
         $Y += 10;
         $pdf->SetFont(PDF_FONT, 'I', 11);
         $pdf->SetTextColor(0,0,0);
         $pdf->MultiCell(80, 0, 'Percentage Complete:', 0, 'L', false, 0, 5,$Y);
         $pdf->SetTextColor(0,0,255);
         $pdf->SetFont(PDF_FONT, '', 11);
         $pdf->SetTextColor(0,0,255);
         $pdf->MultiCell(15, 0, "$this->percentCompl%", array('B'=>$this->borderStyle), 'R', false, 0, 90,$Y);

      }
		$Y += 20;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
		$pdf->MultiCell(140, 0, $this->partial . ' Completion for the above Well Pad/s was reached on the', 0, 'L', false, 0, 5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->SetTextColor(0,0,255);
      $pdf->MultiCell(40, 0, "$this->complDate", 0, 'R', false, 0, 100,$Y);
		$Y += 10;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(120,0,"Signed:",0,'L',false,0,5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(92,0,$this->conSignName,array('B'=>$this->lineStyle),'L',false,0,45);
      $pdf->SetTextColor(0,0,0);
      $Y+=8;
      $image_file = $this->conSignature;
		if (!file_exists($image_file)) {
         $image_file="images/nosig.png";
      }
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,70 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

      $Y+=30;
      $pdf->SetFont(PDF_FONT, 'B', 11);
      $pdf->MultiCell(120,0,"Company Representative or Delegate:",0,'L',false,0,40,$Y);
-
      $Y+=30;
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->SetTextColor(0,0,0);
      $pdf->MultiCell(50,0,"Approved for QGC:",0,'L',false,0,5,$Y);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(92,0,$this->qgcSignName,array('B'=>$this->lineStyle),'L',false,0,45);
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'I', 11);
      $pdf->MultiCell(25, 0, 'Date:', 0, 'L', false, 0, 145);
      $pdf->SetTextColor(0,0,255);
      $pdf->SetFont(PDF_FONT, '', 11);
      $pdf->MultiCell(22, 0, $this->approvalDate, array('B'=>$this->lineStyle), 'L', false,1, 175);
		$Y +=3;

      $image_file = $this->qgcSignature;
		//var_dump($this->qgcSignature);
		if (!file_exists($image_file)) {
         $image_file="images/nosig.png";
      }
		if (strlen($image_file) > 5 ) {
      	$pdf->Image($image_file, 40, $Y,0 ,36 , 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		}

      $name = !is_null($this->fileName) ? $this->fileName : "tmp/Completion_Certificate_{$this->conID}_{$this->compID}.pdf";
      if ($action == "print" ) {
         $pdf->Output($name, 'I');
      }
      else {  
         $pdf->Output($name, 'F');
      }

   }		
	 private function getCompDetails($compID) {
			$connArr = Functions::getConName($this->conID);
      	$this->contractorName = $connArr['con_name'];
      	$this->shortName = $connArr['name'];
			$this->conDomain = $connArr['domain'] . "/";

         $sql = "SELECT a.area_name,c.crew_name,wells_from_ids(cc.well_ids) as well_name,partial_complete,cc.* from {$this->shortName}_completion_cert cc 
         LEFT JOIN area a using(area_id) 
         LEFT JOIN crew c using(crew_id) 
         LEFT JOIN contractor con on con.contractor_id = $this->conID 
         where cc.completion_cert_id = $compID";
         if (! $data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }
         $this->compID = $data['completion_cert_id'];
         $this->callOffID = $data['calloff_order_id'];
         $this->wellName = $data['well_name'];
         $this->areaName = $data['area_name'];
         $this->crewName = $data['crew_name'];
         $this->total = $data['total'];
         $this->contractNum = $data['contract_num'];
         $this->compDate = Functions::dbDate($data['completion_date']);
         $this->comments = $data['comments'];
         $this->origEst = "$" .number_format($data['cost_estimate'],2);
         $this->table1 = "$" .number_format($data['total_t1'],2);
         $this->table2 = "$" .number_format($data['total_t2'],2);
         $this->totExp = "$" .number_format($data['expense'],2);
         $this->total = "$" .number_format($data['total'],2);
         $this->conSignName =  $data['contractor_signee'];
         $this->conSignature = $this->conDomain .$data['contractor_sig'];
         $this->complDate =  Functions::dbDate($data['completion_date']);
         $this->qgcSignName =  $data['qgc_signee'];
         $this->qgcSignature =  $data['qgc_sig'];
         $this->approvalDate =  Functions::dbDate($data['qgc_approve_date']);
			$this->gravelTot = "$" .number_format($data['total_gravel'],2);
			$this->waterTot = "$" .number_format($data['total_water'],2);
			$this->percentCompl = $data['percentage_complete'] = $data['percentage_complete'];
			if (intval($this->percentCompl) != 100 ) {
				$this->partial = $data['partial_complete'] == "t" ? "Partial" : NULL;
			}


      }

		 private function heading($pdf){
      // Heading Title 
		$pString = ! is_null($this->partial) ? "($this->partial)" : "";
      $pdf->SetTextColor(0,0,0);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, 'QGC Well Engineering Construction', 0, 'C', false, 2, 50,3);
      $pdf->SetFont(PDF_FONT, '', 12);
      $pdf->SetTextColor(255,0,0);
      $pdf->MultiCell(100, 0, $this->crewName, 0, 'C', false, 0, 50,10);
      $pdf->SetFont(PDF_FONT, 'B', 14);
      $pdf->MultiCell(100, 0, "COMPLETION CERTIFICATE $this->compID $pString", 0, 'C', false, 0, 50,17);
   }



}
?>
