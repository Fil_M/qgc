--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: plant; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--
DROP TABLE plant;

CREATE TABLE plant (
    plant_id integer NOT NULL,
    plant_name character varying(32),
    plant_rate double precision,
    plant_unit character varying(16),
    removed boolean DEFAULT false,
    new_rate double precision,
    effective_from date,
    plant_type_id integer,
    stand_rate double precision,
    stand_new_rate double precision
);


ALTER TABLE public.plant OWNER TO postgres;

--
-- Name: plant_plant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plant_plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plant_plant_id_seq OWNER TO postgres;

--
-- Name: plant_plant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE plant_plant_id_seq OWNED BY plant.plant_id;


--
-- Name: plant_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY plant ALTER COLUMN plant_id SET DEFAULT nextval('plant_plant_id_seq'::regclass);


--
-- Data for Name: plant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY plant (plant_id, plant_name, plant_rate, plant_unit, removed, new_rate, effective_from, plant_type_id, stand_rate, stand_new_rate) FROM stdin;
109	Excavator - 21t - EC210CL	150	616	t	\N	\N	\N	\N	\N
103	Dozer - D8R	240	627	t	\N	\N	\N	\N	\N
115	Excavator - 30t - EC290BLC Volvo	150	639	f	190	2013-07-01	7	114	\N
119	Grinder - Morbark 4600XL	1000	642	f	\N	\N	1	600	\N
126	Roller - Smooth Drum 20t	150	S007	f	\N	\N	12	90	\N
147	Excavator - 30t - DX300LC (CLK)	150	S018	f	190	2013-07-01	7	114	\N
110	Grinder - Morbark 4600XL	1000	619	f	\N	\N	1	600	\N
121	Dozer - D6R	180	S002	f	\N	\N	3	108	\N
122	Dozer - D6T	180	S003	f	\N	\N	3	108	\N
114	Dozer - Caterpillar D7R	180	631	f	210	2013-07-01	3	126	\N
113	Dozer - D8R	180	627	f	220	2013-07-01	5	132	\N
144	Dozer - D85EX - Komatsu	180	629	f	210	2013-07-01	4	126	\N
145	Dozer - D155AXC-6 - Komatsu	240	651	f	220	2013-07-01	5	132	\N
100	Excavator - 21t  - EC210CL Volvo	150	616	f	\N	\N	6	90	\N
111	Excavator - 24t - EC240CL Volvo	150	625	f	\N	\N	6	90	\N
112	Excavator - 24t - EC240CL Volvo	150	626	f	\N	\N	6	90	\N
116	Excavator - 21t - SH210-5 Sumi	150	645	f	\N	\N	6	90	\N
124	Excavator - 22.5t - DX225LC Doos	150	S005	f	\N	\N	6	90	\N
125	Excavator - 24t - Nethercote	150	S006	f	\N	\N	6	90	\N
128	Excavator - 24t - ZX2403 Hitachi	150	S009	f	\N	\N	6	90	\N
143	Excavator - 21t - EC210BLC Volvo	150	609	f	\N	\N	6	90	\N
123	Excavator - 24t	150	S004	f	\N	\N	6	90	\N
133	Excavator - 29t - EC290CL Volvo	150	647	f	190	2013-07-01	7	114	\N
131	Excavator - 30t  Doosan (OZLEIS)	150	S012	f	190	2013-07-01	7	114	\N
138	Excavator - 30t - DX300LC (BZE)	150	S013	f	190	2013-07-01	7	114	\N
101	Side Tippers - 796KEU	180	31	f	\N	\N	8	108	\N
102	Side Tipper - 445IBH	180	48	f	\N	\N	8	108	\N
135	Side Tipper - TCH	180	S014	f	\N	\N	8	108	\N
136	Side Tipper - Goodland	180	S015	f	\N	\N	8	108	\N
139	Loader - Case - BZE	180	S016	f	\N	\N	9	108	\N
148	Loader - L90B - Volvo	180	450	f	\N	\N	9	108	\N
106	Loader - Volvo L220	180	438	f	\N	\N	9	108	\N
104	Loader - Volvo L120E	180	410	f	\N	\N	9	108	\N
105	Loader - Volvo L120E	180	430	f	\N	\N	9	108	\N
108	Loader - Doosan DL300	180	446	f	\N	\N	9	108	\N
107	Loader - Volvo L220G	180	445	f	\N	\N	9	108	\N
155	Loader Volvo 220G	180	441	f	\N	\N	9	108	\N
129	Grader - 140H	180	S010	f	\N	\N	10	108	\N
146	Grader - 12H	180	S017	f	\N	\N	10	108	\N
118	Grader - Volvo G940	180	628	f	205	2013-07-01	10	108	\N
99	Watercart - Bogie 754RVM	120	30	f	\N	\N	11	72	\N
127	Roller - Padfoot 20t	150	S008	f	\N	\N	12	90	\N
141	Water Tanker 44,000lt - 503SOP	191.669999999999987	45	f	230	2013-06-27	13	138	\N
142	Water Tanker 44,000lt - 504SOP	191.669999999999987	44	f	230	2013-06-27	13	138	\N
140	Pump Truck - 685STI	125	47	f	150	2013-06-27	15	90	\N
153	Accommodation External	225	ACCOM	f	\N	\N	14	0	\N
151	Compactor 825 with laser	300	S019	f	\N	\N	16	180	\N
154	Ancillaries	400	ANCIL	f	\N	\N	17	0	\N
98	Watercart - Tri axle 048GQX	180	26	f	\N	\N	18	108	\N
134	Grinder - CBI 6800BT	1000	650	f	\N	\N	1	600	\N
130	Watercart - Bogie Matilda	120	S011	f	\N	\N	11	72	\N
117	Light Vehicle - 353MAQ	100	927	t	\N	\N	\N	\N	\N
150	Light Vehicle - 469TBB	100	940	t	\N	\N	\N	\N	\N
152	Light Vehicle - 156ROR	100	918	t	\N	\N	\N	\N	\N
120	Compactor - 815F with Laser	240	S001	f	300	2013-06-27	16	180	\N
137	Float - Prime Mover - 954RYT	180	35	f	200	2013-07-01	19	120	\N
149	Float - Prime Mover - 340HZK	180	4	f	200	2013-07-01	19	120	\N
132	Supervisor with ute	100	SUPER	f	\N	\N	20	0	\N
156	Watercart - Tri axle 311 LFI	180	16	f	\N	\N	18	108	\N
157	Bobcat - Caterpillar 259B	130	655	f	\N	\N	21	78	\N
158	Earthco Excavator	150	S020	f	\N	\N	6	105	\N
\.


--
-- Name: plant_plant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('plant_plant_id_seq', 158, true);


--
-- Name: plant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plant
    ADD CONSTRAINT plant_pkey PRIMARY KEY (plant_id);


--
-- PostgreSQL database dump complete
--

