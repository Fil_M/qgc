<?php  // Index.php
 	require_once("include/boot.php");
	error_reporting(E_ALL);
   ini_set('display_errors','1');

 	Functions::verify();
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	$startDate= !empty($_REQUEST['start'])  ? $_REQUEST['start'] : NULL;
   $endDate= !empty($_REQUEST['end'])  ? $_REQUEST['end'] : NULL;
   $conID= !empty($_REQUEST['con_id'])   ? $_REQUEST['con_id'] : 0;
   $type= !empty($_REQUEST['type'])   ? $_REQUEST['type'] : "Gravel";
   $areaID= !empty($_REQUEST['area_id'])  ? intval($_REQUEST['area_id']) : 0;
   $landowner= !empty($_REQUEST['landowner'])  ? $_REQUEST['landowner'] : -1;
   $showDate=  !empty($_REQUEST['show_date'])  ? $_REQUEST['show_date'] : "show";

	$pg = new GravelWaterPDF($action,$startDate,$endDate,$type,$conID,$areaID,$landowner,$showDate);
?>
