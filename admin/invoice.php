<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	$cooID = isset($_REQUEST['coo_id']) ? $_REQUEST['coo_id'] : NULL;
	$conID = isset($_REQUEST['contractor']) ? $_REQUEST['contractor'] : NULL;
	
	if (isset($_REQUEST['datetype'])) {
		$dateType = $_REQUEST['datetype'];
		if ($dateType == "equalto" ) {
			$startDate= isset($_REQUEST['sdate']) ? $_REQUEST['sdate'] : NULL; 
			$endDate = $startDate;
		}
		elseif ($dateType == "less" ) {
			$startDate = '1970-01-01';
			$endDate = $_REQUEST['sdate'];
		}
		elseif ($dateType == "equalgreater" ) {
			$endDate = '2970-01-01';
			$startDate = $_REQUEST['sdate'];
		}
   	else {
			$startDate= isset($_REQUEST['sdate']) ? $_REQUEST['sdate'] : NULL; 
			$endDate= isset($_REQUEST['enddate']) ? $_REQUEST['enddate'] : NULL; 
		}
	 }
	 else {
		$startDate = $endDate = NULL;
	 }

	$pg = new Invoice($action,$startDate,$endDate,$cooID,$conID);
?>
