<?php  // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class CallOff  {
		public $page;
		public $requestID;
		public $cooID;
		public $empID;
		public $contractorID = 0;
		public $conDomain;
		public $dblink;
		public $fieldEstID;
		public $budgetID;
		public $costCOO;
		public $conNum;
		public $cName;
		public $action;
		public $sessionProfile;
		public $conn;
		public $wellID;
		public $wellName;
		public $areaID;
		public $crewID;
		public $subCrewID;
		public $subCrewIDs;
		public $subCrewStr;
		public $areaName;
		public $otherText;
		public $creDate;
		public $commDate;
		public $completionDate;
		public $questions;
		public $services;
		public $remarks;
		public $equalto;
      public $between;
		public $less;
      public $hideEndDate=true;
      public $equalgreater;
      public $endDate;
		public $wellStr;
		public $landOwner_1;
      public $landOwner_2;
      public $loc_1;
      public $loc_2;
      public $returnGravel;
      public $returnWater;
		public $supIDs;
		public $URL;
		public $signature;
		public $qgcApproved;
		public $qgcSigName;
		public $sigDate;
		public $wellIDs;
		public $notifyChecked;
		public $fault;
		 public $check1 = array("1"=>"Construct lease pad and associated access road","2"=>"Construct lease pad with sump and associated access road","3"=>"Gravel pit","4"=>"Equipment supply",
            "5"=>"Shear, clear and mulch","6"=>"Fauna spotter","8"=>"Other");
      public $check2 = array("0"=>"Fluid Haulage","1"=>"Fluid Haulage","2"=>"Rehabilitation of lease pad and associated access road","3"=>"Rehabilitation of lease pad (with sump) and associated access road","4"=>"Equipment supply", "8"=>"Other");



		public function	__construct($action,$cooID=0,$requestID=0,$search=NULL,$redo=NULL) {
			$this->URL="https://".$_SERVER['HTTP_HOST'];
			$this->requestID = $requestID;
			if ($cooID > 0 ) {
				$this->cooID = $cooID;
			}
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			$this->creDate =  date('d-m-Y');
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('calloff');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						if (! is_null($requestID)  ) {
							$this->getEstimateDetails($requestID);  // prefill from Cost Estimate
						}
						$this->qgcSigName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
						$heading_text = "Add New Call Off Order";
						$this->sigDate = date('d-m-Y');
                  $this->signature = AdminFunctions::getSig($this->empID);

						break;
					case "update" :
						if ($this->cooID < 1 ) {
							return false;
						}
						$heading_text = "Updating Call Off Order $this->cooID";
						$this->getCooDetails($this->cooID);
						if ($this->fault > 0 ) {
							header("LOCATION : index.php");
							exit;
						}
						break;
					case "updatesupgroup" :
						if ($this->cooID < 1 ) {
							return false;
						}
						$heading_text = "Updating COO $this->cooID";
						$this->getCooDetails($this->cooID);
						break;
					case "delete" :
						if ($cooID < 1 ) {
							return false;
						}
						$heading_text = "Delete Call Off Order $this->cooID";
						$this->getCooDetails($cooID);
						break;
					case "list" :
						$heading_text =   "List Call Off Orders";
						$this->getAllCoos($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Call Off Order $this->cooID";
						$this->getCooDetails($this->cooID);
						break;
					default:
						$heading_text = "Add New Call Off Order";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->requestID > 0 )) {
				$DISABLED="";
				$UPDISABLED = "disabled";
				$locText_1 = AdminFunctions::locSelect($this->loc_1,1,$this->landOwner_1,$DISABLED,"location_1",false,true);
				$locText_2 = AdminFunctions::locSelect($this->loc_2,2,$this->landOwner_2,$DISABLED,"location_2",false,true);

				switch ($action) {
					 case "new":
                  $button = "Add C.O.O";
                  $UPDISABLED = "";
                  break;
               case "update":
                  $button = "Update C.O.O";
                  $DISABLED= $UPDISABLED = "" ;
                  break;
               case "updatesupgroup":
                  $button = "Update C.O.O";
                  $DISABLED='disabled';
                  $UPDISABLED = "";
                  break;
               case "delete":
                  $button = "Delete C.O.O";
                  break;

					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add C.O.O";
						break;
				}
			$content = <<<FIN
<div style="width:1300px;margin:auto;">
<form name="coo" id="coo" method="post" action="calloff.php?SUBMIT=SUBMIT">
<input type="hidden" name="requestID" value="$this->requestID" />
<input type="hidden" name="cooID" value="$this->cooID" />
<input type="hidden" name="fieldEstID" value="$this->fieldEstID" />
<input type="hidden" name="cost_estimate" value="$this->costCOO" />
<input type="hidden" name="contractorID" value="$this->contractorID" />
<input type="hidden" name="areaID" value="$this->areaID" />
<input type="hidden" name="wellID" value="$this->wellID" />
<input type="hidden" name="empID" value="$this->empID" />
<input type="hidden" name="cooID" value="$this->cooID" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="signature" value="$this->signature" id="signature" />
<fieldset ><legend style="margin-left:45%;">CALL OFF ORDER</legend>
FIN;
            $content .= "<div class=\"div19 multi\" >\n";
				$content .= "<div class=\"div95\" ><label for=\"crew\"  >Work Scope</label>\n";
            $content .= AdminFunctions::crewSelect($this->crewID,$DISABLED);
            $content .= "</div>";
				$content .= "<div class=\"div95\" >";
            $content .= AdminFunctions::budgetSelect($this->budgetID,$UPDISABLED);
            $content .= "</div>";
				$content .= "</div>\n";
				$content .= "<div class=\"div21 marg multi\" ><div class=\"div95\"  id=\"subscopediv\">\n";
            $content .= AdminFunctions::subscopeSelectMulti($this->crewID,$this->subCrewIDs,$DISABLED);


$content .= <<<FIN
</div></div>
<div class="div15 marg" ><label for="area" >Area<input type="text" name="area" id="area" class="required" value="$this->areaName" $DISABLED /></label></div>
FIN;
      if ($this->action == "find" ) {
			$content .= "<div class=\"div28 marg\"  ><div id=\"welldiv\" class=\"div95\" >\n";
         $content .= "<label>Well/s<input  type=\"text\" $DISABLED value=\"$this->wellStr\" /></label>\n";
      }
      else if ($this->action = "update" ) {
			$content .= "<div class=\"div18 marg multi\"  ><div id=\"welldiv\" class=\"div95\" >\n";
         $content .= AdminFunctions::wellSelectMulti($this->areaID,$this->wellIDs);
      }
$content .= <<<FIN
</div></div>
<div style="clear:both;" > </div>
<div class="div19" ><label for="con_num" >Contract Number<input type="text"  name="con_num" id="con_num" value="$this->conNum" class="required"  $DISABLED readonly /></label></div>
<div class="div15 marg" ><label  >Original Request Number<input type="text"  id="req_id" value="$this->requestID"   DISABLED /></label></div>
<div class="div12 marg11" ><label  >Commencement Date<input type="text"  name="commence" id="commence" value="$this->commDate" class="date required"  $UPDISABLED /></label></div>
<div class="div12 marg8" ><label  >Est Completion Date<input type="text"  name="complete" id="complete" value="$this->completionDate" class="date"  $UPDISABLED /></label></div>
<div class="div11 marg55" ><label  >Contractor Estimate<input type="text"  name="cost_estimate" id="cost_est" value="$this->costCOO"  class="txtr"  $DISABLED /></label></div>
</fieldset>
<div style="clear:both;" ></div>
<fieldset ><legend style="margin-left:37.25%;">DESCRIPTION OF SERVICES TO BE PROVIDED</legend>
<div class="div49" ><label for="services"  >Services<textarea  name="services"  id="services"  $DISABLED class="txt"  >$this->services</textarea></label></div>
<div class="div49 marg1" ><label for="remarks" >Remarks<textarea   name="remarks" id="remarks"  $DISABLED class="txt"  >$this->remarks</textarea></label></div>
<div style="clear:both;" > </div>
<div class="div50" >
<div id="landdiv" >
<div class="div60" >
FIN;
   $content .= AdminFunctions::ownerSelect($this->landOwner_1,1,$UPDISABLED,"landowner_1",true,true);
   $content .= "</div></div></div>\n";
   $content .= "<div class=\"div50\" ><div id=\"locdiv_1\" class=\"div69\"  >$locText_1 </div>\n";
   $content .= "<div class=\"div23 marg\"><label >Return Distance<input type=\"text\" name=\"returngravel\" id=\"returngravel\" class=\"num\"  value=\"$this->returnGravel\" $UPDISABLED  /></label></div> \n";
$content .= <<<FIN
</div>
<div style="clear:both;height:5px;" ></div>
<div class="div50" >
<div id="waterdiv" >
<div class="div60" >
FIN;
   $content .= AdminFunctions::ownerSelect($this->landOwner_2,2,$UPDISABLED,"landowner_2",true,true);
   $content .= "</div></div></div>\n";
   $content .= "<div class=\"div50\" ><div id=\"locdiv_2\" class=\"div69\" >$locText_2 </div>\n";
   $content .= "<div class=\"div23 marg\"><label >Return Distance<input type=\"text\" name=\"returnwater\" id=\"returngravel\" class=\"num\"  value=\"$this->returnWater\" $UPDISABLED  /></label></div> \n";
$content .= <<<FIN
</div>
<div style="clear:both;" > </div>
</fieldset>
<fieldset ><legend style="margin-left:36.75%;margin-bottom:5px;">PREPARED AND ISSUED ON BEHALF OF QGC BY</legend>
<div class="div50" ><label for="supervisor" >Supervisor/s</label>
<div class="div60" >
FIN;
$content .= AdminFunctions::supervisorSelect($this->supIDs,$UPDISABLED,true);
$content .= <<<FIN
</div></div>
   <div class="div24" ><img src="$this->signature"  alt="QGC to Sign"  height="180" width="300" /></div>
   <div class="div25 marg1" >
   <div class="div35 marg2" ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"  class="cntr"  value="$this->sigDate" readonly /></label></div>
   <div class="div93 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->qgcSigName"  readonly /></label></div>
	<div class="div93 marg2" ><label >Notify Contractor</label><input type="checkbox" name="notify" id="notify"  value="notified"  $this->notifyChecked /></div>

   </div>
</fieldset>
<script>
$("#coo").submit(function(){
    var isFormValid = true;
    $("#coo input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#coo .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
	 if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
    else {
      $('#SUBMIT').prop("disabled", "disabled");
      return true;
    }
});
$("#contractor").change(function(){
	var conid=$(this).val();
   $('#contractorID').val(conid);
   $.post("get_contract.php",'conid=' + conid,
         function(data) {
				$('#con_num').val(data);
         }
         ,"text");
});
$(document).on('change', '#sub_scope', function(){
   var crewid=$('#crew').val();
   var subcrewid=$(this).val();
   var conid=$('#contractorID').val();
   var areaid=$('#areaID').val();
   var wellids=$('#well').val();
    $.post("unique_coo.php",'conid=' + conid + '&crewid=' + crewid + '&sub_crewid=' + subcrewid + '&areaid=' + areaid + '&wells=' + wellids,
         function(data) {
            if (data[0] != 0) {
               alert(data[1]);
            }
            if (data[0] == 2 ) {
               $("#crew").val($("#crew option:first").val());
            }
         }
         ,"json");

});
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','multi'); });

jQuery('.input.abn').keyup(function () { this.value = this.value.replace(/[^0-9\.]/g,''); });
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});

$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_m',$('#welldiv'),'#contractorID'); return false; }
      });
});
// use this for dynamic DOM  changes
// Gravel = 1
 $(document).on('change', '#landowner_1', function(){
   var ltypeid = 1;
   var ownerid = $(this).val();
   locSelect(ltypeid,ownerid,'#locdiv_1');
   });

// Water = 2
 $(document).on('change', '#landowner_2', function(){
   var ltypeid = 2;
   var ownerid = $(this).val();
   locSelect(ltypeid,ownerid,'#locdiv_2');
   });
</script>
FIN;
				$url = isset($_SESSION['last_request']) ? $_SESSION['last_request'] : "calloff.php?action=list&redo=redo";
            if ($this->action != "find" ) {
               $content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\"  />"; // no submit for found cost
            }

				$content .= "\n<a href=\"$url\"   class=\"button\" >Last Screen</a>";
				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllCoos($search,$redo) {
			$_SESSION['last_request'] = "{$this->URL}/admin/calloff.php?action=list&redo=redo";
			$this->cooID = NULL;
			$this->equalgreater = "SELECTED=\"selected\"";
         $whereClause="";
			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! empty($search)) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if (!empty($search->requestID)) {
               $this->requestID = $search->requestID;
               $whereClause .= " and request_estimate_id = $this->requestID ";
            }
            else if ( !empty($search->cooID)) {
               $this->cooID = $search->cooID;
               $whereClause .= " and calloff_order_id = $this->cooID ";
            }
				 else {
               if (!empty($search->creDate)) {
                     $dateSelected = true;
                     $whereClause .= " and creation_date ";
                     $this->commDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
         					$this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->endDate = $search->endDate;
                           $this->hideEndDate=false;
         						$this->equalgreater = NULL;
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause .= " and commencement_date  >=  current_date ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                  }
               }
					if (!empty($search->areaID)  ) {
                  $whereClause .= " and co.area_id = $search->areaID ";
                  $this->areaName = $search->areaName;
                  $this->areaID = $search->areaID;
               }
					if (!empty($search->crewID)  ) {
                  $whereClause .= " and co.crew_id = $search->crewID ";
                  $this->crewID = $search->crewID;
               }
					if (!empty($search->subCrewID)  ) {
                  $whereClause .= " and $search->subCrewID in (select (unnest(string_to_array(co.sub_crew_ids::text,'|')::int[])))";
                  $this->subCrewID = $search->subCrewID;
               }
					if (!empty($search->contractorID)  ) {
                  $whereClause .= " and co.contractor_id = $search->contractorID ";
                  $this->contractorID = $search->contractorID;
               }
               if (!empty($search->wellID) ) {
						$whereClause .= " and $search->wellID in (select (unnest(string_to_array(co.well_ids::text,'|')::int[])))";
                  $this->wellID = $search->wellID;
               }
			   }
         }
         else {
             // Default query still needs search object serialized
            $this->commDate = date ('d-m-Y');
            $whereClause .= " and commencement_date >= current_date";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            $search->dateType = "equalgreater";
            $search->creDate = $this->commDate;
         }

         $_SESSION['SQL'] = pg_escape_string(serialize($search));
			 $sql = "SELECT co.*,a.area_name,subscopes_from_ids(co.sub_crew_ids) as sub_scopes,wells_from_ids(co.well_ids) as well_name,cr.crew_name,c.con_name,el.fault,gwl.location as gravel_loc,gwg.location as water_loc
         from calloff_order co
         LEFT JOIN area a using (area_id)
         LEFT JOIN crew cr using (crew_id)
         LEFT JOIN contractor c on c.contractor_id = co.contractor_id
         LEFT JOIN gravel_water_loc gwl on gravel_loc_id_1 = gwl.gravel_water_loc_id
         LEFT JOIN gravel_water_loc gwg on water_loc_id_2 = gwg.gravel_water_loc_id
         LEFT JOIN email_log el on el.email_log_id =
         ( select max(email_log_id) from email_log  ell where co.calloff_order_id  = ell.dkt_id  and receiver_type = 'COO' )
         where co.removed is false
         $whereClause
         order by calloff_order_id";

			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}

			$content ="<div style=\"width:1770px;margin:auto;\" >\n";
			$content  .= <<<FIN
 			<form name="calloff" method="post" action="calloff.php?action=list">
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
			<input type="hidden" name="contractorID" id="contractorID" value="0" />
			<input type="hidden" name="empID" id="empID" value="$this->empID" />
			<fieldset ><legend style="margin-left:41%;" >Filter Search - Call Off Orders</legend>
<div class="div9" >
FIN;
         $content .= AdminFunctions::contractorSelect($this->contractorID,"");
         $content  .= <<<FIN
</div>
<div class="div5 marg1" ><label>C.O.O.<input type="text" name="coo_id" id="coo_id" value="$this->cooID" disabled /></label></div>
FIN;
         $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->commDate,$this->endDate,$this->hideEndDate);
         $content  .= <<<FIN
<div class="div10 marg1" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div9 marg1" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= "</div></div>\n";
$content .= "<div class=\"div10 marg1\"  ><label  >Work Scope</label>";
$content .= AdminFunctions::crewSelect($this->crewID,"",true);
$content .= "</div>\n";
$content .= "<div class=\"div11 marg1\" id=\"subscopediv\" >";
$content .= AdminFunctions::subScopeSelect($this->crewID,$this->subCrewID,"");
$content .= "</div>\n";

$content .= <<<FIN
<script> 
$( document ).ready(function() {
	$(function () {
       var ac = $('#coo_id').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=calloffs',
			params: {con_id:  function() { return $('#contractor').val()} },
         minChars:2
      });
		$("#contractor").change(function() {
			$('#coo_id').val('');
			if ($(this).val() != 0 ) {
				$('#coo_id').removeAttr('disabled');
			}
			else {
				 $('#coo_id').prop('disabled','disabled');
			}
			ac.clearCache();
  		});
	});  
});
$.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),'#contractorID'); return false; }
      });
});
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','single'); });
</script>
FIN;
/*$(function () {
      $('#coo_id').autocomplete({
         width: 400,
        // serviceUrl:'autocomplete.php?type=calloffs',
        // params: {con_id: $('#contractor').val()},
        // minChars:2,
        // onSelect: function(value, data){ echo data[1]; return false; }
     // });
 // });  */
			$content .= "<div style=\"float:right;width:15%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button class=\"marg\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"rate hd\">C.O.O.</div>\n";
			$content .= "<div class=\"vehicle hd\">Contractor</div>\n";
			$content .= "<div class=\"vehicle hd\">Area</div>\n";
			$content .= "<div class=\"cli hd\">Well/s</div>\n";
			$content .= "<div class=\"crew hd\">Work Scope</div>\n";
			$content .= "<div class=\"times hd\">Comm Date</div>\n";
			$content .= "<div class=\"fone hd padr\">Estimate</div>\n";
			$content .= "<div class=\"cli hd\">Services</div>\n";
			$content .= "<div class=\"cli hd\">Remarks</div>\n";
			$content .= "<div class=\"cli hd\">Gravel/Water</div>\n";
			$content .= "<div class=\"links hd\" style=\"width:167px;\"   >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$emailButTxt = "EMAIL";
            $emailButClass = "";
            $ln= $lineNo % 2 == 0  ? "line1" : "";
            $coo_id = $rs->fields['calloff_order_id'];
            $req_id = $rs->fields['request_estimate_id'];
            $contractor = $rs->fields['con_name'];
            $conID = $rs->fields['contractor_id'];
            $supIDs = $rs->fields['email_emp_ids'];
            $area = $rs->fields['area_name'];
            $well = $rs->fields['well_name'];
            $crew = $rs->fields['crew_name'];
				$pad = "SUB SCOPES: ";
				$sub_crew =  isset($rs->fields['sub_scopes']) ? $rs->fields['sub_scopes'] : "" ;
            $toolTip = !empty($sub_crew) ?  "<span class=\"show-option\" title=\"${pad}${sub_crew}\" >$crew</span>" : $crew;
            $est = number_format($rs->fields['cost_estimate']);
            $commence = AdminFunctions::dbDate($rs->fields['commencement_date']);
            $hoursBooked = AdminFunctions::cooHours($coo_id,$conID);
            $services = AdminFunctions::cutString(ucwords(strtolower($rs->fields['services'])),80);
            $remarks = AdminFunctions::cutString(ucwords(strtolower($rs->fields['remarks'])),80);
            $supervisor = $rs->fields['qgc_signee'];
            $locs="";
            if (strlen($rs->fields['gravel_loc']) > 0 || strlen($rs->fields['water_loc']) > 0 )  {
               $locs = "<span style=\"font-weight:bold;\" >Gravel</span> - ".$rs->fields['gravel_loc'] . "<br /><span style=\"font-weight:bold;\" > Water</span> - ".$rs->fields['water_loc'];
            }
            $fault = ! is_null($rs->fields['fault']) ? preg_match('/OK/',$rs->fields['fault']) : -1;    // If emailed  no normal  edit
            if ($fault > 0 ) {
               $emailButTxt = "EMAILED";
               $emailButClass = "emailed";
            }
            //o$content .= "<div style=\"float:left;\" >\n";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
            $content .= "<div class=\"rate highh $ln\">$coo_id</div>";
            $content .= "<div class=\"vehicle highh $ln\">$contractor</div>";
            $content .= "<div class=\"vehicle highh $ln\">$area</div>\n";
            $content .= "<div class=\"cli highh $ln\">$well</div>\n";
            $content .= "<div class=\"crew highh $ln cntr\" >$toolTip</div>\n";
            $content .= "<div class=\"times highh $ln cntr\">$commence</div>\n";
            $content .= "<div class=\"fone highh $ln txtr\">$est</div>\n";
            $content .= "<div class=\"cli highh $ln\">$services</div>\n";
            $content .= "<div class=\"cli highh $ln\">$remarks</div>\n";
            $content .= "<div class=\"cli highh $ln\">$locs</div>\n";
            //$content .= "</div>\n";

            if ($this->sessionProfile > 2 ) {
                  $content .= "<div class=\"links highh $ln\" style=\"width:167px;\" >\n";
               if ($fault <= 0 ) {
                  $content .= "<a href=\"calloff.php?action=update&calloff_id=$coo_id\" title=\"Edit Coo\" class=\"linkbutton\" >EDIT</a>";
                  $content .= "<a href=\"calloff.php?action=delete&calloff_id=$coo_id\" title=\"Delete Coo\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >Delete</a>";
               }
               else {
                  $content .= "<a href=\"calloff.php?action=updatesupgroup&calloff_id=$coo_id\" title=\"Edit Coo\" class=\"linkbutton purp\" >EDIT</a>";
                  if (! $hoursBooked) {
                     $content .= "<a href=\"calloff.php?action=delete&calloff_id=$coo_id\" title=\"Delete Coo\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >Delete</a>";
                  }
               }
            	if (strlen($supIDs) > 0 ) {
               	$content .= "<button  class=\"linkbutton $emailButClass\" onclick=\"emailcoo($req_id,$coo_id,$conID,$supIDs,$(this));$(this).prop('disabled','disabled');return false;\">$emailButTxt</button>";
            	}
            }
            else {
               $content .= "<div class=\"links highh $ln\"  style=\"width:167px;\" > &nbsp;";
            }
				$content .= "</div>\n";


				$content .= "</div><div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getCooDetails($cooID) {
			$sql = "SELECT co.*,a.area_name,wells_from_ids(co.well_ids) as well_name,c.name,el.fault from calloff_order co
         LEFT JOIN area a using (area_id)
         LEFT JOIN contractor c on c.contractor_id = co.contractor_id
         LEFT JOIN email_log el on el.email_log_id =
         ( select max(email_log_id) from email_log  ell where co.calloff_order_id  = ell.dkt_id  and receiver_type = 'COO' )
         where calloff_order_id = $cooID";

         if (! $data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }
         //var_dump($data);
         $this->cooID = $cooID;
         $this->budgetID = $data['budget_id'];
         $this->contractorID = $data['contractor_id'];
         $this->cName = $data['name'];
         $this->areaID = $data['area_id'];
         $this->crewID = $data['crew_id'];
         $this->areaName = $data['area_name'];
         $this->costCOO= number_format($data['cost_estimate']);
         $this->fieldEstID = $data['field_estimate_id'];
         $this->services = $data['services'];
         $this->remarks = $data['remarks'];
         $this->commDate = AdminFunctions::dbDate($data['commencement_date']);
         $this->completionDate = AdminFunctions::dbDate($data['est_compl_date']);
         $this->creDate = AdminFunctions::dbDate($data['creation_date']);
         $this->creDate= AdminFunctions::dbDate($data['creation_date']);
         $this->conNum = strlen($data['contract_num']) > 0 ? $data['contract_num'] : AdminFunctions::conNumFromContractor($this->contractorID);
         $this->requestID = intval($data['request_estimate_id']) > 0 ? $data['request_estimate_id'] : NULL;
         $this->wellStr = $data['well_name'];
         $this->wellIDs = $data['well_ids'];
         $this->supIDs = $data['email_emp_ids'];
         //$this->fault = ! is_null($data['fault']) ? preg_match('/OK/',$data['fault']) : -1;  // JOIN on email_log
			$this->notifyChecked = $data['fault'] == "OK" ? "checked" : "";
	
         $this->landOwner_1 = intval($data['landowner_id_1']);
         $this->landOwner_2 = intval($data['landowner_id_2']);
         $this->loc_1 = intval($data['gravel_loc_id_1']);
         $this->loc_2 = intval($data['water_loc_id_2']);
         $this->returnGravel = $data['gravel_return'];
         $this->returnWater = $data['water_return'];
			$this->subCrewIDs = $data['sub_crew_ids'];
			$this->signature = $data['qgc_sig'];
			$this->sigDate = $data['qgc_approve_date'];
			$this->qgcSigName = $data['qgc_signee'];

		}
		private function getEstimateDetails($requestID) {
			$sql = "SELECT r.*,a.area_name, wells_from_ids(r.well_ids) as well_name,c.con_name,c.contract_no from request_estimate r 
			LEFT JOIN area a using (area_id) 
			LEFT JOIN contractor c on c.contractor_id =  r.contractor_id  
			where request_estimate_id = $requestID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}

			$this->contractorID = $data['contractor_id'];
			$this->cName = $data['con_name'];
			$this->areaID = $data['area_id'];
			$this->crewID = $data['crew_id'];
			$this->subCrewIDs = $data['sub_crew_ids'];
			$this->areaName = $data['area_name'];
			$this->wellName = $data['well_name'];
			$this->fieldEstID = $data['field_estimate_id'];
			$this->wellIDs = $data['well_ids'];
			$this->costCOO= number_format($data['contractor_estimate']);
			$this->conNum = $data['contract_no'];
			$this->landOwner_1 = $data['landowner_id_1'];
         $this->landOwner_2 = $data['landowner_id_2'];
         $this->loc_1 = $data['gravel_loc_id_1'];
         $this->loc_2 = $data['water_loc_id_2'];
			$this->returnGravel = intval($data['gravel_return']) > 0 ? $data['gravel_return'] : NULL;
			$this->returnWater = intval($data['water_return']) > 0 ? $data['water_return'] : NULL;
			$this->supIDs = $data['email_emp_ids'];
			$this->commDate = AdminFunctions::dbDate($data['est_start_date']);
			$this->completionDate = AdminFunctions::dbDate($data['est_compl_date']);


			//TODO  this
			$scope_works =  $data['scope_works'] ;
			$this->otherText = $data['other_text'];
			$this->tceTypeID = $data['type_id'];
			$this->check = $data['work_check'];
			$arr = explode("|",$this->check);
			foreach($arr as $key=>$val) {
				if ($val != 8 ) {
					switch($this->tceTypeID) {
						case 1:
							$this->services .=  ! empty($this->check1[$val]) ? $this->check1[$val] . ", " : "";
							break;
						case 2:
							$this->services .=  ! empty($this->check2[$val]) ? $this->check2[$val] . ", " : "";
							break;
					}
				}
				else {
							$this->services .=  ! empty($this->otherText) ? $this->otherText . ", " : "";
				}

			}
			$this->services .=  ! empty($scope_works) ? $scope_works . ", " : "";
			$this->services .=  ! empty($this->otherText) ? $this->otherText : "";
			$this->services = preg_replace('/,\ $/',"",$this->services);
			$this->remarks = $data['site_conditions'];
			$this->commenceDate = AdminFunctions::dbDate($data['est_start_date']);
			$this->supGroupID = $data['supervisor_group_id'];
			$arr = AdminFunctions::getDbLink($this->contractorID);
         $this->dblink=$arr['db_link'];
			$sql = "SELECT sum(estimate_total) from field_estimate where request_estimate_id = $requestID";
         $lnkSQL = "SELECT *
            FROM dblink('$this->dblink','$sql') 
            AS T3(estimate_total double precision)";
			 if (! $this->costCoo = $this->conn->getOne($lnkSQL)) {
            if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
         }
			

		//$this->costCOO= $data['estimate_total'];
		}
		private function processPost() {
			$this->cooID = intval($_POST['cooID']);
			$this->budgetID = isset($_POST['budget']) ? intval($_POST['budget']) : 0;
			$this->requestID = intval($_POST['requestID']) > 0 ? $_POST['requestID'] : $_POST['req_id'];
			$this->requestID = ! is_null($this->requestID) && intval($this->requestID) > 0 ? $this->requestID  : "NULL";
			$this->contractorID = intval($_POST['contractorID']);
         if ($this->action == "delete" ) {
				$this->deleteCoo();
			}
			//$this->supGroupID = intval($_POST['supervisor_group']);
			$this->landOwner_1 = !empty($_REQUEST['landowner_1'])  ? intval($_REQUEST['landowner_1']) : "NULL";
         $this->landOwner_2 = !empty($_REQUEST['landowner_2'])  ? intval($_REQUEST['landowner_2']) : "NULL";
         $this->loc_1 = !empty($_REQUEST['location_1'])  ? intval($_REQUEST['location_1']) :  "NULL";
         $this->loc_2 = !empty($_REQUEST['location_2'])  ? intval($_REQUEST['location_2']) :  "NULL";
			$this->returnGravel = intval($_POST['returngravel']);
         $this->returnWater = intval($_POST['returnwater']);
			$this->commDate = !empty($_POST['commence']) ? "'" . $_POST['commence'] . "'" : "NULL";
			$this->completionDate = !empty($_POST['complete']) ? "'" . $_POST['complete'] . "'" : "NULL";
		 	$this->supIDs = isset($_POST['supervisor']) ? implode("|",$_POST['supervisor']) : "";


			if ($this->action == "updatesupgroup" ) {  // Only  one  field  enabled  and  updateable
				$this->submitForm();
			}
		 	$this->conNum = $_POST['con_num'];
			$this->fieldEstID = intval($_POST['fieldEstID']);
			// dblink field estimate
			$well = $_POST['well'];
			$this->wellStr = implode("|",$well);
			$this->areaID = intval($_POST['areaID']);
			$this->crewID = intval($_POST['crew']);

			$this->subCrewIDs = isset($_POST['sub_scope']) ? $_POST['sub_scope'] : NULL;  // option for one well
			$this->subCrewStr = !is_null($this->subCrewIDs) ? implode("|",$this->subCrewIDs) : "";
			$this->empID = intval($_POST['empID']);
			$this->sigDate = !empty($_POST['approvaldate']) ? "'" . $_POST['approvaldate'] . "'" : "NULL";
			$this->signature = $_POST['signature'];
			$this->remarks = trim(pg_escape_string($_POST['remarks']));
			$this->services = trim(pg_escape_string($_POST['services']));
			$this->qgcSigName = trim(pg_escape_string($_POST['signee_qgc']));
		 	$this->costCOO= floatval($_POST['cost_estimate']) > 0 ? preg_replace('/,/',"",$_POST['cost_estimate']) : "NULL";
			$this->notifyChecked = isset($_POST['notify']) ? $_POST['notify'] : NULL;

			
			$this->submitForm();
		}
		private function deleteCoo() {
			// delete  TCE  and  Field Ests
			$sql = "DELETE from request_estimate where request_estimate_id = $this->requestID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			$sql = "DELETE from  calloff_order where calloff_order_id = $this->cooID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			$arr = AdminFunctions::getDbLink($this->contractorID);
         $this->dblink=$arr['db_link'];
			$sql = "DELETE from field_estimate where request_estimate_id = $this->requestID";
         $res = AdminFunctions::exec2Con($this->dblink,$sql);

			header("LOCATION: calloff.php?action=list");
			exit;
		}
		private function submitForm() {
			global $BASE;
			if ($this->cooID  == 0 ) { // new no COO given
				$sql = "INSERT into calloff_order values (nextval('calloff_order_calloff_order_id_seq'),$this->requestID,$this->fieldEstID,$this->contractorID,$this->areaID,$this->crewID,
				'$this->conNum', $this->commDate,$this->costCOO,E'$this->services',E'$this->remarks','$this->qgcSigName',current_date,$this->empID,false,'$this->wellStr',
				'$this->signature',$this->sigDate,$this->landOwner_1, $this->loc_1, $this->landOwner_2, $this->loc_2 ,$this->returnGravel, $this->returnWater,'$this->supIDs',NULL,
				'$this->subCrewStr',$this->completionDate,$this->budgetID) returning calloff_order_id";


				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->cooID = $rs->fields['calloff_order_id'];
				$this->updateFieldEst(); //update contractoe


				
			 if (!is_null( $this->notifyChecked )) {
					if (! $res= AdminFunctions::updateFieldEst($this->requestID,$this->cooID,$this->contractorID)) {  // Update COO  to  field  estimates
						die($res);
					}
               $_SESSION['last_request'] = "{$this->URL}/admin/calloff.php?action=list&search=true&coo_id=$this->cooID";
      			$eNum = AdminFunctions::insertEmailLog($this->empID,0,$this->contractorID,"Call Off Order - $this->cooID ",'COO',$this->cooID,date('d-m-Y'),$this->cooID,"NULL");
				   exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");

          }
				// Auto Approve Email
			}
			else if ($this->action == "update" ) { // Update
				$sql = "UPDATE calloff_order set contract_num = '$this->conNum',commencement_date = $this->commDate,crew_id = $this->crewID,services = E'$this->services',remarks=E'$this->remarks',
				cost_estimate = $this->costCOO, creation_date = current_date,area_id = $this->areaID,well_ids = '$this->wellStr',email_emp_ids = '$this->supIDs',
				landowner_id_1 = $this->landOwner_1,landowner_id_2 = $this->landOwner_2,gravel_loc_id_1 = $this->loc_1,water_loc_id_2 = $this->loc_2 ,
 				gravel_return = $this->returnGravel,water_return= $this->returnWater ,sub_crew_ids = '$this->subCrewStr',est_compl_date = $this->completionDate ,
				budget_id = $this->budgetID where calloff_order_id = $this->cooID"; 

				//echo $sql;
				//exit;

				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}  
			}
			else if ($this->cooID > 0 && $this->action == "updatesupgroup" ) {
				$sql = "UPDATE calloff_order set email_emp_ids = '$this->supIDs',landowner_id_1 = $this->landOwner_1,landowner_id_2 = $this->landOwner_2, 
				gravel_loc_id_1 = $this->loc_1,water_loc_id_2 = $this->loc_2, gravel_return = $this->returnGravel,water_return= $this->returnWater, 
				commencement_date = $this->commDate ,est_compl_date = $this->completionDate,budget_id = $this->budgetID  where calloff_order_id = $this->cooID"; 
				//echo $sql;
				//exit;
				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}  

			}
			header("LOCATION: calloff.php?action=list&redo=redo");
			exit;
			
		}
		private function updateFieldEst() {
         $arr = AdminFunctions::getDbLink($this->contractorID);
         $this->dblink=$arr['db_link'];
         $sql = "UPDATE field_estimate  set calloff_order_id = $this->cooID where field_estimate_id = $this->fieldEstID";
			$res = AdminFunctions::exec2Con($this->dblink,$sql);
		}
	}
?>
