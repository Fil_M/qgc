<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class UnlockContractor  {
		public $page;
		public $employeeID;
		public $action;
		public $sessionProfile;
		public $conn;
		public $conDomain;
		public $shortName;
	 	public $contractorName;
	 	public $dbLink;
	 	public $contractorID;

		public function	__construct($action,$conID=NULL ) {
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->contractorID = $conID;

			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('unlock');
				$this->page= $pg->page;
				 if (! is_null($conID)) {
               $connArr = AdminFunctions::getConName($conID);
               $this->contractorName = $connArr['con_name'];
               $this->shortName = $connArr['name'];
               $this->conDomain = $connArr['domain'];
               $this->dbLink = $connArr['db_link'];
					$heading_text = "$this->contractorName Unlock Employee Login";
         	}
				else {
						$heading_text = "Unlock Contractor Employee Login";
				}

		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if(! is_null($this->contractorID)) {   
				if ($action != "list" || ($action == "find" && $this->plantID > 0 )) {
					$DISABLED="";
					switch ($action) {
						case "new":
							$button = "Unlock";
							break;
						default:
							$button = "Unlock";
							break;
					}
			$content = <<<FIN
<div style="width:605px;margin:auto;" >
<form name="unlock"  method="post" action="unlock_contractor.php?SUBMIT=SUBMIT" >
<input type="hidden" name="contractor" id="contractor" value="$this->contractorID" />
<fieldset ><legend style="margin-left:38%">$this->contractorName Employee</legend>
<div class="div60" ><label  class="label" >Employee</label>
FIN;
$content .= AdminFunctions::conEmployeeSelect($this->contractorID,0,false);
$content .= <<<FIN
</div>
<div style="float:right;width:21%;" > <input type="submit" name="SUBMIT" id="SUBMIT" value="$button" class="button" /> </div>
</fieldset>
</form></div>
FIN;
				}
				else {
					$content = "";
				}
			}
			else {
				 $content = AdminFunctions::setContractor("unlock_contractor.php?action=new",$this->contractorID);
      	}
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function processPost() {
			$dbarr=unserialize($_SESSION['dbarr']);
   		$dblink = $dbarr[$this->contractorID][0];
			$this->employeeID = $_POST['operator'];
			$sql = "UPDATE employee set locked  = false where employee_id = $this->employeeID";
			$res = 	AdminFunctions::exec2Con($dblink,$sql);
			AdminFunctions::showMessage("User account unlocked");	
			header("LOCATION: index.php");
			exit;
		}

	}
?>
