<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Budget  {
		public $page;
		public $budgetID;
		public $budgetName;
		public $action;
		public $sessionProfile;
		public $conn;

		public function	__construct($action,$budgetID=0,$search="") {
			$this->budgetID = $budgetID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('budget');
				$this->page= $pg->page;
        		switch($action) {
					 case "new" :
                  $heading_text = "Add New Budget";
                  break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List Budgets - $search" : "List Budgets - All";
						$this->getAllBudgets($search) ;
						break;
					case "find" : 
						$heading_text = "Budget $budgetID";
						$this->getBudgetDetails($budgetID);
						break;
					case "update" :
               	if ($budgetID < 1 ) {
                  	return false;
               	}
              		$heading_text = "Updating Budget $budgetID";
              		$this->getBudgetDetails($budgetID);
              	break;
            	case "delete" :
               	if ($budgetID < 1 ) {
                  	return false;
               	}
             		$heading_text = "Delete Budget $budgetID";
             		$this->getBudgetDetails($budgetID);
             	break;
					default:
						$heading_text = "List Budgets - All";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
            $this->processPost();
         }

		}
		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->budgetID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					case "new":
                  $button = "Add Budget";
                  break;
					case "update":
                  $button = "Update Budget";
                  break;
               case "delete":
                  $button = "Delete Budget";
                  break;
					default:
						$button = "No Button";
						break;
				}
			$content = <<<FIN
<div style="width:40%;margin:auto;">
<form name="budget" method="post" action="budget.php">
<input type="hidden" name="budget_id" value="$this->budgetID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="width:83%;"><legend style="margin-left:49.5%;">Budget</legend>
<div class="div95" ><label for="budget_name" >Budget Name<input type="text" name="budget_name" id="budget_name"  value="$this->budgetName" $DISABLED /></label></div>
</fieldset>
FIN;
				if ($this->action != "find" ) {
              	$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found budget
        		}
				$content .= "\n<a href=\"budget.php?action=list\"   class=\"button\" >Last Screen</a>";

				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllBudgets($search="") {
		  	$whereClause="";
         if (strlen($search) > 0 ) {
            $whereClause = " and upper(budget_name) like '$search%'";
         }
	
			$sql = "SELECT budget_id,budget_name from budget  where removed is false $whereClause order by budget_name";
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}
			$content ="<div style=\"width:1160px;margin:auto;\">";
			$content .= AdminFunctions::AtoZ("budget.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:15px;\"></div>\n";
			$content .= AdminFunctions::oneToNine("budget.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:20px;\"></div>\n";
			$content .= "<div class=\"heading\"  ><div class=\"subscope hd\">Budget Name</div>";
			$content .= "<div class=\"links hd\" style=\"width:124px;\" >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "line2";
				$budget_id = $rs->fields['budget_id'];
				$budget_name = strlen($rs->fields['budget_name']) > 2 ? $rs->fields['budget_name'] : " ";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"subscope $ln highh\" ><a href=\"budget.php?action=find&amp;budget_id=$budget_id\" title=\"View Budget\" class=\"name\" style=\"color:black;\" >$budget_name</a></div>\n";
				if ($this->sessionProfile > 2 ) {
            $content .= "<div class=\"links $ln highh \" style=\"width:124px;\"  >\n";
				$content .= "<div style=\"margin:auto;width:100px;\" >\n";
				$content .= "<a href=\"budget.php?action=update&budget_id=$budget_id\" title=\"Edit Budget\" class=\"linkbutton\" >EDIT</a>\n";
				$content .= "<a href=\"budget.php?action=delete&budget_id=$budget_id\" title=\"Delete Budget\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
            }
            else {
               $content .= "<div class=\"links $ln highh\"  style=\"width:124px;\" > &nbsp;";
            }
				$lineNo += 1;
				$content .="</div></div>\n";
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getBudgetDetails($budgetID) {
			$sql = "SELECT *  from budget  where budget_id = $budgetID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->budgetID = $data['budget_id'];
			$this->budgetName = $data['budget_name'];

		}
		 private function processPost() {
         if ($this->action == "delete" ) {
            $this->deleteBudget();
         }
         $this->budgetID = $_POST['budget_id'];
         $this->budgetName = trim(addslashes($_POST['budget_name']));
         $this->submitBudget();
      }
      private function deleteBudget() {
         $sql = "UPDATE budget set removed = TRUE where budget_id = $this->budgetID";
         if (! $this->conn->Execute($sql)) {
            die($this->conn->ErrorMsg());
         }
         header("LOCATION: budget.php?action=list");
         exit;
      }
      private function submitBudget() {
         if ($this->budgetID == 0 ) {
            $sql = "INSERT into budget values (nextval('budget_budget_id_seq'),E'$this->budgetName',FALSE) returning budget_id";

            if (! $rs =  $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }
            $this->budgetID = $rs->fields['budget_id'];
         }
         else { // Update
            $sql = "UPDATE budget set budget_name = E'$this->budgetName' where budget_id = $this->budgetID";
            if (! $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }

         }

         header("LOCATION: budget.php?action=find&budget_id=$this->budgetID");
         exit;

      }

	}
?>
