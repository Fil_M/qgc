<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class ZipList  {
		public $page;
		public $action;
		public $hDate;
		public $equalto;
      public $between;
      public $hideEndDate='style="display:none;"';
      public $equalgreater;
		public $uploadDate;
		public $endDate;
		public $contractorName;
      public $shortName;
      public $conDomain;
      public $conName;
		public $less;
		public $zipID;


		public function	__construct($action,$hDate=NULL,$search="") {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			$pg = new Page('zip');
			$this->page= $pg->page;
			if ($action == "list" ) {
				$this->hDate = !is_null($hDate) ? $hDate : date('d-m-Y',time() - (86400 * 14));
				$heading_text = "List Zips: ";
				$this->setHeaderText($heading_text);
		 		$this->getAllZips($search,NULL);	

			}
			echo $this->page;

		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}
		private function getAllZips($search=NULL,$redo=NULL,$zipID=NULL) {
			$this->equalgreater = "SELECTED=\"selected\"";
         //$whereClause="";
         if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
         if (! is_null($search) && $search) {
				if (intval($search->ID) >  0 ) {
					$this->zipID = $search->ID;
					$whereClause = "WHERE zip_id = $this->zipID ";


				}
				else if (strlen($search->creDate) > 0) {
                $dateSelected = true;
                $whereClause = " WHERE create_date ";
                $this->uploadDate = $search->creDate;
                $this->hDate = $search->creDate;
                switch ($search->dateType) {
                case "equalto":
                   $whereClause .= "  =  '$search->creDate' ";
                   $this->equalto = "SELECTED=\"selected\"";
                   $this->equalgreater = NULL;
                   break;
                case "equalgreater":
                   $whereClause .= "  >=  '$search->creDate' ";
                   $this->equalgreater = "SELECTED=\"selected\"";
                   break;
					 case "less":
                    $whereClause .= "  <  '$search->creDate' ";
                    $this->less = "SELECTED=\"selected\"";
                    break;
                case "between":
                   if (strlen($search->endDate) > 0 ) {
                      $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                      $this->between = "SELECTED=\"selected\"";
                      $this->endDate = $search->endDate;
                      $this->hideEndDate='';
                      $this->equalgreater = NULL;
                   }
                   else {
                      $whereClause = "  =  '$search->creDate' ";
                   }
                   break;
                   default:
                     $whereClause = " WHERE create_date  >=  current_date  - interval '2 weeks' ";
                     $this->equalgreater = "SELECTED=\"selected\"";
                     break;
               }
				}
			}
			else {
             // Default query still needs search object serialized
            $this->uploadDate = date ('d-m-Y',time() - (86400 * 14));
            $whereClause = " WHERE create_date >= current_date - interval '2 weeks'";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new Search();
            $search->dateType = "equalgreater";
            $search->creDate = $this->uploadDate;
         }
        	$_SESSION['SQL'] = pg_escape_string(serialize($search));
        	$sql = "SELECT * from zip $whereClause order by create_date" ;
         	if (! $rs=$this->conn->Execute($sql)) {
            	if ($this->conn->ErrorNo() != 0 ) {
               	die($this->ErrorMsg());
            	}
         	}
         	$content ="<div style=\"width:1038px;margin:auto;\" >\n";
         	$content  .= <<<FIN
         <form name="zip" method="post" action="zip.php?action=list">
         <fieldset ><legend style="margin-left:38%;">Filter Search - List Zips</legend>
FIN;
		 		$content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->hDate,$this->endDate,$this->hideEndDate,"WIDE");
$content .= <<<FIN
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
         	$content .= "<div style=\"float:right;width:27%;\" >\n";
         	$content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         	$content .= "\n<button  onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" class=\"marg\" >Reset</button>";
         	$content .= "</div></fieldset></form>\n";

				$content .= "<div class=\"heading\" >\n";
         	$content .= "<div class=\"usr hd\">Date Created</div>\n";
         	$content .= "<div class=\"email hd\">Area</div>\n";
         	$content .= "<div class=\"usr hd\">Well</div>\n";
         	$content .= "<div class=\"comments hd\"> Zip Name</div>\n";
         	$content .= "<div class=\"links hd\"   >Actions</div></div>\n";
				$lineNo = 0;
         	while (! $rs->EOF ) {
					extract($rs->fields);
					$zip_name = basename($zip_name);
            	$ln= $lineNo % 2 == 0  ? "line1" : "line2";
            	$create_date = AdminFunctions::dbDate($create_date);
					$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
            	$content .= "<div class=\"usr $ln highh cntr\">$create_date</div>\n";
         		$content .= "<div class=\"email $ln highh\">$area_name</div>\n";
         		$content .= "<div class=\"usr $ln highh\">$well_name</div>\n";
            	$content .= "<div class=\"comments highh $ln\">$zip_name</div>\n";
            	$content .= "<div class=\"links $ln highh cntr\"   >\n";
					$content .="<div style=\"width:100px;margin:auto;\" ><a href=\"zips/{$zip_name}\" title=\"Download Zip\"  class=\"linkbutton\" style=\"width:90px;\" >Download Zip</a></div>\n";
					$content .= "</div>";
					$content .= "</div>";
            	$lineNo += 1;
            	$rs->MoveNext();
         	}
         	$content .= "<hr /></div>";
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	}
?>
