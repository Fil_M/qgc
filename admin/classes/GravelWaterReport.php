<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class GravelWaterReport  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $areaID;
		public $type;
		public $areaName;
		public $totals = array();
		public $Type;
		public $sub = 0;
  		public $tot = 0;
   	public $subCost = 0;
   	public $totCost = 0;
		public $contractors;
		public $gravelWater;
		public $startSubLine=2;
		public $showDate;
		public $landOwner;


		public function	__construct($action,$startDate=NULL,$endDate=NULL,$type="Gravel",$con_id=0,$area=0) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= !is_null($startDate) && strlen($startDate) > 5  ?  $startDate : AdminFunctions::beginLastMonth();
         $this->endDate= ! is_null($endDate)   && strlen($startDate) > 5  ? $endDate  :  AdminFunctions::endLastMonth();
		 	$this->areaID=$area;
		 	$this->contractorID=$con_id;
		 	$this->employeeID=$_SESSION['employee_id'];
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			elseif (isset($_POST['PRINT'])) {
				$this->landOwner = $_POST['landowner'];
				$this->gravelWater = isset($_POST['gravel_water']) ? $_POST['gravel_water'] : "Gravel";
				$this->showDate = isset($_POST['dates']) ? $_POST['dates'] : "summ";
				$this->Type = isset($_POST['ptype']) ? $_POST['ptype'] : "pdf";
         	if ($this->Type == "pdf" ) {
					$URL="https://".$_SERVER['HTTP_HOST'] ."/printGravelWaterpdf.php?action=$this->action&start=$this->startDate&end=$this->endDate&type=$this->gravelWater&con_id=$this->contractorID&area_id=$this->areaID";
					$URL .= "&show_date=$this->showDate&landowner=$this->landOwner";
					header("LOCATION: $URL");
					exit;
				}
				else {
            	$this->getData();
            	$this->printCSV();
				}
			}
			else {
				$pg = new Page('GravelWater');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Print Gravel/Water Reports";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print Report\" class=\"button marg\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1770px;margin:auto;" >
<form name="gravreport" id="gravreport" method="post" action="gravel_water_report.php?action=$action">
<input type="hidden" name="area_id" id="areaID" />
<fieldset ><legend style="margin-left:45%;">Gravel/Water Report</legend>
<div class="div8 margt1" >
<div class="div80"><label>Report Gravel:</label></div><div class="div16" ><input type="radio" name="gravel_water"   value="Gravel"  CHECKED  /></div>
<div class="div80"><label>Report Water:</label></div><div class="div16" ><input type="radio" name="gravel_water"   value="Water"   /></div>
</div>
<div class="div7 marg2 margt1" >
<div class="div80" ><label>Print PDF</label></div><div class="div16" ><input type="radio"  name="ptype"   value="pdf" CHECKED /></div>
<div class="div80" ><label>Print C.S.V.</label></div><div class="div16" ><input type="radio"  name="ptype"   value="csv" /></div>
</div>
<div class="div7 marg2 margt1" >
<div class="div80" ><label>Show Date</label></div><div class="div16" ><input type="radio"  name="dates"   value="show" CHECKED /></div>
<div class="div80" ><label>Summarize</label></div><div class="div16" ><input type="radio"  name="dates"   value="summ" /></div>
</div>
<div class="div10 marg2" >
FIN;
	$content .= AdminFunctions::contractorSelect($this->contractorID,"",true);
	$content .= <<<FIN
</div>
<div class="div15 marg1"  id="land_div" >
FIN;
   $content .= AdminFunctions::ownerSelect(NULL,1,"","landowner",true);
   $content .= <<<FIN
</div>
<div class="div6 marg1" > <label>Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div class="div6 marg1" > <label>End Date:<input type="text" name="enddate" id="edate" class="input required date" value="$this->endDate"  /></label></div>
<div class="div14 marg1" ><label >Area:<input type="text" name="area" id="area" class="input"  value="$this->areaName" /></label></div>
<div style="float:right;width:17%;" >
<button class="reset" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div>
</fieldset>
</form>
</div>
<script>
$('input[type=radio][name=gravel_water]').on('change', function(){
    switch($(this).val()){
        case 'Gravel' :
				owner_div(1);
            break;
        case 'Water' :
				owner_div(2);
            break;
    }            
});
$("#gravreport").submit(function(){
   var isFormValid = true;
   $("#gravreport input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#gravreport .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data); }
      });
  });

</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	public function getData() {
		$dateClause = "1 = 1 ";
      $areaClause = "";
		$ownerLine = $this->landOwner == -1 ? "" : " and l.landowner_id = $this->landOwner ";
      if (! is_null($this->startDate)  && ! is_null($this->endDate)) {
         $dateClause = "gravel_date between '$this->startDate'  and '$this->endDate' ";

      }
      if ($this->areaID > 0 ) {
         $areaClause = "and gw.area_id = $this->areaID ";
      }
		if ($this->contractorID > 0 ) {  // single contractor
         $connArr = AdminFunctions::getAllCon($this->contractorID);
		}
		else {
			 $connArr = AdminFunctions::getAllCon(NULL);
		}
		foreach($connArr as $ind=>$val) { 
			$shortName = $val['name'];
			$conName = $val['con_name'];
			$conID = $val['contractor_id'];
			$this->contractors[$conID] = $conName; // assoc aray  con IDs to names
			if ($this->gravelWater == "Gravel" ) {
		   	if ($this->showDate == "show" ) {
               $sql = "SELECT l.owner_name,'$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,gravel_date,calloff_order_id,$conID as contractor_id,
               gw.gravel_rate as rate,coalesce(gravel_units,0) as unit_tot, coalesce(gravel_total,0) as tot_to_date
               from {$shortName}_gravel_water gw
               LEFT JOIN area a using(area_id)
               LEFT join gravel_water_loc gwl on loc_1_id = gwl.gravel_water_loc_id
               LEFT JOIN landowner l using(landowner_id)
               where $dateClause 
               $areaClause
               $ownerLine
               and coalesce(gravel_units,0) + coalesce(gravel_total,0) > 0
               order by owner_name,con_name ,calloff_order_id,gravel_date,well_name";
				}
				else {
					$sql = "SELECT l.owner_name,'$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,calloff_order_id,$conID as contractor_id,
					gw.gravel_rate as rate,sum(coalesce(gravel_units,0)) as unit_tot, sum(coalesce(gravel_total,0)) as tot_to_date
 					from {$shortName}_gravel_water gw
 					LEFT JOIN area a using(area_id)
 					LEFT join gravel_water_loc gwl on loc_1_id = gwl.gravel_water_loc_id
 					LEFT JOIN landowner l using(landowner_id)
  					where $dateClause 
					$areaClause
					$ownerLine
  					and coalesce(gravel_units,0) + coalesce(gravel_total,0) > 0
			   	group by owner_name,con_name,area_name,well_name,calloff_order_id,contractor_id,rate
					order by owner_name,con_name,area_name,well_name";
				}
			}
			else {  // Water
				if ($this->showDate == "show" ) {
               $sql = "SELECT l.owner_name, '$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,gravel_date,calloff_order_id,$conID as contractor_id,
               gw.water_rate as rate, coalesce(water_units,0) as unit_tot, coalesce(water_total,0) as tot_to_date
               from {$shortName}_gravel_water gw
               LEFT JOIN area a using(area_id)
               LEFT join gravel_water_loc gwl on loc_2_id = gwl.gravel_water_loc_id
               LEFT JOIN landowner l using(landowner_id)
               where $dateClause 
               $areaClause
               $ownerLine
               and coalesce(water_units,0) + coalesce(water_total,0) > 0
               order by owner_name,con_name,calloff_order_id,gravel_date,well_name";
            }
				else {
					$sql = "SELECT l.owner_name, '$conName' as con_name,a.area_name,wells_from_ids(gw.well_ids) as well_name,calloff_order_id,$conID as contractor_id,
					gw.water_rate as rate, sum(coalesce(water_units,0)) as unit_tot, sum(coalesce(water_total,0)) as tot_to_date
            	from {$shortName}_gravel_water gw
            	LEFT JOIN area a using(area_id)
            	LEFT join gravel_water_loc gwl on loc_2_id = gwl.gravel_water_loc_id
            	LEFT JOIN landowner l using(landowner_id)
            	where $dateClause 
			   	$areaClause
					$ownerLine
            	and coalesce(water_units,0) + coalesce(water_total,0) > 0
			   	group by owner_name,con_name,area_name,well_name,calloff_order_id,contractor_id,rate
			   	order by owner_name,con_name,area_name,well_name";
				}
			}


			if (! $totAll = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			else {
				$this->totals[] = $totAll;
			}
				//$this->totals[] = array("calloff_order_id"=>"3265","contractor_id"=>"5","well_name"=>'344',"area_name"=>'name',"rate"=>'0.55',"unit_tot"=>'100',"tot_to_date"=>'5.50',"owner_name"=>'Lanndy');
		}  //foreach contractor 
		//array_multisort($this->totals);
	}	
	public function printCSV() {
		if (!is_null($this->startDate)  && !is_null($this->endDate) ) {
         $dateStr = "between $this->startDate and $this->endDate";
      }
		$typeStr = $this->showDate == "show" ? "All Dates Shown" : "Summarized ";
		$title = array('','','','',"$this->gravelWater Report  $typeStr $dateStr");

		if($this->gravelWater == "Gravel") {
			if ($this->showDate == "show" ) {
         	$headings = array('Date','C.O.O.','Contractor','Area','Well/s','Land Owner','Rate','Quantity m3','Cost');
			}
			else {
         	$headings = array('C.O.O.','Contractor','Area','Well/s','Land Owner','Rate','Quantity m3','Cost');
			}
		}
		 else {
			if ($this->showDate == "show" ) {
         	$headings = array('Date','C.O.O.','Contractor','Area','Well/s','Land Owner','Rate','Quantity m3','Cost');
			}
			else {
         	$headings = array('C.O.O.','Contractor','Area','Well/s','Land Owner','Rate','Quantity kL','Cost');
			}
      }
		$file= "tmp/gravel_water_report_{$this->employeeID}.csv";
      $fp = fopen($file,'w');
      fputcsv($fp,$title);
      fputcsv($fp,$headings);
      $lineCount=2;

		$pOwner = -1;
		foreach($this->totals as $i=>$arr) {
      	foreach($arr as $ind=>$val){
         	extract($val);
         	$this->sub += $unit_tot;
         	$this->subCost += $tot_to_date;
         	$this->tot  += $unit_tot;
         	$this->totCost += $tot_to_date;
         	$owner_name = trim($owner_name);
				$date=$this->showDate == "show" ? AdminFunctions::dbDate($gravel_date) : "";
         	if ($owner_name != $pOwner ) {
            	if ($pOwner ==  -1 ) {
               	$pOwner = $owner_name;
            	}
            	else {
               	$this->sub -= $unit_tot;
               	$this->subCost -= $tot_to_date;
               	$this->doSubs($fp,$lineCount);
		   			$lineCount +=2;
						$this->startSubLine = $lineCount;
               	$pOwner = $owner_name;
               	$this->sub = $unit_tot;
               	$this->subCost = $tot_to_date;
            	}
         	}
				$conName = $this->contractors[$contractor_id];
         	$unTot =number_format($unit_tot,2);
         	$tot = number_format($tot_to_date,2);
				if ($this->showDate == "show" ) {
					$tmpArr= array($date,$calloff_order_id,$conName,$area_name,$well_name,$owner_name,$rate,$unTot,$tot);
				}
				else {
					$tmpArr= array($calloff_order_id,$conName,$area_name,$well_name,$owner_name,$rate,$unTot,$tot);
				}
         	fputcsv($fp,$tmpArr);
		   	$lineCount ++;
			}	
		}
		$this->doSubs($fp,$lineCount);
		$this->doTotals($fp,$lineCount);

		header('Content-Description: File Transfer');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment; filename='.basename($file));
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: ' . filesize($file)); //Remove

      readfile($file);
      exit;
	}
	private function doSubs($fp,$line) {
			$this->startSubLine += 1;
			if ($this->showDate == "show" ) {
				$tmpArr= array('','','','','','','Sub Total','=SUM(H'.$this->startSubLine .':H'.$line .')', '=SUM(I'.$this->startSubLine.':I'.$line .')');
			}
			else {
				$tmpArr= array('','','','','','Sub Total','=SUM(G'.$this->startSubLine .':G'.$line .')', '=SUM(H'.$this->startSubLine.':H'.$line .')');
			}
         fputcsv($fp,$tmpArr);
         fputcsv($fp,array());
	}
	private function doTotals($fp,$line) {
         fputcsv($fp,array());
			if ($this->showDate == "show" ) {
				$tmpArr= array('','','','','','','Total',number_format($this->tot,2), number_format($this->totCost,2));
			}
			else {
				$tmpArr= array('','','','','','Total',number_format($this->tot,2), number_format($this->totCost,2));
			}
         fputcsv($fp,$tmpArr);
	}

}
?>
