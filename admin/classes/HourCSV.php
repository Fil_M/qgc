<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class HourCSV  {
		public $page;
		public $action;
		public $sessionProfile;
		public $hourID;
		public $contractorID;
		public $plants;
		public $rates;
		public $areas;
		public $wells;
		public $crews;
		public $hours;
		public $subtot;
		public $coos;
		public $litres;
		public $cubic;
		public $details;
		public $crewID=0;
		public $subCrewID;
		public $plantID=0;
		public $unitID=0;
		public $areaID=0;
		public $wellID=0;
		public $callOffID;
		public $operatorName;
		public $plantName;
		public $plantTypeID;
		public $areaName;
		public $unitName;
		public $statusText = array("0"=>"Newly Entered","1"=>"Awaiting Approval","2"=>"REJECTED","3"=>"Late Submission","4"=>"Rejected Entry","5"=>"Approved","6"=>"Requested Completion",
										"7"=>"Approved Completion","8"=>"Invoiced","9"=>"Paid");
		public $status=NULL;
		public $hDate;
		public $dktHours;
		public $docketNo;
		public $total;
		public $conn;
		public $equalgreater;
		public $equalto; 
		public $between;
		public $less;
		public $hideEndDate=true;
		public $endDate;
		public $invoice;
		public $invoiceNum;
		public $totHours;
		public $employeeID;
		public $operatorID;
		public $dktFrom;
		public $dktTo;
		public $expenseID;
		public $search;
      public $contractorName;
      public $shortName;
      public $conDomain;
      public $dbLink;
		public $file;
		public $csv = array('Date'=>array(),'EnterTime'=>array(),'COO'=>array(),'Operator'=>array(),'DocketHours'=>array(),'Docket'=>array(),'PlantType'=>array(),'Unit'=>array(),'Rate'=>array(),'Total'=>array(),'Area'=>array(),'Well'=>array(),'Detail'=>array(),'Crew'=>array(),'subScope'=>array(),'Status_Txt'=>array(),'Reject_Txt'=>array());


		public function	__construct($action="list",$hourID=0,$conID,$search=NULL,$redo=NULL,$order=NULL) {
			$this->action = $action;
			$this->hourID = $hourID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
         $this->file= "tmp/hour_csv_{$this->employeeID}.csv";
			$this->contractorID = $conID;
			$this->conn = $GLOBALS['conn'];
			$this->search = $search;
			$pg = new Page('hours');
			$this->page= $pg->page;
			if (! is_null($conID)) {
            $connArr = AdminFunctions::getConName($conID);
            $this->contractorName = $connArr['con_name'];
            $this->shortName = $connArr['name'];
            $this->conDomain = $connArr['domain'];
            $this->dbLink = $connArr['db_link'];
         }

         switch($action) {
				case "list" :
					$heading_text =   "Print CSV of Hours";
					$this->getAllHours($search,$redo,$order) ;
					break;
			}
		 	$this->setHeaderText($heading_text);	
		 	///$this->setContent($action,$redo);	
			echo $this->page;
			}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function getAllHours($search,$redo,$order) {
			$hour_table = "hour"; // default hour  table  status  3 lookups  need  hour_audit
			$URL="https://".$_SERVER['HTTP_HOST'] ;
			$totHours = $gTot = 0.0;
			$showApproval= false; //  If exact date with  and Well then offer approval button
			$CHECKED = "";
			$this->hourID = NULL;
			$dateSelected = false;
			$_SESSION['last_request'] = "$URL/admin/hourcsv.php?action=list&redo=redo&con_id=$this->contractorID";

			//$whereClause="where h.removed is false and h.well_id = w.well_id ";
			$whereClause="where 1 = 1 ";
			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
				$_SESSION['last_request'] = "{$URL}/admin/hourcsv.php?action=list&redo=redo";
				if (!is_null($order) && strlen($order) > 0) {
            	$search->order = $order;
          	}
				$_SESSION['SQL'] = pg_escape_string(serialize($search));
				if (intval($search->hourID) > 0 ) {
					$this->hourID = $search->hourID;
					$whereClause .= " and hour_id = $this->hourID ";
				} 
				else if (intval($search->cooID) > 0 ) {
						$this->callOffID = $search->cooID;
						$whereClause .= " and calloff_order_id = $this->callOffID ";
				} 
				else if (! is_null($search->invoiceNum) && strlen($search->invoiceNum) > 2) {
					$this->invoiceNum = $search->invoiceNum;
					$whereClause .= " amd invoice_num  = '$this->invoiceNum' ";
				}
				else if (! is_null($search->dktFrom)  && strlen($search->dktFrom) > 2) {
					$this->dktFrom = $search->dktFrom;
					$whereClause .= "and docket_num ";
					if (! is_null($search->dktTo)  && strlen($search->dktTo) > 2) {
						$this->dktTo = $search->dktTo;
						$whereClause .= " between  '$search->dktFrom' and '$search->dktTo' ";
					}
					else {
						$whereClause .= " = '$search->dktFrom' ";
					}
				}
				else {
					if (strlen($search->hDate) > 0) {
						$dateSelected = true;
						$whereClause .= " and h.hour_date ";
               	$this->hDate = $search->hDate;
               	switch ($search->dateType) {
                  	case "equalto":
                     	$whereClause .= "  =  '$search->hDate' ";
                     	$this->equalto = "SELECTED=\"selected\"";
								$showApproval = true;
                     	break;
                  	case "equalgreater":
                     	$whereClause .= "  >=  '$search->hDate' ";
                     	$this->equalgreater = "SELECTED=\"selected\"";
                     	break;
                  	case "less":
                     	$whereClause .= "  <  '$search->hDate' ";
                     	$this->less = "SELECTED=\"selected\"";
                     	break;
                  	case "between":
                     	if (strlen($search->endDate) > 0 ) {
                        	$whereClause .= " between  '$search->hDate' and '$search->endDate' ";
                        	$this->between = "SELECTED=\"selected\"";
                        	$this->endDate = $search->endDate;
                        	$this->hideEndDate=false;
                     	}
                     	else {
                        	$whereClause .= "  =  '$search->hDate' ";
                     	}
                     	break;
                  	default:
                     	$whereClause .= "  =  '$this->hDate'";
            		   	$this->equalto = "SELECTED=\"selected\"";
                     	break;
               	}
					}
					if ($search->operatorID > 0 ) {
						$whereClause .= " and h.employee_id = $search->operatorID ";
						$this->operatorName = $search->operatorName;
						$this->operatorID = $search->operatorID;
					}
					if (! empty($search->plantTypeID )) {
						  $this->plantTypeID = $search->plantTypeID;
                    $this->plantName = $search->plantName;
                    $whereClause .= " and  h.plant_id in (select plant_id from {$this->shortName}_plant where plant_type_id = $search->plantTypeID)";
					}
					if (!empty($search->areaID) ) {
						$whereClause .= " and h.area_id = $search->areaID ";
						$this->areaName = $search->areaName;
						$this->areaID = $search->areaID;
					}
					if (!empty($search->wellID)) {
						$whereClause .= " and $search->wellID in (select (unnest(string_to_array(h.well_ids::text,'|')::int[])))";
						$this->wellID = $search->wellID;
					}
					if (!empty($search->unitID)) {
						$whereClause .= " and h.plant_id = $search->unitID ";
						$this->unitName = $search->unitName;
						$this->unitID = $search->unitID;
					}
					if (!empty($search->crewID)) {
						$whereClause .= " and h.crew_id = $search->crewID ";
						$this->crewID = $search->crewID;
					}
					if (!empty($search->subCrewID)) {
						$whereClause .= " and h.sub_crew_id = $search->subCrewID ";
						$this->subCrewID = $search->subCrewID;
					}
					if (! is_null($search->status)) {
						$this->status = $search->status;
						if ($search->status == 6) {
							$whereClause .= " and invoice_num != '' ";
						}
						else if ($search->status == 2 ) {
							$whereClause .= " and status in (2,4)  ";	
						}
						else if ($search->status == 3 ) {
							$hour_table = "hour_audit";  ///  Maintains original  status on Update Trigger
							$whereClause = preg_replace('/and h.hour_date/',"and h.enter_time",$whereClause);  // enter_time !!
							$whereClause .= " and status = $this->status ";	
						}
						else {
							$whereClause .= " and status = $this->status ";	
						}
					}
					
				}  // end else  coo id + date
			}  // end search null
			else {
				 // Default query still needs search object serialized
				if (! $dateSelected) {
					$this->hDate = date('d-m-Y');
					$whereClause .= " and h.hour_date = '$this->hDate'";
            	$this->equalto = "SELECTED=\"selected\"";
				}
         	$search = new Search();
         	$search->dateType = "equal";
         	$search->hDate = $this->hDate;
				$search->order = "h.hour_date,opname,calloff_order_id,plant_id,a.area_name";
         	$_SESSION['SQL'] = pg_escape_string(serialize($search));
			}

			//   set heading backgrounds  and SQL order clause for eelected sort order
         $hd1 = $hd2 = $hd3 = $hd4 = $hd5 = $hd6 = $hd7 = $hd8 ="";
         switch($search->order) {
            case "date" :
            $hd2="sort";
            $orderClause = " order by h.hour_date";
            break;
            case "operator" :
            $hd1="sort";
            $orderClause = " order by opname,h.hour_date";
            break;
            case "docket" :
            $hd3="sort";
            $orderClause = " order by docket_num,h.hour_date";
            break;
            case "unit" :
            $hd4="sort";
            $orderClause = " order by p.plant_unit,h.hour_date";
            break;
            case "area" :
            $hd5="sort";
            $orderClause = " order by a.area_name,h.hour_date";
            break;
            case "well" :
            $hd6="sort";
            $orderClause = " order by w.well_name,h.hour_date";
            break;
            case "crew" :
            $hd7="sort";
            $orderClause = " order by c.crew_name,h.hour_date";
            break;
            case "coo" :
            $hd8="sort";
            $orderClause = " order by calloff_order_id,h.hour_date";
            break;
            default:
               $hd2="sort";
               $orderClause = " order by h.hour_date,opname,calloff_order_id,plant_id,a.area_name";
            break;
         }
          
		 if (! is_null($this->contractorID)) {
			$sql = "SELECT h.*,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,pt.p_type,plant_unit,wells_from_ids(h.well_ids) as well_name,a.area_name,c.crew_name,sub_crew_name
						 from {$this->shortName}_{$hour_table} h
						LEFT JOIN {$this->shortName}_plant p using (plant_id)
						LEFT JOIN plant_type pt using (plant_type_id)
						LEFT JOIN area a using (area_id)
						LEFT JOIN {$this->shortName}_employee e using (employee_id)
						LEFT JOIN crew c  using (crew_id)
						LEFT JOIN sub_crew sc  using (sub_crew_id)
						$whereClause   
						$orderClause";

			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content ="<div style=\"width:width1750px;margin:auto;\">";
			// search
			$content .= <<<FIN
         <form name="hour" method="post" action="hourcsv.php?action=list&search=true">
         <input type="hidden" name="operatorID" id="operatorID" value="$this->operatorID" />
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         <input type="hidden" name="unitID" id="unitID" value="$this->unitID" />
			<input type="hidden" name="contractor" id="contractorID" value="$this->contractorID" />
			<input type="hidden" name="shortName" id="shortName" value="$this->shortName" />
			<input type="hidden" name="plantTypeID" id="plantTypeID" value="$this->plantTypeID" />

<fieldset ><legend style="margin-left:46%;">Filter Search - Hours</legend>
<div class="div700 marg2"  ><label for="coo_id" >C.O.O.<input type="text" name="coo_id" id="coo_id" value="$this->callOffID" /></label></div>
<div class="div22 marg2"  ><label for="operator"  >Operator<input type="text" name="operator" id="operator" value="$this->operatorName" /></label></div>
<div class="marg1"  style="margin-left:0.8%;" >&nbsp;</div>
FIN;
      $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->hDate,$this->endDate,$this->hideEndDate);
      $content .=  "<div class=\"div9 marg2\" ><label >Status</label>\n";
   $content .= AdminFunctions::statusSelect($this->status);
$content .= <<<FIN
</div>
<div class="div6 marg2" ><label for="invoicenum" >Invoice Num:<input type="text" name="invoicenum" id="invoicenum"  value="$this->invoiceNum" /></label> </div>
<div style="float:right;width:15%;" >
<input type="submit" name="search" value="Search"  class="button" />
<button  onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
</div>
<div style="clear:both;"> </div>
<div class="div17 marg2" ><label for="machine" >Plant Type<input type="text" name="machine" id="machine"  value="$this->plantName" /></label></div>
<div class="div12 marg2" ><label for="area" >Area<input type="text" name="area" id="area"  value="$this->areaName" /></label></div>
<div class="div12 marg2" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= "</div></div>\n";
$content .= "<div class=\"div12 marg1\" style=\"margin-left:1.08%;\" ><label  >Work Scope</label>";
$content .= AdminFunctions::crewSelect($this->crewID,"",true);
$content .= "</div>\n";
$content .= "<div class=\"div17 marg2\" id=\"subscopediv\" >";
$content .= AdminFunctions::subScopeSelect($this->crewID,$this->subCrewID,"");
$content .= "</div>\n";
	if (! is_null($this->search)) {
   	$content .= "<div style=\"float:right;width:10.25%;\" ><a href=\"hourcsv.php?action=list&search=true&printcsv=true&file=$this->file\" class=\"button oran\" >Print C.S.V.</a></div>";
	}
$content .= <<<FIN
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});

	$(function () {
      $('#operator').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=operator',
         minChars:2,
			params: {con_id: $('#contractorID').val()},
         onSelect: function(value, data){ $('#operatorID').val(data); return false; }
      });
  });
$(function () {
      $('#machine').autocomplete({
         width: 700,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=planttype',
         minChars:1,
         params: {con_id: $('#contractorID').val()},
         onSelect: function(value, data){ $('#plantTypeID').val(data); }
      });
  });
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_s',$('#welldiv'),$('#contractorID')); return false; }
      });
  });
$(function () {
      $('#unit').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=unit',
			params: {shortname: $('#shortName').val()}, 
         minChars:2,
         onSelect: function(value, data){ $('#unitID').val(data[0]);$('#machine').val(data[1]); return false;}
      });
  });
$(function () {
      $('#coo_id').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=calloffs',
			params: {con_id:  function() { return $('#contractorID').val()} },
         minChars:2
      });
  });
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','single'); });
</script>
FIN;


				$content .= "</fieldset></form></div>\n";
				while (! $rs->EOF ) {
					$total = null;
					$coo = $rs->fields['calloff_order_id']; 
					$status = $rs->fields['status'];
					$this->csv['Status_Txt'][] = $this->statusText[$status];
					$this->csv['Reject_Txt'][] =  strlen($rs->fields['reject_text']) > 0 ? $rs->fields['reject_text'] : "";;
					$hourID = $rs->fields['hour_id'];
					$operator = $rs->fields['opname'];
					$this->csv['Operator'][] = $operator;
					$jDate = AdminFunctions::dbdate($rs->fields['hour_date']);
					$this->csv['Date'][] = $jDate;
					$eTime = AdminFunctions::dbTime($rs->fields['enter_time']);
					$this->csv['EnterTime'][] = $eTime;
					$total_t1 = floatval($rs->fields['total_t1']);
					$total_t2 = floatval($rs->fields['total_t2']);
					$expense = floatval($rs->fields['expense']);
					$dktHours_t1 = $rs->fields['docket_hours_t1']; 
					$dktHours_t2 = $rs->fields['docket_hours_t2']; 
					$dktHours = $dktHours_t1 > 0 ? sprintf("%'.05.2f",$dktHours_t1) : sprintf("%'.05.2f",$dktHours_t2);
					if ($total_t1 > 0 ) {
						$total = sprintf('%01.2f',$total_t1);
					}
					else if ($total_t2 > 0 ) {
						$total = sprintf('%01.2f',$total_t2);
					}
					else if ($expense > 0 ) {
						$total = sprintf('%01.2f',$expense);
					}
					$gTot += $total;
					$totHours += $dktHours;
					$this->csv['DocketHours'][] = $dktHours;
				//echo "$hourID $upName $status". "<br />";
					$dktNum = $rs->fields['docket_num']; 
					$this->csv['Docket'][] = $dktNum;
				//$machine = substr($rs->fields['plant_name'],0,40);
					$unit = $rs->fields['plant_unit'];
					$this->csv['Unit'][] = $unit;
					$rate = sprintf('%01.2f',$rs->fields['rate']);
					$this->csv['Rate'][] = $rate;
					$this->csv['Total'][] = $total;
					$area = $rs->fields['area_name'];
					$this->csv['Area'][] = $area;
					$well = $rs->fields['well_name'];
					$this->csv['Well'][] = $well;
					$crew = $rs->fields['crew_name'];
					$this->csv['Crew'][] = $crew;
					$details = $rs->fields['details'];
					$this->csv['Detail'][] = $details;
					$details = $status != 2 ? $details : $rs->fields['reject_text'];
					$invoice = trim($rs->fields['invoice_num']);
					$pType = $rs->fields['p_type'];
					$this->csv['COO'][] = $coo;
				//$pType = $rs->fields['p_type'];
					$this->csv['PlantType'][] = $pType;
					$subScope = !empty($rs->fields['sub_crew_name']) ? $rs->fields['sub_crew_name'] : "";
					$this->csv['subScope'][] = $subScope;
					$rs->MoveNext();
				}  
			//exit;	
				$this->printCSV();
			// Page  totals
				$totHours = number_format($totHours,2);
         	$gTot = number_format($gTot,2);
         	if (! is_null($this->search)) {
            	$content .= "<div class=\"heading\" ><div style=\"width:550px;float:left;border-left:1px solid #000000;\" > &nbsp;</div><div class=\"fone hd\"  >Time Total</div>\n";
            	$content .= "<div style=\"width:500px;float:left;border-left:1px solid #000000;\" > &nbsp;</div><div class=\"fone hd bdr\" >Total</div></div>\n";
            	$content .= "<div style=\"width:550px;float:left;border-left:1px solid #000000;height:2.6125rem;\"  > &nbsp;</div><div class=\"fone cntr padtop\"  >$totHours</div>\n";
            	$content .= "<div style=\"width:500px;float:left;border-left:1px solid #000000;height:2.6125rem;\" > &nbsp;</div><div class=\"fone cntr bdr padtop\" >$gTot</div>\n";
            	$content .="<div style=\"clear:both;\"></div>\n";
            	$content .= "<hr />";
         	}

			}
			else { // ContractorID NULL show contrcator select
            $content = AdminFunctions::setContractor("hourcsv.php?action=list",$this->contractorID);
         }
			$this->page = str_replace('##MAIN##',$content,$this->page);

		}
		private function printCSV() {

         $fp = fopen($this->file,'w');

         $headings = array('Date','Enter Time','C.O.O.','Operator','Docket Hours','Docket','Unit','Plant/Type','Rate','Total','Area','Well','Detail','Work Scope','Sub Scope','Status','Reject Reason');
			if (strlen($this->invoiceNum) > 2 ) {
				$title = array("Invoice Number $this->invoiceNum");
         	fputcsv($fp,$title);
			}
			$lineCount=1;
         fputcsv($fp,$headings);
			if (!empty($this->csv['Date'])  ) {
         	foreach($this->csv['Operator'] as $ind =>$val) {
            	$tmpArr= array($this->csv['Date'][$ind],$this->csv['EnterTime'][$ind],$this->csv['COO'][$ind],$this->csv['Operator'][$ind],$this->csv['DocketHours'][$ind],$this->csv['Docket'][$ind],$this->csv['Unit'][$ind],$this->csv['PlantType'][$ind],$this->csv['Rate'][$ind], $this->csv['Total'][$ind],$this->csv['Area'][$ind],$this->csv['Well'][$ind],$this->csv['Detail'][$ind],$this->csv['Crew'][$ind],$this->csv['subScope'][$ind],$this->csv['Status_Txt'][$ind],$this->csv['Reject_Txt'][$ind]);
			//var_dump($tmpArr);
			//exit;
         		fputcsv($fp,$tmpArr);
					$lineCount ++;
         	}
				fputcsv($fp,array("","","",'=ROUND(SUM(E2:E'.$lineCount.'),2)',"","","","","",'=ROUND(SUM(J2:J'.$lineCount .'),2)'));
			}
         fclose($fp);
      }
	}
?>
