<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class ResendHour  {
		public $page;
		public $action;
		public $startDate;
		public $contractorID;
		public $supervisorID;
		public $employeeID;
		public $conDomain;
		public $contractorName;

		public function	__construct($action,$conID=0,$startDate=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= ! is_null($startDate) ? $startDate  : AdminFunctions::yesterday();
		 	$this->contractorID=$conID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = $_SESSION['employee_id'];
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['RESEND'])) {
				$this->contractorID = $_POST['contractor'];
				$this->supervisorID = $_POST['supervisor'][0];  // shows  Multi  but  only  select  one
				$this->startDate = $_POST['startdate'];
            $connArr = AdminFunctions::getConName($this->contractorID);
            $this->contractorName = $connArr['con_name'];
            $this->conDomain = $connArr['domain'];
				$URL =  $URL="https://".$_SERVER['HTTP_HOST'];
         	$_SESSION['last_request'] = "$URL/admin/index.php";
				$conURL = "{$this->conDomain}/reprintWorksheet.php?action=email&hdate=$this->startDate&sup_id=$this->supervisorID&con_id=$this->contractorID&emp_id=$this->employeeID";
				header("LOCATION: $conURL");
				exit;
			}
			else {
				$pg = new Page('Resend');
				$this->page= $pg->page;
				if ($action == "email" ) {
					$this->supGroupID = AdminFunctions::supGroupFromEmp($this->employeeID);
					$heading_text = "Resend Contractor Hours";
					$button = "<input type=\"submit\" name=\"RESEND\" value=\"Resend Report\" class=\"button\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1300px;margin:auto;">
<form name="resend" id="resend" method="post" action="resend_hour.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<fieldset ><legend style="margin-left:44%;">Resend Daily Worksheet</legend>
<div class="div33"  ><label for="supervisor_group" class="label" >Supervisor:</label>
FIN;
	$content .= AdminFunctions::supervisorSelect($this->employeeID,"",true);
	$content .= "</div> <div class=\"div16 marg\">\n";
   $content .= AdminFunctions::contractorSelect($this->contractorID,"",false,"contractor",false,true);
	$content .= <<<FIN
</div>
<div class="div10 marg" ><label for="sdate" >For Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div style="float:right;width:24%;" >
$button
<button onclick="$('form').clearForm();$(':hidden').val(0);return false;" class="marg" >Reset</button>
</div>
</fieldset>
</form></div>
<script>
$("#resend").submit(function(){
   var isFormValid = true;
   $("#resend input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#resend .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please select one Supervisor (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
 }
?>
