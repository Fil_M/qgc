<?php  // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class ATW  {
		public $page;
		public $atwID;
		public $atwNo;
		public $atwType;
		public $conn;
		public $atwAddress;
		public $action;
		public $contacts;
		public $landOwner;
		public $siteCon;
		public $conIDs;
		public $empIDs;
		public $conFirst;
		public $conLast;
		public $conEmail;
		public $conExpire;
		public $contactIDs;
		public $empID;
		public $documentLines;
      public $creDate;
		public $less;
		public $hideEndDate='style="display:none;"';
		public $endDate;
		public $sessionProfile;
		public $between;
		public $equalto;
		public $contractorID;
		public $contactArr = array();
		public $docID;
		public $docType;
		public $docPrevFile;
		public $docStatus;
		public $docATWTypeID;
      public $sumDoc;
		public $countDoc;
		public $statColor;
		public $emailSent = false;
		public $areaID;
		public $wellIDs;
		public $wellStr;
		public $areaName;
		public $contactFone;
		public $meeting;
		public $updateWell;
		public $notes;
		public $expires;
		public $emailEmpArr = array();
		public $emailConArr = array();
		public $emailContrArr = array();
		public $selType;
		public $URL;

		public function	__construct($action,$atwID=0,$search="",$redo=NULL) {
			$this->URL="https://".$_SERVER['HTTP_HOST'];
			$this->conn = $GLOBALS['conn'];
			$this->atwID = $atwID;
			$this->action = $action;
			$this->empID = $_SESSION['employee_id'];
			$this->sessionProfile = $_SESSION['profile_id'];
			$this->atwType = AdminFunctions::getATWType();
			set_time_limit(0);   // may  help  long  upload  posts
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('atw');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New ATW";
						break;
					case "update" :
						if ($atwID < 1 ) {
							return false;
						}
						$heading_text = "Updating ATW $atwID";
						$this->getATWDetails($atwID);
						break;
					case "delete" :
						if ($atwID < 1 ) {
							return false;
						}
						$heading_text = "Delete ATW $atwID";
						$this->getATWDetails($atwID);
						break;
					case "list" :
						$heading_text =   "List ATW's ";
						$this->getAllATWs($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "ATW $atwID";
						$this->getATWDetails($atwID);
						break;
					case "show" : 
						$heading_text = "ATW $atwID";
						$this->getATWDetails($atwID);
						$this->showDownloadPage();
						break;
					default:
						$heading_text = "Add New ATW";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function showDownloadPage() {
			$DISABLED="disabled";
			$content = <<<FIN
			<div style="width:1500px;margin:auto;">
<input type="hidden" name="atw_id" id="atwID" value="$this->atwID" />
<input type="hidden" name="empID" id="empID" value="$this->empID" />
<input type="hidden" name="contractorID" id="contractorID" value="0" />
<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:46%;">Authority To Work</legend>
<div class="div7"  > <label for="atw_number" >ATW Number<input type="text" name="atw_number" id="atw_number" class="required"  value="$this->atwNo" DISABLED /></label></div>
<div class="div30 marg1"><label for="address" >Location<input type="text" name="address" id="address" class="required" value="$this->atwAddress" DISABLED /></label></div>
<div class="div15 marg1"><label for="sitecon" >Site Contact<input type="text" name="site_con" id="site_con" class="required"  value="$this->siteCon" DISABLED /></label></div>
<div class="div9 marg1" ><label for="contact_fone" >Phone<input name="contact_fone" id="contact_fone" value="$this->contactFone" type="text"  DISABLED /></label></div>
<div class="div35 marg1" ><label for="landowner" >Landowner<input type="text" name="landowner" id="landowner" class="required"  value="$this->landOwner" DISABLED /></label></div>
<div style="clear:both;" ></div>
<div class="div17" style="width:17.25%;" ><label for="area" >Area<input type="text" name="area" id="area" class="required" value="$this->areaName" DISABLED /></label></div>
<div class="div15 marg"  ><div id="welldiv" >
FIN;
         $content .= AdminFunctions::wellSelectMulti($this->areaID,$this->wellIDs);
$content .= <<<FIN
</div></div>
<div class="div11 marg1" ><label for="meeting"  >Meeting<input type="text" name="meeting" id="meeting" class="required cntr"  value="$this->meeting" DISABLED /></label></div>
<div class="div9 marg" style="margin-left:4.95%;" ><label for="expire"  >Expires<input type="text" name="expire" id="expire" class="date required"  value="$this->expires" DISABLED /></label></div>
<div class="div35 marg1" > <label for="notes"  >Notes<textarea  name="notes" id="notes" class="txtnotes" DISABLED >$this->notes</textarea></label></div>
</fieldset>

<fieldset ><legend style="margin-left:47%;">Documents</legend>
<div id="uads" >
<div style="margin:auto;width:800px;margin-bottom:1rem;" >Right click Mouse over the Document Name and choose  "Save (Link) As" &nbsp; or Click to view in Browser</div> 
FIN;
	  	if (count($this->documentLines) > 0 )  {
				$x=0;
            foreach($this->documentLines as $ind=>$val) {
               $sub = chr(97 + $x). " ) ";
               $docName =  basename($val['document_name']);
					$link = $val['document_name'];
               $content .= "<div class=\"div5 subatw\" >$sub</div><div class=\"div90\" ><a href=\"{$this->URL}/admin/$link\" class=\"linkdiv\" >$docName</a></div>";
					$content .= "<div style=\"clear:both;\" ></div>\n";
					
               $x+=1;
            }
       }

			$content .="</div></fieldset>\n";
			$content .= "</div>\n"; // Center Div
			$this->page = str_replace('##MAIN##',$content,$this->page);


		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->atwID > 0 )) {
				$DISABLED="";

				switch ($action) {
					case "new":
						$button = "Add ATW";
						break;
					case "update":
						$button = "Update ATW";
						break;
					case "delete":
						$button = "Delete ATW";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add ATW";
						break;
				}
			$content = <<<FIN
<div style="width:1700px;margin:auto;">
<form name="atw" class="atwform" id="atwform" method="post" action="atw.php" enctype="multipart/form-data"  >
<input type="hidden" name="atw_id" id="atwID" value="$this->atwID" />
<input type="hidden" name="empID" id="empID" value="$this->empID" />
<input type="hidden" name="contractorID" id="contractorID" value="0" />
<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
<input type="hidden" name="emailSent" value="$this->emailSent" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:46%;">Authority To Work</legend>
<div class="div7"  > <label for="atw_number" >ATW Number<input type="text" name="atw_number" id="atw_number" class="required"  value="$this->atwNo" $DISABLED /></label></div>
<div class="div30 marg1"><label for="address" >Location<input type="text" name="address" id="address" class="required" value="$this->atwAddress" $DISABLED /></label></div>
<div class="div15 marg1"><label for="sitecon" >Site Contact<input type="text" name="site_con" id="site_con" class="required"  value="$this->siteCon" $DISABLED /></label></div>
<div class="div9 marg1" ><label for="contact_fone" >Phone<input name="contact_fone" id="contact_fone" value="$this->contactFone" type="text"  $DISABLED /></label></div>
<div class="div35 marg1" ><label for="landowner" >Landowner<input type="text" name="landowner" id="landowner" class="required"  value="$this->landOwner" $DISABLED /></label></div>
<div style="clear:both;" ></div>
<div class="div17" style="width:17.25%;" ><label for="area" >Area<input type="text" name="area" id="area" class="required" value="$this->areaName" $DISABLED /></label></div>
<div class="div15 marg"  ><div id="welldiv" >
FIN;
         $content .= AdminFunctions::wellSelectMulti($this->areaID,$this->wellIDs);
$content .= <<<FIN
</div></div>
<div class="div11 marg1" ><label for="meeting"  >Meeting<input type="text" name="meeting" id="meeting" class="required cntr"  value="$this->meeting" $DISABLED /></label></div>
<div class="div9 marg" style="margin-left:4.95%;" ><label for="expire"  >Expires<input type="text" name="expire" id="expire" class="date required"  value="$this->expires" $DISABLED /></label></div>
<div class="div35 marg1" > <label for="notes"  >Notes<textarea  name="notes" id="notes" class="txtnotes" $DISABLED >$this->notes</textarea></label></div>
</fieldset>
<fieldset ><legend style="margin-left:47%;">Stakeholders</legend>
<div id="contactdiv" >
FIN;
	 if ($this->action != "new" ) {
		$content .= $this->contactDiv($DISABLED);
	 }

	 $content .= "</div>\n";
	 $content .= "<div style=\"clear:both;\" ></div>\n";

	 if ($DISABLED != "disabled" ) {
         $content .= "<button class=\"button tiny\"  onClick=\"addContact();return false;\">Add Contact</button>";
    }

	 	$content .= "</fieldset>";
		//  set color of  heading  to status
		if ($this->countDoc * 1 == $this->sumDoc) {
			$this->statColor = "redd";
		} 
		else if ($this->countDoc * 3 == $this->sumDoc &&   $this->emailSent) {
			$this->statColor = "grn";
		}
		else {
			$this->statColor="oran";
		}
		$content .= <<<FIN
			<fieldset ><legend style="margin-left:47%;">Documents</legend>
<div id="uploads" >
	<div class="heading $this->statColor"  >
	<div class="addr hd bdl bdb nopad"   >Upload Files</div>
	<div class="addr hd bdb nopad"    >Document Title</div>
	<div class="well wd hd bdb nopad"   >Previous Files</div>
	<div class="addr hd bdb bdr"  style="width:300px;"   >Status</div>
	</div>
FIN;
   	$content .= $this->documentDiv($DISABLED);
   	$content .= "</div> <!-- should close uploads --> ";
		if ($DISABLED != "disabled" ) {
        		$content .= "<button class=\"button tiny margt4\"  onClick=\"addDocument();return false;\">Add Document</button>";
    	}
		$content .= "</fieldset>";
   	$content .= "<div style=\"clear:both;\" ></div>\n";

		if ($this->action != "find" ) {
  			$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found atw
			$content .= "\n<a href=\"atw.php?action=list&redo=redo\"   class=\"button\"  >Last Screen</a>";
		}
		else {
			 $content .= "\n<a href=\"atw.php?action=list&redo=redo\"   class=\"button\"  >Last Screen</a>";
		}
		$content .= <<<FIN
<script>
$("#atw").submit(function(){
    $('#SUBMIT').prop("disabled", "disabled");
});
$(".atwform").submit(function(){
    var isFormValid = true;
    $("#atwform input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
jQuery('#atw_number').keyup(function () { this.value = this.value.replace(/[^0-9]/g,''); });
$(function(){
   $(".input.date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_m',$('#welldiv'),'#contractorID'); return false; }
      });
  });
$(function(){
   $("#meeting").datetimepicker({
		dateFormat: 'dd-mm-yy',
		onSelect: function(valueText,inst){
          $('#expire').val(addmonths(valueText,1));
		}
	});
});
</script>
FIN;
				 $content .= "\n</form>\n";
			}
			else {
				$content = "";
			}
			$content .= "</div>\n"; // Center Div
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}

		private function getAllATWs($search,$redo) {
         $this->creDate = date('d-m-Y');
         $_SESSION['last_request'] = "{$this->URL}/admin/atw.php?action=list&redo=redo";  // Mail  bounce back
         $this->equalgreater = "SELECTED=\"selected\"";
			$this->effectiveFrom = NULL;
         $whereClause=" where a.removed is false ";
         if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }

         }
         if (!  empty($search)) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
				if (!empty($search->ID)) {
               $this->atwNo = $search->ID;
               $whereClause .= " and a.atw_number::integer = $this->atwNo ";
            }
		 		else if (isset($search->creDate) && strlen($search->creDate) > 0) {
             	$dateSelected = true;
             	$whereClause .= " and meeting_time::date ";
             	$this->creDate = $search->creDate;
             	switch ($search->dateType) {
             	case "equalto":
                	$whereClause .= "  =  '$search->creDate' ";
                	$this->equalto = "SELECTED=\"selected\"";
                	$this->equalgreater = NULL;
               	 break;
             	case "equalgreater":
               	$whereClause .= "  >=  '$search->creDate' ";
                	$this->equalgreater = "SELECTED=\"selected\"";
                	break;
             	case "less":
                	$whereClause .= "  <  '$search->creDate' ";
                	$this->less = "SELECTED=\"selected\"";
             	break;
             	case "between":
            		if (strlen($search->endDate) > 0 ) {
                      $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                      $this->between = "SELECTED=\"selected\"";
                      $this->endDate = $search->endDate;
                      $this->hideEndDate='';
                      $this->equalgreater = NULL;
                  }
                  else {
                     $whereClause .= "  =  '$search->creDate' ";
                  }
                  break;
              	default:
                  $whereClause = " where a.removed is false and meeting_time::date >= current_date ";
                  //$this->equalgreater = "SELECTED=\"selected\"";
                  break;
              }
				}
				if (isset($search->contractorID) && $search->contractorID > 0 ) {
           		$whereClause .= " and $search->contractorID in (select (unnest(string_to_array(a.contractor_ids::text,'|')::int[])))";
           		$this->contractorID = $search->contractorID;
         	}
			}
			else {
             // Default query still needs search object serialized
            $whereClause = " where a.removed is false and meeting_time::date >= current_date ";
            //$this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            //$search->dateType = "equalgreater";
            $search->creDate = date('d-m-Y');
         }


			$sql = "SELECT  a.*,el.fault,contacts_from_ids(a.contact_ids) as contacts,contractors_from_ids(a.contractor_ids) as contractors,employees_from_ids(a.employee_ids) as employees from atw a 
			 LEFT JOIN email_log el on el.email_log_id = 
            ( select max(email_log_id) from email_log  ell where a.atw_id  = ell.dkt_id and receiver_type = 'ATW' )
         	$whereClause order by create_date";

			
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}
			$content ="<div style=\"width:1762px;margin:auto;\">";
			 $content .= <<<FIN
         <form name="atw" method="post" action="atw.php?action=list">
         <input type="hidden" name="atwID" id="atwID" value="$this->atwID" />
         <input type="hidden"  id="emp_id" value="$this->empID" />
<fieldset ><legend style="margin-left:45%;">Filter Search - ATW's</legend>
<div class="div10"  ><label for="atwno" >ATW No.<input type="text" name="atwno" id="atwno" value="$this->atwNo" /></label></div>
<div class="div15 marg">
FIN;
			$content .= AdminFunctions::contractorSelect($this->contractorID,"",false,"contractor",true);
			$content .= <<<FIN
</div>
<div class="div10 marg" ><label for="datetype"  >Meeting Date is:</label><select class="sel" name="datetype" id="datetype"  onchange="displayDiv('#datetype');">
<option value="equalgreater" $this->equalgreater >Equal or greater</option>
<option value="equalto" $this->equalto >Equal to</option>
<option value="between" $this->between >Between</option>
<option value="less" $this->less >Less than</option>
</select></div>
<div class="div7 marg" ><label for="cre_date" >Date<input type="text" name="cre_date" id="cre_date" class="date" value="$this->creDate" /></label></div>
<div class="div7 marg" id="enddiv" $this->hideEndDate ><label for="end_date" >and Date<input type="text" name="end_date" id="end_date" class="date" value="$this->endDate" /><label></div>
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});

$(function () {
      $('#atwno').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=atwno',
         minChars:1,
      });
  });
</script>
FIN;
 			$content .= "<div style=\"float:right;width:14%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
         //$content .= "<a href=\"hours.php?action=list&search=true&printcsv=true\" class=\"submitbutton printcsv\" >Print C.S.V.</a>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" >\n";
         $content .= "<div class=\"rate hd\">ATW No</div>";
         $content .= "<div class=\"usr hd\">Meeting Time</div>";
			$content .= "<div class=\"email hd\">Location</div>\n";
			$content .= "<div class=\"cli hd\">Site Contact</div>\n";
			$content .= "<div class=\"email hd\">Landowner</div>\n";
			$content .= "<div class=\"cli hd\">Contractors</div>\n";
			$content .= "<div class=\"cli wd  hd\">Employees</div>\n";
			$content .= "<div class=\"email cont hd\">Contacts</div>\n";
			$content .= "<div class=\"links hd\" style=\"width:165px;\" >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$emailButTxt = "Email";
            $emailButClass = "toemail";
				$showEmail = false;
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$atwID = $rs->fields['atw_id'];
				$atwNo = $rs->fields['atw_number'];
         	$meeting = ucwords(strtolower($rs->fields['meeting_time']));
				$meet = AdminFunctions::dbDate(substr($meeting,0,10)) . " " .substr($meeting,11,5);
				$creDate = AdminFunctions::dbDate($rs->fields['create_date']);
				$landOwner =  ucwords(strtolower($rs->fields['landowner']));
				$siteCon =  ucwords(strtolower($rs->fields['site_contact']));
				$address =  ucwords(strtolower($rs->fields['property_address']));
				$contractors =  $rs->fields['contractors'];
				$employees =  $rs->fields['employees'];
				$contacts =  substr(ucwords(strtolower($rs->fields['contacts'])),0,185);
				// document status
				$sql = "SELECT * from atw_status($atwID)";
				if (! $data = $this->conn->getRow($sql)) {
					die($this->conn->ErrorMsg());
				}
				if($rs->fields['fault'] == "OK" ) {
					$emailButTxt = "Emailed";
               $emailButClass = "emailed";
					$showEmail = true;
				}
				if (($data['sum_stat'] / 2 == $data['cnt_doc'] ) || ($data['sum_stat'] / 3 == $data['cnt_doc']) ) {  // reshow  emailed
					$showEmail = true;
				}
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				//$content .= "<a href=\"atw.php?action=find&atw_id=$atwID\" >\n";
         	$content .= "<div class=\"rate highh $ln\">$atwNo</div>";
         	$content .= "<div class=\"usr highh cntr $ln\">$meet</div>";
				$content .= "<div class=\"email highh $ln\">$address</div>\n";
				$content .= "<div class=\"cli highh $ln\">$siteCon</div>\n";
				$content .= "<div class=\"email highh $ln\">$landOwner</div>\n";
				$content .= "<div class=\"cli highh  $ln\">$contractors</div>\n";
				$content .= "<div class=\"cli wd highh  $ln\">$employees</div>\n";
				$content .= "<div class=\"email highh  $ln\">$contacts</div>\n";
				//$content .= "</a>\n";


				if ($this->sessionProfile > 3 ) {
           		$content .= "<div class=\"links highh $ln\" style=\"width:165px;text-align:center;\"  >\n";
					$content .= "<div style=\"margin:auto;width:150px;\" >\n";
					$content .= "<a href=\"atw.php?action=update&atw_id=$atwID\" title=\"Edit ATW\" class=\"linkbutton\" style=\"min-width:39px !important;height:22px;line-height:22px;\"  >Edit</a>\n";
					$content .= "<a href=\"atw.php?action=delete&atw_id=$atwID\" title=\"Delete ATW\" class=\"linkbutton bckred\"  style=\"height:22px;line-height:22px;\"  onclick=\"return confirmDelete();\" >Delete</a>";
					if ($showEmail) {
				 		$content .= "<button  class=\"linkbutton $emailButClass\" style=\"min-width:44px !important;height:22px;\"  onclick=\"emailATW($atwID,$(this));$(this).prop('disabled','disabled');return false;\">$emailButTxt</button>";
					}
				}
				else {   
			      $content .= "<div class=\"links $ln\"  style=\"width:170px;\" > &nbsp;";
				}
			
				$content .= "</div></div></div>\n";
				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";

          $_SESSION['last_request'] = "{$this->URL}/admin/atw.php?action=list&redo=redo";  // Mail  bounce back


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getATWDetails($atwID) {
			$sql = "SELECT at.*,a.area_name,wells_from_ids(at.well_ids) as well_name  from atw at
			 LEFT JOIN area a using(area_id)
			 where atw_id = $atwID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->atwID = $data['atw_id'];
			$this->atwNo = $data['atw_number'];
			$this->atwAddress = $data['property_address'];
			$this->siteCon = $data['site_contact'];
			$this->landOwner = $data['landowner'];
			$this->wellStr = $data['well_name'];
         $this->wellIDs = $data['well_ids'];
         $this->areaName = $data['area_name'];
         $this->areaID = $data['area_id'];
         $meeting = $data['meeting_time'];
			$this->meeting = AdminFunctions::dbDate(substr($meeting,0,10)) . " " .substr($meeting,11,5);
			$this->expires = AdminFunctions::dbDate($data['expire_date']);
         $this->contactFone = $data['site_contact_fone'];
			$this->notes = $data['notes'];
			$this->emailConArr = explode("|",$data['to_email_contacts']);
			$this->emailContrArr = explode("|",$data['to_email_contractors']);
			$this->emailEmpArr = explode("|",$data['to_email_employees']);

			// contacts
			$this->contactIDs = $data['contact_ids'];
			if (strlen($this->contactIDs) > 0 ) {
				$sql = "SELECT * from contactall_from_ids('$this->contactIDs')"; 
				if (! $this->contactArr = $this->conn->getAll($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->conn->ErrorMsg());
					}
				}
			}
			// contractors
			$this->conIDs = $data['contractor_ids'];
			if (strlen($this->conIDs) > 0 ) {
				$sql = "SELECT * from contractorall_from_ids('$this->conIDs')"; 
				if (! $conArr = $this->conn->getAll($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->conn->ErrorMsg());
					}
				}
				else {
					$this->contactArr = array_merge($this->contactArr,$conArr);
				}
			}
			// employees
			$this->empIDs = $data['employee_ids'];
			if (strlen($this->empIDs) > 0 ) {
				$sql = "SELECT * from employeeall_from_ids('$this->empIDs')"; 
				if (! $empArr = $this->conn->getAll($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->conn->ErrorMsg());
					}
				}
				else {
					$this->contactArr = array_merge($this->contactArr,$empArr);
				}
			}
			// Documents
			$sql = "SELECT * from atw_document where atw_id = $this->atwID order by atw_type_id";
			if (! $this->documentLines = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			$this->countDoc = count($this->documentLines);
			$sql = "SELECT sum(status)  from atw_document where atw_id = $this->atwID";
			if (! $this->sumDoc = $this->conn->getOne($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
				else {
					$this->sumDoc = 0;
				}
			}

			 $sql =  "select max(email_log_id) from email_log  ell where ell.dkt_id = $this->atwID and receiver_type = 'ATW' and fault = 'OK' "; 
			if (! $res = $this->conn->getOne($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
				else {
					$this->emailSent = false;
				}
			}
			else {
				$this->emailSent = true;
			}

		}
		private function processPost() {
			$contrArr = $conArr = $empArr = array();
			$this->updateWell = "";
         if ($this->action == "delete" ) {
				//$this->contact->deleteContacts($this->atwID);
				$this->deleteATW();
			}
			$this->empID = $_POST['empID'];
			$this->atwID = $_POST['atw_id'];
			$this->atwNo = $_POST['atw_number'];
			$this->atwAddress = trim(addslashes($_POST['address']));
			$this->landOwner = trim(addslashes($_POST['landowner']));
			$this->siteCon = trim(addslashes($_POST['site_con']));
			$this->contactIDs =  isset($_POST['contact_id']) ?  $_POST['contact_id'] :NULL;
			$this->selType =  isset($_POST['seltype']) ?  $_POST['seltype'] :NULL;
			$this->conFirst =  isset($_POST['con_first']) ?  $_POST['con_first'] :NULL;
			$this->conLast =  isset($_POST['con_last']) ?  $_POST['con_last'] : NULL;
			$this->conEmail =  isset($_POST['con_email']) ?  $_POST['con_email'] : NULL;
			$this->emailSent = $_POST['emailSent'];
			//$this->conExpire = isset($_POST['con_exp']) ?  $_POST['con_exp'] : NULL;
			if (! is_null($this->contactIDs) ) {
				foreach($this->contactIDs as $ind=>$val) {
					switch($this->selType[$ind]) {
						case "EMP":
						$empArr[] = $val;
						$nm="emailit_$ind";
			   		$this->emailEmpArr[] = isset($_POST[$nm]) ? $val  :NULL ;
						break;
						case "CON":
						$conArr[] = $val;
						$nm="emailit_$ind";
			   		$this->emailContrArr[] = isset($_POST[$nm]) ? $val  :NULL ;
						break;
						case "EXT":
						$contrArr[] = $val;
						$nm="emailit_$ind";
			   		$this->emailConArr[] = isset($_POST[$nm]) ? $val  :NULL ;
						break;
					}
				}
			}
			$this->conIDs = count($conArr) > 0  ? implode("|",$conArr) : "";
			$this->empIDs = count($empArr) > 0  ? implode("|",$empArr) : "";
			$this->areaID = $_POST['areaID'];
			$this->notes = trim(addslashes($_POST['notes']));
			if (isset($_POST['well'])) {  // no well in update
				$well = $_POST['well'];
				$this->wellID = count($well) == 1 ? intval($well[0]) : "NULL";  // option for one well
         	$this->wellStr = implode("|",$well);
				$this->updateWell = ",well_ids='$this->wellStr'";
			}

		 	$this->contactFone = trim($_POST['contact_fone']);
			$this->meeting = $_POST['meeting'];
			$this->expires = $_POST['expire'];

			
			$this->submitForm();
		}
		private function deleteATW() {
			$sql = "UPDATE atw set removed = TRUE where atw_id = $this->atwID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: {$this->URL}/admin/atw.php?action=list&redo=redo");
			exit;
		}
		private function submitForm() {
			if ($this->atwID == 0 ) {
				$sql = "INSERT into atw (atw_number,property_address,site_contact,landowner,employee_id,contractor_ids,employee_ids,expire_date,area_id,well_ids,meeting_time,site_contact_fone,notes) 
				values ($this->atwNo,E'$this->atwAddress',E'$this->siteCon',E'$this->landOwner',$this->empID,'$this->conIDs','$this->empIDs','$this->expires',$this->areaID,
				'$this->wellStr','$this->meeting','$this->contactFone',E'$this->notes') returning atw_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
				$this->atwID = $rs->fields['atw_id'];

			}
			else { // Update
				$sql = "UPDATE atw set atw_number = $this->atwNo,property_address = E'$this->atwAddress',site_contact=E'$this->siteCon',landowner=E'$this->landOwner',employee_id = $this->empID,
				contractor_ids = '$this->conIDs',employee_ids = '$this->empIDs',area_id = $this->areaID $this->updateWell ,expire_date = '$this->expires',
				meeting_time='$this->meeting',site_contact_fone='$this->contactFone',notes = E'$this->notes'  where atw_id = $this->atwID";
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}

			}
			// contacts
			$contactIDstring = $emailConStr = $emailContrStr = $emailEmpStr = "";
			if (! is_null($this->conFirst)) {
				$contactIDstring = $this->submitContacts();   // make  new  Contacts  as employees  or UPDATE
			}
			if (strlen($contactIDstring) > 0 ) {
				$contactIDstring = preg_replace('/\|$/',"",$contactIDstring);
				$conArr = explode("|",$contactIDstring);
				foreach($this->emailConArr as $ind=>$id) {
					if (!is_null($id)) {
						$emailConStr .= $conArr[$ind] . "|";
					}
				}
				$emailConStr = preg_replace('/\|$/',"",$emailConStr);  //  which  should or should not be  emailed in this  update
			}
			// email  contractors ??
			if ($this->conIDs != "") {
				foreach($this->emailContrArr as $ind=>$id) {
					if (!is_null($id)) {
						$emailContrStr .= $id . "|";
					}
				}
				$emailContrStr = preg_replace('/\|$/',"",$emailContrStr);  //  which  should or should not be  emailed in this  update

			}
			// email  employees ??
			if ($this->empIDs != "") {
				foreach($this->emailEmpArr as $ind=>$id) {
					if (!is_null($id)) {
						$emailEmpStr .= $id . "|";
					}
				}
				$emailEmpStr = preg_replace('/\|$/',"",$emailEmpStr);  //  which  should or should not be  emailed in this  update

			}
			//$this->empIDs = count($empArr) > 0  ? implode("|",$empArr) : "";
				$sql = "UPDATE atw set contact_ids = '$contactIDstring',to_email_contacts = '$emailConStr',to_email_employees = '$emailEmpStr',to_email_contractors = '$emailContrStr'
				  where atw_id = $this->atwID";
				if (! $res=$this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			// Document Uploads
			$docs = $_FILES['document'];
         $atwTypeIDs = $_POST['atwtypeid'];
			$lineDocs = $_POST['linedoc'];  // gives  array indexes
			$docTypes = $_POST['doctype'];  // Other  uses this  as text
			$atwDocIDs = $_POST['atw_doc_id'];  //  for  Updates
			$prevFiles = !empty($_POST['prevFile']) ? $_POST['prevFile'] : NULL;
			//  status_$x of  Radio Buttons ==  'draft'  or  'ready'  
			$docFound = false;
         //$pcount = count($docs['name']);
         $pcount = count($atwDocIDs);
         $sqlinsdocs = "INSERT into atw_document (atw_id,atw_type_id,document_type,document_name,status,upload_emp_id,document_date ) values ";
         for ($ind = 0;$ind < $pcount;$ind ++) {
				$atwDocID = $atwDocIDs[$ind];
				$status = 0;  // Unknown
            $atwType = $atwTypeIDs[$ind];
            $dType = pg_escape_string($docTypes[$ind]);
				$doc_name = !empty($prevFiles[$ind]) ? pg_escape_string($prevFiles[$ind]) : "";
				$stat = "status_".$lineDocs[$ind];
				if (isset($_POST[$stat]) ) {
					 $status =  $_POST[$stat] == 'draft' ? 1 : 2; 
				}

            if (isset($docs['tmp_name'][$ind]) && strlen($docs['tmp_name'][$ind]) > 0 )  {
               $doc_name = "atw/$this->atwID" ."_" .AdminFunctions::cleanURL($docs['name'][$ind] );
               if (! ($result = AdminFunctions::processImages($docs,$ind,$doc_name)) == "OK") {
                  die("Error in processing uploads - $result");
               }
				}

			   $status =  strlen($doc_name) > 0  ? $status : 1;   // cannot  set status if no document
				if ($this->emailSent == 1 && $status == 2 ) {  // true
			   	$status =  3;   //  update  after  email  can happen  
				}

				if ($atwDocID == 0 && strlen($doc_name) > 0) { // insert
              	$docFound = true;
              	$sqlinsdocs .= "($this->atwID,$atwType,E'$dType',E'$doc_name',$status,$this->empID,current_date),";
				}
				else {  //Update
					$sql = "UPDATE atw_document set document_type = E'$dType',document_name = E'$doc_name',status = $status,upload_emp_id = $this->empID,document_date = current_date 
					WHERE atw_document_id = $atwDocID";
					//echo $sql;
           		if (! $rs =  $this->conn->Execute($sql)) {
              		die( $this->conn->ErrorMsg());
           		}
				}
        }
        if ($docFound ) {  // any photos ??
           $sqlinsdocs = preg_replace('/,$/',"",$sqlinsdocs);
           if (! $rs =  $this->conn->Execute($sqlinsdocs)) {
              die( $this->conn->ErrorMsg());
           }
        }

			header("LOCATION: {$this->URL}/admin/atw.php?action=list&redo=redo");
			exit;
			
		}
		public function submitContacts() {
			// need to create  psuedo  employee with password  for  login  and contact  record  Update  existing atw  record with new  contact  IDs
			$contactIDstring = "";
			$conFound = false;
			foreach ($this->conFirst as $ind=>$name) {
				if ($this->selType[$ind] == 'EXT' ) {
					$name = preg_replace('/\ */',"",$name);
					$contID = $this->contactIDs[$ind];
					$lastName = trim(pg_escape_string($this->conLast[$ind]));
					$last =preg_replace("/'/","", $this->conLast[$ind]);  // just in case we are  Irish
					$email = trim(pg_escape_string($this->conEmail[$ind]));
					//$expire = $this->conExpire[$ind];
					$userName = substr(strtolower($name),0,3) . substr(strtolower($last),0,3) . "456";
					$passwd = md5($userName);
			   	if (intval($contID) == 0 ) {   	
						$conFound = true;  //only set for inserts
						$sql = "INSERT into employee (username,password,firstname,lastname,removed,profile_id,company_id,emp_type,last_login) 
						values  ('$userName','$passwd','$name',E'$lastName','FALSE',3,1,'EXT',current_date) returning employee_id";
						if (! $res=$this->conn->Execute($sql)) {
							die($this->conn->ErrorMsg());
						}
						$empID = $res->fields['employee_id'];

						$sql = "INSERT into contact(atw_id,employee_id,contact_firstname,contact_lastname,contact_email,removed) 
						values ($this->atwID,$empID,'$name',E'$lastName',E'$email','FALSE') returning contact_id";
						if (! $res=$this->conn->Execute($sql)) {
							die($this->conn->ErrorMsg());
						}
						$contactIDstring .= $res->fields['contact_id'] ."|";
					}
					else {  // set  string from  existing contacts
						$contactIDstring .= $contID ."|";
						$sql = "UPDATE contact set contact_firstname = '$name',contact_lastname = E'$lastName',contact_email='$email' WHERE contact_id = $contID";
					
						if (! $res=$this->conn->Execute($sql)) {
							die($this->conn->ErrorMsg());
						}  
					} 
				}
			}
			return $contactIDstring;
		}
		public function getDocLine($ind) {
			if ($this->action == "new" ) {
						$this->docID = 0;
						$this->docPrevFile = "";
						$this->docStatus = 1;
						$this->docATWTypeID = $ind;
				return true;
			}
		 	else  {	// Update Find	
			  	if (count($this->documentLines) > 0 ) {
					$this->docID = isset($this->documentLines[$ind-1]['atw_document_id']) ? $this->documentLines[$ind-1]['atw_document_id'] : 0;
					$this->docPrevFile = isset($this->documentLines[$ind-1]['document_name']) ? $this->documentLines[$ind-1]['document_name'] : "" ;
					$this->docStatus = isset($this->documentLines[$ind-1]['status']) ? $this->documentLines[$ind-1]['status'] : 1;
					$this->docATWTypeID = $ind ;
			 	}
				else {
						$this->docID = 0;
						$this->docPrevFile = "";
						$this->docStatus = 1;
						$this->docATWTypeID = $ind;
				}
			}
		}
		public function documentDiv($DISABLED) {
         $html = "";
			//$x=0;
			if ($this->action != "new"  && count($this->documentLines) > 0 ) {
				$x = 1;
         	foreach($this->documentLines as $ind=>$val) {
					$docID = $val['atw_document_id'];
					$typeID = $val['atw_type_id'];
					$docType = $val['document_type'];
					$status = intval($val['status']);
					$docName = $val['document_name'];
					$upname = basename($docName);
 
				//$this->docStatus = $this->emailSent ? 3 : $this->docStatus;
				$statusStyle="style=\"background:red;\" ";
				$draftChecked = "checked";
				$readyChecked =  "";
            $uplink = "<input type=\"hidden\" name=\"prevFile[]\" value=\"$docName\" />";
            if($docName != "") {
					$upname = basename($docName);
               $uplink .= "<a href=\"$docName\" target=\"_blank\" style=\"margin: 0.7rem 0.6rem;display:block; \"  >$upname</a>";
				}
				switch ($status) {
					case 1:
						$classStyle="redd";
						$draftChecked = "checked";
						$readyChecked =  "";
						break;
					case 2:
						$classStyle="oran";
						$readyChecked = "checked";
						$draftChecked = "";
						break;
					case 3:
						$classStyle="grn";
						$draftChecked = "";
						$readyChecked = "checked";
						break;
					default:
						$draftChecked = "checked";
						$readyChecked =  "";
						break;
				}
            $html .= "<div id=\"document_div_$x\" class=\"docdiv\" >\n";
            $html .= "<input type=\"hidden\" value=\"$typeID\" name=\"atwtypeid[]\" id=\"atwtypeid_$x\" /><input type=\"hidden\" value=\"$x\" id=\"linedoc\" name=\"linedoc[]\" />\n";
				$html .= "<input type=\"hidden\" value=\"$docID\" name=\"atw_doc_id[]\"  />\n";
            $html .= "<div style=\"clear:both;\" ></div>\n";
            $html .= "<div class=\"addr bdb nopad higher\" ><input type=\"file\"  class=\"higher\" name=\"document[]\"  $DISABLED /></div>\n";
            $html .= "<div class=\"addr bdb nopad higher\"   ><input type=\"text\" class=\"higher\"   name=\"doctype[]\"  id=\"doctype_$x\" value=\"$docType\" $DISABLED /></div>\n";
            $html .= "<div class=\"well wd bdb nopad higher\"  >$uplink</div>\n";
            $html .= "<div class=\"addr bdb bdr higher $classStyle\"   style=\"width:300px;\"  id=\"divstatus_$x\"  >\n";
				$html .= "<div class=\"div25 margt5\" ><span>Draft</span><input type=\"radio\" name=\"status_$x\"  id=\"status_$x\" value=\"draft\" $draftChecked  onclick=\"setStatus($x);\" $DISABLED /></div>";
            $html .= "<div class=\"div55 margt5\" ><span>Ready to Release</span><input type=\"radio\" name=\"status_$x\"  id=\"status_$x\" value=\"ready\" $readyChecked  onclick=\"setStatus($x);\" $DISABLED  /></div>";
            $html .= "<div class=\"div20\" ><button class=\"linkbutton delatw\"  onclick=\"delDoc($docID, '#document_div_$x" ."');return false;\" $DISABLED  >Delete</button></div>\n";
				$html .= "</div></div>\n";
            $html .= "<div style=\"clear:both;\" ></div>\n";
				$html .= <<<FIN
<script>
$(function () {
     $('#doctype_$x').autocomplete({
      width: 400,
      serviceUrl:'autocomplete.php?type=atwtype',
      minChars:1,
      onSelect: function(value, data){ $('#atwtypeid_$x').val(data);}
      });
  });

</script>
FIN;
				$x++;
         }
		}
		else {  // new
            $html .= "<div id=\"document_div_1\" class=\"docdiv\" >\n";
            $html .= "<input type=\"hidden\" value=\"6\" name=\"atwtypeid[]\" id=\"atwtypeid_1\" /><input type=\"hidden\" value=\"1\" id=\"linedoc\" name=\"linedoc[]\" />\n";
				$html .= "<input type=\"hidden\" value=\"0\" name=\"atw_doc_id[]\"  />\n";
            $html .= "<div style=\"clear:both;\" ></div>\n";
            $html .= "<div class=\"addr bdb nopad higher \" ><input type=\"file\"   class=\"higher\" name=\"document[]\"  /></div>\n";
            $html .= "<div class=\"addr bdb nopad higher \"  ><input type=\"text\"  class=\"higher\"  name=\"doctype[]\"  id=\"doctype_1\"  /></div>\n";
            $html .= "<div class=\"well wd bdb nopad  higher \"  >&nbsp;</div>\n";
            $html .= "<div class=\"addr bdb bdr higher redd\" style=\"width:300px;\" id=\"divstatus_1\"  >\n";
				$html .= "<div class=\"div25 margt5\" ><span>Draft</span><input type=\"radio\" name=\"status_1\"  id=\"status_1\" value=\"draft\" checked  onclick=\"setStatus(1);\" /></div>";
				$html .= "<div class=\"div55 margt5\" ><span>Ready to Release</span><input type=\"radio\" name=\"status_1\"  id=\"status_1\" value=\"ready\"   onclick=\"setStatus(1);\" /></div>";
				$html .= "<div class=\"div20\" ><button class=\"linkbutton delatw\"  onclick=\"delDoc(0, '#document_div_1" ."');return false;\"  >Delete</button></div>\n";  
				$html .= "</div></div>\n";
            $html .= "<div style=\"clear:both;\" ></div>\n";
				$html .= <<<FIN
<script>
$(function () {
     $('#doctype_1').autocomplete({
      width: 400,
      serviceUrl:'autocomplete.php?type=atwtype',
      minChars:1,
      onSelect: function(value, data){ $('#atwtypeid_1').val(data);}
      });
  });

</script>
FIN;
		}

         return $html;
   }

		public function contactDiv($DISABLED) {
			$html = "";
			$x = 0;
			if (count($this->contactArr) > 0 ) {
				foreach ($this->contactArr as $ind=>$val) {
					$ln = $x % 2 == 0 ? "line1" : "line2" ;
					$CHECKED="";
					$contID = intval($val['contact_id']);
					if (in_array($contID,$this->emailConArr)) {
						$CHECKED="checked";
					}
					else if  (in_array($contID,$this->emailContrArr)) {
						$CHECKED="checked";
					}
					else if (in_array($contID,$this->emailEmpArr)) {
						$CHECKED="checked";
					}
					$first = $val['first_name'];
					$seltype = $val['seltype'];
					$last = $val['last_name'];
					$email = $val['email'];
					$expires = AdminFunctions::dbDate($val['expires']);
					$html .= "<div id=\"conline_$x\" >\n";
   				$html .= "<div style=\"clear:both;\"></div><input type=\"hidden\" value=\"$contID\" name=\"contact_id[]\" id=\"con_id_$x\" /><input type=\"hidden\" value=\"$x\" id=\"linecon\"  />\n";
					$html .= "<input type=\"hidden\"  name=\"seltype[]\" id=\"seltype_$x\"  value=\"$seltype\" />\n";
   				$html .= "<div class=\"div29\" ><label  >First Name<input type=\"text\" name=\"con_first[]\" id=\"con_first\" value=\"$first\" class=\"required\" $DISABLED  /></label></div>\n";
   				$html .= "<div class=\"div29 marg1\" ><label  >Last Name<input type=\"text\" name=\"con_last[]\" id=\"con_last\" value=\"$last\" class=\"required\"  $DISABLED /></label></div>\n";
   				$html .= "<div class=\"div29 marg1\" ><label >Email<input type=\"text\" name=\"con_email[]\" id=\"con_email\" value=\"$email\" class=\"required\" $DISABLED  /></label></div>\n";
   				$html .= "<div class=\"div10 marg1\">\n";
   				$html .= "<div class=\"div70\" ><label for=\"emailit_$x\" class=\"label med\" >Email Contact</label><input type=\"checkbox\" name=\"emailit_$x\" id=\"emailit_$x\"    $CHECKED $DISABLED  /></div>\n";
					$html .= "<div class=\"div30\" >";
					if ($this->action != "find" ) {
   					$html .= "<button class=\"linkbutton delatw\"  onclick=\"deldiv($x,$contID,$this->atwID);return false;\">Delete</button>\n";
					}
   				$html .= "</div></div>\n";
					$html .="</div>\n";
					$x++;
				}
			}	
			return $html;
		}

	}
?>
