<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Upload  {
		public $page;
		public $action;
		public $hDate;
		public $uploadType;
		public $contractorID;
		public $equalto;
      public $between;
      public $hideEndDate='style="display:none;"';
      public $equalgreater;
		public $uploadDate;
		public $endDate;
		public $contractorName;
      public $shortName;
      public $conDomain;
      public $conName;
		public $less;


		public function	__construct($action,$type="timesheet",$conID=NULL,$hDate=NULL,$search="") {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->uploadType=$type;
			$this->conn = $GLOBALS['conn'];
			$this->contractorID = $conID;
			if (! is_null($conID)) {
            $connArr = AdminFunctions::getConName($conID);
            $this->contractorName = $connArr['con_name'];
            $this->shortName = $connArr['name'];
            $this->conDomain = $connArr['domain'];
            $this->conName = $connArr['con_name'];
         }
			$pg = new Page('upload');
			$this->page= $pg->page;
			if ($action == "list" ) {
				$this->hDate = !is_null($hDate) ? $hDate : date('d-m-Y',time() - (86400 * 14));
				$heading_text = "List Timesheets: $this->conName";
				$this->setHeaderText($heading_text);
		 		$this->getAllTimesheets($search,NULL);	

			}
			echo $this->page;

		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}
		private function getAllTimesheets($search=NULL,$redo=NULL) {
			$this->equalgreater = "SELECTED=\"selected\"";
         //$whereClause="";
         if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
         if (! is_null($search) && $search) {
				if (strlen($search->creDate) > 0) {
                $dateSelected = true;
                $whereClause = " WHERE h_date ";
                $this->uploadDate = $search->creDate;
                $this->hDate = $search->creDate;
                switch ($search->dateType) {
                case "equalto":
                   $whereClause .= "  =  '$search->creDate' ";
                   $this->equalto = "SELECTED=\"selected\"";
                   $this->equalgreater = NULL;
                   break;
                case "equalgreater":
                   $whereClause .= "  >=  '$search->creDate' ";
                   $this->equalgreater = "SELECTED=\"selected\"";
                   break;
					 case "less":
                    $whereClause .= "  <  '$search->creDate' ";
                    $this->less = "SELECTED=\"selected\"";
                    break;
                case "between":
                   if (strlen($search->endDate) > 0 ) {
                      $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                      $this->between = "SELECTED=\"selected\"";
                      $this->endDate = $search->endDate;
                      $this->hideEndDate='';
                      $this->equalgreater = NULL;
                   }
                   else {
                      $whereClause = "  =  '$search->creDate' ";
                   }
                   break;
                   default:
                     $whereClause = " WHERE h_date  >=  current_date  - interval '2 weeks' ";
                     $this->equalgreater = "SELECTED=\"selected\"";
                     break;
               }
				}
			}
			else {
             // Default query still needs search object serialized
            $this->uploadDate = date ('d-m-Y',time() - (86400 * 14));
            $whereClause = " WHERE h_date >= current_date - interval '2 weeks'";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new Search();
            $search->dateType = "equalgreater";
            $search->creDate = $this->uploadDate;
         }
			if(! is_null($this->contractorID)) {  
         	$_SESSION['SQL'] = pg_escape_string(serialize($search));
         	$sql = "SELECT  *  from {$this->shortName}_upload 
         	$whereClause
            and upload_type_id !=  7
         	order by upload_date";
         	if (! $rs=$this->conn->Execute($sql)) {
            	if ($this->conn->ErrorNo() != 0 ) {
               	die($this->ErrorMsg());
            	}
         	}
         	$content ="<div style=\"width:850px;margin:auto;\" >\n";
         	$content  .= <<<FIN
         <form name="upload" method="post" action="upload.php?action=list">
			<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
         <fieldset ><legend style="margin-left:38%;">Filter Search - List Timesheets</legend>
FIN;
		 		$content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->hDate,$this->endDate,$this->hideEndDate,"WIDE");
$content .= <<<FIN
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
         	$content .= "<div style=\"float:right;width:33%;\" >\n";
         	$content .= "\n<input type=\"submit\" name=\"search\" value=\"search\"  class=\"button\"/>";
         	$content .= "\n<button  onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" class=\"marg\" >RESET</button>";
         	$content .= "</div></fieldset></form>\n";

				$content .= "<div class=\"heading\" >\n";
         	$content .= "<div class=\"usr hd\">Date For</div>\n";
         	$content .= "<div class=\"well hd\">Name</div>\n";
         	$content .= "<div class=\"links hd\"  style=\"width:265px;\" >Actions</div></div>\n";
				$lineNo = 0;
         	while (! $rs->EOF ) {
            	$ln= $lineNo % 2 == 0  ? "line1" : "line2";
            	$uploadID = $rs->fields['upload_id'];
            	$hDate = AdminFunctions::dbDate($rs->fields['h_date']);
            	$name = $rs->fields['upload_name'];
					$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
            	$content .= "<div class=\"usr $ln highh cntr\">$hDate</div>\n";
            	$content .= "<div class=\"well highh $ln\">$name</div>\n";
            	$content .= "<div class=\"links $ln highh cntr\"  style=\"width:265px;\" >\n";
					$content .="<div style=\"width:100px;margin:auto;\" ><a href=\"{$this->conDomain}/$name\" title=\"View Timesheet\" target=\"_blank\" class=\"linkbutton\" style=\"width:90px;\" >View Timesheet</a></div>\n";
					$content .= "</div>";
					$content .= "</div>";
            	$lineNo += 1;
            	$rs->MoveNext();
         	}
         	$content .= "<hr /></div>";
			}  // end  known  contractor  do search
         else { // ContractorID NULL show contrcator select
            $content = AdminFunctions::setContractor("upload.php?action=list",$this->contractorID);
         }
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	}
?>
