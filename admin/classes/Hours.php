<?php //QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Hours  {
		public $page;
		public $action;
		public $sessionProfile;
		public $hourID;
		public $plants;
		public $rates;
		public $areas;
		public $wells;
		public $crews;
		public $hours;
		public $subtot;
		public $coos;
		public $litres;
		public $cubic;
		public $details;
		public $operatorID=0;
		public $operatorName;
		public $crewID=0;
		public $subCrewID;
		public $plantID=0;
		public $unitID=0;
		public $areaID=0;
		public $wellID=0;
		public $callOffID;
		public $contractNum;
		public $areaName; // 0 Made not submitted  1 emailed  2 rejected  3  late hours   4  reply rejected   5 approved  6 submitted completion 7 approved completion  8 Invoiced  9 Paid  10 Some Rejected
		public $statusArr= array("0"=>"#e1b500","1"=>"#60854e","2"=>"#ef3e55","3"=>"#cc78ef","4"=>"#ef8e8e","5"=>"#cdefbd","6"=>"#d88a5a","7"=>"#b0a75a","8"=>"#c0dcef","9"=>"#b4b4b4","10"=>"#f1793d");
		public $status;
		public $hDate;
		public $dktHours;
		public $docketNo;
		public $rate;
		public $siteIns;
		public $total;
		public $conn;
		public $equalgreater;
		public $equalto; 
		public $between;
		public $less;
		public $hideEndDate='style="display:none;"';
		public $endDate;
		public $invoice;
		public $invoiceNum;
		public $totHours;
		public $employeeID;
		public $cooID;
		public $pType;
		public $contractorID = NULL;
		public $contractorName;
		public $shortName;
		public $machTag;
		public $conDomain;
		public $dbLink;
		public $dktFrom;
		public $dktTo;
		public $plantName;
		public $plantTypeID;
		public $unitName;


		public function	__construct($action,$hourID=0,$conID=NULL,$search=NULL,$redo=NULL,$order=NULL) {
			$this->action = $action;
			$this->hourID = $hourID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
			$this->conn = $GLOBALS['conn'];
			$this->contractorID = $conID;
			$pg = new Page('hours');
			$this->page= $pg->page;
			if (! is_null($conID)) {
				$connArr = AdminFunctions::getConName($conID);
				$this->contractorName = $connArr['con_name'];
				$this->shortName = $connArr['name'];
				$this->conDomain = $connArr['domain'];
				$this->dbLink = $connArr['db_link'];
			}
        	switch($action) {
				case "list" :
					$heading_text =   "List Hours: $this->contractorName";
					$this->getAllHours($search,$redo,$order) ;
					break;
				case "find" :
					$heading_text =   "Show Hours $this->hourID";
					$this->getHourDetails() ;
					break;
				
				default:
					$heading_text = "List Hours";
					break;
			}
		 	$this->setHeaderText($heading_text);	
		 	$this->setContent($action,$redo);	
			echo $this->page;
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$redo) {
			if ($action != "list" || ($action == "find" && $this->hourID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "No Button";
						break;
				}
			$content = <<<FIN
<div style="width:1800px;text-align:center;"><div style="width:1750px;margin:auto;">
<form name="hours" id="hours" method="post" action="hours.php">
<input type="hidden" name="action" value="$this->action" />
<fieldset style="margin-top:15px;"><legend style="margin-left:820px;">Operator</legend>
<label for="operator" class="label" >Operator</label><input type="text" name="operator" id="operator" value="$this->operatorName" class="input required $DISABLED"  $DISABLED  />
<label for="hdate" class="label sml">Date</label><input type="text" name="hdate" id="hdate" class="input required date  $DISABLED" value="$this->hDate" $DISABLED />
<label for="docketno" class="label"  >Docket</label><input type="text" name="docketno" id="docketno" class="input  required abn $DISABLED"  value="$this->docketNo" $DISABLED />
<label for="invoice_num" class="label sml"  >Invoice</label><input type="text" name="invoice_num" id="invoice_num" class="input sml DISABLED"  value="$this->invoice" DISABLED />
<div style="width:315px;float:left;"> &nbsp;</div>
<label for="dkthours" class="label"  >Total Hours</label><input type="text" name="dkthours" id="dkthours" class="input litres DISABLED"  value="$this->dktHours" DISABLED />
<label for="total" class="label abn"  >Total</label><input type="text" name="total" id="total" class="input litres DISABLED"  value="$this->total" DISABLED />
</fieldset>
FIN;
	$content .= <<<FIN
<fieldset style="margin-top:15px;"><legend style="margin-left:800px;">Docket Lines</legend>
<div id="linediv" >
<div id="line_1" >
<input type="hidden" id="lineno" value="1" />
<input type="hidden" name="hourIDs[]"  value="$this->hourID" />
<label class="label abn">Unit</label><input type="text" name="unit[]" id="unit_1"  class="input unit required $DISABLED"  value="$this->unitName"  $DISABLED />
<input type="hidden" name="plantID[]" value="$this->plantID" id="plantID_1"  />
<label class="label sml" >Machine</label><input type="text" name="machine[]" id="machine_1" class="input mach required $DISABLED" value="$this->plantName"  $DISABLED />
<label class="label tiny"  >Rate</label>
FIN;
if ($action != "new" ) {
   $content .= "<div id=\"ratediv_1\" class=\"ratediv\"  >\n";
   $content .= AdminFunctions::rateSelect($this->rate,$this->plantID,$this->hDate,$DISABLED);
   $content .= "</div>\n";
}
else {
 $content .= "<div id=\"ratediv_1\" class=\"ratediv\"  > &nbsp;</div>\n";
}
	$content .= "<label class=\"label abn\"  >Crew</label>\n";
	$content .= AdminFunctions::crewSelect($this->crewID,$DISABLED);
$content .= <<<FIN
<label class="label tiny" >Area</label><input type="text" name="area[]" id="area_1"  class="input area required $DISABLED"  value="$this->areaName" $DISABLED />
<input type="hidden" name="areaID[]"  id="areaID_1" value="$this->areaID" />
<label class="label tiny">Well</label>
FIN;
if ($action != "new" ) {
   $content .= "<div id=\"welldiv_1\" class=\"welldiv\"  >\n";
	$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,$DISABLED);  // All wells from this name  type
   $content .= "</div>\n";
}
else {
 $content .= "<div id=\"welldiv_1\" class=\"welldiv\"  > &nbsp;</div>\n";
}
 //$("#dkthours").scroller({preset: 'time',display: 'bubble',timeFormat: 'HH:ii',timeWheels: 'HHii',stepMinute: 15,mode: 'mixed'});
$content .= <<<FIN
<label class="label abn"  >C.O.O.</label><input type="text" name="coo[]" id="coo_1" class="input required sml DISABLED"   value="$this->callOffID" READONLY />
<div style="clear:both;height:5px;"> </div>
<label class="label abn"  >Hours</label><input type="text" name="hours[]" id="hrs_1" class="input litres hrs required"  onchange="calctotal(1);return false;" value="$this->dktHours" $DISABLED />
<label class="label sml"  >SubTot</label><input type="text" name="subtot[]" id="subhrs_1" class="input litres tot"  value="$this->total" READONLY />
<label class="label abn"  >Details</label><input type="text"  name="details[]" id="details"  class="input details $DISABLED"  $DISABLED value="$this->details" />
<label class="label abn"  >Litres</label><input type="text" name="litres[]" id="litres" class="input litres $DISABLED"  value="$this->litres" $DISABLED />
<label class="label abn"  >Metres</label><input type="text" name="cubic[]" id="cubic" class="input litres $DISABLED"  value="$this->cubic" $DISABLED />
<button  class="delete" onclick="confirmDeleteLine('#line_1');return false;">-</button>
<div style="clear:both;height:5px;"> </div>
<hr>
</div>
</div>
FIN;
$content .= "</fieldset>\n";
//$(function(){
 //  $(".input.date").scroller({preset: 'date',display: 'bubble',dateFormat: 'dd-mm-yy',dateOrder: 'ddmmyy',mode: 'mixed'});
//});
				$content .= "\n<a href=\"hours.php?action=list&redo=redo\"   class=\"submitbutton\" style=\"height:26px;line-height:26px;width:180px;\">List Hours</a>";
				$content .= "\n</form></div></div>\n";
			}

			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
      private function fillDefaults($search) {
			if ($search->operatorID > 0 ) {
				$this->operatorID = $search->operatorID;
				$this->operatorName = $search->operatorName;
			}
			if ($search->areaID > 0 ) {
				$this->areaID = $search->areaID;
				$this->areaName = $search->areaName;
			}
			if ($search->wellID > 0 ) {
				$this->wellID = $search->wellID;
				$this->wellName = $search->wellName;
			}
			
			if ($search->crewID > 0 ) {
				$this->crewID = $search->crewID;
			}
			if (strlen($search->hDate) > 0 ) {
				$this->hDate = $search->hDate;
			}
		}
	
		private function getAllHours($search,$redo,$order) {
			$t1TotHours = $t2TotHours = $t1Tot = $t2Tot = $expTot = 0.0;
			$showApproval= false; //  If exact date with  then offer approval button 
			$joinClause = "";
			$URL="https://".$_SERVER['HTTP_HOST'];
			$_SESSION['last_request'] = "$URL/admin/hours.php?action=list&redo=redo&con_id=$this->contractorID";
			$CHECKED = "";
			$this->hourID = NULL;
			$dateSelected = false;
			// default  Join
			$whereClause = " WHERE status > 0 ";
			$dateClause="where  status > 0";
			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
				if (isset($order) && strlen($order) > 0) {
            	$search->order = $order;
          	}
				$_SESSION['SQL'] = pg_escape_string(serialize($search));
					if (intval($search->cooID) > 0 ) {
						$this->callOffID = $search->cooID;
						$whereClause .= " and calloff_order_id = $this->callOffID ";
						$dateSelected = true;
					}
					else {
						if (!empty($search->operatorID)) {
                  	$this->operatorID = $search->operatorID;
                  	$this->operatorName = $search->operatorName;
                  	$whereClause .= " and h.employee_id = $search->operatorID ";
            		}
						if (!empty($search->unitID) ) {
							$this->unitID = $search->unitID;
							$this->machTag = $search->unitName;
							$whereClause .= " and  h.plant_id = $search->unitID";
						}
						if (!empty($search->plantTypeID) ) {
							$this->plantTypeID = $search->plantTypeID;
							$this->plantName = $search->plantName;
							$whereClause .= " and  h.plant_id in (select plant_id from {$this->shortName}_plant where plant_type_id = $search->plantTypeID)";
						}
						if (!empty($search->areaID)) {
							$whereClause .= " and h.area_id = $search->areaID ";
							$this->areaName = $search->areaName;
							$this->areaID = $search->areaID;
						}
						if (!empty($search->wellID) ) {
							$whereClause .= " and $search->wellID in (select (unnest(string_to_array(h.well_ids::text,'|')::int[])))";
							$this->wellID = $search->wellID;
						}
						if (!empty($search->crewID)) {
							$whereClause .= " and h.crew_id = $search->crewID ";
							$this->crewID = $search->crewID;
						}
						if (!empty($search->subCrewID)) {
                  	$whereClause .= " and h.sub_crew_id = $search->subCrewID ";
                  	$this->subCrewID = $search->subCrewID;
                  }
						if (!empty($search->status)) {
							$this->status = $search->status;
							//if ($search->status == 1 ) {
							//	$dateClause="where  status > 0";
							//	$joinClause = " JOIN {$this->shortName}_upload up on up.upload_id = (select max(upload_id) from {$this->shortName}_upload upp where upload_type_id = 8 and upp.upload_date = h.hour_date)";
							//}
							//else {
								$dateClause="where  status   = $search->status ";
								$whereClause .= " and  status = $search->status ";
							//}
						}
						if (isset($search->hDate) && strlen($search->hDate) > 0) {      // dates combined with COO's
							$dateSelected = true;
							$dateClause .= " and hour_date ";
                     $this->hDate = $search->hDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $dateClause .= "  =  '$search->hDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
								$showApproval = true;
                        break;
                     case "equalgreater":
                        $dateClause .= "  >=  '$search->hDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $dateClause .= "  <  '$search->hDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $dateClause .= " between  '$search->hDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->endDate = $search->endDate;
                           $this->hideEndDate='';
                        }
                        else {
                           $dateClause .= "  =  $search->hDate ";
                        }
                        break;
                     default:
								$dateClause .= " current_date ";
            				$this->equalto = "SELECTED=\"selected\"";
                        break;
                  	}
						}
						else if (! $dateSelected) {
							$this->hDate = date ('d-m-Y');
							$dateClause .= " and hour_date = current_date";
           				$this->equalto = "SELECTED=\"selected\"";
						}
				 }  // not COO
			}
			else {
				 // Default query still needs search object serialized
				$this->hDate = date ('d-m-Y');
				$dateClause = " WHERE hour_date =  current_date ";
            $this->equalto = "SELECTED=\"selected\"";
         	$search = new Search();
         	$search->dateType = "equal";
         	$search->hDate = $this->hDate;
				$search->order = "h.hour_date,opname,c.crew_name,a.area_name";
         	$_SESSION['SQL'] = pg_escape_string(serialize($search));
			}
			//   set heading backgrounds  and SQL order clause for eelected sort order
         $hd1 = $hd2 = $hd3 = $hd4 = $hd5 = $hd6 = $hd7 = "";
			$search->order = isset($search->order) ? $search->order : "date";
         switch($search->order) {
            case "date" :
            $hd2="sort";
            $orderClause = " order by hour_date";
            break;
            case "coo" :
            $hd3="sort";
            $orderClause = " order by h.calloff_order_id,hour_date";
            break;
            case "area" :
            $hd5="sort";
            $orderClause = " order by area_name,hour_date";
            break;
            case "well" :
            $hd6="sort";
            $orderClause = " order by well_name,hour_date";
            break;
            default:
               $hd2="sort";
               $orderClause = " order by hour_date";
            break;
         }
          
		$content ="<div style=\"width:1772px;margin:auto\">";
		//echo $whereClause;
		if(! is_null($this->contractorID)) { 		// search
			$sql = "with dates as (select docket_hours_t1,total_t1,docket_hours_t2,total_t2,status,employee_id,area_id,plant_id,well_id,crew_id,expense,hour_date,calloff_order_id,well_ids,sub_crew_id 
			from {$this->shortName}_hour $dateClause ),
			            tcall as (Select distinct calloff_order_id from dates h $whereClause  )
			SELECT sum(total_t1) as total_t1,sum(total_t2) as total_t2,sum(expense) as expense, sum(docket_hours_t1) as docket_hours_t1,sum(docket_hours_t2) as docket_hours_t2,
			count(status) as stat_count,sum(status) as stat_sum, h.hour_date,calloff_order_id,wells_from_ids(co.well_ids::text) as well_name,a.area_name,el.fault
			from dates h 
			JOIN tcall using(calloff_order_id)
			LEFT JOIN area a using (area_id) 
			LEFT JOIN calloff_order co using(calloff_order_id) 
			LEFT JOIN email_log el on el.email_log_id = 
				( select max(email_log_id) from email_log  ell where h.calloff_order_id  = ell.dkt_id and ell.dkt_date = h.hour_date and ell.receiver_id = $this->contractorID and receiver_type = 'APP' )
			$joinClause
			$whereClause
			group by h.hour_date,calloff_order_id,well_name,area_name,fault
			$orderClause
			LIMIT 200"; 


         //echo "<br />$sql <br />";
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content .= <<<FIN
         <form name="hour" method="post" action="hours.php?action=list&search=true">
         <input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
         <input type="hidden" name="con_name" id="con_name" value="$this->contractorName" />
         <input type="hidden" name="emp_id" id="emp_id" value="$this->employeeID" />
         <input type="hidden" name="operator_id" id="operatorID" value="$this->operatorID" />
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         <input type="hidden" name="unitID" id="unitID" value="$this->unitID" />
         <input type="hidden" name="plantTypeID" id="plantTypeID" value="$this->plantTypeID" />
			<fieldset ><legend style="margin-left:44%;">Filter Search - $this->contractorName</legend>
<div class="div700 marg2"  ><label for="coo_id" >C.O.O.<input type="text" name="coo_id" id="coo_id" value="$this->callOffID" /></label></div>
<div class="div22 marg2"  ><label for="operator"  >Operator<input type="text" name="operator" id="operator" value="$this->operatorName" /></label></div>
<div class="marg1"  style="margin-left:0.8%;" >&nbsp;</div>
FIN;
      $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->hDate,$this->endDate,$this->hideEndDate);
      $content .=  "<div class=\"div10 marg2\" ><label >Status</label>\n";
   $content .= AdminFunctions::statusSelect($this->status);
$content .= <<<FIN
</div>
<div class="div6 marg1" ><label for="invoicenum" >Invoice Num:<input type="text" name="invoicenum" id="invoicenum"  value="$this->invoiceNum" /></label> </div>
<div style="float:right;width:15%;" >
<input type="submit" name="search" value="Search"  class="button" />
<button class="marg"  onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
</div>
<div style="clear:both;"> </div>
<div class="div17 marg2" ><label >Plant Type<input type="text" name="machine" id="machine"  value="$this->plantName" /></label></div>
<div class="div12 marg2" ><label >Area<input type="text" name="area" id="area"  value="$this->areaName" /></label></div>
<div class="div12 marg2" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= "</div></div>\n";
$content .= "<div class=\"div12 marg1\" style=\"margin-left:1.08%;\" ><label  >Work Scope</label>";
$content .= AdminFunctions::crewSelect($this->crewID,"",true);
$content .= "</div>\n";
$content .= "<div class=\"div17 marg2\" id=\"subscopediv\" >";
$content .= AdminFunctions::subScopeSelect($this->crewID,$this->subCrewID,"");
$content .= "</div>\n";

$content .= <<<FIN
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
       var type = this.type, tag = this.tagName.toLowerCase();
		 var idd = $(this).attr('id');
       if (tag == 'form' ) {
          return $(':input',this).clearForm();
		 }
       if (type == 'text' || type == 'password' ||  tag == 'textarea' || type == 'hidden' ) {
			if (idd != 'contractorID') { 
          	this.value = '';
			}	
		 }   
       else if (type == 'checkbox' || type == 'radio') {
          this.checked = false;
		 }
       else if (tag == 'select') {
        	this.selectedIndex = -1;
		 }
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#machine').autocomplete({
         width: 700,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=planttype',
         minChars:1,
         params: {con_id: $('#contractorID').val()},
         onSelect: function(value, data){ $('#plantTypeID').val(data); }
      });
  });
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_s',$('#welldiv'),$('#contractorID')); return false; }
      });
  });
$(function () {
      $('#tag').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=tag&con_id=$this->contractorID',
         minChars:2,
         onSelect: function(value, data){ $('#unitID').val(data); return false;}
      });
  });
$(function () {
      $('#operator').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=operator&con_id=$this->contractorID',
         minChars:1,
         onSelect: function(value, data){ $('#operatorID').val(data); return false;}
      });
  });
$(function () {
      $('#coo_id').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=calloffs',
         params: {con_id: $('#contractorID').val()},
         minChars:2
      });
  });
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','single'); });
</script>
FIN;

			$content .= "</fieldset></form>\n";

			//$content .= "<div style=\"clear:both;\"></div>\n";
			// headings
			$content .= "<div class=\"heading\" >";
			$content .= "<a href=\"hours.php?action=list&redo=redo&order=date&con_id=$this->contractorID\" class=\"lnk\" >  <div class=\"time hd $hd2\">Date</div></a>\n";
			$content .= "<div class=\"crew hd\" >Contractor</div>\n";
			$content .= "<a href=\"hours.php?action=list&redo=redo&order=coo&con_id=$this->contractorID\" class=\"lnk\" ><div class=\"time hd $hd3\">C.O.O.</div></a>\n";
			$content .= "<div class=\"usr narr hd\"><div style=\"float:left;\" >Compl%</div><div style=\"float:right;\" class=\"txtr\" >TCE%</div></div>\n";
			$content .= "<a href=\"hours.php?action=list&redo=redo&order=area&con_id=$this->contractorID\" class=\"lnk\" ><div class=\"ptype wd hd $hd5\"  >Area</div></a>\n";
			$content .= "<a href=\"hours.php?action=list&redo=redo&order=well&con_id=$this->contractorID\" class=\"lnk\" ><div class=\"email narr hd $hd6\">Well/s</div></a>\n";
			$content .= "<div class=\"weight hd padr\">T1 Hours</div>";
			$content .= "<div class=\"time hd padr\"  >T1 Total</div>\n";
			$content .= "<div class=\"weight hd padr\"  >T2 Hours</div>\n";
			$content .= "<div class=\"time hd padr\">T2 Total</div>\n";
			$content .= "<div class=\"weight hd padr\">Expense</div>\n";
			$content .= "<div class=\"approv hd\"  >Approve</div>\n";
			$content .= "<div class=\"usr hd\"  >&nbsp;</div>\n";
			$content .= "<div class=\"links hd \" style=\"width:96px;\">Actions</div>\n";
			$content .= "</div>\n"; // Close heading
			$lineNo = 0;
			while (! $rs->EOF ) {
				$showApprove=true;
				$showT2 = false;
				$percColor = "color:#000000";
				$ln = $lineNo % 2 == 0 ? "background:#f1b496" : "background:#f1d5c7" ;
		//public $csv = array('Operator'=>array(),'Date'=>array(),'DocketHours'=>array(),'Docket'=>array(),'Machine'=>array(),'Unit'=>array(),'Rate'=>array(),'Total'=>array(),'Area'=>array(),'Well'=>array(),'Detail'=>array(),'Crew'=>array());
				$yesChecked = ""; //$status == 4 ? "checked" : "";
				$noChecked =  "";// $status == 2 ? "checked" : "";
				$approve =  "&nbsp;";// $status == 2 ? "checked" : "";
				$emailButTxt = "Email";
				$emailButClass = "";
				$jDate = AdminFunctions::dbdate($rs->fields['hour_date']);
				$cooID = $rs->fields['calloff_order_id'];
				// Percentages
				$sql2 = "SELECT * from percent_est($cooID,'$jDate')";
				$percArr = AdminFunctions::execConFloat($this->dbLink,$sql2);
				$perc = floatval($percArr['tce_est']). "%";
				$compl = floatval($percArr['perc_est']) ."%";
				//$perc = 0.0;
				if (floatval($perc) > 80.0 ) {
               $percColor = "background:#ffffff;color:#ff0000";
            }
				//  is this an Equal to selection only this can trigger reports in percent div
				$equal = !is_null($this->equalto) ? "equal" : "";

				//$this->csv['Date'][] = $jDate;
				if(!empty($rs->fields['docket_hours_t1'])) {
					$t1TotHours += $rs->fields['docket_hours_t1'];
				} 
				if(!empty($rs->fields['docket_hours_t2'])) {
					$t2TotHours += $rs->fields['docket_hours_t2'];
				} 
				if(!empty($rs->fields['total_t1'])) {
					$t1Tot += $rs->fields['total_t1'];
				} 
				if(!empty($rs->fields['total_t2'])) {
					$t2Tot += $rs->fields['total_t2'];
				} 
				if(!empty($rs->fields['expense'])) {
					$expTot += $rs->fields['expense'];
				} 
				$t1Hours = floatval($rs->fields['docket_hours_t1']) > 0 ? number_format($rs->fields['docket_hours_t1'],2) : ""; 
				$t2Hours = floatval($rs->fields['docket_hours_t2']) > 0 ? number_format($rs->fields['docket_hours_t2'],2) : ""; 
				$total_t1 = floatval($rs->fields['total_t1']) > 0 ? number_format($rs->fields['total_t1'],2) : "";
				$total_t2 = floatval($rs->fields['total_t2']) > 0 ? number_format($rs->fields['total_t2'],2) : "";
				$expense = floatval($rs->fields['expense']) > 0 ? number_format($rs->fields['expense'],2) : "";
				
				$statCount = $rs->fields['stat_count'];
				$statSum = $rs->fields['stat_sum'];
				$fault = ! is_null($rs->fields['fault']) ? preg_match('/OK/',$rs->fields['fault']) : -1;  // JOIN on email_log
				if ($fault > 0 ) {
					$emailButTxt = "Emailed";
					$emailButClass = "emailed";
				}
				if ( $statCount == $statSum ) {  // non approved or rejected
					$color = $this->statusArr[0];
				}
				else if ($statSum  == ($statCount * 5 )) {  // All approved
					$yesChecked = "checked";
					$color = $this->statusArr[1];
					if (floatval($rs->fields['total_t2']) > 0 ) {
						$showT2 =  true;
					}
				}
				else if ($statSum  == ($statCount * 6 )) {  //  submitted for completion 
					$color = $this->statusArr[6];
					$showApprove=false;
				}
				else if ($statSum  == ($statCount * 7 )) {  //  approved completion 
					$color = $this->statusArr[7];
					$showApprove=false;
				}
				else if ($statSum  == ($statCount * 2 )) {  //  ALL Rejected 
					$color = $this->statusArr[2];
				}
				else if ($statSum  == ($statCount * 9 )) {  //  100%  paid 
					$color = $this->statusArr[9];
					$showApprove=false;
				}
				else if ($statSum  == ($statCount * 3 )) {  //  ALL Late Entries 
					$color = $this->statusArr[3];
				}
				else if ($statSum  == ($statCount * 8 )) {  //  100%  invoiced 
					$color = $this->statusArr[8];
					$showApprove=false;
				}
				else if ($statSum  == ($statCount * 4 )) {  //  100%  invoiced 
					$color = $this->statusArr[4];
					$showApprove=true;
				}
				else {
					$color = $this->statusArr[10];  //  some rejected  cc
					if (floatval($rs->fields['total_t2']) > 0 ) {
						$showT2 =  true;
					}
				}
				
			
				$area = $rs->fields['area_name'];
				$well = $rs->fields['well_name'];
				if ($this->sessionProfile > 2  ) {
					if ($showApprove) {
						$approve .="<span class=\"approve\" >Approve</span><input type=\"radio\" class=\"rejj\" name=\"approve_$lineNo\" id=\"approve_$lineNo\" value=\"yes\" onclick=\"updateAll('approve_$lineNo',$this->contractorID);\" $yesChecked />\n";
						$approve .= "<span class=\"reject\" >Reject</span><input type=\"radio\" class=\"rejj\" name=\"approve_$lineNo\" id=\"approve_$lineNo\" value=\"no\" onclick=\"updateAll('approve_$lineNo',$this->contractorID);\" $noChecked />\n";
					}
					else {
        				 $approve .= "&nbsp;\n";
      			}
				}
				$content .= "<div id=\"linediv_$lineNo\" style=\"float:left;width:100%;background:$color;display:table;table-layout:fixed;border-bottom:1px solid #000000;\" >\n";
				$content .= "<input type=\"hidden\" id=\"calloff_$lineNo\" value=\"$cooID\" /><input type=\"hidden\" id=\"date_$lineNo\" value=\"$jDate\" />\n";
				$content .= "<div class=\"time highh\">$jDate</div>\n";
				$content .= "<div class=\"crew highh\" >$this->contractorName</div>\n";
				$content .= "<div class=\"time highh\" style=\"$percColor;\" >$cooID</div>\n";
				$content .= "<div class=\"usr narr highh\" id=\"perc_div_$lineNo\"  style=\"$percColor;\" onclick=\"show_perc($lineNo,'$jDate',$cooID,$this->contractorID);\" >\n";
				$content .= "<div style=\"float:left;\">$compl</div><div style=\"float:right;\" class=\"txtr\" >$perc</div></div>\n";
				$content .= "<input type=\"hidden\" id=\"toggle_$lineNo\" value=\"closed\" />\n";
				$content .= "<div class=\"ptype wd highh\"  >$area</div>\n";
				$content .= "<div class=\"email narr highh\">$well</div>\n";
				$content .= "<div class=\"weight txtr highh\">$t1Hours</div>";
				$content .= "<div class=\"time txtr highh\"  >$total_t1</div>\n";
				$content .= "<div class=\"weight txtr highh\">$t2Hours</div>";
				$content .= "<div class=\"time txtr highh\"  >$total_t2</div>\n";
				$content .= "<div class=\"weight txtr highh\">$expense</div>\n";
				$content .= "<div class=\"approv highh\"  >$approve</div>\n";
				$content .= "<div class=\"usr highh\"  >&nbsp;</div>\n";
				$content .= "<div class=\"links highh\"  style=\"width:96px;\" >\n";
				if ($this->sessionProfile > 2  ) {

					$content .= "<button id=\"but1_$lineNo\" class=\"linkbutton orang\" onclick=\"showdiv($cooID,'#coo_$lineNo','#but1_$lineNo','$jDate',$this->contractorID);return false;\">Show</button>";
					$content .= "<button  class=\"linkbutton $emailButClass\" onclick=\"emailres($cooID,'$jDate',$this->contractorID,$(this));$(this).prop('disabled','disabled');return false;\">$emailButTxt</button>";
					if ($showT2 ) {
						$content .= "<a href=\"{$URL}/printSupTable2.php?action=print&con_id=$this->contractorID&startdate=$jDate&coo_id=$cooID\" title=\"View Approval\" target=\"_blank\" class=\"linkbutton t2\"   >T2</a>\n";
					} 	
					else {
						$content .= " &nbsp;\n";
					}

				}
           	$content .= "</div>\n";
			   $content .= "</div>\n";  // closes linediv
				$content .= "<div id=\"coo_$lineNo\" class=\"hiddiv\"></div>\n";
				$content .= "<div id=\"perc_$lineNo\" class=\"hiddiv\"></div>\n";


				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			  }  
			
			//$this->printCSV();
				if ($t1TotHours + $t2TotHours + $t1Tot + $t2Tot + $expTot > 0 ) {
				 	$content .= "<div  style=\"float:left;width:100%;display:table;table-layout:fixed;background:#f9f9f6;margin-left:-3px; \" >\n";
				 	$t1TotHours = !empty($t1TotHours) ? number_format($t1TotHours,2) : "";
				 	$t2TotHours = !empty($t2TotHours) ? number_format($t2TotHours,2) : "";
				 	$t1Tot = !empty($t1Tot) ? number_format($t1Tot,2) : "";
				 	$t2Tot = !empty($t2Tot) ? number_format($t2Tot,2) : "";
				 	$expTot = !empty($expTot) ? number_format($expTot,2) : "";
				 	$content .= "<div class=\"location highh\" style=\"width:916px;border-style:none\" >&nbsp</div>\n";
				 	$content .= "<div class=\"weight txtr highh totdiv\">$t1TotHours</div>";
             	$content .= "<div class=\"time txtr highh totdiv\"  >$t1Tot</div>\n";
             	$content .= "<div class=\"weight txtr highh totdiv\">$t2TotHours</div>";
             	$content .= "<div class=\"time txtr highh totdiv\"  >$t2Tot</div>\n";
             	$content .= "<div class=\"weight txtr highh bdr totdiv\">$expTot</div>\n";
				 	$content .= "<div style=\"width:382px;\"  >&nbsp;</div>\n";
				 	$content .= "</div>\n";
				}
				if ($this->sessionProfile > 2 ) {
            	if ($showApproval  && $lineNo > 0) {  // No lines no approval required
               	$content .= AdminFunctions::supApproval($this->hDate,$this->contractorID);
					}
            }

					$content .= "</div> <!-- Close centre div -->";

			}  // end  no  sql  as still need contractor ID
			else { // ContractorID NULL show contrcator select
				$content = AdminFunctions::setContractor("hours.php?action=list",$this->contractorID); 
	   	}	

			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getHourDetails() {
			$sql = "SELECT h.*,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,p.plant_name,plant_unit,w.well_name,a.area_name,c.crew_name,coo.contract_num from {$this->shortName}_hour h
                  LEFT JOIN {$this->shortName}_plant p using (plant_id)
                  LEFT JOIN area a using (area_id)
                  LEFT JOIN employee e using (employee_id)
                  LEFT JOIN crew c  using (crew_id)
                  LEFT JOIN well w on w.well_id =  h.well_id 
						LEFT JOIN calloff_order  coo using (calloff_order_id)
						WHERE  h.hour_id = $this->hourID";
			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->hourID = $data['hour_id'];
		 	$this->operatorID = $data['employee_id'];
       	$this->plantID = $data['plant_id'];
       	$this->areaID = $data['area_id'];
       	$this->hDate	= AdminFunctions::dbdate($data['hour_date']);
			$this->status = $data['status'];
       	//$this->dktHours = AdminFunctions::floatToHour($data['docket_hours']);
       	$this->dktHours = $data['docket_hours'];
       	$this->docketNo = $data['docket_num'];
       	$this->rate = $data['rate'];
			$this->total = $data['total']; 
       	$this->wellID = $data['well_id'];
       	$this->crewID = $data['crew_id'];
       	$this->details =$data['details'];
			$this->operatorName = $data['opname'];
			$this->machineName = $data['plant_name'];
			$this->areaName = $data['area_name'];
			$this->unitName = $data['plant_unit'];
			$this->invoice = $data['invoice_num'];
			$this->litres = floatval($data['litres']) > 0 ? $data['litres'] : '';
			$this->cubic = floatval($data['cubic_metres']) > 0 ? $data['cubic_metres'] : '';
			$this->callOffID = $data['calloff_order_id'];
			$this->contractNum = $data['contract_num'];

		}

		 private function printCSV() {
          //var_dump($this->csv);
          //exit;
			return;
         $headings = array('Date','C.O.O.','Operator','Docket Hours','Docket','Machine','Unit','Plant Type','Rate','Total','Area','Well','Detail','Crew','Litres','Cubic Metres');
         $file= "tmp/invoice.csv";
         $fp = fopen($file,'w');
			if (strlen($this->invoiceNum) > 2 ) {
				$title = array("Invoice Number $this->invoiceNum");
         	fputcsv($fp,$title);
			}
         fputcsv($fp,$headings);
         foreach($this->csv['Operator'] as $ind =>$val) {
            $tmpArr= array($this->csv['Date'][$ind],$this->csv['COO'][$ind],$this->csv['Operator'][$ind],$this->csv['DocketHours'][$ind],$this->csv['Docket'][$ind],$this->csv['Machine'][$ind],$this->csv['Unit'][$ind],$this->csv['PlantType'][$ind],$this->csv['Rate'][$ind], $this->csv['Total'][$ind],$this->csv['Area'][$ind],$this->csv['Well'][$ind],$this->csv['Detail'][$ind],$this->csv['Crew'][$ind],$this->csv['Litres'][$ind],$this->csv['Cubic'][$ind]);
         fputcsv($fp,$tmpArr);
         }
         fclose($fp);
      }


	}
?>
