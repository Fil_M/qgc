<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class WellLease  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $crewID;

		public function	__construct($action,$startDate=NULL,$endDate=NULL,$type="contractor",$crew=0) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= !empty($startDate)  ?  $startDate : AdminFunctions::beginLastMonth();
         $this->endDate= ! empty($endDate)     ? $endDate  :  AdminFunctions::endLastMonth();
		 	$this->crewID=$crew;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			elseif (isset($_POST['PRINT'])) {
				$URL="https://".$_SERVER['HTTP_HOST'] ."/printLeaseWellpdf.php?action=$this->action&start=$this->startDate&end=$this->endDate&type=$type&crew_id=$this->crewID";
				header("LOCATION: $URL");
				exit;
			}
			else {
				$pg = new Page('Area');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Print Well Lease Report";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print Report\" class=\"button marg\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1100px;margin:auto;" >
<form name="wellreport" id="wellreport" method="post" action="well_lease_report.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<fieldset ><legend style="margin-left:46%;">Well Lease Report</legend>
<div class="div15 margt1" >
<div class="div84" ><label >Split by Contractor:</label></div><div class="div16" ><input type="radio" name="type_split"   value="contractor"  CHECKED  /></div>
<div class="div84" ><label >Split by Area:</label></div><div class="div16" ><input type="radio" name="type_split"   value="average"     /></div>
</div>
<div class="div19 marg" id="work_scope_div"  >
FIN;
          $content .= AdminFunctions::crewParentSelect(NULL,"",true);

$content .= <<<FIN
</div>
<div class="div10 marg" > <label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div class="div10 marg" > <label>End Date:<input type="text" name="enddate" id="edate" class="input required date" value="$this->endDate"  /></label></div>
<div style="float:right;width:27%;" >
<button class="reset" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div>
</fieldset>
</form>
</div>
<script>
$("#wellreport").submit(function(){
   var isFormValid = true;
   $("#wellreport input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#wellreport .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
	 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
 }
?>
