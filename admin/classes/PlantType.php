<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class PlantType  {
		public $page;
		public $plantTypeID;
		public $pType;
		public $action;
		public $sessionProfile;
		public $contractorID = 0;
		public $conn;
		public $rate;
		public $newRate;
		public $effectiveFrom;
		public $standRate;
		public $standNewRate;
		public $equalto;
      public $between;
		public $less;
      public $hideEndDate='style="display:none;"';
      public $equalgreater;
      public $creDate;
      public $endDate;
		public $uom;
		public $priceTypeName;


		public function	__construct($action,$plantTypeID=0,$search=NULL,$redo=NULL) {
			$this->plantTypeID = $plantTypeID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('planttype');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New Plant Type";
						break;
					case "update" :
						if ($plantTypeID < 1 ) {
							return false;
						}
						$heading_text = "Updating Plant Type $plantTypeID";
						$this->getPlantDetails($plantTypeID);
						break;
					case "delete" :
						if ($plantTypeID < 1 ) {
							return false;
						}
						$heading_text = "Delete Plant $plantTypeID";
						$this->getPlantDetails($plantTypeID);
						break;
					case "list" :
						$heading_text =   "List Plant Types";
						$this->getAllPlants($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Plant Type $plantTypeID";
						$this->getPlantDetails($plantTypeID);
						break;
					default:
						$heading_text = "Add New Plant Type";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->plantTypeID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Plant Type";
						break;
					case "update":
						$button = "Update Plant Type";
						break;
					case "delete":
						$button = "Delete Plant Type";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Plant Type";
						break;
				}
			$content = <<<FIN
<div style="width:1260px;margin:auto;">
<form name="planttype" id="planttype" method="post" action="planttype.php">
<input type="hidden" name="plant_type_id" value="$this->plantTypeID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:45%;">Plant Type - $this->plantTypeID</legend>
<div class="div50" ><label for="p_type" >Plant Type<input type="text" name="p_type" class="required" id="p_type" value="$this->pType" $DISABLED /></label></div>
<div class="div17 marg1" >
FIN;
$content .= AdminFunctions::contractorSelect($this->contractorID,$DISABLED);
$content .= "</div><div class=\"div10 marg1\" ><label for=\"uom\" class=\"label site\" >Unit of Measure</label>\n";
$content .= AdminFunctions::uomSelect($this->uom,$DISABLED);
$content .= "</div><div class=\"div11 marg1\" ><label  >Price Type</label>\n";
$content .= AdminFunctions::priceTypeSelect($this->priceTypeName,$DISABLED);
$content .= <<<FIN
</div>
<div style="clear:both;" ></div>
<div class="div50" >
<div class="div20" ><label for="rate" >Table 1 Rate<input type="text" name="rate" id="rate" value="$this->rate" class="required" $DISABLED /></label></div>
<div class="div20 marg" ><label for="standrate" >Table 2 Rate<input type="text" name="standrate" id="standrate"  value="$this->standRate" $DISABLED /></label></div>
</div>
<div class="div50" >
<div class="div22 marg2" ><label for="effective" >New Change From <input type="text" name="effective" id="effective" class="date" value="$this->effectiveFrom" $DISABLED /></label></div>
<div class="div22 marg7" ><label for="newrate"  >New Table 1 Rate<input type="text" name="newrate" id="newrate"  value="$this->newRate" $DISABLED /></label></div>
<div class="div22 marg7" > <label for="standnewrate" >New Table 2 Rate<input type="text" name="standnewrate" id="standnewrate" value="$this->standNewRate" $DISABLED /></label></div>
</div>
</fieldset>
<script>
$("#planttype").submit(function(){
   var isFormValid = true;
   if ($.trim($('#effective').val()).length != 0)  {
      if ($.trim($('#standnewrate').val()).length == 0)  {
         $('#standnewrate').addClass("highlight");
         isFormValid = false;
      }
      else {
          $('#standnewrate').removeClass("highlight");
      }
      if ($.trim($('#newrate').val()).length == 0)  {
         $('#newrate').addClass("highlight");
         isFormValid = false;
      }
      else {
          $('#newrate').removeClass("highlight");
      }
   }
	$("#planttype .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
    else {
      $('#SUBMIT').prop("disabled", "disabled");
      return true;
    }

});
jQuery('.sml').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;


				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found plant
				}
				 $content .= "\n<a href=\"planttype.php?action=list&redo=redo\"   class=\"button\" >Last Screen</a>";

				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllPlants($search,$redo) {
         $this->equalgreater = "SELECTED=\"selected\"";
			$this->effectiveFrom = NULL;
         $whereClause=" where pt.removed is false ";
         if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
         if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if (!empty($search->pTypeName) > 0 ) {
               $this->pType = $search->pTypeName;
               $whereClause .= " and p_type = '$this->pType' ";
            }
            if ( !empty($search->contractorID) > 0) {
               $this->contractorID = $search->contractorID;
               $whereClause .= " and contractor_id = $this->contractorID ";
            }
				if (!empty($search->creDate) > 0) {
                     $dateSelected = true;
                     $whereClause .= " and effective_from ";
                     $this->effectiveFrom = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
                        $this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->endDate = $search->endDate;
                           $this->hideEndDate='';
                           $this->equalgreater = NULL;
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause = " where pt.removed is false ";
                        //$this->equalgreater = "SELECTED=\"selected\"";
                        break;
                  }
				}
			}
			else {
             // Default query still needs search object serialized
            $this->creDate = NULL;
            $whereClause = " where pt.removed is false";
            //$this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            //$search->dateType = "equalgreater";
            $search->creDate = NULL;
         }


			$sql = "SELECT pt.*,c.con_name from plant_type pt LEFT JOIN contractor c using (contractor_id) $whereClause order by p_type";

			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content ="<div style=\"width:1530px;margin:auto;\">";
			  $content .= <<<FIN
         <form name="field" method="post" action="planttype.php?action=list">
         <input type="hidden" name="plantTypeID" id="plantTypeID" value="$this->plantTypeID" />
<fieldset ><legend style="margin-left:44%;">Filter Search - Plant Types</legend>
<div class="div12" >
FIN;
			$content .= AdminFunctions::contractorSelect($this->contractorID,"",true,"contractor");
			$content .= <<<FIN
</div>
<div class="div24 marg2" ><label for="ptype" >Plant Type<input type="text" name="ptype" id="ptype" value="$this->pType" /></label></div>
<div class="div9 marg2" ><label for="datetype" >Effective Date is:</label><select class="sel" name="datetype" id="datetype"  onchange="displayDiv('#datetype');">
<option value="equalgreater" $this->equalgreater >Equal or greater</option>
<option value="equalto" $this->equalto >Equal to</option>
<option value="between" $this->between >Between</option>
<option value="less" $this->less >Less than</option>
</select></div>
<div class="div8 marg2" ><label for="eff_date" >Date<input type="text" name="eff_date" id="eff_date" class="date" value="$this->effectiveFrom" /></label></div>
<div class="div8 marg2" id="enddiv" $this->hideEndDate ><label for="end_date" >and Date<input type="text" name="end_date" id="end_date" class="date" value="$this->endDate" /></label> </div>
<script>
$( document ).ready(function() {
   $(function () {
       var ac = $('#ptype').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=planttype',
         params: {con_id:  function() { return $('#contractor').val()} },
         minChars:1
      });
      $("#contractor").change(function() {
         $('#ptype').val('');
         ac.clearCache();
      });
   });
});
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
 			$content .= "<div style=\"float:right;width:17%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\"  class=\"marg\" >Reset</button>";
         //$content .= "<a href=\"hours.php?action=list&search=true&printcsv=true\" class=\"submitbutton printcsv\" >Print C.S.V.</a>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"contact hd\">Contractor Name</div>\n";
			$content .= "<div class=\"area hd\">Plant Type</div>";
			$content .= "<div class=\"usr hd\">Price Type</div>";
			$content .= "<div class=\"usr hd\">U.O.M.</div>";
         $content .= "<div class=\"fone hd padr\">T1 Rate</div>";
         $content .= "<div class=\"fone hd padr\">T2 Rate</div>";
			$content .= "<div class=\"usr hd\">Change New</div>";
         $content .= "<div class=\"fone hd padr\">New T1 Rate</div>\n";
         $content .= "<div class=\"fone hd padr\">New T2 Rate</div>";

			$content .= "<div class=\"links hd\" style=\"width:116px;\" >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$plantTypeID = $rs->fields['plant_type_id'];
				$conName = $rs->fields['con_name'];
				$conName = intval($rs->fields['contractor_id']) > 0 ? $conName : "ALL CONTRACTORS";
				$pType = strlen($rs->fields['p_type']) > 2 ? $rs->fields['p_type'] : " ";
				$rate =  sprintf('%01.2f',$rs->fields['plant_rate']);
            $newrate = !is_null($rs->fields['new_rate'])  ? sprintf('%01.2f',$rs->fields['new_rate']) : " ";
            $effective = strlen($rs->fields['effective_from']) > 0 ? AdminFunctions::dbdate($rs->fields['effective_from']) : " ";
            $stand_rate =  !is_null($rs->fields['stand_rate'])   ?sprintf('%01.2f',$rs->fields['stand_rate']) : " ";
            $stand_newrate = !is_null($rs->fields['stand_new_rate']) ? sprintf('%01.2f',$rs->fields['stand_new_rate']) : " ";
				$uom = $rs->fields['unit_of_measure'];
				$priceType = $rs->fields['price_type'];
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"contact $ln highh\">$conName </div>\n";
				//$content .= "<div class=\"area $ln highh\"><a href=\"planttype.php?action=find&amp;plant_type_id=$plantTypeID\" title=\"View Plant Type\" class=\"name\" >$pType</a></div>";
				$content .= "<div class=\"area $ln highh\"> $pType</div>";
				$content .= "<div class=\"usr $ln highh\">$priceType</div>";
				$content .= "<div class=\"usr $ln cntr highh\">$uom</div>";
				$content .= "<div class=\"fone $ln txtr highh\">$rate</div>";
            $content .= "<div class=\"fone $ln txtr highh\">$stand_rate</div>";
				$content .= "<div class=\"usr $ln cntr highh\">$effective</div>\n";
            $content .= "<div class=\"fone $ln txtr highh\">$newrate</div>\n";
            $content .= "<div class=\"fone $ln txtr highh\">$stand_newrate</div>\n";

				if ($this->sessionProfile > 2 ) {
           		$content .= "<div class=\"links $ln highh\" style=\"width:116px;\"  >\n";
				 	$content .= "<div style=\"margin:auto;width:100px;\" >\n";
					$content .= "<a href=\"planttype.php?action=update&plant_type_id=$plantTypeID\" title=\"Edit Plant Type\" class=\"linkbutton\" >Edit</a>\n";
					$content .= "<a href=\"planttype.php?action=delete&plant_type_id=$plantTypeID\" title=\"Delete Plant Type\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >Delete</a></div>\n";
				}
				else {   
			   $content .= "<div class=\"links $ln highh\" style=\"width:116px;\"  > &nbsp;\n";
				}
			
				$content .= "</div></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getPlantDetails($plantTypeID) {
			$sql = "SELECT *  from plant_type  where plant_type_id = $plantTypeID";

        // echo $sql;

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->plantTypeID = $data['plant_type_id'];
			$this->pType = $data['p_type'];
			$this->rate = $data['plant_rate'];
			$this->newRate = $data['new_rate'];
			$this->effectiveFrom = AdminFunctions::dbDate($data['effective_from']);
			$this->standRate = $data['stand_rate'];
			$this->standNewRate = $data['stand_new_rate'];
			$this->contractorID = $data['contractor_id'];
			$this->uom = $data['unit_of_measure'];
			$this->priceTypeName = $data['price_type'];

		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deletePlant();
			}
			$this->plantTypeID = $_POST['plant_type_id'];
			$this->pType = trim(addslashes($_POST['p_type']));
			$this->rate = floatval($_POST['rate']);
			$this->newRate = strlen($_POST['newrate']) > 0 ? $_POST['newrate'] : "NULL";
			$this->effectiveFrom = strlen($_POST['effective']) > 2 ? "'" . $_POST['effective'] . "'" : "NULL";
			$this->standRate = floatval($_POST['standrate']) > 0 ? $_POST['standrate'] : "NULL";
			$this->standNewRate = strlen($_POST['standnewrate']) > 0  ? $_POST['standnewrate'] : "NULL";
			if ($this->standNewRate == "NULL" && $this->newRate == "NULL" ) {
				$this->effectiveFrom = "NULL";
			}
			$this->contractorID = intval($_POST['contractor']);
			$this->uom = $_POST['uom'];
			if ($this->uom != "HOUR"  && is_null($this->rate) ) {
				$this->rate = 1;    // double insulation  must  be  EACH  ,  DAY  or  LEASE 
			}
			$this->priceTypeName = trim($_POST['price_type']);
			
			$this->submitPlant();
		}
		private function deletePlant() {
			$sql = "UPDATE plant_type set removed = TRUE where plant_type_id = $this->plantTypeID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: planttype.php?action=list");
			exit;
		}
		private function submitPlant() {
			if ($this->plantTypeID == 0 ) {
				$sql = "INSERT into plant_type values 
				(nextval('plant_type_plant_type_id_seq'),E'$this->pType',false,$this->rate,$this->newRate,$this->effectiveFrom,$this->standRate,$this->standNewRate,$this->contractorID,'$this->uom','$this->priceTypeName') 
				returning plant_type_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->plantTypeID = $rs->fields['plant_type_id'];
			}
			else { // Update
				$sql = "UPDATE plant_type set p_type = E'$this->pType',plant_rate=$this->rate,new_rate=$this->newRate,effective_from = $this->effectiveFrom,
				stand_rate = $this->standRate,stand_new_rate = $this->standNewRate,contractor_id = $this->contractorID,unit_of_measure = '$this->uom',price_type = '$this->priceTypeName'  where plant_type_id = $this->plantTypeID";

				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
			}

			header("LOCATION: planttype.php?action=find&plant_type_id=$this->plantTypeID");
			exit;
			
		}

	}
?>
