<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Supervisor  {
		public $page;
		public $supGroupID;
		public $groupName;
		public $emp_1_ID;
		public $emp_2_ID;
		public $action;
		public $sessionProfile;
		public $conn;
		public $supArr=array();

		public function	__construct($action,$supGroupID=0,$search="") {
			$this->supGroupID = $supGroupID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('supervisor');
				$this->page= $pg->page;
				$heading_text = "Administration<BR />";
         	switch($action) {
					case "new" :
						$heading_text .= "Add New Supervisor Group";
						break;
					case "update" :
						if ($supGroupID < 1 ) {
							return false;
						}
						$heading_text .= "Updating Supervisor Group $supGroupID";
						$this->getSupGroupDetails($supGroupID);
						break;
					//case "delete" :
					//	if ($supGroupID < 1 ) {
					//		return false;
					//	}
					//	$heading_text .= "Delete Supervisor Group $supGroupID";
					//	$this->getSupGroupDetails($supGroupID);
					//	break;
					case "list" :
						$heading_text .=  strlen($search) > 0 ? "List Supervisor Groups - $search" : "List Supervisor Groups - All";
						$this->getAllSupGroups($search) ;
						break;
					case "find" : 
						$heading_text .= "Supervisor Group $supGroupID";
						$this->getSupGroupDetails($supGroupID);
						break;
					default:
						$heading_text .= "Add New Supervisor Group";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->supGroupID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Supervisor Group";
						break;
					case "update":
						$button = "Update Supervisor Group";
						break;
					case "delete":
						$button = "Delete Supervisor Group";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Supervisor Group";
						break;
				}
			$content = <<<FIN
<div style="width:1800px;text-align:center;"><div style="width:1366px;margin:auto;">
<form name="supervisor" id="supervisor" method="post" action="supervisor.php?SUBMIT=SUBMIT">
<input type="hidden" name="sup_id" value="$this->supGroupID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="margin-top:15px;width:1366px;"><legend style="margin-left:640px;">Supervisor Group</legend>
<label for="group_name" class="label wide" >Group Name</label><input type="text" name="group_name" id="group_name" class="input $DISABLED" value="$this->groupName" $DISABLED />
<label for="supervisor" class="label" >Supervisors</label>
FIN;
$content .= AdminFunctions::supervisorSelect($this->supArr,$DISABLED); 
$content .= <<<FIN
<div style ="clear:both;height:15px;" > </div>
<script>
$("#supervisor").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
</script>
</fieldset>
FIN;


				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"submitbutton\" style=\"margin:0px 0px 0px 150px;width:300px;\"/>"; // no submit for found area
				}
				 $content .= "\n</form></div></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllSupGroups($search="") {
		  	$whereClause="";
         if (strlen($search) > 0 ) {
            $whereClause = " and upper(s.group_name) like '$search%'";
         }
	
			$sql = "SELECT  s.*,email_from_ids(s.employee_ids) as emails 
			FROM supervisor_group s
			where removed is false $whereClause order by s.group_name";
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}
			$content ="<div style=\"width:1800px;text-align:center;\"><div style=\"width:1366px;margin:auto;\">";
			$content .= AdminFunctions::AtoZ("supervisor.php?action=list",$search);
			$content .= AdminFunctions::oneToNine("supervisor.php?action=list",$search);
			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div  style=\"width:830px;margin:auto;\" >\n";
			$content .= "<div class=\"heading\" style=\"width:828px;\" ><div class=\"usr hd\">Supervisor Group</div>";
			$content .= "<div class=\"well hd\">Emails</div>\n";
			$content .= "<div class=\"links hd\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$sup_id = $rs->fields['supervisor_group_id'];
				$group_name = strlen($rs->fields['group_name']) > 2 ? $rs->fields['group_name'] : " ";
				$emails = $rs->fields['emails'];
				$content .= "<div class=\"usr $ln\"  ><a href=\"supervisor.php?action=find&amp;sup_id=$sup_id\" title=\"View Supervisor Group\" class=\"name\" >$group_name</a></div>\n";
				$content .= "<div class=\"well $ln\" >$emails</div>\n";
				if ($this->sessionProfile > 2 ) {
           	$content .= "<div class=\"links $ln\" ><a href=\"supervisor.php?action=update&sup_id=$sup_id\" title=\"Edit Supervisor Group\" class=\"linkbutton\" >EDIT</a></div>\n";
				}
				else {
					$content .= "<div class=\"links $ln\" > &nbsp;</div>";
				}
				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "</div></div></div><hr />";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getSupGroupDetails($supGroupID) {
			$sql = "SELECT s.*
			FROM supervisor_group s
			where supervisor_group_id = $supGroupID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->supGroupID = $data['supervisor_group_id'];
			$this->groupName = $data['group_name'];
			$this->supArr =  explode("|" ,$data['employee_ids']);

		}
		private function processPost() {
         //if ($this->action == "delete" ) {
			//	$this->deleteSupGroup();
			//}
			$this->supGroupID = $_POST['sup_id'];
			$this->groupName = trim(addslashes($_POST['group_name']));
			$this->supArr = isset($_POST['supervisor']) ? $_POST['supervisor'] : NULL;
			$this->submitSupGroup();
		}
		private function deleteSupGroup() {
		//	$sql = "UPDATE supervisor_group set removed = TRUE where supervisor_group_id = $this->supGroupID";
		//	if (! $this->conn->Execute($sql)) {
		//		die($this->conn->ErrorMsg());
		//	}
		//	header("LOCATION: supervisor.php?action=list");
		//	exit;
		}
		private function submitSupGroup() {
			$supStr = "";
			if (! is_null($this->supArr)) {
				foreach($this->supArr as $ind=>$val) {
					$supStr .= "$val|";
				}
				$supStr = preg_replace('/\|$/',"",$supStr);
				$supEmailStr = AdminFunctions::getSupEmails($supStr);
			}
			if ($this->supGroupID == 0 ) {
				$sql = "INSERT into supervisor_group values (nextval('supervisor_group_supervisor_group_id_seq'),E'$this->groupName',FALSE,'$supStr','$supEmailStr') returning supervisor_group_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->supGroupID = $rs->fields['supervisor_group_id'];
			}
			else { // Update
				$sql = "UPDATE supervisor_group set group_name = E'$this->groupName',employee_ids = '$supStr',group_emails='$supEmailStr'  where supervisor_group_id = $this->supGroupID";
				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}

			}

			header("LOCATION: supervisor.php?action=list");
			exit;
			
		}

	}
?>
