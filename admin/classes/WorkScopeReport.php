<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class WorkScopeReport  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $crewID;
		public $showSplit;

		public function	__construct($action,$crewID=0,$startDate=NULL,$endDate=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= !is_null($startDate)  ?  $startDate : AdminFunctions::beginLastMonth();
         $this->endDate= ! is_null($endDate)  ?  $endDate  :  AdminFunctions::endLastMonth();
		 	$this->crewID=$crewID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['PRINT'])) {
				$splitStr = isset($_POST['subscope']) ? "&split=true" : ""; 
				$type = isset($_REQUEST['type_split']) ? $_REQUEST['type_split'] : "contractor";
				$crewStr  = isset($_REQUEST['crew']) ? "&crew_id=" .intval($_REQUEST['crew']) : "";
				$conStr  = !empty($_REQUEST['contractor']) ? "&con_id=" .intval($_REQUEST['contractor']) : "";
				if ($type == "contractor" && intval($_REQUEST['crew']) > 0 ) {
					$splitStr =  ""; 
				}
				else if ($type == "contractor" ) {
					$splitStr = "&split=true";
				}
				else if ($type == "crew" ) {
					$crewStr = "";
				}
				$URL="https://".$_SERVER['HTTP_HOST'] ."/printWorkScopepdf.php?action=print{$crewStr}&type=$type{$splitStr}{$conStr}&start=$this->startDate&end=$this->endDate";
				header("LOCATION: $URL");
				exit;
			}
			else {
				$pg = new Page('WorkScope');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Print Work Scope Report";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print Report\" class=\"button marg\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			$content = <<<FIN
<div style="width:1500px;margin:auto;">
<form name="workscope" id="workscope" method="post" action="workscopereport.php?action=$action">
<input type="hidden" name="crew_id" id="crewID" value="$this->crewID" />
<fieldset ><legend style="margin-left:45%;">Work Scope Report</legend>
<div class="div13"  >
FIN;
			$content .= AdminFunctions::contractorSelect(NULL,"",true);
			$content .= <<<FIN
</div>
<div class="div12 marg2 margt1" >
<div style="height:1.5rem !important;" ><div class="div80" id="consplit"  ><label >Split by Contractor:</label></div>
	<div class="div15" id="conradio" ><input type="radio" name="type_split"  id="conrad"  value="contractor"  CHECKED onclick="hide_workscope();" /></div></div>
<div class="div80" ><label >Split by Work Scope:</label></div><div class="div15" ><input type="radio" name="type_split"   value="crew"  id="crewrad"  onclick="hide_workscope();"  /></div>
</div>
<div class="div27 marg1"  >
	<div class="div50" >
		<div class="div95" style="height:2.5rem;"  >&nbsp;</div>
		<div class="div95" id="show_sub" style="display:none;"  >
			<div class="div65" ><label>Show Sub-Scope:</label></div>
			<div class="div10" ><input type="checkbox" name="subscope" id="subscope"  value="true" CHECKED disabled /></div>
		</div>
	</div>
	<div class="div50" >
		<div class="div95 marg8" id="work_scope_div"  >
FIN;
			 $content .= AdminFunctions::crewParentSelect(NULL,"",true);
			 $content .= <<<FIN
		</div>
	</div>
</div>
<div class="div8 marg" >
<label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div class="div8 marg2" ><label >End Date:<input type="text" name="enddate" id="edate" class="required date" value="$this->endDate"  /></label></div>
<div style="float:right;width:20%;margin-top:0.4rem;" >
<button class="reset" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div>
</fieldset>
</form>
</div>
<script>
$("#workscope").submit(function(){
   var isFormValid = true;
   $("#workscope input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#workscope .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$("#contractor").change(function() {
		showSplit();
});


</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
 }
?>
