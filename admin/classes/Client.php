<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Client extends Address {
		public $page;
		public $clientID;
		public $clientName;
		public $clientFirstName;
		public $clientCardID;
		public $clientABN;
		public $address_type="cli";
		public $action;
		public $contact;
		public $rateID;
		public $labAmount="Per Tonne";
		public $pageNo;
		public $rowsPerPage = 100;
		public $totPages;

		public function	__construct($action,$clientID=0,$search="",$pageno=1) {
			parent::__construct();
			$this->clientID = $clientID;
			$this->action = $action;
			$this->pageNo = $pageno;
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('client');
				$this->page= $pg->page;
				$heading_text = "Administration<BR />";
         	switch($action) {
					case "new" :
						$heading_text .= "Add New Client";
						break;
					case "update" :
						if ($clientID < 1 ) {
							return false;
						}
						$heading_text .= "Updating Client $clientID";
						$this->getClientDetails($clientID);
						break;
					case "delete" :
						if ($clientID < 1 ) {
							return false;
						}
						$heading_text .= "Delete Client $clientID";
						$this->getClientDetails($clientID);
						break;
					case "list" :
						$heading_text .=  strlen($search) > 0 ? "List Clients - $search" : "List Clients - All";
						$this->getAllClients($search) ;
						break;
					case "find" : 
						$heading_text .= "Client $clientID";
						$this->getClientDetails($clientID);
						break;
					default:
						$heading_text .= "Add New Client";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->clientID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Client";
						break;
					case "update":
						$button = "Update Client";
						break;
					case "delete":
						$button = "Delete Client";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Client";
						break;
				}
			$content = <<<FIN
<div style="width:1800px;text-align:center;"><div style="width:1466px;margin:auto;">
<form name="client" class="clientform" method="post" action="client.php">
<input type="hidden" name="client_id" id="clientID" value="$this->clientID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="margin-top:15px;"><legend style="font-size:130%;margin-left:650px;">Client</legend>
<div class="divthird"><label for="client_name" class="label" >Name</label><input type="text" name="client_name" id="client_name" class="input required $DISABLED" value="$this->clientName" $DISABLED /></div>
<label for="client_firstname" class="label" >First Name</label><input type="text" name="client_fname" id="client_fname" class="input  $DISABLED" value="$this->clientFirstName" $DISABLED />
<label for="abn" class="label abn" >A.B.N.</label><input type="text" name="abn" id="abn" value="$this->clientABN" class="input abn $DISABLED" $DISABLED />
<label for="cardno" class="label sml" >Card ID</label><input type="text" name="cardno" id="cardno" value="$this->clientCardID" class="input abn $DISABLED" $DISABLED />
<div style="clear:both;height:10px;"></div>
<div class="divfifth" >
<label for="rate" class="label"  >Rate</label>
FIN;
$content .= AdminFunctions::rateSelect($this->rateID,$DISABLED);
//$content .= "</div><div class=\"divfifth\" ><label id=\"amountlab\"  class=\"label wide red\" >$this->labAmount</label>\n";
$content .= "</div></fieldset>\n";

				$content .= $this->addressDiv($DISABLED,true,true,$this->clientID,$action);   // Always start this way !!!

				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"$button\" class=\"submitbutton L\" />"; // no submit for found client
				}
				$content .= <<<FIN
<script>
$(".clientform").submit(function(){
    var isFormValid = true;
    $(".clientform input:text.input.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
jQuery('#rate_amount').keyup(function () { this.value = this.value.replace(/[^0-9\.]/g,''); });
</script>
FIN;
				 $content .= "\n</form>\n";
			}
			else {
				$content = "";
			}
			$content .= "</div></div>\n"; // Center Div
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllClients($search="") {
			$offset = $this->rowsPerPage * ($this->pageNo - 1);
			$whereClause = "";
			if (strlen($search) > 0 ) {
				$whereClause = " and upper(c.client_name) like '$search%' ";

			}
			 $sql = "SELECT count (*) from client c where 1 = 1 $whereClause";
         //echo "$sql <br />";
         if (! $tot = $this->conn->getOne($sql)) {
            if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
         }
         $this->totPages = ceil($tot/$this->rowsPerPage);


			$sql = "with toget as (select cl.client_id, min (ct.contact_id) as contact_id  from client cl
			LEFT JOIN contact ct using (client_id) group by client_id) 
			SELECT toget.client_id,c.client_name || ' ' ||coalesce(client_firstname,'') as client_name ,ad.address_line_1 || ' ' || ad.address_line_2 || ' ' || ad.suburb || ' ' || st.state_name as address,telephone,mobile,email,
			cc.contact_firstname|| ' ' || cc.contact_lastname as contact 
			FROM client c
			LEFT JOIN toget using (client_id)
			LEFT JOIN contact cc using (contact_id)
			LEFT JOIN address_to_relation ar on ar.client_id =  toget.client_id
			LEFT JOIN address ad on ad.address_id = ar.address_id 
			LEFT JOIN state st  using (state_id)
			where c.removed='f' and ar.address_type = 'cli'  
			$whereClause
			order by client_name limit $this->rowsPerPage offset $offset";

         //echo $sql;

			if (! $rs=$this->conn->Execute($sql)) {
				die( $this->ErrorMsg());
			}
			$content = "<div style=\"width:1800px;text-align:center;\"><div style=\"width:1366px;margin:auto;\">";
			$content .= AdminFunctions::AtoZ("client.php?action=list",$search);
			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= AdminFunctions::paginator($this->pageNo,$this->totPages,"client.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:10px;\"></div>\n";

			$content .= "<div class=\"heading\" ><div class=\"cli f hd\">Client Name</div><div class=\"addr hd\">Address</div><div class=\"fone hd\">Telephone</div><div class=\"fone hd\">Mobile</div>";
			$content .= "<div class=\"email hd\">Email</div><div class=\"contact hd\">Contact</div><div class=\"links hd\" >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "line2";
				$client_id = $rs->fields['client_id'];
				$client_name = strlen($rs->fields['client_name']) > 2 ? $rs->fields['client_name'] : " ";
				$address = strlen($rs->fields['address']) > 2 ? $rs->fields['address'] : " ";
				$telephone = strlen($rs->fields['telephone']) > 2 ? $rs->fields['telephone'] : " ";
				$mobile = strlen($rs->fields['mobile']) > 2 ? $rs->fields['mobile'] : " ";
				$email = strlen($rs->fields['email']) > 2 ? $rs->fields['email'] : " ";
				$contact = strlen($rs->fields['contact']) > 2 ? $rs->fields['contact'] : " ";
				$content .= "<div class=\"cli f $ln\"><a href=\"client.php?action=find&amp;client_id=$client_id\" title=\"View Client\" class=\"name\" >$client_name</a></div><div class=\"addr $ln\">$address</div><div class=\"fone $ln\">$telephone</div><div class=\"fone $ln\">$mobile</div>";
				$content .= "<div class=\"email $ln\">$email</div><div class=\"contact $ln\">$contact</div>";
            $content .= "<div class=\"links $ln\" ><a href=\"client.php?action=update&client_id=$client_id\" title=\"Edit Client\" class=\"linkbutton\" >EDIT</a><a href=\"client.php?action=delete&client_id=$client_id\" title=\"Delete Client\" class=\"linkbutton d\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
				$lineNo += 1;
				//$content .= "<div style=\"clear:both;\" > </div>";
				$rs->MoveNext();
			}
			$content .= "</div></div><hr />";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getClientDetails($clientID) {
			$sql = "SELECT c.*,adr.address_id from client c JOIN address_to_relation adr using (client_id)  where client_id = $clientID and adr.address_type = 'cli'";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->clientID = $data['client_id'];
			$this->clientName = $data['client_name'];
			$this->clientFirstName = htmlentities($data['client_firstname']);
			$this->clientABN = $data['abn'];
			$this->clientCardID = $data['card_no'];
			$this->addressID = $data['address_id'];
			$this->rateID = $data['rate_id'];
			switch ($this->rateID) {
				case 1:
				$this->labAmount = "Per Tonne";	
				break;
				case 2:
				$this->labAmount = "Per Km";	
				break;
				case 3:
				$this->labAmount = "Per Hour";	
				break;
				default:
				$this->labAmount = "Unknown";	
				break;
			}

			$this->getAddressDetails($this->addressID,$this->clientID);  // multiple sites
		}
		private function processPost() {
         if ($this->action == "delete" ) {
				//$this->contact->deleteContacts($this->clientID);
				$this->deleteClient();
			}
			$this->ProcessAddressPost();
			$this->clientID = $_POST['client_id'];
			$this->clientName = trim(addslashes($_POST['client_name']));
			$this->clientABN = trim($_POST['abn']);
			$this->clientCardID = trim($_POST['cardno']);
			$this->clientFirstName = trim($_POST['client_fname']);
			$this->rateID = intval($_POST['rate']);
			
			$this->submitForm();
		}
		private function deleteClient() {
			$sql = "UPDATE client set removed = TRUE where client_id = $this->clientID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: client.php?action=list");
			exit;
		}
		private function submitForm() {
			// addresses
			$this->submitAddress();
			if ($this->clientID == 0 ) {
				$sql = "INSERT into client (client_name,abn,removed,client_firstname,card_no,rate_id) values (E'$this->clientName','$this->clientABN',FALSE,E'$this->clientFirstName','$this->clientCardID',$this->rateID) returning client_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
				$this->clientID = $rs->fields['client_id'];


				// first address billing/actual
				$sql = "INSERT into address_to_relation (client_id,address_type,address_id ) values ($this->clientID,'".$this->address_type ."',$this->newAddressID)";
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}
			else { // Update
				$sql = "UPDATE client set client_name = E'$this->clientName',abn = '$this->clientABN',client_firstname = E'$this->clientFirstName',card_no = '$this->clientCardID',
				rate_id = $this->rateID   where client_id = $this->clientID";
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}

			}
			// Do this in all cases
			if (intval($this->siteAddressID) > 0 ) {  // site addresse
				$sql = "INSERT into address_to_relation (client_id,address_type,address_id ) values ($this->clientID,'ste',$this->siteAddressID)";
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}
			if (intval($this->dsiteAddressID) > 0 ) {  // site addresses
				$sql = "INSERT into address_to_relation (client_id,address_type,address_id ) values ($this->clientID,'dst',$this->dsiteAddressID)";
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}
			if (floatval($this->prate) > 0 ) {
         	if (intval($this->tripRateID) > 0 ) {
					$sql = "UPDATE trip_rate set charge_rate = $this->prate where trip_rate_id = $this->tripRateID";
				} 
				else {
					$sql = "INSERT into trip_rate values (nextval('trip_rate_trip_rate_id_seq'),$this->clientID,$this->pickupID,$this->destID,$this->rateID,$this->prate)";
				}

				if (! $res=$this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}

			$this->contact = new Contact($this->clientID);
			$this->contact->processContactPost();
			$this->contact->submitContacts("cli",$this->clientID);

         // exit;

			header("LOCATION: client.php?action=find&client_id=$this->clientID");
			exit;
			
		}

	}
?>
