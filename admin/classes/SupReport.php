<?php  // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class SupReport  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;

		public function	__construct($action,$conID=NULL,$startDate=NULL,$endDate=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= !is_null($startDate)  ?  $startDate : AdminFunctions::yesterday();
         $this->endDate= ! is_null($endDate)  ?  $endDate  :  NULL;
		 	$this->contractorID=$conID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			elseif (isset($_POST['PRINT'])) {
				$URL="https://".$_SERVER['HTTP_HOST'] ."/printSupRecords.php?action=$this->action&con_id=$this->contractorID&startdate=$this->startDate";
				if (strlen($endDate) > 0 ) {
					$URL .= "&enddate=$this->endDate";
				}
				header("LOCATION: $URL");
				exit;
			}
			else {
				$pg = new Page('Area');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Print Superintendent Work Record Reports";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print Report\" class=\"button marg\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1400px;margin:auto;">
<form name="supreport" id="supreport" method="post" action="sup_work_reports.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<fieldset ><legend style="margin-left:40%;">Filter Search - Print SuperIntendent Work Records</legend>
<div class="div12">
FIN;
   $content .= AdminFunctions::contractorSelect($this->contractorID,"",false);
	$content .= <<<FIN
</div>
<div class="div9 marg" >
<label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label> </div>
<div class="div9 marg" > <label>End Date:<input type="text" name="enddate" id="edate" class="required date" value=""  /></label> </div>
<div class="div12 marg" >
<div class="div50" ><label >Print</label></div><div class="div50" ><input type="radio" value="print" name="action" id="cnsplit" checked /></div>
<div class="div95" style="height:1rem;" ></div>
<div class="div50" ><label>Email</label></div><div class="div50" ><input type="radio" value="email" name="action" id="crsplit" /></div>
</div>
<div style="float:right;width:22%;" >
<button class="reset" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div>
</fieldset>
</form>
</div>
<script>
$("#supreport").submit(function(){
   var isFormValid = true;
   $("#wellreport input:text.input.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#supreport .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
	 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
 }
?>
