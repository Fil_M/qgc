<?php

  class Address {
	public $addressID=0;
	public $address_line_1;
	public $address_line_2;
	public $suburb;
	public $city;
	public $state=4;
	public $siteAddressIDs = array();
	public $sites; // array of address details
	public $mobile;
	public $telephone;
	public $fax;
   public $postcode;
	public $email;
	public $conn;
	public $address_div;
	public $contact;
	public $contactDiv;
	public $siteContact;
	public $siteContactDiv;


	public function __construct() {
		$this->conn = $GLOBALS['conn'];
		$this->contact = new Contact();
		/*$this->contact->getContactDetails('cli');
		$this->contact->contactDiv($DISABLED);
		$this->contactDiv = $this->contact->contactDv;  */
	}

   public function submitAddress() {
		if(isset($_POST['siteaddr1'])) {
			$siteAddr1 = $_POST['siteaddr1'];
			$siteAddr2 = $_POST['siteaddr2'];
			$siteSuburb = $_POST['sitesuburb'];
			$siteCity = $_POST['sitecity'];
			$siteState = $_POST['sitestate'];
			$sitePostcode = $_POST['sitepostcode'];
			$sconFirst = $_POST['scon_first'];
			$sconLast = $_POST['scon_last'];
			$sconEmail = $_POST['scon_email'];
			$sconFone = $_POST['scon_fone'];
		}
		if (isset($_POST['usiteaddr1'])) {  // Updates
			$uaddrID = $_POST['uaddress_id'];
			$usiteAddr1 = $_POST['usiteaddr1'];
			$usiteAddr2 = $_POST['usiteaddr2'];
			$usiteSuburb = $_POST['usitesuburb'];
			$usiteCity = $_POST['usitecity'];
			$usiteState = $_POST['usitestate'];
			$usitePostcode = $_POST['usitepostcode'];
			$usconFirst = $_POST['uscon_first'];
			$usconLast = $_POST['uscon_last'];
			$usconEmail = $_POST['uscon_email'];
			$usconFone = $_POST['uscon_fone'];
			$usconID = $_POST['uscontact_id'];
		}
		if ($this->addressID == 0 ) {
			$sql = "INSERT into address values (nextval('address_address_id_seq'),E'$this->address_line_1',E'$this->address_line_2','$this->suburb','$this->city','$this->telephone','$this->mobile','$this->fax','$this->postcode','$this->email',$this->state) returning address_id";

			if (! $rs=$this->conn->Execute($sql)) {
				die( $this->conn->ErrorMsg());
			}
			$this->addressID = $rs->fields['address_id'];

		}
		else {   // normal address  UPDATES
			$sql = "UPDATE address set address_line_1 = E'$this->address_line_1',address_line_2 = E'$this->address_line_2',suburb = '$this->suburb',city = '$this->city',telephone='$this->telephone',mobile='$this->mobile',fax='$this->fax',postcode='$this->postcode',email='$this->email',state_id = $this->state where address_id = $this->addressID";
			
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			if (isset($uaddrID)) {
				foreach ($uaddrID as $ind => $val) {
					$a1 = $usiteAddr1[$ind];
					$a2 = $usiteAddr2[$ind];
					$as = $usiteSuburb[$ind];
					$ac = $usiteCity[$ind];
					$ast = $usiteState[$ind];
					$ap = $usitePostcode[$ind];
					$cf = $usconFirst[$ind];
					$cl = trim(addslashes($usconLast[$ind]));
					$ce = $usconEmail[$ind];
					$cfn = $usconFone[$ind];
					$conID = intval($usconID[$ind]);
					$sql = "UPDATE address set address_line_1 = E'$a1',address_line_2 = E'$a2',suburb = '$as',city = '$ac',state_id = '$ast',postcode='$ap'  where address_id = $val";
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}  
					if (strlen($cf . $cl .$ce .$cfn ) > 0 ) {
						if ($conID > 0 ) {
							$sql = "UPDATE contact set contact_firstname = '$cf',contact_lastname= E'$cl',contact_email='$ce',contact_fone='$cfn' where contact_id = $conID";
						}
						else {
							$sql = "INSERT into contact (contact_firstname,contact_lastname,contact_email,contact_fone,address_id) values ('$cf',E'$cl','$ce','$cfn',$val)";
						}
					}	
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}  
				}
			}
		}
      // Insert New Sites
		if (isset($siteAddr1)) {
			foreach ($siteAddr1 as $ind=>$val) {
				$s1 = $siteAddr1[$ind];
				$s2 = $siteAddr1[$ind];
				$ss = $siteSuburb[$ind];
				$sc = $siteCity[$ind];
				$sp = $sitePostcode[$ind];
				$st = $siteState[$ind];
				$con1 = $sconFirst[$ind];
				$con2 = trim(addslashes($sconLast[$ind]));
				$cone = $sconEmail[$ind];
				$conf = $sconFone[$ind];
				if (strlen($s1 . $s2 . $ss . $sc .$sp ) > 5 ) {  //  site address
					$sql = "INSERT into address (address_line_1,address_line_2,suburb,city,state_id,postcode) values (E'$s1',E'$s2','$ss','$sc','$st','$sp') returning address_id";
					if (! $rs=$this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
				}
				$addressID = intval($rs->fields['address_id']);
				$this->siteAddressIDs[] = $addressID;

				if (strlen($con1 . $con2) > 2 ) {
					$sql = "INSERT into contact (contact_firstname,contact_lastname,contact_email,contact_fone,address_id) values ('$con1',E'$con2','$cone','$conf',$addressID)";
					if (! $rs=$this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
				} 
			}
		}
	}

	public function getAddressDetails($addressID,$clientID) {
		$sql = "select * from address where address_id = $addressID";
		if (! $data=$this->conn->getRow($sql)) {
			if ($this->conn->ErrorMsg() != "" ) {
				die($this->conn->ErrorMsg());
			}
		}
		$this->addressID = isset($data['address_id']) ? intval($data['address_id']) : 0;
		if ($this->addressID > 0 ) {
			$this->address_line_1 = $data['address_line_1'];
			$this->address_line_2 = $data['address_line_2'];
			$this->suburb = $data['suburb'];
			$this->city = $data['city'];
			$this->state = $data['state_id'];
			$this->telephone = $data['telephone'];
			$this->mobile = $data['mobile'];
			$this->fax = $data['fax'];
			$this->postcode = $data['postcode'];
			$this->email = $data['email'];
		}
		if ($clientID > 0 ) {
			// any sites  and site contacts??
			$sql ="select a.*,c.contact_id,c.contact_firstname,c.contact_lastname,c.contact_email,c.contact_fone from address a
			LEFT JOIN contact c using (address_id) 
 			where address_id in ( select address_id from address_to_relation where client_id = $clientID and address_type = 'ste')
 			and coalesce(c.removed,false) is false";
			if (! $this->sites=$this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
		}
	}

	public function processAddressPost() {
		$this->addressID = $_POST['address_id'];
		$this->address_line_1 = trim(addslashes($_POST['addr1']));
		$this->address_line_2 = trim(addslashes($_POST['addr2']));
		$this->suburb = trim($_POST['suburb']);
		$this->city = trim($_POST['city']);
		$this->state = trim($_POST['state']);
		$this->telephone = trim($_POST['telephone']);
		$this->mobile = trim($_POST['mobile']);
		$this->fax = trim($_POST['fax']);
		$this->postcode = trim($_POST['postcode']);
		$this->email = trim($_POST['email']);
  /// site address yes or no 
	}
	public function addressDiv($DISABLED="",$showSite=false,$showContact=false,$clientID=0) {
		$content =  <<<FIN
<fieldset  ><legend style="margin-left:45%;">Address Details</legend>
<input type="hidden" name="address_id" value="$this->addressID" />
<div class="div32" > <label for="addr1" >Address 1<input type="text" name="addr1" id="addr1" value="$this->address_line_1"  $DISABLED/></label> </div>
<div class="div32 marg2 "> <label for="addr2"  >Address 2<input type="text" name="addr2" id="addr2" value="$this->address_line_2"  $DISABLED /></label> </div>
<div class="div31 marg2" > <label for="suburb"  >Suburb<input type="text" name="suburb" id="suburb" value="$this->suburb" $DISABLED/></label> </div>
<div style="clear:both;"> </div>
<div class="div32" > <label for="city"   >City<input type="text" name="city" id="city" value="$this->city"  $DISABLED /></label> </div>
<div class="div10 marg2" ><label for="state"  >State</label>
FIN;
$content .= AdminFunctions::stateSelect($this->state,$DISABLED,"state");
$content .= <<<FIN
</div>
<div class="div5 marg2" >&nbsp;</div>
<div class="div10 marg" ><label for="postcode" >Postcode<input type="text" class="cntr" name="postcode" id="postcode" value="$this->postcode"  $DISABLED /></label></div>
<div style="clear:both;" ></div>
<div class="div32" > <label for="email"  >Email<input type="text" name="email" id="email" value="$this->email"  $DISABLED /></label> </div>
<div class="div10 marg2" > <label for="telephone" >Telephone<input type="text" name="telephone" id="telephone" value="$this->telephone"  $DISABLED /></label> </div>
<div class="div10 marg1"> <label for="mobile"  class="label" >Mobile<input type="text" name="mobile" id="mobile" value="$this->mobile" $DISABLED /></label> </div>
<div class="div10 marg1" > <label for="fax"  >Fax<input type="text" name="fax" id="fax" value="$this->fax"  $DISABLED /></label> </div>
FIN;
 if ($showContact) {
		$this->contact = new Contact($clientID);
      $this->contact->getContactDetails('cli');
      $this->contact->contactDiv($DISABLED);
      $this->contactDiv = $this->contact->contactDv;
 		$content .= $this->contactDiv;
 }
 $content .= "</fieldset>";
		if ($showSite) {
			$content .= "<fieldset style=\"margin-top:20px;\"><legend style=\"font-size:130%;margin-left:560px;\">Site Details</legend>";
			$content .= "<div id=\"ste\" >";
			if (count($this->sites) > 0 ) {
			 foreach ($this->sites as $ind=> $val) {
				$content .= "<input value=\"".$val['address_id'] ."\" name=\"uaddress_id[]\" type=\"hidden\" />\n";
				$content .= "<div class=\"divthird\">";
				$content .= "<label for=\"siteaddr1\" class=\"label\" >Site Addr1</label><input type=\"text\" name=\"usiteaddr1[]\" id=\"siteaddr1\" value=\"" .$val['address_line_1'] ."\" class=\"input $DISABLED\" $DISABLED/>\n";
				$content .= "</div><div class=\"divthird\" >\n";
				$content .= "<label for=\"siteaddr2\" class=\"label\" >Site Addr2</label><input type=\"text\" name=\"usiteaddr2[]\" id=\"siteaddr2\" value=\"".$val['address_line_2'] ."\" class=\"input $DISABLED\" $DISABLED />\n";
				$content .= "</div><div class=\"divthird\" >\n";
				$content .= "<label for=\"sitesuburb\" class=\"label\" >Site Suburb</label><input type=\"text\" name=\"usitesuburb[]\" id=\"sitesuburb\" value=\"".$val['suburb'] ."\" class=\"input $DISABLED\" $DISABLED/>\n";
				$content .= "</div>\n";
				$content .= "<div style=\"clear:both;height:10px;\"> </div>\n";
				$content .= "<div class=\"divthird\">\n";
				$content .= "<label for=\"sitecity\"  class=\"label\" >Site City</label><input type=\"text\" name=\"usitecity[]\" id=\"sitecity\" value=\"".$val['city'] ."\" class=\"input $DISABLED\" $DISABLED />\n";
				$content .= "</div><div class=\"divthird\" >\n";
				$content .= "<label for=\"sitestate\"  class=\"label\" >Site State</label>\n";
				$content .= AdminFunctions::stateSelect($val['state_id'],$DISABLED,"usitestate[]");
				$content .= "<label for=\"sitepostcode\"  class=\"label\" style=\"width:93px;\" >Site Pcode</label><input type=\"text\" name=\"usitepostcode[]\" id=\"sitepostcode\" value=\"".$val['postcode'] . "\"";
				$content .= " class=\"input tiny $DISABLED\" $DISABLED />\n";
				$content .= "</div>\n";
				$content .= "<div style=\"clear:both;height:5px;\"></div>\n";
				$content .= "<div class=\"centerDiv\" >Site Contact Details</div>\n";
				$content .= "<input value=\"".$val['contact_id'] ."\" name=\"uscontact_id[]\" type=\"hidden\" />\n";
				$content .= "<label for=\"scon_first\" class=\"label\">First Name</label><input name=\"uscon_first[]\" id=\"scon_first\" value=\"".$val['contact_firstname'] ."\" class=\"input dest $DISABLED\" type=\"text\" $DISABLED/>\n";
				$content .= "<label for=\"scon_last\" class=\"label\">Last Name</label><input name=\"uscon_last[]\" id=\"scon_last\" value=\"".$val['contact_lastname'] ."\" class=\"input dest $DISABLED\" type=\"text\" $DISABLED/>\n";
				$content .= "<label for=\"scon_email\" class=\"label abn\">Email</label><input name=\"uscon_email[]\" id=\"scon_email\" value=\"".$val['contact_email'] ."\" class=\"input dest $DISABLED\" type=\"text\" $DISABLED />\n";
				$content .= "<label for=\"scon_fone\" class=\"label abn\">Phone</label><input name=\"uscon_fone[]\" id=\"scon_fone\" value=\"".$val['contact_fone'] ."\" class=\"input phone $DISABLED\" type=\"text\"  $DISABLED/>\n";
				$content .= "<hr style=\"margin-top:10px;\" >\n";
		  }
		}
			$content .= "\n</div><!-- ste -->";  // close  ste
			if ($DISABLED != "disabled" ) {
         	$content .= "<div class=\"buttonDiv\"><button class=\"addButton\"  onClick=\"addSite();return false;\">Add Site</button></div>";
      	}
			$content .= "</fieldset>";

		}
	$content .= "<script> $(document).on('keyup','.input.tiny', function(){ this.value = this.value.replace(/[^0-9]/g,''); });</script>";
	return $content;
	}
}
?>
