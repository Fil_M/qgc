<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class T2ReportAll  {
		public $page;
		public $action;
		public $data = array();
		public $conn;
		public $employeeID;
		public $startDate;
		public $endDate;
		public $connArr;
		public $statusText = array("0"=>"Newly Entered","1"=>"Awaiting Approval","2"=>"REJECTED","3"=>"Late Submission","4"=>"Rejected Entry","5"=>"Approved","6"=>"Requested Completion",
                              "7"=>"Approved Completion","8"=>"Invoiced","9"=>"Paid");
		public $lineCount = 0;


		public function	__construct($action,$startDate,$endDate) {
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
			$this->startDate = !empty($startDate) ? $startDate : AdminFunctions::beginLastMonth();
			$this->endDate = !empty($endDate) ? $endDate : AdminFunctions::endLastMonth(); 
			$this->conn = $GLOBALS['conn'];
			$this->today = date('d-m-Y');
			if (isset($_POST['PRINT'])) {
				$file= "tmp/T2_Report_{$this->startDate}_{$this->endDate}_{$this->employeeID}.csv";
		  		$this->fp = fopen($file,'w');
				$this->getData();
				fclose($this->fp);
				$this->printFile($file);
	
			}
			else {
				$pg = new Page('coo');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "T2 Report $this->startDate - $this->endDate";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print\" class=\"button\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}
		private function printFile($file) {
			if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file)); //Remove
           	readfile($file);
           	exit;
      	}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:800px;margin:auto;">
<form name="t2_all" id="well_all" method="post" action="t2ReportAll.php?action=$action">
<fieldset ><legend style="margin-left:33%;" id="head" ></legend>
<div class="div20 marg1" ><label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate" onChange="doLegend();"  /></label></div>
<div class="div20 marg" ><label >End Date:<input type="text" name="enddate" id="edate" class="required date" value="$this->endDate" onChange="doLegend();" /></label></div>
<div style="float:right;width:18%;" >
$button
</div>
</fieldset>
</form>
</div>
<script>
$(document).ready(function() {
	doLegend();
});
function doLegend() {
	var startD = $('#sdate').val();
	var endD = $('#edate').val();
	$('#head').text('T2 Report from 	' + startD + ' - ' + endD);
}
$(function(){
 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	private function getData() {
		$whereClause = " WHERE total_t2 is not null and hour_date between '$this->startDate' and '$this->endDate' ";
		$tmpArr = array();
		$this->connArr = AdminFunctions::getAllCon();
		foreach($this->connArr as $ind=>$val)  {
			extract($val);
				 $sql = "SELECT hour_date,docket_hours_t2,rate,total_t2,docket_num,status,reject_text,calloff_order_id,enter_time,details,
					coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,pt.p_type,plant_unit,wells_from_ids(h.well_ids) as well_name,a.area_name,c.crew_name,sub_crew_name
                   from {$name}_hour h
                  LEFT JOIN {$name}_plant p using (plant_id)
                  LEFT JOIN plant_type pt using (plant_type_id)
                  LEFT JOIN area a using (area_id)
                  LEFT JOIN {$name}_employee e using (employee_id)
                  LEFT JOIN crew c  using (crew_id)
                  LEFT JOIN sub_crew sc  using (sub_crew_id)
                  $whereClause  order by hour_date";  


      	if (! $data = $this->conn->getAll($sql)) {
         	if ($this->conn->ErrorNo() != 0 ) {
           		die($this->conn->ErrorMsg());
        		}
				else {
					continue;
				}
     		}
				//$headings = array('Contractor','Date','Enter Time','Docket','C.O.O.','Docket Hours T2','Rate T2','Total T2','Operator','Unit','Plant/Type','Area','Well/s','Detail','Work Scope','Sub Scope','Status','Reject Reason');
			$headings = array('Contractor','Date','Enter Time','Docket','C.O.O.','Docket Hours T2','Rate T2','Total T2','Operator','Unit','Plant/Type','Area','Well/s','Detail','Work Scope','Sub Scope','Status','Reject Reason');
			if (count($data) > 0 ) {
         		fputcsv($this->fp,$headings);
					$this->lineCount++;
					$tmpArr = array($con_name);
					fputcsv($this->fp,$tmpArr,",",'"');
					$this->lineCount++;
					$startLine = $this->lineCount +1;
				foreach($data as $ind=>$val) {
					extract($val);
					$hour_date = AdminFunctions::dbDate($hour_date);
					$eTime = AdminFunctions::dbTime($enter_time);
					$stat = $this->statusText[$status];
					$tmpArr = array("",$hour_date,$eTime,$docket_num,$calloff_order_id,$docket_hours_t2,$rate,$total_t2,$opname,$plant_unit,$p_type,$area_name,$well_name,$details,$crew_name,$sub_crew_name,$stat,$reject_text);
					fputcsv($this->fp,$tmpArr,",",'"');
					$this->lineCount++;
				}

			}
			fputcsv($this->fp,array("","","","","",'=ROUND(SUM(F'.$startLine.':F'.$this->lineCount.'),2)',"",'=ROUND(SUM(H'.$startLine.':H'.$this->lineCount.'),2)'));
			fputcsv($this->fp,array());
			fputcsv($this->fp,array());
			$this->lineCount+= 3;  
		}

	}
}
?>
