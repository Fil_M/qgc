<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class COOSummary  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $areaName;
		public $areaID;
		public $wellID;
		public $cooID;
		public $crewID;
		public $contractorName;
		public $shortName;
		public $conDomain;
		public $dbLink;
		public $uploads;
		public $T2Dates;
		public $expenses;
		public $siteIns;
		public $CompletionCert;
		public $compID;


		public function	__construct($action,$cooID=NULL,$conID=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->contractorID = $conID;
			$this->cooID = $cooID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			$pg = new Page('Summary');
			$this->page= $pg->page;
			if (isset($_REQUEST['SHOW'])) {
				if (!is_null($conID)) {
					$connArr = AdminFunctions::getConName($conID);
            	$this->contractorName = $connArr['con_name'];
            	$this->shortName = $connArr['name'];
            	$this->conDomain = $connArr['domain'];
            	$this->dbLink = $connArr['db_link'];
				}
				$heading_text = "Calloff Order Summary - $cooID for $this->contractorName";
				$this->setHeaderText($heading_text);
				$this->getSummary();
         }
			else {
				if ($action == "show" ) {
					$heading_text = "Show Calloff Order Summary";
					$button = "<input type=\"submit\" name=\"SHOW\" value=\"Show Summary\" class=\"button\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1650px;margin:auto;">
<form name="coo_summary" id="coo_summary" method="post" action="coo_summary.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="area_id" id="areaID"  />
<fieldset ><legend style="margin-left:43%;">Filter Search - COO Summary</legend>
<div class="div10" ><label for="req_id" >Call Off Order<input type="text" name="coo_id" id="coo" value="$this->cooID" class="required" /></label></div>
<div class="div5 marg1" ><label class="cntr" >Or:</label></div>
<div class="div17 marg1" >

FIN;
         $content .= AdminFunctions::contractorSelect($this->contractorID,"",true);
			$content .= "</div><div class=\"div16 marg1\" ><label>WorkScope</label>\n";
         $content .= AdminFunctions::crewSelect(NULL,"",true);
         $content  .= <<<FIN
</div>
<div class="div16 marg1" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div16 marg1" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<div style="float:right;width:12%;" >$button</div>
</fieldset>
<script>
$("#coo_summary").submit(function(){
    var isFormValid = true;
    $("#coo_summary .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),$('#contractor')); return false; }
      });
  });
$(function () {
      $('#coo').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=coo',
         minChars:2,
         onSelect: function(value, data){ $('#contractorID').val(data[0]); $('#contractor').val(data[0]);$('#crew').val(data[3]);$('#area').val(data[1]);wellDiv(data[2],$('#welldiv')); return false; }
      });
  });
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
	getCOO(this);
});
$('#area').change(function() {
	getCOO(this);
});
$('#crew').change(function() {
	getCOO(this);
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$content .= "</form></div>\n";
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	public function getSummary() {
		$URL="https://".$_SERVER['HTTP_HOST'];
		// Expenses
		$sql = "select h.hour_date,h.expense,details,h.hour_id,u.upload_name from {$this->shortName}_hour h
		LEFT JOIN {$this->shortName}_upload u on ( u.hour_id = h.hour_id and u.upload_type_id = 1 ) 
 		where calloff_order_id = $this->cooID and expense is not null 
	   order by h.hour_date";

		if (! $this->expenses = $this->conn->getAll($sql)) {
			if($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
		}
		// T2's
		$sql = "select distinct(hour_date) from {$this->shortName}_hour where calloff_order_id = $this->cooID and total_t2 is not null order by hour_date";
		if (! $this->T2Dates = $this->conn->getAll($sql)) {
			if($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
		}
		// Contractor Invoice
		$sql = "SELECT cc.completion_cert_id,u.upload_name from {$this->shortName}_completion_cert  cc
		LEFT JOIN {$this->shortName}_upload u on ( u.completion_cert_id = cc.completion_cert_id and u.upload_type_id = 13 ) 
		where qgc_approve_date is not null and calloff_order_id = $this->cooID";
		if (! $this->completionCert = $this->conn->getRow($sql)) {
			if($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
			else {
				$this->completionCert = 0;
			}
		}
		// Site Instructions
		$sql = "SELECT si.site_instruction_id,si.comments,instruction_date,instruction_no 
				from {$this->shortName}_site_instruction si 
				LEFT JOIN {$this->shortName}_field_estimate fe using(site_instruction_id) 
				where si.calloff_order_id = $this->cooID and si.removed is false and fe.qgc_approve_date is not null ";
		if (! $this->siteIns = $this->conn->getAll($sql)) {
			if($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
		}

		$content = <<<FIN
<div style="width:1440px;margin:auto;">
<fieldset ><legend style="margin-left:47%;">Expenses</legend>
		<div class="heading"  >
		<div class="fone hd" >Date</div><div class="usr hd padr" >Expense</div><div class="location narr hd" >Detail</div><div class="well hd bdr" >Upload</div>
		</div>
FIN;
		$divNo = $lineNo=0;
		$ln = "line1";
// Expenses
	if (count($this->expenses) > 0 ) {
		foreach($this->expenses as $ind=>$val) {
			if ($divNo % 2 == 0 ) {
				 $ln = "line1";  
			}
			else  {
				 $ln = "";  
			}
			$upName = $val['upload_name'];
			$details = $val['details'];
			$hDate = AdminFunctions::dbDate($val['hour_date']);
			$expense = number_format($val['expense'],2);
			$name = basename($upName);
			$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;border-bottom:1px solid #000000;\" class=\"$ln\"  >\n";
			$content .= "<div class=\"fone cntr highh\" >$hDate</div><div class=\"usr txtr highh\" >$expense</div><div class=\"location narr highh\" >$details</div><div class=\"well bdr highh\" >\n";
			if (strlen($upName) > 3 ) {
				$content .= "<a href=\"$this->conDomain/admin/$upName\"  target=\"__blank\" class=\"linkbutton\"   >$name</a>\n";
			}
			else {
				$content .= "No Expense Upload Found";
			}
			$content .= "</div> <!-- closes email --> \n";
			$content .= "</div>";
			$content .= "<div style=\"clear:both;\" ></div>";
			$lineNo ++;

		}
	}
		$content .= "</fieldset>\n";
		$content .= <<<FIN
		<fieldset ><legend style="margin-left:45%;">Site Instructions</legend>
	 	<div class="heading"  >
      <div class="fone hd" >Date</div><div class="usr hd padr" >Site Instruction No</div><div class="location narr hd" >Comments</div><div class="well hd bdr" >Print Site Instruction</div>
      </div>
FIN;
		$divNo =$lineNo=0;
		$ln = "line1";
		foreach($this->siteIns as $ind=>$val) {
			$comments = $val['comments'];
			$siDate = AdminFunctions::dbDate($val['instruction_date']);
			$insNo = $val['instruction_no'];
			$siteInsID = $val['site_instruction_id'];
			if ($divNo % 2 == 0 ) {
				 $ln = "line1";  
			}
			else  {
				 $ln = "";  
			}
			if ($lineNo %2 == 0 ) {
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;border-bottom:1px solid #000000;\" class=\"$ln\"  >\n";
			}
			$content .= "<div class=\"fone highh\" >$siDate</div><div class=\"usr cntr padr highh\" >$insNo</div><div class=\"location narr highh\" >$comments</div>\n";
			$content .= "<div class=\"well narr bdr cntr highh\" >\n";
			if (strlen($insNo) > 3 ) {
				$name = "Instruction No. $insNo";
				$content .= "<a href=\"{$URL}/printSiteInstructionpdf.php?action=print&siteins_id=$siteInsID&con_id=$this->contractorID\"  target=\"__blank\" class=\"linkbutton\" style=\"margin-left:4rem;\"  >$name</a>\n";
			}
			else {
				$content .= "";
			}
			$content .= "</div> <!-- closes email --> \n";
			$content .= "</div>";
			$content .= "<div style=\"clear:both;\" ></div>";
			$lineNo ++;
		}
		$content .= "</fieldset>\n";
			$content .= "<div style=\"clear:both;\" ></div>\n";




  //table 2
		if (count($this->T2Dates) > 0 ) {
			$content .= "<fieldset style=\"padding-bottom:1rem;\" ><legend style=\"margin-left:46%;\">Table 2 PDF's</legend>\n";
			foreach($this->T2Dates as $ind=>$val) {
				$hDate = AdminFunctions::dbDate($val['hour_date']);
				$content .= "<div class=\"div125\" ><a href=\"{$URL}/printSupTable2.php?action=print&con_id=$this->contractorID&startdate=$hDate&coo_id=$this->cooID\" target=\"__blank\"  class=\"linkbutton\" >T2: $hDate</a></div>";
			}
			$content .= "<div style=\"clear:both;\" ></div>\n";
			$content .= "\n</fieldset>\n";
		}

			$content .= "<div style=\"clear:both;\" ></div>\n";
// Completion Certificate and Invoice
		$content .= "<fieldset style=\"padding-bottom:1rem;\" ><legend style=\"margin-left:41%;\">Completion Certificate & Invoice</legend>\n";
		$upName = $this->completionCert['upload_name'];
		$name =  "Invoice";
		if (strlen($upName) > 3 ) {
				$content .= "<a href=\"$this->conDomain/admin/$upName\"  target=\"__blank\" class=\"linkbutton\" style=\"width:105px;\"  >$name</a>\n";
		}
		else {
			$content .= "<div class=\"div15\"  ><span class=\"error\"  >No Invoice Upload Found</span></div>\n";
		}
		$this->compID = $this->completionCert['completion_cert_id'];
		if ($this->compID != 0 ) {
			$content .= "<a href=\"{$URL}/printCompletepdf.php?con_id=$this->contractorID&comp_id=$this->compID&action=print\"  class=\"linkbutton\" style=\"width:105px; margin-left:5.5rem;\" target=\"__blank\" >Completion Certificate</a>\n";
		}
		else {
			$content .="<div class=\"div15 marg1\"  ><span class=\"error\"  >No Completion Certificate Found ?</span></div>\n";
		}
		$content .= "</fieldset>\n";

		$content.="<div style=\"clear:both;\" ></div>\n";
		$url = isset($_SESSION['last_request']) ? $_SESSION['last_request'] : "coo_summary.php?action=show";
		$content .= "<a href=\"$url\"   class=\"button\"  >Last Screen</a>";
		$content .= "</div>\n"; 
		$this->page = str_replace('##MAIN##',$content,$this->page);
		echo $this->page;


	}
 }
?>
