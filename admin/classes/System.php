<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class System extends Address {
		public $page;
		public $companyID;
		public $companyName;
		public $companyABN;
		public $contactFirstName;
		public $contactLastName;
		public $contactEmail;
		public $address_type="com";
		public $action;
		public $companyLogo;

		public function	__construct($action) {
			parent::__construct();
			$this->companyID = 0;
			$this->action = $action;
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('company');
				$this->page= $pg->page;
         	switch($action) {
				/*	case "new" :
						$heading_text .= "Add New Company";
						break;  */
					case "update" :
						$heading_text = "Updating System";
						$this->getCompanyDetails($this->companyID);
						break;
				/*	case "delete" :
						if ($companyID < 1 ) {
							return false;
						}
						$heading_text .= "Delete Company $companyID";
						$this->getCompanyDetails($companyID);
						break; */
			/*		case "list" :
						$heading_text .=  strlen($search) > 0 ? "List Companys - $search" : "List Companys - All";
						$this->getAllCompanys($search) ;
						break;   */
					case "find" : 
						$heading_text = "System";
						$this->getCompanyDetails($this->companyID);
						break;
					default:
						$heading_text = "Updating System";
						$this->getCompanyDetails($this->companyID);
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->companyID > 0 )) {
				$DISABLED="";
				switch ($action) {
					/*case "new":
						$button = "Add Company";
						break;  */
					case "update":
						$button = "Update Company";
						break;
				/*	case "delete":
						$button = "Delete Company";
						break;  */
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Update Company";
						break;
				}
			$content = <<<FIN
<div style="width:1240px;margin:auto;">
<form name="system" id="system" method="post" action="system.php?SUBMIT=SUBMIT">
<input type="hidden" name="company_id" value="$this->companyID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:46.5%;" >Company</legend>
<div class="div32"><label for="company_name" >Company Name</label><input type="text" name="company_name" id="company_name"  value="$this->companyName" $DISABLED /></label></div>
<div class="div11" >&nbsp;</div>
<div class="div10 marg2" ><label for="abn"  >A.B.N.</label><input type="text" name="abn" id="abn" value="$this->companyABN"  $DISABLED /></label></div>
<div class="div8" >&nbsp;</div>
<div class="div31 marg"> <label for="logo"  >Logo</label><input type="text" name="logo" id="logo" value="$this->companyLogo"  DISABLED /></label></div>
</fieldset>
FIN;

			$content .= $this->addressDiv($DISABLED); 

			$content .= <<<FIN
<div style="clear:both;"> </div>
<fieldset ><legend style="margin-left:45%;">Contact Details</legend>
<div class="div32" ><label for="con_first"  >First Name<input type="text" name="con_first" id="con_first" value="$this->contactFirstName"  $DISABLED/></label></div>
<div class="div32 marg2" ><label for="con_last"  >Last Name<input type="text" name="con_last" id="con_last" value="$this->contactLastName"  $DISABLED/></label></div>
<div class="div31 marg2" ><label for="con_email" >Email<input type="text" name="con_email" id="con_email" value="$this->contactEmail"  $DISABLED/></label></div>
</fieldset>
<script>
$("#system").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
</script>
FIN;
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found company
				}
				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}

		private function getCompanyDetails($companyID) {
			$sql = "SELECT c.*,adr.address_id from company c JOIN address_to_relation adr using (company_id)  where company_id = $companyID";

			if (! $data = $this->conn->getRow($sql)) {
				echo $this->conn->ErrorMsg();
				exit;
			}
			$this->companyID = $data['company_id'];
			$this->companyName = $data['company_name'];
			$this->companyABN = $data['abn'];
			$this->contactFirstName = $data['contact_firstname'];
			$this->contactLastName = $data['contact_lastname'];
			$this->contactEmail = $data['contact_email'];
			$this->addressID = $data['address_id'];
			$this->companyLogo = $data['company_logo'];


			$this->getAddressDetails($this->addressID,0);
		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteCompany();
			}
			$this->ProcessAddressPost();
			$this->companyID = $_POST['company_id'];
			$this->companyName = trim(addslashes($_POST['company_name']));
			$this->companyABN = trim($_POST['abn']);
		 	$this->contactFirstName = trim($_POST['con_first']);
		 	$this->contactLastName = trim(addslashes($_POST['con_last']));
		 	$this->contactEmail = trim($_POST['con_email']);
			
			$this->submitCompany();
		}
		private function deleteCompany() {
			$sql = "UPDATE company set removed = TRUE where company_id = $this->companyID";
			if (! $this->conn->Execute($sql)) {
				echo $this->conn->ErrorMsg();
				exit;
			}
			header("LOCATION: system.php?action=list");
			exit;
		}
		private function submitCompany() {
			$this->submitAddress();
			if ($this->companyID < 0 ) {
				$sql = "INSERT into company values (nextval('company_company_id_seq'),E'$this->companyName','$this->companyABN',FALSE,'$this->contactFirstName',E'$this->contactLastName','$this->contactEmail') returning company_id";

				
				if (! $rs =  $this->conn->Execute($sql)) {
					echo $this->conn->ErrorMsg();
					exit;
				}
				$this->companyID = $rs->fields['company_id'];
				$sql = "INSERT into address_to_relation (company_id,address_type,address_id ) values ($this->companyID,'".$this->address_type ."',$this->addressID)";

			}
			else { // Update
				$sql = "UPDATE company set company_name = E'$this->companyName',abn = '$this->companyABN',contact_firstname= '$this->contactFirstName',contact_lastname = E'$this->contactLastName', contact_email = '$this->contactEmail' where company_id = $this->companyID";

			}
			if (! $this->conn->Execute($sql)) {
				echo $this->conn->ErrorMsg();
				exit;
			}

			header("LOCATION: system.php?action=find");
			exit;
			
		}

	}
?>
