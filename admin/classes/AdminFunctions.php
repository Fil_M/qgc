<?php
require_once("../classes/Functions.php");
	class AdminFunctions extends Functions {
		static function inClauseFromArray($hourArr) {
			$clause = "(";
			foreach ($hourArr as $ind=>$val) {
				$clause .= $val . ",";
			}
			$clause = preg_replace('/,$/',")",$clause);
			return $clause;
		}
		static function cutString($str,$length ) {  //  always  breaks on whole word
			$string = $str;
			if (strlen($str) > $length ) {
            $string= substr($str,0,$length);
				$string = substr($string,0,strrpos($string, " "))."...";
         }
			return $string;
		}
		static function splitStr($str) {
			return " ".preg_replace('/;/',"\n",$str);
		}
		 static function minOneMonth() {
         $currDay = date('d');
         $now = intval(date('U')) - 86400;
         while (date('d',$now) != $currDay) {
           $now -= 86400;
         }

         return $now;
      }


		 static function checkCOO($cooID,$conID) {
         global $conn;
         $answer = false;
         $sql = "select calloff_order_id from calloff_order where contractor_id = $conID and calloff_order_id = $cooID";
         if (! $data = $conn->getAll($sql)) {
            if ($conn->ErrorNo() != 0 ) {
               die($conn->ErrorMsg());
            }
            else {
               return false;
            }
         }
         /*foreach($data as $ind=>$coo) {
            extract($coo);
            if ($calloff_order_id == $cooID ) {
               $answer=true;
               break;
            }

         } */

         return true;
      }

		static function getMessage() {
			global $conn;
			$div = "";
			$now = time();
			if (!isset($_SESSION['employee_id'])) {
				return $div;
			}
			$empID = $_SESSION['employee_id'];
			$sql = "SELECT * from tmp_message where  $now  < (select extract (epoch  from expires))  and employee_id = $empID and seen is false order by expires desc limit 1";
			if (!$data = $conn->getRow($sql)) {
				if ($conn->ErrorNo() != 0 ) {
					die($sql);
				}
				else {
					return $div;
				}

			}
			if (!empty($data)) {
				$id = $data['tmp_message_id'];
				$div = $data['message'];
				$sql = "UPDATE tmp_message set seen = true where tmp_message_id = $id";
				if (!$res = $conn->Execute($sql)) {
					die($sql);
				}

			}
			return $div;
		}
		static function dateSelect($equalgreater,$equalto,$between,$less,$sDate,$eDate,$hideEndDate=true,$wideDiv="") {
			if(empty($wideDiv) ) {
				$div1="class=\"div9 marg1\"";
				$div2="class=\"div700 marg1\"";
				$div3="class=\"div700 marg1\"";
		   }	
			else {
				$div1="class=\"div18 marg1\"";
				$div2="class=\"div14 marg2\"";
				$div3="class=\"div14 marg2\"";
			}
         $content = "<div $div1 >\n";
         $content .= "<label >Date is:</label><select  name=\"datetype\" id=\"datetype\" style=\"width:100%;\" onchange=\"displayDiv('#datetype');\">
<option value=\"equalgreater\" $equalgreater >Equal or greater</option>
<option value=\"equalto\" $equalto >Equal to</option>
<option value=\"between\" $between >Between</option>
<option value=\"less\" $less >Less than</option>
</select>
</div>
<div $div2 ><label for=\"hdate\" >Date<input type=\"text\" name=\"sdate\" id=\"sdate\" class=\"date\" value=\"$sDate\" /></label></div>
<div $div3 style=\"height:2rem;\"  >";
         if ($hideEndDate ) {
            $content .= "<div id=\"blankdiv\" ><label>&nbsp;<input type=\"text\" disabled class=\"mainback\" /></label></div>\n";
            $content .= "<div id=\"enddiv\" style=\"display:none;\" ><label for=\"end_date\" >and Date<input type=\"text\" name=\"end_date\" id=\"end_date\"  class=\"date\" value=\"$eDate\" /></label></div>\n";
         }
         else {
            $content .= "<div id=\"blankdiv\" style=\"display:none;\"  ><label>&nbsp;<input type=\"text\" disabled class=\"mainback\" /></label></div>\n";
            $content .= "<div id=\"enddiv\"  ><label for=\"end_date\" >and Date<input type=\"text\" name=\"end_date\" id=\"end_date\"  class=\"date\" value=\"$eDate\" /></label></div>\n";
         }
         $content .= "</div>\n";
         return $content;

      }
		static function cleanURL($name) {
         $name = preg_replace('/[\$&\?#^%@!\*\(\):;\"\<\>\']/',"",$name);
         $name = preg_replace('/\ /',"_",$name);
         return $name;
      }
		static function showMessage($message) {
			$_SESSION['email_message']  =  "<div style=\"clear:both;height:40px;\" ></div><div style=\"float:left;width:1790px;border:5px groove #000000;padding:0px 2px 2px 1px;\" >\n";
      	$_SESSION['email_message'] .= "<div class=\"email_div\" >$message</div></div>\n";
		}
		static function getFieldApproval($ID,$conID,$type="cost") {
			global $conn;
			$dbarr=unserialize($_SESSION['dbarr']);
         $dblink = $dbarr[$conID][0];
			if ($type == "cost" ) {
         	$sql = "select * from 
				(select count(*) as countall from field_estimate where request_estimate_id = $ID) as c ,
				(select count(qgc_approve_date) as countdate from field_estimate where request_estimate_id = $ID and qgc_approve_date is not null) as d";
			}
			else {
         	$sql = "select * from 
				(select count(*) as countall from field_estimate where site_instruction_id = $ID) as c ,
				(select count(qgc_approve_date) as countdate from field_estimate where site_instruction_id = $ID and qgc_approve_date is not null) as d";
			}
         $sql = pg_escape_string($sql);
         $stmt = "SELECT * from dblink('$dblink','$sql',true) AS t1(countall double precision,countdate double precision)" ;
         if (! $data = $conn->getRow($stmt)) {
            if ($conn->ErrorMsg() != 0 ) {
               die($conn->ErrorMsg());
            }
            else {
               return NULL;
            }
         }
         return $data;
		}
		static function updateFieldEst($requestID,$cooID,$conID) {  //  Updates all Field Estimates  with COO issued  there on
			global $conn;
			$dbarr=unserialize($_SESSION['dbarr']);
         $dblink = $dbarr[$conID][0];
			$sql = "UPDATE field_estimate set calloff_order_id = $cooID  where request_estimate_id = $requestID";
			$res = AdminFunctions::execCon($dblink,$sql);
         return $res;
		}
		static function contactEmail($empID) {
			global $conn;
			$sql = "SELECT contact_email from contact where employee_id = $empID";
			return $conn->getOne($sql);
		}
		static function cooHours($cooID,$conID) {
			global $conn;
			if	(! isset($_SESSION['dbarr'])) {
				die("login");
			}
			$dbarr=unserialize($_SESSION['dbarr']);
         $dblink = $dbarr[$conID][0];
         $sql = "SELECT 'TRUE' from hour where calloff_order_id = $cooID";
         $sql = pg_escape_string($sql);
         $stmt = "SELECT * from dblink('$dblink','$sql',true) AS t1(answer text)" ;
         if (! $data = $conn->getRow($stmt)) {
            if ($conn->ErrorMsg() != 0 ) {
               die($conn->ErrorMsg());
            }
            else {
               return false;
            }
         }
         return true;
		}
		static function getAllFromCoo($cooID) {
			global $conn;
			$sql = "SELECT crew_id,area_id,well_ids from calloff_order where calloff_order_id = $cooID";
			return $conn->getRow($sql);
		}
		static function getATWType() {
			global $conn;
			$sql = "SELECT * from atw_type";
			return $conn->getAssoc($sql);
		}
      static function endLastMonth() {
          $now = time();
         while (date('d',$now) != '01') {
            $now -= 86400;
         }
         $now -= 86400;
          return date('d-m-Y',$now);
      }
		static function compUpTypes() {
         $conn = $GLOBALS['conn'];
         $sql = "SELECT upload_type_id,description from upload_type where sub_type = 'CPL' order by description";
         return $conn->getAll($sql);

      }
		 static function statusSelect($statusID) {
			$profileID = intval($_SESSION['profile_id']);
         //$stats=array("1"=>"Late Timesheets","2"=>"Rejected by QGC","3"=>"Late Entries","4"=>"Reply to Rejection");
         $stats=array("1"=>"Not Approved","2"=>"Rejected by QGC","3"=>"Late Entries","4"=>"Reply to Rejection","6"=>"Wait Compl Approval","7"=>"Approved Completion");
         $content="\n<select  name=\"status\"  id=\"status\" style=\"width:100%;\"  >\n";
         $content .= "<option value=\"-1\" ></option>\n";
         foreach ($stats as $key=>$val) {
            $SELECTED =  !is_null($statusID) && $statusID == $key ? "selected" : "";
            $content .= "<option value=\"".$key . "\" $SELECTED >".$val ."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }

      static function completeUploadSelect($compTypeID,$DISABLED) {
         $compTypes=AdminFunctions::compUpTypes();
         $content="\n<select    name=\"complete_upload[]\" style=\"width:100%;\"  $DISABLED >\n";
         foreach ($compTypes as $key=>$val) {
            $SELECTED = $compTypeID == $val['upload_type_id'] ? "selected" : "";
            $content .= "<option value=\"".$val['upload_type_id'] . "\" $SELECTED >".$val['description'] ."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }  
		static function AtoZ($hrefTxt,$search) {
         $content = "<div id=\"fullalpha\"><label class=\"label\">SEARCH:</label>\n";
         for ($x=65;$x<91;$x++) {
            $highlight = ord($search) == $x ? "hilite" : "";
            $content .= "<a href=\"$hrefTxt&amp;search=".chr($x) ."\" class=\"alpha $highlight\" >".chr($x)."</a>\n";
         }
         $highlight = $search == "" ? "hilite" : "";
         $content .= "<a href=\"$hrefTxt\" class=\"alpha all $highlight \" >All</a>\n</div>\n";
         return $content;
      }

		static function oneToNine($hrefTxt,$search) {
         $content = "<div id=\"fullalpha\"><label class=\"label\">SEARCH:</label>\n";
         for ($x=0;$x<10;$x++) {
            $highlight = strlen($search) > 0  && is_numeric($search) && $search == $x  ? "hilite" : "";
            $content .= "<a href=\"$hrefTxt&amp;search=".$x ."\" class=\"alpha $highlight\" >".$x."</a>\n";
         }
         $content .= "</div>\n";
         return $content;
      }
		static function subscopes($crewID) {
			global $conn;
			$sql = "SELECT sub_crew_id,sub_crew_name from sub_crew where crew_id = $crewID";
			return $conn->getAssoc($sql);

		}
		static function budgets() {
			global $conn;
			$sql = "SELECT budget_id,budget_name from budget where removed is false order by budget_name";
			return $conn->getAssoc($sql);
		}
	   static function budgetSelect($budgetID=NULL,$DISABLED,$name="budget",$id="budget") {
         $buds=AdminFunctions::budgets();
         if (count($buds) == 0 ) {
            $content = "<label>&nbsp;<input type=\"text\" disabled class=\"mainback\" /></label>\n";
            return $content;
         }
         $content = "<label>Budget</label>\n";
         $content .="\n<select class=\"required\"  style=\"width:100%;\" name=\"$name\"  id=\"$id\" $DISABLED >\n";
			$content .= "<option></option>";
         foreach ($buds as $key=>$val) {
            $SELECTED = $budgetID == $key ? "selected" : "";
            $content .= "<option value=\"$key\" $SELECTED >$val</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function email_approve_comp($compID,$conID,$cooID,$empID,$callType) {  // called from AJAX  or  Completion Form $_POST
			global $conn;
			$conArr = AdminFunctions::getConName($conID);
			$dbLink = $conArr['db_link'];
      	$shortName = $conArr['name'];
      	$conName = $conArr['con_name'];
      	$invoiceEmail = AdminFunctions::getConfig('INVOICE_EMAIL'); // This sets qgc-invoice  for  send  to's
			$signee = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
   		$signature = AdminFunctions::getSig($empID);
   		$dat = date('d-m-Y');


			$sql = "UPDATE hour set status = 7 where completion_cert_id = $compID  and calloff_order_id = $cooID";  // Partial Completions
         $linkSQL = "SELECT  dblink_exec('$dbLink','$sql')";
         if (! $res=$conn->getOne($linkSQL)) {
            var_dump($linkSQL);
           	die($conn->ErrorMsg());
         }
         $sql = "UPDATE gravel_water set status = 7  where completion_cert_id = $compID  and calloff_order_id = $cooID";  // Partial Completions
         $linkSQL = "SELECT  dblink_exec('$dbLink','$sql')";
         if (! $res=$conn->getOne($linkSQL)) {
            var_dump($linkSQL);
            die($conn->ErrorMsg());
         }


			$sql = "SELECT area_id,crew_id,well_ids from calloff_order where calloff_order_id = $cooID";
			if (! $data=$conn->getRow($sql)) {
            die($conn->ErrorMsg());
         }
			extract($data);

         // Insert Docket Day Lock on all  hour  entry against this COO except partial completions
         $sql = "INSERT into docket_day(docket_date,status,area_id,calloff_order_id,crew_id,well_ids) values (current_date,7,$area_id,$cooID,$crew_id,'$well_ids')";
         $sql = preg_replace("/'/","''",$sql);
         $linkSQL = "SELECT  dblink_exec('$dbLink','$sql')";
         if (! $res=$conn->getOne($linkSQL)) {
             var_dump($linkSQL);
             die($conn->ErrorMsg());
          }
          if (!empty($signature)) {
            $sql = "UPDATE calloff_order set completion_cert_id = $compID where calloff_order_id = $cooID"; // only room for  one  partial completion being the latest
            if(! $conn->Execute($sql)) {
               die($conn->ErrorMsg());
            }
         }

		 	$sql = "SELECT upload_name from {$shortName}_upload where completion_cert_id = $compID and upload_type_id = 13";
      	if (!$upname = $conn->getOne($sql)) {
         	$err = "No Invoice Upload found ?";
				if ($callType == "AJAX" ) {
					echo $err;
				}
				else {
					AdminFunctions::showError($err);
					header("LOCATION : ".$_SESSION['last_request']);
					exit;
				}
      	}
      	else {
         // Only allow  emailing  once except PARTIAL !!!
         	$sql = "SELECT email_log_id from email_log  where dkt_id = $compID and receiver_type = 'CPL' and subject like 'INVOICE%' and receiver_id = $conID ";
         	if ($eNum = $conn->getOne($sql)) {
            	$err = "This Invoice has already been sent $eNum";
					if ($callType == "AJAX" ) {
						echo $err;
						return;
					}
					else {
						AdminFunctions::showError($err);
						header("LOCATION : ".$_SESSION['last_request']);
						exit;
					}	
         	}


         // Approve now  updates  approval as well as emailiong

         	$sql = "UPDATE completion_cert set qgc_sig = '$signature',qgc_approve_date = '$dat', qgc_signee = E'$signee' where completion_cert_id = $compID";
         	$sql = preg_replace("/'/","''",$sql);

         	$linkSQL = "SELECT  dblink_exec('$dbLink','$sql')";
         	if (! $res=$conn->getOne($linkSQL)) {
            	var_dump($linkSQL);
            	die($conn->ErrorMsg());
         	}



         	$upName = $conArr['domain'] ."/$upname";
         	$eNum = AdminFunctions::insertEmailLog($empID,0,$conID,"INVOICE for Completion Certificate $compID for $conName",'CPL',$compID,$dat);
      	// make   email attachment  with  invoice 
         	$sql = "INSERT into email_attachment (email_log_id,attachment_name,recipient_type,email,attach_date )    
         	values ($eNum,'$upName','company','$invoiceEmail',current_date)";
         	if (! $res = $conn->Execute($sql)) {
            	echo $sql;
            	die($conn->ErrorMsg());
         	}
				if ($callType == "AJAX" ) {
         		echo $eNum;
				}
				else {
					return $eNum;
				}
      	}

		}
	   static function subScopeSelect($crewID=NULL,$subID = NULL,$DISABLED,$name="sub_scope",$id="sub_scope") {
			$subs = array();
         if (! is_null($crewID)) {
            $subs=AdminFunctions::subscopes($crewID);
         }
         if (count($subs) == 0 ) {
            $content = "<label>&nbsp;<input type=\"text\" disabled class=\"mainback\" /></label>\n";
            return $content;
         }
         $content = "<label>Sub Scopes</label>\n";
         $subs=AdminFunctions::subscopes($crewID);
         $content .="\n<select class=\"required\"  style=\"width:100%;\" name=\"$name\"  id=\"$id\" $DISABLED >\n";
			$content .= "<option></option>";
         foreach ($subs as $key=>$val) {
            $SELECTED = $subID == $key ? "selected" : "";
            $content .= "<option value=\"$key\" $SELECTED >$val</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		 static function subscopeSelectMulti($crewID=NULL,$subIDs = NULL,$DISABLED="",$subsOnly=false) {
			$subs = array();
			if (! is_null($crewID)) {
         	$subs=AdminFunctions::subscopes($crewID);
			}
         if (count($subs) == 0 ) {
				$content = "<label>&nbsp;<textarea disabled class=\"mainback txtmulti\"></textarea></label>\n";
            return $content;
         }
         $subids = explode("|",$subIDs);
         $content = "<label>Sub Scopes</label>\n";
         $content.="\n<select class=\"required\"  style=\"width:100%;\"  name=\"sub_scope[]\" id=\"sub_scope\"  MULTIPLE SIZE=\"10\" $DISABLED >\n";
         foreach ($subs as $key=>$val) {
				if (!$subsOnly) {
            	$SELECTED = "";
            	if (in_array($key,$subids)) {
               	$SELECTED =  "selected";
            	}
            	$content .= "<option value=\"$key\" $SELECTED >$val</option>\n";
				}
				else {
					 if (in_array($key,$subids)) {
                  $content .= "<option value=\"".$key . "\"  >".$val."</option>\n";
               }
				}
         }
         $content .= "</select>";
         return $content;
      }

	   static function stateSelect($stateID = NULL,$DISABLED,$name="state",$id="sitestate") {
         $states=AdminFunctions::states();
         $content="\n<select  name=\"$name\" style=\"width:100%;\"  id=\"$id\" $DISABLED >\n";
         foreach ($states as $key=>$val) {
            $SELECTED = $stateID == $key ? "selected" : "";
            $content .= "<option value=\"".$key . "\" $SELECTED >".$val ."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function priceTypes() {
			global $conn;
			$sql = "SELECT * from price_type order by price_type_id";
			return $conn->getAll($sql);
		}
	   static function priceTypeSelect($priceName = NULL,$DISABLED,$name="price_type",$id="price_type") {
         $prices=AdminFunctions::priceTypes();
         $content="\n<select  name=\"$name\"  style=\"width:100%;\" id=\"$id\" $DISABLED >\n";
         foreach ($prices as $key=>$val) {
            $SELECTED = $priceName == $val['price_name'] ? "selected" : "";
            $content .= "<option value=\"".$val['price_name'] . "\" $SELECTED >".$val['price_name'] ."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function uom() {
			$uom=array(array("uom_id"=>"1","uom_name"=>"HOUR"),array("uom_id"=>"2","uom_name"=>"DAY"),array("uom_id"=>"3","uom_name"=>"LEASE"),array("uom_id"=>"3","uom_name"=>"EACH"));
			return $uom;
		}
	   static function uomSelect($uomName = NULL,$DISABLED,$name="uom",$id="uom") {
         $uoms=AdminFunctions::uom();
         $content="\n<select name=\"$name\" style=\"width:100%;\"  id=\"$id\" $DISABLED >\n";
         foreach ($uoms as $key=>$val) {
            $SELECTED = $uomName == $val['uom_name'] ? "selected" : "";
            $content .= "<option value=\"".$val['uom_name'] . "\" $SELECTED >".$val['uom_name'] ."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function inv_uom() {
			global $conn;
			$sql = "SELECT * from uom";
			return($conn->getAssoc($sql));
		}
	   static function invUOMSelect($uomID = NULL,$DISABLED,$name="invuom",$id="invuom") {
         $uoms=AdminFunctions::inv_uom();
         $content="\n<select name=\"$name\" style=\"width:100%;\"   class=\"required\" id=\"$id\" $DISABLED >\n";
         foreach ($uoms as $key=>$val) {
            $SELECTED = $uomID == $key ? "selected" : "";
            $content .= "<option value=\"$key\" $SELECTED >$val </option>\n";
         }
         $content .= "</select>";
         return $content;
      }
	   static function complStatSelect($statID = NULL,$DISABLED,$name="compl_status") {
         $status=array("0"=>"All","1"=>"Wait Approval","2"=>"Approved");
         $content="\n<select  name=\"$name\" style=\"width:100%;\"  $DISABLED >\n";
         foreach ($status as $key=>$val) {
            $SELECTED = $statID == $key ? "selected" : "";
            $content .= "<option value=\"".$key . "\" $SELECTED >".$val ."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }

		static function clientSelect($clientID = NULL,$DISABLED) {
			$clients=AdminFunctions::clients();
			$content="\n<select  name=\"client\" id=\"client\" $DISABLED >\n";
         $content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($clients as $key=>$val) {
				$SELECTED = $clientID == $val['client_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['client_id'] . "\" $SELECTED >".$val['client_name']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function certificateSelect($certID = NULL,$DISABLED) {
			$certIDs = explode("|",$certID);
			$certificates=AdminFunctions::certificates();
			$content="\n<select  name=\"certificate[]\" id=\"certificate\" MULTIPLE  $DISABLED  SIZE=\"10\" >\n";
         //$content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($certificates as $key=>$val) {
				$SELECTED = "";
				if (in_array($val['certificate_id'],$certIDs)) {
					$SELECTED =  "selected";
				}
				$content .= "<option value=\"".$val['certificate_id'] . "\" $SELECTED >".$val['certificate_desc']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
			static function TCETypeSelect($tceTypeID,$DISABLED) {
			$CON_SELECTED = $REHAB_SELECTED = $ROAD_SELECTED = NULL;
			if ($tceTypeID == 1 ) {
				$CON_SELECTED = "selected";
				$REHAB_SELECTED = "";
				$ROAD_SELECTED = "";
			}
			else if ($tceTypeID == 2 ){
				$CON_SELECTED = "";
				$REHAB_SELECTED = "selected";
				$ROAD_SELECTED = "";
			}
			else if ($tceTypeID == 3 ){
				$CON_SELECTED = "";
				$REHAB_SELECTED = "";
				$ROAD_SELECTED = "selected";
			}
			$content="\n<select name=\"tce_type\" id=\"tce_type\"  class=\"required\" style=\"width:100%;\"  $DISABLED  onChange=\"showTCEDiv();\"  >\n";
			$content .= "<option ></option>\n"; 
			$content .= "<option value=\"1\" $CON_SELECTED >Construction</option>\n"; 
			$content .= "<option value=\"2\" $REHAB_SELECTED >Rehabilitation</option>\n"; 
			$content .= "<option value=\"3\" $ROAD_SELECTED >Road Maintenance</option>\n"; 
			$content .= "</select>";
			return $content;
		}
		static function rateTypeSelect($rateID = NULL,$DISABLED,$name="ratetype") {
			$rates=AdminFunctions::rateTypes();
			$content="\n<select name=\"$name\" id=\"$name\"   $DISABLED   >\n";
         //$content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($rates as $key=>$val) {
				$SELECTED = "";
				if ($val['rate_type_id'] == $rateID) {
					$SELECTED =  "selected";
				}
				$content .= "<option value=\"".$val['rate_type_id'] . "\" $SELECTED >".$val['rate_desc']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function atwTypes() {
     		$conn = $GLOBALS['conn'];
			$sql = "Select * from atw_type where atw_type_id < 6 order by atw_type_id";
      	return $conn->getAll($sql);
		}
		static function atwTypeSelect($atwTypeID = NULL,$DISABLED) {
			$atwtype=AdminFunctions::atwTypes();
			$content="\n<select name=\"atw_type_id[]\" id=\"atw_type_id\" $DISABLED  >\n";
		/*	if ($nosize) {
        		$content .= "<option></option>\n"; // makes non compulsory list item
			}  */
			foreach ($atwtype as $key=>$val) {
				$SELECTED = $atwTypeID == $val['atw_type_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['atw_type_id'] . "\" $SELECTED >".$val['type_name']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function supgroups() {
     		$conn = $GLOBALS['conn'];
			$sql = "Select * from supervisor_group where removed is false order by group_name";
      	return $conn->getAll($sql);
		}
		static function supGroupFromEmp($empID) {
     		$conn = $GLOBALS['conn'];
			$sql = "Select supervisor_group_id  from supervisor_group  WHERE $empID in 
					(select (unnest(string_to_array(employee_ids::text,'|')::int[])))";
      	return $conn->getOne($sql);
		}
		static function supGroupSelect($supGroupID = NULL,$DISABLED) {
			$supgroup=AdminFunctions::supgroups();
			$content="\n<select class=\"required\" name=\"supervisor_group\" id=\"supervisor_group\" $DISABLED  >\n";
		/*	if ($nosize) {
			}  */
        	$content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($supgroup as $key=>$val) {
				$SELECTED = $supGroupID == $val['supervisor_group_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['supervisor_group_id'] . "\" $SELECTED >".$val['group_name']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function _old_getMessage() {
			return true;
         $page = preg_replace('/\.php/',"",preg_replace('/\?.*$/',"",basename($_SERVER['REQUEST_URI'])));  // get current page minus  getstrings and  PHP  ext
         $arr = NULL;
         $conn = $GLOBALS['conn'];
         $sql = "SELECT message_id,message_text,show_pages,show_once from message where expire_date <= current_date and shown is false order by expire_date desc limit 1";
         $messArr = $conn->getRow($sql);
         if (count($messArr) > 0 ) {
            $mess = $messArr['message_text'];
            $pg = $messArr['show_pages'];
            $id = $messArr['message_id'];
				$show = $messArr['show_once'];
            $match = preg_match("/$page/",$pg);
            if ($pg == "ALL" ||  $match  >= 1 ) {
					$html  =  "<div style=\"clear:both;height:40px;\" ></div><div style=\"float:left;width:1790px;border:5px groove #000000;padding:0px 2px 2px 1px;text-align:center;\" >\n";
               $html .= "<div style=\"margin-left:500px;width:400px;font-size:14px;min-height:80px;\" > <span class=\"tooltip\" >$mess</span></div>\n";
               $html .= "</div>\n";
               $arr = array("message_id"=>$id,"message"=>$html,"show_once"=>$show);
            }
         }
         return $arr;
      }
      static function  updateMessage($messageID) {
         $conn = $GLOBALS['conn'];
         $sql = " UPDATE message set shown = true where message_id = $messageID  and show_once is true";
         if ( !$res= $conn->Execute($sql )) {
            die($conn->ErrorMsg());
         }

      }
		static function supervisors() {
			// and profile_id = 1
     		$conn = $GLOBALS['conn'];
      	$sql = "select employee_id,firstname || ' ' || lastname as fullname,a.email
			from employee e 
			LEFT JOIN address_to_relation using (employee_id)
			LEFT JOIN address a using (address_id) 
			where e.company_id in (0,2) and coalesce(a.email,'') != '' and removed is false and employee_id > 2 order by lastname";
      	return $conn->getAll($sql);
   	}
		static function getSupEmails($supStr) {
			$supEmailStr = "";
			$supIDs = explode("|",$supStr);
         $supervisors=AdminFunctions::supervisors();
			foreach($supervisors as $key=>$val) {
            if (in_array($val['employee_id'],$supIDs)) {
					$supEmailStr .= $val['email'] ."|";
				}
			}
		  	$supEmailStr = preg_replace('/\|$/',"",$supEmailStr);
			return $supEmailStr;
		}
		static function supervisorSelect($supIDs="",$DISABLED,$mult=true) {
         $supervisors=AdminFunctions::supervisors();
			$supArr = explode("|",$supIDs);
         $sizelist="SIZE=\"10\"";
			if ($mult) {
         	$MULTIPLE="multiple";
				$name="supervisor[]";
			}
			else {
         	$MULTIPLE="";
				$name="supervisor";

			}
         $content="\n<select class=\"required\" name=\"$name\" id=\"supervisor\" style=\"width:100%;\" $DISABLED  $sizelist $MULTIPLE >\n";
         foreach ($supervisors as $key=>$val) {
            $SELECTED = "";
            if (in_array($val['employee_id'],$supArr)) {
               $SELECTED =  "selected";
            }
				$email = strlen($val['email']) > 0 ? $val['email'] : "NO EMAIL ADDRESS";
				$li = $val['fullname'] . " - " . $email;

            $content .= "<option value=\"".$val['employee_id'] . "\" $SELECTED >$li</option>\n";
         }
        // $content .= "<option></option>\n"; // makes non compulsory list item
         $content .= "</select>";
         return $content;
      }
		static function conEmployees($conID) {
      	$conn = $GLOBALS['conn'];
			$dbarr=unserialize($_SESSION['dbarr']);
   		$shortName = $dbarr[$conID][2];
      	$sql = "select employee_id,firstname || ' ' || lastname as fullname  from {$shortName}_employee where employee_id > 2 and removed is false order by lastname";
      	return $conn->getAll($sql);
   	}
		static function superIntendents() {
			$conn = $GLOBALS['conn'];
			$sql = "SELECT  employee_id,firstname || ' ' || lastname as supname,email from employee
			LEFT JOIN address_to_relation ar using(employee_id)
			LEFT JOIN address a using (address_id)
			Where employee_id > 2  and  profile_id = 16	
			order by supname";
			if (! $arr = $conn->getAll($sql)) {
				die("no superintendents found?");
				die($conn->ErrorMsg());
			}
			return $arr;
		}
		static function superIntendentSelect($superID = NULL,$DISABLED="") {
			$supers = AdminFunctions::superIntendents();
			$content="\n<select  style=\"width:100%;\" name=\"superintendent\" id=\"superintendent\"  >\n";
         foreach ($supers as $key=>$val) {
            $SELECTED =  $superID == $val['employee_id'] ? "SELECTED" : "";
            $email = strlen($val['email']) > 0 ? $val['email'] : "NO EMAIL ADDRESS";
            $li = $val['supname'] . " - " . $email;

            $content .= "<option value=\"".$val['employee_id'] . "\" $SELECTED >$li</option>\n";
         }
        // $content .= "<option></option>\n"; // makes non compulsory list item
         $content .= "</select>";
         return $content;
		}

		static function supApproval($hDate,$conID) {
			$conn = $GLOBALS['conn'];
			// Has this been emailed
			
			$sql = "SELECT true from email_log where dkt_date = '$hDate' and receiver_type = 'ST2' and dkt_id = $conID";
			if (! $res = $conn->getOne($sql)) {
				if ($conn->ErrorNo() != 0 ) {
					die($conn->ErrorMsg());
				}
				else {
					$emailButTxt = "Email Superintendent";
            	$emailButClass = "superin";
				}

			}
			else {
					$emailButTxt = "Superintendent Emailed";
            	$emailButClass = "superin emailed";
			}

			$html = "";
      	$conn = $GLOBALS['conn'];
			$connArr = AdminFunctions::getConName($conID);
         $shortName = $connArr['name'];
			$sql = "SELECT true from {$shortName}_hour where hour_date = '$hDate' and total_t2 > 0 and status < 5";  // must be approved first
			if (! $res = $conn->getOne($sql)) {
			
				if($conn->ErrorNo() != 0 ) {
					die($conn->ErrorMsg());
				}
				else {
					$sql = "SELECT count(total_t2) as cnt from {$shortName}_hour where hour_date = '$hDate' and total_t2 > 0 ";
					if (! $cnt = $conn->getOne($sql)) {
						if ($conn->ErrorNo() != 0 ) {
							die($conn->ErrorMsg());
						}
					}
					if ($cnt > 0 ) {
						$html = "<div style=\"clear:both;\" ></div>\n";
						$html .= "<div style=\"width:918px;float:left;\" >\n";
						$html .= "<fieldset style=\"margin-top:10px;\" ><legend style=\"margin-left:43%;\">Superintendent Table 2 Approval</legend>\n";
						$html .= "<div class=\"div35\" >\n";
						$html .= "<button class=\"button $emailButClass\" onclick=\"emailSup($(this));$(this).prop('disabled','disabled');return false;\" >$emailButTxt</button>\n";
						$html .= "</div>\n";
						$html .= "<div class=\"div60\" >\n";
						$html .= AdminFunctions::superIntendentSelect(NULL,"");
						$html .= "</div>\n";
						$html .= "</fieldset>";
						$html .= "</div>\n";
					}

					return $html;
				}


			}
			return $html;
		}
		
	static function  rrmdir($dir) { 
   	if (is_dir($dir)) { 
     		$objects = scandir($dir); 
     		foreach ($objects as $object) { 
       		if ($object != "." && $object != "..") { 
        			if (is_dir($dir."/".$object))
        				AdminFunctions::rrmdir($dir."/".$object);
        			else
        				unlink($dir."/".$object); 
       			} 
     		}
     		rmdir($dir); 
   	} 
	}
		static function conemployeeSelect($conID,$operatorID,$showBlank=true) {
			$employees=AdminFunctions::conEmployees($conID);
			$content="\n<select  name=\"operator\" id=\"operator\" >\n";
			if ($showBlank) {
        		$content .= "<option></option>\n"; // makes non compulsory list item
			}
			foreach ($employees as $key=>$val) {
				$SELECTED = $operatorID == $val['employee_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['employee_id'] . "\" $SELECTED >".$val['fullname']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function employeeSelect($employeeID = NULL,$DISABLED,$nosize=false) {
			$employees=AdminFunctions::employees();
			$size = ! $nosize ? "SIZE=\"10\"" :"";
			$content="\n<select  name=\"employee\" id=\"employee\" $DISABLED  >\n";
			if ($nosize) {
        		$content .= "<option></option>\n"; // makes non compulsory list item
			}
			foreach ($employees as $key=>$val) {
				$SELECTED = $employeeID == $val['employee_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['employee_id'] . "\" $SELECTED >".$val['fullname']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function employeeMulSelect($employeeIDs = NULL,$DISABLED) {
			$employees=AdminFunctions::employees();
			$empIDs = explode("|",$employeeIDs);
			$content="\n<select  name=\"employee[]\" id=\"employee\" $DISABLED  MULTIPLE SIZE=\"10\"  >\n";
			foreach ($employees as $key=>$val) {
				$SELECTED = "";
				if (in_array($val['employee_id'],$empIDs)) {
                $SELECTED =  "selected";
            }
				$content .= "<option value=\"".$val['employee_id'] . "\" $SELECTED >".$val['fullname']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function crews() { // Hardcoded
         $conn = $GLOBALS['conn'];
         $sql = "SELECT crew_id,crew_name from crew where removed is false order by crew_name";
         if (! $data=$conn->getAll($sql)) {
            if ($conn->ErrorNo() != 0 ) {
               die($conn->ErrorMsg());
            }
         }
         return $data;
      }
		static function units($plantName) {
         $conn = $GLOBALS['conn'];
         $sql = "SELECT plant_id,plant_unit from plant where  plant_name = '$plantName' order by plant_unit";
         if (! $data=$conn->getAll($sql)) {
				die ($conn->ErrorMsg());
			}
			return $data;

		}
		static function setContractor($action,$contractorID) {
			$content = <<<FIN
         <div style="margin:auto;width:605px;" >
         <form  method="post" action="$action">
         <input type="hidden" name="contractorID" id="contractorID" value="$contractorID" />
         <fieldset ><legend style="margin-left:44%;">Set Contractor</legend>
         <div class="div40" >
FIN;
         $content .= AdminFunctions::contractorSelect($contractorID,"",false);
         $content .= "</div>\n";
         $content .="<div style=\"float:right;width:32%;\">\n";
         $content .= "\n<input type=\"submit\" name=\"setcon\" value=\"Set Contractor\"  class=\"button\" />";
			$content .= "</div>\n";
         $content .= "</fieldset></form></div>\n";
			return $content;
		}
      static function unitSelect($plantName = NULL,$plantID = NULL,$DISABLED) {
         $units=AdminFunctions::units($plantName);
         $content="\n<select   name=\"unit\" id=\"unit\"   style=\"width:200px !important;\" $DISABLED >\n";
         foreach ($units as $key=>$val) {
            $SELECTED = $plantID == $val['plant_id'] ? "selected" : "";
            $content .= "<option value=\"".$val['plant_id'] . "\" $SELECTED >".$val['plant_unit']."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
      static function crewSelect($crewID = NULL,$DISABLED,$blank=true) {
         $crews=AdminFunctions::crews();
         $content="\n<select class=\"required\"  style=\"width:100%;\" name=\"crew\" id=\"crew\" $DISABLED >\n";
			if ($blank) {
				$content .= "<option></option>";
			}
         foreach ($crews as $key=>$val) {
            $SELECTED = $crewID == $val['crew_id'] ? "selected" : "";
            $content .= "<option value=\"".$val['crew_id'] . "\" $SELECTED >".$val['crew_name']."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
      static function crewParentSelect($crewID = NULL,$DISABLED,$blank=true) {
         $crews=AdminFunctions::getAllCrew();
			$content = "<label>Work Scope</label>\n";
         $content .="\n<select  name=\"crew\" style=\"width:100%;\" id=\"crew\" $DISABLED >\n";
			if ($blank) {
				$content .= "<option value=\"0\" >All</option>";
			}
         foreach ($crews as $key=>$val) {
            $SELECTED = $crewID == $key ? "selected" : "";
            $content .= "<option value=\"".$key . "\" $SELECTED >".$val['crew_name']."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function wells($areaID) {
         $conn = $GLOBALS['conn'];
         $sql = "SELECT well_id,well_name from well where  area_id  = $areaID  and removed is false order by well_name";
         if (! $data=$conn->getAll($sql)) {
				if ($conn->ErrorNo() != 0 ) {
					die ($conn->ErrorMsg());
				}
			}
			return $data;

		}
      static function wellSelect($areaID=NULL,$wellID = NULL,$DISABLED,$showall=true) {
		 	$wells = array();
         if (! is_null($areaID)) {
        		$wells=AdminFunctions::wells($areaID);
         }
			if (count($wells) == 0 ) {
				$content = "<label>&nbsp;<input type=\"text\" disabled class=\"mainback\" /></label>\n";
				return $content;
			}	
			$content = "<label>Well</label>\n";
         $content.="\n<select class=\"required $DISABLED\"  style=\"width:100%;\" name=\"well[]\" id=\"well\" $DISABLED >\n";
			if ($showall) {
				$content .= "<option></option>";
			}
         foreach ($wells as $key=>$val) {
            $SELECTED = $wellID == $val['well_id'] ? "selected" : "";
            $content .= "<option value=\"".$val['well_id'] . "\" $SELECTED >".$val['well_name']."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
      static function wellSelectMulti($areaID=NULL,$wellIDs = NULL,$DISABLED="",$wellsOnly=false) {
		 	$wells = array();
         if (! is_null($areaID)) {
        		$wells=AdminFunctions::wells($areaID);
         }
         if (count($wells) == 0 ) {
            $content = "<label>&nbsp;<textarea disabled class=\"required mainback txtmulti\"></textarea></label>\n";
            return $content;
         }
			$wellids = explode("|",$wellIDs);
			$content = "<label>Well/s</label>\n";
         $content.="\n<select class=\"required\"  style=\"width:100%;min-height:11.15rem;height:11.15rem !important;\"  name=\"well[]\" id=\"well\"  MULTIPLE SIZE=\"10\" $DISABLED >\n";
         foreach ($wells as $key=>$val) {
				if (!$wellsOnly) {
					$SELECTED = "";
					if (in_array($val['well_id'],$wellids)) {
               	$SELECTED =  "selected";
            	}
            	$content .= "<option value=\"".$val['well_id'] . "\" $SELECTED >".$val['well_name']."</option>\n";
				}
				else {
					if (in_array($val['well_id'],$wellids)) {
            		$content .= "<option value=\"".$val['well_id'] . "\"  >".$val['well_name']."</option>\n";
            	}

				}
         }
         $content .= "</select>";
         return $content;
      }
		static function contractors() {
         $conn = $GLOBALS['conn'];
         $sql = "SELECT contractor_id,con_name from contractor where removed is false order by name";
         if (! $data=$conn->getAll($sql)) {
				if ($conn->ErrorNo() != 0 ) {
					die ($conn->ErrorMsg());
				}
			}
			return $data;
		}
      static function contractorMulSelect($conIDs=NULL,$DISABLED,$name="contractor[]",$blank=false) {
        	$conns=AdminFunctions::contractors();
			$conids = explode("|",$conIDs);
         $content="\n<select name=\"$name\" id=\"contractor\" $DISABLED  MULTIPLE SIZE=\"10\" >\n";
			if ($blank) {
				$content .= "<option></option>";
			}
         foreach ($conns as $key=>$val) {
            $SELECTED = "";
				if (in_array($val['contractor_id'],$conids)) {
                $SELECTED =  "selected";
            }
            $content .= "<option value=\"".$val['contractor_id'] . "\" $SELECTED >".$val['con_name']."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function locations($ownerID,$locTypeID) {
         $conn = $GLOBALS['conn'];
			if (intval($ownerID) > 0 ) {
				$sql = "select gravel_water_loc_id,location from gravel_water_loc 
						where loc_type = $locTypeID 
						and landowner_id = $ownerID
						and removed is false";
				return $conn->getAll($sql);
			}
			else {
				return false;
			}
		}
		static function conNumFromContractor($conID) {
         $conn = $GLOBALS['conn'];
			$sql = "SELECT contract_no from contractor where contractor_id = $conID";
			return $conn->getOne($sql);
		}
      static function locSelect($locID=NULL,$locTypeID,$ownerID,$DISABLED,$name="gwl_location",$blank=false,$required=false) {
			$req = $required ? "required" : "";
			$owners = array();
         if (! is_null($ownerID)) {
        		$owners=AdminFunctions::locations($ownerID,$locTypeID);
         }
         if (count($owners) == 0 ) {
				$content = "<label>&nbsp;<input type=\"text\" disabled class=\"mainback\" /></label>\n";
            return $content;
         }
			$content = "<label>Location</label>"; //  if no data just return space
			if (count($owners) > 0 && $owners) {
         	$content .="\n<select class=\"$req\" style=\"width:100%;\"  name=\"$name\" id=\"$name\"  $DISABLED >\n";
				if ($blank) {
					$content .= "<option></option>";
				}
         	foreach ($owners as $key=>$val) {
            	$SELECTED = $locID == $val['gravel_water_loc_id'] ? "selected" : "";
            	$content .= "<option value=\"".$val['gravel_water_loc_id'] . "\" $SELECTED >".$val['location']."</option>\n";
         	}
         	$content .= "</select>";
			}
         return $content;
		}
		static function landowners($locTypeID) {
			$gwlType = !is_null($locTypeID) ? " where gwl.loc_type = $locTypeID " : " where 1 = 1 ";
         $conn = $GLOBALS['conn'];
			$sql = "select distinct(landowner_id),owner_name from landowner l
						JOIN gravel_water_loc  gwl using(landowner_id)
						$gwlType 
						and l.removed is false
						and gwl.removed is false
                  order by owner_name";
			return $conn->getAll($sql);
		}
      static function ownerSelect($ownerID=NULL,$locTypeID=NULL,$DISABLED,$name="landowner",$blank=false,$required=false) {
			$labType = "";
			$req = $required ? "required" : "";
        	$owners=AdminFunctions::landowners($locTypeID);
			if (! is_null($locTypeID)) {
				$labType = $locTypeID == 1 ? "Gravel" : "Water";
			}
			$content = "<label>$labType Land Owner:</label>\n";
         $content .="\n<select class=\"$req\" style=\"width:100%;\"  name=\"$name\" id=\"$name\"  $DISABLED >\n";
			if ($blank) {
				$content .= "<option value=\"-1\" >All</option>";
			}
         foreach ($owners as $key=>$val) {
            $SELECTED = $ownerID == $val['landowner_id'] ? "selected" : "";
            $content .= "<option value=\"".$val['landowner_id'] . "\" $SELECTED >".$val['owner_name']."</option>\n";
         }
         $content .= "</select>";
         return $content;
		}
      static function locTypeSelect($typeID=NULL,$DISABLED,$name="loc_type[]",$blank=false) {
         $content="\n<select   style=\"width:100%;\" id=\"loc_type\"  name=\"$name\"   $DISABLED >\n";
			if ($blank) {
				$content .= "<option></option>";
			}
			if ($typeID == 1) {  // Gravel
            $content .= "<option value=\"1\" SELECTED >Gravel</option>\n";
            $content .= "<option value=\"2\" >Water</option>\n";
         }
			else {
            $content .= "<option value=\"1\" >Gravel</option>\n";
            $content .= "<option value=\"2\" SELECTED >Water</option>\n";
			}
         $content .= "</select>";
         return $content;
      }
		static function conPlantStatus($statID) {
			if ($statID == 0 ) {
				$zeroSelected = "selected";
				$oneSelected = "";
			}
			else  {
				$zeroSelected = "";
				$oneSelected = "selected";

			}
			$content = "<label>Show Approved</label><select name=\"approved\" style=\"width:100%;\" >\n";
			$content .= "<option value=\"0\" $zeroSelected >Show All</option>\n";
			$content .= "<option value=\"1\" $oneSelected >Show NOT Approved</option>\n";
			$content .= "</select>\n";
			return $content;

		}
      static function contractorSelect($conID=NULL,$DISABLED,$showall=true,$name="contractor",$blank=false,$required=false) {
			$req = $required == true ? "required" : "";
        	$conns=AdminFunctions::contractors();
			$content = "<label>Contractor</label>\n";
         $content .="\n<select class=\"$req\"  style=\"width:100%;\"  name=\"$name\" id=\"contractor\"  $DISABLED >\n";
			if ($showall) {
				$content .= "<option value=\"0\" >All</option>";
			}
			else if ($blank) {
				$content .= "<option></option>";
			}
         foreach ($conns as $key=>$val) {
            $SELECTED = $conID == $val['contractor_id'] ? "selected" : "";
            $content .= "<option value=\"".$val['contractor_id'] . "\" $SELECTED >".$val['con_name']."</option>\n";
         }
         $content .= "</select>";
         return $content;
      }
		static function profileSelect($profileID = NULL,$DISABLED) {
			$profiles=AdminFunctions::profiles();
			$content="\n<select  name=\"profile\" style=\"width:100%;\" id=\"profile\"  $DISABLED >\n";
         $content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($profiles as $key=>$val) {
				$SELECTED = $profileID == $val['profile_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['profile_id'] . "\" $SELECTED >".$val['profile_text']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function plantTypeSelect($plantTypeID = NULL,$DISABLED) {
			$pTypes=AdminFunctions::plant_types();
			$content="\n<select  name=\"plant_type\" id=\"plant_type\"  $DISABLED >\n";
         $content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($pTypes as $key=>$val) {
				$SELECTED = $plantTypeID == $val['plant_type_id'] ? "selected" : "";
				$content .= "<option value=\"".$val['plant_type_id'] . "\" $SELECTED >".$val['p_type']."</option>\n"; 
			}
			$content .= "</select>";
			return $content;
		}
		static function rateSelect($rate = NULL,$plantID,$date,$DISABLED) {
			$rates=AdminFunctions::plant_rates($plantID,"'".$date ."'");
			$content="\n<select  name=\"rate[]\" id=\"rate\"  onchange=\"calctotal();\"  $DISABLED >\n";
         $content .= "<option></option>\n"; // makes non compulsory list item
			foreach ($rates as $key=>$val) {
				$SELECTED = $rate == $val['rate'] ? "selected" : "";
				if ($val['type'] == "Normal" ) {
					$content .= "<option value=\"".$val['rate'] . "\" $SELECTED >Normal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp ".$val['rate']."</option>\n"; 
				}
				else {
					$content .= "<option value=\"".$val['rate'] . "\" $SELECTED >Stand Down ".$val['rate']."</option>\n"; 
				}
			}
			$content .= "</select>";
			return $content;
		}
		static function clientName($client_id) {
			global $conn;
			$sql = "SELECT client_name from client where client_id = $client_id";
			if (! $rs = $conn->Execute($sql)) {
				die($conn->ErrorMsg());
			}
			return $rs->fields['client_name'];
		}
		static function employeeName($employee_id) {
			global $conn;
			$sql = "SELECT firstname || ' ' || lastname as fullname from employee where employee_id = $employee_id";
			if (! $rs = $conn->Execute($sql)) {
				die($conn->ErrorMsg());
			}
			return $rs->fields['fullname'];
		}

}
?>
