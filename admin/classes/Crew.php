<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Crew  {
		public $page;
		public $crewID;
		public $crewName;
		public $contractorID;
		public $siteInsNo;
		public $siteInstruction;
		public $action;
		public $sessionProfile;
		public $conn;
		public $subCrewArr;
		public $subCrewIDs;

		public function	__construct($action,$crewID=0,$search="") {
			$this->crewID = $crewID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('crew');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New Work Scope";
						break;
					case "update" :
						if ($crewID < 1 ) {
							return false;
						}
						$heading_text = "Updating Work Scope $crewID";
						$this->getCrewDetails($crewID);
						break;
					case "delete" :
						if ($crewID < 1 ) {
							return false;
						}
						$heading_text = "Delete Work Scope $crewID";
						$this->getCrewDetails($crewID);
						break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List Work Scope - $search" : "List Work Scope - All";
						$this->getAllCrews($search) ;
						break;
					case "find" : 
						$heading_text = "Work Scope $crewID";
						$this->getCrewDetails($crewID);
						break;
					default:
						$heading_text = "Add New Work Scope";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->crewID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Work Scope";
						break;
					case "update":
						$button = "Update Work Scope";
						break;
					case "delete":
						$button = "Delete Work Scope";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Work Scope";
						break;
				}
			$content = <<<FIN
<div style="width:600px;margin:auto;">
<form name="crew" id="crew" method="post" action="crew.php?SUBMIT=SUBMIT">
<input type="hidden" name="crew_id" value="$this->crewID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:41%;">Work Scope</legend>
<div class="div44" > <label for="crew_name" >Work Scope Name<input type="text" name="crew_name" id="crew_name"  value="$this->crewName" $DISABLED /></label></div>
<div class="div44 marg10" >
FIN;
$content .= AdminFunctions::contractorSelect($this->contractorID,$DISABLED);
			$content .= <<<FIN
</div>
<script>
$("#crew").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
</script>
</fieldset>
FIN;


				$content .= $this->subCrewDiv($DISABLED);
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found crew
				}
				$content .= "\n<a href=\"crew.php?action=list&redo=redo\"   class=\"button\" >Last Screen</a>";

				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllCrews($search="") {
			$sql = "SELECT *,subscopes(crew_id) as sub_scopes from crew where removed is false order by crew_name";
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content = "<div style=\"width:1400px;margin:auto;\" >";
         $content .= "<div class=\"heading\" ><div class=\"email hd\">Work Scope</div>";
         $content .= "<div class=\"subscope hd\">Sub Scopes</div>\n";
         $content .= "<div class=\"links hd\"  style=\"width:110px;\"  >Actions</div></div>\n";
         $lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$crew_id = $rs->fields['crew_id'];
				$crew_name = !empty($rs->fields['crew_name'])  ? $rs->fields['crew_name'] : " ";
				$sub_scopes = trim($rs->fields['sub_scopes']);
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
            $content .= "<div class=\"email $ln highh\"><a href=\"crew.php?action=find&amp;crew_id=$crew_id\" title=\"View Crew\" class=\"name\" style=\"color:black;\" >$crew_name</a></div>";
            $content .= "<div class=\"subscope $ln highh\"  >$sub_scopes</div>\n";
            if ($this->sessionProfile > 2 ) {
            	$content .= "<div class=\"links $ln highh\"  style=\"width:110px;\"  >\n";
				 	$content .= "<div style=\"margin:auto;width:100px;\" >\n";
					$content .= "<a href=\"crew.php?action=update&crew_id=$crew_id\" title=\"Edit Work Scope\" class=\"linkbutton\" >Edit</a>\n";
					//$content .= "</a><a href=\"crew.php?action=delete&crew_id=$crew_id\" title=\"Delete Work Scope\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >Delete</a>
               $content .= "</div>\n";
            }
            else {
               $content .= "<div class=\"links $ln highh\"  style=\"width:110px;\"  > &nbsp;</div>\n";
            }
            $content .= "</div></div>";
            $lineNo += 1;
            $rs->MoveNext();
         }
         $content .= "<hr /></div>";



			$this->page = str_replace('##MAIN##',$content,$this->page);

		}
		private function subCrewDiv($DISABLED) {
			$cont = "";
			$lineNo = 0;
			$cont .= "<div class=\"div95\"  >";
			$cont .= "<fieldset ><legend style=\"margin-left:40.5%;\">Sub Scopes</legend>\n";
			$cont .= "<div id=\"subDiv\" >\n";
			if (count($this->subCrewArr) > 0 ) {
				foreach($this->subCrewArr as $ind=>$val) {
					$ln= $lineNo % 2 == 0  ? "line1" : "";
					extract($val);
               $cont .= "<div id=\"line_$lineNo\" >\n";
					$cont .= "<input type=\"hidden\" name=\"sub_crew_id[]\" value=\"$sub_crew_id\" /> <div class=\"div87\"  ><input type=\"text\"  name=\"sub_crew_name[]\" value=\"$sub_crew_name\" $DISABLED /></div>\n";
					//$cont .= "<div class=\"div10 marg3\" >  <button class=\"linkbutton bckred\"  style=\"top:0.4rem;\" onclick=\"delsubscope($sub_crew_id,$lineNo);return false;\" >Delete</button>

					$cont .= "</div>\n";
					$lineNo += 1;
				}
			}
			$cont .= "</div><div style=\"clear:both;height:0.1rem;\" ></div>\n";
			if ($this->action != "find" ) {
				$cont .= "<div ><button class=\"button tiny\"  onclick=\"addsubscope();return false;\">Add Sub Scope</button></div>\n";
			}
			$cont .= "</fieldset></div>\n";
			return $cont;

		}

		private function getCrewDetails($crewID) {
			$sql = "SELECT *  from crew  where crew_id = $crewID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->crewID = $data['crew_id'];
			$this->crewName = $data['crew_name'];
			$this->contractorID = $data['contractor_id'];
			$sql = "SELECT sub_crew_id,sub_crew_name from sub_crew where crew_id = $crewID and removed is false order by sub_crew_name";
			if (! $this->subCrewArr = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}

		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteCrew();
			}
			$this->crewID = $_POST['crew_id'];
			$this->crewName = ucwords(strtolower(trim(addslashes($_POST['crew_name']))));
			$this->contractorID = intval($_POST['contractor']);
			$this->subCrewArr = $_POST['sub_crew_name'];
			$this->subCrewIDs = $_POST['sub_crew_id'];
			
			$this->submitCrew();
		}
		private function deleteCrew() {
			$sql = "UPDATE crew set removed = TRUE where crew_id = $this->crewID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: crew.php?action=list");
			exit;
		}
		private function submitCrew() {
			if ($this->crewID == 0 ) {
				$sql = "INSERT into crew values (nextval('crew_crew_id_seq'),E'$this->crewName',false,$this->contractorID) returning crew_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->crewID = $rs->fields['crew_id'];
			}
			else { // Update
				$sql = "UPDATE crew set crew_name = E'$this->crewName',contractor_id = $this->contractorID  where crew_id = $this->crewID";
				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
			}
			foreach($this->subCrewIDs as $ind=>$subID) {
				$sql = NULL;
				$subCrewName = ucwords(strtolower(pg_escape_string($this->subCrewArr[$ind])));
				if(!empty($subCrewName)) {
					if($subID == 0  ) {
						$sql = "INSERT into sub_crew values (nextval('sub_crew_sub_crew_id_seq'),$this->crewID,E'$subCrewName',false)";
					}
					else {
						$sql = "UPDATE sub_crew  set sub_crew_name = E'$subCrewName' where sub_crew_id = $subID";
					}

				}
				if (!is_null($sql)) {
					if (! $res=$this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}

				}	
			}

			header("LOCATION: crew.php?action=find&crew_id=$this->crewID");
			exit;
		}
	}
?>
