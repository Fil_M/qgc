<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class CompOneEmployee {
		public $page;
		public $ID;
		public $firstName;
		public $lastName;
		public $userName;
		public $password;
		public $nokName;
		public $nokFone;
		public $address_type;
		public $action;
		public $sessionProfile;
		public $certificateObj;
		public $companyID = 1;
		public $conn;
		public $email;
		public $empType;

		public function	__construct($action,$ID=0,$search="") {
			$this->ID = $ID;
			$this->action = $action;
			$this->companyID = 1;
			$this->conn= $GLOBALS['conn'];
			$this->address_type = "emp";
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('com_employee');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New Third Party Login";
						break;
					case "update" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text = "Updating Third Party Login $ID";
						$this->getEmployeeDetails($ID);
						break;
					case "delete" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text = "Delete Third Party Login $ID";
						$this->getEmployeeDetails($ID);
						break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List Logins - $search" : "List Logins - All";
						$this->getAllEmployees($search) ;
						break;
					case "find" : 
						$heading_text = "Third Party Login $ID";
						$this->getEmployeeDetails($ID);
						break;
					default:
						$heading_text = "Add New Third Party Login";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->ID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Login";
						break;
					case "update":
						$button = "Update Login";
						break;
					case "delete":
						$button = "Delete Login";
						$DISABLED='disabled';
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Login";
						break;
				}
			$content = <<<FIN
<div style="width:1400px;margin:auto;">
<form name="comp_one_emp" id="employee" method="post" action="comp_one_employee.php?SUBMIT=SUBMIT">
<input type="hidden" name="id" value="$this->ID" />
<input type="hidden" name="prev_password" value="$this->password" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="emp_type" value="$this->empType" />
<fieldset ><legend style="margin-left:46%;">Third Party Login</legend>
<div class="div22" ><label>First Name<input type="text" name="firstname" id="firstname"  value="$this->firstName" $DISABLED /></label></div>
<div class="div22 marg2" ><label>Last Name<input type="text" name="lastname" id="lastname" value="$this->lastName" $DISABLED /></label></div>
<div class="div12 marg2" ><label>User Name<input type="text" name="username" id="username" value="$this->userName" $DISABLED /></label></div>
<div class="div12 marg2" ><label>Password<input type="text" name="password" id="password" value=""  $DISABLED /></label></div>
<div class="div24 marg2" ><label>Email<input type="text" name="email" id="email"  $DISABLED value="$this->email" /></label></div>
<script>
$("#commp_one_employee").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
</script>
FIN;
				$content .= "</fieldset>\n";
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for find employee 
				}


				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllEmployees($search="") {
			$whereClause="";
			if (strlen($search) > 0 ) {
				$whereClause = " and upper(lastname) like '$search%' ";

			}
			$sql = "SELECT e.employee_id,e.firstname || ' ' || coalesce(e.lastname,'') as employee_name,e.username,e.emp_type,
			contact_email
			from employee e 
			LEFT JOIN contact c using (employee_id)
			where e.emp_type = 'EXT' and e.removed is false   and e.company_id = $this->companyID $whereClause  and employee_id > 2 order by e.lastname LIMIT 100";
			
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content = "<div style=\"width:1160px;margin:auto;\">\n";
			$content .= AdminFunctions::AtoZ("comp_one_employee.php??action=list",$search);
			$content .= "<div style=\"clear:both;height:20px;\"></div>\n";
			$content .= "<div class=\"heading\" ><div class=\"addr hd\">Login Name</div>\n<div class=\"addr hd\">Username</div>\n";
			$content .= "<div class=\"comments hd\">Email</div>\n<div class=\"links hd\" style=\"width:196px;\" >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln = $lineNo % 2 == 0  ? "line1" : "";
				$id = $rs->fields['employee_id'];
				$employee_name = strlen($rs->fields['employee_name']) > 2 ? $rs->fields['employee_name'] : " ";
				$user_name = strlen($rs->fields['username']) > 2 ? $rs->fields['username'] : " ";
				$user_name = $user_name ;
				$email = strlen($rs->fields['contact_email']) > 2 ? $rs->fields['contact_email'] : " ";
            if ($rs->fields['emp_type'] == 'EXT') {
					$email=AdminFunctions::contactEmail($id);
				}
				 $content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				//$content .= "<div class=\"addr $ln highh\"> <a href=\"employee.php?action=find&id=$id\" title=\"View Login\" class=\"name $ln\" >$employee_name</a></div>";
				$content .= "<div class=\"addr $ln highh\"> $employee_name</div>";
            $content .= "<div class=\"addr $ln highh\">$user_name</div>";
				$content .= "<div class=\"comments $ln highh\">$email</div>";
				if ($this->sessionProfile > 2 ) {
					$content .= "<div class=\"links $ln cntr highh\" style=\"width:196px;\" >\n";
					$content .= "<div style=\"margin:auto;width:100px;\" >\n";
					$content .= "<a href=\"comp_one_employee.php?action=update&id=$id\" title=\"Edit Login\" class=\"linkbutton\" >EDIT</a>";
					$content .= "<a href=\"comp_one_employee.php?action=delete&id=$id\" title=\"Delete Login\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
				}
				else {
					$content .="<div class=\"links $ln highh\" style=\"width:196px;\" > &nbsp;";
				}
				$content .= "</div></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getEmployeeDetails($ID) {
			$sql = "SELECT e.*,con.contact_email,adr.address_id,a.email from employee e 
			LEFT JOIN address_to_relation adr on adr.employee_id = e.employee_id  
			LEFT JOIN address a using(address_id)
			LEFT JOIN contact con on con.employee_id = e.employee_id
			where e.employee_id = $ID";

			if (! $data = $this->conn->getRow($sql)) {
				echo $sql;
				die($this->conn->ErrorMsg());
			}

			$this->ID = $data['employee_id'];
			$empType = $data['emp_type'];
			if ($empType == "EXT" ) {
				$this->email = $data['contact_email'];
			}
			else {
				$this->email = $data['email'];
			}
			$this->empType = $data['emp_type'];
			$this->firstName = $data['firstname'];
			$this->lastName = $data['lastname'];
			$this->userName =   $data['username'];
			$this->password = $data['password'];
			$this->addressID = intval($data['address_id']);

		}
		private function processPost() {
			$this->ID = $_POST['id'];
         if ($this->action == "delete" ) {
				$this->deleteEmployee();
			}
			$this->firstName = ucfirst(trim($_POST['firstname']));
			$this->lastName = ucfirst(trim(addslashes($_POST['lastname'])));
			$this->userName = trim($_POST['username']);
			$this->userName = strlen($this->userName) > 0 ? $this->userName : uniqid(true);
		 	$this->password =  strlen(trim($_POST['password'])) > 1  ? md5(trim($_POST['password'])) : $_POST['prev_password']; // use previous if not given
		 	$this->empType =  ($_POST['emp_type']);
		 	$this->email = trim(addslashes($_POST['email']));


			
			$this->submitEmployee();
		}
		private function deleteEmployee() {
				$sql = "UPDATE contact set removed = TRUE where employee_id = $this->ID";
			if (! $this->conn->Execute($sql)) {
				die( $this->conn->ErrorMsg());
			}
			header("LOCATION: comp_one_employee.php?action=list");
			exit;
		}
		private function submitEmployee() {
			if ($this->ID == 0 ) {
				$sql = "INSERT into employee values (nextval('employee_employee_id_seq'),'$this->userName','$this->password','$this->firstName',E'$this->lastName',NULL,FALSE,
				3,E'$this->nokName','$this->nokFone',$this->companyID) returning employee_id";
				try {
					if (! $rs =  $this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
					$this->ID = $rs->fields['employee_id'];
			}
			else { // Update
					$sql = "UPDATE employee set username = '$this->userName',password = '$this->password',firstname= '$this->firstName',lastname = E'$this->lastName',
					profile_id=3,next_of_kin_name = E'$this->nokName',next_of_kin_fone = '$this->nokFone'  where employee_id = $this->ID";
				try {
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
				if ($this->empType == "CON" ) {
					$sql = "update address set email = '$this->email'  where address_id = ( select address_id from address_to_relation where employee_id = $this->ID)";
				}
				else { // EXT
					$sql = "update contact set contact_email = '$this->email'  where  employee_id = $this->ID";
				}
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				if ($this->empType == "EXT" ) {
					$sql = "UPDATE contact set contact_email = '$this->email' where employee_id = $this->ID";
					if (! $rs =  $this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
					
				}
	
			}
			header("LOCATION: comp_one_employee.php?action=list");
		}
	}
?>
