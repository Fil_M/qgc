<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Well  {
		public $page;
		public $wellID;
		public $areaID;
		public $wellName;
		public $contractorID;
		public $areaName;
		public $action;
		public $sessionProfile;
		public $conn;
		public $siteIns;
		public $calloffID;

		public function	__construct($action,$wellID=0,$search="",$redo=NULL) {
			$this->wellID = $wellID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('well');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
                  $heading_text = "Add New Well";
                  break;
               case "update" :
                  if ($wellID < 1 ) {
                     return false;
                  }
                  $heading_text = "Updating Well $wellID";
                  $this->getWellDetails($wellID);
                  break;
               case "delete" :
                  if ($wellID < 1 ) {
                     return false;
                  }
                  $heading_text = "Delete Well $wellID";
                  $this->getWellDetails($wellID);
                  break;
					case "list" :
						$heading_text =   "List Wells";
						$this->getAllWells($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Well $wellID";
						$this->getWellDetails($wellID);
						break;
					default:
						$heading_text = "List Wells - All";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->wellID > 0 )) {
				$DISABLED="";
				switch ($action) {
					 case "new":
                  $button = "Add Well";
                  break;
               case "update":
                  $button = "Update Well";
                  break;
               case "delete":
                  $button = "Delete Well";
                  break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "No Button";
						break;
				}
			$content = <<<FIN
<div style="width:40%;margin:auto;" >
<form name="well" method="post" action="well.php">
<input type="hidden" name="well_id" value="$this->wellID" />
<input type="hidden" name="area_id" id="areaID" value="$this->areaID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:49.5%;">Well</legend>
<div class="div49" ><label for="area_name" >Area<input type="text" name="area" id="area" value="$this->areaName" $DISABLED /></label></div>
<div class="div49 marg1" ><label for="well_name" class="label" >Well Name<input type="text" name="well_name" id="well_name"  value="$this->wellName" $DISABLED /></label></div>
</fieldset>
<script>
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data); return false; }
      });
  });
</script>
FIN;
			  if ($this->action != "find" ) {
               $content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found well
            }
             $content .= "\n<a href=\"well.php?action=list&redo=redo\"   class=\"button\" >Return to List</a>";

				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllWells($search="",$redo) {
			  $whereClause=" where w.removed is false ";
           if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
         if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if (intval($search->cooID) > 0 ) {
               $this->calloffID = $search->cooID;
               $whereClause .= " and w.well_id in (select (unnest(string_to_array(co.well_ids::text,'|')::int[])) from calloff_order co where calloff_order_id = $search->cooID)";
            }
            else if (intval($search->areaID) > 0 ) {
               $this->areaID = $search->areaID;
               $this->areaName = $search->areaName;
               $whereClause .= " and w.area_id = $this->areaID ";
            }
            if (intval($search->wellID) > 0 ) {
               $this->wellID = $search->wellID;
               $whereClause .= " and w.well_id  = $this->wellID ";
            }
         }
         else {
             $search = new FieldSearch();
             $_SESSION['SQL'] = pg_escape_string(serialize($search));
         }

         //echo $sql;

			$sql = "SELECT w.*,a.area_name from well w LEFT JOIN area a using (area_id) $whereClause order by area_name ,well_name LIMIT 200";

			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content ="<div style=\"width:1160px;margin:auto;\">";
         $content .= <<<FIN
         <form name="field" method="post" action="well.php?action=list">
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         <input type="hidden" name="contractorID" id="contractorID" value="0" />
			<fieldset style="width:96.25%;"><legend style="margin-left:48%;">Filter Search</legend>
			<div class="div10" ><label for="coo" >Calloff Order<input type="text" name="coo" id="coo"  value="$this->calloffID" /></label></div>
			<div class="div25 marg" ><label for="area" >Area<input type="text" name="area" id="area"  value="$this->areaName" /></label></div>
			<div class="div30 marg" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".input.date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),'#contractorID'); return false; }
      });
  });
$(function () {
      $('#coo').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=calloffs',
         minChars:2
      });
  });
</script>
FIN;
         $content .= "<div style=\"float:right;width:22%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button  onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
         //$content .= "<a href=\"hours.php?action=list&search=true&printcsv=true\" class=\"submitbutton printcsv\" >Print C.S.V.</a>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\"  ><div class=\"workscope hd\">Well Name</div><div class=\"workscope hd\">Area</div>";
			$content .= "<div class=\"workscope hd\">CallOff Orders</div>\n";
			$content .= "<div class=\"links hd\" style=\"width:156px;\"  >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$calloffs = "";
				$ln= $lineNo % 2 == 0  ? "line1" : "line2";
				$well_id = $rs->fields['well_id'];
				$area_id = $rs->fields['area_id'];
				$well_name = strlen($rs->fields['well_name']) > 0 ? $rs->fields['well_name'] : " ";
				$area = strlen($rs->fields['area_name']) > 0 ? $rs->fields['area_name'] : " ";
				$sql2 = "SELECT * from calloffs($area_id,$well_id)";
            if (! $arr = $this->conn->getAll($sql2)) {
               if ($this->conn->ErrorNo() != 0 ) {
                  die($this->conn->ErrorMsg());
               }
            }
            if (count($arr) > 0 ) {
               foreach($arr as $ind=>$val) {
                  $id = $val['calloff_order_id'];
                  $calloffs .= "<a href=\"calloff.php?action=find&calloff_id=$id\"  class=\"name\">$id</a>&nbsp;,&nbsp;";
               }
               $calloffs = preg_replace('/,&nbsp;$/',"",$calloffs);
            }
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"workscope $ln highh\"><a href=\"well.php?action=find&amp;well_id=$well_id\" title=\"View Well\" class=\"name\" style=\"color:black;\" >$well_name</a></div>\n";
				$content .= "<div class=\"workscope $ln highh\">$area</div>\n";
				$content .= "<div class=\"workscope $ln highh\" style=\"word-wrap:normal; word-break:break-all;\" >$calloffs</div>\n";
				 if ($this->sessionProfile > 2 ) {
            	$content .= "<div class=\"links $ln highh\"  style=\"width:156px;\" >\n";
					$content .= "<div style=\"margin:auto;width:100px;\" >\n";
					$content .= "<a href=\"well.php?action=update&well_id=$well_id\" title=\"Edit Well\" class=\"linkbutton\" >EDIT</a>\n";
					$content .= "<a href=\"well.php?action=delete&well_id=$well_id\" title=\"Delete Well\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DELETE</a>\n";
					$content .= "</div>\n";
            }
            else {
               $content .= "<div class=\"links $ln highh\"  style=\"width:156px;\"  > &nbsp;\n";
            }

				$content .= "</div></div>\n";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getWellDetails($wellID) {
			$sql = "SELECT w.*,a.area_name  from well w
						LEFT JOIN area a using (area_id) 
					  where well_id = $wellID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->areaID = $data['area_id'];
			$this->areaName = $data['area_name'];
			$this->wellID = $data['well_id'];
			$this->wellName = $data['well_name'];
		}

		 private function processPost() {
         if ($this->action == "delete" ) {
            $this->deleteWell();
         }
         $this->areaID = intval($_POST['area_id']);
         $this->wellID = $_POST['well_id'];
         $this->wellName = trim(addslashes($_POST['well_name']));

         $this->submitWell();
      }
      private function deleteWell() {
         $sql = "UPDATE well set removed = TRUE where well_id = $this->wellID";
         if (! $this->conn->Execute($sql)) {
            die($this->conn->ErrorMsg());
         }
         header("LOCATION: well.php?action=list");
         exit;
      }
		private function submitWell() {
         if ($this->wellID == 0 ) {
            $sql = "INSERT into well values (nextval('well_well_id_seq'),$this->areaID,E'$this->wellName',FALSE,0) returning well_id";
				try {
               if (! $rs =  $this->conn->Execute($sql)) {
                  die( $this->conn->ErrorMsg());
               }
            } catch (Exception $e) {
               if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
                  die("This Well exists for this Area already");
               }
               else {
                  die($e->getMessage());
               }
         	}

            $this->wellID = $rs->fields['well_id'];
         }
         else { // Update
            $sql = "UPDATE well set well_name = E'$this->wellName',area_id = $this->areaID  where well_id = $this->wellID";

				try {
               if (! $rs =  $this->conn->Execute($sql)) {
                  die( $this->conn->ErrorMsg());
               }
            } catch (Exception $e) {
               if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
                  die("This Well exists for this Area already");
               }
               else {
                  die($e->getMessage());
               }
         	}
         }

         //Inserts
         $contractor = isset($_POST['contractor']) ? $_POST['contractor'] : NULL;
         $callno = isset($_POST['callno']) ? $_POST['callno'] : NULL;
         $contractor = isset($_POST['call_contractor']) ? $_POST['call_contractor'] : NULL;

         if (!is_null($callno)) {
            $sql = "INSERT into calloff_order (calloff_order_id,contractor_id,area_id,well_id) values ";
            foreach($callno as $ind=>$no) {
               $callID=intval($callno[$ind]);
               $conID = intval($contractor[$ind]);
               $sql .= "($callID,$conID,$this->areaID,$this->wellID),";
            }
            $sql = preg_replace('/,$/',"",$sql);
            if (!$res=$this->conn->Execute($sql)) {
               die($this->conn->ErrorMsg());
            }
         }
         header("LOCATION: well.php?action=find&well_id=$this->wellID");
         exit;

      }

	}
?>
