<?php
//QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class UnlockDocketDay  {
		public $page;
		public $action;
		public $sessionProfile;
		public $crewID=0;
		public $areaID=0;
		public $wellID=0;
		public $callOffID;
		public $empID;
		public $empName;
		public $areaName;
		public $conn;
		public $statusArr= array("0"=>"#e1b500","1"=>"#60854e","2"=>"#ef3e55");
		public $employeeID;
		public $contractorID = NULL;
		public $contractorName;
		public $shortName;
		public $conDomain;
		public $dbLink;
		public $equalgreater;
      public $equalto;
      public $between;
      public $less;
      public $hideEndDate='style="display:none;"';
      public $endDate;
		public $comDate;



		public function	__construct($action,$cooID=0,$conID=NULL,$search=NULL,$redo=NULL) {
			$this->action = $action;
			$this->callOffID = $cooID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			$this->conn = $GLOBALS['conn'];
			$this->contractorID = $conID;
			$pg = new Page('unlock');
			$this->page= $pg->page;
			if (! is_null($conID)) {
					$connArr = AdminFunctions::getConName($conID);
					$this->contractorName = $connArr['con_name'];
					$this->shortName = $connArr['name'];
					$this->conDomain = $connArr['domain'];
					$this->dbLink = $connArr['db_link'];
			}
        	switch($action) {
				case "list" :
				$heading_text =   "List Percentage Complete Locked COO's - $this->contractorName";
				$this->getAllLockedCOOs($search,$redo) ;
				break;

				default:
				$heading_text = "List Percentage Complete";
				break;
			}
		 	$this->setHeaderText($heading_text);	
			echo $this->page;
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function getAllLockedCOOs($search,$redo) {
			if (!is_null($this->contractorID)) {
				$URL="https://".$_SERVER['HTTP_HOST'];
				if (! is_null($redo)) {
            	if (isset($_SESSION['SQL'])) {
               	$search = unserialize($_SESSION['SQL']);
            	}
            	else {
               	$search=NULL;
            	}
         	}
				if (! is_null($search)) {
					$_SESSION['SQL'] = pg_escape_string(serialize($search));
		  			$whereClause=" where 1 = 1 ";
         		if (!empty($search->cooID) ) {
            		 $this->callOffID = $search->cooID;
             		$whereClause .= " and call.calloff_order_id = $this->callOffID ";
         		}
					else if (!empty($search->creDate))  {
                     $dateSelected = true;
                     $whereClause .= " and co.commencement_date ";
                     $this->comDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
                        $this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (!empty($search->endDate)  ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->equalgreater = NULL;
                           $this->endDate = $search->endDate;
                           $this->hideEndDate='';
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
								$this->comDate = AdminFunctions::beginLastMonth();
								$whereClause .= " >=  '$this->comDate'";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                  }
					}
					else {
						$this->comDate = AdminFunctions::beginLastMonth();
						$whereClause .= " and co.commencement_date >=  '$this->comDate'";
                  $this->equalgreater = "SELECTED=\"selected\"";
					}
					if (!empty($search->areaID)  ) {
               	$whereClause .= " and co.area_id = $search->areaID ";
               	$this->areaName = $search->areaName;
               	$this->areaID = $search->areaID;
           		}

					if (!empty($search->wellID)  ) {
               	$whereClause .= " and $search->wellID in (select (unnest(string_to_array(co.well_ids::text,'|')::int[])))";
               	$this->wellID = $search->wellID;
           		}
				}
				else {
					$this->comDate = AdminFunctions::beginLastMonth();
					$whereClause = " where co.commencement_date >=  '$this->comDate'";
					$search = new FieldSearch();
            	$search->dateType = "equalgreater";
            	$search->creDate = $this->comDate;
					$_SESSION['SQL'] = pg_escape_string(serialize($search));
				}
				$content ="<div style=\"width:1800px;text-align:center;\">";
				$sql = "with call as  (select * from (with dock as (select max(dd.percentage_complete) as mx, docket_day_id,calloff_order_id 
					from {$this->shortName}_docket_day dd group by docket_day_id,calloff_order_id)
				select max(dd.percentage_complete) as maxx,dd.calloff_order_id,completion_cert_id from {$this->shortName}_docket_day dd
				JOIN dock using(docket_day_id)
				LEFT JOIN {$this->shortName}_completion_cert cc on cc.calloff_order_id = dd.calloff_order_id
				where dock.mx >= 100
				and  coalesce(completion_cert_id,0) = 0
				group by dd.calloff_order_id,completion_cert_id) as sub) 
				select call.*,co.commencement_date,a.area_name,c.crew_name,wells_from_ids(co.well_ids) as well_name
 				from call
  				LEFT JOIN calloff_order co using(calloff_order_id)
  				LEFT JOIN area a  on a.area_id = co.area_id 
  				LEFT JOIN crew  c on c. crew_id = co.crew_id 
				$whereClause
				order by calloff_order_id";

			//echo $sql;
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}


 			$content ="<div style=\"margin:auto;width:1241px;\" >";
         // search
         $content .= <<<FIN
         <form name="unlock" method="post" action="unlock_docket_day.php?action=list">
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         <input type="hidden" name="con_id" id="con_id" value="$this->contractorID" />
         <input type="hidden" name="emp_id" id="emp_id" value="$this->empID" />
<fieldset ><legend style="margin-left:40%;">Filter Search - Locked Completions</legend>
<div class="div7" ><label >C.O.O.<input type="text" name="calloff_id" id="coo" value="$this->callOffID" /></label></div>
<div class="div12 marg1" ><label for="datetype" >Date is:</label><select class="sel" name="datetype" id="datetype"  onchange="displayDiv('#datetype');">
<option value="equalgreater" $this->equalgreater >Equal or greater</option>
<option value="equalto" $this->equalto >Equal to</option>
<option value="between" $this->between >Between</option>
<option value="less" $this->less >Less than</option>
</select></div>
<div class="div9 marg1" ><label>Date<input type="text" name="com_date" id="com_date" class="date" value="$this->comDate" /></label></div>
<div class="div9 marg1" id="enddiv" $this->hideEndDate ><label>and Date<input type="text" name="end_date" id="end_date" class="date" value="$this->endDate" /></label> </div>
<div class="div18 marg1" ><label>Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div20 marg1" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= "</div></div>\n";
$content .= <<<FIN
<script>
$.fn.clearForm = function() {
      return this.each(function() {
       var type = this.type, tag = this.tagName.toLowerCase();
       var idd = $(this).attr('id');
       if (tag == 'form' ) {
          return $(':input',this).clearForm();
       }
       if (type == 'text' || type == 'password' ||  tag == 'textarea' || type == 'hidden' ) {
         if (idd != 'contractorID') {
            this.value = '';
         }
       }   
       else if (type == 'checkbox' || type == 'radio') {
          this.checked = false;
       }
       else if (tag == 'select') {
         this.selectedIndex = -1;
       }
      });
    };
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_s',$('#welldiv'),'#con_id'); return false; }
      });
  });
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#coo').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=coo&con_id=$this->contractorID',
         minChars:3
      });
  });
</script>
FIN;
			$content .= "<div style=\"float:right;width:19.5%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button  onclick=\"$('form').clearForm();return false;\" >Reset</button>";
         $content .= "</div></fieldset></form>\n";
			//$content .= "<div style=\"clear:both;\" >\n";


			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"fone hd\">Comm Date</div>";
			$content .= "<div class=\"fone hd\">C.O.O.</div>\n";
			$content .= "<div class=\"cli hd\">Work Scope</div>\n";
			$content .= "<div class=\"cli hd\">Area</div>\n";
			$content .= "<div class=\"addr hd\">Well/s</div>\n";
			$content .= "<div class=\"crew hd\" >Percentage Complete</div>\n";
			$content .= "<div class=\"links hd\" style=\"\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				extract($rs->fields);
				$cooDate = AdminFunctions::dbDate($commencement_date);
				$color = "#ffffff;";
				$ln= $lineNo % 2 == 0  ? "line1" : "line2";
				$content .= "<div id=\"linediv_$lineNo\" class=\"$ln\" style=\"float:left;width:100%;display:table;table-layout:fixed;border-bottom:1px solid #000000;\" >\n";
				  $content .= "<div class=\"fone highh cntr\">$cooDate</div>";

				$content .= "<div class=\"fone highh cntr\">$calloff_order_id</div>\n";
				$content .= "<div class=\"cli highh\">$crew_name</div>\n";
				$content .= "<div class=\"cli highh\">$area_name</div>\n";
				$content .= "<div class=\"addr highh\">$well_name</div>\n";
				$content .= "<div class=\"crew cntr highh\" id=\"perc_div_$lineNo\" >$maxx</div>\n";
				//$content .= "</a>\n";
				if ($this->sessionProfile > 2 ) {
           		$content .= "<div class=\"links highh cntr\" >\n";
					if ($this->sessionProfile >= 10 ) {
						  $content .= "<button  class=\"button tiny margt4\" onclick=\"updateDocket($calloff_order_id,$this->contractorID,'#perc_div_$lineNo');return false;\">Reset Perc</button>";
					}
					$content .= "</div>\n";
				}
				else {
					$content .= "<div class=\"links highh\" > &nbsp;</div>";
				}
				$content .= "</div><div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "<hr /></div>";
		}
		else { // ContractorID NULL show contrcator select
			$content = AdminFunctions::setContractor("unlock_docket_day.php?action=list" ,$this->contractorID);
   	}	

		$this->page = str_replace('##MAIN##',$content,$this->page);

		}

	}
?>
