<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class CostEstimate  {
		public $companyID;
		public $page;
		public $requestID;
		public $fieldEstID;
		public $empID;
		public $contractorID=0;
		public $action;
		public $sessionProfile;
		public $conn;
		public $areaID;
		public $wellID;
		public $crewID;
		public $subCrewID;
		public $subCrewIDs;
		public $subCrewStr;
		public $areaName;
		public $constructChecked;
		public $withSumpChecked;
		public $spineChecked;
		public $sumpChecked;
		public $rehabChecked;
		public $otherChecked;
		public $otherText;
		public $siteConditions;
		public $returnGravel;
		public $returnWater;
		public $estStart;
		public $estComplete;
		public $sigDate;
		public $supName;
		public $questions;
		public $equalto;
		public $between;
		public $less;
      public $hideEndDate=true;
      public $equalgreater;
      public $creDate;
      public $endDate;
		public $tceTypeID;
		public $scope;
		public $supIDs;
		public $check;
		public $wellStr;
		public $wellIDs;
		public $qgcName;
		public $landOwner_1;
      public $landOwner_2;
      public $loc_1;
      public $loc_2;
		public $URL;
		public $notifyChecked;
		public $empName;
		public $sigName;
		public $signature;
		public $lineNo;
		public $divType;


		public function	__construct($action,$requestID=0,$lineNo=NULL,$type=NULL,$search=NULL,$redo=NULL) {
			$this->URL="https://".$_SERVER['HTTP_HOST'];
			$this->requestID = $requestID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->lineNo = $lineNo;
			$this->divType = $type;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			$this->signature = AdminFunctions::getSig($this->empID);
			$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('cost');
				$this->page= $pg->page;
         	switch($action) {
					 case "new" :
                  $heading_text = "Add New Target Cost Request";
						$this->sigDate = date('d-m-Y');
                  break;

					case "list" :
						$heading_text =   "List Target Cost Estimates";
						$this->getAllEstimates($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Target Cost Estimate Request $requestID";
						$this->getEstimateDetails($requestID);
						break;
					 case "delete" :
                  if ($requestID < 1 ) {
                     return false;
                  }
                  $heading_text = "Delete Target Cost Estimate $requestID";
                  $this->getEstimateDetails($requestID);
                  break;
					case "update" :
                  if ($requestID < 1 ) {
                     return false;
                  }
                  $heading_text = "Updating Target Cost Estimate $requestID";
                  $this->getEstimateDetails($requestID);
                  break;
					default:
						//$heading_text .= "Add New Estimate";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->requestID > 0 )) {
				switch ($action) {
					 case "new":
                  $button = "Add Cost Estimate";
                  $DISABLED='';
                  break;
               case "update":
                  $button = "Update Cost Estimate";
                  $DISABLED='';
                  break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					case "delete":
						$button = "Delete Cost Estimate";
						$DISABLED='disabled';
						break;
					default:
						$button = "No Button";
						$DISABLED='disabled';
						break;
				}
			$content = <<<FIN
<div style="width:1650px;margin:auto;">
<form name="cost" id="cost" method="post" action="cost_estimate.php?SUBMIT=SUBMIT">
<input type="hidden" name="requestID" value="$this->requestID" />
<input type="hidden" name="empID" value="$this->empID" />
<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
<input type="hidden" name="well_id" value="$this->wellID" />
<input type="hidden" name="contractorID" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="signature" value="$this->signature" />
<fieldset ><legend style="margin-left:44%;">Request For Target Cost Estimate</legend>
<div class="div15" >
FIN;
 			$content .= AdminFunctions::contractorSelect($this->contractorID,$DISABLED,false,"contractor",false,true);
			$content .= "</div>\n";
	    	$content .= "<div class=\"div15 marg2\" ><label for=\"crew\"  >Work Scope</label>\n";
         $content .= AdminFunctions::crewSelect($this->crewID,$DISABLED);
         $content .= "</div><div class=\"div17 marg2 multi\" ><div class=\"div95\"  id=\"subscopediv\">\n";
         $content .= AdminFunctions::subscopeSelectMulti($this->crewID,$this->subCrewIDs,$DISABLED);
			$content .= <<<FIN
</div></div>
<div class="div15 marg2" ><label for="area" >Area<input type="text" class="required" name="area" id="area" value="$this->areaName"  $DISABLED /></label></div>
<div id="welldiv" class="div15 marg2"  >
FIN;
   $content .= AdminFunctions::wellSelectMulti($this->areaID,$this->wellIDs,$DISABLED);
	$content .= "</div>\n";
	$content .= "<div class=\"div10 marg\" ><label >TCE Type</label>\n";
   $content .= AdminFunctions::TCETypeSelect($this->tceTypeID,$DISABLED);

	$content .= <<<FIN
</div>
</fieldset>
<div style="clear:both;" ></div>
<fieldset ><legend style="margin-left:47.6%;">Work Required</legend>
<div id="workdiv" class="div95" style="height:11.5rem !important;min-height:11.5rem;"  >&nbsp;
FIN;
   $locText_1 = AdminFunctions::locSelect($this->loc_1,1,$this->landOwner_1,$DISABLED,"location_1",false,true);
   $locText_2 = AdminFunctions::locSelect($this->loc_2,2,$this->landOwner_2,$DISABLED,"location_2",false,true);
	if ($this->action != "new" ) {
		$arr = explode("|",$this->check);
		for ($x = 1;$x<=8;$x++) {
			$nm = "ch_";
		 	$nm .= $x;
			$$nm = "";
		}
		 foreach($arr as $val) {
			$nm = "ch_";
		 	$nm .= $val;
			$$nm = "checked";
		}
		if ($this->tceTypeID == 1) {
      	$content .= "<div class=\"div50 smll\" ><input type=\"checkbox\" name=\"check_1\" id=\"check_1\"  $ch_1  $DISABLED  /><label>Construct lease pad and associated access road</label></div>\n";
      	$content .= "<div class=\"div49 smll\" ><input type=\"checkbox\" name=\"check_2\" id=\"check_2\"  $ch_2   $DISABLED /><label>Construct lease pad with sump and associated access road</label></div>\n";
      	$content .= "<div class=\"div50 smll\" ><input type=\"checkbox\" name=\"check_3\" id=\"check_3\"  $ch_3   $DISABLED /><label>Gravel pit</label></div>\n";
      	$content .= "<div class=\"div49 smll\" ><input type=\"checkbox\" name=\"check_4\" id=\"check_4\"  $ch_4  $DISABLED /><label>Equipment supply</label></div>\n";
      	$content .= "<div class=\"div50 smll\" ><input type=\"checkbox\" name=\"check_5\" id=\"check_5\"  $ch_5   $DISABLED /><label>Shear, clear and mulch</label></div>\n";
      	$content .= "<div class=\"div49 smll\" ><input type=\"checkbox\" name=\"check_6\" id=\"check_6\"  $ch_6  $DISABLED /><label>Fauna spotter</label></div>\n";
      	$content .= "<div style=\"clear:both;height:0.5rem;\"> </div>\n";
			$content .= "<div class=\"div50\" >\n";
      	$content .= "<div class=\"div95\" ><label>Other</label>";
      	$content .= "<textarea  name=\"othertext\"  id=\"othertext\" class=\"txt\"  $DISABLED >$this->otherText</textarea></div></div>\n";
			$content .= "<div class=\"div50\" >\n";
			$content .= "<div class=\"div98 marg1\" ><label >Scope of Works</label><textarea   name=\"scope\"  id=\"scope\" class=\"txt $DISABLED\" $DISABLED  >$this->scope</textarea></div></div>\n";
   	}
   	else if ($this->tceTypeID == 2 ) {
      	$content .= "<div class=\"div50 smll\" ><input type=\"checkbox\" name=\"check_1\" id=\"check_1\" class=\"chk\" $ch_1   $DISABLED />Fluid Haulage</div>\n";
      	$content .= "<div class=\"div49 smll\" ><input type=\"checkbox\" name=\"check_2\" id=\"check_2\" class=\"chk\" $ch_2  $DISABLED />Rehabilitation of lease pad and associated access road</div>\n";
      	$content .= "<div class=\"div50 smll\" ><input type=\"checkbox\" name=\"check_3\" id=\"check_3\" class=\"chk\" $ch_3   $DISABLED />Rehabilitation of lease pad (with sump) and associated access road</div>\n";
      	$content .= "<div class=\"div49 smll\" ><input type=\"checkbox\" name=\"check_4\" id=\"check_4\" class=\"chk\" $ch_4  $DISABLED />Equipment supply</div>\n";
      	$content .= "<div style=\"clear:both;height:0.5rem;\"> </div>\n";
			$content .= "<div class=\"div50\" >\n";
      	$content .= "<div class=\"div95\" ><label>Other</label>";
      	$content .= "<textarea  name=\"othertext\"  id=\"othertext\" class=\"txt $DISABLED \"  $DISABLED  >$this->otherText</textarea></div></div>\n";
			$content .= "<div class=\"div50\" >\n";
			$content .= "<div class=\"div98 marg1\" ><label >Scope of Works</label><textarea   name=\"scope\"  id=\"scope\" class=\"txt $DISABLED\" $DISABLED  >$this->scope</textarea></div></div>\n";

   	}
   	else if ($this->tceTypeID == 3 ) {
      	$content .= "<div class=\"div50 smll\" ><input type=\"checkbox\" name=\"check_1\" id=\"check_1\" class=\"chk\" $ch_1   $DISABLED />On-Plot Road Asset Management Process</div>\n";
      	$content .= "<div style=\"clear:both;height:0.5rem;\"> </div>\n";
			$content .= "<div class=\"div50\" >\n";
      	$content .= "<div class=\"div95\" ><label>Other</label>";
      	$content .= "<textarea  name=\"othertext\"  id=\"othertext\" class=\"txt $DISABLED \"  $DISABLED  >$this->otherText</textarea></div></div>\n";
			$content .= "<div class=\"div50\" >\n";
			$content .= "<div class=\"div98 marg1\" ><label >Scope of Works</label><textarea   name=\"scope\"  id=\"scope\" class=\"txt $DISABLED\" $DISABLED  >$this->scope</textarea></div></div>\n";
		}
   	$content .= "<div style=\"clear:both;\"> </div>\n";


  // echo $locText_1 ;
   //echo $locText_2 ;
	//exit;
}
/*


*/
$content .= <<<FIN
</div>
<div class="div50" >
<div class="div18" ><label  >Est Start Date<input type="text" name="start" id="start" class="date" value="$this->estStart"  $DISABLED /></label></div>
<div class="div18" style="float:right;margin-right:1.3%;" ><label   >Est Completion Date<input type="text" name="complete" id="complete" class="date" value="$this->estComplete"  $DISABLED /></label></div>
</div>
<div class="div50" >
<div class="div98 marg1" ><label for="sitecond" >Site Conditions</label><textarea   name="sitecond" id="sitecond" class="txt" $DISABLED  >$this->siteConditions</textarea></div>
</div>
</fieldset>

<fieldset ><legend style="margin-left:47.1%;">Water/Gravel</legend>
<div style="clear:both;height:5px;" > </div>
<div class="div50" >
<div id="landdiv" >
<div class="div50" >
FIN;
   $content .= AdminFunctions::ownerSelect($this->landOwner_1,1,$DISABLED,"landowner_1",true,true);
   $content .= "</div></div></div>\n";
	$content .= "<div class=\"div50\" ><div id=\"locdiv_1\" class=\"div50 marg1\"  >$locText_1 </div>\n";
   $content .= "<div class=\"div15\" style=\"float:right;margin-right:2.2%;\" ><label >Return Distance<input type=\"text\" name=\"returngravel\" id=\"returngravel\" class=\"num\"  value=\"$this->returnGravel\" $DISABLED  /></label></div> \n";
$content .= <<<FIN
</div>
<div style="clear:both;height:5px;" ></div>
<div class="div50" >
<div id="waterdiv" >
<div class="div50" >
FIN;
   $content .= AdminFunctions::ownerSelect($this->landOwner_2,2,$DISABLED,"landowner_2",true,true);
   $content .= "</div></div></div>\n";
	$content .= "<div class=\"div50\" ><div id=\"locdiv_2\" class=\"div50 marg1\"  >$locText_2 </div>\n";
   $content .= "<div class=\"div15\" style=\"float:right;margin-right:2.2%;\" ><label >Return Distance<input type=\"text\" name=\"returnwater\" id=\"returngravel\" class=\"num\"  value=\"$this->returnWater\" $DISABLED  /></label></div> \n";
$content .= <<<FIN
</div>
<div style="clear:both;height:15px;" > </div>
</fieldset>
<fieldset ><legend style="margin-left:46%;">Signed for QGC</legend>
<div class="div50" ><label for="supervisor" >Supervisor/s</label>
<div class="div50" >
FIN;
$content .= AdminFunctions::supervisorSelect($this->supIDs,$DISABLED,true);
$content .= <<<FIN
</div></div>
	<div class="div24" ><img src="$this->signature"  alt="QGC to Sign"  height="180" width="300" /></div>
	<div class="div25 marg1" >
	<div class="div35 marg2"  ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"  class="cntr"  value="$this->sigDate" disabled/></label></div>
	<div class="div94 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->sigName"  readonly /></label></div>
	<div class="div94 marg2" ><label >Notify Contractor</label><input type="checkbox" name="notify" id="notify"  value="notified"  $this->notifyChecked /></div>
	</div>

</fieldset>
<script>
jQuery('.num').keyup(function () { this.value = this.value.replace(/[^0-9\.]/g,''); });
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_m',$('#welldiv'),'#contractorID'); return false; }
      });
  });
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});

 $('#contractor').change(function() {
   var conid = $('#contractor').val();
   $('#contractorID').val(conid);
   });
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','multi'); });
$("#cost").submit(function(){
	var isFormValid = true;
    $("#cost input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#cost .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
    else {
      $('#SUBMIT').prop("disabled", "disabled");
      return true;
    }
});
// use this for dynamic DOM  changes
// Gravel = 1
 $(document).on('change', '#landowner_1', function(){
   var ltypeid = 1;
   var ownerid = $(this).val();
   locSelect(ltypeid,ownerid,'#locdiv_1');
   });

// Water = 2
 $(document).on('change', '#landowner_2', function(){
   var ltypeid = 2;
   var ownerid = $(this).val();
   locSelect(ltypeid,ownerid,'#locdiv_2');
   });
</script>

FIN;
			 	if ($this->action != "find" ) {
               $content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\"  />";
           	}

				$url =  "$this->URL/admin/cost_estimate.php?action=list&redo=redo";
            $content .= "\n<a href=\"$url\"   class=\"button\"  >Last Screen</a>";
		 		$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllEstimates($search,$redo) {
			$this->requestID = NULL;
         $this->equalgreater = "SELECTED=\"selected\"";
			$whereClause=" where 1 = 1 ";

			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if (!empty($search->fieldEstID) ) {
               $this->fieldEstID = $search->fieldEstID;
               $whereClause .= " and r.field_estimate_id = $this->fieldEstID ";
            }
            else if ( !empty($search->requestID)) {
               $this->requestID = $search->requestID;
               $whereClause .= " and r.request_estimate_id = $this->requestID ";
            }
				 else {
               if (!empty($search->creDate) && strlen($search->creDate) > 0) {
                     $dateSelected = true;
                     $whereClause .= " and r.create_date ";
                     $this->creDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
         					$this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->endDate = $search->endDate;
                           $this->hideEndDate=false;
         						$this->equalgreater = NULL;
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause .= " and r.create_date  >=  current_date ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                  }
               }
					if (!empty($search->areaID) ) {
                  $whereClause .= " and r.area_id = $search->areaID ";
                  $this->areaName = $search->areaName;
                  $this->areaID = $search->areaID;
               }
               if (!empty($search->wellID)) {
						$whereClause .= " and $search->wellID in (select (unnest(string_to_array(r.well_ids::text,'|')::int[])))";
                  $this->wellID = $search->wellID;
               }
					if (!empty($search->crewID)  ) {
                  $whereClause .= " and r.crew_id = $search->crewID ";
                  $this->crewID = $search->crewID;
               }
               if (!empty($search->subCrewID)  ) {
                  $whereClause .= " and $search->subCrewID in (select (unnest(string_to_array(r.sub_crew_ids::text,'|')::int[])))";
                  $this->subCrewID = $search->subCrewID;
               }

               if (!empty($search->contractorID) ) {
                  $whereClause .= " and r.contractor_id = $search->contractorID ";
                  $this->contractorID = $search->contractorID;
               }
			   }
         }
         else {
             // Default query still needs search object serialized
            $this->creDate = date ('d-m-Y');
            $whereClause .= " and r.create_date >= current_date";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            $search->dateType = "equalgreater";
            $search->creDate = $this->creDate;
         }

         $_SESSION['SQL'] = pg_escape_string(serialize($search));
			$sql = "SELECT r.*,a.area_name,cr.crew_name,wells_from_ids(r.well_ids) as well_name,subscopes_from_ids(r.sub_crew_ids,true) as sub_scopes,c.con_name,co.calloff_order_id,el.fault
			from request_estimate r
         LEFT JOIN area a using (area_id)
			LEFT JOIN crew cr using (crew_id)
         LEFT JOIN contractor c on c.contractor_id = r.contractor_id
         LEFT JOIN calloff_order co using(request_estimate_id)
			 LEFT JOIN email_log el on el.email_log_id =
         ( select max(email_log_id) from email_log  ell where r.request_estimate_id  = ell.dkt_id  and receiver_type = 'TCE' )
         $whereClause
         and r.removed is false
         order by est_start_date";

			//echo $sql;
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}


			$content ="<div style=\"width:1772px;margin:auto;\" >\n";
			$content  .= <<<FIN
 			<form name="cost_estimate" method="post" action="cost_estimate.php?action=list" >
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
			 <input type="hidden" name="contractorID" id="contractorID" value="$this->contractorID" />
			 <input type="hidden" name="empID" id="empID" value="$this->empID" />
			 <input type="hidden" name="line_no" id="line_no" value="$this->lineNo" />
			 <input type="hidden" name="div_type" id="div_type" value="$this->divType" />

			<fieldset style="margin-bottom:10px;width:97.60%;"><legend style="margin-left:42%;" >Filter Search - Target Cost Estimates</legend>
<div class="div9" >
FIN;
			$content .= AdminFunctions::contractorSelect($this->contractorID,"");
			$content  .= <<<FIN
</div>
<div class="div5 marg1" ><label for="req_id" >TCE ID<input type="text" name="req_id" id="req_id" value="$this->requestID" /></label></div>
FIN;
         $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->creDate,$this->endDate,$this->hideEndDate);
         $content  .= <<<FIN
<div class="div10 marg1" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div9 marg1" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= "</div></div>\n";
$content .= "<div class=\"div11 marg1\"  ><label  >Work Scope</label>";
$content .= AdminFunctions::crewSelect($this->crewID,"",true);
$content .= "</div>\n";
$content .= "<div class=\"div9 marg1\" id=\"subscopediv\" >";
$content .= AdminFunctions::subScopeSelect($this->crewID,$this->subCrewID,"");
$content .= "</div>\n";

$content .= <<<FIN
<script>
$( document ).ready(function() {
   $(function () {
       var ac = $('#req_id').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=tce',
         params: {con_id:  function() { return $('#contractor').val()} },
         minChars:2
      });
      $("#contractor").change(function() {
         $('#req_id').val('');
         ac.clearCache();
      });
   });
});
$( document ).ready(function() {
	 var line = $('#line_no').val();
	 var typ = $('#div_type').val();
	 if (line.length > 0 ) {
		var reqid = $('#reqline_' + line).val();
		var conid = $('#conid_' + line).val();

		fieldDiv('#show_'+line,line,reqid,conid,typ);

	 } 
});
$.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),'#contractorID'); return false; }
      });
  });
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','single'); });
</script>
FIN;
			$content .= "<div style=\"float:right;width:15%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button  class=\"marg\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div class=\"heading\"  >\n";
			$content .= "<div class=\"time hd\">Cost Req ID</div>\n";
			$content .= "<div class=\"usr hd\">Contractor</div>\n";
			$content .= "<div class=\"usr wd hd padr\">Contractor Estimate</div>\n";
			$content .= "<div class=\"ptype hd\">Work Scope</div>\n";
			$content .= "<div class=\"ptype hd\">Area</div>\n";
			$content .= "<div class=\"email hd\">Well/s</div>\n";
			$content .= "<div class=\"fone hd\" style=\"width:91px;\" >Est Start</div>\n";
			$content .= "<div class=\"fone hd\" style=\"width:92px;\" >Est Compl</div>\n";
			$content .= "<div class=\"cli wider hd\"  >Other Text</div>\n";
			$content .= "<div class=\"cli wider hd\"  >Site Conditions</div>\n";
			$content .= "<div class=\"links hd\" style=\"width:116px;\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			$URL="https://".$_SERVER['HTTP_HOST'];
			$_SESSION['last_request'] = "{$URL}/admin/cost_estimate.php?action=list&redo=redo";
			while (! $rs->EOF ) {
				$emailButTxt = "Email";
            $emailButClass = "toemail";
				$notify = "";
				$approved =  "oran";
				$approveButText = "Approve";
				$val = "";
            $ln= $lineNo % 2 == 0  ? "line1" : "";
            $request_id = $rs->fields['request_estimate_id'];
            //$field_est_id = intval($rs->fields['field_estimate_id']);
            $conID =  intval($rs->fields['contractor_id']);
            $cOOID =  intval($rs->fields['calloff_order_id']);
				$conSig = !empty($rs->fields['contractor_sig']) ? true : false;
				if ($conSig) {
            	$est = floatval($rs->fields['contractor_estimate']);
				}
            if ($conSig ) {
               $data = AdminFunctions::getFieldApproval($request_id,$conID,"cost");
					if (!is_null($data)) {
               	if ( ($data['countall']) == $data['countdate'] ) {
                  	$approved =  "grn";
							$approveButText = "Approved";
               	}
               //if ( strlen($data['contractor_sig_date']) > 5 ) {
                //  $val = "<a href=\"field_estimate.php?action=find&field_id=$field_est_id&con_id=$conID\" title=\"View Contractor Estimate\" class=\"linkbutton $approved\" >Field Est</a><span class=\"est\" >$conEst</span>\n";
               //}
            	}  
            	if ($est > 0 ) {
					   $val = "<div style=\"width:122px;margin:auto;\" >\n";
						$val .= "<button id=\"show_$lineNo\" class=\"linkbutton $approved\" style=\"min-width:55px !important;\" onclick=\"fieldDiv('#show_$lineNo',$lineNo,$request_id,$conID,'cost');\" value=\"closed\" >" . number_format($rs->fields['contractor_estimate']) . "</button>\n"; 

						$val .= "<button id=\"approveall_$request_id\" class=\"linkbutton $approved\" style=\"min-width:55px !important;\" onclick=\"approveAll($(this),'#show_$lineNo',$request_id,$conID,'cost');\" >$approveButText</button>\n";
						$val .= "</div>\n";
					}
				}
				$fault = ! is_null($rs->fields['fault']) ? preg_match('/OK/',$rs->fields['fault']) : -1;    // If emailed  no normal  edit
            if ($fault > 0 ) {
               $emailButTxt = "Emailed";
               $emailButClass = "emailed";
            }
            $area = $rs->fields['area_name'];
            $well = $rs->fields['well_name'];
				$crew = $rs->fields['crew_name'];
				$pad = "SUB SCOPES: ";
            $sub_crew =  $rs->fields['sub_scopes'] ; //$rs->fields['sub_crew_name'];
            $toolTip = !empty($sub_crew) ?  "<span class=\"show-option\" title=\"${pad}${sub_crew}\" >$crew</span>" : $crew;

            $start = AdminFunctions::dbDate($rs->fields['est_start_date']);
            $compl = AdminFunctions::dbDate($rs->fields['est_compl_date']);
            $other = AdminFunctions::cutString($rs->fields['other_text'],150);
            $site = AdminFunctions::cutString($rs->fields['site_conditions'],150);
            $supGroup =  "" ; ##$rs->fields['group_name'];
            $conName = $rs->fields['con_name'];

				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" id=\"linediv_$lineNo\"  >\n";
				$content .= "<div class=\"time $ln highh cntr\"  >$request_id</div><input type=\"hidden\" id=\"reqline_$lineNo\" value=\"$request_id\" />\n";
				$content .= "<div class=\"usr $ln highh\"  >$conName</div><input type=\"hidden\" id=\"conid_$lineNo\" value=\"$conID\" />\n";
				$content .= "<div class=\"usr wd $ln highh cntr padr\"  >$val</div>\n";
				$content .= "<div class=\"ptype $ln highh cntr\"  >$toolTip</div>\n";
				$content .= "<div class=\"ptype $ln highh\"  >$area</div>\n";
				$content .= "<div class=\"email $ln highh\"  >$well</div>\n";
				$content .= "<div class=\"fone $ln highh cntr\"  style=\"width:91px;\"  >$start</div>\n";
				$content .= "<div class=\"fone $ln highh cntr\"  style=\"width:92px;\"  >$compl</div>\n";
				$content .= "<div class=\"cli wider $ln highh\"  >$other</div>\n";
				$content .= "<div class=\"cli wider $ln highh\"  >$site</div>\n";
				if ($this->sessionProfile > 2 ) {
               $content .= "<div class=\"links highh $ln\" style=\"width:116px;\"  >";
					$content .= "<div style=\"margin:auto;width:110px;\" >\n";
					$content .= "<button  class=\"linkbutton $emailButClass\" onclick=\"emailTCE($request_id,$conID,$(this));$(this).prop('disabled','disabled');return false;\">$emailButTxt</button>";
            //   if ($field_est_id != 0  && strlen($data['contractor_signee']) > 0 ) {
                  if ($cOOID == 0 ) {
               		$content .= "<a href=\"cost_estimate.php?action=update&request_id=$request_id\" title=\"Edit Request\" class=\"linkbutton\" style=\"min-width:39px !important;\"  >Edit</a>\n";
							//if ($emailButTxt != "Emailed" ) {
               			//$content .= "<a href=\"cost_estimate.php?action=delete&request_id=$request_id\" title=\"Delete Estimate\" class=\"linkbutton bckred\"  >Delete</a>\n";
							//}
                     if ($approved == "grn" ) {  // need to have approved Field Estimate to issue C.O.O.
                        $content .= "<a href=\"calloff.php?action=new&request_id=$request_id\" title=\"Issue C.O.O.\" class=\"linkbutton bckred\" style=\"min-width:42px !important;\" >Issue COO</a>\n";
                     }
                  }
                  else {
               		$content .= "<a href=\"cost_estimate.php?action=find&request_id=$request_id\" title=\"View Request\" class=\"linkbutton\" style=\"min-width:39px !important;\"  >View</a>\n";
                   	$content .= "<a href=\"calloff.php?action=find&calloff_id=$cOOID\" title=\"View C.O.O.\" class=\"linkbutton\" style=\"padding:0;min-width:44px !important;\"  >C.O.O.</a>\n";
                  }
             //  }
               $content .= "</div>\n";
            }
            else {
               $content .= "<div class=\"links highh $ln\" style=\"width:108px;\"  > &nbsp;";
            }

				$content .= "</div></div>";
				$content .= "<div id=\"field_div_$lineNo\" style=\"display:none;\" ></div>\n";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}
		private function getEstimateDetails($requestID) {
			$sql = "SELECT r.*,a.area_name, wells_from_ids(r.well_ids) as well_name,el.fault from request_estimate r
         LEFT JOIN area a using (area_id)
			 LEFT JOIN email_log el on el.email_log_id =
         ( select max(email_log_id) from email_log  ell where r.request_estimate_id  = ell.dkt_id  and receiver_type = 'TCE' )
         where request_estimate_id = $requestID";

         if (! $data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }
         $this->contractorID = $data['contractor_id'];
         $this->areaID = $data['area_id'];
         $this->crewID = $data['crew_id'];
			$this->subCrewIDs = $data['sub_crew_ids'];
         $this->areaName = $data['area_name'];
         //$this->otherChecked = $data['other'] == "t" ? "CHECKED" : "";
         $this->tceTypeID = $data['type_id'];
         $this->otherText = $data['other_text'];
         $this->scope = $data['scope_works'];
         $this->siteConditions = $data['site_conditions'];
         $this->returnGravel = intval($data['gravel_return']) > 0 ? $data['gravel_return'] : NULL;
         $this->returnWater = intval($data['water_return']) > 0 ? $data['water_return'] : NULL;
         $this->estStart = AdminFunctions::dbDate($data['est_start_date']);
         $this->estComplete = AdminFunctions::dbDate($data['est_compl_date']);
         $this->sigDate = strlen($data['qgc_approve_date']) > 5 ? AdminFunctions::dbDate($data['qgc_approve_date']) : date('d-m-Y');
         $this->supIDs = $data['email_emp_ids'];
         $this->check = $data['work_check'];
         $this->wellStr = $data['well_name'];
         $this->wellIDs = $data['well_ids'];
         $this->signature = $data['qgc_sig'];
         $this->sigName = $data['qgc_signee'];
         $this->landOwner_1 = $data['landowner_id_1'];
         $this->landOwner_2 = $data['landowner_id_2'];
         $this->loc_1 = $data['gravel_loc_id_1'];
         $this->loc_2 = $data['water_loc_id_2'];
         if (strlen($this->signature) < 5 ) {
            $this->signature = AdminFunctions::getSig($this->empID);
         }
			$this->notifyChecked = $data['fault'] == "OK" ? "checked" : "";

      }
		 private function processPost() {
         if ($this->action == "delete" ) {
				return false;
            $this->deleteEstimate();
         }
         $this->requestID = intval($_POST['requestID']);
         $this->contractorID = intval($_POST['contractor']);
         $well = $_POST['well'];
         $this->wellID = count($well) == 1 ? intval($well[0]) : "NULL";  // option for one well
         $this->wellStr = implode("|",$well);

         $this->areaID = intval($_POST['areaID']);
			$this->crewID = intval($_POST['crew']);
         $this->subCrewIDs = isset($_POST['sub_scope']) ? $_POST['sub_scope'] : NULL;  // option for one well
         $this->subCrewStr = !is_null($this->subCrewIDs) ? implode("|",$this->subCrewIDs) : "";
         $this->empID = intval($_POST['empID']);
         $this->tceTypeID = $_POST['tce_type'];
         $this->scope = substr(trim(pg_escape_string($_POST['scope'])),0,1020);
         $this->supIDs = isset($_POST['supervisor']) ? implode("|",$_POST['supervisor']) : "";
         $this->otherChecked = isset($_POST['other']) ? "8" : NULL;
         $this->check = "";
         for ($x = 1;$x<=6;$x++) {
            $chk = "check_$x";
            if (isset($_POST[$chk])) {
               $this->check .= "$x|";
            }
         }
         if (! is_null($this->otherChecked)) {
            $this->check .= "8|";
         }
         $this->notifyChecked = isset($_POST['notify']) ? $_POST['notify'] : NULL;
         $this->check = preg_replace('/\|$/',"",$this->check);
         $this->otherText = !empty($_POST['othertext']) ? substr(trim(pg_escape_string($_POST['othertext'])),0,1020) :"";
         $this->siteConditions = substr(trim(pg_escape_string($_POST['sitecond'])),0,1020);
         $this->returnGravel = intval($_POST['returngravel']);
         $this->returnWater = intval($_POST['returnwater']);
         $this->estStart = strlen($_POST['start']) > 2 ? "'" . $_POST['start'] . "'" : "NULL";
         $this->estComplete = strlen($_POST['complete']) > 2 ? "'" . $_POST['complete'] . "'" : "NULL";
         $this->sigDate = isset($_POST['sig_date']) && strlen($_POST['sig_date']) > 2 ? "'" . $_POST['sig_date'] . "'" : "current_date";
         $this->signature=  "'" .$_POST['signature'] ."'";
         $empName = trim(pg_escape_string($_POST['signee_qgc']));
         $this->empName = strlen($empName) > 2 ? "E'" . $empName . "'" : "NULL";
         $this->landOwner_1 = isset($_REQUEST['landowner_1']) && intval($_REQUEST['landowner_1']) > 0 ? intval($_REQUEST['landowner_1']) : "NULL";
         $this->landOwner_2 = isset($_REQUEST['landowner_2']) && intval($_REQUEST['landowner_2']) > 0 ? intval($_REQUEST['landowner_2']) : "NULL";
         $this->loc_1 = isset($_REQUEST['location_1']) && intval($_REQUEST['location_1']) > 0 ? intval($_REQUEST['location_1']) :  "NULL";
         $this->loc_2 = isset($_REQUEST['location_2']) && intval($_REQUEST['location_2']) > 0 ? intval($_REQUEST['location_2']) :  "NULL";

         $this->submitForm();
      }


		private function deleteEstimate() {
         $sql = "UPDATE request_estimate set removed = TRUE where request_estimate_id = $this->requestID";
         if (! $this->conn->Execute($sql)) {
            die($this->conn->ErrorMsg());
         }
         header("LOCATION: $this->URL/admin/cost_estimate.php?action=list&redo=redo");
         exit;
      }
      private function submitForm() {
			global $BASE;
         if ($this->requestID == 0 ) {
            $sql = "INSERT into request_estimate values (nextval('request_estimate_request_estimate_id_seq'),$this->contractorID,$this->areaID,$this->wellID,NULL,$this->tceTypeID,NULL,
            $this->empID,'$this->check',E'$this->otherText',E'$this->scope',E'$this->siteConditions',
            $this->returnGravel,$this->returnWater,$this->estStart,$this->estComplete,current_date,false,$this->signature,$this->sigDate,NULL,NULL,NULL,'$this->wellStr',NULL,$this->empName,
            $this->landOwner_1, $this->loc_1, $this->landOwner_2, $this->loc_2,'$this->supIDs',$this->crewID,'$this->subCrewStr' ) returning request_estimate_id";

            if (! $rs =  $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }
            $this->requestID = $rs->fields['request_estimate_id'];
				if (!is_null( $this->notifyChecked )) {
            	$_SESSION['last_request'] = "{$this->URL}/admin/cost_estimate.php?action=find&request_id=$this->requestID";
            	$eNum = AdminFunctions::insertEmailLog($this->empID,0,$this->contractorID,'Request for Target Cost Estimate','TCE',$this->requestID,date('d-m-Y'),NULL);
					exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
				}
         }
         else { // Update
            $sql = "UPDATE request_estimate set contractor_id = $this->contractorID,area_id = $this->areaID,well_id = $this->wellID,work_check = '$this->check',type_id = $this->tceTypeID,
            scope_works = E'$this->scope',other_text = E'$this->otherText',site_conditions = E'$this->siteConditions', landowner_id_1 = $this->landOwner_1, landowner_id_2 = $this->landOwner_2, 
				gravel_loc_id_1 = $this->loc_1, water_loc_id_2 = $this->loc_2 , gravel_return = $this->returnGravel,water_return= $this->returnWater,est_start_date = $this->estStart, 
				est_compl_date= $this->estComplete, email_emp_ids = '$this->supIDs',qgc_sig = $this->signature,qgc_approve_date = $this->sigDate,well_ids = '$this->wellStr',qgc_signee = $this->empName,
				crew_id = $this->crewID,sub_crew_ids = '$this->subCrewStr'  where request_estimate_id = $this->requestID";
            //echo $sql;
           // exit;
            if (! $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }

            // special COO  update
            $sql = "Update calloff_order set email_emp_ids = '$this->supIDs' where request_estimate_id = $this->requestID";
            if (! $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }

         }
         //echo $sql;
         //exit;

         header("LOCATION: $this->URL/admin/cost_estimate.php?action=find&request_id=$this->requestID");
         exit;

      }



	}
?>
