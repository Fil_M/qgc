<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Contractor extends Address {
		public $page;
		public $ID;
		public $name;
		public $userName;
		public $password;
		public $profileID;
		public $address_type;
		public $action;
		public $sessionProfile;
		public $type;
		public $crews;
		public $conname;
		public $expPercent;
		public $contract;
		public $partialChecked;

		public function	__construct($action,$ID=0,$search="") {
			parent::__construct();
			$this->ID = $ID;
			$this->action = $action;
			$this->address_type = "con";
			$this->type = "Sub Contractor";
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('contractor');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New $this->type";
						break;
					case "update" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text = "Updating $this->type $ID";
						$this->getContractorDetails($ID);
						break;
					case "delete" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text = "Disable $this->type $ID";
						$this->getContractorDetails($ID);
						break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List {$this->type}s - $search" : "List {$this->type}s - All";
						$this->getAllContractors($search) ;
						break;
					case "find" : 
						$heading_text = "$this->type $ID";
						$this->getContractorDetails($ID);
						break;
					default:
						$heading_text = "Add New $this->type";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->ID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add $this->type";
						break;
					case "update":
						$button = "Update $this->type";
						break;
					case "delete":
						$button = "Disable $this->type";
						$DISABLED='disabled';
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add type";
						break;
				}
			$content = <<<FIN
<div style="width:80%;margin:auto;">
<form name="conform" id="conform" method="post" action="contractor.php?SUBMIT=SUBMIT">
<input type="hidden" name="id" value="$this->ID" />
<input type="hidden" name="prev_password" value="$this->password" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="type" value="$this->type" />
<fieldset ><legend style="margin-left:45.5%;">$this->type</legend>
<div class="div32" > <label for="firstname"  >Name<input type="text" name="conname" id="conname"  value="$this->conname" $DISABLED /></label></div>
<div class="div10 marg2" ><label  >Expense Percentage<input type="text" name="expense_perc" id="expense_perc"  value="$this->expPercent" $DISABLED /></label></div>
<div class="div20 marg2" ><label  >Contract<input type="text" name="contract" id="contract"  value="$this->contract" $DISABLED /></label></div>
<div class="div15 marg2" ><label  >Partial Completions</label><input type="checkbox" name="partial" id="partial"  value="partial" $this->partialChecked $DISABLED /></div>
<input type="hidden" name="username" id="username"  value="$this->userName" />
<input type="hidden" name="password" id="password" value="" />
FIN;
			$content .= "</fieldset>\n";
			$content .= $this->addressDiv($DISABLED); 
			$content .= <<<FIN
<script>
jQuery('#expense_perc').keyup(function () { this.value = this.value.replace(/[^0-9\.]/g,''); });
jQuery('.t3rate').keyup(function () { this.value = this.value.replace(/[^0-9\.]/g,''); });
$("#conform").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
</script>
FIN;
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\"   />"; // no submit for find contractor 
				}
				$content .= "<a href=\"contractor.php?action=list\"  class=\"button marg2\" >Last Screen</a>\n"; 


				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllContractors($search="") {
			$whereClause="";
			if (strlen($search) > 0 ) {
				$whereClause = " and upper(name) like '$search%' ";
			}
				$sql = "SELECT c.contractor_id,c.con_name, c.username,c.contract_no,partial_complete,
				ad.address_line_1 || ' ' || ad.address_line_2 || ' ' || ad.suburb || ' ' || st.state_name as address,telephone,mobile,email
				from contractor c 
				LEFT JOIN address_to_relation ar using (contractor_id) 
				LEFT JOIN address ad using (address_id) 
				LEFT JOIN state st using (state_id)
				where c.removed='f' $whereClause order by c.name LIMIT 100";
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content = "<div style=\"width:1435px;margin:auto;\">\n";
			$content .= AdminFunctions::AtoZ("contractor.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:20px;\"></div>\n";
			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"crew hd\">Contractor Name</div>\n";
			$content .= "<div class=\"crew hd\">Contract Number</div>\n";
			$content .= "<div class=\"usr hd\">Partial Completion</div>\n";
			$content .= "<div class=\"addr hd\">Address</div>\n";
			$content .= "<div class=\"fone hd\">Telephone</div>\n<div class=\"fone hd\">Mobile</div>\n";
			$content .= "<div class=\"workscope hd\">Email</div>\n<div class=\"links hd\" style=\"width:110px;\" >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln = $lineNo % 2 == 0  ? "line1" : "";
				$id = $rs->fields['contractor_id'];
				$name = strlen($rs->fields['con_name']) > 2 ? $rs->fields['con_name'] : " ";
				$contract = strlen($rs->fields['contract_no']) > 2 ? $rs->fields['contract_no'] : " ";
				$address = strlen($rs->fields['address']) > 2 ? $rs->fields['address'] : " ";
				$telephone = strlen($rs->fields['telephone']) > 2 ? $rs->fields['telephone'] : " ";
				$mobile = strlen($rs->fields['mobile']) > 2 ? $rs->fields['mobile'] : " ";
				$email = strlen($rs->fields['email']) > 2 ? $rs->fields['email'] : " ";
				$partial = $rs->fields['partial_complete'] == "t" ? "Yes" : "No";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"crew $ln highh\"> <a href=\"contractor.php?action=find&id=$id\" title=\"View Contractor\" class=\"name $ln highh\" >$name</a></div>";
				$content .= "<div class=\"crew $ln highh\">$contract</div>\n";
				$content .= "<div class=\"usr $ln highh cntr\">$partial</div>\n";
            $content .= "<div class=\"addr $ln highh\">$address</div><div class=\"fone $ln highh\">$telephone</div><div class=\"fone $ln highh\">$mobile</div>";
				$content .= "<div class=\"workscope $ln highh\">$email</div>";
				if ($this->sessionProfile > 2 ) {
				$content .= "<div class=\"links $ln highh\" style=\"width:110px;\" ><a href=\"contractor.php?action=update&id=$id\" title=\"Edit Contractor\" class=\"linkbutton\" >EDIT</a><a href=\"contractor.php?action=delete&id=$id\" title=\"Disable Contractor\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DISABLE</a></div>\n";
				}
				else {
					$content .="<div class=\"links $ln highh\" style=\"width:110px;\" > &nbsp;</div>";
				}
				$content .= "</div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getContractorDetails($ID) {
				$sql = "SELECT c.*,adr.address_id from contractor c LEFT JOIN address_to_relation adr using (contractor_id)  where contractor_id = $ID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->ID = $data['contractor_id'];
			$this->conname = $data['con_name'];
			$this->userName =   $data['username'];
			//$this->password = $data['password'];
			$this->addressID = intval($data['address_id']);
			$this->profileID = intval($data['profile_id']);
			$this->expPercent = $data['expense_percentage']; 
			$this->contract = $data['contract_no'];
			$this->partialChecked = $data['partial_complete'] == "t" ? "checked" : "" ;


			$this->getAddressDetails($this->addressID,0);
		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteContractor();
			}
			$this->ProcessAddressPost();
			$this->ID = $_POST['id'];
			$this->conname = trim($_POST['conname']);
			$this->contract = trim($_POST['contract']);
			$this->userName =  preg_replace('/\ /',"_",$this->conname);;
		 	//$this->password =  strlen(trim($_POST['password'])) > 1  ? md5(trim($_POST['password'])) : $_POST['prev_password']; // use previous if not given
			$this->expPercent = floatval($_POST['expense_perc']) > 0 ? $_POST['expense_perc'] : "NULL"; 
		 	$this->profileID =  0;
			$this->partialChecked = !empty($_POST['partial']) ? "TRUE" : "FALSE";

			$this->submitForm();
		}
		private function deleteContractor() {
			$sql = "UPDATE contractor set removed = TRUE where contractor_id = $this->ID";
			if (! $this->conn->Execute($sql)) {
				die( $this->conn->ErrorMsg());
			}
			header("LOCATION: contractor.php?type=$type&action=list");
			exit;
		}
		private function submitForm() {
		 	$dblink = $db = $domain = uniqid(true);	
			$prevAddressID = $this->addressID;  // If zero always make address_to_relation record
			$this->submitAddress();
			if ($this->ID == 0 ) {
					header("LOCATION: index.php");
					exit;
					$sql = "INSERT into contractor values (nextval('contractor_contractor_id_seq'),'$this->userName',NULL,FALSE,
					$this->profileID,'$dblink','$db','$domain','$this->conname',$this->expPercent,NULL,'$this->contract',NULL,$this->partialChecked) returning contractor_id";

				try {
					if (! $rs =  $this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
				$this->ID = $rs->fields['contractor_id'];
			}
			else { // Update
					$sql = "UPDATE contractor set username = '$this->userName',con_name= '$this->conname',
					profile_id=$this->profileID,expense_percentage = $this->expPercent,contract_no = '$this->contract',partial_complete = $this->partialChecked  where contractor_id = $this->ID";
				try {
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						AdminFunctions::showError("This user exists already");
						header("LOCATION: contractor.php?action=new");
					}
					else {
    					die($e->getMessage());
					}
				}
			}
			if ($prevAddressID == 0 ) {  //Could be update
				$sql = "INSERT into address_to_relation (contractor_id,address_type,address_id ) values ($this->ID,'".$this->address_type ."',$this->addressID)";
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}

			header("LOCATION: contractor.php?action=list");
		}
	}
?>
