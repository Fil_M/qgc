<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class ContractorPlant  {
		public $page;
		public $plantID;
		public $plantName;
		public $unit;
		public $action;
		public $sessionProfile;
		public $conn;
		public $rate;
		public $effectiveFrom;
		public $newRate;
		public $standRate;
		public $standNewRate;
		public $plantType;
		public $plantTypeID;
		public $contractorID;
		public $approvedStyle;
		public $approved;
		public $URL;
		public $empID;
		public $conName;
		public $dbLink;
      public $conDomain;
      public $shortName;
      public $contractorName;
		public $status;


		public function	__construct($action,$plantID=NULL,$conID=NULL,$search=NULL,$redo=NULL) {
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->plantID = $plantID;
			$this->URL = "https://". $_SERVER['HTTP_HOST'];
			$this->empID = intval($_SESSION['employee_id']);
			$this->contractorID = $conID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$pg = new Page('plant');
			$this->page= $pg->page;
			$conText = "";
			if (! is_null($conID)) {
               $connArr = AdminFunctions::getConName($conID);
               $this->contractorName = $connArr['con_name'];
               $this->shortName = $connArr['name'];
               $this->conDomain = $connArr['domain'];
               $this->dbLink = $connArr['db_link'];
					$conText = " - $this->contractorName";
         }
        	switch($action) {
				case "list" :
					$heading_text =   "List Contractor Plant $conText";
					$this->getAllPlants($search,$redo) ;
					break;
				default:
					$heading_text = "List Contractor Plant";
					break;
			}
		 	$this->setHeaderText($heading_text);	
			echo $this->page;
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function getAllPlants($search,$redo) {
			if (!empty($this->plantID)) {
               $whereClause = " WHERE plant_id = $this->plantID ";
           	 	$search = new FieldSearch();
					$search->plantID = $this->plantID;
            	$_SESSION['SQL'] = pg_escape_string(serialize($search));
			}
			else {
				$whereClause=" WHERE p.removed is false ";
				if (! is_null($redo)) {
            	if (isset($_SESSION['SQL'])) {
               	$search = unserialize($_SESSION['SQL']);
            	}
            	else {
               	$search=NULL;
            	}
         	}
				if (! is_null($search) && $search) {
            	$_SESSION['SQL'] = pg_escape_string(serialize($search));
					if (!empty($search->plantID)) {  // run out of IDs
               	$whereClause = " WHERE plant_id = $search->plantID ";
						$this->plantID = $search->plantID;

					}
					else if ( !empty($search->status)) {
						$this->status = $search->status;
						if ($this->status == 1 ) {
               		$whereClause .= " and approved is false";
						}
			   	}
					if (! empty($search->pTypeName) ) {
						$this->plantType = $search->pTypeName;
						$this->plantTypeID = $search->ID;
               	$whereClause .= " and p.plant_type_id = $this->plantTypeID ";

					}
         	}
         	else {
             // Default query still needs search object serialized
           	 	$search = new FieldSearch();
            	$_SESSION['SQL'] = pg_escape_string(serialize($search));
         	}
			}
				
			if(! is_null($this->contractorID)) {
				$sql = "SELECT p.plant_id,p.plant_name,p.plant_unit,approved,pt.* from {$this->shortName}_plant p 
				LEFT JOIN plant_type pt using (plant_type_id) $whereClause  and pt.price_type != 'OPEN' order by plant_name";

				if (! $rs=$this->conn->Execute($sql)) {
					die($this->ErrorMsg());
				}
				$content ="<div style=\"width:1394px;margin:auto;\">";
				$content .= <<<FIN
            <form name="field" method="post" action="contractor_plant.php?action=list">
				<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
         	<input type="hidden" name="con_name" id="con_name" value="$this->contractorName" />
         	<input type="hidden" name="plantTypeID" id="plantTypeID" value="$this->plantTypeID" />
         	<input type="hidden" name="plant_id" id="plant_id" value="$this->plantID" />
				<fieldset style="padding:0.5rem 0 0.5rem 0;" ><legend style="margin-left:45%;">Filter Search - $this->contractorName Plant</legend>
				<div class="div25 marg" ><label >QGC Plant Type<input type="text" name="plant_type" id="plant_type"  value="$this->plantType" /></label></div>
				<div class="div15 marg" >
FIN;
				$content .= AdminFunctions::conPlantStatus($this->status);
				$content .= <<<FIN
				</div>
				<div style="float:right;width:22%;" >
				<input type="submit" name="search" value="Search"  class="button" />
				<button class="marg10" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
				</fieldset>
				</form>
<script>
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function () {
      $('#plant_type').autocomplete({
         width: 700,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=planttype',
         minChars:1,
         params: {con_id: $('#contractorID').val()},
         onSelect: function(value, data){ $('#plantTypeID').val(data); }
      });
  });
</script>

FIN;


				$content .= "<div style=\"clear:both;height:20px;\"></div>\n";
				$content .= "<div class=\"heading\" ><div class=\"email hd\">Contractor Plant Name</div>\n";
				$content .= "<div class=\"workscope hd\" >Plant Type</div><div class=\"crew hd\">Contractor Unit</div>\n";
				$content .= "<div class=\"rate hd\">U.O.M.</div>";
				$content .= "<div class=\"rate hd padr\">Rate</div>";
				$content .= "<div class=\"rate hd  padr\">New Rate</div><div class=\"fone hd\">Effective</div>";
				$content .= "<div class=\"rate hd  padr\"> S Rate</div>";
				$content .= "<div class=\"time hd  padr\"> S New Rate</div>";
				$content .= "<div class=\"links hd\" style=\"width:100px;\"  >Approve</div></div>\n";
				$lineNo = 0;
				while (! $rs->EOF ) {
					$ln= $lineNo % 2 == 0  ? "line1" : "line2";
					$plant_id = $rs->fields['plant_id'];
					$plant_name = strlen($rs->fields['plant_name']) > 1 ? $rs->fields['plant_name'] : " ";
					$unit = strlen($rs->fields['plant_unit']) > 0 ? $rs->fields['plant_unit'] : " ";
					$rate =  sprintf('%01.2f',$rs->fields['plant_rate']);
					$newrate = floatval($rs->fields['new_rate']) > 0 ? sprintf('%01.2f',$rs->fields['new_rate']) : " ";
					$effective = strlen($rs->fields['effective_from']) > 0 ? AdminFunctions::dbdate($rs->fields['effective_from']) : " ";
					$stand_rate =   floatval($rs->fields['stand_rate']) > 0 ? sprintf('%01.2f',$rs->fields['stand_rate']) :NULL;
					$stand_newrate = floatval($rs->fields['stand_new_rate']) > 0 ? sprintf('%01.2f',$rs->fields['stand_new_rate']) : " ";
					$uom = $rs->fields['unit_of_measure']; 
					$checked = $rs->fields['approved'] == "t" ? "checked" : "";
					$approved = "<input style=\"margin-left:2.6rem;\" type=\"checkbox\" $checked onclick=\"approve_plant($plant_id,$this->contractorID,$(this));return false;\" />\n";
					$pType = $rs->fields['p_type'];
					$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
					$content .= "<div class=\"email $ln highh\">$plant_name</div>\n";
            	$content .= "<div class=\"workscope  $ln highh\" >$pType</div><div class=\"crew $ln highh\">$unit</div>\n";
					$content .= "<div class=\"rate $ln cntr highh\">$uom</div>";
					$content .= "<div class=\"rate $ln highh txtr\">$rate</div>";
					$content .= "<div class=\"rate $ln highh txtr\">$newrate</div><div class=\"fone $ln cntr highh\">$effective</div>\n";
					$content .= "<div class=\"rate $ln highh txtr\">$stand_rate</div>";
					$content .= "<div class=\"time $ln highh txtr\">$stand_newrate</div>\n";
					if ($this->sessionProfile > 2 ) {
           			$content .= "<div class=\"links $ln highh\"   style=\"width:100px;\"   >$approved\n";
					}
					else {
					     $content .= "<div class=\"links $ln highh\"   style=\"width:100px;\"  > &nbsp;\n";
					}
					$content .= "</div></div><div style=\"clear:both;\"></div>";
					$lineNo += 1;
					$rs->MoveNext();
				}
				$content .= "<hr /></div>";
			}
			else { // ContractorID NULL show contrcator select
            $content = AdminFunctions::setContractor("contractor_plant.php?action=list",$this->contractorID);
         }
			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

	}
?>
