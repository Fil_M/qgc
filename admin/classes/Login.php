<?php

 	class Login {
		public $conn;
		public $page;
		public $pageType='login';

		public function	__construct() {
			$this->conn = $GLOBALS['conn'];
			$pg = new Page('login');
			$this->page= $pg->page;
		 	$this->setHeaderText();	
		 	$this->setContent();	
         echo $this->page;
		}

		private function setHeaderText() {
			$this->page = str_replace('##HEADER_TEXT##','Admin Login',$this->page);
		}

		private function setContent() {
			$error = '';
			if (isset($_SESSION['locked']) && $_SESSION['locked']  === 'true' ) {
				$_SESSION['locked'] = 'false';
            $error .= '<div ><span class="error cntr" >This account shown as Locked</span></div>';
			}
			else if($_SESSION['user_unknown'] === 'true'){
            $_SESSION['user_unknown'] = 'false';
            $error .=  '<div ><span class="error cntr" >Username unknown. Please try again</span></div>';
         }
         else if ($_SESSION['pwd_unknown'] === 'true') {
            $_SESSION['pwd_unknown'] = 'false';
            $error .= '<div ><span  class="error cntr" >Password Incorrect. Please try again</span></div>';
         }

			$content = "\t<div id=\"main_login\">
       <form name=\"input\" style=\"padding:1rem 1.2rem 0 1.2rem;\" action=\"login.php\" method=\"post\">
			$error
       <label for=\"name\">Username: <input type=\"text\" name=\"user\"  /></label>
       <label for=\"pwd\">Password:<input type=\"password\" name=\"pwd\"  /></label>
       <div id=\"login_submit\"><input  type=\"submit\" value=\"Log In\" class=\"button\" style=\"margin-left:9%;\"  onClick=\"$('#main_login').slideUp(250);\" /></div>
       </form>
     </div>";

			$this->page = str_replace('##MAIN##',$content,$this->page);
			$this->page = str_replace('##CURRENT##',"Log In",$this->page);
		}
	}
?>
