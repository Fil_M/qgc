<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Invoice  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $areaName;
		public $areaID;
		public $wellID;
		public $callOffID;
		public $equalto;
      public $less;
      public $creDate;
      public $between;
      public $hideEndDate=false;
      public $equalgreater;


		public function	__construct($action,$startDate=NULL,$endDate=NULL,$cooID=NULL,$conID=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= !is_null($startDate)  ?  $startDate : AdminFunctions::beginLastMonth();
			$this->endDate= ! is_null($endDate)  ?  $endDate  :  AdminFunctions::endLastMonth();
			$this->between = "SELECTED=\"selected\"";
			$this->callOffID = $cooID;
		 	$this->contractorID=intval($conID);
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			elseif (isset($_POST['PRINT'])) {
				 $wellStr = $areaStr = "";
             $areaStr = isset($_POST['area_id']) && intval($_POST['area_id']) > 0 ? "&area=".$_POST['area_id'] : "";
             $well = isset($_POST['well']) ? $_POST['well'] : NULL;
             if (! is_null($well)) {
               if (is_array($well)) {
                  $wellStr =  "&well=".$well[0];
               }
               else {
                  $wellStr =  "";
               }
             }
				 $URL="https://".$_SERVER['HTTP_HOST'] ."/printInvoicepdf.php?action=print&startdate=$this->startDate&enddate=$this->endDate&coo_id=$this->callOffID&con_id=$this->contractorID{$areaStr}{$wellStr}";
             //die($URL);
				 header("LOCATION: $URL");
				 exit;
			}
			else {
				$pg = new Page('Invoice');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Print Work Record PDF's";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print PDF's\" class=\"button\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {

			 $content = <<<FIN
<div style="width:1650px;margin:auto;">
<form name="invoice" id="invoice" method="post" action="invoice.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="area_id" id="areaID"  />
<fieldset ><legend style="margin-left:44%;">Filter Search - Work Records</legend>
<div class="div15">
FIN;
    $content .= AdminFunctions::contractorSelect($this->contractorID,"",false);
$content .= <<<FIN
</div>
<div class="div8 marg2" > <label for="coo" >C.O.O.<input type="text" name="coo_id" id="coo_id"  value="$this->callOffID" /></label></div>
FIN;
   $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->startDate,$this->endDate,$this->hideEndDate);


$content .= <<<FIN
<div class="div14 marg2" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div18 marg2" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<div style="float:right;width:8%;" >$button</div>
</fieldset>
</form></div>
<script>
$("#invoice").submit(function(){
    var isFormValid = true;
    $("#invoice .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
$( document ).ready(function() {
   $(function () {
       var ac = $('#coo_id').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=calloffs',
         params: {con_id:  function() { return $('#contractor').val()} },
         minChars:2
      });
      $("#contractor").change(function() {
         $('#coo_id').val('');
         ac.clearCache();
      });
   });
});
$(function(){
	 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),$('#contractor')); return false; }
      });
  });
$(function () {
      $('#coo').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=coo',
         minChars:3,
         onSelect: function(value, data){ $('#contractorID').val(data[0]); $('#contractor').val(data[0]);$('#area').val(data[1]);wellDiv(data[2],$('#welldiv')); return false; }
      });
  });
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
 }
?>
