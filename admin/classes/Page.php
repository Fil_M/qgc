<?php
 	class Page {
		public $page;
		public $pageType;
		public $logo;
		public $clientID;
      public $employeeID;
		public $empName;
      public $vehicleID;
		public $extraMeta="";
		public $extraCSS="";
		public $extraJS="";
		public $companyName="";


		public function	__construct($pageType) {
			$this->pageType = $pageType;
			$this->employeeID = isset($_SESSION['employee_id']) ? $_SESSION['employee_id'] : 0;
         $this->empName = isset($_SESSION['firstname']) ? $_SESSION['firstname'] . " " . $_SESSION['lastname'] : "";
 			$this->extraCSS="<link rel=\"stylesheet\" type=\"text/css\" href=\"css/$pageType.css\" /> ";
			$this->extraJS="<script type='text/javascript' src=\"js/$pageType.js\"></script>";
			$this->setHead();
			$this->setHeader();
			$this->setMenu();
				//$this->setContent();
			$this->setFooter();
		}


		private function setHead() {
			$arr = Functions::getLogo();
			$this->companyName = $arr['title'];
			//$this->logo = $arr['logo'];
			$head = <<<FIN
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>$this->companyName Administration</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="robots" content="noindex,nofollow" /> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
$this->extraMeta
$this->extraCSS
<link rel="stylesheet" type="text/css" href="css/mobiscroll.custom-2.5.2.min.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/foundation.css" />
<link rel="stylesheet" type="text/css" href="css/admin.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/themes/black-tie/jquery-ui.min.css" />
<link rel="stylesheet" media="all" type="text/css" href="css/themes/black-tie/theme.css" />
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-ui.min.js'></script>
<script type='text/javascript' src='js/jquery.autocomplete.js'></script>
<script type='text/javascript' src='js/jquery-ui-timepicker-addon.js'></script>
<script type='text/javascript' src="js/menu.js"></script>
<script type='text/javascript' src='js/admin.js'></script>
$this->extraJS
FIN;
	 		$head .= "\n</head>";
         $this->page=$head;

}
		private function setBlankPage() {
			$content = "\n<body>\n<div class=\"wrap\">\n";
		   $content .= "\n<div id=\"content\">\n<div id=\"main\">\n";
			$content .= "##MAIN##\n</div> <!-- main --></div> <!-- content -->";
			$content .= "\n</body>\n</html>";
			$this->page .= $content;
		}
		private function setHeader() {
			$divWidth = $this->pageType != "login" ?  "style=\"max-width:1782px;\"" : "";  // ???????
			$addr = Functions::getAddress();
			$header = "\n<body>\n";
			$header .= "<div id=\"ttt\"  $divWidth >\n";
			$header .= '<nav class="top-bar docs-bar" data-topbar role="navigation" data-options="is_hover: true">';
			$header .="<div id=\"header_logo\">$this->companyName</div>";
			$menu = new Menu($this->pageType);
		   $header .= $menu->div;
      	$header .= "\n</nav> </div>";
         $header .= "<nav class=\"breadcrumbs\"><a href=\"/admin\">Dashboard</a><a class=\"current\">##CURRENT##</a><div class=\"empname\" >$this->empName</div> </nav><!-- header -->\n";
			$header .= "<div style=\"height:3.3rem;float:left;margin-left:26px;width:1774px;\" id=\"displaydiv\" >\n";
			if (isset($_SESSION['email_message'])) {
              $header .= $_SESSION['email_message'];
              unset ($_SESSION['email_message']);
         }
			else {
			
				//$head = AdminFunctions::getMessage();
				//var_dump($head);
				$header .= AdminFunctions::getMessage();
            unset ($_SESSION['email_message']);
			}
			$header .= "</div><!-- closes height div -->\n";

			$this->page .= $header;
		}

		private function setContent() {

		}
		private function setMenu() {
        $content = "\n<div id=\"content\">\n<div class=\"main\">\n";
         $content .= "##MAIN##\n</div> <!-- main --></div> <!-- content -->";
			$this->page .= $content;
		}
	
		private function setFooter() {
			$footer = "\n<div id=\"footer\"> <div class=\"bold\"> <p>&copy; Copyright - NTD </p> </div></div>\n</body>\n</html>";
			$this->page .= $footer;
		}

	}
?>
