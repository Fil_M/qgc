<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class WellReportAllBatch  {
		public $page;
		public $action;
		public $data = array();
		public $conn;
		public $csv;
		public $employeeID;
		public $startDate;
		public $endDate;
		public $wellArr = array();
		public $connArr;
		public $cooArr = array();
		public $lineCount;
		public $cooTotalsStr = "";
      public $grandTotal = 0;
		public $today;
		public $empEmail;

		public function	__construct($action,$startDate,$endDate,$email=NULL) {
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
			$this->empEmail =  !is_null($email) ? $email : $this->getEmail($this->employeeID);
			$this->startDate = $startDate;
			$this->endDate = $endDate;
			$this->today = date('d-m-Y');
			$pg = new Page('coo');
         $this->page= $pg->page;

			if (isset($_POST['PRINT'])) {
				global $BASE;
				exec("{$BASE}classes/batch/batch_wellreport $this->startDate $this->endDate $this->empEmail $this->employeeID  >/dev/null 2>&1  &");
				$button = "";
         	$heading_text = "Well Report In Progres...";
         	$this->setHeaderText($heading_text);
         	$this->setContent($action,$button);
	
			}
			else {
				$pg = new Page('Coo');
				$this->page= $pg->page;
         	$heading_text = "Well Report $this->startDate - $this->endDate";
         	$this->setHeaderText($heading_text);
				if ($action == "print" ) {
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print\" class=\"button margt4\" />\n";
		 			$this->setContent($action,$button);	
				}
			}
			echo $this->page;
		}
		private function printFile($file) {
			if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file)); //Remove
           	readfile($file);
           	exit;
      	}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1100px;margin:auto;">
<form name="well_all" id="well_all" method="post" action="wellReportAll.php?action=$action">
<fieldset ><legend style="margin-left:37%;">Well Report &nbsp;for &nbsp; $this->startDate - $this->endDate</legend>
<div class="div10" ><label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div class="div10 marg" ><label >End Date:<input type="text" name="enddate" id="edate" class="required date" value="$this->endDate"  /></label></div>
<div class="div50 marg" ><label >Email will be sent to:<input type="text" name="email" id="email" class="required" value="$this->empEmail"  /></label></div>
<div style="float:right;width:15%;" >
$button
</div>
</fieldset>
</form>
</div>
<script>
$(function(){
 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$("#well_all").submit(function(){
   var isFormValid = true;
    $("#cost input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#well_all .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
    else {
      $('#SUBMIT').prop("disabled", "disabled");
      return true;
    }
});
</script>
FIN;
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	private function getData() {
		$tmpArr = array();
		$this->connArr = AdminFunctions:: getAllCon();
		foreach($this->connArr as $ind=>$val)  {
			extract($val);
			$sql = "with t as (select distinct(calloff_order_id) from {$name}_hour h where hour_date between '$this->startDate' and '$this->endDate' and h.removed is false and h.status >= 5 )
			select co.calloff_order_id,well_ids from calloff_order co
			JOIN t using(calloff_order_id)
         order by area_id,calloff_order_id,well_ids ";
			//WHERE area_id in (select area_id from area where upper(area_name) like 'W%')  


      	if (! $data = $this->conn->getAll($sql)) {
         	if ($this->conn->ErrorNo() != 0 ) {
           		die($this->conn->ErrorMsg());
        		}
				else {
					continue;
				}
     		}
			if (count($data) > 0 ) {
				foreach($data as $ind=>$val) {
					extract($val);
					$this->data[] = array('wellids'=>$well_ids,'coos' => $calloff_order_id);
				}

			}
		}
		while (!$this->uniqueWells($this->data)) {
			$this->data = $this->reduceArray($this->data);
		}

	   $this->data = $this->sortData($this->data);

		$this->getCooData();


	}
	private function sortData($data) {
		$sql = "TRUNCATE table tmp_wells";
		if (! $res = $this->conn->Execute($sql)) {
			echo "dies in truncate";
			die($this->conn->ErrorMsg());
		}
		foreach($data as $ind=>$val) {
			extract($val);
			$wellArr	= explode("|",$wellids);
			foreach($wellArr as $wellID) {
				$sql = "SELECT coos_from_well('$wellID','$coos')";
				if (! $callOffs = $this->conn->getOne($sql)) {
					die($this->conn->ErrorMsg());
				}
		   	$sql2 = "INSERT into tmp_wells values ((select area_name from well w JOIN area using(area_id) where well_id = $wellID),$wellID,'$callOffs')";
				if (! $res = $this->conn->Execute($sql2)) {
					die($this->conn->ErrorMsg());
				}
			}
		}

		$sql = "select t.*,w.well_name from tmp_wells t JOIN well w using(well_id) order by area_name,well_name";

		if (! $sortData = $this->conn->getAll($sql)) {
			echo "dies no select from tmp_wells";
			die($this->conn->ErrorMsg());
		}
		return $sortData;
	}
	private function uniqueWells($data) {
		$wellStr = "";
		foreach($data as $ind=>$val) {
			extract($val);
			$wellArr	= explode("|",$wellids);
			foreach($wellArr as $well) {
				if (!stristr($wellStr,$well)) {
					$wellStr .= "$well|";
				}
				else {
					return false;
				}
			}
		}
		return true;
	}

	private function reduceArray($data) { 
		$newArr = array();
		$cnt = count($data);
		for($x=0;$x<$cnt;$x++) {
			for ($y=$x+1;$y<$cnt;$y++) {
				$wells1 = $data[$x]['wellids'];	
				if (!empty($data[$y]['wellids'])) {
					$wells2Arr = explode("|",$data[$y]['wellids']);
					foreach($wells2Arr as $well) {
						if (stristr($wells1,$well)) {
				  			$data[$x]['coos'] .= "|".$data[$y]['coos'];
				   		foreach($wells2Arr as $w) {
								if (!stristr($wells1,$w)) {
									$wells1 .= "|$w";
								}
							}	
							$data[$x]['wellids'] = $wells1;
							$data[$y] = array('wellids'=>"",'coos'=>"");
							break;
						}

					}  //foreach
				} //  not  empty
			}  // y

		}  // x
		foreach($data as $ind=>$val) {
			extract($val);
			if($wellids != "") {
				$newArr[] = array('wellids'=>$wellids,'coos'=>$coos);
			}
		}
		return $newArr;
	}
	
	private function getCooData() {
		$line = 1;
		//var_dump($this->data);
		foreach($this->data as $val) {
			$this->cooTotalsStr ="";
			extract($val);
			//if ($area_name != "Cam") {
			//	continue;
			//}
			fputcsv($this->fp,array("Area","Well"));
			fputcsv($this->fp,array("$area_name","$well_name"));
			$cooArr = explode(",",$coos);
			$wellTot = $hourTot = $wellTotal = $wellDktHours = 0;
			foreach ($cooArr as $coo ) {
				$gTot = $dktTot =  0;
				$sql = "Select contractor_id,name,con_name from calloff_order co
				LEFT JOIN contractor c using(contractor_id)
				where co.calloff_order_id = $coo";
      		if (! $data = $this->conn->getRow($sql)) {
					echo "dies here  $coo ";
          		die($this->conn->ErrorMsg());
     			}
				extract($data);
				$sql = "SELECT h.*,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,pt.p_type,plant_unit,a.area_name,c.crew_name,sub_crew_name
               from {$name}_hour h
               LEFT JOIN {$name}_plant p using (plant_id)
               LEFT JOIN plant_type pt using (plant_type_id)
               LEFT JOIN area a using (area_id)
               LEFT JOIN {$name}_employee e using (employee_id)
					LEFT JOIN crew c  using (crew_id)
               LEFT JOIN sub_crew sc  using (sub_crew_id)
					where calloff_order_id = $coo and hour_date between '$this->startDate' and '$this->endDate' and h.removed is false and  h.status >= 5
               order by hour_date";

				

				if (!$hourData = $this->conn->getAll($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						echo "no data for coo ??? $coo";
						die($this->conn->ErrorMsg());
					}
				}

				$startGroup = $this->lineCount + 1;
				$loopCount = 1;
				foreach($hourData as $ind=>$val) {
					extract($val);
					if ($loopCount == 1 ) {
						$cntArr = explode("|",$well_ids);
						$wellCount = count($cntArr);
						$coo = $calloff_order_id;
						$budgetName = $this->getBudget($coo);
					}
					$hour_date = AdminFunctions::dbDate($hour_date);
					$total_t1 = floatval($total_t1);
            	$total_t2 = floatval($total_t2);
            	$expense = floatval($expense);
					if ($docket_hours_t1 > 0 ) {
						$dktTot += $docket_hours_t1;
						$dktHours =  $docket_hours_t1;
					}
					else {
						$dktTot += $docket_hours_t2;
						$dktHours =  $docket_hours_t2;
					}
            	if ($total_t1 > 0 ) {
					 	$gTot += $total_t1;
                	$total = $total_t1;
            	}
            	else if ($total_t2 > 0 ) {
					 	$gTot += $total_t2;
                	$total = $total_t2;
            	}
            	else if ($expense > 0 ) {
						$gTot += $expense;
                	$total = $expense;
            	}
					$dktHours = $dktHours / $wellCount;
					$wellDktHours += $dktHours;
					$total = round(($total / $wellCount),2);
					$wellTotal += $total;
					$tmpArr = array($con_name,$hour_date,$coo,$budgetName,$docket_num,$opname,$plant_unit,$p_type,$crew_name,$sub_crew_name,$area_name,$well_name,$dktHours,$rate,$total,$wellCount);
					fputcsv($this->fp,$tmpArr,",",'"');
            	$this->lineCount ++;
					$loopCount ++;
				}
			//$this->cooArr[$coo] = array('well_count'=>$wellCount,'gtot'=>$gTot,'dkt_tot'=>$dktTot);
			 	$wellTot = $wellTot + ($gTot / $wellCount);
			 	$hourTot =  $hourTot + ($dktTot / $wellCount);
			 	$this->grandTotal += $gTot;
			//fputcsv($this->fp,array("","","","","","","","","","","","","","",'=ROUND(SUM(L'.$startGroup.':L'.$this->lineCount.'),2)','=ROUND(SUM(N'.$startGroup.':N'.$this->lineCount .'),2)'));
			//$this->lineCount ++;
			//$this->cooTotalsStr .="P$this->lineCount,";

			}  // foreach COO
			fputcsv($this->fp,array());
			fputcsv($this->fp,array("Area","Well","","Well Hours","Well Total","Well Hours ACTUAL","Well Total ACTUAL"));
			$this->lineCount += 2;
			fputcsv($this->fp,array("$area_name","$well_name","",number_format(round($wellDktHours,2),2), number_format(round($wellTotal,2),2), number_format(round($hourTot,2),2),number_format(round($wellTot,2),2)));
			fputcsv($this->fp,array());
	 } // foreach data  line
		//$this->summaryCOO($wellCooArr);
	}
	private function getBudget($coo) {
		$sql = "SELECT budget_name from budget join calloff_order using(budget_id) where calloff_order_id = $coo";

		if (! $budget_name = $this->conn->getOne($sql)) {
			if ( $this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
			else {
				$budget_name = "";

			}
		}
		return $budget_name;
	}
	private function summaryCOO($wellCooArr)  {
		$startWell = $this->lineCount +1;
		foreach($wellCooArr as $wellID=>$val) {
			extract($val);
			$callTotArr = $this->getTotals($coos);
			$tmpArr = array($area_name,$well_name,'',$callTotArr[0],$callTotArr[1]);
			fputcsv($this->fp,$tmpArr);
			$this->lineCount ++;
		}
		$sumCOO = '=SUM('. preg_replace('/,$/',"",$this->cooTotalsStr) . ')';
		fputcsv($this->fp,array("","","","","",'=ROUND(SUM(E'.$startWell.':E'.$this->lineCount.'),2)',$sumCOO));
		$this->lineCount ++;
		fputcsv($this->fp,array());
		fputcsv($this->fp,array());
		$this->lineCount += 2;
	}
	private function getTotals($coos) {  // from cooArr for  this  well
		reset($this->cooArr);
		$dktTot = $gTot = 0;
		$callArr = explode(",",$coos);
		foreach($callArr as $coo ) {
			foreach($this->cooArr as $c=>$v) {
				extract($v);
				if ($c == $coo) {
					$dktTot += ($dkt_tot / $well_count);
					$gTot += ($gtot / $well_count);
				}
			}
		}
		return array(round($dktTot,2),round($gTot,2));
	}
	private function getCoosFromWells($wellStr,$cooLine) {
		$arr = array();
		$wells = explode("|",$wellStr);
		foreach($wells as $well) {
			$sql = "SELECT coos,w.well_name,a.area_name from coos_from_well('$well','$cooLine') as coos
			JOIN well w on w.well_id = $well 
			JOIN area a using(area_id)";	

			if (! $data=$this->conn->getRow($sql)) {
				echo $this->grandTotal;
				echo "dies coos from wells  $well  from $cooLine";
				die($this->conn->ErrorMsg());
			}
			extract($data);
			$arr[$well]= array('coos'=>$coos,'well_name'=>$well_name,'area_name'=>$area_name);
		}
		return($arr);
	}
   private function getEmail($empID) {
		$sql = "select email from  employee e 
		 	LEFT JOIN address_to_relation ar using (employee_id) 
         LEFT JOIN  address ad using (address_id) 
			where e.employee_id = $empID";
			$email = $this->conn->getOne($sql);
		 	return $email;
	}
}
?>
