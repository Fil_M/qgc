<?php    //QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
class Complete  {
		public $page;
		public $compID;
		public $callOffID;
		public $empID;
		public $action;
		public $sessionProfile;
		public $conn;
		public $wellIDs;
		public $wellID;
		public $wellName;
		public $areaID;
		public $crewID;
		public $subCrewID;
		public $subCrewIDs ;
		public $subCrewName ;
		public $statusArr= array("0"=>"#ecefbe","1"=>"#efd38f","2"=>"#cdefbd","3"=>"#ef3e55");
		public $areaName;
		public $crewName;
		public $empName;
		public $equalto;
      public $between;
		public $less;
      public $hideEndDate=true;
		public $equalgreater;
		public $endDate;
		public $contractNum;
		public $compDate;
		public $conSigDate;
		public $comments;
		public $origEst;
		public $table1;
		public $table2;
		public $totExp;
		public $total;
		public $signature;
		public $contractorID = NULL;
      public $conSignature;
      public $conSignName;
		public $multi;
		public $complStatus;
		public $photoLines;
		public $allApproved = "";
		public $qgcSignature;
      public $qgcApprDate;
      public $qgcSignee;
		public $notifyChecked;
		public $gravel;
		public $water;
		public $gravelTot;
		public $waterTot;
		public $creDate;
		public $approvalDate;
		public $totalsHTML;
		public $percComplete;
		public $dbLink;
		public $conDomain;
		public $shortName;
      public $contractorName;
		public $approveText;
		public $URL;


		public function	__construct($action,$compID=0,$conID=NULL,$search=NULL,$redo=NULL,$order=NULL) {
			if (empty($action)) {
				header("LOCATION: login.php");
			}
			$this->URL="https://".$_SERVER['HTTP_HOST'];
			$this->action = $action;
			$this->compID = $compID;
			$this->conn = $GLOBALS['conn'];
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->contractorID = $conID; 
			$this->empID = intval($_SESSION['employee_id']);
			$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			$this->compDate = $this->qgcApprDate = date('d-m-Y');
			set_time_limit(0);
			 if (! is_null($conID)) {
               $connArr = AdminFunctions::getConName($conID);
               $this->contractorName = $connArr['con_name'];
               $this->shortName = $connArr['name'];
               $this->conDomain = $connArr['domain'];
               $this->dbLink = $connArr['db_link'];
         }

			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('completion');
				$this->page= $pg->page;
         	switch($action) {
					case "update" :
						if ($compID  > 0 ) {
							$this->getCompDetails($this->compID);
               		$this->approvalDate = date('d-m-Y');
               		$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
               		$this->signature = AdminFunctions::getSig($this->empID);  // No profile on who can do this ?
						}
						$heading_text = "Updating Completion Certificate $this->compID";
						break;
					case "list" :
						$heading_text =   "List Completion Certificates - $this->contractorName";
						$this->getAllCompletions($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Completion Certificate $this->compID";
						$this->getCompDetails($this->compID);
						break;
					default:
						$heading_text = "List Completion Certificate";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->submitForm();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->requestID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "update":
						$button = "Update Completion";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "No Button";
						break;
				}
//<input type="hidden" name="signature" value="$this->qgcSignature" />
//<input type="hidden" name="con_id" value="$this->contractorID" />
			$content = <<<FIN
<div style="width:1388px;margin:auto;">
<form name="complete" id="complete" method="post" action="complete.php?SUBMIT=SUBMIT"  enctype="multipart/form-data">
<input type="hidden" name="empID" value="$this->empID" />
<input type="hidden" name="comp_id" value="$this->compID" />
<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
<input type="hidden" name="wellIDs" id="wellIDs" value="$this->wellIDs" />
<input type="hidden" name="crewID" id="crewID" value="$this->crewID" />
<input type="hidden" name="subCrewIDs" id="subCrewIDs" value="$this->subCrewIDs" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="signature" id="signature" value="$this->signature" />
<input type="hidden" name="qgc_approved" id="qgc_approved" value="" />
<input type="hidden" name="empName" value="$this->empName" />
<input type="hidden" name="con_id" value="$this->contractorID" />
<fieldset ><legend style="margin-left:44%;">Completion Certificate</legend>
<div class="div9" style="height:11.75rem;" ><div><label> C.O.O.<input name="coo" id="coo" class="required"  value="$this->callOffID" type="text" readonly /></label></div>
<div style="margin-top:3.5rem;" ><label >Completion Date<input type="text" class="date" name="comp_date" value="$this->compDate" disabled /></label></div>
</div>
<div><div id="line_1" class="div90" >
<input type="hidden" id="lineno" value="1" />
<div class="div15 marg" ><label  >Work Scope<input type="text" name="crew" id="crew_1" disabled value="$this->crewName" /></label>
</div><div class="div22 marg"  id="subscopediv_1" ><label>Sub Scopes</label>
<textarea class="txta" name="sub_scopes" id="sub_scopes_1" DISABLED >$this->subCrewName</textarea>
</div>
<div class="div20 marg" ><label for="area" >Area<input type="text" name="area" id="area_1"  value="$this->areaName" DISABLED /></label></div>
<div id="welldiv" class="div22 marg55" ><label  >Well/s</label>
<textarea DISABLED class="txta" id="well_1" >$this->wellName</textarea>
</div>
<div style="clear:both;" > </div>
<div class="div15 marg" ><label for="con_num" >Contract Number<input type="text"  name="con_num" id="con_num" value="$this->contractNum"  READONLY /></label></div>
<div class="div10 marg"  ><label for="gravel" >Gravel m3<input type="text"  name="gravel" id="gravel" value="$this->gravel" class="txtr" style="background:#f5f5f5;"  READONLY /></label></div>
<div class="div10 marg2"  ><label >Gravel Cost<input type="text"  name="gravel_tot" id="gravel_tot" value="$this->gravelTot" class="txtr"  READONLY /></label></div>
<div class="div9 marg" ><label for="water" >Water kL<input type="text"  name="water" id="water" value="$this->water" class="txtr" style="background:#f5f5f5;"  READONLY /></label></div>
<div class="div10 marg1" ><label  >Water Cost<input type="text"  name="water_tot" id="water_tot" value="$this->waterTot" class="txtr"  READONLY /></label></div>
<div class="div10 marg55" ><label for="orig_est" >Original Estimate<input type="text"  name="orig_est" id="orig_est"  class="txtr" value="$this->origEst"   style="background:#f5f5f5;"  READONLY /></label></div>
<div class="div10 marg2" ><label for="orig_est" >Contractor Cost<input type="text"  name="totl" id="totl"  class="required txtr" value="$this->total"   READONLY /></label></div>
</div></div>
<div style="clear:both;" ></div>
<div class="div9" > <label>Percentage Compl<input type="text" disabled value="$this->percComplete"  class="cntr" /></label></div>
<div class="div84 marg4" style="margin-left:4.5%;"  ><label>Comments</label><textarea  name="comments" id="comments" $DISABLED >$this->comments</textarea></div>
<input type="hidden" id="water_tot" name="water_tot" value="$this->waterTot" />
<input type="hidden" id="gravel_tot" name="gravel_tot" value="$this->gravelTot" />
<input type="hidden" id="t1" name="t1" value="$this->table1" />
<input type="hidden" id="t2" name="t2" value="$this->table2" />
<input type="hidden" id="expense" name="expense" value="$this->totExp" />
</fieldset>
<div style="clear:both;" > </div>
<fieldset ><legend style="margin-left:47.75%;">Totals</legend>
<div class="div98" style="min-height:7.3rem;" > <div id="totalsdiv" class="div95" >
$this->totalsHTML
</div></div>
<div style="clear:both;" ></div>
</fieldset>
<fieldset ><legend style="margin-left:46.75%;">Uploads</legend>
<div id="uploads"  class="div98" >
<div class="heading" >
<div class="area hd padr"  >&nbsp;</div>
<div class="cli  hd" style="width:219px;"  >Upload Type</div>
<div class="well hd bdr"  style="width:670px;"   >Uploaded Files</div>
</div>
FIN;
	$content .= $this->photoDiv($DISABLED);
	$content .= "</div>";
	$content .= "<div style=\"clear:both;\" ></div>\n";
	$content .= <<<FIN
</fieldset>
<fieldset style="margin-top:15px;"><legend style="margin-left:46.95%;">Signed</legend>
<div class="div50">
         <div class="div40" ><img src="$this->conSignature" height="180" width="300" /></div>
         <div class="div15 marg2" ><label for="apprdate" >Date<input type="text" name="apprdate" id="apprdate" class="date required"  value="$this->conSigDate" $DISABLED /></label></div>
         <div class="div30 marg2" ><label for="empname" >Signed for Contractor<input type="text" name="signee" id="signee" value="$this->conSignName" readonly /></label></div>
         </div>
         <div class="div50">
         <div class="div40" style="min-width:40% !important;" ><img src="$this->qgcSignature" id="qgcsig"  alt="QGC to Sign"  height="180" width="300" /></div>
         <div class="div60" >
         <div class="div25 marg2" ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"   value="$this->qgcApprDate" /></label></div>
         <div class="div63 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->empName"   /></label></div>
FIN;
	if ($this->approveText == "Approve" )  {
			$content .= "<div class=\"div30 marg2\" > <input type=\"checkbox\" name=\"approve\" id=\"approve\"  value=\"approve\"  $this->notifyChecked onclick=\"putSig();return false;\"  />\n";
			$content .= " <span id=\"appr\" class=\"apprspan\"  >$this->approveText</span> </div>\n";
	}
	else {
			$content .= "<div class=\"div30 marg2\" ><span id=\"appr\" class=\"apprspan\" >$this->approveText</span></div>\n";
	}
	$content .= <<<FIN
			</div>
         </div>
         </fieldset>
<script>
$("#complete").submit(function(){
    var isFormValid = true;
    $("#complete input:text.input.required").each(function() { // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#complete input:file.input.required").each(function() {
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight_red");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight_red");
        }
    });
    $("#complete .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
	 if (!isFormValid) {
			alert("Please fill in all the required fields (highlighted in red)"); 
    		return isFormValid;
	 }
	 else {
	   $('#SUBMIT').prop("disabled", "disabled");
    	return true;
	 }
});
$(function(){
   $(".input.date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area_1').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID_1').val(data);childSelect(data,'well',$('#welldiv_1')); return false; }
      });
  });
</script>
<script>
	jQuery('.input.hours').keyup(function () { this.value = this.value.replace(/[^0-9]/g,''); });
	$(document).on('change','.sel.creww',function() { getCOO($(this)); });
</script>
FIN;

				$content .= "<div style=\"clear:both;\" ></div> \n";
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found cost
				}
				$content .= "\n<a href=\"complete.php?action=list&redo=redo&con_id=$this->contractorID\"   class=\"button\" >Last Screen</a>";
				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllCompletions($search,$redo) {
			$URL="https://".$_SERVER['HTTP_HOST'];
			$whereClause=" where  length(cc.contractor_signee) > 0 and cc.removed is false ";
			$this->equalgreater = "SELECTED=\"selected\"";

			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
				$this->complStatus = $search->status;
            if (!empty($search->ID) ) {
               $this->compID = $search->ID;
               $whereClause .= " and cc.completion_cert_id = $this->compID ";
            }
            if (intval($search->cooID) > 0 ) {
               $this->callOffID = $search->cooID;
               $whereClause .= " and calloff_order_id = $this->callOffID ";
            }
				 else {
               if (isset($search->creDate) && strlen($search->creDate) > 0) {
                     $dateSelected = true;
                     $whereClause .= " and completion_date ";
                     $this->compDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
								$this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
								   $this->equalgreater = NULL;
                           $this->endDate = $search->endDate;
                           $this->hideEndDate=false;
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause .= "  =  current_date ";
                        $this->equalto = "SELECTED=\"selected\"";
                        break;
                  }
               }
					if ($search->areaID > 0 ) {
                  $whereClause .= " and cc.area_id = $search->areaID ";
                  $this->areaName = $search->areaName;
                  $this->areaID = $search->areaID;
               }
					if ($search->wellID > 0 ) {
                  $whereClause .= " and $search->wellID in (select (unnest(string_to_array(cc.well_ids::text,'|')::int[])))";
                  $this->wellID = $search->wellID;
               }
					if (!empty($search->crewID)  ) {
                  $whereClause .= " and cc.crew_id = $search->crewID ";
                  $this->crewID = $search->crewID;
               }
               if (!empty($search->subCrewID)  ) {
                  $whereClause .= " and $search->subCrewID in (select (unnest(string_to_array(cc.sub_crew_ids::text,'|')::int[])))";
                  $this->subCrewID = $search->subCrewID;
               }

					switch ($search->status) {
                  case 1:
                     $whereClause .= " and cc.qgc_approve_date is null ";
                  break;
                  case 2:
                     $whereClause .= " and cc.qgc_approve_date is not null ";
                 break;
                  default:
                  break;
               }
			   }
         }
         else {
             // Default query still needs search object serialized
            $this->compDate = date ('d-m-Y');
            $whereClause .= " and completion_date >= current_date";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            $search->dateType = "equalgreater";
            $search->creDate = $this->compDate;
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
         }
				



			if(! is_null($this->contractorID)) {  
				$sql = "SELECT cc.*,a.area_name,c.crew_name,wells_from_ids(cc.well_ids) as well_name,subscopes_from_ids(cc.sub_crew_ids) as sub_scopes,co.cost_estimate,el.fault from {$this->shortName}_completion_cert cc
         	LEFT JOIN calloff_order co using(calloff_order_id)
         	LEFT JOIN area a  on a.area_id = cc.area_id
         	LEFT JOIN crew  c on c. crew_id = cc.crew_id
         	LEFT JOIN email_log el on el.email_log_id = 
				( select max(email_log_id) from email_log  ell where cc.completion_cert_id  = ell.dkt_id and ell.receiver_type = 'CPL'  and ell.receiver_id = $this->contractorID )
         	$whereClause
         	order by completion_date,completion_cert_id";

				if (! $rs=$this->conn->Execute($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->ErrorMsg());
						}
				}



 				$content ="<div style=\"margin:auto;width:1770px;\" >";
				if ($this->compID == 0 ) {
					$this->compID = null;
				}
				if ($this->callOffID == 0 ) {
					$this->callOffID = null;
				}
         // search
//<div class="div300" ><label for="comp_id" class="label med">Completion ID</label><input type="text" name="comp_id" id="comp_id" class="input abn" value="$this->compID" /></div>
//<div class="div300" ><label for="calloff_id" class="label abn">C.O.O.</label><input type="text" name="calloff_id" id="calloff_id" class="input abn" value="$this->callOffID" /></div>
         	$content .= <<<FIN
         	<form name="field" method="post" action="complete.php?action=list">
         	<input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         	<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
				<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
				<input type="hidden" name="emp_id" id="emp_id" value="$this->empID" />
         	<input type="hidden" name="con_name" id="con_name" value="$this->contractorName" />
<fieldset ><legend style="margin-left:41%;">Filter Search - Completion Certificates: $this->contractorName</legend>
<div class="div5" ><label >Complete ID<input type="text" name="comp_id" id="comp_id" value="$this->compID" /></label></div>
<div class="div4 marg1" ><label >C.O.O.<input type="text" name="coo_id" id="coo_id" value="$this->callOffID" /></label></div>
FIN;
         $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->compDate,$this->endDate,$this->hideEndDate);
         $content  .= <<<FIN
<div class="div9 marg1" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div8 marg1" ><div id="welldiv" >
FIN;
			$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
			$content .= "</div></div>\n";
			$content .= "<div class=\"div10 marg1\"  ><label  >Work Scope</label>";
			$content .= AdminFunctions::crewSelect($this->crewID,"",true);
			$content .= "</div>\n";
			$content .= "<div class=\"div9 marg1\" id=\"subscopediv\" >";
			$content .= AdminFunctions::subScopeSelect($this->crewID,$this->subCrewID,"");
			$content .= "</div>\n";
			$content .= "<div class=\"div8 marg1\" ><label for=\"comp_status\" >Status:</label>\n";
			$content .= AdminFunctions::complStatSelect($this->complStatus,"");
			$content .= "</div>\n";

			$content .= <<<FIN
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),'#contractorID'); return false; }
      });
  });
$(function () {
      $('#coo_id').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=calloffs',
         params: {con_id: $('#contractorID').val()},
         minChars:2
      });
  });
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','single'); });
</script>
FIN;
			$content .= "<div style=\"float:right;width:14%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
         $content .= "\n<button class=\"marg\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
         //$content .= "<a href=\"hours.php?action=list&search=true&printcsv=true\" class=\"submitbutton printcsv\" >Print C.S.V.</a>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" >\n";
         $content .= "<div class=\"fone hd\">Compl Date</div>";
         $content .= "<div class=\"fone wd hd\">Completion ID</div>\n";
         $content .= "<div class=\"rate hd\">C.O.O.</div>\n";
         $content .= "<div class=\"usr wd hd\">Contract No.</div>";
         $content .= "<div class=\"crew hd\">Work Scope</div>\n";
         $content .= "<div class=\"crew hd\">Area</div>\n";
         $content .= "<div class=\"email hd\">Well/s</div>\n";
         $content .= "<div class=\"time hd padr\" >Orig Est.</div>\n";
         $content .= "<div class=\"fone hd padr\" >Total</div>\n";
         $content .= "<div class=\"email hd\">Comments</div>\n";
         $content .= "<div class=\"links hd\" style=\"width:253px;\"  >Actions</div></div>\n";
	
			$lineNo = 0;
			while (! $rs->EOF ) {
				$emailButTxt = "Approve";
            $emailButClass = "";
            $comp_id = $rs->fields['completion_cert_id'];
            $coo_id = $rs->fields['calloff_order_id'];
				$val = "<a href=\"calloff.php?action=find&calloff_id=$coo_id\" class=\"linkbutton\" style=\"margin-left:0.85rem;\"  >$coo_id</a>\n";
            $area = $rs->fields['area_name'];
            $well = $rs->fields['well_name'];
            $crew = $rs->fields['crew_name'];
				$sub_crew =  $rs->fields['sub_scopes'] ; //$rs->fields['sub_crew_name'];
				$pad = "SUB SCOPES: ";
            $toolTip = !empty($sub_crew) ?  "<span class=\"show-option\" title=\"${pad}${sub_crew}\" >$crew</span>" : $crew;
            $origEst = number_format($rs->fields['cost_estimate']);
            $compDate = AdminFunctions::dbDate($rs->fields['completion_date']);
            $status  = strlen($rs->fields['qgc_approve_date']) > 5 ? 2 : 1;
            $color = $this->statusArr[$status];
            $tot = number_format($rs->fields['total'],2);
            $contract = $rs->fields['contract_num'];
            $fault = ! is_null($rs->fields['fault']) ? preg_match('/OK/',$rs->fields['fault']) : -1;  // JOIN on email_log
            if ($fault > 0 ) {
               $emailButTxt = "Emailed";
               $emailButClass = "emailed";
            }
            $comments = $rs->fields['comments'];
            $content .= "<div id=\"linediv_$lineNo\" style=\"background:$color;border-bottom:1px solid #000000;float:left;display:table;table-layout:fixed;\" >\n";
            $content .= "<div class=\"fone highh cntr\">$compDate</div>";
            $content .= "<div class=\"fone wd highh cntr\">$comp_id</div>\n";
            $content .= "<div class=\"rate highh\">$val</div>\n";
            $content .= "<div class=\"usr wd highh\">$contract</div>";
            $content .= "<div class=\"crew highh cntr\">$toolTip</div>\n";
            $content .= "<div class=\"crew highh\">$area</div>\n";
            $content .= "<div class=\"email highh\">$well</div>\n";
            $content .= "<div class=\"time txtr highh\">$origEst</div>\n";
            $content .= "<div class=\"fone txtr highh\">$tot</div>\n";
            $content .= "<div class=\"email highh\">$comments</div>\n";
            //$content .= "</a>\n";
            if ($this->sessionProfile > 2 ) {
               $content .= "<div class=\"links highh\" style=\"width:253px;\"  >\n";
               if ($this->sessionProfile >= 4 ) {
                  if($status == 1 ) {  // can't re-approve
                     $content .= "<button  class=\"linkbutton $emailButClass\" style=\"float:left;\" onclick=\"emailComp($comp_id,$this->contractorID,$coo_id,$(this));$(this).prop('disabled','disabled');return false;\">$emailButTxt</button>";
               		$content .= "<a href=\"complete.php?action=update&comp_id=$comp_id&con_id=$this->contractorID\" title=\"Edit Completion\" class=\"linkbutton grn\"  >Edit</a>\n";
                  }
                  else {
                     $content .= "<button  class=\"linkbutton $emailButClass\" style=\"float:left;\" >$emailButTxt</button>";
               		$content .= "<a href=\"complete.php?action=find&comp_id=$comp_id&con_id=$this->contractorID\" title=\"View Completion\" class=\"linkbutton oran\"  >View</a>\n";
                  }

               }
               $content .= "<a href=\"{$URL}/printCompletepdf.php?con_id=$this->contractorID&comp_id=$comp_id&action=print\" title=\"Print Certificate\" class=\"linkbutton d\"  target=\"_blank\" >Print</a>\n";
               $content .= "<a href=\"coo_summary.php?action=show&SHOW=show&con_id=$this->contractorID&coo_id=$coo_id\" title=\"COO Summary\" class=\"linkbutton purp\"   >Summary</a>\n";
               if ($this->sessionProfile >= 16 && $status > 0 ) {  // Superintendent override
                     $content .= "<button  class=\"linkbutton bckred\"  onclick=\"unlockComp($comp_id,$this->contractorID,$coo_id);return false;\">Delete</button>";
               }

               $content .= "</div>\n";
            }
            else {
               $content .= "<div class=\"links highh\" style=\"width:235px;\" > &nbsp;</div>";
            }
            $content .= "</div><div style=\"clear:both;\"></div>";
            $lineNo += 1;
            $rs->MoveNext();

				}  
				$content .= "<hr /></div>";
			}
			else { // ContractorID NULL show contrcator select
         	$content = AdminFunctions::setContractor("complete.php?action=list",$this->contractorID); 
      	}
			$_SESSION['last_request'] = "${URL}/admin/complete.php?action=list&redo=redo&con_id=$this->contractorID";
			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		 private function submitForm() {
			global $BASE;
			$this->compID = intval($_POST['comp_id']);
			$coo = isset($_POST['coo']) ? $_POST['coo'] : $_POST['coo_id'];
			$this->callOffID =  is_array($coo) ? intval($coo[0]) : $coo;
			$this->empID = intval($_POST['empID']);
			if (!empty($_POST['qgc_approved'])) { 
				$eNum = AdminFunctions::email_approve_comp($this->compID,$this->contractorID,$this->callOffID,$this->empID,"POST");
				 exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
				// send email 
			}

         header("LOCATION: complete.php?action=list&redo=redo&con_id=$this->contractorID");
         exit;

      }

		private function getCompDetails($compID) {
         $sql = "SELECT a.area_name,c.crew_name,wells_from_ids(cc.well_ids) as well_name,subscopes_from_ids(cc.sub_crew_ids) as subscope_name,cc.* from {$this->shortName}_completion_cert cc
         LEFT JOIN area a using(area_id)
			LEFT JOIN crew c using(crew_id) 

         where cc.completion_cert_id = $compID";
         if (! $data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }
         //$this->total = $data['total'];  // over rides it from totalsDiv
         $this->compID = $data['completion_cert_id'];
         $this->callOffID = $data['calloff_order_id'];
			$this->totalsHTML = $this->totalsDiv($this->callOffID);
         $this->wellName = $data['well_name'];
         $this->wellIDs = $data['well_ids'];
         $this->areaName = $data['area_name'];
         $this->areaID = $data['area_id'];
         $this->crewID = $data['crew_id'];
			$this->crewName = $data['crew_name'];
			$this->subCrewName = $data['subscope_name'];
			$this->subCrewIDs = $data['sub_crew_ids'];
         $this->contractNum = $data['contract_num'];
         $this->compDate = AdminFunctions::dbDate($data['completion_date']);
         $this->comments = $data['comments'];
			$this->percComplete = $data['percentage_complete'];
         $this->origEst = "$".number_format($data['cost_estimate'],2);
         $this->table1 = "$".number_format($data['total_t1'],2);
         $this->table2 = "$".number_format($data['total_t2'],2);
         $this->totExp = "$".number_format($data['expense'],2);
         $this->conSignName =  $data['contractor_signee'];
         $this->conSigDate = AdminFunctions::dbDate($data['contractor_sig_date']);
         $this->gravelTot = "$".number_format($data['total_gravel'],2);
         $this->waterTot = "$".number_format($data['total_water'],2);
         $this->multi = $data['well_ids'];
			if (!empty($data['contractor_sig'])) {
         	$this->conSignature = "{$this->conDomain}/admin/" . $data['contractor_sig'];
			}
         else {
         	$this->conSignature =  "{$this->conDomain}/admin/" ."images/nosig.png"; 
			}
         //if ($this->action == "update" ) {
           // $this->approvalDate =  date('d-m-Y');
           // $this->qgcSignature = strlen($data['qgc_sig']) > 5 ? $data['qgc_sig'] : AdminFunctions::getSig($this->empID);
           // $this->empName = strlen($data['qgc_signee']) > 3 ? $data['qgc_signee']: $this->empName;
        // }
        // else {
        	$this->approvalDate =  AdminFunctions::dbDate($data['qgc_approve_date']);
			if (!empty($data['qgc_sig'])) {
        		$this->qgcSignature =  $data['qgc_sig'];
			}
			
         $this->empName = $data['qgc_signee'];
			if (!empty($this->approvalDate)) {
				$this->notifyChecked =  "checked" ;
				$this->approveText = "Approved";
			}
			else {
				$this->notifyChecked =  ""; 
				$this->approveText = "Approve";
			}
        // }
         $sql = "SELECT upload_id,upload_name,upload_type_id from {$this->shortName}_upload where completion_cert_id = $compID";
         if(! $this->photoLines = $this->conn->getAll($sql)) {
            if ($this->conn->ErrorNo() != 0 ) {
               die($this->conn->ErrorMsg());
            }
         }

		// lines
		}
		public function photoDiv($DISABLED) {
			$pcount = count($this->photoLines);
			$html = "";
			for ($x=0;$x<$pcount;$x++) {
				$uplink = $uptypeID =  $upID = NULL;
				if ($this->action != "new" ) {
					if(isset($this->photoLines[$x]['upload_name'])) {
						$upname = $this->photoLines[$x]['upload_name'];
						$upID = $this->photoLines[$x]['upload_id'];
						$uplink = "<a href=\"{$this->conDomain}/admin/$upname\" target=\"_blank\"  class=\"margpad\" >$upname</a>";
						$uptypeID = $this->photoLines[$x]['upload_type_id'];
					}
					else if($x == 0 ) {
						$uptypeID = 13;
						$REQUIRED = "required";
					}
					else if ($x == 1) {
						$uptypeID = 12;
						$REQUIRED = "required";
					}
				}
				else {
					if ($x == 0 ) {
						$uptypeID = 13;
						$REQUIRED = "required";
					}
					else if ($x == 1 ) {
						$uptypeID = 12;
						$REQUIRED = "required";
					}
				}
				$html .= "<div id=\"photo_div_$x\" >\n";
				$html .= "<input type=\"hidden\" value=\"$upID\" name=\"uploadid[]\" /> "; 
				$html .= "<div style=\"clear:both;\" ></div>\n";
				//$html .= "<div class=\"area bdb padr\"  ><input type=\"file\"  name=\"photo[]\"  disabled  /></div>\n";
				$html .= "<div class=\"area bdb padr\"  >&nbsp;</div>\n";
				$html .= "<div class=\"cli bdb\"  style=\"width:219px;\"  >\n";
				$html .= AdminFunctions::completeUploadSelect($uptypeID,"disabled","");
				$html .= "</div>\n";
				$html .= "<div class=\"well line2 bdb bdr\" style=\"width:670px;\"   >$uplink</div>\n";
				$html .= "</div>";
				$html .= "<div style=\"clear:both;\" ></div>\n";
			}
			return $html;
		}
	private function totalsDiv($cooID) {
		$content = "";
//  Got thru all above  tests must be good to go
		$sql = "with tab as (select 0 as total_t1,0 as total_t2, 0 as expense,coalesce(sum(water_units),0) as water_units,coalesce(sum(water_total),0) as water_total,
		coalesce(sum(gravel_units),0) as gravel_units,coalesce(sum(gravel_total),0) as gravel_total,sub_crew_name from {$this->shortName}_gravel_water
	 	LEFT JOIN sub_crew using(sub_crew_id)
 		where calloff_order_id = $cooID 
		GROUP by sub_crew_name
 		UNION
 		select coalesce(sum(total_t1),0) as total_t1,coalesce(sum(total_t2),0) as total_t2,coalesce(sum(expense),0) as expense,0 as water_units,0 as water_total,0 as gravel_units,
		0 as gravel_total,sub_crew_name from {$this->shortName}_hour h
 		LEFT JOIN sub_crew using(sub_crew_id)
 		WHERE h.calloff_order_id = $cooID 
		AND h.completion_cert_id = $this->compID
 		GROUP by sub_crew_name)

 		select coalesce(sum(total_t1),0) as total_t1,coalesce(sum(total_t2),0) as total_t2,coalesce(sum(expense),0) as expense,
 		coalesce(sum(water_units),0) as water_units,coalesce(sum(water_total),0) as water_total,
 		coalesce(sum(gravel_units),0) as gravel_units,coalesce(sum(gravel_total),0) as gravel_total,sub_crew_name
 		From tab
 		group by sub_crew_name
 		Order by sub_crew_name";

		if (!$data=$this->conn->getAll($sql)){
			if ($this->conn->ErrorNo() != 0 ) {
				echo $sql;
				die($this->conn->ErrorMsg());
			}
		}

		if (count($data)> 0 ) {
			$t1Tot = $t2Tot = $expTot =  $gTot = $sTot = $gravTot = $waterTot = $gravUnits = $waterUnits = 0;

       	$content ="<div class=\"heading\" ><div class=\"email hd\" >Sub Scope</div><div class=\"usr narr hd padr\">Gravel m3</div><div class=\"usr hd padr\">Gravel Cost</div>\n";
			$content .= "<div class=\"usr narr hd padr\">Water kL</div><div class=\"usr hd padr\">Water Cost</div>\n";
      	$content .= "<div class=\"usr hd padr\">Table 1</div><div class=\"usr hd padr\">Table 2</div><div class=\"usr hd padr\">Expense</div><div class=\"usr hd padr bdr\">Totals</div></div>\n";
       	foreach ($data as $ind=>$val) {
            extract($val);
            $gravUnits += $gravel_units;
            $waterUnits += $water_units;
            $gravTot += $gravel_total;
            $waterTot += $water_total;
            $t1Tot += $total_t1;
            $t2Tot += $total_t2;
            $expTot += $expense;
            $sTot = floatval($total_t1)  + floatval($total_t2)  + floatval($expense) ; ## + floatval($gravel_total) + floatval($water_total);
            $total_t1 = floatval($total_t1) > 0 ? "$".number_format($total_t1,2)  : "";
            $total_t2 = floatval($total_t2) > 0 ? "$".number_format($total_t2,2)  : "";
            $expense = floatval($expense) > 0 ? "$".number_format($expense,2) : "";
            $gravel = floatval($gravel_units) > 0 ? number_format($gravel_units)  : "";
            $water = floatval($water_units) > 0 ? number_format($water_units)  : "";
            $gravel_total = floatval($gravel_total) > 0 ? "$".number_format($gravel_total,2)  : "";
            $water_total = floatval($water_total) > 0 ? "$".number_format($water_total,2)  : "";
            $sTot = floatval($sTot) > 0 ? "$".number_format($sTot,2) : "";
            $content .= "<div style=\"float:left;display:table;table-layout:fixed;border-bottom:1px solid #000000;\" >\n";
            $content .="<div class=\"email highh\"  >$sub_crew_name</div><div class=\"usr narr txtr highh lte\">$gravel</div><div class=\"usr txtr highh \">$gravel_total</div>\n";
				$content .= "<div class=\"usr narr txtr highh lte\">$water</div><div class=\"usr txtr highh \">$water_total</div>\n";
            $content .= "<div class=\"usr txtr highh \">$total_t1</div><div class=\"usr txtr highh \">$total_t2</div>\n";
            $content .= "<div class=\"usr txtr highh \">$expense</div><div class=\"usr bdr txtr highh line1\">$sTot</div>\n";
            $content .= "</div>\n";


       	}
         $gTot += floatval($t1Tot)  + floatval($t2Tot)  + floatval($expTot); ##+ floatval($gravTot) + floatval($waterTot);
         $t1Tot = floatval($t1Tot) > 0 ? "$".number_format($t1Tot,2)  : "";
         $t2Tot = floatval($t2Tot) > 0 ? "$".number_format($t2Tot,2)  : "";
         $expTot = floatval($expTot) > 0 ? "$".number_format($expTot,2) : "";
         $gTot = floatval($gTot) > 0 ? "$".number_format($gTot,2) : "";
         $gravUnits = floatval($gravUnits) > 0 ? number_format($gravUnits) : "";
         $waterUnits = floatval($waterUnits) > 0 ? number_format($waterUnits) : "";
         $gravTot = floatval($gravTot) > 0 ? "$".number_format($gravTot,2) : "";
         $waterTot = floatval($waterTot) > 0 ? "$".number_format($waterTot,2) : "";
			$this->gravel = $gravUnits;
			$this->water = $waterUnits;
			$this->total = $gTot;
         $content .= "<div style=\"float:left;display:table;table-layout:fixed;border-bottom:1px solid #000000;\" >\n";
         $content .="<div class=\"email highh\" >&nbsp;</div><div class=\"usr narr txtr highh lte\">$gravUnits</div><div class=\"usr txtr highh disback\">$gravTot</div>\n";
			$content .= "<div class=\"usr narr txtr highh lte\">$waterUnits</div><div class=\"usr txtr highh disback\">$waterTot</div>\n";
         $content .= "<div class=\"usr txtr highh line1\">$t1Tot</div><div class=\"usr txtr highh line1\">$t2Tot</div>\n";
         $content .= "<div class=\"usr txtr highh line1\">$expTot</div><div class=\"usr bdr txtr highh disback\">$gTot</div>\n";
         $content .= "</div>\n";
         $content .= "<div style=\"clear:both;height:1rem;\" ></div>\n";
		}
		return $content;
	}

}
?>
