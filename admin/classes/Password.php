<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Password  {
		public $page;
		public $employeeID;
		public $user;
		public $sessionProfile;
		public $conn;
		public $oldPassword;
		public $empName;
		public $pass1;
		public $pass2;

		public function	__construct() {
			$this->conn = $GLOBALS['conn'];
			$this->employeeID = $_SESSION['employee_id'];
			if (! isset($_REQUEST['SUBMIT'])) {
				$this->getEmpDetails($this->employeeID);
				$pg = new Page('login');
				$this->page= $pg->page;
				$heading_text = "Reset Password for $this->empName";
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent();	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		 private function setContent() {
         $content = '';
			$error = "";
         if ($_SESSION['pwd_unknown'] === 'true') {
            $_SESSION['pwd_unknown'] = 'false';
            $error = '<div><span  class="error cntr" >Password must be different from previous password. Please try again</span></div>';
         }

        $content .=  <<<FIN
		 <div id="main_login">
       <form name="password" id="password" action="change_password.php" method="post" style="padding:1rem 1.2rem 0 1.2rem;" >
		  $error
		 <input type="hidden" name="prev_pass" value="$this->oldPassword" />
		 <input type="hidden" name="empID" value="$this->employeeID" />
       <label for="pass1" >Password:<input type="password" name="pass1" id="pass1"   /></label>
       <label for="pass2" >Re-Type Password:</label> <input type="password" name="pass2" id="pass2"  /></label>
       <div id="login_submit"><input  type="submit" value="Reset" name="SUBMIT" class="button" style=\"margin-left:9%;\" /></div>
       </form>
     </div>";
<script>
$("#password").submit(function(){
   var p1 = $('#pass1').val(); 
   var p2 = $('#pass2').val(); 
	if (p1 != p2 ) {
    	$('#pass1').val(''); 
    	$('#pass2').val(''); 
    	$('#pass1').focus(); 
		alert("Passwords are not the same. Please try again");
    	return false;
	}
	 
});
</script>
FIN;
         $this->page = str_replace('##MAIN##',$content,$this->page);
			$this->page = str_replace('##CURRENT##',"Change Password",$this->page);

      }


		private function getEmpDetails($empID) {
         return true;
			$sql = "SELECT username,password,firstname || ' ' || coalesce(lastname,'') as name from old_employee where employee_id = $empID";
			if (!$data = $this->conn->getRow($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
				else {

				}
			}
			$this->user = $data['username'];
			$this->oldPassword = trim($data['password']);
			$this->empName = $data['name'];

		}
		private function processPost() {
			$this->oldPassword = $_POST['prev_pass'];
			$this->pass1 = trim($_POST['pass1']);
			if (md5($this->pass1) != $this->oldPassword ){ 
				$this->pass2 = md5($this->pass1);
				$this->employeeID = $_POST['empID'];
				$sql = "UPDATE employee set last_login = now(),password = '$this->pass2'  where employee_id = $this->employeeID";
				//echo $sql;
            //exit;

				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				if (isset($_SESSION['last_request'])) {
         		$request = $_SESSION['last_request'];
         		unset($_SESSION['last_request']);
         		header("Location: $request");
         		exit;
      		}
      		else {
         		header("Location: index.php");
         		exit;
      		}

			
			}
			else {
            $_SESSION['pwd_unknown'] = 'true';
				header("Location: change_password.php");
				exit;
			}

		}
	}
?>
