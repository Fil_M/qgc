<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Correspondence {
		public $page;
		public $sessionProfile;
		public $conn;
		public $workflowID;
		public $childWin;
		public $account_name;
		public $clientID;
		public $creDate;
		public $between;
      public $endDate;
      public $hideEndDate=true;
      public $equalgreater;
      public $equalto;
      public $less;
      public $blankdate;
		public $formType;
		public $corrType;
		public $empID;
		public $sentBy;
		public $URL;
		public $hDate;


		public function	__construct($action,$workflowID=NULL,$search=NULL,$redo=NULL,$childWin="false") {
			$this->workflowID = $workflowID;
			$this->action = $action;
			$this->childWin = $childWin;
			$this->conn = $GLOBALS['conn'];
			$this->empID = $_SESSION['employee_id'];
			$this->URL = "https://". $_SERVER['HTTP_HOST'];

			//$this->certificateObj = new Certificate($this->address_type);
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('correspondence');
				$this->page= $pg->page;
         	switch($action) {
					case "list" :
						if (intval($this->workflowID) > 0 ) {
							$heading_text = "List Correspondence WF{$workflowID}";
							$this->getClientDetails($this->workflowID);
						}
						else {
							$heading_text =   "List Correspondence";
						}
						$this->getAllCorrespondence($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Correspondence $workflowID";
						$this->getCorrespondenceDetails($workflowID);
						break;
					default:
						$heading_text =   "List Correspondence";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function getClientDetails($workflowID) {
			$sql = "SELECT c.client_id,c.account_name from client c join workflow w using(client_id) where w.workflow_id = $workflowID";
			if (!$data = $this->conn->getRow($sql)) {
				if($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			extract($data);
			$this->account_name = $account_name;
			$this->clientID = $client_id;
		}
		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->productID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add";
						$this->valHTML = "<label>&nbsp;<input type=\"text\" readonly  name=\"servplanvalid[]\" /></label>";
						break;
					case "update":
						$button = "Update";
						break;
					case "delete":
						$button = "Delete";
						$DISABLED='disabled';
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add";
						break;
				}
			$prodHTML = AdminFunctions::productSelect($this->productType,"",true,"product_type[]",0);
			$docHTML = AdminFunctions::supplierSelect($this->supplierID,"",true,"supplier[]" );
			$packHTML = AdminFunctions::packageSelect($this->packageID,"",true,"PHONE","package[]" );
			$content = <<<FIN
<div style="width:1772px;margin:auto;">
<form name="product" id="product" method="post" action="product.php?SUBMIT=SUBMIT">
<input type="hidden" name="product_id" value="$this->productID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="padding:0px 0.3rem 5px;" ><legend style="margin-left:46.85%;">Correspondence</legend>
rdipondence.php
 id="proddiv" style="width:100%;"  >
<div class="linenum" id="linenum_0" >
<div class="div700"  > $prodHTML </div>
<div class="div14" id="dv_0" ><label>Plan<input type="text" name="plan[]" id="plan_0" value="$this->servicePlanText" /></label> </div>
<div id="auto_0" ></div>
<input type="hidden" name="planid[]" id="planid_0"  value="$this->planID" />
<input type="hidden" name="productID[]" value="$this->productID" />
<div class="div5" id="serv_val_div_0" >$this->valHTML</div>
<div class="div9" ><label>Correspondence Code<input type="text" name="product_code[]" id="pcode_0" value="$this->productCode" /></label> </div>
<div class="div25" ><label>Description<input type="text" name="description[]" id="descrip_0" value="$this->description" /></label></div>
<div class="div6" ><label>Multiplier<input type="text" name="multiplier[]" class="cntr num" value="$this->multiplier" /></label></div>
<div class="div5" ><label>Cost Price<input type="text" name="cost[]" class="cntr num required" value="$this->cost" /></label></div>
<div class="div5" ><label>Rebate<input type="text" name="rebate[]" class="cntr num required " value="$this->rebate" /></label></div>
<div class="div12" >$packHTML</div>
<div class="div12" >$docHTML</div>
</div>
<div style="clear:both;" ></div>
</div> <!-- end  prod div -->
<script>
	$("#product").submit(function(){
    var isFormValid = true;
    var statVal = $('#status').val();
    $("#product .required").each(function(){
        if ($.trim($(this).val()).length == 0 ){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });

    if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
    else {
      $('#SUBMIT').prop("disabled", "disabled");
      return true;
    }
});

   $(document).on('keyup', '.num', function(){
   this.value = this.value.replace(/[^0-9\.-]/g,'');
});
</script>
FIN;
/*<script>
	$(function () {
      $('#plan_0').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=servplan',
         minChars:2,
			onSelect: function(value, data){ $('#planid_0').val(data[0]);$('#pcode_0').val(data[2]);servVals(data[0],0);return false; }
      });
  });
</script>  */
				if ($this->action == "new" ) {
					$content .= "<button class=\"button tiny margt4\" style=\"padding:0.625rem 0.725rem 0.6875rem 0.725rem;\" id=\"addbutton\" onclick=\"getPack();return false;\">Add New Line</button>\n";
				}
				$content .= "</fieldset>\n";
				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" style=\"min-width:97px;\" />"; // no submit for find employee 
					$content .= "\n<a href=\"product.php?action=list&search=true&redo=redo\"   class=\"button\" style=\"padding:0.8rem 1rem 0.9rem;\" >Last Screen</a>";
				}
				else {
					$content .= "\n<a href=\"product.php?action=list&search=true&redo=redo\"   class=\"button\"  style=\"padding:0.8rem 1rem 0.9rem;\" >Last Screen</a>";
				}


			 	$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}	

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllCorrespondence($search=NULL,$redo=NULL) {
			$whereClause=" where 1 = 1 ";
			$defaultDate = true;
			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
				if (!empty($search->sDate)) {
                $dateSelected = true;
					 $defaultDate = false;
                $whereClause = " WHERE  send_time::date ";
                switch ($search->dateType) {
                case "equalto":
                   $whereClause .= "  =  '$search->sDate' ";
                   $this->equalto = "SELECTED=\"selected\"";
                   $this->equalgreater = NULL;
                   $this->hDate = $search->sDate;
                   break;
                case "equalgreater":
                   $whereClause .= "  >=  '$search->sDate' ";
                   $this->hDate = $search->sDate;
                   $this->equalgreater = "SELECTED=\"selected\"";
                   break;
                case "less":
                   	$this->hDate = $search->sDate;
                      $whereClause .= "  <  '$search->sDate' ";
                      $this->less = "SELECTED=\"selected\"";
                      break;
                case "between":
                   if (strlen($search->endDate) > 0 ) {
                      $whereClause .= " between  '$search->sDate' and '$search->endDate' ";
                      $this->between = "SELECTED=\"selected\"";
                      $this->endDate = $search->endDate;
                      $this->hideEndDate='';
                      $this->equalgreater = NULL;
                      $this->hDate = $search->sDate;
                   }
                   else {
                      $whereClause = "  =  '$search->sDate' ";
                   }
                   break;
                   default:
                     $whereClause = "WHERE send_time::date >= current_date - interval '3 days'";
                     $this->equalGreater = "SELECTED=\"selected\"";
							$this->hDate = date('d-m-Y',time() - (86400 * 3 ));
                     break;
               }
            }
				if (intval($search->workflowID) > 0 ) {
					$this->workflowID = $search->workflowID;
					$whereClause=" where client_id = (select client_id from workflow where workflow_id = $this->workflowID) ";
				}
				if (intval($search->clientID) > 0 ) {
					$this->clientID = $search->clientID;
					$this->account_name = $search->clientName;
					$whereClause=" where client_id = $this->clientID";
				}
				if (intval($search->empID) > 0 ) {
					$this->sentBy = $search->empID;
					$whereClause .= " and employee_id = $this->sentBy ";
				}
				if ( ! empty($search->formType) && $search->formType != '-1'  ) {
					$this->formType = $search->formType;
					$whereClause .=" and form_type = '$this->formType' ";
				}

				if ($defaultDate ) {
					$whereClause .= " AND send_time::date >= current_date - interval '3 days'";
               $this->hDate = date('d-m-Y',time() - (86400 * 3)); 
               $this->equalgreater = "SELECTED=\"selected\"";
				}
			}
			else {
				if ($defaultDate ) {
					$whereClause .= " AND send_time::date >= current_date - interval '3 days'";
               $this->hDate = date('d-m-Y',time() - (86400 * 3)); 
				}
            $search = new Search();
            $search->sDate = $this->hDate; 
         }
			$sql = "select el.email_log_id,receiver_id,subject,fault,sent_receiver_emails,sent_company_emails,attachments,receiver_message_id,company_message_id,receiver_fault,send_time,dkt_id,con_name
from email_log el
LEFT JOIN email_result er on er.email_log_id  = el.email_log_id
LEFT JOIN  contractor con on con.contractor_id = el.receiver_id 
$whereClause 
order by send_time desc LIMIT 300";

			 $_SESSION['SQL'] = serialize($search);

			//echo $sql;
         if (! $rs=$this->conn->Execute($sql)) {
            die($this->ErrorMsg());
         }



			$content = "<div style=\"width:1772px;margin:auto;\">\n";
			   $content  .= <<<FIN
<form name="correspondence" method="post" action="correspondence.php?action=list&search=true">
<fieldset ><legend style="margin-left:46%;">Filter Search - Correspondence</legend>
FIN;
			 $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->hDate,$this->endDate,$this->hideEndDate);
			 $content .= "<div style=\"float:right;width:15%;\" >\n";
          $content .= "<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\" style=\"margin-left:2%;\"  />\n";
			 $content .="<button  style=\"padding: 1.1rem 2rem 1.0625rem;\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>\n";
			 $content .= "</div>\n";


			$content .= "</fieldset></form>\n";
//Rows
			$content .= "<div class=\"heading\" >\n";
         $content .= "<div class=\"usr hd\">Send Time</div>\n";
         $content .= "<div class=\"addr hd\">Subject</div>\n";
         $content .= "<div class=\"ptype hd\">Receiver</div>\n";
         $content .= "<div class=\"workscope hd\">Sent Emails</div>\n";
         $content .= "<div class=\"workscope micro hd\">Attachment/s</div>\n";
         $content .= "<div class=\"workscope narr hd\">Message ID</div>\n";
         $content .= "<div class=\"rate narr hd\">Log ID</div>\n";
         $content .= "<div class=\"rate micro hd\">Result</div>\n";
         $content .= "<div class=\"links hd\" style=\"width:70px;\"  >Actions</div>\n";
			$content .= "</div>\n";
         $lineNo = 0;
			while (! $rs->EOF ) {
				$attachments = $attachLink = "";
				$ln = $lineNo % 2 == 0  ? "line2" : "line3";
            extract($rs->fields);
				$send_time = substr(AdminFunctions::dbTime($send_time),0,16);
				$message_id = !empty($receiver_message_id) ? $receiver_message_id : $company_message_id;
				$emails = !empty($sent_receiver_emails) ? $sent_receiver_emails : $sent_company_emails;
				$message_id = htmlentities(str_replace("Message-ID:","",$message_id));
				$con_name = !empty($con_name) ? $con_name : "SELF";
				//$attach = $this->getAttaches($email_log_id);
				if (!empty($attachments)) {
					$res = preg_match('/https:\/\//',$attachments);
					if ($res > 0  ) {
						$attachName = basename($attachments);
						$attachLink = "<a href=\"$attachments\" target=\"_blank\" >$attachName</a>\n";
					}
					else {
						$attachDir = basename(dirname($attachments));
						$attachName = basename($attachments);
						$attachLink = "<a href=\"$this->URL/admin/$attachDir/$attachName\" target=\"_blank\" >$attachName</a>\n";
					}	
				}
				//$receiver_fault = empty($sent_receiver_emails) ? $receiver_fault: $fault;
            if (empty($fault) ) {
					$ln = "redd";
				}
				$butt = "<div style=\"margin:auto;width:54px;\" ><a href=\"resend_email.php?enum=$email_log_id\" class=\"linkbutton oran\" style=\"padding:0.1rem 0.25rem;\" target=\"_blank\" >Resend</a></div>\n";
				$content .= "<div class=\"tabdiv $ln\" >\n";
         	$content .= "<div class=\"usr cntr highh\">$send_time</div>\n";
         	$content .= "<div class=\"addr highh\">$subject</div>\n";
         	$content .= "<div class=\"ptype highh cntr\">$con_name</div>\n";
         	$content .= "<div class=\"workscope highh\" style=\"word-break: break-all;\"  >$emails</div>\n";
         	$content .= "<div class=\"workscope micro highh\"  style=\"word-break: break-all;\" >$attachLink</div>\n";
         	$content .= "<div class=\"workscope narr cntr highh\">$message_id</div>\n";
         	$content .= "<div class=\"rate narr highh\">$email_log_id</div>\n";
         	$content .= "<div class=\"rate micro highh\">$fault</div>\n";
         	$content .= "<div class=\"links highh\" style=\"width:70px;text-align:center;\"  >$butt</div>\n";
				$content .= "</div>\n";
				$lineNo++;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";
			$content .= <<<FIN
<script>
$.fn.clearForm = function() {
	return this.each(function() {
      var type = this.type, tag = this.tagName.toLowerCase();
      if (tag == 'form') {
         return $(':input',this).clearForm();
      }
      if (type == 'text' || type == 'password' ||  tag == 'textarea' || type == 'hidden' ) {
          this.value = '';
       }
       else if (tag == 'select') {
          var id = $(this).attr('id');
          this.selectedIndex = -1;
       }
    });
};
$(function () {
      $('#client').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=client_all',
         minChars:2,
         onSelect: function(value, data){ $('#clientID').val(data);$('#client').val(value); return false; }
      });
  });
$(function () {
      $('#workflow').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=workflow_email',
         minChars:2,
         onSelect: function(value, data){ $('#workflow').val(value); return false; }
      });
  });
$(document).on('focus',".date", function(){   /* Adds Dynamic Datepicker !!  */
    $(this).datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
			 if ($this->childWin == "true" ) { // sent as $_REQUEST parameter
               $content .= "<div style=\"float:left;\" ><button class=\"button\" onclick=\"window.open('', '_self', ''); window.close();\"  style=\"padding:1.15rem 1rem;min-width:115px;\" >Close</button></div>\n";
          }



			$this->page = str_replace('##MAIN##',$content,$this->page);

		}
		private function getAttaches($emailID) {
			global $BASE;  ///  filessytem  of DOCROOT ending in /
			$attach = "";
			$sql = "SELECT * from attachment_from_email($emailID)";
			if (!$data = $this->conn->getOne($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			$attachArr=explode(",",$data);
			if (count($attachArr > 0 )) {
				foreach($attachArr as $ind=>$att ) {
					$cut_name = str_replace($BASE,"",$att); 
					$attName = basename($att);
					$attach .= "<a href=\"{$this->URL}/$cut_name\" target=\"_blank\" >$attName</a><br />";

				}
			}

			return $attach;
		}

		private function getCorrespondenceDetails($productID) {
			$sql = "SELECT  p.*,sp.service_plan_text,esp.ex_service_plan_text
			from product  p
			LEFT JOIN service_plan sp using(service_plan_id)
			LEFT JOIN extra_service_plan esp using(extra_service_plan_id)
         where product_id = $productID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			
			$this->multiplier = is_numeric($data['use_multiplier']) > 0 ? $data['use_multiplier'] : "System * $this->multiplierVal";
			$this->servicePlanText = !empty($data['service_plan_text']) ? $data['service_plan_text'] : $data['ex_service_plan_text'];
			$this->productID = $data['product_id'];
			$this->description = $data['description'];
			$this->productType = $data['product_type_id'];
			$this->packageID = $data['package_id'];
			$this->packageID = $data['package_id'];
			$this->supplierID = $data['supplier_id'];
			if ($this->productType == 1) {
         	$this->planID = $data['service_plan_id'];
			}
			elseif ($this->productType == 2 ) {
         	$this->planID = $data['extra_service_plan_id'];
			}
			else {
				$this->planID = "";
			}

         $this->rebate = $data['rebate'];
         $this->cost = $data['cost_price'];
         $this->productCode = $data['product_code'];
         $this->servPlanValID = $data['service_plan_val_id'];
			if (intval($this->planID) > 0 ) {
				if ($this->productType == 1 ) {
					$this->valHTML = AdminFunctions::servicePlanValSelect($this->planID,$this->servPlanValID,"",false,"servplanvalid[]",0);
				}
				else {
					$this->valHTML = "<label>&nbsp;<input type=\"text\" readonly  name=\"servplanvalid[]\" /></label>";
				}
			}

		}
}
?>
