<?php
	// Administration
	class Menu {
		public $pageType;
		public $div;
		public $conn;
		public $companyID;
		public $profileID;
		public function __construct($pageType) {
			$this->conn = $GLOBALS['conn'];
			$this->pageType=$pageType;
			if ($this->pageType != 'login' ) {  // No menu's
				$this->companyID = $_SESSION['company_id'];
				$this->profileID = $_SESSION['profile_id'];
				$this->doMenu();
			} 
		}

      private function doMenu() {
        $this->div .= '<section class="top-bar-section">';
        $this->div .= "\n<ul id=\"menu\" class=\"right\">";
            $sql = "SELECT admin_menu_id,menu_text,href from admin_menu where  level = 0   and  min_profile_id <= $this->profileID and  company_id in (0,$this->companyID) order by menu_order";
            if (!$data = $this->conn->getAll($sql)) {
                 die($this->conn->ErrorMsg());
            }
            foreach($data as $key=>$val) {
                if (strlen($val["href"]) <= 5) {
                    $this->div .= "<li class=\"menudiv\" ><a>".$val['menu_text']."</a>";
                } else {
                    $this->div .= "<li><a href=\"".$val["href"]."\">".$val['menu_text']."</a>";
                }
                
                // Second Pass recursive loop through menus 
                    $str = "";
                    $id = $val['admin_menu_id'];
                    $uList = $this->getChildren($id,$str);
                    $uList = preg_replace('/^<ul>/',"<ul>",$uList);
                    $this->div .= $uList;
                $this->div .= "</li>\n";
            }
            $this->div .= "</ul>\n";
				$this->div .= "</section><script> menuify('#menu'); </script> ";
      }
      private function getChildren($parentID,$str) {
			$sql = "SELECT * from admin_menu where parent_menu_id = $parentID   and min_profile_id <= $this->profileID  and company_id in (0,$this->companyID) order by menu_order";
			$data = $this->conn->getAll($sql);
			if (count($data) == 0 ) {
            $str .= "</li>\n";
				return $str;
				exit;

			}
			else {
				$str .="<ul class=\"has-dropdown\" >\n";
				foreach($data as $key=>$val) {
					$menTxt = $val['menu_text'];
					if ($menTxt == "Help Document") {
						$href = AdminFunctions::getHelpDoc();
						$str .= "\t<li><a href=\"".$href . "\" target='_blank'>".$val['menu_text'] ."</a>";					
					}

					if ($val['menu_text'] !=="Help Document") {
						$str .= "\t<li><a href=\"".$val['href'] . "\">".$val['menu_text'] ."</a>";
					}
					$str = $this->getChildren($val['admin_menu_id'],$str);
				}
				$str .= "</ul>\n";
			   return $str;
			}
			
		}
	}
?>
