<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class MonthCSV  {
		public $page;
		public $action;
		public $sessionProfile;
		public $subtot;
		public $statusText = array("0"=>"Newly Entered","1"=>"Awaiting Approval","2"=>"REJECTED","3"=>"Late Submission","4"=>"Rejected Entry","5"=>"Approved","6"=>"Requested Completion",
										"7"=>"Approved Completion","8"=>"Invoiced","9"=>"Paid");
		public $status=NULL;
		public $total;
		public $conn;
		public $endDate;
		public $invoice;
		public $invoiceNum;
		public $employeeID;
		public $operatorID;
		public $search=false;
		public $conArr;
      public $contractorName;
      public $shortName;
      public $conDomain;
      public $dbLink;
		public $file;
      public $startDate;
		public $heading_text;
		public $gTot;
		public $totHours;
		public $csv = array('Contractor'=>array(),'Date'=>array(),'EnterTime'=>array(),'COO'=>array(),'Operator'=>array(),'DocketHours'=>array(),'Docket'=>array(),'PlantType'=>array(),'Unit'=>array(),'Rate'=>array(),'Total'=>array(),'Area'=>array(),'Well'=>array(),'Detail'=>array(),'Crew'=>array(),'subScope'=>array(),'Status_Txt'=>array(),'Reject_Txt'=>array());


		public function	__construct($action="list",$startDate,$endDate) {
			$this->action = $action;
			$this->startDate = $startDate;
			$this->endDate = $endDate;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
         $this->file= "tmp/monthreport_{$this->startDate}_{$this->endDate}_{$this->employeeID}.csv";
			$this->conn = $GLOBALS['conn'];
			$pg = new Page('hours');
			$this->page= $pg->page;
			if (isset($_REQUEST['search'])) {
				$this->search = true;
			}
         switch($action) {
				case "list" :
					$this->heading_text =   "Print Monthly CSV of Hours from $this->startDate to $this->endDate";
					$this->setContent();
					break;
			}
		 	$this->setHeaderText($this->heading_text);	
			echo $this->page;
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent() {
			$hour_table = "hour"; // default hour  table  status  3 lookups  need  hour_audit
			$URL="https://".$_SERVER['HTTP_HOST'] ;
			$totHours = $gTot = 0.0;
			$showApproval= false; //  If exact date with  and Well then offer approval button
			$CHECKED = "";
			$this->hourID = NULL;
			$dateSelected = false;
			$_SESSION['last_request'] = "$URL/admin/monthcsv.php?action=list&startdate=$this->startDate&enddate=$this->endDate";

			//$whereClause="where h.removed is false and h.well_id = w.well_id ";
			if ($this->search == true) {
				$this->getAllHours();
				$this->search=true;
			}
          
			$content ="<div style=\"width:910px;margin:auto;\">";
			// search
			$content .= <<<FIN
         <form name="hour" method="post" action="monthcsv.php?action=list">
			<fieldset ><legend style="margin-left:44%;">Print Monthly C.S.V.</legend>
			<div class="div12 marg2" > <label for="sdate" >Start Date<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
			<div class="div12 marg10" > <label for="edate" >End Date<input type="text" name="enddate" id="edate" class="required date" value="$this->endDate"  /></label></div>
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
      		$content .= "<div style=\"float:right;width:35%;\" >\n";
				$content .= "<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\" />\n";
				if ( $this->search) {
               $content .= "<a href=\"monthcsv.php?action=list&printcsv=true&file=$this->file\" class=\"button oran\" style=\"margin-left:5%;\" >Print C.S.V.</a>";
   			}
				$content .= "</div>";
				$content .= "</fieldset></form>";

			// Page  totals
				$totHours = number_format($this->totHours,2);
         	$gTot = number_format($this->gTot,2);
         	if ($this->search) {
            	$content .= "<div class=\"heading\" ><div class=\"well hd\"  >Time Total</div><div class=\"well hd bdr\" >Total</div></div>\n";
            	$content .= "<div class=\"well cntr padtop\"  >$totHours</div>\n";
            	$content .= "<div class=\"well cntr bdr padtop\" >$gTot</div>\n";
            	//$content .="<div style=\"clear:both;\"></div>\n";
            	$content .= "<hr />";
         	}      

			$content .= "</div>\n";
			$this->page = str_replace('##MAIN##',$content,$this->page);

		}
		private function getAllHours() {
			$this->conArr = AdminFunctions::getAllCon();
			$gTot = $totHours = 0;
			$whereClause = " WHERE hour_date between '$this->startDate' and '$this->endDate' ";
			foreach($this->conArr as $ind=>$val) {
				extract($val);
            $contractorArr = AdminFunctions::getConName($contractor_id);
            $this->shortName = $contractorArr['name'];
            //$this->dbLink = $connArr['db_link'];
				$sql = "SELECT h.*,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,pt.p_type,plant_unit,wells_from_ids(h.well_ids) as well_name,a.area_name,c.crew_name,sub_crew_name
						 from {$this->shortName}_hour h
						LEFT JOIN {$this->shortName}_plant p using (plant_id)
						LEFT JOIN plant_type pt using (plant_type_id)
						LEFT JOIN area a using (area_id)
						LEFT JOIN {$this->shortName}_employee e using (employee_id)
						LEFT JOIN crew c  using (crew_id)
						LEFT JOIN sub_crew sc  using (sub_crew_id)
						$whereClause
                  order by hour_date";

				if (! $rs=$this->conn->Execute($sql)) {
					if($this->conn->ErrorNo() != 0 ) {
				   	die($this->ErrorMsg());
					}
				}     
				while (! $rs->EOF ) {
					$this->csv['Contractor'][] = $con_name; 
					$total = null;
					$coo = $rs->fields['calloff_order_id']; 
					$status = $rs->fields['status'];
					$this->csv['Status_Txt'][] = $this->statusText[$status];
					$this->csv['Reject_Txt'][] =  strlen($rs->fields['reject_text']) > 0 ? $rs->fields['reject_text'] : "";;
					$hourID = $rs->fields['hour_id'];
					$operator = $rs->fields['opname'];
					$this->csv['Operator'][] = $operator;
					$jDate = AdminFunctions::dbdate($rs->fields['hour_date']);
					$this->csv['Date'][] = $jDate;
					$eTime = AdminFunctions::dbTime($rs->fields['enter_time']);
					$this->csv['EnterTime'][] = $eTime;
					$total_t1 = floatval($rs->fields['total_t1']);
					$total_t2 = floatval($rs->fields['total_t2']);
					$expense = floatval($rs->fields['expense']);
					$dktHours_t1 = $rs->fields['docket_hours_t1']; 
					$dktHours_t2 = $rs->fields['docket_hours_t2']; 
					$dktHours = $dktHours_t1 > 0 ? sprintf("%'.05.2f",$dktHours_t1) : sprintf("%'.05.2f",$dktHours_t2);
					if ($total_t1 > 0 ) {
						$total = sprintf('%01.2f',$total_t1);
					}
					else if ($total_t2 > 0 ) {
						$total = sprintf('%01.2f',$total_t2);
					}
					else if ($expense > 0 ) {
						$total = sprintf('%01.2f',$expense);
					}
					$this->gTot += $total;
					$this->totHours += $dktHours;
					$this->csv['DocketHours'][] = $dktHours;
					$dktNum = $rs->fields['docket_num']; 
					$this->csv['Docket'][] = $dktNum;
					$unit = $rs->fields['plant_unit'];
					$this->csv['Unit'][] = $unit;
					$rate = sprintf('%01.2f',$rs->fields['rate']);
					$this->csv['Rate'][] = $rate;
					$this->csv['Total'][] = $total;
					$area = $rs->fields['area_name'];
					$this->csv['Area'][] = $area;
					$well = $rs->fields['well_name'];
					$this->csv['Well'][] = $well;
					$crew = $rs->fields['crew_name'];
					$this->csv['Crew'][] = $crew;
					$details = $rs->fields['details'];
					$this->csv['Detail'][] = $details;
					$details = $status != 2 ? $details : $rs->fields['reject_text'];
					$invoice = trim($rs->fields['invoice_num']);
					$pType = $rs->fields['p_type'];
					$this->csv['COO'][] = $coo;
					$this->csv['PlantType'][] = $pType;
					$subScope = !empty($rs->fields['sub_crew_name']) ? $rs->fields['sub_crew_name'] : "";
					$this->csv['subScope'][] = $subScope;
					$rs->MoveNext();
				}  


			}
			$this->printCSV();


		}
		private function printCSV() {
         $fp = fopen($this->file,'w');
         $headings = array('Contractor','Date','Enter Time','C.O.O.','Operator','Docket Hours','Docket','Unit','Plant/Type','Rate','Total','Area','Well/s','Detail','Work Scope','Sub Scope','Status','Reject Reason');
			$title = array("$this->heading_text");
        	fputcsv($fp,$title);
			$lineCount=2;
         fputcsv($fp,$headings);
			if (!empty($this->csv['Date'])  ) {
         	foreach($this->csv['Operator'] as $ind =>$val) {
            	$tmpArr= array($this->csv['Contractor'][$ind],$this->csv['Date'][$ind],$this->csv['EnterTime'][$ind],$this->csv['COO'][$ind],$this->csv['Operator'][$ind],$this->csv['DocketHours'][$ind],$this->csv['Docket'][$ind],$this->csv['Unit'][$ind],$this->csv['PlantType'][$ind],$this->csv['Rate'][$ind], $this->csv['Total'][$ind],$this->csv['Area'][$ind],$this->csv['Well'][$ind],$this->csv['Detail'][$ind],$this->csv['Crew'][$ind],$this->csv['subScope'][$ind],$this->csv['Status_Txt'][$ind],$this->csv['Reject_Txt'][$ind]);
			//var_dump($tmpArr);
			//exit;
         		fputcsv($fp,$tmpArr);
					$lineCount ++;
         	}
				fputcsv($fp,array("","","","","",'=ROUND(SUM(F3:F'.$lineCount.'),2)',"","","",'=ROUND(SUM(K3:K'.$lineCount .'),2)'));
			}
         fclose($fp);
      }
	}
?>
