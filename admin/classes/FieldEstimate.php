<?php //QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class FieldEstimate  {
		public $page;
		public $requestID;
		public $siteInsNo;
		public $contractorID = NULL;
		public $fieldEstID;
		public $empID;
		public $empName;
		public $action;
		public $sessionProfile;
		public $conn;
		public $wellID;
		public $crewName;
		public $subCrewName;
		public $wellName;
		public $areaID;
		public $areaName;
		public $hoursTot;
		public $tot;
		public $fieldLines;	
		public $photoLines;
		public $equalto;
      public $between;
		public $less;
      public $hideEndDate='style="display:none;"';
		public $equalgreater;
		public $creDate;
		public $endDate;
		public $contractorName;
      public $shortName;
      public $conDomain;
		public $comments;
		public $qgcApproved;
		public $estType="cost";
		public $costLabel;
		public $formNumber;
		public $approvalDate;
		public $URL;
		public $conSignature;
      public $conSigDate;
      public $conSigName;
		public $qgcSignee;
		public $qgcApprDate;
		public $qgcSignature;

		public function	__construct($action,$fieldEstID=0,$conID=NULL,$search=NULL,$redo=NULL) {
			$this->URL="https://".$_SERVER['HTTP_HOST'];
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->contractorID = $conID;
			if (! is_null($conID)) {
            $connArr = AdminFunctions::getConName($conID);
            $this->contractorName = $connArr['con_name'];
            $this->shortName = $connArr['name'];
            $this->conDomain = $connArr['domain'];
         }

			$this->fieldEstID = $fieldEstID;
			if ($this->fieldEstID > 0 ) {
            $this->getFieldEstDetails($fieldEstID);
         }
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);

			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('cost');
				$this->page= $pg->page;
				//$heading_text = "Contractor: $this->contractorName ";
         	switch($action) {
					case "list" :
						$heading_text =   "List Field Estimates";
						$this->getAllEstimates($search,$redo) ;
						break;
					case "find" : 
						if( $this->estType == "cost" ) {
            			$this->costLabel =  "Cost Request No.";
            			$this->formNumber = $this->requestID;
            			$headText = " For Target Cost Estimate $this->formNumber";
         			}
         			else {
            			$this->costLabel = "Site Instr No.";
            			$this->formNumber = $this->siteInsNo;
            			$headText = " For Site Instruction No. $this->formNumber";
         			}
						$heading_text = "Estimate $this->fieldEstID: $headText";
						break;
					default:
						$heading_text = "List Field Estimate";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->requestID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Estimate";
						break;
				}
			$content = <<<FIN
<div style="width:1360px;margin:auto;">
<form name="field" id="field" method="post" action="field_estimate.php"  enctype="multipart/form-data">
<input type="hidden" name="requestID" value="$this->requestID" id="request_id" />
<input type="hidden" name="empID" id="emp_id" value="$this->empID" />
<input type="hidden" name="fieldEstID" value="$this->fieldEstID" id="field_id" />
<input type="hidden" name="contractorID" id="con_id" value="$this->contractorID" />
<input type="hidden" name="est_type" id="est_type" value="$this->estType" />
<input type="hidden" name="site_ins_id" id="site_ins_id" value="$this->siteInsID" />
<fieldset ><legend style="margin-left:41%;">Field Cost Estimate - $this->contractorName</legend>
<div class="div14" ><label >Work Scope<input type="text" id="crew" value="$this->crewName"  disabled /></label></div>
<div class="div14 marg1" ><label>Sub Scope<input type="text" id="sub_crew" value="$this->subCrewName"  disabled /></label></div>
<div class="div14 marg1" ><label for="area" >Area<input type="text" id="area" value="$this->areaName"  disabled /></label></div>
<div class="div29 marg1" ><label for="well" >Well/s<input type="text"  id="well" value="$this->wellName"  disabled /></label></div>
<div class="div9 marg1" ><label for="req_id" >$this->costLabel<input type="text"  id="req_id" value="$this->formNumber"  disabled /></label></div>
<div class="div7 marg1" ><label>Units<input type="text" id="sub_hours" name="subhours"  READONLY value="$this->hoursTot" class="txtr" /></label></div>
<div class="div700 marg1"  ><label>Total<input type="text" id="total"  name="ttal" class="txtr"  READONLY value="$this->tot"/></label></div>
</fieldset>
<div style="clear:both;" ></div>
<script>
$("#cost").submit(function(){
    var isFormValid = true;
    $("#field input:text.input.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#field .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
	 if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
	 else {
		return true;
	 }
});
</script>
<div class="heading" >
<div class="comments hd nopad" >Plant Type</div
><div class="comments hd nopad">Machine</div>
<div class="usr hd nopad" >Rate</div>
<div class="vehicle hd nopad"> Estimate of Units</div>
<div class="usr hd nopad"  >Sub Total</div>
<div class="links hd nopad" style="width:208px;" >Action</div>
</div>
<div id="lines" >
FIN;
	$cnt = count($this->fieldLines);
	for ($x = 0;$x<$cnt;$x++) {
		$id=0;
		$ptype=$mach=$rate=$hours=$sub= "";
		$delete = false;
		$link = " &nbsp;";
		if (isset($this->fieldLines[$x]['plant_type']) && strlen($this->fieldLines[$x]['plant_type'])> 0 ) {
			$wash = "";
			$id = $this->fieldLines[$x]['field_estimate_line_id'];
			$ptype = $this->fieldLines[$x]['plant_type'];
			$mach = $this->fieldLines[$x]['machine'];
			$rate =  sprintf("%6s",$this->fieldLines[$x]['rate']);
			$hours = sprintf("%6s",$this->fieldLines[$x]['hours']);
			$sub = number_format($this->fieldLines[$x]['sub_total']);
			if ($this->fieldLines[$x]['washdown'] == "t") {
            $wash =  "lte";
         }

			// find 
			$content .= <<<ENDLOOP
<div class="comments nopad" ><input type="text" class="$wash" id="ptype_$x" name="upType[]"  value="$ptype" $DISABLED  READONLY /></div>
<div class="comments nopad" ><input type="text" id="mach_$x" name="umachine[]" class="$wash" READONLY value="$mach" /></div>
<div class="usr nopad"  ><input type="text" id="rate_$x" name="urate[]" class="$wash txtr" READONLY  value="$rate" /></div>
<div class="vehicle nopad"><input type="text" id="hours_$x" name="uhourEst[]" value="$hours" class="$wash txtr" onChange="calcrate('#rate_$x','#hours_$x','#sub_$x');" $DISABLED /></div>
<div class="usr nopad"  ><input type="text" id="sub_$x" name="usub[]" class="$wash txtr" READONLY value="$sub" /></div>
<div class="links line1 nopad"   style="width:208px;"  >$link</div>
ENDLOOP;
		}
	}
	$content .= <<<FIN
</div>
<div style="clear:both;" ></div>
<div class="heading"  >
<div class="location hd" style="width:738px;" >Files Uploaded</div>
<div class="location hd bdr" style="width:613px;" >Comments</div>
</div>
<div style="clear:both;" ></div> 
<div style="float:left;width:738px;" >
FIN;
		$pcount = count($this->photoLines);
		if ($pcount > 0 ) {
			$pcount = $pcount >= 2 ? $pcount : 2;
			$photo="&nbsp;";
			for ($x = 0;$x<$pcount;$x++) {
				if (isset($this->photoLines[$x]['upload_name'])) {
					if (strlen($this->photoLines[$x]['upload_name'])> 0 ) {
						$file = $this->photoLines[$x]['upload_name'];
						$id = $this->photoLines[$x]['upload_id'];
						$photo = "<a href=\"$this->conDomain/{$file}\" style=\"margin-top:0.5rem;display:block\" target=\"_blank\" >$file</a>\n";
					}
				}
				else {
					$photo="&nbsp;";
				}


			//$content .= "<input type=\"text\" class=\"input ptype\" style=\"margin-left:0px;\"  />\n";
				$content .= "<div class=\"location line2 bdb bdr\"  style=\"width:738px;\" >$photo</div>\n";
				//$content .= "<div style=\"clear:both;\" ></div> \n";
			}		
		}
		else {
				$content .= "<div class=\"location line2 bdb bdr\" style=\"border-bottom:1px solid #000000;height:4.75rem;width:738px;\" > &nbsp;</div>\n";

		}
		$content .= "</div>\n";
      $content .= "<div style=\"float:right;width:617px;\"><textarea class=\"txt\" style=\"width:100%;height:4.75rem;\" READONLY name=\"comments\" >$this->comments</textarea></div>\n";
		$content .= "<div style=\"clear:both;\" ></div> \n";
		if (! $this->qgcApproved ) {
			$appr = "<label for=\"approve\" >Approve</label>\n";
			$appr .= "<input type=\"checkbox\"  name=\"approve\" id=\"approve\" value=\"true\" onclick=\"approveFieldEst();\" />\n";
		}
		else {
			$appr = "<span >APPROVED</span>\n";
		}
		$content .= <<<FIN
		<fieldset ><legend style="margin-left:47%;">Signed</legend>
		<div class="div50">
         <div class="div40" ><img src="{$this->conDomain}/$this->conSignature" height="180" width="300" /></div>
         <div class="div15 marg2" ><label for="apprdate" >Date<input type="text" name="insdate" id="insdate" class="date required"  value="$this->conSigDate" $DISABLED /></label></div>
         <div class="div33 marg2" ><label  >Signed for Contractor<input type="text" name="signee" id="signee" value="$this->conSigName" readonly /></label></div>
</div>
<div class="div50">
         <div class="div40" style="min-width:40% !important;height:12rem;" ><img src="$this->qgcSignature"  alt="QGC to Sign"  height="180" width="300" /></div>
			<div class="div60" >
         <div class="div25 marg2" ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"   value="$this->qgcApprDate" disabled/></label></div>
         <div class="div65 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->qgcSignee"  disabled /></label></div>
			<div class="div90 marg3" >$appr</div>
			</div>
</div>
		</fieldset>
FIN;
				$url = isset($_SESSION['last_request']) ? $_SESSION['last_request'] : "$this->URL/admin/cost_estimate.php?action=list&redo=redo";
            $content .= "\n<a href=\"$url\"   class=\"button\"  >Last Screen</a>";

				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllEstimates($search,$redo) {
		  	$whereClause=" where fe.removed is false";
			$this->equalgreater = "SELECTED=\"selected\"";

			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if (intval($search->fieldEstID) > 0 ) {
               $this->fieldEstID = $search->fieldEstID;
               $whereClause .= " and field_estimate_id = $this->fieldEstID ";
            }
            else if ( intval($search->requestID) > 0) {
               $this->requestID = $search->requestID;
               $whereClause .= " and fe.request_estimate_id = $this->requestID ";
            }
				 else {
               if (strlen($search->creDate) > 0) {
                     $dateSelected = true;
                     $whereClause .= " and create_date ";
                     $this->creDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
								$this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
								   $this->equalgreater = NULL;
                           $this->endDate = $search->endDate;
                           $this->hideEndDate='';
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause .= "  =  current_date ";
                        $this->equalto = "SELECTED=\"selected\"";
                        break;
                  }
               }
					if ($search->areaID > 0 ) {
                  $whereClause .= " and fe.area_id = $search->areaID ";
                  $this->areaName = $search->areaName;
                  $this->areaID = $search->areaID;
               }
               if ($search->wellID > 0 ) {
                  $whereClause .= " and fe.well_id = $search->wellID ";
                  $this->wellID = $search->wellID;
               }
			   }
         }
         else {
             // Default query still needs search object serialized
            $this->creDate = date ('d-m-Y');
            $whereClause .= " and create_date >= current_date";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            $search->dateType = "equalgreater";
            $search->creDate = $this->creDate;
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
         }
			$sql = "SELECT fe.*,a.area_name, wells_from_ids(re.well_ids) as well_name from field_estimate fe 
			LEFT JOIN area a using (area_id) 
			LEFT JOIN request_estimate re using(request_estimate_id) 
			$whereClause
			order by create_date";

			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}


 			$content ="<div style=\"width:1800px;text-align:center;\"><div style=\"margin:auto;width:1410px;\" >";
			if ($this->fieldEstID == 0 ) {
				$this->fieldEstID = null;
			}
			if ($this->requestID == 0 ) {
				$this->requestID = null;
			}
         // search
         $content .= <<<FIN
         <form name="field" method="post" action="field_estimate.php?action=list">
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
<fieldset style="margin-bottom:10px;width:1400px;"><legend style="margin-left:600px;">Filter Search</legend>
<div class="div300" ><label for="field_id" class="label wide">Field Estimate ID</label><input type="text" name="field_id" id="field_id" class="input abn" value="$this->fieldEstID" /></div>
<div class="div300" ><label for="req_id" class="label wide">Cost Request ID</label><input type="text" name="req_id" id="req_id" class="input abn" value="$this->requestID" /></div>
<div class="div300" ><label for="datetype" class="label smll" >Date is:</label><select class="sel" name="datetype" id="datetype"  onchange="displayDiv('#datetype');">
<option value="equalgreater" $this->equalgreater >Equal or greater</option>
<option value="equalto" $this->equalto >Equal to</option>
<option value="between" $this->between >Between</option>
<option value="less" $this->less >Less than</option>
</select></div>
<div class="div250" ><label for="cre_date" class="label abn">Date</label><input type="text" name="cre_date" id="cre_date" class="input date" value="$this->creDate" /></div>
<div class="div250" id="enddiv" $this->hideEndDate ><label for="end_date" class="label">and Date</label><input type="text" name="end_date" id="end_date" class="input date" value="$this->endDate" /> </div>
<div style="clear:both;height:10px;"> </div>
<div class="div300" ><label for="area" class="label ninety">Area</label><input type="text" name="area" id="area" class="input mobile" value="$this->areaName" /></div>
<div class="div300" ><label for="well" class="label abn ">Well</label><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
   $(".input.date").scroller({preset: 'date',dateFormat: 'dd-mm-yy',dateOrder: 'ddmmyy',display: 'bubble',mode: 'mixed'});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv')); return false; }
      });
  });
</script>
FIN;
         //$content .= "<div class=\"div250\" ><label for=\"crew\" class=\"label abn\">Crew</label>";
        // $content .= AdminFunctions::crewSelect($this->crewID,"",true);
         //$content .= "</div>\n";
			$content .= "<div style=\"float:right;width:15%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"search\"  class=\"submitbutton tiny\"/>";
         $content .= "\n<button class=\"reset\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >RESET</button>";
         //$content .= "<a href=\"hours.php?action=list&search=true&printcsv=true\" class=\"submitbutton printcsv\" >Print C.S.V.</a>";
         $content .= "</div></fieldset></form></div></div>\n";


			$content .= "<div style=\"width:1800px;text-align:center;\"><div style=\"width:1105px;margin:auto;\">";
			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" style=\"width:1103px\">\n";
			$content .= "<div class=\"vehicle hd\">Create Date</div>";
			$content .= "<div class=\"crew hd\">Field Req Num</div>\n";
			$content .= "<div class=\"crew hd\">Request Num</div>\n";
			$content .= "<div class=\"vehicle hd\">Area</div>\n";
			$content .= "<div class=\"cli hd\">Well/s</div>\n";
			$content .= "<div class=\"vehicle hd\">Tot Hours</div>\n";
			$content .= "<div class=\"vehicle hd\">Total Est</div>\n";
			$content .= "<div class=\"links hd\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$field_id = $rs->fields['field_estimate_id'];
				$req_id = $rs->fields['request_estimate_id'];
				$area = $rs->fields['area_name'];
				$well = $rs->fields['well_name'];
				$creDate = AdminFunctions::dbDate($rs->fields['create_date']);
				$totHours = $rs->fields['total_hours'];
				$tot = $rs->fields['estimate_total'];
				$content .= "<div class=\"vehicle $ln\">$creDate</div>";
				$content .= "<div class=\"crew $ln\">$field_id</div>\n";
				$content .= "<div class=\"crew $ln\">$req_id</div>\n";
				$content .= "<div class=\"vehicle $ln\">$area</div>\n";
				$content .= "<div class=\"cli $ln\">$well</div>\n";
				$content .= "<div class=\"vehicle $ln\">$totHours</div>\n";
				$content .= "<div class=\"vehicle $ln\">$tot</div>\n";
				$content .= "<div class=\"links $ln\" > &nbsp;</div>";
				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "</div></div><hr />";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getEstimateDetails($requestID) {
			$sql = "SELECT r.area_id,r.well_id,a.area_name,wells_from_ids(r.well_ids) as well_name 
			from request_estimate r 
			LEFT JOIN area a using (area_id) 
			where request_estimate_id = $requestID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->areaID = $data['area_id'];
			$this->areaName = $data['area_name'];
			$this->wellName = $data['well_name'];

		}
		private function getFieldEstDetails($fieldEstID) {
			$sql = "SELECT fe.*,a.area_name,c.crew_name,sc.sub_crew_name,wells_from_ids(fe.well_ids) as well_name ,si.instruction_no
			from {$this->shortName}_field_estimate fe
		   LEFT JOIN area a using(area_id) 
		   LEFT JOIN crew c  using(crew_id) 
		   LEFT JOIN sub_crew sc  using(sub_crew_id) 
			LEFT JOIN request_estimate re  using (request_estimate_id)
			LEFT JOIN {$this->shortName}_site_instruction si  using (site_instruction_id)
			where fe.field_estimate_id = $fieldEstID";
			if (! $data = $this->conn->getRow($sql)) {
				echo $sql;
				die($this->conn->ErrorMsg());
			}
			$this->requestID = $data['request_estimate_id'];
			$this->siteInsID = $data['site_instruction_id'];
			if (intval($this->requestID) > 0 ) {
				$this->estType="cost";
			}
			else if (intval($this->siteInsID) > 0 ) {
				$this->estType="site";
				$this->siteInsNo = $data['instruction_no'];
			}
		//	$this->wellID = $data['well_id'];
			$this->wellName = $data['well_name'];
		//	$this->areaID = $data['area_id'];
			$this->areaName = $data['area_name'];
			$this->crewName = $data['crew_name'];
			$this->subCrewName = $data['sub_crew_name'];
			$this->hoursTot = number_format($data['total_hours']);
         $this->tot = number_format($data['estimate_total']);
			$this->comments = $data['comments'];
			$this->qgcApproved = strlen($data['qgc_signee']) > 0 ? true :false ;
			$this->empName = $data['qgc_signee'];
			$this->qgcApprDate = AdminFunctions::dbDate($data['qgc_approve_date']);
			$this->qgcSignature = $data['qgc_sig'];
			$this->qgcSignee = $data['qgc_signee'];
			$this->conSignature = $data['contractor_sig'];
         $this->conSigDate = $data['contractor_sig_date'];
         $this->conSigName = $data['contractor_signee'];


		// lines
			$sql = "SELECT * from {$this->shortName}_field_estimate_line where field_estimate_id = $fieldEstID  order by machine,field_estimate_line_id";
			if (! $this->fieldLines = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
			// photos
			$sql = "SELECT upload_id,upload_name from {$this->shortName}_upload where field_estimate_id = $fieldEstID";
			if (! $this->photoLines = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
		}

	}
?>
