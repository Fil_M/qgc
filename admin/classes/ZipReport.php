<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
class ZipReport  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $areaName;
		public $wellName;
		public $areaID;
		public $wellID;
		public $callOffID;
		public $zipName;
		public $zipDir;
		public $conArr;
		public $shortNameArr;
		public $URL;
		public $employeeID;

		public function	__construct($action,$conID=0,$areaID=NULL,$wellID=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
		 	$this->contractorID=$conID;
		 	$this->areaID=$areaID;
		 	$this->wellID=$wellID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
			$this->conn = $GLOBALS['conn'];
			$this->URL = "https://".$_SERVER['HTTP_HOST'];

			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			else {
				$pg = new Page('zipreport');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Create Zip for Well";
					$button = "<input type=\"button\" name=\"SEARCH\" value=\"Search\" class=\"button marg2\"  onClick=\"showDiv();return false;\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}
		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1000px;margin:auto;">
<form name="zipreport" id="zipreport" method="post" action="zipreport.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="employee_id" id="empID" value="$this->employeeID" />
<input type="hidden" name="area_id" id="areaID" value="$this->areaID" />
<input type="hidden" name="well_id" id="wellID" value="$this->wellID" />
<fieldset ><legend style="margin-left:44%;">Filter Search - Well Zip</legend>
<div class="div20">
FIN;
   $content .= AdminFunctions::contractorSelect($this->contractorID,"",true);
	$content .= <<<FIN
</div>
<div class="div25 marg2" ><label >Area:<input type="text" name="area" id="area" class="required" value="$this->areaName" /></label></div>
<div class="div22 marg1" ><div id="welldiv"  >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<div style="float:right;width:25%;" > 
<button class="reset" style="margin-top:0.5rem;" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div>
<div style="clear:both;" ></div>
	<div id="con_div" ></div>
</fieldset>
</form>
</div>
<script>
$("#zipreport").submit(function(){
   var isFormValid = true;
	var conSelected = false;
   $("#zipreport input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#zipreport .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
	 $('.conclick').each(function(i, obj) {
         if($(this).is(':checked')) {
			 conSelected = true;
			}
     });
	  if (!conSelected) {
		 alert('Please Select at least One Contractor to Download');
		 return false;
	  }

    if (!isFormValid) {
		 alert("Please fill in all the required fields (highlighted in red)");
	 }
    return isFormValid;
	 
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),$('#contractor')); return false; }
      });
  });
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function processPost()  {
			global $BASE;
			$sql = "Delete from batch where create_date <= current_date - Interval '2 days'";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			$sql = "SELECT contractor_id ,name from contractor where removed is false";
			if (!$this->shortNameArr = $this->conn->getAssoc($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->employeeID = $_POST['employee_id'];
			$this->conArr = $_REQUEST['concheck'];  // array of selected contractors
			$well = $_REQUEST['well'];
			$this->wellID = $well[0];  // is  an  array
			$area = $_REQUEST['area'];
			$this->areaID = $_REQUEST['area_id'];
			$this->setUpZip($area,$this->wellID,$this->conArr);  // sets  area  and well names
			$this->findCallOffs();
         exec("{$BASE}batch_zipreport $this->employeeID  $this->areaName $this->wellName >/dev/null 2>&1  &");
			header("LOCATION: index.php");
			exit;

		}
		private function findCalloffs() {
			// completions have to be separate as some are partial
			foreach($this->conArr as $ind=>$conID) {
				$shortName = $this->shortNameArr[$conID];
				$sql = "select co.contractor_id,co.calloff_order_id,request_estimate_id,site_instruction_id,instruction_no
				from calloff_order co
				LEFT JOIN {$shortName}_site_instruction si on si.calloff_order_id = co.calloff_order_id
				where co.contractor_id = $conID 
				and co.area_id = $this->areaID  and $this->wellID in (select (unnest(string_to_array(co.well_ids::text,'|')::int[])))";
				if (! $data = $this->conn->getAll($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->conn->ErrorMsg());
					}
				}
				$this->loadBatch($data,$shortName);
			}
		}
		private function loadBatch($data,$shortName) {
			$currDir = getcwd();
			// Calloffs and CSV + Expenses
			$sql = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) values ";
			$sql1 = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) values ";
			$sql2 = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) values ";
			$cnt = 1;
			foreach($data as $ind=>$val) {
				extract($val);
				$fileName = "{$currDir}/{$this->zipDir}/{$shortName}/CallOff_Order/CallOff_Order_{$calloff_order_id}.pdf";
				$sql .= "($contractor_id,$calloff_order_id,$calloff_order_id,'$fileName','$this->zipDir','CALLOFF','$this->areaName','$this->wellName'),";
				$fileName2 = "{$currDir}/{$this->zipDir}/{$shortName}/Expenses/USE_NAME_FROM_UPLOAD";   // will be  entered  twice but only made once need all CallOffs
				$sql2 .= "($contractor_id,$calloff_order_id,$calloff_order_id,'$fileName2','$this->zipDir','EXPENSE','$this->areaName','$this->wellName'),";
				if ($cnt == 1 ) {
					$fileName1 = "{$currDir}/{$this->zipDir}/{$shortName}/CSV/{$this->areaName}_{$this->wellName}.csv";   // will be  entered  twice but only made once need all CallOffs
					$sql1 .= "($contractor_id,$calloff_order_id,$calloff_order_id,'$fileName1','$this->zipDir','CSV','$this->areaName','$this->wellName'),";
				}
				$cnt ++;

			}
			$sql = preg_replace('/,$/',"",$sql);
			$sql1 = preg_replace('/,$/',"",$sql1);
			$sql2 = preg_replace('/,$/',"",$sql2);

			if (!$this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			if (!$this->conn->Execute($sql1)) {
				die($this->conn->ErrorMsg());
			}
			if (!$this->conn->Execute($sql2)) {
				die($this->conn->ErrorMsg());
			}
			//TCE and associated Field Estimates
			$sql = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) values ";
			foreach($data as $ind=>$val) {
				extract($val);
				$fileName = "{$currDir}/{$this->zipDir}/{$shortName}/TCE/Target_Cost_Estimate_{$request_estimate_id}.pdf";   // TCE
				$sql .= "($contractor_id,$calloff_order_id,$request_estimate_id,'$fileName','$this->zipDir','TCE','$this->areaName','$this->wellName'),";
				$fileName = "{$currDir}/{$this->zipDir}/{$shortName}/TCE/Field_Estimate_{$request_estimate_id}.pdf";   // TCE
				$sql .= "($contractor_id,$calloff_order_id,$request_estimate_id,'$fileName','$this->zipDir','TCE_FIELD','$this->areaName','$this->wellName'),";

			}
			$sql = preg_replace('/,$/',"",$sql);

			if (!$this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
		 	//Site	 Instruction  // And associated  Field Estimates
			foreach($data as $ind=>$val) {
				extract($val);
				if (intval($site_instruction_id) > 0 ) {
					$fileName = "{$currDir}/{$this->zipDir}/{$shortName}/Site_Instruction/Site_Instruction_{$instruction_no}.pdf";
					$sql = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) 
					values ($contractor_id,$calloff_order_id,$site_instruction_id,'$fileName','$this->zipDir','SITE','$this->areaName','$this->wellName')";
					if (!$this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
					$fileName = "{$currDir}/{$this->zipDir}/{$shortName}/Site_Instruction/Field_Estimate_{$instruction_no}.pdf";
					$sql = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) 
					values ($contractor_id,$calloff_order_id,$site_instruction_id,'$fileName','$this->zipDir','SITE_FIELD','$this->areaName','$this->wellName')";
					if (!$this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
				}

			}
			// Completion Certs including Partial + Invoice
			foreach($data as $ind=>$val) {
				extract($val);
				$sql1 = "SELECT completion_cert_id from {$shortName}_completion_cert where calloff_order_id = $calloff_order_id";
				if (!$compData = $this->conn->getAll($sql1)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->conn->ErrorMsg());
					}
				}
				if (count($compData) > 0 ) {
					foreach($compData as $ind=>$val) {
						extract($val);
						$fileName = "{$currDir}/{$this->zipDir}/{$shortName}/Completion_Certificate/Completion_Certificate_{$completion_cert_id}.pdf";
						$sql = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) 
						values ($contractor_id,$calloff_order_id,$completion_cert_id,'$fileName','$this->zipDir','COMPLETION','$this->areaName','$this->wellName')";
						if (!$this->conn->Execute($sql)) {
							die($this->conn->ErrorMsg());
						}
						$fileName1 = "{$currDir}/{$this->zipDir}/{$shortName}/Invoice/USE_NAME_FROM_UPLOAD";
						$sql1 = "INSERT into batch (contractor_id,calloff_order_id,table_id,filename,zipdir,batch_type,area_name,well_name) 
						values ($contractor_id,$calloff_order_id,$completion_cert_id,'$fileName1','$this->zipDir','INVOICE','$this->areaName','$this->wellName')";
						if (!$this->conn->Execute($sql1)) {
							die($this->conn->ErrorMsg());
						}

					}

				}
			}
			
		}

		private function setUpZip($area_name,$wellID,$conArr) {
			$currDir = getcwd();
			//echo $currDir;
			$dat = date('Ymd');
			$sql = "SELECT well_name from well where well_id = $wellID";
			if (!$well_name = $this->conn->getOne($sql)) {
				die($this->conn->ErrorMsg());
			}
			$areaName = preg_replace('/\ /',"_",$area_name);
			$wellName = preg_replace('/\ /',"_",$well_name);
			$this->wellName = $wellName;
			$this->areaName = $areaName;
			$this->zipDir = "zips/{$areaName}_{$wellName}_{$dat}";
			//echo $this->zipDir;
			$this->zipName = "{$areaName}_{$wellName}_{$dat}.zip";
			if (file_exists($this->zipDir)) {
				AdminFunctions::rrmdir($this->zipDir);
			}
			mkdir($this->zipDir);
			chdir($this->zipDir);
			foreach($conArr as $ind=>$conID) {
				$name = $this->shortNameArr[$conID];
				//echo $name;
				mkdir($name);
				mkdir("{$name}/Site_Instruction");
				mkdir("{$name}/Invoice");
				mkdir("{$name}/Expenses");
				mkdir("{$name}/CallOff_Order");
				mkdir("{$name}/TCE");
				mkdir("{$name}/Completion_Certificate");
				mkdir("{$name}/CSV");
			}
			chdir($currDir);

		}
		private function zipIt() {
			if(!AdminFunctions::Zip($this->zipDir,"tmp/{$this->zipName}",true)) {
				die("Can't Create Zip ???");
			}	

		}
}
?>
