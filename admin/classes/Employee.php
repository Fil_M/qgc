<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Employee extends Address {
		public $page;
		public $ID;
		public $firstName;
		public $lastName;
		public $userName;
		public $password;
		public $nokName;
		public $nokFone;
		public $profileID;
		public $address_type;
		public $action;
		public $sessionProfile;
		public $type;
		public $certificateObj;
		public $companyID = 1;

		public function	__construct($action,$type,$ID=0,$search="") {
			parent::__construct();
			$this->ID = $ID;
			$this->action = $action;
			$this->companyID = 2 ;
			if ($type == "Employee" ) {
				$this->address_type = "emp";
				$this->type = $type;
			}
			else {
				$this->address_type = "con";
				$this->type = "Sub Contractor";
			}
			//$this->certificateObj = new Certificate($this->address_type);
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('employee');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New $this->type";
						break;
					case "update" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text = "Updating $this->type $ID";
						$this->getEmployeeDetails($type,$ID);
						break;
					case "delete" :
						if ($ID < 1 ) {
							return false;
						}
						$heading_text = "Delete $this->type $ID";
						$this->getEmployeeDetails($type,$ID);
						break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List {$this->type}s - $search" : "List {$this->type}s - All";
						$this->getAllEmployees($type,$search) ;
						break;
					case "find" : 
						$heading_text = "$this->type $ID";
						$this->getEmployeeDetails($type,$ID);
						break;
					default:
						$heading_text = "Add New $this->type";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($type,$action);	
				echo $this->page;
			}
			else {
				$this->processPost($type);
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($type,$action) {
			if ($action != "list" || ($action == "find" && $this->ID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add $this->type";
						break;
					case "update":
						$button = "Update $this->type";
						break;
					case "delete":
						$button = "Delete $this->type";
						$DISABLED='disabled';
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add type";
						break;
				}
			$content = <<<FIN
<div style="width:1200px;margin:auto;">
<form name="employee" id="employee" method="post" action="employee.php?SUBMIT=SUBMIT">
<input type="hidden" name="id" value="$this->ID" />
<input type="hidden" name="prev_password" value="$this->password" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="type" value="$this->type" />
<fieldset ><legend style="margin-left:46.5%;">$this->type</legend>
<div class="div32" ><label for="firstname" >First Name<input type="text" name="firstname" id="firstname" value="$this->firstName" $DISABLED /></label></div>
<div class="div32 marg2" ><label for="lastname" >Last Name<input type="text" name="lastname" id="lastname" value="$this->lastName"  $DISABLED /></label></div>
<div class="div20 marg2" ><label for="profile" >Profile</label>
FIN;
			$content .= AdminFunctions::profileSelect($this->profileID,$DISABLED);
			$content .= <<<FIN
</div>
<div style="clear:both;"> </div>
<div class="div32" ><label for="username"  >User Name<input type="text" name="username" id="username" value="$this->userName" $DISABLED /></label></div>
<div class="div21 marg2" ><label for="password"  >Password<input type="text" name="password" id="password" value=""   $DISABLED /></label></div>
<script>
	$("#employee").submit(function(){
		$('#SUBMIT').prop("disabled", "disabled");
      return true;
});
</script>
FIN;
if ($this->type == "Employee" ) {
	$content .= "<div  class=\"div10 marg1\" ><label for=\"nokfone\"  >Next of Kin Phone<input type=\"text\" name=\"nokfone\" id=\"nokfone\"  value=\"$this->nokFone\"  $DISABLED /></label></div>\n";
	$content .= "<div  class=\"div31 marg2\" >\n";
	$content .= "<label for=\"nokname\" >Next of Kin Name<input type=\"text\" name=\"nokname\" id=\"nokname\"  value=\"$this->nokName\" $DISABLED /></label></div>";
}
			$content .= "</fieldset>\n";
			$content .= $this->addressDiv($DISABLED); 
			/*if ($this->type == "Employee" ) {
				$this->certificateObj->certificateDiv($type,$this->ID,$DISABLED);
				$content .= $this->certificateObj->content;
			}  */



				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for find employee 
				}
				else {
					$content .= "\n<a href=\"employee.php?action=list&type=Employee\"   class=\"button\" >Last Screen</a>";
				}


				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllEmployees($type,$search="") {
			//$whereClause=" and employee_id > 2 ";
			$whereClause=" ";
			if (strlen($search) > 0 ) {
				$whereClause .= " and upper(lastname) like '$search%' ";

			}
			switch($type) {
				case "Employee":
				$sql = "SELECT e.employee_id,e.firstname || ' ' || e.lastname as employee_name,e.username,e.next_of_kin_name,next_of_kin_fone,
				ad.address_line_1 || ' ' || ad.address_line_2 || ' ' || ad.suburb || ' ' || st.state_name as address,telephone,mobile,email
				from employee e 
				LEFT JOIN address_to_relation ar using (employee_id) 
				LEFT JOIN address ad using (address_id) 
				LEFT JOIN state st using (state_id)
				where e.removed='f'  and e.company_id in (0,2)  $whereClause  and employee_id > 2 order by e.lastname LIMIT 100";
				break;

				case "Contractor":
				$sql = "SELECT c.contractor_id,c.firstname || ' ' || c.lastname as employee_name,c.username,next_of_kin_name,next_of_kin_fone,
				ad.address_line_1 || ' ' || ad.address_line_2 || ' ' || ad.suburb || ' ' || st.state_name as address,telephone,mobile,email
				from contractor c 
				LEFT JOIN address_to_relation ar using (contractor_id) 
				LEFT JOIN address ad using (address_id) 
				LEFT JOIN state st using (state_id)
				where c.removed='f'  and c.company_id = $this->companyID $whereClause order by c.lastname LIMIT 100";
				break;
			}
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content = "<div style=\"width:1160px;margin:auto;\">\n";
			$content .= AdminFunctions::AtoZ("employee.php?type=$type&action=list",$search);
			$content .= "<div style=\"clear:both;height:20px;\"></div>\n";
			$content .= "<div class=\"heading\" >\n<div class=\"cli hd\">$type Name</div>\n<div class=\"usr hd\">Username</div>\n<div class=\"addr hd\">Address</div>\n";
			$content .= "<div class=\"fone hd\">Telephone</div>\n<div class=\"fone hd\">Mobile</div>\n";
			$content .= "<div class=\"cli hd\">Email</div>\n<div class=\"links hd\" style=\"width:119px;\"  >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln = $lineNo % 2 == 0  ? "line1" : "line2";
				if ($type == "Employee" ) {
					$id = $rs->fields['employee_id'];
				}
				else {
					$id = $rs->fields['contractor_id'];
				}
				$employee_name = strlen($rs->fields['employee_name']) > 2 ? $rs->fields['employee_name'] : " ";
				$user_name = strlen($rs->fields['username']) > 2 ? $rs->fields['username'] : " ";
				$user_name = $type == "Employee" ? $user_name : "";
				$address = strlen($rs->fields['address']) > 2 ? $rs->fields['address'] : " ";
				$telephone = strlen($rs->fields['telephone']) > 2 ? $rs->fields['telephone'] : " ";
				$mobile = strlen($rs->fields['mobile']) > 2 ? $rs->fields['mobile'] : " ";
				$email = strlen($rs->fields['email']) > 2 ? $rs->fields['email'] : " ";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"cli $ln highh\"> <a href=\"employee.php?type=$type&action=find&id=$id\" title=\"View $type\" style=\"color:black;\"  >$employee_name</a></div>";
            $content .= "<div class=\"usr $ln highh\">$user_name</div><div class=\"addr $ln highh\">$address</div><div class=\"fone $ln highh\">$telephone</div><div class=\"fone $ln highh\">$mobile</div>";
				$content .= "<div class=\"cli $ln highh\">$email</div>";
				if ($this->sessionProfile > 2 ) {
				$content .= "<div class=\"links $ln highh cntr\" style=\"width:119px;\"  >\n";
				$content .= "<div style=\"margin:auto;width:100px;\" >\n";
            $content .= "<a href=\"employee.php?type=$type&action=update&id=$id\" title=\"Edit $type\" class=\"linkbutton\" >EDIT</a>\n";
				$content .= "<a href=\"employee.php?type=$type&action=delete&id=$id\" title=\"Delete $type\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DELETE</a>\n";
				$content .= "</div></div>\n";
				}
				else {
					$content .="<div class=\"links $ln highh\" style=\"width:119px;\"  > &nbsp;</div>";
				}
				$content .= "</div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getEmployeeDetails($type,$ID) {
			if ($type == "Employee" ) {
				$sql = "SELECT e.*,adr.address_id from employee e LEFT JOIN address_to_relation adr using (employee_id)  where employee_id = $ID";
			}
			else {
				$sql = "SELECT c.*,adr.address_id from contractor c LEFT JOIN address_to_relation adr using (contractor_id)  where contractor_id = $ID";
			}

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			if ($type == "Employee" ) {
				$this->ID = $data['employee_id'];
			}
			else {
				$this->ID = $data['contractor_id'];
			}
			$this->firstName = $data['firstname'];
			$this->lastName = $data['lastname'];
			$this->userName =  $type == "Employee" ? $data['username'] : "";
			$this->password = $data['password'];
			$this->addressID = intval($data['address_id']);
			$this->profileID = intval($data['profile_id']);
			$this->nokName = $data['next_of_kin_name'];
      	$this->nokFone = $data['next_of_kin_fone'];


			$this->getAddressDetails($this->addressID,0);
		}
		private function processPost($type) {
         if ($this->action == "delete" ) {
				$this->deleteEmployee($type);
			}
			$this->ProcessAddressPost();
			$this->ID = $_POST['id'];
			$this->firstName = ucfirst(trim($_POST['firstname']));
			$this->lastName = ucfirst(trim(addslashes($_POST['lastname'])));
			$this->userName = trim($_POST['username']);
			$this->userName = strlen($this->userName) > 0 ? $this->userName : uniqid(true);
		 	$this->password =  strlen(trim($_POST['password'])) > 1  ? md5(trim($_POST['password'])) : $_POST['prev_password']; // use previous if not given
		 	$this->profileID =  intval($_POST['profile']);
			if($type == "Employee" ) {
				$this->nokName = trim(addslashes($_POST['nokname']));
				$this->nokFone = trim($_POST['nokfone']);
			}
			else {
				$this->nokName = "";
				$this->nokFone = "";
			}
         //$this->certificateObj->processCertificatePost();


			
			$this->submitEmployee($type);
		}
		private function deleteEmployee($type) {
			if ($type == "Employee" ) {
				$sql = "UPDATE employee set removed = TRUE where employee_id = $this->ID";
			}
			else {
				$sql = "UPDATE contractor set removed = TRUE where contractor_id = $this->ID";
			}
			if (! $this->conn->Execute($sql)) {
				die( $this->conn->ErrorMsg());
			}
			header("LOCATION: employee.php?type=$type&action=list");
			exit;
		}
		private function submitEmployee($type) {
			$prevAddressID = $this->addressID;  // If zero always make address_to_relation record
			$this->submitAddress();
			if ($this->ID == 0 ) {
				if ($type == "Employee" ) {
					$sql = "INSERT into employee values (nextval('employee_employee_id_seq'),'$this->userName','$this->password','$this->firstName',E'$this->lastName',NULL,FALSE,
					$this->profileID,E'$this->nokName','$this->nokFone',$this->companyID,NULL,false,'EMP') returning employee_id";
				}
				else {
					$sql = "INSERT into contractor values (nextval('contractor_contractor_id_seq'),'$this->userName','$this->password','$this->firstName',E'$this->lastName',NULL,FALSE,
					$this->profileID,E'$this->nokName','$this->nokFone',$this->companyID) returning contractor_id";

				}

				try {
					if (! $rs =  $this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
				if ($type == "Employee" ) {
					$this->ID = $rs->fields['employee_id'];
				}
				else {
					$this->ID = $rs->fields['contractor_id'];
				}
			}
			else { // Update
				if ($type == "Employee" ) {
					$sql = "UPDATE employee set username = '$this->userName',password = '$this->password',firstname= '$this->firstName',lastname = E'$this->lastName',
					profile_id=$this->profileID,next_of_kin_name = E'$this->nokName',next_of_kin_fone = '$this->nokFone'  where employee_id = $this->ID";
				}
				else {
					$sql = "UPDATE contractor set username = '$this->userName',password = '$this->password',firstname= '$this->firstName',lastname = E'$this->lastName',
					profile_id=$this->profileID ,next_of_kin_name = E'$this->nokName',next_of_kin_fone = '$this->nokFone'  where contractor_id = $this->ID";
				}
				try {
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
				} catch (Exception $e) {
    				if (preg_match('/duplicate key /',$e->getMessage()) > 0 ) {
						die("This user exists already");
					}
					else {
    					die($e->getMessage());
					}
				}
			}
			if ($prevAddressID == 0 ) {  //Could be update
				if ($type == "Employee" ) {
					$sql = "INSERT into address_to_relation (employee_id,address_type,address_id ) values ($this->ID,'".$this->address_type ."',$this->addressID)";
				}
				else {
					$sql = "INSERT into address_to_relation (contractor_id,address_type,address_id ) values ($this->ID,'".$this->address_type ."',$this->addressID)";
				}
				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}

			header("LOCATION: employee.php?type=$type&action=find&id=$this->ID");
		}
	}
?>
