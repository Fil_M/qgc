<?php // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class SiteInsRequired  {
		public $companyID;
		public $page;
		public $empID;
		public $cooID;
		public $contractorID=0;
		public $siteInsReqID;
		public $siteInsNum;
		public $action;
		public $sessionProfile;
		public $conn;
		public $areaID;
		public $wellID;
		public $crewID;
		public $subCrewIDs;
		public $subCrewName;
		public $crewName;
		public $wellName;
		public $areaName;
		public $notifyDate;
		public $supName;
		public $questions;
		public $equalto;
		public $between;
		public $less;
      public $hideEndDate=true;
      public $equalgreater;
      public $creDate;
      public $endDate;
		public $check;
		public $wellStr;
		public $wellIDs;
		public $sigDate;
		public $sigName;
		public $signature;
		public $URL;
		public $empName;
		public $supIDs;
		public $notifyChecked;


		public function	__construct($action,$siteInsReqID=0,$search=NULL,$redo=NULL) {
			$this->URL="https://".$_SERVER['HTTP_HOST'];
			$this->siteInsReqID = $siteInsReqID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			$this->signature = AdminFunctions::getSig($this->empID);
			$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('siteins_req');
				$this->page= $pg->page;
         	switch($action) {
					 case "new" :
                  $heading_text = "Add New Required Site Instruction";
						$this->sigDate = date('d-m-Y');
						$this->sigName = $this->empName;
						$this->notifyChecked = "checked";
                  break;

					case "list" :
						$heading_text =   "List Required Site Instructions";
						$this->getAllRequired($search,$redo) ;
						break;
					case "find" : 
						$heading_text = "Find Required Site Instruction - $this->siteInsReqID";
						$this->getRequiredDetails($siteInsReqID);
						break;
					 case "delete" :
                  if ($siteInsReqID < 1 ) {
                     return false;
                  }
                  $heading_text = "Delete Required Site Instruction - $this->siteInsReqID";
                  $this->getRequiredDetails($siteInsReqID);
                  break;
					case "update" :
                  if ($siteInsReqID < 1 ) {
                     return false;
                  }
                  $heading_text = "Updating Required Site Instruction - $this->siteInsReqID";
                  $this->getRequiredDetails($siteInsReqID);
                  break;
					default:
						//$heading_text .= "Add New Estimate";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->siteInsReqID > 0 )) {
				switch ($action) {
               case "update":
                  $button = "Update Required Instruction";
                  $DISABLED='';
						$display="style=\"display:block;\"";
                  break;
					 case "new":
                  $button = "Add Required Instruction";
						$DISABLED='';
						$display="style=\"display:none;\"";
						$now = time() + (14 * 86400) ; // + 2 weeks
						$this->notifyDate = date('d-m-Y',$now);
                  break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						$display="style=\"display:block;\"";
						break;
					case "delete":
						$button = "Delete Requirement";
						$DISABLED='disabled';
						$display="style=\"display:block;\"";
						break;
					default:
						$button = "No Button";
						$DISABLED='disabled';
						break;
				}
			$content = <<<FIN
<div style="width:1530px;margin:auto;">
<form name="site_ins_req" id="site_ins_req" method="post" action="site_ins_req.php?SUBMIT=SUBMIT">
<input type="hidden" name="siteInsReqID" value="$this->siteInsReqID" />
<input type="hidden" name="empID" value="$this->empID" />
<input type="hidden" name="contractorID" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="signature" value="$this->signature" />
<fieldset ><legend style="margin-left:42%;">Request for Required Site Instruction</legend>
<div class="div25" style="height:11.75rem;" >
<div class="div56"  >
FIN;
 			$content .= AdminFunctions::contractorSelect($this->contractorID,$DISABLED,false,"contractor",true,true);
			$content .= <<<FIN
			</div>
			<div class="div32 marg10" ><label>C.O.O.<input type="text" class="required" id="coo_id" name="coo_id" value="$this->cooID" disabled /></label></div>
			<div class="div56" style="margin-top:2.7rem;" ><label>Site Instruction Number<input type="text" class="num required" id="site_ins_num" name="site_ins_num" value="$this->siteInsNum" $DISABLED /></label></div>
			<div class="div32 marg10" style="margin-top:2.7rem;" ><label>Complete by Date<input type="text" class="date required" id="notif_date" name="notif_date" value="$this->notifyDate"$DISABLED  /></label></div>
</div>
<div id="line_1" class="div75" $display >
<input type="hidden" id="lineno" value="1" />
<div class="div20 marg3" style="margin-left:3.5%;"  ><label  >Work Scope<input type="text" name="crew" id="crew_1" disabled value="$this->crewName" /></label>
</div><div class="div22 marg4"  id="subscopediv_1" ><label>Sub Scopes</label>
<textarea class="txta" name="sub_scopes" id="sub_scopes_1" DISABLED style="height:9.5rem;" >$this->subCrewName</textarea>
</div>
<div class="div20 marg3" ><label for="area" >Area<input type="text" name="area" id="area_1"  value="$this->areaName" DISABLED /></label></div>
<div id="welldiv" class="div23 marg3" ><label  >Well/s</label>
<textarea DISABLED class="txta" id="well_1" style="height:9.5rem;"  >$this->wellName</textarea>
</div>
</div>
<div style="clear:both;" > </div>
</fieldset>
<fieldset ><legend style="margin-left:46%;">Signed for QGC</legend>
<div class="div50" ><label for="supervisor" >Supervisor/s</label>
<div class="div50" id="super_div" `>
FIN;
$content .= AdminFunctions::supervisorSelect($this->supIDs,$DISABLED,true);
$content .= <<<FIN
</div></div>
	<div class="div24" ><img src="$this->signature"  alt="QGC to Sign"  height="180" width="300" /></div>
	<div class="div25 marg1" >
	<div class="div35 marg2"  ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"  class="cntr"  value="$this->sigDate" disabled/></label></div>
	<div class="div94 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->sigName"  readonly /></label></div>
	<div class="div94 marg2" ><label >Notify Contractor</label><input type="checkbox" name="notify" id="notify"  value="notified"  $this->notifyChecked /></div>
	</div>

</fieldset>
<script>
jQuery('.num').keyup(function () { this.value = this.value.replace(/[^0-9]/g,''); });
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
/*  use this for dynamic Select allocation to autocomplete  */
$( document ).ready(function() {
   $(function () {
       var ac = $('#coo_id').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=calloffs',
         params: {con_id:  function() { return $('#contractor').val()} },
         minChars:2,
			onSelect: function(value, data){ showCooLine(data[0]); }
      });
      $("#contractor").change(function() {
			$('#contractorID').val($(this).val());
			if ($.trim($('#contractorID').val()).length > 0 ) {
				$('#coo_id').removeAttr('disabled');
			}
			else {
				 $('#coo_id').prop('disabled','disabled');
			}
         $('#coo_id').val('');
         ac.clearCache();
      });
   });
});
$("#site_ins_req").submit(function(){
	var isFormValid = true;
    $("#site_ins_req input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#site_ins_req .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) {
         alert("Please fill in all the required fields (highlighted in red)");
         return isFormValid;
    }
    else {
      $('#SUBMIT').prop("disabled", "disabled");
      return true;
    }
});
</script>

FIN;
			 	if ($this->action != "find" ) {
               $content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\"  />";
           	}

				$url = isset($_SESSION['last_request']) ? $_SESSION['last_request'] : "$this->URL/admin/site_ins_req.php?action=list&redo=redo";
            $content .= "\n<a href=\"$url\"   class=\"button\"  >Last Screen</a>";
		 		$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllRequired($search,$redo) {
			$_SESSION['last_request'] =  "$this->URL/admin/site_ins_req.php?action=list&redo=redo";
			$this->siteInsReqID = NULL;
         $this->equalgreater = "SELECTED=\"selected\"";
			$whereClause=" where 1 = 1 ";

			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if ( !empty($search->requestID)) {
               $this->siteInsReqID = $search->requestID;
               $whereClause .= " and site_ins_required_id = $this->siteInsReqID ";
               $this->siteInsNum = $search->siteInsNum;
					if (!empty($search->contractorID) ) {
               	$whereClause .= " and sir.contractor_id = $search->contractorID ";
               	$this->contractorID = $search->contractorID;
					}

            }
           	else if (!empty($search->contractorID) ) {
               $whereClause .= " and sir.contractor_id = $search->contractorID ";
               $this->contractorID = $search->contractorID;
           		if (!empty($search->cooID) ) {
                  $whereClause .= " and sir.calloff_order_id = $search->cooID ";
                  $this->cooID = $search->cooID;
           		}
           	}
				else {
               if (!empty($search->creDate) && strlen($search->creDate) > 0) {
                     $dateSelected = true;
                     $whereClause .= " and sir.create_date ";
                     $this->creDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
         					$this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->endDate = $search->endDate;
                           $this->hideEndDate=false;
         						$this->equalgreater = NULL;
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause .= " and sir.create_date  >=  current_date ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                  }
               }
					if (!empty($search->areaID) ) {
                  $whereClause .= " and sir.area_id = $search->areaID ";
                  $this->areaName = $search->areaName;
                  $this->areaID = $search->areaID;
               }
               if (!empty($search->wellID)) {
						$whereClause .= " and $search->wellID in (select (unnest(string_to_array(co.well_ids::text,'|')::int[])))";
                  $this->wellID = $search->wellID;
               }
			   }
         }
         else {
             // Default query still needs search object serialized
            $this->creDate = date ('d-m-Y');
            $whereClause .= " and sir.create_date >= current_date";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            $search->dateType = "equalgreater";
            $search->creDate = $this->creDate;
         }

         $_SESSION['SQL'] = pg_escape_string(serialize($search));
			$sql = "SELECT sir.*,a.area_name,cr.crew_name,c.con_name,subscopes_from_ids(co.sub_crew_ids) as sub_scopes,wells_from_ids(co.well_ids) as well_name,el.fault
			from site_ins_required sir
			LEFT JOIN calloff_order co using(calloff_order_id)
		   LEFT JOIN area a using (area_id)
         LEFT JOIN crew cr using (crew_id)
         LEFT JOIN contractor c on c.contractor_id = sir.contractor_id
			LEFT JOIN email_log el on el.email_log_id =
         ( select max(email_log_id) from email_log  ell where receiver_type = 'REO'  and  ell.dkt_id  = sir.instruction_no )
			$whereClause
			order by sir.calloff_order_id";


			//echo $sql;
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}


			$content ="<div style=\"width:1644px;margin:auto;\" >\n";
			$content  .= <<<FIN
 			<form name="site_ons_req" method="post" action="site_ins_req.php?action=list" >
			<input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         <input type="hidden" name="contractorID" id="contractorID" value="0" />
         <input type="hidden" name="siteInsReqID" id="siteInsReqID" value="$this->siteInsReqID" />
         <input type="hidden" name="empID" id="empID" value="$this->empID" />
         <fieldset style="margin-bottom:10px;width:97.6%;" ><legend style="margin-left:41%;" >Filter Search - Required Site Instructions</legend>
<div class="div10" >
FIN;
         $content .= AdminFunctions::contractorSelect($this->contractorID,"");
         $content  .= <<<FIN
</div>
<div class="div8 marg1" ><label>Call Off Order<input type="text" name="coo_id" id="coo_id" value="$this->cooID" disabled /></label></div>
<div class="div8 marg1" ><label>Site Instr Number<input type="text" name="ins_no" id="ins_no" value="$this->siteInsNum" disabled /></label></div>
<div class="div1" >&nbsp;</div>
FIN;
         $content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->creDate,$this->endDate,$this->hideEndDate);
         $content  .= <<<FIN
<div class="div11 marg1" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div14 marg1" ><div id="welldiv" >
FIN;
			$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
         $content  .= <<<FIN
</div></div>
<script>
$( document ).ready(function() {
   $(function () {
		 var conid = $('#contractor').val();
		 if( conid > 0 ) {
            $('#coo_id').removeAttr('disabled');
            $('#ins_no').removeAttr('disabled');
		 }
       var ac = $('#coo_id').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=calloffs',
         params: {con_id:  function() { return $('#contractor').val()} },
         minChars:2
      });
       var sir = $('#ins_no').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=ins_no_req',
         params: {con_id:  function() { return $('#contractor').val()} },
         minChars:2,
			onSelect: function(value, data){ $('#siteInsReqID').val(data[1]); return false; }
      });
      $("#contractor").change(function() {
         $('#coo_id').val('');
         $('#ins_no').val('');
			 if ($(this).val() != 0 ) {
            $('#coo_id').removeAttr('disabled');
            $('#ins_no').removeAttr('disabled');
         }
         else {
             $('#coo_id').prop('disabled','disabled');
             $('#ins_no').prop('disabled','disabled');
         }

         ac.clearCache();
         sir.clearCache();
      });
   });
});
$.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
			$content .= "<div style=\"float:right;width:16%;\" >\n";
       	$content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button\"/>";
       	$content .= "\n<button class=\"marg\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
		 	$content .= "</div></fieldset></form>\n";

			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"vehicle hd\">Site Instruction Number</div>\n";
			$content .= "<div class=\"rate hd\">C.O.O.</div>\n";
			$content .= "<div class=\"crew hd\">Contractor</div>\n";
			$content .= "<div class=\"crew hd\">Work Scope</div>\n";
			$content .= "<div class=\"cli hd\">Area</div>\n";
			$content .= "<div class=\"email hd\">Well/s</div>\n";
			$content .= "<div class=\"times hd\">Create Date</div>\n";
			$content .= "<div class=\"usr hd\">Create Emailed</div>\n";
			$content .= "<div class=\"times hd\">Notify Date</div>\n";
			$content .= "<div class=\"usr hd\">Notify Emailed</div>\n";
			$content .= "<div class=\"links hd\" style=\"width:130px;\"   >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$bStyle = "style=\"margin-left:2.4rem;min-width:45px !important;\"";
				$emailButTxt = $emailNotButTxt = "To Email";
            $emailButClass = $emailNotButClass = $emCreateBut =  $emNotBut = "";
            $ln= $lineNo % 2 == 0  ? "line1" : "";
            $ins_no = $rs->fields['instruction_no'];
            $coo_id = $rs->fields['calloff_order_id'];
            $sitereq_id = $rs->fields['site_ins_required_id'];
            $contractor = $rs->fields['con_name'];
            $conID = $rs->fields['contractor_id'];
            $create = AdminFunctions::dbDate($rs->fields['create_date']);
            $notify = AdminFunctions::dbDate($rs->fields['notification_date']);
            $emailCreate = $rs->fields['emailed_create'];
				if ($emailCreate == "t" ) {
               $emailButTxt = "Emailed";
               $emailButClass = "emailed";
					$emCreateBut= "<button  class=\"linkbutton $emailButClass\" style=\"margin-left:2.4rem;\" onclick=\"emailSiteReq($sitereq_id,$conID,1,$ins_no,'$notify',$coo_id,$(this));return false;\">$emailButTxt</button>";
            }
            $emailNotify = $rs->fields['fault'];
				if ($emailNotify == "OK" ) {
               $emailNotButTxt = "Emailed";
               $emailNotButClass = "emailed";
					$bStyle = "style=\"margin-left:2.4rem;min-width:45px !important;background:#ca2020;\"";
				}
				$emNotBut= "<button  class=\"linkbutton $emailNotButClass\" $bStyle onclick=\"emailSiteReq($sitereq_id,$conID,2,$ins_no,'$notify',$coo_id,$(this));$(this).prop('disabled','disabled');return false;\">$emailNotButTxt</button>";
            $area = $rs->fields['area_name'];
            $well = $rs->fields['well_name'];
            $crew = $rs->fields['crew_name'];
				$pad = "SUB SCOPES: ";
				$sub_crew =  isset($rs->fields['sub_scopes']) ? $rs->fields['sub_scopes'] : "" ;
            $toolTip = !empty($sub_crew) ?  "<span class=\"show-option\" title=\"${pad}${sub_crew}\" >$crew</span>" : $crew;
            //o$content .= "<div style=\"float:left;\" >\n";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"vehicle highh $ln cntr\">$ins_no</div>\n";
				$content .= "<div class=\"rate highh $ln\">$coo_id</div>\n";
				$content .= "<div class=\"crew highh $ln\">$contractor</div>\n";
				$content .= "<div class=\"crew highh $ln cntr\">$toolTip</div>\n";
				$content .= "<div class=\"cli highh $ln\">$area</div>\n";
				$content .= "<div class=\"email highh $ln\">$well/s</div>\n";
				$content .= "<div class=\"times highh $ln cntr\">$create</div>\n";
				$content .= "<div class=\"usr highh $ln\">$emCreateBut</div>\n";
				$content .= "<div class=\"times highh $ln cntr\">$notify</div>\n";
				$content .= "<div class=\"usr highh $ln\">$emNotBut</div>\n";
            if ($this->sessionProfile > 2 ) {
               $content .= "<div class=\"links highh $ln\" style=\"width:130px;\" >\n";
					$content .= "<div style=\"width:100px;margin:auto;\" >\n";
					$content .= "<a href=\"site_ins_req.php?action=find&sitereq_id=$sitereq_id\" title=\"View Request\" class=\"linkbutton\" style=\"min-width:39px !important;\"  >View</a>\n";
					$content .= "<a href=\"site_ins_req.php?action=delete&sitereq_id=$sitereq_id\" title=\"Delete Request\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >Delete</a>";

					$content .= "</div>\n";

            }
            else {
               $content .= "<div class=\"links highh $ln\"  style=\"width:130px;\" > &nbsp;";
            }
				$content .= "</div>\n";


				$content .= "</div><div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "<hr /></div>";

			$this->page = str_replace('##MAIN##',$content,$this->page);

		}
		private function getRequiredDetails($siteInsReqID) {
			$sql = "SELECT sir.*,crew_name,area_name,wells_from_ids(co.well_ids) as well_name,subscopes_from_ids(co.sub_crew_ids) as subscopes
			FROM site_ins_required sir
			LEFT JOIN calloff_order co using(calloff_order_id)
			LEFT JOIN crew c on c.crew_id = co.crew_id
			LEFT JOIN area a on a.area_id = co.area_id
			WHERE site_ins_required_id = $siteInsReqID";
			if (! $data = $this->conn->getRow($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}

			}
         $this->siteInsReqID = $data['site_ins_required_id'];
         $this->cooID = $data['calloff_order_id'];
		 	$this->siteInsNum = $data['instruction_no'];
         $this->contractorID = $data['contractor_id'];
         $this->empID = $data['employee_id'];
         $this->supIDs = $data['supervisor_ids'];
         $this->sigDate = AdminFunctions::dbDate($data['qgc_approve_date']);
         $this->notifyDate = AdminFunctions::dbDate($data['notification_date']);
         $this->signature=  $data['qgc_sig'];
         $this->sigName = $data['qgc_signee'];
			$this->notifyChecked = $data['emailed_create'] == "t" ? "checked" : ""; // TODO  this  please
			$this->crewName = $data['crew_name'];
			$this->subCrewName = AdminFunctions::splitStr($data['subscopes']);
			$this->areaName = $data['area_name'];
			$this->wellName = AdminFunctions::splitStr($data['well_name']);




      }
		 private function processPost() {
         $this->siteInsReqID = intval($_POST['siteInsReqID']);
         if ($this->action == "delete" ) {
            $this->deleteSiteInsRequired();
         }
         $this->cooID = intval($_POST['coo_id']);
		 	$this->siteInsNum = $_POST['site_ins_num'];
         $this->contractorID = intval($_POST['contractor']);
			if (! AdminFunctions::checkCOO($this->cooID,$this->contractorID)) {
				die("Incorrect C.O.O.");
			}
         $this->empID = intval($_POST['empID']);
         $this->supIDs = isset($_POST['supervisor']) ? implode("|",$_POST['supervisor']) : "";
         $this->sigDate = !empty($_POST['sig_date']) ? "'" . $_POST['sig_date'] . "'" : "current_date";
         $this->creDate =  "current_date";
         $this->notifyDate =  "'" . $_POST['notif_date'] . "'";
         $this->signature=  "'" .$_POST['signature'] ."'";
         $empName = trim(pg_escape_string($_POST['signee_qgc']));
         $this->empName = strlen($empName) > 2 ? "E'" . $empName . "'" : "NULL";
			$this->notifyChecked = isset($_POST['notify']) ? "true" : "false" ;

         $this->submitForm();
      }


		private function deleteSiteInsRequired() {
         $sql = "DELETE from site_ins_required where site_ins_required_id  = $this->siteInsReqID";
         if (! $this->conn->Execute($sql)) {
            die($this->conn->ErrorMsg());
         }
         header("LOCATION: $this->URL/admin/site_ins_req.php?action=list&redo=redo");
         exit;
      }
      private function submitForm() {
			global $BASE;
         if ($this->siteInsReqID == 0 ) {
            $sql = "INSERT into site_ins_required values (nextval('site_ins_required_site_ins_required_id_seq'),$this->cooID,$this->contractorID,$this->siteInsNum,$this->empID,$this->creDate,$this->notifyDate,
				'$this->supIDs' ,$this->signature,$this->sigDate,$this->empName,$this->notifyChecked) returning site_ins_required_id";


            if (! $rs =  $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }
            $this->siteInsReqID = $rs->fields['site_ins_required_id'];
				if ( $this->notifyChecked == "true"  ) {
					// quotes added in insertEmailLog  for  dates  remove  here
					$this->notifyDate = preg_replace("/\'/","",$this->notifyDate);
            	$_SESSION['last_request'] = "{$this->URL}/admin/site_ins_req.php?action=find&sitereq_id=$this->siteInsReqID";
            	$eNum = AdminFunctions::insertEmailLog($this->empID,0,$this->contractorID,"Request for Site Instruction Estimate for Instruction Number $this->siteInsNum",'REQ',$this->siteInsNum,$this->notifyDate,$this->cooID);
					exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
				}
         }
         else { // Update
            $sql = "UPDATE site_ins_required set calloff_order_id = $this->cooID, contractor_id = $this->contractorID,instruction_no = $this->siteInsNum,notification_date = $this->notifyDate,
				supervisor_ids =  '$this->supIDs',emailed_create = $this->notifyChecked where site_ins_required_id = $this->siteInsReqID";
            //echo $sql;
           // exit;
            if (! $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }

         }

         header("LOCATION: $this->URL/admin/site_ins_req.php?action=find&sitereq_id=$this->siteInsReqID");
         exit;

      }


	}
?>
