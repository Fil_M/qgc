<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Certificate {
		public $conn;
		public $certificates = array();
		public $content;
		public $emp_type;
		public $existingCertIDs = array();

		public function	__construct($empType) {
			$this->emp_type = $empType;
			$this->conn= $GLOBALS['conn'];
			//$this->certificateDiv(NULL);
		}

		public function certificateDiv($type,$id,$DISABLED) {
			if ($type == "Employee" ) {
				$whereClause = " WHERE ec.employee_id = $id ";
			}
			else {
				$whereClause = " WHERE ec.contractor_id = $id ";
			}

			$this->content = "<fieldset ><legend style=\"margin-left:42%;\">Licences &amp; Certificates</legend>\n";
			$sql = "SELECT c.certificate_id,c.certificate_desc,ec.certificate,ec.expiry_date 
			from certificate c 
         LEFT JOIN employee_certificate ec  using  (certificate_id ) 
			$whereClause
			and  c.removed is false 
			and ec.removed is false
			union
			SELECT certificate_id,certificate_desc,NULL,NULL 
			from certificate  where certificate_id not in (select certificate_id from employee_certificate ec $whereClause) 
			order by certificate_id";
			if (! $data = $this->conn->getAll($sql)) {
				die($this->conn->ErrorMsg());
			}
			$mod = 0;
			foreach ($data as $key=>$val) {
				$marg = $mod % 2 == 0 ? "" : "marg2";
				$certmargdiv = $mod % 2 == 0 ? "" : "<div class=\"div10\">&nbsp;</div>\n";
				$labDesc = "cert_desc".$val['certificate_id'];
				$labExp = "exp".$val['certificate_id'];
				$this->content .="<input type=\"hidden\" name=\"cert_id[]\" value=\"".$val['certificate_id'] ."\" />";
				$this->content .= "<div class=\"div49 $marg\" >$certmargdiv<div class=\"div65\" >\n";
				$this->content .="<label for=\"$labDesc\" >".$val['certificate_desc'] ."<input type=\"text\" name=\"certificate[]\" id=\"$labDesc\"  value=\"".$val['certificate'] ."\" $DISABLED /></label></div>\n";
				$this->content .= "<div class=\"div20 marg4\" >\n";
				$this->content .="<label for=\"expiry\"  >Expiry Date<input type=\"text\" name=\"expiry[]\" id=\"$labExp\"  value=\"".$val['expiry_date'] ."\" $DISABLED class=\"date\" /></label></div></div>\n";
				$mod ++;

			}
				$this->content .= "<div style=\"clear:both;\" ></div>\n";
            $this->content .= <<<FIN
<script>
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
   //$(document).on('focus.datepicker','.input.abn").datepicker({dateFormat: 'dd-mm-yy'});


         $this->content .= "</fieldset>\n";
			//echo $this->content;

		}

		public function processCertificatePost() {
			$cnt = count($_POST['cert_id']);
			for ($x = 0;$x<$cnt;$x++) {
			   $cert = trim($_POST['certificate'][$x]);
				if (strlen($cert) > 2 ) {
					$expiry = trim($_POST['expiry'][$x]);
					$this->certificates[] = array('cert_id'=>$_POST['cert_id'][$x],'cert'=>$cert,'expiry'=>$expiry);
				}
			}
		}

		public function submitCertificates($type,$id) {
			if ($type == "Employee" ) {
				$sql = "SELECT certificate_id from employee_certificate where employee_id = $id and removed is false";
			}
			else {
				$sql = "SELECT certificate_id from employee_certificate where contractor_id = $id and removed is false";
			}
			if (!$res = $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			while (! $res->EOF ) {
				$this->existingCertIDs[] = $res->fields['certificate_id'];
				$res->MoveNext();
			}
			foreach($this->certificates as $key => $val) {
				if (in_array($val['cert_id'],$this->existingCertIDs)) {
					$this->updateCertificates($type,$id,$val['cert_id']);
				}
				else {
					$this->insertCertificates($type,$id,$val['cert_id']);
				}
         }

		}

		public function insertCertificates($type,$id,$cert_id) {
			foreach($this->certificates as $key => $val ) {
				if ($val['cert_id'] == $cert_id ) {
					$insExpiry = strlen($val['expiry']) > 8 ? "'".$val['expiry'] ."'" : "NULL";
					if ($type == "Employee" ) {
						$sql = "INSERT into employee_certificate (employee_id,certificate_id,certificate,expiry_date,emp_type) 
						values ($id,".$val['cert_id'].",'".$val['cert'] ."',$insExpiry,'$this->emp_type')";
					}
					else {
						$sql = "INSERT into employee_certificate (contractor_id,certificate_id,certificate,expiry_date,emp_type) 
						values ($id,".$val['cert_id'].",'".$val['cert'] ."',$insExpiry,'$this->emp_type')";
					}
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
					break;
				}
			}
		}
      public function updateCertificates($type,$id,$cert_id) {
			foreach($this->certificates as $key => $val ) {
				if ($val['cert_id'] == $cert_id ) {
					$upExpiry = strlen($val['expiry']) > 8 ? "expiry_date = '".$val['expiry'] ."'" : "expiry_date = NULL";
					if ($type == "Employee") {
						$sql = "UPDATE employee_certificate set certificate = '".$val['cert'] ."',$upExpiry where certificate_id = ".$val['cert_id'] ." and employee_id = $id";
					}
					else {
						$sql = "UPDATE employee_certificate set certificate = '".$val['cert'] ."',$upExpiry where certificate_id = " .$val['cert_id'] . " and contractor_id = $id";
					}
					if (! $this->conn->Execute($sql)) {
						die($this->conn->ErrorMsg());
					}
					break;
				}

			}

		}

	}
?>
