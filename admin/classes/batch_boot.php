<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class CostEstimate  {
		public $page;
		public $requestID;
		public $empID;
		public $contractorID;
		public $action;
		public $sessionProfile;
		public $conn;
		public $wellID;
		public $wellName;
		public $areaID;
		public $areaName;
		public $constructChecked;
		public $withSumpChecked;
		public $spineChecked;
		public $sumpChecked;
		public $rehabChecked;
		public $otherChecked;
		public $otherText;
		public $siteConditions;
		public $gravel;
		public $water;
		public $returnGravel;
		public $returnWater;
		public $estStart;
		public $estComplete;
		public $creDate;
		public $supName;

		public function	__construct($action,$requestID=0,$search="") {
			$this->requestID = $requestID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			$this->supName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('cost');
				$this->page= $pg->page;
				$heading_text = "Administration<BR />";
         	switch($action) {
					case "new" :
						$this->supName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
						$this->creDate = date('d-m-Y');
						$heading_text .= "Add New Target Cost Estimate Request";
						break;
					case "update" :
						if ($requestID < 1 ) {
							return false;
						}
						$heading_text .= "Updating Cost Estimate $requestID";
						$this->getEstimateDetails($requestID);
						break;
					case "delete" :
						if ($requestID < 1 ) {
							return false;
						}
						$heading_text .= "Delete Cost Estimate Request $requestID";
						$this->getEstimateDetails($requestID);
						break;
					case "list" :
						$heading_text .=   "List Cost Estimates";
						$this->getAllEstimates($search) ;
						break;
					case "find" : 
						$heading_text .= "Cost Estimate Request $requestID";
						$this->getEstimateDetails($requestID);
						break;
					default:
						$heading_text .= "Add New Estimate";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->requestID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Estimate";
						break;
					case "update":
						$button = "Update Estimate";
						break;
					case "delete":
						$button = "Delete Estimate";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Estimate";
						break;
				}
			$content = <<<FIN
<div style="width:1800px;text-align:center;"><div style="width:1366px;margin:auto;">
<form name="cost" id="cost" method="post" action="cost_estimate.php">
<input type="hidden" name="requestID" value="$this->requestID" />
<input type="hidden" name="empID" value="$this->empID" />
<input type="hidden" name="contractor_id" value="$this->contractorID" />
<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
<input type="hidden" name="well_id" value="$this->wellID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="margin-top:15px;width:1366px;"><legend style="margin-left:530px;">Request For Target Cost Estimate</legend>
<label for="contractor" class="label wide" >Contractor</label>
FIN;
	$content .= AdminFunctions::contractorSelect($this->contractorID,$DISABLED);
$content .= <<<FIN
<label for="area" class="label sml" >Area</label><input type="text" name="area" id="area" value="$this->areaName" class="input required $DISABLED"  $DISABLED />
<label for="well" class="label sml">Well</label><div id="welldiv" > &nbsp;
FIN;
   $content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,$DISABLED);  // All wells from this name  type
	$content .= <<<FIN
</div>
</fieldset>
<div style="clear:both;" ></div>
<fieldset style="margin-top:15px;width:1366px;"><legend style="margin-left:630px;">Work Required</legend>
<div class="divhalf" ><input type="checkbox" name="construct" id="construct" class="chk" $this->constructChecked  $DISABLED />CONSTRUCT LEASE PAD AND ASSOCIATED ACCESS ROAD</div>
<div class="divhalf" ><input type="checkbox" name="withsump" id="withsump" class="chk" $this->withSumpChecked  $DISABLED />CONSTRUCT LEASE PAD (WITH SUMP) AND ASSOCIATED ACCESS ROAD</div>
<div class="divhalf" ><input type="checkbox" name="spine" id="spine" class="chk" $this->spineChecked  $DISABLED />CONSTRUCT SPINE ROAD</div>
<div class="divhalf" ><input type="checkbox" name="sump" id="sump" class="chk" $this->sumpChecked  $DISABLED />CONSTRUCT SUMP</div>
<div style="clear:both;"> </div> 
<div class="divhalf other" ><input type="checkbox" name="other" id="other" class="chk" $this->otherChecked  $DISABLED />OTHER</div>
<textarea  name="othertext"  id="othertext" class="txt sml $DISABLED" $DISABLED  >$this->otherText</textarea>
</fieldset>
<fieldset style="margin-top:15px;width:1366px;"><legend style="margin-left:630px;">Site</legend>
<label for="sitecond" class="label wide" >Site Conditions</label><textarea   name="sitecond" id="sitecond" class="txt sml $DISABLED" $DISABLED  >$this->siteConditions</textarea>
<div style="clear:both;height:15px;" > </div>
<div class="divhalf" ><label for="gravel" class="label wide sm" >Gravel Source</label><input type="text" name="gravel" id="gravel" class="input desc" value="$this->gravel"  $DISABLED /></div>
<div class="divhalf" ><label for="water" class="label wide sm" >Water Source</label><input type="text" name="water" id="water" class="input desc" value="$this->water"  $DISABLED /></div>
<div style="clear:both;height:15px;" > </div>
<div class="divhalf" ><label for="returngravel" class="label wide sm" >Return Distance</label><input type="text" name="returngravel" id="returngravel" class="input tiny" value="$this->returnGravel" $DISABLED  /> Km</div>
<div class="divhalf" ><label for="returnwater" class="label wide sm" >Return Distance</label><input type="text" name="returnwater" id="returnwater" class="input tiny" value="$this->returnWater"  $DISABLED /> Km</div>
<div style="clear:both;height:15px;" > </div>
<div class="divhalf" ><label for="start" class="label swide sm" >Estimated Start Date</label><input type="text" name="start" id="start" class="input date" value="$this->estStart"  $DISABLED /></div>
<div class="divhalf" ><label for="complete" class="label swide sm" >Estimated Completion Date</label><input type="text" name="complete" id="complete" class="input date" value="$this->estComplete"  $DISABLED /></div>
</fieldset>
<fieldset style="margin-top:15px;width:1366px;"><legend style="margin-left:630px;">Signed</legend>
<div class="divhalf" ><label for="supervisor" class="label wide sm" >Supervisor</label><input type="text" name="supervisor" id="supervisor" class="input" value="$this->supName"  $DISABLED /></div>
<div class="divhalf" ><label for="cre_date" class="label wide sm" >Date</label><input type="text" name="cre_date" id="cre_date" class="input date" value="$this->creDate" $DISABLED  /></div>
</fieldset>
<script>
$(function () {
      $('#sitecond').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=sitecond',
         minChars:1
      });
  });

$(function () {
      $('#othertxt').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=othertxt',
         minChars:1
      });
  });
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv')); return false; }
      });
  });
$("#cost").submit(function(){
    var isFormValid = true;
    $("#cost input:text.input.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#cost .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
	jQuery('.input.tiny').keyup(function () { this.value = this.value.replace(/[^0-9]/g,''); });
   $(".input.date").scroller({preset: 'date',dateFormat: 'dd-mm-yy',dateOrder: 'ddmmyy',display: 'bubble',mode: 'mixed'});

</script>
FIN;


				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"$button\" class=\"submitbutton\" style=\"margin:0px;\"/>"; // no submit for found cost
				}
				 $content .= "\n</form></div></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllEstimates($search="") {
		  	$whereClause="";
         if (strlen($search) > 0 ) {
            $whereClause = " and upper(c.name) like '$search%'";
         }
	
			$sql = "SELECT r.*,a.area_name,w.well_name,c.name from request_estimate r 
			LEFT JOIN area a using (area_id) 
			LEFT JOIN well w using (well_id)  
			LEFT JOIN contractor c using (contractor_id)  
			where r.removed is false 
			$whereClause
			order by est_start_date";
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}
			$content ="<div style=\"width:1800px;text-align:center;\"><div style=\"width:1712px;margin:auto;\">";
			$content .= AdminFunctions::AtoZ("cost_estimate.php?action=list");
			$content .= "<div style=\"clear:both;\"></div>\n";
			$content .= "<div class=\"heading\" style=\"width:1710px\">\n";
			$content .= "<div class=\"contact hd\">Contractor</div>";
			$content .= "<div class=\"usr hd\">Area</div>\n";
			$content .= "<div class=\"fone hd\">Well</div>\n";
			$content .= "<div class=\"fone hd\">Est Start</div>\n";
			$content .= "<div class=\"fone hd\">Est Compl</div>\n";
			$content .= "<div class=\"addr hd\">Other Text</div>\n";
			$content .= "<div class=\"addr hd\">Site Conditions</div>\n";
			$content .= "<div class=\"contact hd\">Supervisor</div>\n";
			$content .= "<div class=\"links hd\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$request_id = $rs->fields['request_estimate_id'];
				$contractor = $rs->fields['name'];
				$area = $rs->fields['area_name'];
				$well = $rs->fields['well_name'];
				$start = AdminFunctions::dbDate($rs->fields['est_start_date']);
				$compl = AdminFunctions::dbDate($rs->fields['est_compl_date']);
				$other = $rs->fields['other_text'];
				$site = $rs->fields['site_conditions'];
				$supervisor = $rs->fields['supervisor_name'];

				$content .= "<div class=\"contact $ln\">$contractor</div>";
				$content .= "<div class=\"usr $ln\">$area</div>\n";
				$content .= "<div class=\"fone $ln\">$well</div>\n";
				$content .= "<div class=\"fone $ln\">$start</div>\n";
				$content .= "<div class=\"fone $ln\">$compl</div>\n";
				$content .= "<div class=\"addr $ln\">$other</div>\n";
				$content .= "<div class=\"addr $ln\">$site</div>\n";
				$content .= "<div class=\"contact $ln\">$supervisor</div>\n";
				if ($this->sessionProfile > 2 ) {
           	$content .= "<div class=\"links $ln\"><a href=\"cost_estimate.php?action=update&request_id=$request_id\" title=\"Edit Estimate\" class=\"linkbutton\" >EDIT</a><a href=\"cost_estimate.php?action=delete&request_id=$request_id\" title=\"Delete Estimate\" class=\"linkbutton d\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
				}
				else {
					$content .= "<div class=\"links $ln\" > &nbsp;</div>";
				}
				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "</div></div><hr />";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getEstimateDetails($requestID) {
			$sql = "SELECT r.*,a.area_name,w.well_name from request_estimate r LEFT JOIN area a using (area_id) LEFT JOIN well w using (well_id)  where request_estimate_id = $requestID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->contractorID = $data['contractor_id'];
			$this->wellID = $data['well_id'];
			$this->areaID = $data['area_id'];
			$this->areaName = $data['area_name'];
			$this->wellName = $data['well_name'];
			$this->constructChecked = $data['construct'] == "t" ? "CHECKED" : "";
			$this->withSumpChecked = $data['with_sump'] == "t" ? "CHECKED" : "";
			$this->spineChecked = $data['spine'] == "t" ? "CHECKED" : "";
			$this->sumpChecked = $data['sump'] == "t" ? "CHECKED" : "";
			$this->otherChecked = $data['other'] == "t" ? "CHECKED" : "";
			$this->otherText = $data['other_text'];
			$this->siteConditions = $data['site_conditions'];
			$this->gravel = $data['gravel_source'];
			$this->water = $data['water_source'];
			$this->returnGravel = $data['gravel_return'];
			$this->returnWater = $data['water_return'];
			$this->estStart = $data['est_start_date'];
			$this->estComplete = $data['est_compl_date'];
			$this->creDate = $data['create_date'];
			$this->supName = $data['supervisor_name'];

		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteEstimate();
			}
			$this->requestID = intval($_POST['requestID']);
			$this->contractorID = intval($_POST['contractor']);
			$this->wellID = intval($_POST['well']);
			$this->areaID = intval($_POST['areaID']);
			$this->empID = intval($_POST['empID']);
			$this->constructChecked = isset($_POST['construct']) ? "TRUE" : "FALSE";
			$this->withSumpChecked = isset($_POST['withsump']) ? "TRUE" : "FALSE";
			$this->spineChecked = isset($_POST['spine']) ? "TRUE" : "FALSE";
			$this->sumpChecked = isset($_POST['sump']) ? "TRUE" : "FALSE";
			$this->otherChecked = isset($_POST['other']) ? "TRUE" : "FALSE";
			$this->otherText = trim(addslashes($_POST['othertext']));
			$this->siteConditions = trim(addslashes($_POST['sitecond']));
			$this->gravel= trim(addslashes($_POST['gravel']));
			$this->water= trim(addslashes($_POST['water']));
			$this->returnGravel = intval($_POST['returngravel']);
			$this->returnWater = intval($_POST['returnwater']);
			$this->estStart = strlen($_POST['start']) > 2 ? "'" . $_POST['start'] . "'" : "NULL";
			$this->estComplete = strlen($_POST['complete']) > 2 ? "'" . $_POST['complete'] . "'" : "NULL";
			$this->creDate = strlen($_POST['cre_date']) > 2 ? "'" . $_POST['cre_date'] . "'" : "NULL";
			$this->supName = trim(addslashes($_POST['supervisor']));
			
			$this->submitForm();
		}
		private function deleteEstimate() {
			$sql = "UPDATE cost_estimate set removed = TRUE where request_estimate_id = $this->requestID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: cost_estimate.php?action=list");
			exit;
		}
		private function submitForm() {
			if ($this->requestID == 0 ) {
				$sql = "INSERT into request_estimate values (nextval('request_estimate_request_estimate_id_seq'),$this->contractorID,$this->areaID,$this->wellID,$this->empID,$this->constructChecked,
				$this->withSumpChecked,$this->spineChecked,$this->sumpChecked,$this->otherChecked,E'$this->otherText',E'$this->siteConditions',E'$this->gravel',E'$this->water',
				$this->returnGravel,$this->returnWater,$this->estStart,$this->estComplete,E'$this->supName',$this->creDate) returning request_estimate_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->requestID = $rs->fields['request_estimate_id'];
			}
			else { // Update
				$sql = "UPDATE request_estimate set contractor_id = $this->contractorID,area_id = $this->areaID,well_id = $this->wellID,construct = $this->constructChecked,
				with_sump = $this->withSumpChecked,spine = $this->spineChecked,sump = $this->sumpChecked,other = $this->otherChecked,other_text = E'$this->otherText',site_conditions = E'$this->siteConditions',
				gravel_source = E'$this->gravel',water_source = E'$this->water',gravel_return = $this->returnGravel,water_return= $this->returnWater,est_start_date = $this->estStart, est_compl_date= $this->estComplete,
				supervisor_name = E'$this->supName',create_date = $this->creDate where request_estimate_id = $this->requestID";
				//echo $sql;
				//exit;
				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}

			}

			header("LOCATION: cost_estimate.php?action=find&request_id=$this->requestID");
			exit;
			
		}

	}
?>
