<?php
class Contact 	{
	public $conn;
	public $ID;
	public $contactRows;
	public $contactDv;
	public $contacts = array();

	public function __construct($ID=0) {
		$this->conn = $GLOBALS['conn'];
		$this->ID = $ID;
	}


   public function getContactDetails($type) {
		if ($type == "cli") {
			$sql = "SELECT contact_id,contact_firstname,contact_lastname,contact_email,contact_fone from contact where client_id = $this->ID and removed is false order by contact_id";
		}
		else {
			$sql = "SELECT contact_id,contact_firstname,contact_lastname,contact_email,contact_fone from contact where pilot_id = $this->ID and removed is false order by contact_id";
		}
		if (! $this->contactRows = $this->conn->getAll($sql)) {
			if ( $this->conn->ErrorMsg() != 0 ) {  // No rows found
				die($this->conn->ErrorMsg());
			}
		}
	}
  

	public function contactDiv($DISABLED="") {
		$content = "<div style=\"clear:both;height:10px;\"> </div><div class=\"centerDiv\" >Address Contact Details</div><div id=\"contactdiv\">\n";
      if (isset($this->contactRows)) {
			foreach($this->contactRows as $key=>$val) {
				$content .= "<div style=\"clear:both;\"></div>";
				$content .= "<input type=\"hidden\" value=\"".$val['contact_id'] ."\" name=\"contact_id[]\" />";
         	$content .= "<label for=\"con_first\" class=\"label\" >First Name</label><input type=\"text\" name=\"con_first[]\" id=\"con_first\" value=\"".$val['contact_firstname'] ."\" class=\"input dest $DISABLED\" $DISABLED/>\n";
				$content .= "<label for=\"con_last\" class=\"label\" >Last Name</label><input type=\"text\" name=\"con_last[]\" id=\"con_last\" value=\"" .$val['contact_lastname'] ."\" class=\"input dest $DISABLED\" $DISABLED/>\n";
				$content .= "<label for=\"con_email\" class=\"label abn\" >Email</label><input type=\"text\" name=\"con_email[]\" id=\"con_email\" value=\"" .$val['contact_email'] ."\" class=\"input dest $DISABLED\" $DISABLED/>\n";
				$content .= "<label for=\"con_fone\" class=\"label abn\" >Phone</label><input type=\"text\" name=\"con_fone[]\" id=\"con_fone\" value=\"" .$val['contact_fone'] ."\" class=\"input phone $DISABLED\" $DISABLED/>\n";
			}
		}
		$content .="</div>\n";
		if ($DISABLED != "disabled" ) {
			$content .= "<div class=\"buttonDiv\"><button class=\"addButton\"  onClick=\"addContact();return false;\">Add Contact</button></div>";
		}
      $this->contactDv =  $content; 
	}

	public function processContactPost() {
		if (isset($_POST['contact_id'])) {
			$cnt = count($_POST['contact_id']);
			for ($x = 0;$x<$cnt;$x++) {
				$this->contacts[] = array("contact_id"=>$_POST['contact_id'][$x],"con_first"=>trim($_POST['con_first'][$x]),"con_last"=>trim(addslashes($_POST['con_last'][$x])),
				"con_email"=>trim($_POST['con_email'][$x]),"con_fone"=>trim($_POST['con_fone'][$x]));
			}
		}
	}

	public function submitContacts($type,$ID) {
		$cnt = count($this->contacts);
		if ($cnt > 0 ) {
			for($x=0;$x<$cnt;$x++) {
            $contact_id = intval($this->contacts[$x]['contact_id']);
				if(strlen($this->contacts[$x]['con_first'] . $this->contacts[$x]['con_last'] . $this->contacts[$x]['con_email'] ) < 5 ) {
					continue;
				}
				if ($contact_id > 0 ) {  // Update
					$sql = "UPDATE contact set contact_firstname = '".$this->contacts[$x]['con_first'] ."',contact_lastname = E'".$this->contacts[$x]['con_last']
					 ."',contact_email = '".$this->contacts[$x]['con_email'] . "', contact_fone = '".$this->contacts[$x]['con_fone'] ."' where contact_id = $contact_id";
				}
				else {  // Insert
					if ($type == "cli" ) {
						$sql = "INSERT into contact (client_id,contact_firstname,contact_lastname,contact_email,contact_fone) values ($ID,'".$this->contacts[$x]['con_first'] 
					."',E'".$this->contacts[$x]['con_last'] ."','".$this->contacts[$x]['con_email'] ."','".$this->contacts[$x]['con_fone']."')";
					}
               else {
						$sql = "INSERT into contact (pilot_id,contact_firstname,contact_lastname,contact_email,contact_fone) values ($ID,'".$this->contacts[$x]['con_first'] 
					."',E'".$this->contacts[$x]['con_last'] ."','".$this->contacts[$x]['con_email'] ."','".$this->contacts[$x]['con_fone']."')";

					}
				}
				if (! $res= $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}
		}
	}
	public function deleteContacts($type,$ID) {
		if ($type == "cli" ) {
			$sql = "UPDATE contact set removed = TRUE where client_id = $ID";
		}
		else {
			$sql = "UPDATE contact set removed = TRUE where pilot_id = $ID";
		}
		if (! $res = $this->conn->Execute($sql)) {
			die($$this->conn->ErrorMsg());
		}
	}

}
?>
