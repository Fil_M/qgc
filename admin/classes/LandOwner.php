<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class LandOwner  {
		public $page;
		public $lOwnerID;
		public $landownerName;
		public $address;
		public $action;
		public $sessionProfile;
		public $conn;
		public $locs;
      public $locType;
	   public $locIDs;
	   public $pushed;
	   public $nonPushed;
	   public $water;
	

		public function	__construct($action,$lOwnerID=0,$search="") {
			$this->lOwnerID = $lOwnerID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('landowner');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Add New Land Owner";
						break;
					case "update" :
						if ($lOwnerID < 1 ) {
							return false;
						}
						$heading_text = "Updating Land Owner $lOwnerID";
						$this->getLOwnerDetails($lOwnerID);
						break;
					case "delete" :
						if ($lOwnerID < 1 ) {
							return false;
						}
						$heading_text = "Delete Land Owner $lOwnerID";
						$this->getLOwnerDetails($lOwnerID);
						break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List Land Owners - $search" : "List Land Owners - All";
						$this->getAllLOwners($search) ;
						break;
					case "find" : 
						$heading_text = "Land Owner $lOwnerID";
						$this->getLOwnerDetails($lOwnerID);
						break;
					default:
						$heading_text = "Add New Land Owner";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->lOwnerID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Owner";
						break;
					case "update":
						$button = "Update Owner";
						break;
					case "delete":
						$button = "Delete Owner";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Owner";
						break;
				}
//<div class="div70 marg1" ><label for="address" >Address<input type="text" name="address" id="address" value="$this->address" $DISABLED /></label></div>
			$content = <<<FIN
<div style="width:1421px;margin:auto;">
<form name="landowner" id="landowner" method="post" action="landowner.php?SUBMIT=SUBMIT">
<input type="hidden" name="landowner_id" id="landowner_id" value="$this->lOwnerID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:47%;" >Land Owner</legend>
<div class="div27" > <label for="landowner_name" >Land Owner Name<input type="text" name="landowner_name" id="landowner_name" value="$this->landownerName" $DISABLED /></label></div>
<div class="div70 marg1" ><label for="address" >Address <input type="text" name="address" id="address" value="$this->address" $DISABLED />       </label></div>
</fieldset>
FIN;
	$content .= $this->pitsPonds($DISABLED) ;




		$content .= <<<FIN
<script>
$("#landowner").submit(function(){
	 $('#SUBMIT').prop("disabled", "disabled");
});
  $(document).on('keyup', '.pushed', function(){
	this.value = this.value.replace(/[^0-9\.-]/g,'');	
});
  $(document).on('change', '.sel.loctype', function(){
   var l = $(this).parents("div");
   var line = l.parents("div");
	var type = $(this).val();
	var dv = $(line).attr("id");
	var dvStr  = dv.toString();
	var arr = dvStr.split("_");
	changeLine(arr[2],type);
	
});
$(function () {
      $('#landowner_name').autocomplete({
         width: 700,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=landowner',
         minChars:1,
         onSelect: function(value, data){ $('#landowner_id').val(data['landowner_id']);$('#address').val(data['address']); }
      });
  });

</script>
FIN;


				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found landowner
				}
				 $content .= "\n<a href=\"landowner.php?action=list\"   class=\"button\"  >Last Screen</a>";
				 $content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllLOwners($search="") {
		  	$whereClause="";
         if (strlen($search) > 0 ) {
            $whereClause = " and upper(owner_name) like '$search%'";
         }
	
			$sql = "SELECT l.*,locs_from_land(l.landowner_id) as locs from landowner l where removed is false $whereClause order by l.owner_name";
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}
			$content ="<div style=\"width:1160px;margin:auto;\">";
			$content .= AdminFunctions::AtoZ("landowner.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:1.1rem;\"></div>\n";
			$content .= AdminFunctions::oneToNine("well.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:1.1rem;\"></div>\n";
			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"workscope  hd\">Land Owner Name</div>";
			$content .= "<div class=\"workscope  hd\">Address</div>";
			$content .= "<div class=\"workscope  hd\">Pits/Ponds/Lakes</div>";
			$content .= "<div class=\"links hd\"  style=\"width:156px;\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$landowner_id = $rs->fields['landowner_id'];
				$landowner_name = strlen($rs->fields['owner_name']) > 2 ? $rs->fields['owner_name'] : " ";
				$address = strlen($rs->fields['address']) > 2 ? $rs->fields['address'] : " ";
				$locs = $rs->fields['locs'];
				$style =   "";
				$linkstyle = "style=\"width:156px;\"";
				$lines =  ceil(strlen($rs->fields['locs']) / 50) - 2;  // expanding height divs
				if ($lines > 0 ) {
					$height = ($lines * 16) + 32;
					$style = "style=\"height:{$height}px;\"";
					$linkstyle = $linkstyle ."height:{$height}px;";
				}
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"workscope $ln highh\" $style ><a href=\"landowner.php?action=find&amp;landowner_id=$landowner_id\" title=\"View Land Owner\" class=\"name\" >$landowner_name</a></div>\n";
				$content .= "<div class=\"workscope $ln highh\" $style >$address</div>";
				$content .= "<div class=\"workscope $ln highh\" $style >$locs</div>";
				if ($this->sessionProfile > 2 ) {
           	$content .= "<div class=\"links $ln highh\" $linkstyle>\n";
					$content .= "<div style=\"margin:auto;width:100px;\" >\n";
					$content .= "<a href=\"landowner.php?action=update&landowner_id=$landowner_id\" title=\"Edit Land Owner\" class=\"linkbutton\" >EDIT</a>\n";
					$content .= "<a href=\"landowner.php?action=delete&landowner_id=$landowner_id\" title=\"Delete Land Owner\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
				}
				else {
					$content .= "<div class=\"links $ln highh\" $linkstyle> &nbsp;";
				}
				$content .= "</div></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getLOwnerDetails($lOwnerID) {
			$sql = "SELECT * from landowner  where landowner_id = $lOwnerID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->lOwnerID = $data['landowner_id'];
			$this->landownerName = $data['owner_name'];
			$this->address = htmlspecialchars($data['address']);
			$sql = "SELECT * from gravel_water_loc where landowner_id = $lOwnerID";

			if ( $this->locs = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());

				}
			}
		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteLOwner();
			}
			$this->lOwnerID = $_POST['landowner_id'];
			$this->landownerName = trim(pg_escape_string($_POST['landowner_name']));
			$this->address = trim(pg_escape_string(htmlspecialchars_decode($_POST['address'])));
			$this->locs = $_POST['location'];
      	$this->locType = $_POST['loc_type'];
	   	$this->locIDs = $_POST['loc_id'];
	   	$this->pushed = $_POST['pushed_up'];
	   	$this->nonPushed = $_POST['non_pushed_up'];
	   	$this->water = $_POST['water'];
			$this->submitForm();
		}
		private function deleteLOwner() {
			$sql = "UPDATE landowner set removed = TRUE where landowner_id = $this->lOwnerID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: landowner.php?action=list");
			exit;
		}
		private function submitForm() {
			if ($this->lOwnerID == 0 ) {
				$sql = "INSERT into landowner values (nextval('landowner_landowner_id_seq'),E'$this->landownerName',E'$this->address',FALSE) returning landowner_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->lOwnerID = $rs->fields['landowner_id'];
			}
			else { // Update
				$sql = "UPDATE landowner set owner_name = E'$this->landownerName',address = E'$this->address'  where landowner_id = $this->lOwnerID";
				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}

			}
		//  Locations	
			$found = false;
			$sqlIns = "INSERT into gravel_water_loc (landowner_id,loc_type,location,pushed_up_rate,non_pushed_up_rate,water_rate) values ";
			foreach ($this->locIDs as $ind=>$val) {
				$ltype = $this->locType[$ind];
				$loc = trim(pg_escape_string(htmlspecialchars_decode($this->locs[$ind])));
				$push = $ltype == 1  ? floatval($this->pushed[$ind]) : -1;
				$nopush = $ltype == 1 ? floatval($this->nonPushed[$ind]) : -1;
				$wat =  $ltype == 2  ? floatval($this->water[$ind]) : -1;

				if ($val > 0 ) {
					$sql = "UPDATE gravel_water_loc set loc_type = $ltype,location=E'$loc',pushed_up_rate=$push,non_pushed_up_rate=$nopush,water_rate=$wat where gravel_water_loc_id = $val";
					if (! $this->conn->Execute($sql)) {
						die( $this->conn->ErrorMsg());
					}

				}
				else {
					$found = true;
					$sqlIns .= "($this->lOwnerID,$ltype,E'$loc',$push,$nopush,$wat),";

				}
			}
			if ($found ) {
				$sqlIns = preg_replace('/,$/',"",$sqlIns);
				if (! $this->conn->Execute($sqlIns)) {
					die( $this->conn->ErrorMsg());
				}

			}

			header("LOCATION: landowner.php?action=list");
			exit;
			
		}
		private function pitsPonds($DISABLED) {
		$content = <<<FIN
<fieldset ><legend style="margin-left:45%;">Pits/Ponds/Lakes</legend>
<div class="heading"  >
<div class="vehicle hd nopad" >Location Type</div>
<div class="location hd nopad" >Location</div>
<div class="usr  hd nopad" >Pushed Up Rate &nbsp;m3</div>
<div class="usr  hd nopad" >Non Pushed Up Rate</div>
<div class="usr  hd nopad" >Water Rate &nbsp;kL</div>
<div class="links  hd" style="width:80px;" >Actions</div>
</div>
<div id="pit_div" >
FIN;

	$butt = "<button onclick=\"addPit();return false;\" class=\"button tiny margt4\"  >Add Location</button>\n";
			if ($this->action == "new" ) {
				$content .= <<<FIN
<div id="loc_div_1" >
<input type="hidden" id="linedoc" value="1" />
<input type="hidden" name="loc_id[]" value="0" />
<div class="vehicle nopad" >
FIN;
		$content .= AdminFunctions::locTypeSelect(1,$DISABLED);
		$content .= <<<FIN
</div>
<div class="location nopad" ><input type="text" class="loc" name="location[]" /></div>
<div class="usr nopad" ><input type="text" class="pushed up cntr" name="pushed_up[]" id="pushed_up_1"  value="" placeholder="0" /></div>
<div class="usr nopad" ><input type="text" class="pushed non cntr" name="non_pushed_up[]" id="non_push_1" value="" placeholder="0" /></div>
<div class="usr nopad" ><input type="text" class="pushed water cntr" name="water[]" value=""  id="water_1"  placeholder="0" /></div>
<div class="links bdb"   style="width:80px;" > <button class="linkbutton margt5 bckred"  onclick="delGrav(0, '#loc_div_1');return false;"  $DISABLED >Delete</button> </div>
</div>
</div>
<button onclick="addPit();return false;" class="button tiny margt4"  >Add Location</button>
FIN;
			} 
			else {  // find/update
				if ($this->action == "find" ) {
					$butt = "";
				}
				$x = 1;
				foreach($this->locs as $ind=>$val) {
					extract($val);
            	$location = htmlspecialchars($location);
					$pushed_up_rate = $pushed_up_rate != -1 ? $pushed_up_rate : "N/A";
					$non_pushed_up_rate = $non_pushed_up_rate != -1 ? $non_pushed_up_rate : "N/A";
					$water_rate = $water_rate != -1 ? $water_rate : "N/A";
					if ($loc_type == 1) {
						$watReadOnly = "readonly";
						$gravReadOnly = "";
					}
					else {
						$watReadOnly = "";
						$gravReadOnly = "readonly";
					}
					$content .= <<<FIN
<div id="loc_div_$x" >
<input type="hidden" id="linedoc" value="$x" />
<input type="hidden" name="loc_id[]" value="$gravel_water_loc_id" />
<div class="vehicle nopad" >
FIN;
					$content .= AdminFunctions::locTypeSelect($loc_type,$DISABLED);
					$content .= <<<FIN
</div>
<div class="location nopad" ><input type="text" class="loc $DISABLED" name="location[]" value="$location"  $DISABLED/></div>
<div class="usr nopad" ><input type="text" class="pushed up cntr $DISABLED" name="pushed_up[]" id="pushed_up_$x"  value="$pushed_up_rate"  $DISABLED  $gravReadOnly /></div>
<div class="usr nopad" ><input type="text" class="pushed non cntr $DISABLED" name="non_pushed_up[]" id="non_push_$x" value="$non_pushed_up_rate"  $DISABLED  $gravReadOnly /></div>
<div class="usr nopad" ><input type="text" class="pushed water cntr $DISABLED" name="water[]" value="$water_rate"  id="water_$x"  $DISABLED $watReadOnly /></div>
<div class="links bdb"   style="width:80px;" >
FIN;
					if ($this->action == "update" ) {
						$content .= "<button class=\"linkbutton margt5 bckred\"  onclick=\"delGrav($gravel_water_loc_id, '#loc_div_$x');return false;\"  $DISABLED >Delete</button>\n";
					}
					else {
						$content .= "&nbsp;";
					}
					$content .= "</div></div>\n";
					$x++;
         	}
				$content .= "</div>\n"; // close pit_div
				if ($this->action == "update" ) {
					$content .= "$butt\n";
				}
			}


			$content .= "</fieldset>\n";
			return $content;
		}
	}
?>
