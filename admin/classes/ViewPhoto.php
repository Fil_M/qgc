<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class ViewPhoto  {
		public $page;
		public $requestID;
		public $requestIDAvail;
		public $fieldEstID;
		public $empID;
		public $contractorID = 1;  // Harcode Corbets
		public $action;
		public $sessionProfile;
		public $conn;
		public $wellID;
		public $crewID;
		public $wellName;
		public $areaID;
		public $areaName;
		public $empName;
		public $hoursTot;
		public $tot;
		public $siteInsNo;
		public $fieldLines;	
		public $photoLines;
		public $equalto;
      public $between;
      public $hideEndDate='style="display:none;"';
		public $equalgreater;
		public $creDate;
		public $endDate;

		public function	__construct() {
			$this->conn = $GLOBALS['conn'];
			//$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('view');
				$this->getPhotoLines();
				$this->page= $pg->page;
				$heading_text = "Administration<BR />";
				$heading_text .= "View Photos";
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent();	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent() {
			$content = "";
			//$URL = "https://corbets.ntd.com.au";  // TODO  hardcoded
			$URL = "https://corbetsqgc2.devel.ntd.com.au";  // TODO  hardcoded
    		if (count($this->photoLines) > 0 ) {
				foreach ($this->photoLines as $ind=>$val) {
					if (strlen($val['photo_name'])> 0 ) {
						$file = $val['photo_name'];
						$id = $val['field_photo_id'];
						$photo = "<a href=\"$URL/photos/{$file}\" class=\"tiny\" target=\"_blank\" >$file</a>\n";
						$content .= "$photo &nbsp;&nbsp; \n";
					}
				}
			}
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getPhotoLines() {
			$sql = "SELECT * from field_photo order by photo_name";
			if (!$this->photoLines = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
		}

	}
?>
