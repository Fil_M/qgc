<?php
 	class Calendar {
		public $conn;
		public $page;
		public $pageType='index';
		public $clientID;
		public $employeeID;
		public $vehicleID;

		public function	__construct($client_id=0,$employee_id=0,$vehicle_id=0) {
			$this->conn = $GLOBALS['conn'];
			$this->clientID = $client_id;
			$this->employeeID = $employee_id;
			$this->vehicleID = $vehicle_id;
			$pg = new Page('calendar',$client_id,$employee_id,$vehicle_id);
			$this->page= $pg->page;
		 	$this->setHeaderText();	
		 	$this->setContent();	
			echo $this->page;
		}

		private function setHeaderText() {
			$header_text = "Administration<BR />Calendar";
			if ($this->clientID > 0 ) {
				$clientName = AdminFunctions::clientName($this->clientID);
				$header_text .= "<br />Client - $clientName";
			}
			if ($this->employeeID > 0 ) {
				$empName = AdminFunctions::employeeName($this->employeeID);
				$header_text .= "<br /> Employee - $empName";
			}
			if ($this->vehicleID > 0 ) {
				$vRego = AdminFunctions::vehicleRego($this->vehicleID);
				$header_text .= "<br /> Vehicle - $vRego";
			}
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent() {
			$content = <<<FIN
<div id='loading' style='display:none'>loading...</div>
<div id='calendar'></div>
FIN;

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}

	}
?>
