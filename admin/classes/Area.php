<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Area  {
		public $page;
		public $areaID;
		public $conID;
		public $areaName;
		public $action;
		public $sessionProfile;
		public $conn;
		public $wells;

		public function	__construct($action,$areaID=0,$search="") {
			$this->areaID = $areaID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('area');
				$this->page= $pg->page;
        		switch($action) {
					 case "new" :
                  $heading_text = "Add New Area";
                  break;
					case "list" :
						$heading_text =  strlen($search) > 0 ? "List Areas - $search" : "List Areas - All";
						$this->getAllAreas($search) ;
						break;
					case "find" : 
						$heading_text = "Area $areaID";
						$this->getAreaDetails($areaID);
						break;
					case "update" :
               	if ($areaID < 1 ) {
                  	return false;
               	}
              		$heading_text = "Updating Area $areaID";
              		$this->getAreaDetails($areaID);
              	break;
            	case "delete" :
               	if ($areaID < 1 ) {
                  	return false;
               	}
             		$heading_text = "Delete Area $areaID";
             		$this->getAreaDetails($areaID);
             	break;
					default:
						$heading_text = "List Areas - All";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
            $this->processPost();
         }

		}
		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->areaID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					case "new":
                  $button = "Add Area";
                  break;
					case "update":
                  $button = "Update Area";
                  break;
               case "delete":
                  $button = "Delete Area";
                  break;
					default:
						$button = "No Button";
						break;
				}
			$content = <<<FIN
<div style="width:40%;margin:auto;">
<form name="area" method="post" action="area.php">
<input type="hidden" name="area_id" value="$this->areaID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="width:83%;"><legend style="margin-left:49.5%;">Area</legend>
<div class="div55" ><label for="area_name" >Area Name<input type="text" name="area_name" id="area_name"  value="$this->areaName" $DISABLED /></label></div>
<div class="div40 marg">
FIN;
$content .= AdminFunctions::contractorSelect($this->conID,$DISABLED,true);
$content .= <<<FIN
</div>
<div style ="clear:both;height:10px;" > </div>
<div class="div95" ><label for="wells" >Wells<textarea   id="wells" class="txt" style="width:100%;"  DISABLED  >$this->wells</textarea></label></div>
</fieldset>
FIN;
				if ($this->action != "find" ) {
              	$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"button\" />"; // no submit for found area
        		}
				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllAreas($search="") {
		  	$whereClause="";
         if (strlen($search) > 0 ) {
            $whereClause = " and upper(a.area_name) like '$search%'";
         }
	
			$sql = "SELECT a.*,wells(a.area_id) as wells from area a where removed is false $whereClause order by a.area_name";
			if (! $rs=$this->conn->Execute($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->ErrorMsg());
				}
			}
			$content ="<div style=\"width:1160px;margin:auto;\">";
			$content .= AdminFunctions::AtoZ("area.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:15px;\"></div>\n";
			$content .= AdminFunctions::oneToNine("area.php?action=list",$search);
			$content .= "<div style=\"clear:both;height:20px;\"></div>\n";
			$content .= "<div class=\"heading\"  ><div class=\"addr hd\">Area Name</div>";
			$content .= "<div class=\"well wide hd\">Wells</div>\n";
			$content .= "<div class=\"links hd\" style=\"width:151px;\" >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "line2";
				$area_id = $rs->fields['area_id'];
				$area_name = strlen($rs->fields['area_name']) > 2 ? $rs->fields['area_name'] : " ";
				$wells = trim($rs->fields['wells']);
				$linkstyle = "style=\"width:151px;\"";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
				$content .= "<div class=\"addr $ln highh\" ><a href=\"area.php?action=find&amp;area_id=$area_id\" title=\"View Area\" class=\"name\" style=\"color:black;\" >$area_name</a></div>\n";
				$content .= "<div class=\"well wide $ln highh\" $ >$wells</div>\n";
				if ($this->sessionProfile > 2 ) {
            $content .= "<div class=\"links $ln highh \" $linkstyle >\n";
				$content .= "<div style=\"margin:auto;width:100px;\" >\n";
				$content .= "<a href=\"area.php?action=update&area_id=$area_id\" title=\"Edit Area\" class=\"linkbutton\" >EDIT</a>\n";
				$content .= "<a href=\"area.php?action=delete&area_id=$area_id\" title=\"Delete Area\" class=\"linkbutton bckred\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
            }
            else {
               $content .= "<div class=\"links $ln highh\" $linkstyle> &nbsp;";
            }
				$lineNo += 1;
				$content .="</div></div>\n";
				$rs->MoveNext();
			}
			$content .= "<hr /></div>";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getAreaDetails($areaID) {
			$sql = "SELECT a.*,wells(a.area_id) as wells  from area a  where area_id = $areaID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->areaID = $data['area_id'];
			$this->areaName = $data['area_name'];
			$this->wells = $data['wells'];
			$this->conID = $data['contractor_id'];

		}
		 private function processPost() {
         if ($this->action == "delete" ) {
            $this->deleteArea();
         }
         $this->areaID = $_POST['area_id'];
         $this->areaName = trim(addslashes($_POST['area_name']));
         $this->contractorID = intval($_POST['contractor']);
         $this->submitArea();
      }
      private function deleteArea() {
         $sql = "UPDATE area set removed = TRUE where area_id = $this->areaID";
         if (! $this->conn->Execute($sql)) {
            die($this->conn->ErrorMsg());
         }
         header("LOCATION: area.php?action=list");
         exit;
      }
      private function submitArea() {
         if ($this->areaID == 0 ) {
            $sql = "INSERT into area values (nextval('area_area_id_seq'),E'$this->areaName',FALSE,$this->contractorID) returning area_id";

            if (! $rs =  $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }
            $this->areaID = $rs->fields['area_id'];
         }
         else { // Update
            $sql = "UPDATE area set area_name = E'$this->areaName',contractor_id = $this->contractorID  where area_id = $this->areaID";
            if (! $this->conn->Execute($sql)) {
               die( $this->conn->ErrorMsg());
            }

         }

         header("LOCATION: area.php?action=find&area_id=$this->areaID");
         exit;

      }

	}
?>
