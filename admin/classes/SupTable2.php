<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class SupTable2  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $contractorName;
		public $shortName;
		public $conDomain;
		public $empName;
		public $approvalDate;
		public $qgcSignature;
		public $hour;
		public $dbLink;
		public $notifyCon;
		public $cooArr;
		//public $statusArr= array("0"=>"#e1b500","1"=>"#60854e","2"=>"#ef3e55");
		public $statusArr= array("0"=>"#ecefbe","1"=>"#efd38f","2"=>"#ef3e55","3"=>"#e1c0ef","4"=>"#ef8e8e","5"=>"#cdefbd","6"=>"#c0dcef","7"=>"#b4b4b4");




		public function	__construct($action,$conID=NULL,$startDate=NULL,$endDate=NULL) {
			$this->action = $action;
			//$this->startDate= !is_null($startDate)  ?  $startDate : AdminFunctions::yesterday();
			$this->startDate= !is_null($startDate)  ?  $startDate : date('d-m-Y');
         $this->endDate= ! is_null($endDate) && strlen($endDate) > 0  ?  $endDate  :  $startDate;
		 	$this->contractorID=$conID;
			$this->empID = intval($_SESSION['employee_id']);
			$this->empName = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if ($this->sessionProfile < 16 ) {
				header("LOCATION: index.php");
				exit;
			}
			$this->conn = $GLOBALS['conn'];
			if (! is_null($conID)) {
           	$connArr = AdminFunctions::getConName($conID);
           	$this->contractorName = $connArr['con_name'];
           	$this->shortName = $connArr['name'];
           	$this->conDomain = $connArr['domain'];
           	$this->dbLink = $connArr['db_link'];
         }
			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			elseif (isset($_REQUEST['search'])) {
				$pg = new Page('table2');
				$this->page= $pg->page;
				$heading_text = "Approve Table 2 Claim - $this->contractorName for $this->startDate";
				$button = "<input type=\"submit\" name=\"search\" value=\"Search\" class=\"button tiny\" />\n";
				$this->setHeaderText($heading_text);
				$this->approvalDate = date('d-m-Y');
				$this->showSummary();
				echo $this->page;
			}
			else {
				$pg = new Page('table2');
				$this->page= $pg->page;
				$heading_text = "Approve Table 2 Claim - $this->contractorName for $this->startDate";
				$button = "<input type=\"submit\" name=\"search\" value=\"Search\" class=\"button marg\" />\n";
				$this->setHeaderText($heading_text);
		 		$this->setContent($action,$button);	

				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:800px;margin:auto;">
<form name="suptable2" id="supreport" method="post" action="sup_table2.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<fieldset ><legend style="margin-left:40%;"> Filter Search - Table 2 Claim</legend>
<div class="div29">
FIN;
   $content .= AdminFunctions::contractorSelect($this->contractorID,"",false);
	$content .= <<<FIN
</div>
<div class="div15 marg10" ><label >Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label> </div>
<div style="float:right;width:35%;" >
<button class="reset" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div> 
</fieldset>
</form>
</div>
<script>
$("#suptable2").submit(function(){
   var isFormValid = true;
   $("#wellreport input:text.input.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#supreport .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
	 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getDetails(){
      	$sql = "Select a.area_name,pt.p_type,sc.sub_crew_name,
     	 	h.hour_id,h.hour_date,h.status,docket_hours_t2,h.rate,total_t2,wells_from_ids(h.well_ids) as well_name,h.area_id,h.crew_id,h.details,h.calloff_order_id,c.crew_name,h.price_type,sr.reason,h.qgc_approver_id,el.fault
			FROM {$this->shortName}_hour h
      	LEFT JOIN {$this->shortName}_plant p  on h.plant_id = p.plant_id
      	LEFT JOIN plant_type  pt using(plant_type_id) 
      	LEFT JOIN area a using (area_id)
      	LEFT JOIN crew c on c.crew_id = h.crew_id
      	LEFT JOIN sub_crew sc on sc.sub_crew_id = h.sub_crew_id
			LEFT JOIN standdown_reason sr using(standdown_reason_id) 
			 LEFT JOIN email_log el on el.email_log_id = 
            ( select max(email_log_id) from email_log  ell where  ell.dkt_date = '$this->startDate' and ell.receiver_id = $this->contractorID and receiver_type = 'TB2' )
      	WHERE  h.hour_date = '$this->startDate'  and total_t2 > 0  and h.removed is false  
      	order by h.calloff_order_id,a,area_name,well_name,h.hour_date ";


      	if (! $this->data = $this->conn->getAll($sql)) {
         	if ($this->conn->ErrorNo() != 0 ) {
            	die($this->conn->ErrorMsg());
         	}
      	}
		}
	private function showSummary() {
			$URL="https://".$_SERVER['HTTP_HOST'];
			$this->getDetails();
			$this->getSig();
		 	$emailButTxt = "Email";
         $emailButClass = "";
			$showApprove = $showNotify = false;
			$notifyChecked = "";
			$supURL = "{$URL}/admin/sup_table2.php?action=list&con_id=$this->contractorID&startdate=$this->startDate&search=true";
         $_SESSION['last_request'] = $supURL;


			$content = <<<FIN
<div style="width:1423px;margin:auto;">
<form name="suptable2" id="supreport" method="post" action="sup_table2.php?action=$this->action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="signature" value="$this->qgcSignature" />
<input type="hidden" name="empID" id="empID" value="$this->empID" />
<input type="hidden" name="startdate" id="startdate" value="$this->startDate" />
<input type="hidden" name="conID" id="conID" value="$this->contractorID" />
<div class="heading"  >
<div class="time hd" >C.O.O.</div>
<div class="cli  hd" >Work Scope</div>
<div class="cli  hd" >Sub Scope</div>
<div class="vehicle  hd" >Area</div>
<div class="cli hd" >Well/s</div>
<div class="cli  hd" >Plant Type</div>
<div class="cli hd" >Reason</div>
<div class="rate hd" >Hours</div>
<div class="rate hd bdr" >Rate</div>
</div>
FIN;
		if (count($this->data) > 0 ) {
			foreach ($this->data as $ind=>$val) {
			 	$status = intval($val['qgc_approver_id']) > 0 ? 5 : 1;
				if (! $showApprove && $status == 1 ) {  // only show approve button if  any non  superintendent approved hours found
					$showApprove = true;
				}
				else {
					$showNotify = true;
				}
      		$color = $this->statusArr[$status];
      		$yesChecked = $status == 5 ? "checked" : "";
      		$noChecked =  ($status == 2 || $status == 4 ) ? "checked" : "";
				$hourID = $val['hour_id'];
				$coo = $val['calloff_order_id'];
				$area = $val['area_name'];
				$well = $val['well_name'];
				$crew = $val['crew_name'];
				$sub_crew = $val['sub_crew_name'];
				$pType = $val['p_type'];
				$units = sprintf('%1.2f',$val['docket_hours_t2']);
				$rate = sprintf('%01.2f',$val['rate']);
				$sreason = $val['reason'];
				$fault = ! is_null($val['fault']) ? preg_match('/OK/',$val['fault']) : -1;  // JOIN on email_log
            if ($fault > 0  ) {
               $emailButTxt = "Emailed";
               $emailButClass = "emailed";
					$notifyChecked = "checked";
            }

				$content .= "<div id=\"line_$hourID\" style=\"background:$color;border-bottom:1px solid #000000;float:left;width:100%;display:table;table-layout:fixed;\" >\n";
            $content .= "<input type=\"hidden\" id=\"hour_$hourID\" value=\"$hourID\" name=\"hour[]\" />";
            $content .= "<input type=\"hidden\"  value=\"$coo\" name=\"coo[]\" />";
				$content .= "<div class=\"time highh\" >$coo</div>";
				$content .= "<div class=\"cli highh\" >$crew</div>";
				$content .= "<div class=\"cli highh\" >$sub_crew</div>";
				$content .= "<div class=\"vehicle highh\" >$area</div>";
				$content .= "<div class=\"cli highh\" >$well</div>";
				$content .= "<div class=\"cli highh\" >$pType</div>";
				$content .= "<div class=\"cli highh\" >$sreason</div>";
				$content .= "<div class=\"rate cntr highh\" >$units</div>";
				$content .= "<div class=\"rate cntr bdr highh\" >$rate</div>";
				$content .= "</div>\n";
			}

		}
		if ($this->sessionProfile >= 16 ) {
         $content .= <<<ENDL
<div class="div95 cntr" style="padding:1rem 0 1rem 0;"  ><span>I hereby approve the Stand Down claim above.<span></div>
<fieldset ><legend style="margin-left:48%">Signed for QGC</legend>
<div class="div24" ><img src="$this->qgcSignature"  alt="QGC to Sign"  height="180" width="300" /></div>
<div class="div25 marg1" >
	<div class="div35 marg2" ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"  class="cntr"  value="$this->approvalDate" readonly/></label></div>
ENDL;
			if ( $showNotify  ) {
				$content .= "<div class=\"div35 marg10\" ><label >Notify Contractor</label></div><div class=\"div7\" ><input type=\"checkbox\" name=\"send_email\" value=\"true\" $notifyChecked /></div>\n";
			}
         $content .= <<<ENDL
	<div class="div93 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->empName"  disabled /></label></div>
</div>
ENDL;
		$content .= "</fieldset>\n";
		}
		if ($this->action != "find" ) {
			if ($showApprove) {
          	$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"Approve\" class=\"button marg2\" />"; // no submit for found cost
			}
      }  
		$content .= "<a href=\"{$URL}/printSupTable2.php?action=print&con_id=$this->contractorID&startdate=$this->startDate\"  class=\"button marg2\"  >Print PDF</a>\n"; 
		if ($this->sessionProfile >= 16 ) {
			if ( $showNotify  && ! $showApprove  ) { 
				$content .= "<button  class=\"button $emailButClass marg2\"  onclick=\"emailT2();$(this).prop('disabled','disabled');return false;\">$emailButTxt</button>\n";
			}
		}
		 $content .= "\n<a href=\"{$URL}/admin/sup_table2.php?action=list\"   class=\"button marg2\"  >Last Screen</a>";

		$content .= "</form>";
		$content .= "</div>\n";
	 	$this->page = str_replace('##MAIN##',$content,$this->page);




	}
		private function getSig() {
         $sql = "SELECT signature from employee where employee_id = $this->empID";
         if (! $this->qgcSignature = $this->conn->getOne($sql)) {
            echo $sql;
            die( $this->conn->ErrorMsg());
         }
      }

		private function processPost() {
			$URL="https://".$_SERVER['HTTP_HOST'];
			$this->startDate = $_POST['startdate'];
			$this->contractorID = $_POST['conID'];
			$this->hour = $_POST['hour'];
			$this->empID = intval($_POST['empID']);
			$this->approvalDate =  $_POST['approvaldate'];
			$this->notifyCon = isset($_POST['send_email']) ? true  : false;
			$this->cooArr = array_unique($_POST['coo']);

			$inClause = AdminFunctions::inClauseFromArray($this->hour);
			$supURL = "{$URL}/admin/sup_table2.php?action=list&con_id=$this->contractorID&startdate=$this->startDate&search=true";
			$_SESSION['last_request'] = $supURL;
			$sql = "UPDATE hour set qgc_approver_id = $this->empID,qgc_approval_date = '$this->approvalDate' where hour_id in $inClause "; 
			$res = AdminFunctions::execCon($this->dbLink,$sql);

			if ($this->notifyCon ) {
				//Super "locks" docket_day with area/well_ids/Crew  and status 99  to prevent futher hour entry
				$sql = "INSERT into docket_day (docket_date,status,crew_id,area_id,calloff_order_id,well_ids) values   ";
				foreach ($this->cooArr as $ind=>$coo ) {
				$dktArr = AdminFunctions::getAllFromCoo($coo);
					$crewID = $dktArr['crew_id'];
					$areaID = $dktArr['area_id'];
					$wellIDS = $dktArr['well_ids'];
					$sql .= "('$this->startDate',99,$crewID,$areaID,$coo,'$wellIDS'),";	
				}
				$sql = preg_replace('/,$/',"",$sql);
				$res = AdminFunctions::execCon($this->dbLink,$sql);
				//$eNum = AdminFunctions::insertEmailLog($this->empID,1,0,"Approval Summary Of Table 2 Hours",'','TB2',"NULL","NULL",$this->startDate);
             header("LOCATION: {$URL}/printSupTable2.php?action=email&con_id=$this->contractorID&startdate=$this->startDate");
             exit;
			}
			else {
				header("LOCATION: $supURL");
				exit;
			}

		}
 }
?>
