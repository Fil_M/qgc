<?php  // QGC
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class GravelWater  {
		public $page;
		public $action;
		public $sessionProfile;
		public $gravelID;
		public $landOwnerID;
		public $contractorID;
		public $areas;
		public $wells;
		public $details;
		public $areaID=0;
		public $wellID;
		public $crewID;
		public $cooID;
		public $areaName;
		public $statusArr= array("0"=>"#ecefbe","1"=>"#efd38f","2"=>"#ef3e55","3"=>"#e1c0ef","4"=>"#ef8e8e","5"=>"#cdefbd","6"=>"#c0dcef","7"=>"#b4b4b4");
		public $status=NULL;
		public $docketNo;
		public $conn;
		public $equalgreater;
		public $equalto; 
		public $between;
		public $less;
		public $hideEndDate='style="display:none;"';
		public $endDate;
		public $gravelIDs;
		public $employeeID;
		public $dktFrom;
		public $dktTo;
		public $multi;
		public $cooDivTxt="";
		public $upload;
		public $uploadID;
		public $uploadType;
		public $pushedRate;
		public $nonPushedRate;
		public $waterRate;
		public $gravelLoc_1;
		public $waterLoc_2;
		public $pushed;
		public $unitGrav;
		public $subGrav;
		public $unitWater;
		public $subWater;
		public $gDates;
		public $shortName;
		public $conDomain;
		public $contractorName;
		public $dbLink;
		public $ownerID;
		public $csv = array();


		public function	__construct($action,$gravelID=0,$conID=NULL,$search=NULL,$redo=NULL,$order=NULL) {
			$this->action = $action;
			$this->gravelID = $gravelID;
			$this->contractorID = $conID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
			$this->conn = $GLOBALS['conn'];
			$this->gDate = date('d-m-Y');  // no separators
			$pg = new Page('gravel_water');
			$this->page= $pg->page;
			if (! is_null($conID)) {
            $connArr = AdminFunctions::getConName($conID);
            $this->contractorName = $connArr['con_name'];
            $this->shortName = $connArr['name'];
            $this->conDomain = $connArr['domain'];
            $this->dbLink = $connArr['db_link'];
         }
         switch($action) {
				case "list" :
					$heading_text =   "List Gravel/Water";
					$this->getAllGravels($search,$redo,$order) ;
					break;
				case "find" :
					$heading_text =   "Find Gravel/Water <br /> $this->gravelID";
					$this->getGravelDetails() ;
					break;
				
				default:
					$heading_text = "List Gravel/Water";
					break;
			}
		 	$this->setHeaderText($heading_text);	
		 	$this->setContent($action,$redo);	
			echo $this->page;
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$redo) {
			if ($action != "list" || ($action == "find" && $this->gravelID > 0 )) {
				$DISABLED= $UPDATEDISABLED = "";
				switch ($action) {
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						$wellStr = $this->getCOODetails($this->cooID,$DISABLED);
						break;
					default:
						$button = "List Gravel/Water";
						break;
				}
			$content = <<<FIN
<div style="width:97%;margin:auto;">
<form name="gravel" id="gravel" method="post" action="gravel_water.php?SUBMIT=SUBMIT" enctype="multipart/form-data" >
<input type="hidden" name="action" value="$this->action" />
<input type="hidden" name="status" value="$this->status" />
<input type="hidden" name="gravel_id" value="$this->gravelID" />
<input type="hidden" name="employeeID" value="$this->employeeID"   />
FIN;
	if ($action == "update" ) {  // UPDATEDISABLED loses  disabled $_POST values so set them here
	$content .= <<<FIN
<input type="hidden" name="docketno" value="$this->docketNo" />
<input type="hidden" name="hdate" value="$this->gDate"  />
<input type="hidden" name="upload_id" value="$this->uploadID"  />
FIN;
	}
$content .= <<<FIN
<fieldset ><legend style="margin-left:800px;">Gravel/Water</legend>
<label  class="label abn">C.O.O.</label><input type="text" name="coo" id="coo_1" class="input required date  $DISABLED" value="$this->cooID"  $DISABLED  readonly />
<label for="docketno" class="label sml"  >Docket</label><input type="text" name="docketno" id="docketno" class="input  required abn $DISABLED"  value="$this->docketNo" $DISABLED  />
<label for="docketno" class="label abn"  >Crew</label>
FIN;
$content .= AdminFunctions::crewSelect($this->crewID,"",true);

$content .= <<<FIN
<label class="label tiny" >Area</label><input type="text" name="area[]" id="area_1"  class="input area required $DISABLED"  value="$this->areaName"  $DISABLED />
<input type="hidden" name="areaID[]"  id="areaID_1" value="$this->areaID" />
<label class="label sml">Well/s</label>
$wellStr
<label class="label sml"  >Total M3</label><input type="text" name="gravel" id="gravel_tot" class="input litres  disabled"   readonly  />
<label class="label med"  >Total kiloLitre</label><input type="text" name="water" id="water_tot" class="input litres  disabled"   readonly  />
<div id="graveldiv" style="width:100%;" ></div>
</fieldset>
FIN;
	$content .= <<<FIN
<fieldset style="margin-top:15px;"><legend style="margin-left:800px;">Docket Lines</legend>
<div id="linediv" >
<div id="line_1" >
<input type="hidden" id="lineno" value="1" />
<input type="hidden" name="gravelIDs[]"  value="$this->gravelID" />
FIN;
   $content .= "</div>\n";
	$content .= "<div style=\"clear:both;height:10px;\"></div>\n";

$content .= "<div style=\"clear:both;height:5px;\"> </div>\n";
$content .= "<label  class=\"label abn\">Date</label><input type=\"text\" name=\"gdate[]\"  id=\"gdate_1\" class=\"input required date\" value=\"$this->gDate\" />\n";
$content .= "<div style=\"float:left;\" id=\"grav_1\" >\n";
$content .= "<label class=\"label sml\"  >Pushed</label>\n";
$content .= AdminFunctions::pushedSelect($this->pushed,"",1);
$content .= <<<FIN
<label class="label ninety"  >Gravel m3</label><input type="text" name="un_gravel[]" id="gravel_1" class="input grvl litres" value="$this->unitGrav" />
<input type="hidden" name="grav_subtot[]" id="grav_subtot_1" value="$this->subGrav"  />
</div>
<div style="float:left;" id="wat_1" >
<label class="label smll"  >Water kL</label><input type="text" name="un_water[]" id="water_1" class="input wtr litres"  value="$this->unitWater" />
<input type="hidden" name="water_subtot[]" id="water_subtot_1" value="$this->subWater"  />
</div>
<div style="clear:both;height:5px;"> </div>
FIN;
$content .= <<<FIN
<label for="upload_1" class="label sml">Upload</label><input type="file" name="upload[]" id="upload_1" class="input" style="border:1px solid #000000;width:400px;"  />
<label  class="label med">Upload Type</label>
FIN;
$content .= AdminFunctions::gravelWaterSelect($this->uploadType,$DISABLED);
$content .= <<<FIN
<label for="prev_upload" class="label wide">Previous Upload</label><input type="text" id="prev_upload" class="input other" value="$this->upload" disabled />
<div style="clear:both;height:5px;"> </div>
<hr>
</div>
FIN;
	if ($this->action == "new" ) {
		$content .= "<button class=\"addButton\" id=\"addbutton\" onclick=\"addGravelLine();return false;\" >Add Docket Line</button>\n";
	}
$content .= "</fieldset>\n";
$content .= <<<FIN
<script>
var tabKey = 9;
$(document).on('keyup','#gdate',function () { this.value = this.value.replace(/[^0-9-]/g,''); });
jQuery('#gdate').focusout(function () { var gdt =this.value; var pattern =/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/;
    if (gdt == null || gdt == "" || !pattern.test(gdt)) {
       $('#gdt').focus(); alert("Invalid date format"); return false; 
    }
    else{
		document.cookie="gdate=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
		document.cookie="gdate=" + gdt;
      return true;
    } 
});
$(document).on('keydown', 'input', function(e, ui) {
    if(e.keyCode === 13 ){
			var sb = $(this).attr('id');
		 if (sb != 'SUBMIT' ) {
        	e.preventDefault();
       	$(this).nextAll('input:visible').eq(0).focus();
		 }
    }
});
$(function () {
      $('#area_1').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID_1').val(data);childSelect(data,'wellgravel',$('#welldiv_1'),'#conID'); return false; }
      });
  });
$("#gravel").submit(function(){
   var isFormValid = true;
 	isFormValid = checkInputs();
	if (!isFormValid ) {
    	return isFormValid;
	}
    $("#gravel input:text.input.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
	 $("#gravel .sel.required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
	 if (!isFormValid) {
			alert("Please fill in all the required fields (highlighted in red)"); 
    		return isFormValid;
	 }
	 else {
	   $('#SUBMIT').prop("disabled", "disabled");
    	return true;
	 }
});
$(document).on('keyup','.input.litres',function() { this.value = this.value.replace(/[^0-9\.]/g,''); });
$(document).on('change','.sel.pushed',function() { calcGravel(1); });
$(document).on('keyup', '.input.grvl',function() { calcGravel(1); $(this).focus(); });
$(document).on('keyup', '.input.wtr',function() { calcGravel(2); $(this).focus(); });
$(document).on('change','.sel.creww',function() { getGravelCOO($(this)); });
$(document).ready(function() { getGravelCOO($('#crew_1')); });
</script>
FIN;
				if ($this->status == 2 ) { // Reject
					$content .= <<<FIN
<fieldset style="margin-top:15px;background:#ffe3e3;"><legend style="margin-left:620px;color:#ff0000;">REJECTED - This entry has been rejected by QGC</legend>
<label class="label" >Reason for Rejection</label><textarea class="txt disabled" disabled >$this->rejectText</textarea>
<div style="clear:both;" > </div>
</fieldset>
FIN;
				}

				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" id=\"SUBMIT\" value=\"$button\" class=\"submitbutton\" style=\"margin:0px;width:240px;\"/>"; // no submit for found mill
				}
				$content .= "\n<a href=\"gravel_water.php?action=list&redo=redo\"   class=\"submitbutton\" style=\"height:26px;line-height:26px;width:200px;\">List Gravel/Water</a>";
				$content .= "\n</form></div>\n";
			}

			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
      private function fillDefaults($search) {
			if (isset($search->operatorID) && $search->operatorID > 0 ) {
				$this->operatorID = $search->operatorID;
				$this->operatorName = $search->operatorName;
			}
			if (isset($search->areaID) && $search->areaID  > 0 ) {
				$this->areaID = $search->areaID;
				$this->areaName = $search->areaName;
			}
			if (isset($search->wellID) && $search->wellID > 0 ) {
				$this->wellID = $search->wellID;
				$this->wellName = $search->wellName;
			}
			
			if (isset($search->crewID) && $search->crewID > 0 ) {
				$this->crewID = $search->crewID;
			}
			if (isset($search->gDate) && strlen($search->gDate) > 0 ) {
				$this->gDate = $search->gDate;
			}
		}
	
		private function getAllGravels($search,$redo,$order) {
			$URL="https://".$_SERVER['HTTP_HOST'] ;
			$totGravels = $gTot = 0.0;
			$CHECKED = "";
			$this->gravelID = NULL;
			$dateSelected = false;
			//$whereClause="where h.removed is false and h.well_id = w.well_id ";
			$whereClause="where 1 = 1 ";
			$dateClause = "";
			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
				$_SESSION['last_request'] = "{$URL}/admin/gravel_water.php?action=list&redo=redo";
				if (!is_null($order) && strlen($order) > 0) {
            	$search->order = $order;
          	}
				$_SESSION['SQL'] = pg_escape_string(serialize($search));
				if (!empty($search->ID) ) {
					$this->gravelID = $search->ID;
					$whereClause = " WHERE gravel_water_id = $this->gravelID ";
				} 
				else if (!empty($search->hDate)) {
						$dateSelected = true;
						$dateClause = "WHERE  g.gravel_date ";
               	$this->gDate = $search->hDate;
               	switch ($search->dateType) {
                  	case "equalto":
                     	$dateClause .= "  =  '$search->hDate' ";
                     	$this->equalto = "SELECTED=\"selected\"";
								$showApproval = true;
                     	break;
                  	case "equalgreater":
                     	$dateClause .= "  >=  '$search->hDate' ";
                     	$this->equalgreater = "SELECTED=\"selected\"";
                     	break;
                  	case "less":
                     	$dateClause .= "  <  '$search->hDate' ";
                     	$this->less = "SELECTED=\"selected\"";
                     	break;
                  	case "between":
                     	if (strlen($search->endDate) > 0 ) {
                        	$dateClause .= " between  '$search->hDate' and '$search->endDate' ";
                        	$this->between = "SELECTED=\"selected\"";
                        	$this->endDate = $search->endDate;
                        	$this->hideEndDate='';
                     	}
                     	else {
                        	$whereClause .= "  =  '$search->hDate' ";
                     	}
                     	break;
                  	default:
								$dateClause = "WHERE g.gravel_date = current_date ";
                     	$whereClause .= "  =  '$this->gDate'";
            		   	$this->equalto = "SELECTED=\"selected\"";
                     	break;
               	}
					if (intval($search->cooID) > 0 ) {
						$this->cooID = $search->cooID;
						$whereClause .= " and calloff_order_id = $this->cooID ";
					} 
					if ($search->areaID > 0 ) {
						$whereClause .= " and g.area_id = $search->areaID ";
						$this->areaName = $search->areaName;
						$this->areaID = $search->areaID;
					}
					if ($search->wellID > 0 ) {
						$whereClause .= " and g.well_id = $search->wellID ";
						$this->wellID = $search->wellID;
					}
					if ($search->crewID > 0 ) {
						$whereClause .= " and g.crew_id = $search->crewID ";
						$this->crewID = $search->crewID;
					}
					if ($search->ownerID > 0 ) {
						$whereClause .= " and (lg.landowner_id = $search->ownerID or ll.landowner_id = $search->ownerID )";
						$this->ownerID = $search->ownerID;
					}
					if (! is_null($search->status)) {
						$this->status = $search->status;
						if ($search->status == 6) {
							$whereClause .= " and invoice_num != '' ";
						}
						else if ($search->status == 2 ) {
							$whereClause .= " and status in (2,4)  ";	
						}
						else {
							$whereClause .= " and status = $this->status ";	
						}
					}
					
				}  // end else  coo id + date
			}  // end search null
			else {
				 // Default query still needs search object serialized
				if (! $dateSelected) {
					$this->gDate = date('d-m-Y');
					$whereClause = "";
					$dateClause = " WHERE  g.gravel_date = '$this->gDate'";
            	$this->equalto = "SELECTED=\"selected\"";
				}
         	$search = new Search();
         	$search->dateType = "equal";
         	$search->gDate = $this->gDate;
				$search->order = "g.gravel_date,calloff_order_id";
         	$_SESSION['SQL'] = pg_escape_string(serialize($search));
			}

			//   set heading backgrounds  and SQL order clause for eelected sort order
         $hd1 = $hd2 = $hd3 = $hd4 = $hd5 = $hd6 = $hd7 = $hd8 ="";
         switch($search->order) {
            case "date" :
            $hd2="sort";
            $orderClause = " order by tab.gravel_date";
            break;
            case "docket" :
            $hd3="sort";
            $orderClause = " order by docket_num,tab.gravel_date";
            break;
            case "unit" :
            case "area" :
            $hd5="sort";
            $orderClause = " order by a.area_name,tab.gravel_date";
            break;
            case "well" :
            $hd6="sort";
            $orderClause = " order by w.well_name,tab.gravel_date";
            break;
            case "crew" :
            $hd7="sort";
            $orderClause = " order by c.crew_name,tab.gravel_date";
            break;
            case "coo" :
            $hd8="sort";
            $orderClause = " order by calloff_order_id,tab.gravel_date";
            break;
            default:
               $hd2="sort";
               $orderClause = " order by tab.gravel_date,calloff_order_id";
            break;
         }
          
			$sql = " with tab as (SELECT g.*  from {$this->shortName}_gravel_water g $dateClause )
            select tab.*,a.area_name,c.crew_name,sc.sub_crew_name, NULL as upload_name
				from  tab 
				LEFT JOIN area a using (area_id)
				LEFT JOIN crew c  using (crew_id)
				LEFT JOIN sub_crew sc  using (sub_crew_id)
				$whereClause   
				$orderClause
				LIMIT 1000";

		//echo "$sql <br /><br />";
				//LEFT JOIN {$this->shortName}_upload up using (gravel_water_id) 

		if(! is_null($this->contractorID)) {

			$content ="<div style=\"width:1762px;margin:auto;\">";
			// search
			$content .= <<<FIN
         <form name="gravel_water" method="post" action="gravel_water.php?action=list&search=true">
         <input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         <input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         <input type="hidden" name="con_id" id="conID" value="$this->contractorID" />
<fieldset ><legend style="margin-left:37%;">Filter Search Gravel/Water - $this->contractorName</legend>
<div class="div5" ><label for="coo_id" >C.O.O.<input type="text" name="coo_id" id="coo_id" value="$this->cooID" /></label></div>
<div class="div17 marg1" >
FIN;
			$content .= AdminFunctions::ownerSelect($this->ownerID,NULL,"","landowner",true);
			$content .= "</div>\n";
			$content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->gDate,$this->endDate,$this->hideEndDate);
			$content .= <<<FIN
<div class="div12 marg1" ><label for="area" >Area<input type="text" name="area" id="area"  value="$this->areaName" /></label></div>
<div class="div14 marg1" ><div id="welldiv" >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<script>
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well_s',$('#welldiv'),'#conID'); return false; }
      });
  });
$( document ).ready(function() {
   $(function () {
       var ac = $('#coo_id').autocomplete({
         width: 400,
         serviceUrl:'autocomplete.php?type=calloffs',
         params: {con_id:  function() { return $('#contractorID').val()} },
         minChars:2
      });
   });
});
</script>
FIN;
			$content .= "<div style=\"float:right;width:20%;\">\n";
		   $content .= "\n<input type=\"submit\" name=\"search\" value=\"Search\"  class=\"button marg1\" style=\"padding:1rem 1.5rem 1rem;\" />";
		   $content .= "\n<button class=\"marg1\"  style=\"padding:1.1rem 1.5rem 1rem;\" onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" >Reset</button>";
			if (isset($_REQUEST['search'])) {
            $content .= "<a href=\"gravel_water.php?action=list&search=true&printcsv=true\" class=\"button marg1\" style=\"padding:1.1rem 1.5rem 1rem;\" >Print C.S.V.</a>";
         }


			$content .= "</div></fieldset></form>\n";
			//$content .= "<div style=\"clear:both;\"></div>\n";
			// headings
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" >\n";
			$content .= "<div class=\"heading\" >";
			$content .= "<a href=\"gravel_water.php?action=list&redo=redo&order=date\" class=\"lnk\" >  <div class=\"time hd $hd2\">Date</div></a>\n";
			$content .= "<a href=\"gravel_water.php?action=list&redo=redo&order=coo\" class=\"lnk\" ><div class=\"rate hd $hd8\">C.O.O.</div></a>\n";
			$content .= "<div class=\"cli hd\">Gravel Landowner/Location</div>";
			$content .= "<div class=\"time hd\">Pushed</div>";
			$content .= "<div class=\"time hd\">Gravel m3</div>\n";
			$content .= "<div class=\"rate hd\">Rate</div>\n";
			$content .= "<div class=\"rate  hd\">Grvl Cost</div>";
			$content .= "<div class=\"cli hd\">Water Landowner/Location</div>";
			$content .= "<div class=\"time hd\"  >Water kL</div>\n";
			$content .= "<div class=\"rate hd\">Rate</div>\n";
			$content .= "<div class=\"rate hd\">Wtr Cost</div>";
			$content .= "<a href=\"gravel_water.php?action=list&redo=redo&order=crew\" class=\"lnk\" ><div class=\"vehicle wd hd $hd7\">Work Scope/Sub Scope</div></a>\n";
			$content .= "<a href=\"gravel_water.php?action=list&redo=redo&order=area\" class=\"lnk\" ><div class=\"vehicle hd $hd5\"  >Area</div></a>\n";
			$content .= "<a href=\"gravel_water.php?action=list&redo=redo&order=well\" class=\"lnk\" ><div class=\"cli hd $hd6\">Well/s</div></a>\n";
			$content .= "<div class=\"links hd\" style=\"width:89px;\" >Actions</div>\n";
			$content .= "</div></div>\n"; // Close heading
			$lineNo = 0;
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$lineNo=0;
//gwl.location as gravel_loc,gwg.location as water_loc, ll.owner_name as gravel_owner,lg.owner_name as water_owner,
				/*LEFT JOIN gravel_water_loc gwl on loc_1_id = gwl.gravel_water_loc_id
				LEFT JOIN landowner ll on gwl.landowner_id = ll.landowner_id
				LEFT JOIN gravel_water_loc gwg on loc_2_id = gwg.gravel_water_loc_id
				LEFT JOIN landowner lg on gwg.landowner_id = lg.landowner_id */
			while (! $rs->EOF ) {
				$water_loc = $gravel_loc = $water_owner = $gravel_owner = "";
				$pushed =  $uplink = "";
				extract($rs->fields);
				$sql = "select count(*) from {$this->shortName}_upload where gravel_water_id = $gravel_water_id";
				$upCount = $this->conn->getOne($sql);
				$ln = $lineNo % 2 == 0 ? "line1" : "line2";
				$gDate = AdminFunctions::dbDate($gravel_date);
				$sql2 = "select wells_from_ids('$well_ids')";
				$well_name = $this->conn->getOne($sql2);
				$loc_1_id = intval($loc_1_id);
				$loc_2_id = intval($loc_2_id);
				$sql3 = "select owner_name as gravel_owner,location as gravel_loc from gravel_water_loc 
            LEFT JOIN landowner using(landowner_id)
            where gravel_water_loc_id =  $loc_1_id ";
				//echo "<br />$sql3";
            $data = $this->conn->getRow($sql3);
				extract($data);
				//var_dump($data);
				$sql4 = "select owner_name as water_owner,location as water_loc from gravel_water_loc 
            LEFT JOIN landowner using(landowner_id)
            where gravel_water_loc_id =  $loc_2_id ";
				//echo "<br />$sql3";
            $data = $this->conn->getRow($sql4);
				//extract($data);
				//var_dump($data);
				if (floatval($rs->fields['gravel_units']) > 0 ) {
					$pushed = $rs->fields['pushed'] == "t" ? "Pushed" : "Not Pushed";
				}
				if (intval($upCount) > 0 ) {
					$uplink = "<button id=\"but1_$lineNo\" class=\"linkbutton orang\" style=\"min-width:44px !important;\" onclick=\"showupdiv($gravel_water_id,'#upload_$lineNo','#but1_$lineNo',$this->contractorID);return false;\">Uploads</button>";
				}

            $this->csv['Date'][] = $gDate;
				$this->csv['COO'][] = $calloff_order_id;
				$this->csv['GravLand'][] = $gravel_owner . " " .$gravel_loc;
				$this->csv['Pushed'][] = $pushed;
				$this->csv['Gravelm3'][] = $gravel_units;
				$this->csv['GravRate'][] = $gravel_rate;
				$this->csv['GravCost'][] = $gravel_total;
				$this->csv['WaterLand'][] = $water_owner . " " .$water_loc;
				$this->csv['WaterKL'][] = $water_units;
				$this->csv['WaterRate'][] = $water_rate;
				$this->csv['WaterCost'][] = $water_total;
				$this->csv['Crew'][] = $crew_name;
				$this->csv['SubCrew'][] = $sub_crew_name;
				$this->csv['Area'][] = $area_name;
				$this->csv['Well'][] = $well_name;
				if ($status == 0 ) {
					$approve = "<button id=\"app_$lineNo\" class=\"linkbutton  approve\" onclick=\"gravel_approve($gravel_water_id,$this->contractorID,'#app_$lineNo');\">Approve</button>\n";
				}
				else {
					$approve = "<button id=\"app_$lineNo\" class=\"linkbutton approve red\" style=\"min-width:44px !important;\"  >Locked </button>\n";
				}
				$gravel = "<span style=\"font-weight:bold;\" >" . substr($gravel_owner,0,25) . "</span><br />"  .  substr($gravel_loc,0,30);
				$water =  "<span style=\"font-weight:bold;\" >" . substr($water_owner,0,25) . "</span><br />"  .  substr($water_loc,0,30);
				$crew_name .= !empty($sub_crew_name) ? " - $sub_crew_name" : "";
			//	$content .= "<a href=\"gravel_water.php?action=find&gravel_id=$gravel_water_id\" style=\"color:black;\" >\n";
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" id=\"linediv_$lineNo\" >\n";
				$content .= "<div class=\"time $ln highh\">$gDate</div>\n";
				$content .= "<div class=\"rate $ln highh\" > $calloff_order_id</div>\n";
				$content .= "<div class=\"cli $ln highh\">$gravel</div>";
				$content .= "<div class=\"time $ln highh\">$pushed</div>";
				$content .= "<div class=\"time $ln highh\">$gravel_units</div>\n";
				$content .= "<div class=\"rate $ln highh\">$gravel_rate</div>\n";
				$content .= "<div class=\"rate $ln highh\">$gravel_total</div>";
				$content .= "<div class=\"cli  $ln highh\">$water</div>";
				$content .= "<div class=\"time $ln highh\"  >$water_units</div>\n";
				$content .= "<div class=\"rate $ln highh\">$water_rate</div>\n";
				$content .= "<div class=\"rate $ln highh\">$water_total</div>";
				$content .= "<div class=\"vehicle wd $ln highh\">$crew_name</div>\n";
				$content .= "<div class=\"vehicle $ln highh\"  >$area_name</div>\n";
				$content .= "<div class=\"cli $ln highh\">$well_name</div>\n";
				//$content .= "</a>\n";
				$content .= "<div class=\"links $ln highh\"  style=\"width:89px;text-align:center;\"  >\n";
				//$content .= "<button id=\"but1_$lineNo\" class=\"linkbutton oran\" onclick=\"showdiv($gravelID,'#hour_$gravelID','#but1_$lineNo','$jDate');return false;\">SHOW</button>";
				if ($this->sessionProfile > 2   ) {
					 	$content .= "<div style=\"margin:auto;width:50px;\" >\n";
				 		$content .= $approve . $uplink;	
				}
				else {
					$content .= " &nbsp;\n";
				}
				$content .= "</div></div></div>"; // close links
				$content .= "<div id=\"upload_$lineNo\" class=\"hiddiv\" ></div>\n";
				//$content .= "</div>\n";

				//$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			//exit;	
			$this->printCSV();
			$content .= "<hr />";
			$content .= " </div>\n";
			// Page  totals

		} // end search
		else { // ContractorID NULL show contrcator select
            $content = AdminFunctions::setContractor("gravel_water.php?action=list",$this->contractorID);
      }
			$this->page = str_replace('##MAIN##',$content,$this->page);

	}
		//TODO  needs to use  uuids when available ..
		private function getPrevRecords($plantID,$areaID,$cooID) {  // Update of multi well C.O.O. get all origanl records //Indexed
			$sql = "SELECT hour_id from hour h     
						WHERE  h.hour_date = '$this->gDate'      
						and calloff_order_id = $cooID
						and h.area_id = $areaID
						and plant_id = $plantID
						and docket_num = '$this->docketNo'
						and employee_id = $this->operatorID
						order by hour_id";

			//echo $sql;
			if (! $this->updateRecords = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}
			}
		}

		private function getGravelDetails() {
			$sql = "SELECT g.*,wells_from_ids(g.well_ids) as well_name,u.upload_name,u.upload_type_id,u.upload_id
						FROM gravel_water g
                  LEFT JOIN area a using (area_id)
                  LEFT JOIN crew c  using (crew_id)
                  LEFT JOIN upload u   using (gravel_water_id)
						WHERE  g.gravel_water_id = $this->gravelID";
			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
						//LEFT JOIN upload up on up.hour_id = h.hour_id



			$this->gravelID = $data['gravel_water_id'];
       	$this->areaID = $data['area_id'];
       	$this->gDate	= AdminFunctions::dbdate($data['gravel_date']);
			$this->cooID = $data['calloff_order_id'];
       	$this->docketNo = $data['docket_num'];
			$this->multi = $data['well_ids'];
			$this->unitGrav = $data['gravel_units'];
			$this->unitWater = $data['water_units'];
			$this->subGrav = $data['gravel_total'];
			$this->subWater = $data['water_total'];
			$this->pushed = $data['pushed'] == "t" ? 1 : 2;
			$this->upload = basename($data['upload_name']);
			$this->uploadID = $data['upload_id'];
			$this->uploadType = $data['upload_type_id'];
			$well_name = $data['well_name'];
			$arr = explode("|",$this->multi);

		}
		private function processPost() {
			//var_dump($_POST);
			$this->upload = $_FILES['upload'];
			$this->uploadType = $_POST['type'];
         if ($this->action == "delete" ) {
				$this->deleteGravels();
			}
		 	$this->employeeID = isset($_POST['employeeID']) ? intval($_POST['employeeID']) : 0;
       	$this->docketNo = $_POST['docketno'];
       	$this->uploadID =isset($_POST['upload_id']) ?  intval($_POST['upload_id']) : 0;
			$this->status = intval($_POST['status']);
			$this->cooID = $_POST['coo'];
			$this->crewID = $_POST['crew'][0];
			$this->areaID = $_POST['areaID'][0];
			$this->pushedRate =  isset($_POST['pushed_rate']) ? floatval($_POST['pushed_rate']) :"NULL";
			$this->nonPushedRate = isset($_POST['non_pushed_rate']) ? floatval($_POST['non_pushed_rate']) : "NULL";
			$this->waterRate =  isset($_POST['water_rate']) ? floatval($_POST['water_rate']) : "NULL";
       	$this->multi = $_POST['multi'];
			$this->gravelLoc_1 = !empty($_POST['gravel_loc_1']) ? $_POST['gravel_loc_1'] : "NULL";
			$this->waterLoc_2 =  !empty($_POST['water_loc_2']) ? $_POST['water_loc_2'] : "NULL" ;
			// arrays
			$this->gravelIDs = isset($_POST['gravelIDs']) ? $_POST['gravelIDs'] : NULL;
			$this->pushed = isset($_POST['pushed']) ? $_POST['pushed'] : NULL;
			$this->unitGrav = isset($_POST['un_gravel']) ? $_POST['un_gravel'] : NULL;
			$this->subGrav = isset($_POST['grav_subtot']) ? $_POST['grav_subtot'] : NULL;
			$this->unitWater = isset($_POST['un_water']) ? $_POST['un_water'] : NULL;
			$this->subWater = isset($_POST['water_subtot']) ? $_POST['water_subtot'] : NULL;
			$this->gDates = $_POST['gdate'];

			$this->submitForm();
		}
		private function deleteGravels() {
			$redo="redo";
			if ($this->gravelID > 0 ) {
					$sql = "DELETE from gravel_water where gravel_water_id = $this->gravelID";

				if (! $this->conn->Execute($sql)) {
					die($this->conn->ErrorMsg());
				}
			}
			header("LOCATION: gravel_water.php?action=list&redo=$redo");
			exit;
		}
		private function submitForm() {
			$insFound = false;
			if (!is_null($this->gravelIDs)) {
				foreach($this->gravelIDs as $ind=>$val) {
					$gravRate = $waterRate = $push = "NULL";
					$gravSub = $waterSub =  $unGrav = $sbGrav = $unWater = $sbWater = "NULL"; 
				//gravel
					$sbGrav = !empty($this->subGrav[$ind]) ? floatval($this->subGrav[$ind]) : NULL;
					$unGrav = !empty($this->unitGrav[$ind]) ? floatval($this->unitGrav[$ind]) : NULL;
					if (isset($this->pushed[$ind]) && !is_null($unGrav)) {    // isset and has been selected
						if  ( $this->pushed[$ind]   == 1 ) { 
							$push = "TRUE";
							$gravRate = $this->pushedRate;
							$gravSub = $gravRate * $unGrav ;
						}
						else if ($this->pushed[$ind] == 2 ) {
							$push = "FALSE";
							$gravRate = $this->nonPushedRate;
							$gravSub = $gravRate * $unGrav ;

						}	
						// now  check
						if (round(floatval($gravSub),2) != round(floatval($sbGrav),2) ){
							echo "Gravel sub total problem  $gravSub   -    $sbGrav ";
						}
					}	
				// Water
					$unWater = !empty($this->unitWater[$ind]) ? floatval($this->unitWater[$ind]) : NULL;
					$sbWater = !empty($this->subWater[$ind]) ? floatval($this->subWater[$ind]) : NULL;
					if (isset($this->subWater[$ind]) && !is_null($unWater) ) { 
						$waterSub =   $this->waterRate * $unWater;
						$waterRate = $this->waterRate;
						if (round(floatval($waterSub),2) != round(floatval($sbWater),2)) {
							echo "Water sub total problem  $waterSub   -    $sbWater ";
						}
					}
					$gDat  = isset($this->gDates[$ind]) ?  $this->gDates[$ind] : "NULL";
					$unGrav = !is_null($unGrav) ? $unGrav  : "NULL"; 
					$sbGrav =  !is_null($sbGrav) ? $sbGrav : "NULL";
					$unWater = !is_null($unWater) ? $unWater : "NULL"; 
					$sbWater = !is_null($sbWater) ? $sbWater : "NULL";  
					if ($val == 0 ) {
						$insql = "INSERT into gravel_water values ";
						$insql .= 	"(nextval('gravel_water_gravel_water_id_seq'),$this->cooID,$this->crewID,$this->areaID,'$this->multi','$gDat','$this->docketNo',
						$this->gravelLoc_1,$gravRate,$unGrav,$gravSub,$push,$this->waterLoc_2,$waterRate,$unWater,$waterSub,0,$this->employeeID,now()) returning gravel_water_id";
						if (! $this->gravelID = $this->conn->getOne($insql)) {
							die($this->conn->ErrorMsg());
						}  
						$this->doUpload($this->gravelID,$ind,$gDat);  //   singular

					}
					else { // Update
						$sql = "UPDATE gravel_water set calloff_order_id = $this->cooID,crew_id = $this->crewID,area_id = $this->areaID,well_ids='$this->multi',gravel_date = '$gDat',docket_num = '$this->docketNo',
						loc_1_id =   $this->gravelLoc_1,gravel_rate = $gravRate,gravel_units = $unGrav,gravel_total=$gravSub,pushed=$push,loc_2_id=$this->waterLoc_2,water_rate=$waterRate,water_units=$unWater,
						water_total=$waterSub where gravel_water_id = $val";
						 //die($sql);
						if (! $this->conn->Execute($sql)) {
							die($this->conn->ErrorMsg());
						}
						$this->doUpload($val,0,$gDat);  // singular  record  always  array 0 element  for FILES  etc
				
					}
				}  // end foreach
		 	}

			header("LOCATION: gravel_water.php?action=list&redo=redo");
			exit;
		}
		private function getCoo($crewID,$areaID,$wellID,$coo) {
			$sql = "SELECT * from coos_from_crewareawell($crewID,$areaID,$wellID,$this->contractorID)";
			if (! $cooID = $this->conn->getOne($sql)) {
				die("This combination of Work Scope : $crewID  Area:  $areaID  Well:  $wellID does NOT! equal  C.O.O.  $coo<br />Please Contact NTD Immediately !! ");
			}
			elseif (intval($cooID) != $coo ) {
				die("This combination of Work Scope : $crewID  Area:  $areaID  Well:  $wellID does NOT! equal  C.O.O.  $coo<br />Please Contact NTD Immediately !! ");
			}
		}
		private function getCOODetails($cooID,$DISABLED) {
			$sql = "SELECT area_id,a.area_name,crew_id,well_ids from calloff_order 
						LEFT JOIN area a using(area_id) 
						where calloff_order_id = $cooID";
        	if (! $data= $this->conn->getRow($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
           		die($this->conn->ErrorMsg());
				}
				else {
					return  "<div id=\"welldiv_1\" class=\"welldiv\"  > &nbsp;</div>\n";
				}
        	}
			$this->areaName = $data['area_name'];
			$this->crewID = $data['crew_id'];
			$this->areaID = $data['area_id'];
			$this->wells = $data['well_ids'];
   		$wellStr = "<div id=\"welldiv_1\" class=\"welldiv\"  >\n";
			$wellStr .= AdminFunctions::wellSelect($this->areaID,$this->wells,"disabled",false,0);  // All wells from this name  type
   		$wellStr .= "</div>\n";
			return $wellStr;
		}
	
		private function doUpload($gravelID,$ind,$gDat) {
			$tDate = preg_replace('/-/',"",AdminFunctions::dbDate($gDat));
			$gravel_id = $gravelID;
         //$upName = "{$tDate}_{$this->contractorID}_{$gravelID}_";
			$uploadTypeID = $this->uploadType[$ind]; 
			$coo = $this->cooID;

			if (strlen($this->upload['name'][$ind]) > 0 ) {
				if (intval($this->upload['error'][$ind]) > 0) {
           		die ("Upload Problem " .$this->upload['name'][$ind] . "  ".$this->upload['size'][$ind] );
         	}
				//$name = AdminFunctions::cleanURL($this->upload['name'][$ind]);
				$ext = AdminFunctions::getExtension($this->upload['name'][$ind]);
            $uniq = uniqid();
				switch($uploadTypeID) {
           	 	case 16:
            		$name = "{$tDate}_{$this->contractorID}_{$gravel_id}_gravel{$ext}";   // global
            		$new_name = "uploads/".$name;
            		break;
            	case 18:
            		$name = "{$tDate}_{$this->contractorID}_{$gravel_id}_water{$ext}";   // global
            		$new_name = "uploads/".$name;
            		break;
            	default :
               	$name = "{$tDate}_{$this->contractorID}_{$hour_id}_UNKNOWN{$ext}";   // global
               	$new_name = "uploads/".$name;
            		break;
         	}

         	if (!move_uploaded_file($this->upload['tmp_name'][$ind], $new_name)) {
           		return "Can't upload file";
         	}

				if (! is_array($gravelID) && $this->uploadID == 0 ) {  // is an insert action
					$sql = "INSERT into upload (gravel_water_id,contractor_id,upload_type_id,employee_id,upload_date,upload_name,coos,h_date) 
					values($gravelID,$this->contractorID,$uploadTypeID,$this->employeeID,current_date,'$new_name','$coo','$gDat')";
				}
				else if (is_array($gravelID) && $this->uploadID == 0 ){
					$sql = "INSERT into upload (hour_id,contractor_id,upload_type_id,employee_id,upload_date,upload_name,coos,h_date) values ";
					foreach($gravelID as $key=>$val) {
						$id= $val['gravel_id'];
						$sql .= "($id,$this->contractorID,$uploadTypeID,$this->employeeID,current_date,'$new_name','$coo','$gDat'),";
					}
					$sql = preg_replace('/,$/',"",$sql);

				}
				else {  // must be non array  "UPDATE"
					$sql = "UPDATE upload set upload_type_id = $uploadTypeID,upload_name = '$new_name',upload_date=current_date where upload_id = $this->uploadID"; 
				}

        		if (! $res= $this->conn->Execute($sql)) {
           		die($this->conn->ErrorMsg());
        		}
		 	}

		}
		private function printCSV() {
          //exit;
         $headings = array('Date','C.O.O.','Gravel Landowner/Location','Pushed','Gravel m3','Rate','Gravel Cost','Water Landowner/Location','Water kL','Rate','Water Cost','Work Scope','Area','Well/s');
         $file= "tmp/water_gravel_{$this->employeeID}.csv";
         $fp = fopen($file,'w');
         fputcsv($fp,$headings);
         $lineCount=1;
			if (!empty($this->csv['Date'])  ) {
         	foreach($this->csv['Date'] as $ind =>$val) {
            	$tmpArr= array($this->csv['Date'][$ind],$this->csv['COO'][$ind],$this->csv['GravLand'][$ind],$this->csv['Pushed'][$ind],$this->csv['Gravelm3'][$ind],$this->csv['GravRate'][$ind],$this->csv['GravCost'][$ind],$this->csv['WaterLand'][$ind],$this->csv['WaterKL'][$ind], $this->csv['WaterRate'][$ind],$this->csv['WaterCost'][$ind],$this->csv['Crew'][$ind],$this->csv['Area'][$ind],$this->csv['Well'][$ind]);
         //var_dump($tmpArr);
         //exit;
         		fputcsv($fp,$tmpArr);
            	$lineCount ++;
         	}

         	fputcsv($fp,array("","","","",'=ROUND(SUM(E1:E'.$lineCount.'),2)',"",'=ROUND(SUM(G1:G'.$lineCount.'),2)',"",'=ROUND(SUM(I1:I'.$lineCount .'),2)',"",'=ROUND(SUM(K1:K'.$lineCount .'),2)'));
			}
         fclose($fp);
      }
   }

?>
