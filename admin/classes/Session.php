<?php
	class Session {
	public $conn;
	
	public function __construct(){
		$this->conn = $GLOBALS['conn'];
	 	session_set_save_handler(
    	array($this, "_open"),
    	array($this, "_close"),
    	array($this, "_read"),
    	array($this, "_write"),
    	array($this, "_destroy"),
    	array($this, "_gc")
  		);

	 	ini_set('session.gc_maxlifetime', 36000);  // 10 Hours
    	session_set_cookie_params(36000); // 10 Hours
	 	session_start();
	}

	public function _open(){
    	return true;
	}

	public function _close(){
		$this->conn->Close();
    	return true;
	}

	public function _read($id){
 		$data = NULL; 
  		$access = time();
		$sql = "SELECT data from sessions where id = '$id'";
  		if(! $data = $this->conn->getOne($sql)){
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
		}
		else {
 	   	$sql = "UPDATE sessions set access = $access where id = '$id'";
			if ( !$res = $this->conn->Execute($sql) ) {
				die($this->conn->ErrorMsg());
			}
		}
		return $data;
  }

	public function _write($id, $data){
  		$access = time();
		$data = pg_escape_string($data);
		$sql = "SELECT id from sessions where id = '$id'";
  		if(! $res = $this->conn->getOne($sql)){
			if ($this->conn->ErrorNo() != 0 ) {
				die($this->conn->ErrorMsg());
			}
 	   	$sql = "INSERT into sessions VALUES ('$id', $access, E'$data')";
		}
		else {
 	   	$sql = "UPDATE sessions set data = E'$data',access = $access where id = '$id'";
		}
		if (!$res=$this->conn->Execute($sql)) {
			die($this->conn->ErrorMsg());
		}
      return true;
  }

	public function _destroy($id){
		if (ini_get("session.use_cookies")) {
    		$params = session_get_cookie_params();
    		setcookie(session_name(), '', time() - 42000,
        		$params["path"], $params["domain"],
        		$params["secure"], $params["httponly"]
    		);
		}

  		$sql = "DELETE FROM sessions WHERE id = '$id'"; 
		if (!$res=$this->conn->Execute($sql)) {
			die($this->conn->ErrorMsg());
		}
     
    return true;
  }
	public function _gc($max){
  		$old = time() - $max;
  		$sql = "DELETE FROM sessions WHERE access < $old";
		if (!$res=$this->conn->Execute($sql)) {
			die($this->conn->ErrorMsg());
		}
    return true;
  }

}

?>
