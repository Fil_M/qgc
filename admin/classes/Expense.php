<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Expense  {
		public $page;
		public $expenseID;
		public $expenseName;
		public $contractorID;
		public $siteInsNo;
		public $siteInstruction;
		public $action;
		public $sessionProfile;
		public $conn;

		public function	__construct($action,$expenseID=0,$search="") {
			$this->expenseID = $expenseID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_POST['SUBMIT'])) {
				$pg = new Page('expense');
				$this->page= $pg->page;
				$heading_text = "Administration<BR />";
         	switch($action) {
					case "new" :
						$heading_text .= "Add New Expense Type";
						break;
					case "update" :
						if ($expenseID < 1 ) {
							return false;
						}
						$heading_text .= "Updating Expense Type $expenseID";
						$this->getExpenseTypeDetails($expenseID);
						break;
					case "delete" :
						if ($expenseID < 1 ) {
							return false;
						}
						$heading_text .= "Delete Expense Type $expenseID";
						$this->getExpenseTypeDetails($expenseID);
						break;
					case "list" :
						$heading_text .=  strlen($search) > 0 ? "List Expense Type - $search" : "List Expense Types - All";
						$this->getAllExpenseTypes($search) ;
						break;
					case "find" : 
						$heading_text .= "Expense Type $expenseID";
						$this->getExpenseTypeDetails($expenseID);
						break;
					default:
						$heading_text .= "Add New Expense Type";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##HEADER_TEXT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->expenseID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Add Expense Type";
						break;
					case "update":
						$button = "Update Expense Type";
						break;
					case "delete":
						$button = "Delete Expense Type";
						break;
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "Add Expense Type";
						break;
				}
			$content = <<<FIN
<div style="width:1800px;text-align:center;"><div style="width:1366px;margin:auto;">
<form name="expense" method="post" action="expense.php">
<input type="hidden" name="expense_id" value="$this->expenseID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset style="margin-top:15px;width:1366px;"><legend style="margin-left:630px;">Expense Type</legend>
<label for="expense_type" class="label wide" >Expense Name</label><input type="text" name="expense_type" id="expense_type" class="input $DISABLED" value="$this->expenseName" $DISABLED />
FIN;
$content .= AdminFunctions::contractorSelect($this->contractorID,$DISABLED);
$content .= "</fieldset>\n";


				if ($this->action != "find" ) {
  					$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"$button\" class=\"submitbutton\" style=\"margin:0px;width:250px;\"/>"; // no submit for found expense
				}
				 $content .= "\n</form></div></div>\n";
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllExpenseTypes($search="") {
			$sql = "SELECT * from expense_type where removed is false order by type_expense";
			if (! $rs=$this->conn->Execute($sql)) {
				die($this->ErrorMsg());
			}
			$content ="<div style=\"width:1800px;text-align:center;\">";
			$content .= "<div style=\"margin:auto;width:587px;\" >";
			$content .= "<div class=\"heading\" style=\"width:586px;\"><div class=\"addr hd\">Expense Name</div>";
			$content .= "<div class=\"links hd\" style=\"width:200px;\" >Actions</div></div>\n";
			$lineNo = 0;
			while (! $rs->EOF ) {
				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$expense_id = $rs->fields['expense_type_id'];
				$expense_type = strlen($rs->fields['type_expense']) > 2 ? $rs->fields['type_expense'] : " ";
				$content .= "<div class=\"addr $ln\"><a href=\"expense.php?action=find&expense_id=$expense_id\" title=\"View Expense Type\" class=\"name\" >$expense_type</a></div>";
				if ($this->sessionProfile > 2 ) {
           	$content .= "<div class=\"links $ln\" style=\"width:200px;\"><a href=\"expense.php?action=update&expense_id=$expense_id\" title=\"Edit Expense Type\" class=\"linkbutton\" >EDIT</a><a href=\"expense.php?action=delete&expense_id=$expense_id\" title=\"Delete Expense Type\" class=\"linkbutton d\" onclick=\"return confirmDelete();\" >DELETE</a></div>\n";
				}
			   else {
               $content .= "<div class=\"links $ln\" style=\"width:200px;\" > &nbsp;</div>\n";
            }

				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}
			$content .= "</div></div><hr />";


			$this->page = str_replace('##MAIN##',$content,$this->page);

		}

		private function getExpenseTypeDetails($expenseID) {
			$sql = "SELECT *  from expense_type  where expense_type_id = $expenseID";

			if (! $data = $this->conn->getRow($sql)) {
				die($this->conn->ErrorMsg());
			}
			$this->expenseID = $data['expense_type_id'];
			$this->expenseName = $data['type_expense'];
			$this->contractorID = $data['contractor_id'];

		}
		private function processPost() {
         if ($this->action == "delete" ) {
				$this->deleteExpenseType();
			}
			$this->expenseID = $_POST['expense_id'];
			$this->contractorID = intval($_POST['contractor']);
			$this->expenseName = trim(addslashes($_POST['expense_type']));
			
			$this->submitExpenseType();
		}
		private function deleteExpenseType() {
			$sql = "UPDATE expense_type set removed = TRUE where expense_type_id = $this->expenseID";
			if (! $this->conn->Execute($sql)) {
				die($this->conn->ErrorMsg());
			}
			header("LOCATION: expense.php?action=list");
			exit;
		}
		private function submitExpenseType() {
			if ($this->expenseID == 0 ) {
				$sql = "INSERT into expense_type values (nextval('expense_type_expense_type_id_seq'),E'$this->expenseName',$this->contractorID,false) returning expense_type_id";
				
				if (! $rs =  $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
				$this->expenseID = $rs->fields['expense_type_id'];
			}
			else { // Update
				$sql = "UPDATE expense_type set type_expense = E'$this->expenseName',contractor_id = $this->contractorID where expense_type_id = $this->expenseID";
				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}
			}

			header("LOCATION: expense.php?action=find&expense_id=$this->expenseID");
			exit;
			
		}

	}
?>
