<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class WellReport  {
		public $page;
		public $action;
		public $startDate;
		public $endDate;
		public $contractorID;
		public $areaName;
		public $areaID;
		public $wellID;
		public $callOffID;
		public $split;

		public function	__construct($action,$conID=0,$areaID=NULL,$wellID=NULL,$split=NULL,$startDate=NULL,$endDate=NULL) {
			//var_dump($action,$hDate);
			$this->action = $action;
			$this->startDate= !is_null($startDate)  ?  $startDate : AdminFunctions::beginLastMonth();
         $this->endDate= ! is_null($endDate)  ?  $endDate  :  AdminFunctions::endLastMonth();
			$this->split = $split;
		 	$this->contractorID=$conID;
		 	$this->areaID=$areaID;
		 	$this->wellID=$wellID;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->conn = $GLOBALS['conn'];
			if (isset($_POST['SUBMIT'])) {
				$this->processPost();
         }
			elseif (isset($_POST['PRINT'])) {
				 $URL="https://".$_SERVER['HTTP_HOST'] ."/printWellpdf.php?action=print&con_id=$this->contractorID&area_id=$this->areaID&well_id=$this->wellID&split=$this->split&start=$this->startDate&end=$this->endDate";
				 header("LOCATION: $URL");
				 exit;
			}
			else {
				$pg = new Page('wellreport');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Print Well Report";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print Report\" class=\"button marg2\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:1600px;margin:auto;">
<form name="wellreport" id="wellreport" method="post" action="wellreport.php?action=$action">
<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
<input type="hidden" name="area_id" id="areaID" value="$this->areaID" />
<fieldset ><legend style="margin-left:44%;">Filter Search - Well Report</legend>
<div class="div12">
FIN;
   $content .= AdminFunctions::contractorSelect($this->contractorID,"",true);
	$content .= <<<FIN
</div>
<div class="div8 marg1" ><label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div class="div8 marg1" ><label >End Date:<input type="text" name="enddate" id="edate" class="required date" value="$this->endDate"  /></label></div>
<div class="div12 marg1" >
<div class="div95" ><div class="div80" ><label>Split by Contractor</label></div><div class="div20" ><input type="radio" value="con" name="consplit" id="cnsplit" /></div></div>
<div class="div95" style="height:1rem;" ></div>
<div class="div95" ><div class="div80" ><label>Split by Work Scope</label></div><div class="div20" ><input type="radio" value="crew" name="consplit" id="crsplit" /></div></div>
</div>
<div class="div15 marg2" ><label >Area:<input type="text" name="area" id="area" class="required" value="$this->areaName" /></label></div>
<div class="div17 marg1" ><div id="welldiv"  >
FIN;
$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
$content .= <<<FIN
</div></div>
<div style="float:right;width:18%;" > 
<button class="reset" onclick="$('form').clearForm();$(':hidden').val(0);return false;" >Reset</button>
$button
</div>
</fieldset>
</form>
</div>
<script>
$("#wellreport").submit(function(){
   var isFormValid = true;
   $("#wellreport input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#wellreport .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
});
 $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),$('#contractor')); return false; }
      });
  });
$(function () {
      $('#coo').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=coo',
         minChars:3,
         onSelect: function(value, data){ $('#contractorID').val(data[0]); $('#contractor').val(data[0]);$('#area').val(data[1]);wellDiv(data[2],$('#welldiv')); return false; }
      });
  });
$('#contractor').change(function() {
	var conid = $('#contractor').val();
	$('#contractorID').val(conid);
	if (conid == 0 ) {
		$('#splitdiv').css("display","block");
	}
	else {
		$('#splitdiv').css("display","none");
		 $('#cnsplit').prop("checked",false);
		 $('#crsplit').prop("checked",false);
	}
});
</script>

FIN;
	 ////var conid = $('#contractor').val(); alert(conid);  );
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
 }
?>
