<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class Unlock  {
		public $page;
		public $employeeID;
		public $action;
		public $sessionProfile;
		public $conn;

		public function	__construct($action) {
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			if (! isset($_REQUEST['SUBMIT'])) {
				$pg = new Page('plant');
				$this->page= $pg->page;
         	switch($action) {
					case "new" :
						$heading_text = "Unlock Employee Password";
						break;
					default:
						$heading_text = "Unlock Employee Password";
						break;
				}
		 		$this->setHeaderText($heading_text);	
		 		$this->setContent($action);	
				echo $this->page;
			}
			else {
				$this->processPost();
			}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->plantID > 0 )) {
				$DISABLED="";
				switch ($action) {
					case "new":
						$button = "Unlock";
						break;
					default:
						$button = "Unlock";
						break;
				}
			$content = <<<FIN
<div style="width:605px;margin:auto;">
<form name="unlock" id="unlock" method="post" action="unlock.php?SUBMIT=SUBMIT">
<fieldset  ><legend style="margin-left:44%;">Employee</legend>
<div class="div50" ><label   >Employee</label>
FIN;
$content .= AdminFunctions::employeeSelect(NULL,"");
//<input type="text" name="plant_name" id="plant_name" class="input desc $DISABLED" value="$this->plantName" $DISABLED />
$content .= <<<FIN
</div>
<div style="float:right;width:21%;" >
<input type="submit" name="SUBMIT" id="SUBMIT" value="$button" class="button" /> 
</div>
</fieldset>
</form></div>
FIN;
			}
			else {
				$content = "";
			}

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function processPost() {
			$this->employeeID = $_POST['employee'];
				$sql = "UPDATE employee set locked  = false where employee_id = $this->employeeID";
				//echo $sql;
            //exit;

				if (! $this->conn->Execute($sql)) {
					die( $this->conn->ErrorMsg());
				}

			header("LOCATION: index.php");
			exit;
			
		}

	}
?>
