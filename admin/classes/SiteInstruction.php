<?php //QGC  Site  Instruction
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class SiteInstruction  {
		public $companyID;
		public $page;
		public $siteInsID;
		public $siteInsNo;
		public $fieldEstIDs;
		public $callOffID;
		public $empID;
		public $contractorID;
		public $action;
		public $sessionProfile;
		public $conn;
		public $areaID;
		public $wellID;
		public $crewID;
		public $subCrewID;
		public $areaName;
		public $equalto;
		public $between;
      public $hideEndDate=true;
      public $equalgreater;
      public $insDate;
      public $endDate;
		public $less;
		public $scope;
		public $supGroupID;
		public $wellStr;
		public $wellIDs;
		public $subCrewIDs;
		public $contractorName;
		public $shortName;
		public $conDomain;
      public $conName;
		public $conSignature;
		public $conSigDate;
		public $conSigName;
		public $empName;
		public $qgcSignature;
		public $qgcSignee;
		public $qgcApprDate;
		public $lineNo;
		public $divType;


		public function	__construct($action,$siteInsID=0,$conID=NULL,$lineNo=NULL,$type=NULL,$search=NULL,$redo=NULL) {
			$this->siteInsID = $siteInsID;
			$this->conn = $GLOBALS['conn'];
			$this->action = $action;
			$this->lineNo = $lineNo;
         $this->divType = $type;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->empID = intval($_SESSION['employee_id']);
			$this->contractorID = $conID;
			 if (! is_null($conID)) {
            $connArr = AdminFunctions::getConName($conID);
            $this->contractorName = $connArr['con_name'];
            $this->shortName = $connArr['name'];
            $this->conDomain = $connArr['domain'];
            $this->conName = $connArr['con_name'];
         }
			$pg = new Page('siteins');
			$this->page= $pg->page;
        	switch($action) {
				case "list" :
					$heading_text =   "List Site Instructions - $this->conName";
					$this->getAllSiteIns($search,$redo) ;
					break;
				case "find" : 
					$this->getInsDetails($siteInsID);
					$heading_text = "Site Instruction $this->siteInsNo - $this->contractorName";
					break;
				default:
					//$heading_text .= "Add New Estimate";
					break;
			}
		 	$this->setHeaderText($heading_text);	
		 	$this->setContent($action);	
			echo $this->page;
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action) {
			if ($action != "list" || ($action == "find" && $this->siteInsID > 0 )) {
				switch ($action) {
					case "find":
						$button = "No Button";
						$DISABLED='disabled';
						break;
					default:
						$button = "No Button";
						$DISABLED='disabled';
						break;
				}
			$content = <<<FIN
<div style="width:1530px;margin:auto;">
<form name="siteins" action="site_instruction.php" id="site_ins" enctype="multipart/form-data"> 
<input type="hidden" name="empID" value="$this->empID" />
<input type="hidden" name="siteInsID" value="$this->siteInsID" />
<input type="hidden" name="wellID" value="$this->wellID" />
<input type="hidden" name="action" value="$this->action" />
<fieldset ><legend style="margin-left:46%;">Site Instruction</legend>
<input type="hidden" id="lineno" value="1" />
<div class="div15" ><label >Site Instruction No<input type="text"  name="site_ins_no" id="site_ins_no" value="$this->siteInsNo"  $DISABLED /></label></div>
<div class="div12 marg6" ><label >This Instruction Estimate<input type="text"  name="this_est" id="this_est" value="$this->estTotal" class="txtr"  READONLY /></label></div>
<div class="div20 marg10" style="margin-left:13%;" ><label>Field Estimate ID/s <input type="text"  name="field_id" id="field_id" value="$this->fieldEstIDs"   READONLY /></label></div>
<div class="div12 marg" ><label >Total Estimate for COO<input type="text"  name="orig_est" id="orig_est" value="$this->costEstimate" class="txtr" READONLY /></label></div>
<div class="div700 marg10" style="margin-left:9.5%;" > <label  >C.O.O.<input type="text"  name="coo_id" id="coo_id" value="$this->callOffID"  READONLY /></label></div>
<div style="clear:both;" ></div>
FIN;
			$content .= "<div class=\"div15\" ><label class=\"label\" >Work Scope</label>\n";
         $content .= AdminFunctions::crewSelect($this->crewID,$DISABLED,true,1);
			$content .= "</div><div class=\"div20 marg6\" >\n";
      	$content .= AdminFunctions::subScopeSelectMulti($this->crewID,$this->subCrewIDs,$DISABLED,true);
$content .= <<<FIN
</div>
<div class="div20 marg" ><label for="area" >Area<input type="text" name="area" id="area" class="required" value="$this->areaName" $DISABLED /></label></div>
<div id="welldiv" class="div18 marg" >
FIN;
			$content .= AdminFunctions::wellSelectMulti($this->areaID,$this->wellIDs,$DISABLED,true);
$content .= <<<FIN
</div>
<input type="hidden" name="areaID"  id="areaID_1" value="$this->areaID" />
<div id="coodiv_1" class="coodiv" ></div>
<input type="hidden" name="multi[]" id="multi_1" value="$this->wellIDs" />
<div style="clear:both;" ></div>
<div class="div95"  style="width:99.5%;" ><label for="comments" class="label">Comments</label><textarea class="txt" style="width:100%;" name="comments" id="comments" $DISABLED >$this->comments</textarea>
</div></fieldset>
<fieldset ><legend style="margin-left:47.5%;">Signed</legend>
<div class="div50">
         <div class="div40" ><img src="$this->conSignature" height="180" width="300" /></div>
         <div class="div15 marg2" ><label for="apprdate" >Date<input type="text" name="insdate" id="insdate" class="date required"  value="$this->conSigDate" $DISABLED /></label></div>
         <div class="div30 marg2" ><label  >Signed for Contractor<input type="text" name="signee" id="signee" value="$this->conSigName" readonly /></label></div>
</div>
<div class="div50">
         <div class="div40" style="min-width:40% !important;" ><img src="$this->qgcSignature"  alt="QGC to Sign"  height="180" width="300" /></div>
         <div class="div15 marg2" ><label for="compdate" >Date<input type="text" name="approvaldate" id="approvaldate"   value="$this->qgcApprDate" disabled/></label></div>
         <div class="div30 marg2" ><label for="signee_qgc" >Signed for QGC<input type="text" name="signee_qgc" id="signee_qgc"  value="$this->qgcSignee"  disabled /></label></div>
</div>
</fieldset>

FIN;
				$content .= "<div style=\"clear:both;\" ></div> \n";
				$url = isset($_SESSION['last_request']) ? $_SESSION['last_request'] : "site_instruction.php?action=list&redo=redo&con_id=$this->contractorID";
				$content .= "\n<a href=\"$url\"   class=\"button\"  >Last Screen</a>";
				$content .= "\n</form></div>\n";
			}
			else {
				$content = "";
			}
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		private function getAllSiteIns($search,$redo) {
			$_SESSION['last_request'] = "site_instruction.php?action=list&redo=redo&con_id=$this->contractorID";
			$this->siteInsID = NULL;
         $this->equalgreater = "SELECTED=\"selected\"";
		  	$whereClause=" WHERE si.contractor_id = $this->contractorID ";
			if (! is_null($redo)) {
            if (isset($_SESSION['SQL'])) {
               $search = unserialize($_SESSION['SQL']);
            }
            else {
               $search=NULL;
            }
         }
			if (! is_null($search) && $search) {
            $_SESSION['SQL'] = pg_escape_string(serialize($search));
            if (intval($search->fieldEstID) > 0 ) {
               $this->fieldEstID = $search->fieldEstID;
               $whereClause .= " and field_estimate_id = $this->fieldEstID ";
            }
            else if ( intval($search->ID) > 0) {
               $this->siteInsID = $search->ID;
               $whereClause .= " and site_instruction_id = $this->siteInsID ";
            }
            else if ( intval($search->siteInsNum) > 0) {
               $this->siteInsNo = $search->siteInsNum;
               $whereClause .= " and instruction_no = $this->siteInsNo ";
            }
				else  if (intval($search->cooID) > 0 ) {
               $this->callOffID = $search->cooID;
               $whereClause .= " and calloff_order_id = $this->callOffID ";
            }
				else {
               if (strlen($search->creDate) > 0) {
                     $dateSelected = true;
                     $whereClause .= " and instruction_date ";
                     $this->creDate = $search->creDate;
                     switch ($search->dateType) {
                     case "equalto":
                        $whereClause .= "  =  '$search->creDate' ";
                        $this->equalto = "SELECTED=\"selected\"";
         					$this->equalgreater = NULL;
                        break;
                     case "equalgreater":
                        $whereClause .= "  >=  '$search->creDate' ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                     case "less":
                        $whereClause .= "  <  '$search->creDate' ";
                        $this->less = "SELECTED=\"selected\"";
                        break;
                     case "between":
                        if (strlen($search->endDate) > 0 ) {
                           $whereClause .= " between  '$search->creDate' and '$search->endDate' ";
                           $this->between = "SELECTED=\"selected\"";
                           $this->endDate = $search->endDate;
                           $this->hideEndDate=false;
         						$this->equalgreater = NULL;
                        }
                        else {
                           $whereClause .= "  =  '$search->creDate' ";
                        }
                        break;
                     default:
                        $whereClause .= " and instruction_date  >=  current_date ";
                        $this->equalgreater = "SELECTED=\"selected\"";
                        break;
                  }
               }
					if ($search->areaID > 0 ) {
                  $whereClause .= " and co.area_id = $search->areaID ";
                  $this->areaName = $search->areaName;
                  $this->areaID = $search->areaID;
               }
               if ($search->wellID > 0 ) {
						$whereClause .= " and $search->wellID in (select (unnest(string_to_array(si.well_ids::text,'|')::int[])))";
                  $this->wellID = $search->wellID;
               }
					if (!empty($search->crewID)  ) {
                  $whereClause .= " and co.crew_id = $search->crewID ";
                  $this->crewID = $search->crewID;
               }
               if (!empty($search->subCrewID)  ) {
                  $whereClause .= " and $search->subCrewID in (select (unnest(string_to_array(co.sub_crew_ids::text,'|')::int[])))";
                  $this->subCrewID = $search->subCrewID;
               }

			   }
         }
         else {
             // Default query still needs search object serialized
            $this->creDate = date ('d-m-Y');
            $whereClause .= " and instruction_date >= current_date";
            $this->equalgreater = "SELECTED=\"selected\"";
            $search = new FieldSearch();
            $search->dateType = "equalgreater";
            $search->creDate = $this->creDate;
         }
			if(! is_null($this->contractorID)) {
         	$_SESSION['SQL'] = pg_escape_string(serialize($search));
				$sql = "SELECT si.*,a.area_name,cr.crew_name,wells_from_ids(si.well_ids) as well_name,subscopes_from_ids(co.sub_crew_ids) as sub_scopes,c.con_name 
				from {$this->shortName}_site_instruction si 
				LEFT JOIN contractor c on c.contractor_id = si.contractor_id
				LEFT JOIN calloff_order co using (calloff_order_id)  
				LEFT JOIN area a on a.area_id = co.area_id 
				LEFT JOIN crew cr on cr.crew_id = co.crew_id 
				$whereClause
				and si.removed is false 
				order by instruction_date";
			   //echo $sql;
				if (! $rs=$this->conn->Execute($sql)) {
					if ($this->conn->ErrorNo() != 0 ) {
						die($this->ErrorMsg());
					}
				}
				$content ="<div style=\"width:1772px;margin:auto;\" >\n";
				$content  .= <<<FIN
 				<form name="site_instruction" method="post" action="site_instruction.php?action=list" >
         	<input type="hidden" name="wellID" id="wellID" value="$this->wellID" />
         	<input type="hidden" name="areaID" id="areaID" value="$this->areaID" />
         	<input type="hidden" name="con_id" id="contractorID" value="$this->contractorID" />
			   <input type="hidden" name="line_no" id="line_no" value="$this->lineNo" />
            <input type="hidden" name="div_type" id="div_type" value="$this->divType" />
<fieldset ><legend style="margin-left:40%;">Filter Search - Site Instructions: $this->contractorName</legend>
<div class="div8" ><label >Site Instruction No<input type="text" name="site_ins_num" id="site_ins_num" value="$this->siteInsNo" /></label></div>
<div class="div5 marg1" ><label>C.O.O.<input type="text" name="calloff_id" id="calloff_id" value="$this->callOffID" /></label></div>
<div style="width:1%;height:1rem;float:left;"></div>
FIN;
				$content .= AdminFunctions::dateSelect($this->equalgreater,$this->equalto,$this->between,$this->less,$this->creDate,$this->endDate,$this->hideEndDate);
         	$content  .= <<<FIN
<div class="div10 marg1" ><label for="area" >Area<input type="text" name="area" id="area" value="$this->areaName" /></label></div>
<div class="div10 marg1" ><div id="welldiv" >
FIN;
				$content .= AdminFunctions::wellSelect($this->areaID,$this->wellID,"");
				$content .= "</div></div>\n";
				$content .= "<div class=\"div10 marg1\"  ><label  >Work Scope</label>";
				$content .= AdminFunctions::crewSelect($this->crewID,"",true);
				$content .= "</div>\n";
				$content .= "<div class=\"div10 marg1\" id=\"subscopediv\" >";
				$content .= AdminFunctions::subScopeSelect($this->crewID,$this->subCrewID,"");
				$content .= "</div>\n";
$content .= <<<FIN
<script>
 $( document ).ready(function() {
    var line = $('#line_no').val();
    var typ = $('#div_type').val();
    if (line.length > 0 ) {
      var reqid = $('#siteline_' + line).val();
      var conid = $('#contractorID').val();

      fieldDiv('#show_'+line,line,reqid,conid,typ);

    }
});
  $.fn.clearForm = function() {
      return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
          return $(':input',this).clearForm();
        if (type == 'text' || type == 'password' ||  tag == 'textarea')
          this.value = '';
        else if (type == 'checkbox' || type == 'radio')
          this.checked = false;
       else if (tag == 'select')
         this.selectedIndex = -1;
      });
    };
$(function(){
   $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
$(function () {
      $('#area').autocomplete({
         width: 400,
         delimiter: /(,|;)\s*/,
         serviceUrl:'autocomplete.php?type=area',
         minChars:1,
         onSelect: function(value, data){ $('#areaID').val(data);childSelect(data,'well',$('#welldiv'),'#contractorID'); return false; }
      });
  });
$(document).on('change','#crew',function() { getSubScopes($(this),'#subscopediv','single'); });
</script>
FIN;
         $content .= "<div style=\"float:right;width:14%;\" >\n";
         $content .= "\n<input type=\"submit\" name=\"search\" value=\"search\"  class=\"button\"/>";
         $content .= "\n<button  onclick=\"$('form').clearForm();$(':hidden').val(0);return false;\" class=\"marg\" >Reset</button>";
         //$content .= "<a href=\"hours.php?action=list&search=true&printcsv=true\" class=\"submitbutton printcsv\" >Print C.S.V.</a>";
         $content .= "</div></fieldset></form>\n";


			$content .= "<div class=\"heading\" >\n";
			$content .= "<div class=\"usr hd\" style=\"width:118px;\" >Instruction Date</div>\n";
			$content .= "<div class=\"usr wd  hd padr\">Instruction No</div>\n";
			$content .= "<div class=\"time hd padr\">C.O.O.</div>\n";
         $content .= "<div class=\"ptype hd\">Work Scope</div>\n";
			$content .= "<div class=\"ptype hd\">Contractor Estimate </div>\n";
			$content .= "<div class=\"cli wider hd\"  style=\"width:232px;\" >Area</div>\n";
			$content .= "<div class=\"workscope hd\" style=\"width:343px;\" >Well/s</div>\n";
			$content .= "<div class=\"workscope  hd nopad\">Comments</div>\n";
			$content .= "<div class=\"links hd\"  style=\"width:116px;\"  >Actions</div></div>\n";
			
			$lineNo = 0;
			while (! $rs->EOF ) {
				$approved = $val = "";
				$approveButText = "Approve";

				$ln= $lineNo % 2 == 0  ? "line1" : "";
				$siteInsID = $rs->fields['site_instruction_id'];
				$siteInsNo = $rs->fields['instruction_no'];
				$field_est_id = intval($rs->fields['field_estimate_id']);
				$conID =  intval($rs->fields['contractor_id']);
				$cOOID =  intval($rs->fields['calloff_order_id']);
				$conSig = !empty($rs->fields['contractor_sig']) ? true : false;
            if ($conSig) {
               $est = floatval($rs->fields['estimate_total']);
            }
            if ($conSig ) {
               $data = AdminFunctions::getFieldApproval($siteInsID,$conID,"site");
               if (!is_null($data)) {
                  if ( ($data['countall']) == $data['countdate'] ) {
                     $approved =  "grn";
                     $approveButText = "Approved";
                  }
               //if ( strlen($data['contractor_sig_date']) > 5 ) {
                //  $val = "<a href=\"field_estimate.php?action=find&field_id=$field_est_id&con_id=$conID\" title=\"View Contractor Estimate\" class=\"linkbutton $approved\" >Field Est</a><span class=\"est\" >$conEst</span>\n";
               //}
               }
               if ($conEst = $est > 0 ) {
                  $val = "<div style=\"width:122px;margin:auto;\" >\n";
                  $val .= "<button id=\"show_$lineNo\" class=\"linkbutton $approved\" style=\"min-width:55px !important;\" onclick=\"fieldDiv('#show_$lineNo',$lineNo,$siteInsID,$conID,'site');\" value=\"closed\" >" . number_format($rs->fields['estimate_total']) . "</button>\n";

                  $val .= "<button id=\"approveall_$siteInsID\" class=\"linkbutton $approved\" style=\"min-width:55px !important;\" onclick=\"approveAll($(this),'#show_$lineNo',$siteInsID,$conID,'site');return false;\" >$approveButText</button>\n";
                  $val .= "</div>\n";
               }
            }

				if ($field_est_id != 0  ) {
					 $coo = "<a href=\"calloff.php?action=find&calloff_id=$cOOID\" title=\"C.O.O.\" class=\"linkbutton\" style=\"width:45px;margin-left:1.2rem;\"  >$cOOID</a>\n";
				}
				else {
					$coo = $cOOID;
				}
				$area = $rs->fields['area_name'];
				$well = $rs->fields['well_name'];
				$crew = $rs->fields['crew_name'];
				$sub_crew =  $rs->fields['sub_scopes'] ; //$rs->fields['sub_crew_name'];
				$pad = "SUB SCOPES: ";
            $toolTip = !empty($sub_crew) ?  "<span class=\"show-option\" title=\"${pad}${sub_crew}\" >$crew</span>" : $crew;
				$insDate = AdminFunctions::dbDate($rs->fields['instruction_date']);
				$comments = $rs->fields['comments'];
				$content .= "<div style=\"float:left;width:100%;display:table;table-layout:fixed;\" id=\"linediv_$lineNo\" >\n";
				$content .= "<div class=\"usr highh $ln cntr\" style=\"width:118px;\" >$insDate</div><input type=\"hidden\" id=\"siteline_$lineNo\" value=\"$siteInsID\" />\n";
         	$content .= "<div class=\"usr wd highh $ln cntr padr\">$siteInsNo</div>\n";
         	$content .= "<div class=\"time highh $ln cntr padr\">$coo</div>\n";
         	$content .= "<div class=\"ptype highh $ln cntr\">$toolTip</div>\n";
         	$content .= "<div class=\"ptype highh $ln\">$val</div>\n";
         	$content .= "<div class=\"cli wider highh $ln\" style=\"width:232px;\"  >$area</div>\n";
         	$content .= "<div class=\"workscope highh $ln\" style=\"width:343px;\" >$well</div>\n";
         	$content .= "<div class=\"workscope highh $ln nopad \">$comments</div>\n";

				if ($this->sessionProfile > 2 ) {
					$content .= "<div class=\"links highh $ln\" style=\"width:116px;\"  >";
					$content .= "<div style=\"margin-left:0.3rem;width:50px;\" >\n";

					$content .= "<a href=\"site_instruction.php?action=find&site_ins_id=$siteInsID&con_id=$conID\" title=\"View Instruction\" class=\"linkbutton\" style=\"min-width:39px !important;\" >View</a>\n";
					$content .= "</div>\n";
				}
				else {
					$content .= "<div class=\"links highh $ln\"  style=\"width:116px;\" > &nbsp;</div>";
				}
				$content .= "</div></div>\n";
			 	$content .= "<div id=\"site_div_$lineNo\" style=\"display:none;\" ></div>\n";
				$content .= "<div style=\"clear:both;\"></div>";
				$lineNo += 1;
				$rs->MoveNext();
			}  
			$content .= "<hr /></div>";
		} // end known contractor
		else {
         $content = AdminFunctions::setContractor("site_instruction.php?action=list",$this->contractorID);
        }

			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
		 private function getInsDetails($siteInsID) {
         $sql = "SELECT si.*,a.area_name,co.area_id,co.cost_estimate,wells_from_ids(si.well_ids) as well_name,
         array_to_string(array(select field_estimate_id from {$this->shortName}_field_estimate where site_instruction_id = $siteInsID and removed is false order by field_estimate_id),', ','*') as field_ids
         from {$this->shortName}_site_instruction si 
         LEFT JOIN calloff_order co using (calloff_order_id)
         LEFT JOIN area a using(area_id) 
         where si.site_instruction_id = $siteInsID";
      // echo $sql;
         if (! $data = $this->conn->getRow($sql)) {
            die($this->conn->ErrorMsg());
         }
         $this->siteInsNo = $data['instruction_no'];
         $this->callOffID = $data['calloff_order_id'];
         $this->fieldEstIDs = $data['field_ids'];
         $this->wellName = $data['well_name'];
         $this->areaName = $data['area_name'];
         $this->areaID = $data['area_id'];
         $this->crewID = $data['crew_id'];
         $this->costEstimate = number_format($data['cost_estimate']);
         $this->estTotal = number_format($data['estimate_total']);
         $this->insDate = AdminFunctions::dbDate($data['instruction_date']);
         $this->comments = $data['comments'];
         $this->origEst = "$" .number_format($data['cost_estimate'],2);
         $this->wellIDs = $data['well_ids'];
         $this->subCrewIDs = $data['sub_crew_ids'];
		 	$this->conSignature = $data['contractor_sig'];
			$this->conSigDate = $data['contractor_sig_date'];
			$this->conSigName = $data['contractor_signee'];
			$this->qgcSignature = $data['qgc_sig'];
			$this->qgcSignee = $data['qgc_signee'];
			$this->qgcApprDate = $data['qgc_approve_date'];

      // lines
      }


	}
?>
