<?php
ini_set('display_errors','1');
error_reporting(E_ALL);
 	class WellReportAll  {
		public $page;
		public $action;
		public $dat;
		public $data = array();
		public $conn;
		public $csv;
		public $employeeID;
		public $startDate;
		public $endDate;
		public $wellArr = array();
		public $connArr;
		public $cooArr = array();
		public $lineCount;
		public $cooTotalsStr = "";
      public $grandTotal = 0;

		public function	__construct($action,$startDate,$endDate) {
			$this->action = $action;
			$this->sessionProfile = intval($_SESSION['profile_id']);
			$this->employeeID = intval($_SESSION['employee_id']);
			$this->startDate = $startDate;
			$this->endDate = $endDate;
			$this->conn = $GLOBALS['conn'];
			$this->dat = date('d-m-Y');
			if (isset($_POST['PRINT'])) {
				$file= "tmp/monthreport_{$this->startDate}_{$this->endDate}_{$this->employeeID}.csv";
		  		$this->fp = fopen($file,'w');
         	$headings = array('Contractor','Date','C.O.O.','Docket','Operator','Unit','Plant/Type','Work Scope','Sub Scope','Area','Well/s','Docket Hours','Rate','Total','C.O.O. Total Hours','C.O.O. Total');
				$heading_text = "Well Report Month $this->startDate - $this->endDate";
         	$title = array("$heading_text");
				fputcsv($this->fp,$title);
         	fputcsv($this->fp,$headings);
				$this->lineCount=2;
				$this->getData();
				fclose($this->fp);
				$this->printFile($file);
	
			}
			else {
				$pg = new Page('coo');
				$this->page= $pg->page;
				if ($action == "print" ) {
					$heading_text = "Well Report Month $this->startDate - $this->endDate";
					$button = "<input type=\"submit\" name=\"PRINT\" value=\"Print\" class=\"button\" />\n";
					$this->setHeaderText($heading_text);
		 			$this->setContent($action,$button);	

				}
				echo $this->page;
			}
		}
		private function printFile($file) {
			if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file)); //Remove
           	readfile($file);
           	exit;
      	}
		}

		private function setHeaderText($header_text) {
			$this->page = str_replace('##CURRENT##',$header_text,$this->page);
		}

		private function setContent($action,$button) {
			 $content = <<<FIN
<div style="width:800px;margin:auto;">
<form name="well_all" id="well_all" method="post" action="wellReportAll.php?action=$action">
<fieldset ><legend style="margin-left:30%;">Well Report Month $this->startDate - $this->endDate</legend>
<div class="div15 marg1" ><label >Start Date:<input type="text" name="startdate" id="sdate" class="required date" value="$this->startDate"  /></label></div>
<div class="div15 marg" ><label >End Date:<input type="text" name="enddate" id="edate" class="required date" value="$this->endDate"  /></label></div>
<div style="float:right;width:18%;" >
$button
</div>
</fieldset>
</form>
</div>
<script>
$(function(){
 $(".date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
FIN;
			$this->page = str_replace('##MAIN##',$content,$this->page);
		}
	private function getData() {
		$tmpArr = array();
		$this->connArr = AdminFunctions:: getAllCon();
		foreach($this->connArr as $ind=>$val)  {
			extract($val);
			$sql = "with t as (select distinct(calloff_order_id) from {$name}_hour h where hour_date between '$this->startDate' and '$this->endDate' and h.removed is false and h.status >= 5 )
			select co.calloff_order_id,well_ids from calloff_order co
			JOIN t using(calloff_order_id)
         order by area_id,calloff_order_id,well_ids"; 
			//WHERE area_id = 22


      	if (! $data = $this->conn->getAll($sql)) {
         	if ($this->conn->ErrorNo() != 0 ) {
           		die($this->conn->ErrorMsg());
        		}
     		}
			if (count($data) > 0 ) {
				foreach($data as $ind=>$val) {
					extract($val);
					$this->data[] = array('wellids'=>$well_ids,'coos' => $calloff_order_id);
				}

			}
		}
		while (!$this->uniqueWells($this->data)) {
			$this->data = $this->reduceArray($this->data);
		}

		$this->data = $this->sortData($this->data);

		foreach($this->data as $ind =>$val) {
			extract($val);
			//if (substr($wellids,0,4) == "1692" )  {    // TODO    remove  this !!
            $this->getCooData($val);
         //}
		}
	}
	private function sortData($data) {
		$sql = "TRUNCATE table tmp_wells";
		if (! $res = $this->conn->Execute($sql)) {
			die($this->conn->ErrorMsg());
		}
		foreach($data as $ind=>$val) {
			extract($val);
			$wellArr	= explode("|",$wellids);
			$well = $wellArr[0];
			$sql = "SELECT  area_name from well JOIN area using (area_id) where well_id = $well";
      	if (! $aName = $this->conn->getOne($sql)) {
					$aName = 'UNKNOWN';
           		//die($this->conn->ErrorMsg());
     		}
		   $sql2 = "INSERT into tmp_wells values ('$aName','$wellids','$coos')";
			if (! $res = $this->conn->Execute($sql2)) {
				die($this->conn->ErrorMsg());
			}
		}
		$sql = "SELECT  wells_name_order_from_ids(wellids) as wellids,coos from tmp_wells order by area_name";

		if (! $sortData = $this->conn->getAll($sql)) {
			die($this->conn->ErrorMsg());
		}
		return $sortData;
	}
	private function uniqueWells($data) {
		$wellStr = "";
		foreach($data as $ind=>$val) {
			extract($val);
			$wellArr	= explode("|",$wellids);
			foreach($wellArr as $well) {
				if (!stristr($wellStr,$well)) {
					$wellStr .= "$well|";
				}
				else {
					return false;
				}
			}
		}
		return true;
	}

	private function reduceArray($data) { 
		$newArr = array();
		$cnt = count($data);
		for($x=0;$x<$cnt;$x++) {
			for ($y=$x+1;$y<$cnt;$y++) {
				$wells1 = $data[$x]['wellids'];	
				if (!empty($data[$y]['wellids'])) {
					$wells2Arr = explode("|",$data[$y]['wellids']);
					foreach($wells2Arr as $well) {
						if (stristr($wells1,$well)) {
				  			$data[$x]['coos'] .= "|".$data[$y]['coos'];
				   		foreach($wells2Arr as $w) {
								if (!stristr($wells1,$w)) {
									$wells1 .= "|$w";
								}
							}	
							$data[$x]['wellids'] = $wells1;
							$data[$y] = array('wellids'=>"",'coos'=>"");
							break;
						}

					}  //foreach
				} //  not  empty
			}  // y

		}  // x
		foreach($data as $ind=>$val) {
			extract($val);
			if($wellids != "") {
				$newArr[] = array('wellids'=>$wellids,'coos'=>$coos);
			}
		}
		return $newArr;
	}
	
	private function getCooData($cooLine) {
		$line = 1;
		$this->cooTotalsStr ="";
		extract($cooLine);
		$wellCooArr = $this->getCoosFromWells($wellids,$coos);
		$cooArr = explode("|",$coos);
		foreach ($cooArr as $coo ) {
			$sql = "Select contractor_id,name,con_name from calloff_order co
			LEFT JOIN contractor c using(contractor_id)
			where co.calloff_order_id = $coo";
      	if (! $data = $this->conn->getRow($sql)) {
					echo "dies here  $coo ";
          		die($this->conn->ErrorMsg());
     		}
			extract($data);
			$sql = "SELECT h.*,coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,pt.p_type,plant_unit,wells_from_ids(h.well_ids) as well_name,a.area_name,c.crew_name,sub_crew_name
               from {$name}_hour h
               LEFT JOIN {$name}_plant p using (plant_id)
               LEFT JOIN plant_type pt using (plant_type_id)
               LEFT JOIN area a using (area_id)
               LEFT JOIN {$name}_employee e using (employee_id)
					LEFT JOIN crew c  using (crew_id)
               LEFT JOIN sub_crew sc  using (sub_crew_id)
					where calloff_order_id = $coo and hour_date between '$this->startDate' and '$this->endDate' and h.removed is false and  h.status >= 5
               order by hour_date";

			if (!$hourData = $this->conn->getAll($sql)) {
				if ($this->conn->ErrorNo() != 0 ) {
					die($this->conn->ErrorMsg());
				}

			}

			$startGroup = $this->lineCount + 1;
			$loopCount = 1;
			$gTot = $dktTot = 0;
			foreach($hourData as $ind=>$val) {
				extract($val);
				if ($loopCount == 1 ) {
					$cntArr = explode(",",$well_name);
					$wellCount = count($cntArr);
					$coo = $calloff_order_id;
				}
				$hour_date = AdminFunctions::dbDate($hour_date);
				$total_t1 = floatval($total_t1);
            $total_t2 = floatval($total_t2);
            $expense = floatval($expense);
				if ($docket_hours_t1 > 0 ) {
					$dktTot += $docket_hours_t1;
            	$dktHours =  sprintf("%'.05.2f",$docket_hours_t1);
				}
				else {
					$dktTot += $docket_hours_t2;
					$dktHours =  sprintf("%'.05.2f",$docket_hours_t2);
				}
            if ($total_t1 > 0 ) {
					 $gTot += $total_t1;
                $total = sprintf('%01.2f',$total_t1);
            }
            else if ($total_t2 > 0 ) {
					 $gTot += $total_t2;
                $total = sprintf('%01.2f',$total_t2);
            }
            else if ($expense > 0 ) {
					 $gTot += $expense;
                $total = sprintf('%01.2f',$expense);
            }
				$tmpArr = array($con_name,$hour_date,$coo,$docket_num,$opname,$plant_unit,$p_type,$crew_name,$sub_crew_name,$area_name,$well_name,$dktHours,$rate,$total);
				fputcsv($this->fp,$tmpArr,",",'"');
            $this->lineCount ++;
				$loopCount ++;
			}
			$this->cooArr[$coo] = array('well_count'=>$wellCount,'gtot'=>$gTot,'dkt_tot'=>$dktTot);
				$this->grandTotal += $gTot;
			fputcsv($this->fp,array("","","","","","","","","","","","","","",'=ROUND(SUM(L'.$startGroup.':L'.$this->lineCount.'),2)','=ROUND(SUM(N'.$startGroup.':N'.$this->lineCount .'),2)'));
			$this->lineCount ++;
			$this->cooTotalsStr .="P$this->lineCount,";

		}  // foreach COO
		fputcsv($this->fp,array());
		fputcsv($this->fp,array("Area","Well","COO's","Well Hours","Well Total","ALL Wells Total","ALL COO's Total"));
		$this->lineCount += 2;
		$this->summaryCOO($wellCooArr);
	}
	private function summaryCOO($wellCooArr)  {
		$startWell = $this->lineCount +1;
		foreach($wellCooArr as $wellID=>$val) {
			extract($val);
			$callTotArr = $this->getTotals($coos);
			$tmpArr = array($area_name,$well_name,$coos,$callTotArr[0],$callTotArr[1]);
			fputcsv($this->fp,$tmpArr);
			$this->lineCount ++;
		}
		$sumCOO = '=SUM('. preg_replace('/,$/',"",$this->cooTotalsStr) . ')';
		fputcsv($this->fp,array("","","","","",'=ROUND(SUM(E'.$startWell.':E'.$this->lineCount.'),2)',$sumCOO));
		$this->lineCount ++;
		fputcsv($this->fp,array());
		fputcsv($this->fp,array());
		$this->lineCount += 2;
	}
	private function getTotals($coos) {  // from cooArr for  this  well
		reset($this->cooArr);
		$dktTot = $gTot = 0;
		$callArr = explode(",",$coos);
		foreach($callArr as $coo ) {
			foreach($this->cooArr as $c=>$v) {
				extract($v);
				if ($c == $coo) {
					$dktTot += ($dkt_tot / $well_count);
					$gTot += ($gtot / $well_count);
				}
			}
		}
		return array(round($dktTot,2),round($gTot,2));
	}
	private function getCoosFromWells($wellStr,$cooLine) {
		$arr = array();
		$wells = explode("|",$wellStr);
		foreach($wells as $well) {
			$sql = "SELECT coos,w.well_name,a.area_name from coos_from_well('$well','$cooLine') as coos
			JOIN well w on w.well_id = $well 
			JOIN area a using(area_id)";	
			if (! $data=$this->conn->getRow($sql)) {
				echo $this->grandTotal;
				echo "dies coos from wells  $well  from $cooLine";
				die($this->conn->ErrorMsg());
			}
			extract($data);
			$arr[$well]= array('coos'=>$coos,'well_name'=>$well_name,'area_name'=>$area_name);
		}
		return($arr);
	}
}
?>
