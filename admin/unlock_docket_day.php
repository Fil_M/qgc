<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action = "list";
	}

	$con_id = !empty($_REQUEST['con_id'])   ? $_REQUEST['con_id'] : NULL;
	$coo_id = !empty($_REQUEST['coo_id'])   ? $_REQUEST['coo_id'] : NULL;
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;



	 if (isset($_REQUEST['search']) ) {
      $search = new FieldSearch();
      $search->contractorID =!empty($_REQUEST['con_id']) ? intval($_REQUEST['con_id']): NULL;
      $search->cooID =!empty($_REQUEST['calloff_id']) ? $_REQUEST['calloff_id'] : NULL; 
		$search->areaID =!empty($_REQUEST['areaID']) ? intval($_REQUEST['areaID']): NULL;
      $search->areaName = !empty($_REQUEST['area'])   ? $_REQUEST['area'] : NULL;
      $well =!empty($_REQUEST['well']) ? $_REQUEST['well'] : NULL;    //   different 
		$search->wellID = ! is_null($well) && is_array($well)  ? $well[0] : $well;
		$search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalto";
      $search->creDate = !empty($_REQUEST['com_date'])  ? $_REQUEST['com_date'] : NULL; 
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;


   }
	else if (isset($_REQUEST['setcon'])) {
      $search = NULL;
		$con_id = $_REQUEST['contractor'];
	}
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }


	$pg = new UnlockDocketDay($action,$coo_id,$con_id,$search,$redo);
?>
