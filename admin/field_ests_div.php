<?php  // ajaxes from  Field Estimates 
 	require_once("include/boot.php");
	ini_set('display_errors','1');
	error_reporting(E_ALL);
	
   $statusArr= array("0"=>"#ecefbe","1"=>"#efd38f","2"=>"#ef3e55","3"=>"#e1c0ef","4"=>"#ef8e8e","5"=>"#cdefbd","6"=>"#d8946a","7"=>"#b0a96b","8"=>"#c0dcef","9"=>"#b4b4b4","10"=>"#f1793d");
	$requestID = $_REQUEST['req_id'];
	$siteInsID = $_REQUEST['req_id'];
	$conID = $_REQUEST['con_id'];
	$type = $_REQUEST['type'];
	$lineNo = $_REQUEST['lineno'];
	$dbarr=unserialize($_SESSION['dbarr']);
	$shortName = $dbarr[$conID][2];
	$conDomain = $dbarr[$conID][3];
//  Bind search  to  form  search
	if ($type == "cost" ) {
		$sql = "SELECT field_estimate_id,site_instruction_id,calloff_order_id,total_hours,estimate_total,qgc_signee,contractor_signee,c.crew_name,sc.sub_crew_name,comments
                   from {$shortName}_field_estimate fe
                  LEFT JOIN crew c  using (crew_id)
                  LEFT JOIN sub_crew sc  using (sub_crew_id)
						WHERE request_estimate_id = $requestID 
						ORDER BY field_estimate_id";
	}
	else {
		$sql = "SELECT field_estimate_id,site_instruction_id,calloff_order_id,total_hours,estimate_total,qgc_signee,contractor_signee,c.crew_name,sc.sub_crew_name,comments
                   from {$shortName}_field_estimate fe
                  LEFT JOIN crew c  using (crew_id)
                  LEFT JOIN sub_crew sc  using (sub_crew_id)
						WHERE site_instruction_id = $siteInsID 
						ORDER BY field_estimate_id";

	}
	$content = "<div style=\"clear:both;\"></div>\n";
	$content .= "<div class=\"heading\" >\n";
   $content .= "<div class=\"usr hd wbdr\"  style=\"width:118px;\"  >Field Estimate</div>\n";
   $content .= "<div class=\"time hd padr wbdr\" style=\"width:86px;\" >Total Units</div>";
   $content .= "<div class=\"usr wd hd padr wbdr\">Field Estimate Totals</div>";
   $content .= "<div class=\"ptype hd wbdr\"   >Work Scope</div>\n";
   $content .= "<div class=\"ptype hd wbdr\"   >Sub Scope</div>\n";
   $content .= "<div class=\"time hd wbdr\"  >COO</div>\n";
   $content .= "<div class=\"usr wd hd nopad wbdr\"  >Site Instruction ID</div>\n";
   $content .= "<div class=\"contact hd wbdr\"  style=\"padding-left:2px;\" >Contractor Signee</div>\n";
   $content .= "<div class=\"contact hd wbdr\"   >QGC Signee</div>\n";
   $content .= "<div class=\"workscope hd nopad wbdr\"   >Comments</div>\n";
   $content .= "<div class=\"links hd wbdr\" style=\"width:116px;\"   >Approve</div>\n";
   $content .= "</div>\n"; // Close heading

	if (! $rs=$conn->Execute($sql)) {
            die($this->ErrorMsg());
   }
	$lNo = 0;
	while (! $rs->EOF ) {
		extract($rs->fields);
		$color = $statusArr[0];
		$butText = "Approve";
		$butClass = "oran";
		if (!empty($qgc_signee)) {
			$color = $statusArr[5];
			$butText =  "Approved";
			$butClass = "";
		}

		$yesChecked = !empty($qgc_signee) ? "checked" :"";

		$calloff_order_id = intval($calloff_order_id) > 0 ? $calloff_order_id : "";
		$estimate_total  = number_format($estimate_total);
		$content .= "<div id=\"field_$field_estimate_id\" style=\"background:$color;display:table;table-layout:fixed;border-bottom:1px solid #000;float:left;\" >";
    	$content .= "<div class=\"usr cntr highh\"  style=\"width:118px;\"  >$field_estimate_id</div>\n";
    	$content .= "<div class=\"time txtr highh\" style=\"width:86px;\" >$total_hours</div>";
    	$content .= "<div class=\"usr wd txtr highh\">$estimate_total</div>";
    	$content .= "<div class=\"ptype highh\"   >$crew_name</div>\n";
    	$content .= "<div class=\"ptype highh\"   >$sub_crew_name</div>\n";
    	$content .= "<div class=\"time highh\"  >$calloff_order_id</div>\n";
    	$content .= "<div class=\"usr wd highh nopad cntr\"  >$site_instruction_id</div>\n";
    	$content .= "<div class=\"contact highh\"  style=\"padding-left:2px;\" >$contractor_signee</div>\n";
    	$content .= "<div class=\"contact highh\"   >$qgc_signee</div>\n";
    	$content .= "<div class=\"workscope highh nopad\"   >$comments</div>\n";
    	$content .= "<div class=\"links highh\" style=\"width:116px;\"  >";
		$content .= "<div style=\"width:110px;margin:auto;\" >\n";
		if (empty($qgc_signee )) {
	 		$content .="<button class=\"linkbutton $butClass\"  style=\"min-width:55px !important;\" name=\"approve_$field_estimate_id\" id=\"approve_$field_estimate_id\" value=\"yes\" onclick=\"updateField($(this),$field_estimate_id,$conID,'$type');\" >$butText</button>\n";
		}
		 $content .= "<a href=\"field_estimate.php?action=find&field_id=$field_estimate_id&con_id=$conID&lineno=$lineNo&type=$type\" title=\"View Estimate\" class=\"linkbutton\" >Field Est</a>\n";

		$content .= "</div></div>\n";
      $content .= "</div>\n";
		$lNo += 1;
  		$rs->MoveNext();

	}




	echo $content;

?>
