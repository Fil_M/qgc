<?php  // Index.php
 	require_once("include/boot.php");
 	Functions::verify();
	$employeeID = $_SESSION['employee_id'];
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}
	if (isset($_REQUEST['printcsv'])) {
      $file= $_REQUEST['file'];
      if (file_exists($file)) {
               header('Content-Description: File Transfer');
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment; filename='.basename($file));
               header('Content-Transfer-Encoding: binary');
               header('Expires: 0');
               header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
               header('Pragma: public');
               header('Content-Length: ' . filesize($file)); //Remove

            readfile($file);
            exit;
      }

   }

	$con_id = isset($_REQUEST['contractor']) && intval($_REQUEST['contractor']) > 0  ? $_REQUEST['contractor'] : NULL;
	$hour_id = isset($_REQUEST['hour_id']) ? $_REQUEST['hour_id'] : 0;
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;
	$order=isset($_REQUEST['order']) ? $_REQUEST['order'] : NULL;


	 if (isset($_REQUEST['search']) ) {
      $search = new Search();
      //$search->hourID =intval($_REQUEST['hour_id']) > 0 ? $_REQUEST['hour_id'] : NULL;
      $search->cooID = !empty($_REQUEST['coo_id']) ? $_REQUEST['coo_id'] : NULL;
      $search->operatorID = !empty($_REQUEST['operatorID'])   ? $_REQUEST['operatorID'] : NULL;
      $search->operatorName =  !empty($_REQUEST['operator'])  ? $_REQUEST['operator'] : NULL;
      $search->machineName =  !empty($_REQUEST['machine'])   ? $_REQUEST['machine'] : NULL;
      $search->areaID = !empty($_REQUEST['areaID'])  ?  $_REQUEST['areaID'] : NULL;
      $search->areaName =  !empty($_REQUEST['area'])  ? $_REQUEST['area'] : NULL;
		$well = isset($_REQUEST['well'])  ?  $_REQUEST['well'] : NULL; 
		if (!is_null($well)) {
			$search->wellID = is_array($well)  ?  intval($well[0]) : $well; 
		}
      $search->status = !empty($_REQUEST['status']) && intval($_REQUEST['status']) > -1 ? intval($_REQUEST['status']) : NULL;
      $search->unitID = !empty($_REQUEST['unitID'])  ? $_REQUEST['unitID'] : NULL;
      $search->unitName =  !empty($_REQUEST['unit']) ? $_REQUEST['unit'] : NULL;
		$search->plantTypeID = !empty($_REQUEST['plantTypeID']) ? $_REQUEST['plantTypeID'] : NULL;
      $search->plantName =  !empty($_REQUEST['machine'])  ? $_REQUEST['machine'] : NULL;
      $crewID =isset($_REQUEST['crew']) ? $_REQUEST['crew'] : NULL; 
      $search->crewID = ! is_null($crewID) && isset($crewID[0])  ? $crewID[0] : NULL; 
      $search->subCrewID = isset($_REQUEST['sub_scope'])  ? $_REQUEST['sub_scope'] : NULL; 
      $search->noInvoice =isset($_REQUEST['noinvoice']) ? true : false;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalto";
      $search->hDate = !empty($_REQUEST['sdate'])   ? $_REQUEST['sdate'] : NULL;
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;
      $search->invoiceNum = !empty($_REQUEST['invoicenum'])  ? trim($_REQUEST['invoicenum']) : NULL;
      $search->dktFrom = !empty($_REQUEST['dktfrom'])  ? $_REQUEST['dktfrom'] : NULL;
      $search->dktTo = !empty($_REQUEST['dktto'])  ? $_REQUEST['dktto'] : NULL;

   }
   else {
      $search = NULL;
      // unset($_SESSION['SQL']); 
   }


	$pg = new HourCSV($action,$hour_id,$con_id,$search,$redo,$order);
?>
