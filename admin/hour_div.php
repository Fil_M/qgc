<?php  // ajaxes from hours 
 	require_once("include/boot.php");
	ini_set('display_errors','1');
	error_reporting(E_ALL);
   $statusArr= array("0"=>"#ecefbe","1"=>"#efd38f","2"=>"#ef3e55","3"=>"#e1c0ef","4"=>"#ef8e8e","5"=>"#cdefbd","6"=>"#d8946a","7"=>"#b0a96b","8"=>"#c0dcef","9"=>"#b4b4b4","10"=>"#f1793d");
	$dat = $_REQUEST['date'];
	$cooID = $_REQUEST['coo_id'];
	$suf = explode("_",$_REQUEST['dv']); //  parent div number
	$suffix = $suf[1];
	$conID = $_REQUEST['con_id'];
	$dbarr=unserialize($_SESSION['dbarr']);
	$shortName = $dbarr[$conID][2];
	$conDomain = $dbarr[$conID][3];
//  Bind search  to  form  search
	$andClause = "";
 	if (isset($_SESSION['SQL'])) {
      $search = unserialize($_SESSION['SQL']);
    }
    else {
        $search=NULL;
    }
    if (! is_null($search) && $search) {
		 if (isset($search->operatorID)  && intval($search->operatorID) > 0 ) {
			$andClause .= " and h.employee_id = $search->operatorID ";
		 } 
		 if (isset($search->operatorID)  && intval($search->unitID) > 0 ) {
			$andClause .= " and h.plant_id = $search->unitID ";
		 }
		 if (isset($search->operatorID)  && intval($search->areaID) > 0 ) {
			$andClause .= " and h.area_id = $search->areaID ";
		 } 
		 if (isset($search->operatorID)  && intval($search->wellID) > 0 ) {
			$andClause .= " and h.well_id = $search->wellID ";
		 }
		 if (!empty($search->plantTypeID) ) {
           $andClause .= " and  h.plant_id in (select plant_id from {$shortName}_plant where plant_type_id = $search->plantTypeID)";
       }
	 }

	$sql = "SELECT h.hour_id,h.hour_date,h.plant_id,h.crew_id,h.rate,h.details,h.docket_hours_t1,h.total_t1,coalesce(h.reject_text,'') as reject_text,
	h.docket_hours_t2,h.total_t2,h.expense,h.standdown_reason_id, pt.p_type,a.area_name,c.crew_name,sc.sub_crew_name,sr.reason,h.status,
	coalesce(e.firstname,'') || ' ' || coalesce(e.lastname,'') as opname,up.upload_name
                   from {$shortName}_hour h
                  LEFT JOIN {$shortName}_employee  e using (employee_id)
                  LEFT JOIN {$shortName}_plant  p using (plant_id)
                  LEFT JOIN plant_type pt using (plant_type_id)
                  LEFT JOIN area a using (area_id)
                  LEFT JOIN crew c  using (crew_id)
                  LEFT JOIN sub_crew sc  using (sub_crew_id)
						LEFT JOIN standdown_reason sr using (standdown_reason_id)
						LEFT JOIN {$shortName}_upload up on up.upload_id = (select max(upload_id) from {$shortName}_upload upp where  upload_type_id = 8 and upp.upload_date = h.hour_date )
						WHERE hour_date = '$dat' and h.removed is false and calloff_order_id = $cooID and h.status > 0
						$andClause
						ORDER BY hour_id";
	 $content = "<div style=\"clear:both;\" ></div>\n";
	 $content  .= "<div class=\"heading\" >";
    $content .= "<div class=\"contact hd wbdr\">Plant/Type</div>\n";
    $content .= "<div class=\"usr hd wbdr\" >Operator</div>\n";
    $content .= "<div class=\"vehicle hd wbdr\">Work Scope</div>\n";
    $content .= "<div class=\"vehicle hd wbdr\">Sub Scope</div>\n";
    $content .= "<div class=\"cli  hd wbdr\" style=\"width:162px;\"  >Detail</div></a>\n";
    $content .= "<div class=\"usr hd wbdr\"  >T2 Reason</div>\n";
    $content .= "<div class=\"weight hd padr wbdr\">T1 Hours</div>";
    $content .= "<div class=\"time hd padr wbdr\"  >T1 Total</div>\n";
    $content .= "<div class=\"weight hd padr wbdr\"  >T2 Hours</div>\n";

    $content .= "<div class=\"time hd padr wbdr\">T2 Total</div>\n";
    $content .= "<div class=\"weight hd padr wbdr\">Expense</div>\n";
	 $content .= "<div class=\"approv hd wbdr\"  >Approve</div>\n";
    $content .= "<div class=\"usr hd  wbdr\" >Rejection Reason</div>\n";
	 $content .= "<div class=\"links hd  wbdr\" style=\"width:95px;\" >Actions</div>\n";

    $content .= "</div>\n"; // Close heading
	 //$content .= "<br />$sql";

	if (! $rs=$conn->Execute($sql)) {
            die($this->ErrorMsg());
   }
	$lNo = 0;
	while (! $rs->EOF ) {
		$status = $rs->fields['status'];
		$color = $statusArr[$status];
		$yesChecked = $status == 5 ? "checked" : "";
      $noChecked =  ($status == 2 || $status == 4 ) ? "checked" : "";
		$hourID = $rs->fields['hour_id'];
		$jDate = $rs->fields['hour_date'];
		$plant = $rs->fields['p_type'];
		$rate = $rs->fields['rate'];
		$crew = $rs->fields['crew_name'];
		$sub_crew = $rs->fields['sub_crew_name'];
		$upName = $rs->fields['upload_name'];
		$detail = trim($rs->fields['details']);
		if (! is_null($upName) ) {
		 $timelink = "<a href=\"{$conDomain}/$upName\" title=\"View Timesheet\" target=\"_blank\" >\n";
		 $timelinkFin = "</a>";
		}
		else {
			$timelink =  $timelinkFin = "";
		}
		$opname = $rs->fields['opname'];
		$t2reason = $rs->fields['reason'];

		$t1Hours = !is_null($rs->fields['docket_hours_t1']) ? number_format($rs->fields['docket_hours_t1'],2) : "";
      $t2Hours = !is_null($rs->fields['docket_hours_t2'])  ? number_format($rs->fields['docket_hours_t2'],2) : "";
      $total_t1 = !is_null($rs->fields['total_t1']) ? "$" .number_format($rs->fields['total_t1'],2) : "";
      $total_t2 = !is_null($rs->fields['total_t2'])  ? "$" . number_format($rs->fields['total_t2'],2) : "";
      $expense = !is_null($rs->fields['expense']) ? "$" .number_format($rs->fields['expense'],2) : "";
		$rejTxt = strlen($rs->fields['reject_text']) > 0 ? $rs->fields['reject_text'] : "";
		$content .= "<input type=\"hidden\" id=\"hour_$hourID\" value=\"$hourID\" />\n";
		$content .= "<div id=\"line_$hourID\" style=\"background:$color;display:table;table-layout:fixed;border-bottom:1px solid #000;float:left;\" >";
		$content .= $timelink;
    	$content .= "<div class=\"contact highh\">$plant</div>\n";
    	$content .= "<div class=\"usr sf highh\" >$opname</div>\n";
    	$content .= "<div class=\"vehicle highh\">$crew</div>\n";
    	$content .= "<div class=\"vehicle highh\">$sub_crew</div>\n";
    	$content .= "<div class=\"cli highh\" style=\"width:162px;\"  >$detail</div>\n";
    	$content .= "<div class=\"usr highh\"   >$t2reason</div>\n";
    	$content .= "<div class=\"weight txtr highh\">$t1Hours</div>";
    	$content .= "<div class=\"time txtr highh\"  >$total_t1</div>\n";
    	$content .= "<div class=\"weight txtr highh\"  >$t2Hours</div>\n";
    	$content .= "<div class=\"time txtr highh\">$total_t2</div>\n";
    	$content .= "<div class=\"weight txtr highh\">$expense</div>\n";
		$content .= $timelinkFin;
    	$content .= "<div class=\"approv highh\"  >";
		if ($status <6 ) {
	 		$content .="<span class=\"approve\" style=\"margin-left:3px;\"> Approve</span><input type=\"radio\" class=\"rejj\" name=\"approve_$hourID\" id=\"approve_$hourID\" value=\"yes\" onclick=\"updateHour('approve_$hourID',$conID,$suffix);\" $yesChecked />\n";
      	$content .= "<span class=\"reject\" > Reject</span><input type=\"radio\" class=\"rejj\" name=\"approve_$hourID\"  id=\"approve_$hourID\" value=\"no\" onclick=\"updateHour('approve_$hourID',$conID,$suffix);\" $noChecked />\n";
		}
		else {
			$content .= "&nbsp;";
		}
      $content .= "</div>\n";
		$content .= "<div class=\"usr highh\" ><textarea  class=\"txtreason\" id=\"reject_$hourID\">$rejTxt</textarea></div>\n";
		$content .= "<div class=\"links highh cntr\" style=\"width:95px;\" >\n";
		$content .= "<div style=\"width:60px;margin:auto;\" >\n";
		$content .= "<button id=\"timeBut_$hourID\" class=\"linkbutton wd\" onclick=\"timedivshow($hourID,'#timediv_$hourID','#timeBut_$hourID','$jDate',$conID);return false;\">Uploads</button>";
		$content .= "</div></div>\n";  // links div
    	//$content .= "<div class=\"addr rej\" ><textarea class=\"rejecttxt\" id=\"reject_$hourID\">$rejTxt</textarea></div>\n";
		$content .= "</div>\n";  //line div
		$content .= "<div style=\"clear:both;\" > </div>\n";
		$content .= "<div  class=\"divbdr\" id=\"timediv_$hourID\"></div>\n";
		$lNo += 1;
  		$rs->MoveNext();

	}




	echo $content;

?>
