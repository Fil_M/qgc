<?php  // Index.php
 	require_once("include/boot.php");
 	Functions::verify();
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}

	if (isset($_REQUEST['work_id'])) {
		$workflow_id = $_REQUEST['work_id'];
	}
	else {
		$workflow_id = NULL;
	}
	if (isset($_REQUEST['childwin'])) {
		$childWin = $_REQUEST['childwin'];
	}
	else {
		$childWin = "false";
	}
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;
	if (isset($_REQUEST['search']) ) {
      $search = new Search();
      $search->workflowID = !empty($_REQUEST['workflow']) ? $_REQUEST['workflow'] : NULL;
		$search->clientID = !empty($_REQUEST['client_id']) ? $_REQUEST['client_id'] : NULL;
      $search->clientName = !empty($_REQUEST['client']) ? $_REQUEST['client'] : NULL;
		$search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->sDate = !empty($_REQUEST['sdate']) ? $_REQUEST['sdate'] : NULL;
      $search->endDate = !empty($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;
      $search->empID = isset($_REQUEST['sent_by']) && intval($_REQUEST['sent_by']) > 0  ? $_REQUEST['sent_by'] : NULL;
      $search->formType = !empty($_REQUEST['email_type'])  ? $_REQUEST['email_type'] : NULL;
      $search->corrType = !empty($_REQUEST['corr_type'])  ? $_REQUEST['corr_type']  : NULL;
   }
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }
	

	$pg = new Correspondence($action,$workflow_id,$search,$redo,$childWin);
?>
