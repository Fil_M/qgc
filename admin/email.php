<?php  // ajaxes from hours approve 
 	require_once("include/boot.php");
	ini_set('display_errors','1');
	error_reporting(E_ALL);
	$type = $_REQUEST['type'];
	$empID = intval($_SESSION['employee_id']);
   $signee = $_SESSION['firstname'] . " " . $_SESSION['lastname'];
	$signature = AdminFunctions::getSig($empID);
	$dat = date('d-m-Y'); 



	switch ($type) {
		case "coo" :
			email_coo();
			break;

		case "atw" :
			email_atw();
			break;

		case "super" :
			email_super();
			break;

		case "tce" :
			email_tce();
			break;

		case "approve" :
			email_approve();
			break;

		case "comp" :
			email_comp();
			break;

		case "site_req" :
			email_sitereq();
			break;

	}



	function email_comp() {
		global $conn, $signature, $signee,  $dat, $empID, $BASE ;
		$conID = $_REQUEST['conid'];
		$compID = $_REQUEST['comp_id'];
		$cooID = $_REQUEST['coo_id'];
		$eNum = AdminFunctions::email_approve_comp($compID,$conID,$cooID,$empID,"BACKGROUND");
		exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
		echo $eNum;
	}

	function email_approve() {
		global $conn,$BASE;
		$dat= $_REQUEST['date'];
		$conID = $_REQUEST['conid'];
   	$dbarr=unserialize($_SESSION['dbarr']);
		$dblink = $dbarr[$conID][0];
		$cooID = $_REQUEST['coo_id'];
		$empID = $_REQUEST['emp_id'];
		$URL="https://".$_SERVER['HTTP_HOST'];
	//$supGroup = AdminFunctions::supgroupFromCOO($cooID);

		$sql = "SELECT 'false' from hour where hour_date = '$dat' and calloff_order_id = $cooID and status = 2";
		$sql = preg_replace("/'/","''",$sql);
		$linkSQL = "SELECT  * from  dblink('$dblink','$sql',true) AS t1(txt text)";
      if (! $res=$conn->getOne($linkSQL)) {  // sets  subject
			if ($conn->ErrorNo() != 0 ) {
         	die($conn->ErrorMsg());
			}	
			else {
				$ans = " - All Hours approved. C.O.O. $cooID Date: $dat";
			}
      }
		else {
				$ans = " - Some Hours rejected. C.O.O. $cooID Date: $dat";
		}

		$eNum = AdminFunctions::insertEmailLog($empID,0,$conID,"QGC Hour Approval $ans",'APP',$cooID,$dat,$cooID);	
		exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
		echo $eNum;
	}


	function email_sitereq() {
		global $conn, $BASE;
		$conID = $_REQUEST['conid'];
		$siteReqID = $_REQUEST['sitereq_id'];
		$empID = $_REQUEST['emp_id'];
		$notify = $_REQUEST['notify'];
		$notifyDate = $_REQUEST['not_date'];
		$insNo = $_REQUEST['ins_no'];
		$cooID = $_REQUEST['coo_id'];
		$dat = date('d-m-Y');
		$URL="https://".$_SERVER['HTTP_HOST'];

		if ($notify == 1 ) { //  Create
			$eNum = AdminFunctions::insertEmailLog($empID,0,$conID,'Request for Site Instruction Estimate','REQ',$insNo,$notifyDate,$cooID);
			exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
		}
		else { // Notifucation date reached

		}
		echo $eNum;

	}
	function email_tce() {
		global $conn,$BASE;
		$conID = $_REQUEST['conid'];
		$tceID = $_REQUEST['tce_id'];
		$empID = $_REQUEST['emp_id'];
		$dat = date('d-m-Y');

		$eNum = AdminFunctions::insertEmailLog($empID,0,$conID,"Request for Target Cost Estimate",'TCE',$tceID,$dat,$tceID,"NULL");
		exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
		echo $eNum;

	}

	function email_super() {
		global $conn,$BASE;
		$conID = $_REQUEST['conid'];
		$supID = $_REQUEST['supid'];
		$empID = $_REQUEST['emp_id'];
		$supID = $_REQUEST['supid'];
		$conName = $_REQUEST['con_name'];
		$dat =  isset($_REQUEST['hdate']) ? $_REQUEST['hdate'] : date('d-m-Y');

		$eNum = AdminFunctions::insertEmailLog($empID,0,$supID,"Request for Table 2 Hour Approval - $dat for $conName",'ST2',$conID,$dat,NULL,$supID);
		exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
		echo $eNum;
	}

	function email_atw() {
		global $conn,$BASE;
		$atwID = $_REQUEST['atw_id'];
		$empID = $_REQUEST['emp_id'];
		$dat = date('d-m-Y');
		$sql = "SELECT *  from  atw where atw_id = $atwID";

		if (! $data = $conn->getRow($sql)) {
			echo $sql;
			die($conn->ErrorMsg());
		}


		$atwNumber = $data['atw_number'];
		$empFound = $contrFound = $conFound = false;
	// Emails to send  employees 
		$sql = "SELECT * from  email_from_ids('" .$data['to_email_employees'] . "')";
		$empEmails = $conn->getOne($sql);  // returns  ,<space> separated list
	// Emails to send contacts 
		$sql = "SELECT * from  contacts_email_from_ids('" .$data['to_email_contacts'] . "')";
		$conEmails = $conn->getOne($sql);  // returns  ,<space> separated list
	// Emails to send  contractors 
		$sql = "SELECT * from  contractor_email_from_ids('" .$data['to_email_contractors'] . "')";
   	$contrEmails = $conn->getOne($sql);  // returns  ,<space> separated list
		if (strlen($empEmails) > 1  ||  strlen($contrEmails) > 1 ||  strlen($conEmails) > 1 ) {  // At least one to continue 
			$eNum = AdminFunctions::insertEmailLog($empID,0,0,"Authority To Work  - $atwNumber ",'ATW',$atwID,$dat);
			if (strlen($empEmails) > 1 ) {  // try them all for  new
				$inssql = "INSERT into email_attachment (email_log_id,attachment_name,recipient_type,email) values ";
				$empArr = explode(",",$empEmails);
				foreach($empArr as $email ) {
					$empFound = true;
					$email = trim(pg_escape_string($email)); // leading  space
					$inssql .= "($eNum,'{$BASE}tmp/employee_pass.html','company',E'$email'),";   // standard STATIC HTML  Not!  generated !!! must be present in tmp !!
				}
				if ($empFound) {
         		$inssql = preg_replace('/,$/',"",$inssql);
         		if (! $res = $conn->Execute($inssql)) {
            		echo $sql;
            		die($conn->ErrorMsg());
         		}
      		}
			}
			if (strlen($contrEmails) > 1 ) {  // try them all for  new
				$inssql = "INSERT into email_attachment (email_log_id,attachment_name,recipient_type,email) values ";
				$contrArr = explode(",",$contrEmails);
				foreach($contrArr as $email ) {
					$contrFound = true;
					$email = trim(pg_escape_string($email)); // leading  space
					$inssql .= "($eNum,'{$BASE}tmp/contractor_pass.html','company',E'$email'),"; 
				}
				if ($contrFound) {
         		$inssql = preg_replace('/,$/',"",$inssql);
         		if (! $res = $conn->Execute($inssql)) {
            		echo $sql;
            		die($conn->ErrorMsg());
         		}
      		}	

			}
			if (strlen($conEmails) > 1 ) {  // try them all for  new
				$inssql = "INSERT into email_attachment (email_log_id,attachment_name,recipient_type,email) values ";
		// mke individual  username/password  inline  html attachments  and separate  attachment  record
				$conArr = explode(",",$conEmails);
				$x = 1;
				foreach($conArr as $email ) {
					$email = trim(pg_escape_string($email)); // leading  space
					$sql = "select username from employee e 
					LEFT JOIN contact c using(employee_id)
					WHERE c.contact_email = E'$email'";
					if (! $user = $conn->getOne($sql)) {
						if($conn->ErrorNo() != 0 ) {	
							die($conn->ErrorMsg());
						}
					}
					else {
						$conFound = true;
						$passName = "tmp/{$atwID}_{$x}_pass.html";
	  					$str = "<br /><br /><div>Your username and password :<span style=\"font-weight:bold;\" > $user $user </span></div><br />";
						if (! file_put_contents($passName,$str)) {
							die ("Can't write file");
						}
						$inssql .= "($eNum,'{$BASE}$passName','company',E'$email'),"; 
					}	
					$x+=1;
				}
				if ($conFound) {
					$inssql = preg_replace('/,$/',"",$inssql);
					if (! $res = $conn->Execute($inssql)) {
						echo $sql;
						die($conn->ErrorMsg());
					}
				}
			}

		// Update all documents as  emailed
			$sql = "UPDATE atw_document set status = 3 where atw_id = $atwID";
			if (! $res = $conn->Execute($sql)) {
				echo $sql;
				die($conn->ErrorMsg());
			}
			exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
			echo $eNum;
		}
		else {  // end emails  found
			echo "-1";
		}

	}
	function email_coo() {
		global $conn,$BASE;
		$conID = $_REQUEST['conid'];
		$cooID = $_REQUEST['coo_id'];
		$reqID = $_REQUEST['request_id'];
		$empID = $_REQUEST['emp_id'];
		$supIDs = $_REQUEST['sups'];
		$dat = date('d-m-Y');
		$URL="https://".$_SERVER['HTTP_HOST'];
		if (! $res= AdminFunctions::updateFieldEst($reqID,$cooID,$conID)) {  // Update COO  to  field  estimates
         die($res);
      }

		$sql = "UPDATE calloff_order set qgc_approve_date = current_date,qgc_sig = (Select signature from employee where employee_id = $empID) Where calloff_order_id = $cooID";
		if (! $res = $conn->Execute($sql)) {
			echo $sql;
			die($conn->ErrorMsg());
		}


		$eNum = AdminFunctions::insertEmailLog($empID,0,$conID,"Call Off Order - $cooID ",'COO',$cooID,$dat,$cooID,"NULL");
		exec("{$BASE}classes/batch/batch_email.batch $eNum >/dev/null 2>&1  &");
		echo $eNum;
	}

?>
