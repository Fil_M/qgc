<?php  //  QGC Hours
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	if (isset($_REQUEST['printcsv'])) {
       $file="tmp/invoice.csv";
      if (file_exists($file)) {
               header('Content-Description: File Transfer');
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment; filename='.basename($file));
               header('Content-Transfer-Encoding: binary');
               header('Expires: 0');
               header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
               header('Pragma: public');
               header('Content-Length: ' . filesize($file)); //Remove

            readfile($file);
            exit;
      }

   }

	$con_id = !empty($_REQUEST['con_id']) ? $_REQUEST['con_id'] : NULL;
	$hour_id = !empty($_REQUEST['hour_id']) ? $_REQUEST['hour_id'] : 0;
	$redo=!empty($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;
	$order=!empty($_REQUEST['order']) ? $_REQUEST['order'] : NULL;


	 if (isset($_REQUEST['search']) ) {
      $search = new Search();
      $search->cooID =!empty($_REQUEST['coo_id']) ? intval($_REQUEST['coo_id']): NULL;
      $search->areaID =!empty($_REQUEST['areaID']) ? intval($_REQUEST['areaID']): NULL;
      $search->areaName = !empty($_REQUEST['area']) ? $_REQUEST['area'] : NULL;
      $well =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;    //   different  select made from selects.php  $type
      $search->wellID =is_array($well) ? $well[0] : $well;    //   different  select made from selects.php  $type
      $search->crewID =isset($_REQUEST['crew']) ? $_REQUEST['crew'] : NULL; 
		 $search->subCrewID = isset($_REQUEST['sub_scope'])  ? $_REQUEST['sub_scope'] : NULL;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalto";
      $search->hDate = !empty($_REQUEST['sdate'])    ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;
		$search->unitID = !empty($_REQUEST['unitID']) ? $_REQUEST['unitID'] : NULL;
      $search->unitName =  !empty($_REQUEST['tag'])  ? $_REQUEST['tag'] : NULL;
		$search->plantTypeID = !empty($_REQUEST['plantTypeID']) ? $_REQUEST['plantTypeID'] : NULL;
      $search->plantName =  !empty($_REQUEST['machine'])  ? $_REQUEST['machine'] : NULL;
      $crewID =isset($_REQUEST['crew']) ? $_REQUEST['crew'] : NULL;
		$search->operatorID = !empty($_REQUEST['operator_id'])  ? $_REQUEST['operator_id'] : NULL;
		$search->operatorName = !empty($_REQUEST['operator'])   ? $_REQUEST['operator'] : NULL;
      $search->status = !empty($_REQUEST['status']) && $_REQUEST['status'] != -1  ? $_REQUEST['status'] : NULL;
   }
	else if (isset($_REQUEST['setcon'])) {
      $search = NULL;
		$con_id = $_REQUEST['contractor'];
	}
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }


	$pg = new Hours($action,$hour_id,$con_id,$search,$redo,$order);
?>
