<?php  // ajaxes from hours 
 	require_once("include/boot.php");
	ini_set('display_errors','1');
	error_reporting(E_ALL);
	
   $statusArr= array("0"=>"#ecefbe","1"=>"#efd38f","2"=>"#ef3e55","3"=>"#e1c0ef","4"=>"#ef8e8e","5"=>"#cdefbd","6"=>"#d8946a","7"=>"#b0a96b","8"=>"#c0dcef","9"=>"#b4b4b4","10"=>"#f1793d");
	$dat = $_REQUEST['date'];
	$cooID = $_REQUEST['coo_id'];
   $URL = "https://" .$_SERVER['HTTP_HOST'];
//	$suf = explode("_",$_REQUEST['dv']); //  parent div number
//	$suffix = $suf[1];
	$conID = $_REQUEST['con_id'];
	$dbarr=unserialize($_SESSION['dbarr']);
	$shortName = $dbarr[$conID][2];
	$conDomain = $dbarr[$conID][3];
	$dbLink = 	 $dbarr[$conID][0];
	$conName = 	 $dbarr[$conID][4];

//  Bind search  to  form  search

	$sql = "SELECT * from percent_est($cooID,'$dat',true)";
	$sql = pg_escape_literal($sql);
   $stmt = "SELECT * from dblink('$dbLink',$sql,true) AS t1(crew_name text,sub_crew_name text,tce_est double precision,subtot_to_date double precision,percentage_complete double precision)" ;

      if (! $data = $conn->getAll($stmt)) {
         if( $conn->ErrorNo() != 0  ) {
         	echo $stmt;
         	die($conn->ErrorMsg());
			}
      }


	 $content = "<div style=\"clear:both;\" ></div>\n";
	 $content  .= "<div class=\"heading\" >";
    $content .= "<div class=\"time hd wbdr\">Date</div>\n";
    $content .= "<div class=\"crew hd wbdr\">Contractor</div>\n";
    $content .= "<div class=\"time hd wbdr\">C.O.O.</div>\n";
    $content .= "<div class=\"cli wider hd wbdr\">Work Scope</div>\n";
    $content .= "<div class=\"cli hd wbdr\" style=\"width:238px;\" >Sub Scope</div>\n";
    $content .= "<div class=\"contact hd wbdr\">Percentage Complete</div>\n";
    $content .= "<div class=\"crew hd wbdr\" style=\"width:182px;\" >Percentage of TCE</div>\n";
    $content .= "<div class=\"crew hd padr wbdr\" style=\"width:176px;\" >Total to Date</div>\n";
    $content .= "<div class=\"approv hd wbdr\">Original Estimate</div>\n";
	 $content .= "<div class=\"links hd  wbdr\" style=\"width:225px;\">Actions</div>\n";

    $content .= "</div>\n"; // Close heading
	 //$content .= "<br />$sql";

	$lNo = 0;
	foreach($data as $key=>$val) {
		$percColor = $action = "";
		extract($val);

		$tce_est = $tce_est > 0 ? $tce_est : 1;
		$perc= $percentage_complete . "%";
		$tcePerc = round($subtot_to_date / $tce_est,2) * 100 . "%";
		if (floatval($tcePerc) > 80.0 ) {
           $percColor = "background:#ffffff;color:#ff0000";
      } 
		if ($lNo == 0 ) {
			$action = "<div style=\"width:160px;margin:auto;\" >\n";
			$action .= "<a href=\"{$URL}/admin/hour_reports.php?type=csv&con_id=$conID&date=$dat\"  class=\"linkbutton print \"  >Print C.S.V.</a>\n";
			$action .= "<a href=\"{$URL}/printSubScopepdf.php?action=print&con_id=$conID&date=$dat\"  class=\"linkbutton print\" target=\"_blank\" style=\"margin-left:15px;\"  >Print PDF</a>\n";
			$action .= "</div>\n";
		}


		$subtot_to_date = number_format($subtot_to_date);
		$tce_est = number_format($tce_est);
    	//$content .= "<div class=\"addr rej\" ><textarea class=\"rejecttxt\" id=\"reject_$hourID\">$rejTxt</textarea></div>\n";
	   $content .= "<div id=\"line_$lNo\" style=\"$percColor;display:table;table-layout:fixed;border-bottom:1px solid #000;float:left;\" >";
    	$content .= "<div class=\"time highh\">$dat</div>\n";
    	$content .= "<div class=\"crew highh\">$conName</div>\n";
    	$content .= "<div class=\"time highh\">$cooID</div>\n";
    	$content .= "<div class=\"cli wider highh\">$crew_name</div>\n";
    	$content .= "<div class=\"cli highh\" style=\"width:238px;\" >$sub_crew_name</div>\n";
    	$content .= "<div class=\"contact highh cntr\">$perc</div>\n";
    	$content .= "<div class=\"contact highh cntr\" style=\"width:182px;\" >$tcePerc</div>\n";
    	$content .= "<div class=\"contact highh txtr\" style=\"width:176px;\" >$subtot_to_date</div>\n";
    	$content .= "<div class=\"approv highh txtr \" style=\"width:159px;\" >$tce_est</div>\n";
	 	$content .= "<div class=\"links highh \" style=\"width:225px;\">$action</div>\n";

		$content .= "</div>\n";  //line div
		$content .= "<div style=\"clear:both;\" > </div>\n";
		$lNo += 1;

	}




	echo $content;

?>
