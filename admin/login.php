<?php   // login.php
	require_once("include/boot.php");
	if (!isset($_SESSION['user_unknown'])) {
		$_SESSION['user_unknown'] = '';
		$_SESSION['pwd_unknown'] = '';
		$_SESSION['pass_fail'] = 0;
		$_SESSION['locked'] = 'false';
	}    
if (isset($_POST['user'])){
	$_SESSION['verified'] = 'false';
	$user = strtoupper(trim($_POST['user']));
	$user=preg_replace('/\ .*$/',"",$user);  // first line of defence
	$pwd = trim($_POST['pwd']);
	if (isset($_SESSION['pass_fail']) && intval($_SESSION['pass_fail']) > 2 ) {  // fourth
			$sessID = session_id();
			$sql = "UPDATE employee set locked = true,session_id = '$sessID'  where upper(username) = '$user'";
			if (! $res=$conn->Execute($sql)) {
				$_SESSION['pass_fail'] += 1;
 				$_SESSION['user_unknown'] = 'true';	
				header("Location: login.php");
				exit;
			}
	}

	$sql = "execute log_stmt('$user')";  // 2nd  line

	try {
		$data = $conn->getRow($sql);
	}
	catch (Exception $e) {        // third  line
		if (isset($_SESSION['pass_fail'])) {
         $_SESSION['pass_fail'] += 1;
      }
      else {
         $_SESSION['pass_fail'] = 1;
      }
 		$_SESSION['user_unknown'] = 'true';	
		header("Location: login.php");
		exit;
	}

	if (count($data) < 1) {
		//unset($_POST);	
		if (isset($_SESSION['pass_fail'])) {
         $_SESSION['pass_fail'] += 1;
      }
      else {
         $_SESSION['pass_fail'] = 1;
      }
 		$_SESSION['user_unknown'] = 'true';	
		header("Location: login.php");
		exit;
	}
	else if ($data['locked'] == "t" ) {
		$_SESSION['locked'] = 'true';
		header("Location: login.php");
		exit;
	}
	else if (md5($pwd) !== $data['password']) {
		//unset($_POST);	
		if (isset($_SESSION['pass_fail'])) {
         $_SESSION['pass_fail'] += 1;
      }
      else {
         $_SESSION['pass_fail'] = 1;
      }
		$_SESSION['pwd_unknown'] = 'true';
		header("Location: login.php");
		exit;
	}	
	else {
		 $empID  = $data['employee_id'];
       updateSess($empID);
		// verified continue
		$_SESSION['verified'] = 'true';
      $_SESSION['employee_id']  = $data['employee_id'];
      $_SESSION['firstname']  = ucfirst($data['firstname']);
      $_SESSION['lastname']  = ucfirst($data['lastname']);
		$_SESSION['profile_id']  = $data['profile_id'];
		$_SESSION['company_id']  = $data['company_id'];
		if ($data['profile_id'] < 1 ) {
         return false;
      }


		if (empty($data['last_login'])) {  // all null last logins need password reset
         header("Location: change_password.php");
         exit;
      }



		if (isset($_SESSION['last_request'])) {
         $request = $_SESSION['last_request'];
         unset($_SESSION['last_request']);
         header("Location: $request");
         exit;
      }
      else {
         header("Location: index.php");
         exit;
      }
	}
} 
else {
	$pg = new Login;
}
	function updateSess($empID) {
      $conn = $GLOBALS['conn'];
      $sessID = session_id();
      $sql = "UPDATE employee set session_id = '$sessID' where employee_id = $empID";
      if (!$res=$conn->Execute($sql)) {
         die($conn->ErrorMsg());
      }
   }

?>
