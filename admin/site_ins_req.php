<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	if (isset($_REQUEST['sitereq_id'])) {
		$siteInsReqID = $_REQUEST['sitereq_id'];
	}
	else {
		$siteInsReqID = NULL;
	}
	//$coo_id = isset($_REQUEST['cooID']) ? $_REQUEST['cooID'] : 0;

	if (isset($_REQUEST['search'])) {
      $search = new FieldSearch();
      $search->cooID =!empty($_REQUEST['coo_id'])   ? $_REQUEST['coo_id'] : NULL;
      $search->requestID =!empty($_REQUEST['siteInsReqID'])  ? $_REQUEST['siteInsReqID'] : NULL;
      $search->siteInsNum =!empty($_REQUEST['ins_no'])  ? $_REQUEST['ins_no'] : NULL;
      $search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']) : NULL;
      $search->areaName = !empty($_REQUEST['area'])  ? $_REQUEST['area'] : NULL;
      $wellID =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->wellID = ! is_null($wellID) && is_array($wellID) ? $wellID[0] : $wellID;
      $search->crewID =isset($_REQUEST['crew']) ? intval($_REQUEST['crew']) : NULL;
      $search->contractorID =!empty($_REQUEST['contractor']) ? intval($_REQUEST['contractor']) : NULL;
      $search->dateType = !empty($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->creDate = !empty($_REQUEST['sdate'])    ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate =!empty($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

   }
   else {
      $search = NULL;
   }
   $redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

	

	$pg = new SiteInsRequired($action,$siteInsReqID,$search,$redo);
?>
