<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}

   $plantID =  isset($_REQUEST['plant_id']) ? $_REQUEST['plant_id'] :NULL;
	$con_id = isset($_REQUEST['con_id']) && intval($_REQUEST['con_id']) > 0  ? $_REQUEST['con_id'] : NULL;
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;


	 if (isset($_REQUEST['search']) ) {
      $search = new FieldSearch();
      $search->contractorID =isset($_REQUEST['con_id']) ? intval($_REQUEST['con_id']): NULL;
      $search->ID =  !empty($_REQUEST['plantTypeID']) ? $_REQUEST['plantTypeID'] :NULL;
      $search->plantID =  !empty($_REQUEST['plant_id']) ? $_REQUEST['plant_id'] :NULL;
		$search->pTypeName = !empty($_REQUEST['plant_type']) ? $_REQUEST['plant_type'] :NULL;
		$search->status = !empty($_REQUEST['approved']) ? $_REQUEST['approved'] :0;

   }
	else if (isset($_REQUEST['setcon'])) {
      $search = NULL;
		$con_id = $_REQUEST['contractor'];
	}
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }


	$pg = new ContractorPlant($action,$plantID,$con_id,$search,$redo);
?>
