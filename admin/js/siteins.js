function fieldDiv(obj,dvid,site_id,conid,typ) {
    var line = '#linediv_'+ dvid;
    var dv = '#site_div_'+ dvid;
    var cs = $(obj).val();
   if (cs == 'closed' ) {
     $(obj).val('open');
     $.post("field_ests_div.php",'req_id=' + site_id + '&con_id=' + conid + '&lineno='+dvid +'&type='+typ,
     function(data) {
         $(dv).css("display","none");
         $(dv).html(data);
         $(dv).fadeIn(2000);
     }
    ,"html");
     $(line).css("font-weight","bold");

   }
   else {
      $(obj).val('closed');
      $(dv).css("display","none");
      $(line).css("font-weight","normal");

   }
}
function approveAll(obj,but,id,conid,typ) {
	  var host = window.location;
	  $.post("upd_field.php",'field_id=-1' + '&conid='+conid + '&ans=yes' +'&requestid=' + id +'&type='+typ,
         function(data) {
            if (data != 'OK') {
					alert('something went wrong?' + data);
					return false;
				}
				$(but).removeClass('oran').addClass('grn');
				obj.removeClass('oran').addClass('grn');
				loc = host + '&redo=redo&con_id=' + conid;
				window.location.href = loc;
         }
         ,"text")
}
function updateField(obj,id,conid,typ) {
   var dv = $('#field_'+id);
   var h = obj.html();
    $.post("upd_field.php",'field_id='+id + '&conid='+conid + '&ans=yes' +'&requestid=-1' + '&type='+typ,
         function(data) {
            if (data != 'OK') {
               alert('something went wrong?' + data);
               return false;
            }
            dv.css("background-color","#cdefbd");
            obj.html('Approved');
            obj.removeClass('oran');


         }
         ,"text");
}
