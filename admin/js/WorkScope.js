function hide_workscope() {
	var res = $("input[name=type_split]:checked").val(); 
	if (res == 'crew' ) {
		$('#work_scope_div').fadeOut(500);
		$('#show_sub').fadeIn(1000);
		$('#subscope').prop('disabled',false);
	}
	else {
		$('#work_scope_div').fadeIn(1000);
		$('#show_sub').fadeOut(500);
		$('#subscope').prop('disabled',true);
	}
}
function showSplit() {
	var con_id = $('#contractor').val();
	if (con_id == 0 ) {
		$('#consplit').fadeIn(1000);
		$('#conradio').fadeIn(1000);
		$('#work_scope_div').fadeIn(1000);
		$('#show_sub').fadeOut(500);
		$('#crewrad').prop('checked', false);
		$('#conrad').prop('checked', true);
		$('#subscope').prop('disabled',true);

	}
	else {
		$('#work_scope_div').fadeOut(500);
		$('#show_sub').fadeIn(1000);
		$('#conradio').fadeOut(500);
		$('#consplit').fadeOut(500);
		$('#crewrad').prop('checked', true);
		$('#conrad').prop('checked', false);
		$('#subscope').prop('disabled',false);

	}



}
