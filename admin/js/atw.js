function addContact() {
	var dt = plusmonths(2);  // add two months to todays  date
   var condiv = $('#contactdiv');
	var cl = parseInt($("input#linecon:last").val());
   if (!isNaN(cl)) {
      cl +=1;
   }
   else {
      cl = 1;
   }

	var divtxt = '<div id="conline_'+cl +'" >\n';
   divtxt += '<div style="clear:both;"></div><input type="hidden" value="0" name="contact_id[]" id="contactID_'+cl + '" /><input type="hidden" value="'+cl +'" id="linecon"  />\n';
   divtxt += '<input type="hidden"  name="seltype[]" id="seltype_'+cl +'"  value="EXT" />\n';
   divtxt += '<div class="div29" ><label  >First Name<input type="text" name="con_first[]" id="con_first_'+cl +'" value="" class="required" /></label></div>\n';
   divtxt += '<div class="div29 marg1" ><label >Last Name<input type="text" name="con_last[]" id="con_last_'+cl +'" value="" class="required" /></label></div>\n';
   divtxt += '<div class="div29 marg1" ><label  >Email<input type="text" name="con_email[]" id="con_email_'+cl +'" value="" class="required" /></label></div>\n';
   divtxt += '<div class="div10 marg1" >';
	divtxt += '<div class="div70" ><label for="emailit_' +cl +'"  >Email Contact</label><input type="checkbox" name="emailit_'+cl +'" id="emailit_'+cl +'" checked   /></div>\n'; 
	divtxt += '<div class="div30" ><button class="linkbutton delatw"  onclick="deldiv(' +cl +',0,0);return false;">Delete</button></div>\n';
	divtxt += '</div>\n';
	divtxt += "<script>$('#con_first_"+ cl + "'" + ").autocomplete({ width: 400, delimiter: /(,|;)\s*/, serviceUrl:'autocomplete.php?type=companyone', minChars:1, onSelect: function(value, data){ insertDetails("+cl +",data);} });";
	divtxt += "</script>\n";
   condiv.append(divtxt);
}
function addDocument() {
	var dv = $('#uploads');
	var cl = parseInt($("input#linedoc:last").val());
   if (!isNaN(cl)) {
      cl +=1;
   }
   else {
      cl = 1;
   }
	var txt = '<div id="document_div_'+cl + '" class="docdiv"  >\n';
       txt += '<input type="hidden" value="6" name="atwtypeid[]" id="atwtypeid_'+cl +'" /><input type="hidden" value="'+cl +'" id="linedoc" name="linedoc[]"  /><input type="hidden" value="0" name="atw_doc_id[]"  />\n';
       txt += '<div style="clear:both;" ></div>\n';
       txt += '<div class="addr bdb nopad higher" ><input type="file" class="higher" name="document[]" /></div>\n';
       txt += '<div class="addr bdb nopad higher" ><input type="text"  class="higher"    name="doctype[]" id="doctype_'+cl +'" /></div>\n';
       txt += '<div class="well wd line2 bdb nopad higher"  ><input type="hidden" name="prevFile[]" value="" /></div>\n';
       txt += '<div class="addr bdb bdr higher redd" style="width:300px;" id="divstatus_'+cl + '"   >';
		 txt += '<div class="div25 margt5" ><span>Draft</span><input type="radio" name="status_' +cl +'"  id="status_'+cl +'" value="draft" checked  onclick="setStatus('+cl+');" /></div>';
       txt += '<div class="div55 margt5" ><span>Ready to Release</span><input type="radio" name="status_' +cl +'"  id="status_'+cl +'" value="ready"  onclick="setStatus('+cl + ');" /></div>';
       txt += '<div class="div20" ><button class="linkbutton delatw"  onclick="delDoc('+cl +', ' + "'#document_div_" +cl + "'"  +');return false;"  >Delete</button></div>\n';
		 txt += '</div></div>\n';
		 txt += '<div style="clear:both;" ></div>\n';
		 txt += "<script> $(function () { $('#doctype_"+cl +"').autocomplete({ width: 400, serviceUrl:'autocomplete.php?type=atwtype', minChars:1, onSelect: function(value, data){ $('#atwtypeid_"+cl + "').val(data);} }); }); </script>";
   dv.append(txt);
}
function deldiv(dvnum,cont_id,atwid) {
	if (!isNaN(cont_id) && parseInt(cont_id) != 0 ) {
		var dv = '#conline_'+dvnum;
		var seltype = $('#seltype_'+dvnum).val();
  		 $.post("del_contact.php",'cont_id='+ cont_id +'&atw_id=' + atwid +'&seltype='+seltype,
      	function(data) {
         	$(dv).html('');
      	}
      	,"html");
	}
}
