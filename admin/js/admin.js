$(function() {
$( ".show-option" ).tooltip({
show: {
effect: "slideDown",
delay: 250
},
hide: {
effect: "explode",
delay: 250
},
position: {
my: "left top",
at: "left bottom"
}
});
});
function isNumber(n) {
  return !isNaN(n) && isFinite(n);
}

function isArray( obj ) {
    return toString.call(obj) === "[object Array]";
}
function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
function plusmonths(offset) {
	 var dt = new Date();
    var day = pad(dt.getDate(),2);
	 var year =  dt.getFullYear();
	 var mth = dt.getMonth()+1;  // always add one
    if (parseInt(mth)  + offset > 12 )  {
		 mth = (mth + offset) - 12;
		 year += 1;
	 }	
	 else {
		mth +=offset;
	 }
	 var mth = pad(mth,2);
	 return  day +'-'+mth+'-'+year;
}
function addmonths(datestr,offset) {  // from  mobiscroller  date and  time  and  month offset and  return  string
	var dtArr = datestr.split("-");
   var day = parseInt(dtArr[0]);
	var yrArr = dtArr[2].split(" ");
	var year =  parseInt(yrArr[0]);
	var mth = parseInt(dtArr[1]);  // always add one
	if (parseInt(mth)  + offset > 12 )  {
      mth = (mth + offset) - 12;
      year += 1;
   }
   else {
     mth +=offset;
   }
	if ((mth == 4 || mth == 6 || mth == 9 || mth == 11 ) && day > 30 ) {
		day = 30;
	} 
	else if (mth == 2 && day > 28 ) {  // we can't do leap  years
		day = 28;
	}
   var mth = pad(mth,2);
   day = pad(day,2);
   return  day +'-'+mth+'-'+year; // + ' ' + yrArr[1];
}
function chkDupeWell() {
	 var arid= $('#areaID').val();
	 var wlname= $('#well_name').val();
	 if (wlname.length() < 1 ) {
		 return;
	 }
	 $.post("check_dupe_well.php",'area='+arid + '&well=' + wlname,
         function(data) {
            if (data != 'OK') {
					$('#well_name').val('');
					alert(data);
					$('#well_name').focus();
					return false;
				}

         }
         ,"text");
}
function getSubScopes(obj,dv,type) {
   var crewid = obj.val();
   $.post("sub_scope_select.php",'crewid=' + crewid + '&type=' + type,
     function(data) {
			$(dv).css("display","none");
         $(dv).html(data);
         $(dv).fadeIn(2000);
     }
    ,"html");
}
function gravel_approve(gwid,conid,but) {
	 $.post("upd_gravel.php",'gwid='+gwid + '&conid=' + conid,
         function(data) {
            if (data != 'OK') {
					alert('something went wrong?' + data);
					return false;
				}
				else {
					$(but).addClass("red");
					$(but).prop('value',"Locked");
				}  

         }
         ,"text");
}
function updateAll(nm,conid) {
	var arr = nm.split("_");
   var suf =arr[1];
	var res = $("[name="+nm + "]:checked").val(); 
	var coo = $('#calloff_'+suf).val();
	var dat = $('#date_'+suf).val();
	var dv = $('#linediv_'+suf);

	// update contrcator hour record
	 $.post("upd_hour.php",'type=all' + '&coo_id='+coo + '&conid=' + conid + '&date='+dat + '&ans=' + res,
         function(data) {
            if (data != 'OK') {
					alert('something went wrong?' + data);
					return false;
				}
				if (res == 'yes' ) {
					dv.css("background-color","#60854e"); 
				}
				else {
					dv.css("background-color","#ef3e55");
				}  

         }
         ,"text");
}  
function emailATW(atwid,obj) {
	var emp_id = $('#emp_id').val();
	$.post("email.php",'type=atw' + '&atw_id='+atwid +  '&emp_id=' + emp_id,
         function(data) {
            if ( isNaN(data) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
				else if (data == "-1" ) {
               alert('No emails to send');
               return false;
				}
            else { 
					$(obj).html('Emailed');
               $(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
            }
		 }
         ,"text");
}
function emailSup(obj) {
	var emp_id = $('#emp_id').val();
	var dat = $('#sdate').val();
	var conid = $('#contractorID').val();
	var conname = $('#con_name').val();
	var supid = $('#superintendent').val();
	 $.post("email.php", 'type=super' + '&hdate=' + dat +'&conid=' + conid  +'&con_name=' + conname + '&supid=' + supid + '&emp_id=' + emp_id,
         function(data) {
            if ( isNaN(data) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
            else { 
					$(obj).html('Superintendent Emailed');
					$(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
            }
			}
         ,"text");
}
function emailT2() {
// checks each  radio for  a no
   var host = window.location.hostname;
	var emp_id = $('#empID').val();
	var conid = $('#conID').val();
	var dat = $('#startdate').val();
	var loc =  'https://' + host + '/printSupTable2.php?action=email&con_id=' + conid + '&startdate=' + dat;
   window.location.href = loc;	
}
function emailTCE(tce,conid,obj) {
	var emp_id = $('#empID').val();
	$.post("email.php", 'type=tce' + '&tce_id='+tce + '&conid=' + conid + '&emp_id=' + emp_id,
         function(data) {
            if ( isNaN(data) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
            else { 
					$(obj).html('Emailed');
					$(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
            }
		 }
         ,"text");
}
function emailcoo(req_id,coo,conid,sups,obj) {
	var emp_id = $('#empID').val();
	$.post("email.php", 'type=coo' + '&coo_id='+coo + '&conid=' + conid + '&sups=' + sups + '&emp_id=' + emp_id + '&request_id=' + req_id,
         function(data) {
            if ( isNaN(data) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
            else { 
					$(obj).html('Emailed');
					$(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
            }
		 }
         ,"text");
}
function emailres(coo,dat,conid,obj) {
	var emp_id = $('#emp_id').val();
	$.post("email.php",'type=approve' +'&date=' + dat +  '&coo_id='+coo + '&conid=' + conid + '&emp_id=' + emp_id,
         function(data) {
            if ( isNaN(data) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
            else {  
					$(obj).html('Emailed');
					$(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
            }
		 }
         ,"text");
}
function emailComp(compid,conid,cooid,obj) {
	var emp_id = $('#emp_id').val();

	$.post("email.php",'type=comp' +'&conid='+conid + '&comp_id=' + compid + '&coo_id=' + cooid,
         function(data) {
            if ( isNaN(data) ) {
              alert('something went wrong?\n' +data);
              return false;
            }
            else {  
					$(obj).html('Emailed');
					$(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
           }
		 }
         ,"text"); 
}
function updateDocket(coo_id,conid,dv) {
   var host = window.location.hostname;
	$.post("unlock_docket.php",'conid='+conid + '&coo_id=' + coo_id ,
         function(data) {
            if ( data != "OK" ) {
              alert('something went wrong?\n' +data);
              return false;
            }
           else {  
					$(dv).html("99");
           }
		 }
         ,"text"); 
}
function approveFieldEst() {
   var url = document.URL;
	var conid = $('#con_id').val();
	var fieldid = $('#field_id').val();
	var empid = $('#emp_id').val();
	var est_type = $('#est_type').val();
	var site_ins_id = $('#site_ins_id').val();

	$.post("approve_field.php",'conid=' + conid  +'&empid='+empid + '&fieldid='+fieldid + '&est_type='+est_type +'&site_ins_id=' + site_ins_id,
         function(data) {
            if (data != 'OK') {
               alert(data);
               return false;
            }
			   window.location.href = url;	
         }
         ,"text");
}
function showCooLine(cooid) {
		var dv="#line_1";
      $.post("selects.php",'val=' + cooid +'&type=cooline' ,
      function(data) {
			 $(dv).css("display","none");
			 $('#super_div').css("display","none");
          $('#crew_1').val(data[0]);
          $('#sub_scopes_1').val(data[1]);
          $('#area_1').val(data[2]);
          $('#well_1').val(data[3]);
          $('#supervisor').val(data[8]);
          $(dv).fadeIn(2000);
			 $('#super_div').fadeIn(2000);

      }
      ,"json");

}
function owner_div(ltypeid) {
		var dv="#land_div";
      $.post("selects.php",'val=' + ltypeid +'&type=landowner' ,
      function(data) {
			 $(dv).css("display","none");
          $(dv).html(data);
          $(dv).fadeIn(2000);

      }
      ,"html");

}
function timedivshow(hour_id,dv,bt,dat,con_id) {
   var ht = $(bt).html();
   if (ht == "Uploads") {
      $.post("time_div.php",'hour_id=' + hour_id + '&date=' + dat + '&con_id=' + con_id,
      function(data) {
          $(dv).html(data);
      }
      ,"html");
 				 
      $(dv).fadeIn(1500);
      //$(dv).css("display","block");
      $(bt).html('Close');
   }
   else {
      $(dv).css("display","none");
      $(bt).html('Uploads');
   }
}
function newcalloff(dv,html) {
		var sitediv = $(dv);
		var txt = '<label  class="label" >Contractor</label>\n';
			txt += html; 
		   txt += '<label  class="label wide" >CallOff Order Num</label><input type="text" name="callno[]"  class="input sml"  />\n';
         txt += '<div style="clear:both;height:5px;" ></div>\n';
			sitediv.append(txt);
}
function newinstr(dv,html) {
		var sitediv = $(dv);
		var txt = '<label  class="label" >Contractor</label>\n';
			txt += html; 
		   txt += '<label  class="label wide" >Instruction Num</label><input type="text" name="insno[]"  class="input sml"  />\n';
         txt += '<label  class="label site" >Site Instructions</label><textarea  name="instruction[]"  class="txt sml"></textarea>\n';
         txt += '<div style="clear:both;height:5px;" ></div>\n';
			sitediv.append(txt);
}
function conSelect(dv) {
      	$.post("selects.php",'val=0&type=contractor',
      	function(data) {
         	newinstr(dv,data);
      	}
      	,"html");
}
function callSelect(dv) {
      	$.post("selects.php",'val=0&type=calloff_con',
      	function(data) {
         	newcalloff(dv,data);
      	}
      	,"html");
}
function wellDiv(wellname,dv) {
	var html = '<label>Well/s<input type="text" name="well" id="well" class="input mobile" value="' + wellname + '" /></label>';
   dv.html(html);

}
function getCOO(obj) {
	var conid = $('#contractor').val();
	var well = $('#well_1').val();
	var crew = $('#crew').val();
	var area = $('#areaID').val();
	if (!isNaN(conid) && !isNaN(well) && !isNaN(crew) &&  !isNaN(area)) {
      	$.post("getCoo.php",'conid='+ conid +'&area=' + area +'&crew=' + crew + '&well=' + well,
      	function(data) {
         	$('#coo').val(data);
      	}
      	,"text");
	}
}
function locSelect(val,conid,dv) {
         if (conid > 0) {
      		$.post("selects.php",'val='+ val +'&type=gwl_loc' +'&contractor_id=' + conid,
      		function(data) {
					$(dv).css("display","none");
         		$(dv).html(data);
         		$(dv).fadeIn(2000);
      		}
      		,"html");
			}
			else {
         		$(dv).html(' &nbsp;');
			}
}
function ownerSelect(val) {
			var dv = $('#landdiv');
      	$.post("selects.php",'val='+ val +'&type=landowner' +'&contractor_id=0',
      	function(data) {
         	dv.html(data);
      	}
      	,"html");
		 
}
function childSelect(val,type,dv,con) {
			var conid = $(con).val();
      	$.post("selects.php",'val='+ val +'&type=' + type +'&contractor_id=' + conid,
      	function(data) {
				dv.css("display","none");
         	dv.html(data);
				dv.fadeIn(2000); 
      	}
      	,"html");
		 
}
function rateselect(dv,rate,srate) {
	var ratediv = $(dv);
	var txt = '<select class="sel" name="rate" id="rate" onchange="calctotal();" >\n';
	txt += '<option value="' + rate + '" > ' +  'Normal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp ' + rate + '</option>\n';
	txt += '<option value="' + srate + '" > ' + 'Stand Down ' + srate + '</option>\n';
	txt += '</select>\n';

	ratediv.html(txt);
	calctotal();

}
function checkInputs() {
	if ($('#operatorID').val() == 0 ) {
		alert('Please autocomplete an Operator');
		$('#operator').focus();
		return false;
	}
	if ($('#plantID').val() == 0 ) {
		alert('Please autocomplete a Machine');
		$('#machine').focus();
		return false;
	}  
	if ($('#areaID').val() == 0 ) {
		alert('Please autocomplete an Area');
		$('#area').focus();
		return false;
	}
	return true;
}
function calctotal() {
	var dkt = $('#dkthours').val();
	var rte = $('#rate').val();
	if ((!isNaN(dkt)) && (!isNaN(rte))) {
		var tot = dkt * rte;
	}
	$('#total').val(tot);

}
function fillinputs(data) {
	$('#machine').val(data[1]);
	$('#unit').val(data[2]);
	$('#rate').val(data[3]);
	calctotal();
}
function displayDiv(el) {
   var opt = $(el).val();
   var ediv = $('#enddiv');
   var bdiv = $('#blankdiv');
   if (opt == "between" ) {
      //ediv.css("display","block");
      bdiv.css("display","none");
      ediv.fadeIn(2000);
   }
   else {
      ediv.css("display","none");
     // bdiv.css("display","block");
      bdiv.fadeIn(2000);
   }

}
function addressFill(id) {
   $.post("autocomplete.php","type=" + "destaddress" + "&query="+  id ,
      function(data) {
			$('#dest_name').val(data['destination_name']);
			$('#addr1').val(data['address_line_1']);
			$('#addr2').val(data['address_line_2']);
			$('#suburb').val(data['suburb']);
			$('#postcode').val(data['postcode']);
			$('#phone').val(data['phone']);
      }
      ,"json");  
}

function productSelect(arr) {
	var rt = $('#rate_pickup');
	rt.val(arr[1]);
 dv = $('#product');	
   $.post("products.php",'supplier_id='+ arr[0],
      function(data) {
         $(dv).html(data);
      }
      ,"html");
}
function confirmDelete() {
  if ( ! confirm('Do you really want to disable this record?')) {
		return false;
  }
}
function confirmDeleteT3(t3id) {
  if ( ! confirm('Do you really want to disable this record?')) {
		return false;
  }
  else {
		 $.post("del_t3.php",'t3id='+ t3id);
		return true;
  }
		
}
function chg_rec_interval() {
		 var e = $('#rec_interval');
	if($('#recurring').is(':checked')) {
		e.removeAttr('disabled');
		e.removeClass("sel disabled").addClass("sel");
	}
	else {
		e.prop('disabled','disabled');
		e.removeClass("sel").addClass("sel disabled");
	}
}
function uncheck_all_day() {
		var e = $('#all_day');
		e.prop("checked",false)
}
function clear_times() {
	if($('#all_day').is(':checked')) {
		$('#start_time').val("");
		$('#end_time').val("");
   }
}
function delDoc(atwdocid,dv) {
		if (!isNaN(atwdocid) && parseInt(atwdocid) != 0 ) {
		  $.post("del_atwdoc.php",'atw_doc_id='+atwdocid,
      	function() {
			$(dv).html('');
      	});
		}  
		else {
			$(dv).html('');
			return true;
		}
}
function setStatus(num) {
//   HOW to read a dynamic  radio  group !!!
	var dv = $('#divstatus_'+num);
	var rad = 'status_'+num;
	
	var radval = $('input[name='+rad +']:checked').val();
	if (radval == 'draft' ) {
               dv.css("background-color","red");
            }
            else {
               dv.css("background-color","orange");
            }
}
function delGrav(locid,dv) {
		if (!isNaN(locid) && parseInt(locid) != 0 ) {
		  $.post("del_location.php",'loc_id='+locid,
      	function() {
			$(dv).html('');
      	});
		}  
		else {
			$(dv).html('');
			return true;
		}
}
function addPit() {
	var dv = $('#pit_div');
	var cl = parseInt($("input#linedoc:last").val());
   if (!isNaN(cl)) {
      cl +=1;
   }
   else {
      cl = 1;
   }
	var txt = '<div id="loc_div_'+cl +'" >\n';
	txt += '<input type="hidden" id="linedoc" value="'+cl +' " />\n';
	txt += '<input type="hidden" name="loc_id[]" value="0" />\n';
	txt += '<div class="vehicle nopad" >\n';
	txt += '<select   style="width:100%;"  name="loc_type[]"    >\n';
	txt += '<option value="1" SELECTED >Gravel</option>\n';
	txt += '<option value="2" >Water</option>\n';
	txt += '<select></div>\n';
	txt += '<div class="location nopad" ><input type="text" class="loc" name="location[]" /></div>\n';
	txt += '<div class="usr nopad" ><input type="text" class="pushed up cntr" name="pushed_up[]" id="pushed_up_'+cl +'" value="" placeholder="0" /></div>\n';
	txt += '<div class="usr nopad" ><input type="text" class="pushed non cntr" name="non_pushed_up[]" id="non_push_'+cl +'" value="" placeholder="0" /></div>\n';
	txt += '<div class="usr nopad" ><input type="text" class="pushed water cntr" name="water[]"  id="water_' +cl +'" placeholder="0" /></div>\n';
	txt += '<div class="links bdb"   style="width:80px;" >\n';
	txt += '<button class="linkbutton bckred margt5"  onclick="delGrav(0, ' +"'#loc_div_" + cl +"' " + ');return false;"   >Delete</button>\n';
	txt += '<div>\n';

   dv.append(txt);
}
function changeLine(cl,type) {
	var push = '#pushed_up_'+cl;
	var nopush = '#non_push_'+cl;
	var water = '#water_'+cl;

if (type == 1 ) {
      $(nopush).removeAttr('readonly');
      $(nopush).attr('placeholder',"0");
      $(nopush).val(0);
      $(push).removeAttr('readonly');
      $(push).attr('placeholder',"0");
      $(push).val(0);
      $(water).attr('readonly','readonly');
      $(water).attr('placeholder','N/A');
      $(water).val('N/A');
   }
   else {
      $(nopush).attr('readonly','readonly');
      $(nopush).attr('placeholder','N/A');
      $(nopush).val('N/A');
      $(push).attr('readonly','readonly');
      $(push).attr('placeholder','N/A');
      $(push).val('N/A');
      $(water).removeAttr('readonly');
      $(water).attr('placeholder',"0");
      $(water).val(0);
   }
}
function insertDetails(cl,data) {
	var con = '#contactID_'+cl;
	var fst = '#con_first_'+cl;
	var lst = '#con_last_'+cl;
	var em = '#con_email_'+cl;
	var slt = '#seltype_'+cl;
	$(con).val(data['conid']);
	$(fst).val(data['firstname']);
	$(lst).val(data['lastname']);
	$(em).val(data['email']);
	$(slt).val(data['seltype']);
	 //$('#contactID_" + cl + "'" + ").val(data['conid']);
}
		
function testEmail(element) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test($(element).val())
}
function putSig() {
 	var sig = $('#signature').val();
	//var qgcsig = $('#qgcSig');
	/* $('#qgcSig').val(sig);  */
	$('#qgcsig').attr('src',sig);
	$('#appr').html('Approved');
	$('#approve').css("display","none");
	$('#qgc_approved').val('APPROVED');

}
