function showDiv() {
	if (!checkRequired()) {
		return false;
	}
	var area = $('#areaID').val();
	var well = $('#well_1').val();
	var con_id = $('#contractor').val();
	var condiv = '#con_div';
 $.post("get_contractors.php", 'area='+ area + '&well=' +well + '&con_id='+con_id,
   function(data) {
      $(condiv).css("display","none");
      $(condiv).html(data);
     // $(condiv).fadeIn(2500);
		$(condiv).show(500,'easeInQuad');

   }
   ,"html");

}
function checkRequired() {
	var isFormValid = true;
   $("#zipreport input:text.required").each(function(){ // Note the :text
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    $("#zipreport .required").each(function(){
        if ($.trim($(this).val()).length == 0){
            $(this).addClass("highlight");
            isFormValid = false;
        } else {
            $(this).removeClass("highlight");
        }
    });
    if (!isFormValid) alert("Please fill in all the required fields (highlighted in red)");
    return isFormValid;
}
function selectAll(obj) {
	 if (!$(obj).is(':checked')) {
		 $('.conclick').each(function(i, obj) {
          $(this).prop('checked',false);
       });

    }
	 else {
		 $('.conclick').each(function(i, obj) {
          $(this).prop('checked',true);
       });
	}

}
