function approve_plant(plantid,conid,chk) {
	if (chk.attr('checked')) {
		var approve='false';
	}
	else {
		var approve='true';
	}
   var host = window.location.hostname;
   $.post("approve_plant.php", 'plant_id=' + plantid + '&conid='+conid +'&approve=' +approve,
         function(data) {
            if ( (data != 'OK' ) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
            else {  
               var loc = 'https://' + host +'/admin/contractor_plant.php?action=list&con_id=' +conid + '&redo=redo';
               window.location.href = loc;
            }
       }
         ,"text");
}

