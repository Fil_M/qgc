function menuify(menu) {
    function createMenu(menu) {
        if (typeof menu === "string") {
            menu = $(menu);
        }
        menu.addClass("menu-parent");
        menu.children("li").each(function(i, child) {
            child = $(child);

            // iPad touch fix
            var ua = navigator.userAgent,
            event = (ua.match(/iPad/i)) ? "touchstart" : "click";

            $(document).bind(event, function(e) {
                if (child.has(e.target).length === 0)
                {
                    child.children("ul").hide();
                }
            });

            child.children("ul").toggle();
            child.addClass("menu-option")
            if (child.children("ul").length > 0) {
                child.children("ul").each(function(j, subchild) {
                    createMenu($(subchild));
                })
            }
        });
        menu.children("li").hover(function() {
            self = $(this);
            self.children("ul").show();
        }, function() {
            self = $(this);
            self.children("ul").hide();
        })
        menu.children("li").click(function(e) {
            e.stopPropagation()
            self = $(this);
            self.children("ul").toggle()
        });
    }
    createMenu(menu)
}