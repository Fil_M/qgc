function unlockComp(compid,conid,coo_id) {
   var loc = window.location.href;
   loc += '&redo=redo&con_id='+conid;
   var reply = confirm("WARNING!! This will Delete this Completion Certificate from the System. It will reset this CO.O.s Percentage Completion to 99%  AND it MUST reset ALL entries for this Completion to a TO BE APPROVED status. All Invoice status from the contractor will be lost.  Do you wish to continue?" );
   if ( reply == true ) {
      $.post("unlock_comp.php",'conid='+conid + '&comp_id=' + compid +'&coo_id='+coo_id,
         function(data) {
            if ( data != "OK" ) {
              alert('something went wrong?\n' +data);
              return false;
            }
           else {
               window.location.href = loc;
           }
       }
         ,"text");
   }
   else {
      return false;
   }

}
