/*
    Plugin:       Calendar-Scheduler
    Requirements: jQuery, jQuery UI, date.js
    Description:  Library to schedule jobs and employees to a calendar system
    Publisher:    Tristan Matthias
    Company:      National Telephone & Data
    
    last updated: 12 Dec 2012
*/

//------------------------------------------------------------------------------- Global functions
//------------------------------------------------------------------------------- 
function run(object, method) {
    // Function used to handle all custom jQuery objects for initilization and running methods
    // Validation of parameters
    
    if (typeof object != "object") {
        $.error("'object' is not of type object");
    }
    // Convert arguments to usable Array
    if (object[method]) { // Run the method
        if (method[0] === "_") {
            $.error("Method '" + method + "' is a reserved function of jQuery.object");
        }
        return object[method].apply(this, arguments[2]);
    } else if ((typeof method === "object") || (typeof method === "undefined")) { // Construct the object
        return object.__init__.call(this, Array.prototype.slice.call(arguments, 1, 2)[0])
    } else { // Method is not defined, nor is it a construction
        $.error("Method '" + method + "' is not a function of jQuery.object");
    }
}
function data_get(url, complete, data) {
    var self = this;
    if (typeof data === "undefined") {
        data = {}
    }
    $.ajax({
        url: url
      , type: "POST"
      , dataType: "json"
      , data: data
    }).done(function(response){
        complete.call(self, response);
    })
}
function data_post(url, complete, data) {
    var self = this;
    if (typeof data === "undefined") {
        data = {}
    }
    $.ajax({
        url: url
      , type: "POST"
      , data: data
    }).done(function(response){
        if (complete) {
            complete.call(self, response);
        }
    })
}
function convertDateString(string) {
    if (typeof string === "string") {
        return string.replace(/-/g, '/')
    } else if (typeof string === "object") {
        return string;
    }
}
Date.prototype.difference = function(otherDate) {
    return this.getTime() - otherDate.getTime();
}
Date.prototype.mon = function() {
    var self = this;
    if (self.getDayName() === "Sunday") {
        return self.add(-6).days()
    } else {
        return self.mondayThisWeek();
    }
}
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};
//------------------------------------------------------------------------------- Objects
//------------------------------------------------------------------------------- 
var Scheduler = {
    __init__: function(options) {
        var self = this;
        self._headerHeight = 48;
        self._sideBarWidth = 220;
        self._viewWidth = 140;
        self._animationSpeed = 400;
        self._state = "";
        self._today = new Date();

        self._weeks = {};
        self._days = {};
        self._eventsAssigned = [];
        self._eventsUnassigned = [];
        self._employees = [];
        return self.each(function() {
            // Validating events-unassigned)
            if (options.eventsUnassigned) {
                if (Object.keys(options.eventsUnassigned).length != 2) {
                    throw new Error("Scheduler: eventsUnassigned takes exactly 2 elements");
                } else if (typeof options.eventsUnassigned.url === "undefined") {
                    throw new Error("Scheduler: eventsUnassigned.url is not defined");
                } else if (typeof options.eventsUnassigned.key === "undefined") {
                    throw new Error("Scheduler: eventsUnassigned.key is not defined");
                } else {
                    self.data_eventsUnassigned = options.eventsUnassigned;
                }
            }

            // Validating events-assigned
            if (options.eventsAssigned) {
                if (Object.keys(options.eventsAssigned).length != 2) {
                    throw new Error("Scheduler: eventsAssigned takes exactly 2 elements");
                } else if (typeof options.eventsAssigned.url === "undefined") {
                    throw new Error("Scheduler: eventsAssigned.url is not defined");
                } else if (typeof options.eventsAssigned.key === "undefined") {
                    throw new Error("Scheduler: eventsAssigned.key is not defined");
                } else {
                    self.data_eventsAssigned = options.eventsAssigned;
                }
            }

            // Validating employees
            if (options.employees) {
                if (Object.keys(options.employees).length != 2) {
                    throw new Error("Scheduler: employees takes exactly 2 elements");
                } else if (typeof options.employees.url === "undefined") {
                    throw new Error("Scheduler: employees.url is not defined");
                } else if (typeof options.employees.key === "undefined") {
                    throw new Error("Scheduler: employees.key is not defined");
                } else {
                    self.data_employees = options.employees;
                }
            }

            // Validating yData (row data to be shown on the side)
            if (options.yData) {
                if (Object.keys(options.yData).length != 2) {
                    throw new error("Scheduler: yData takes exactly 2 elements");
                } else if (typeof options.yData.url === "undefined") {
                    throw new error("Scheduler: yData.url is not defined");
                } else if (typeof options.yData.key === "undefined") {
                    throw new error("Scheduler: yData.key is not defined");
                } else {
                    self.yData = options.yData;
                }
            }


            
            // HTML
            self.html("") // Reset anytihng that is inside the div
                .addClass("cal")
                .css({
                    position: "relative"
                })
                .hide()
                .fadeIn(self._animationSpeed);
            
            self._html_header = $("<div class='cal-header ui-widget-header'></div>")
                .height(self._headerHeight - 2)
                .width(self.width() -2)
                .appendTo(self)
                // Text
                self._html_headerText_left = $("<div id='header-text-left'></div>")
                    .css({
                        width:self._sideBarWidth
                      , float:"left"
                      , "text-align": "center"
                      , "padding-top": "15px"
                    })
                    .appendTo(self._html_header);
                self._html_headerText_right = $("<div id='header-text-right'></div>")
                    .css({
                       float:"left"
                    })
                    .appendTo(self._html_header);
                self._html_navigation = [];
                    self._html_navigation["containerO"] = $("<div id='header-nav'></div>")
                    .css({
                        float: "left"
                      , height: "100%"
                      , width: self.width() - self._sideBarWidth - self._viewWidth - 10
                    })
                    .hide()
                    .appendTo(self._html_header);
                    self._html_navigation["containerI"] = $("<div />")
                        .css({
                            margin: "auto"
                          , width: 300
                        })
                        .appendTo(self._html_navigation["containerO"])
                    self._html_navigation["back"] = $("<button><</button>")
                        .css({
                            float: "left"
                          , marginTop: 10
                        })
                        .click(function() {
                            self.scheduler("navigate", -1);
                        })
                        .appendTo(self._html_navigation["containerI"]);
                    self._html_navigation["text"] = $("<p></p>")
                        .css({
                            float: "left"
                          , margin: "0 10"
                          , paddingTop: 15
                        })
                        .disableSelection()
                        .appendTo(self._html_navigation["containerI"]);
                    self._html_navigation["next"] = $("<button>></button>")
                        .css({
                            float: "left"
                          , marginTop: 10
                        })
                        .click(function() {
                            self.scheduler("navigate", 1);
                        })
                        .appendTo(self._html_navigation["containerI"]);

                self._html_headerViews = [];
                    self._html_headerViews["container"] = $("<div id='header-views'></div>")
                    .css({
                        float: "right"
                      , padding: "6 6 0 0"
                      , width: self._viewWidth
                      , textAlign: "right"
                    })
                    .appendTo(self._html_header);
                    self._html_headerViews["week"] = $("<button>Week</button>")
                        .button({disabled:true})
                        .click(function() {
                            self.scheduler("load_week");
                        })
                        .appendTo(self._html_headerViews["container"]);
                    self._html_headerViews["day"] = $("<button>Day</button>")
                        .button({disabled: true})
                        .click(function() {
                            self.scheduler("load_day");
                        })
                        .appendTo(self._html_headerViews["container"]);
            self._html_sideBar = $("<div class='cal-sidebar ui-widget ui-widget-content'></div>")
                .css({
                    width: self._sideBarWidth - 4
                  , height: self.height() - self._headerHeight - 2
                  , "border-right": 0
                })
                .appendTo(self);
            // self._html_sideBar_day = $("<div")
            self._html_main = $("<div class='cal-main ui-widget ui-widget-content'></div>")
                .css({
                    width: self.width() - self._sideBarWidth  -2/* border width */
                  , height: self.height() - self._headerHeight - 2
                  , "border-left": "0"
                })
                .appendTo(self)
                self._html_sideBarToggle = $("<div class= 'ui-widget ui-widget-content'><span class='ui-icon ui-icon ui-icon-circle-arrow-w'></span></div>")
                    .css({
                        float:"left"
                      , height: "100%"
                      , cursor: "pointer"
                      , "border-left": "0"
                      , "border-top": "0"
                      , "border-bottom": "0"
                    })
                    .toggle(
                        function() { 
                            self._html_headerText_left.fadeOut(self._animationSpeed);
                            self._html_sideBarToggle.html("<span class='ui-icon ui-icon ui-icon-circle-arrow-e'></span>");
                            self._html_sideBar.animate({ width: 'hide' }, 400);
                            self._html_main.animate({ width: self.width() - 2 }, 400, function() {
                            self._html_sideBarToggle.css({
                                "border-left": "1px solid"
                            })
                            });
                        },
                        function() {
                            self._html_headerText_left.fadeIn(self._animationSpeed);
                            self._html_sideBarToggle.css({
                                "border-left": 0
                            })
                            self._html_main.width(self._html_main.width() - 1)
                            self._html_sideBarToggle.html("<span class='ui-icon ui-icon ui-icon-circle-arrow-w'></span>");
                            self._html_sideBar.animate({ width: 'show' }, 400);
                            self._html_main.animate({ width: self.width() - self._sideBarWidth}, 400);
                        }
                    )
                    .appendTo(self._html_main)
            self._img_loading = $("<img class='cal-loading=image' src='/images/loading.gif' />")
                .css({
                    position: "absolute"
                  , left: self.width() /2 - 32
                  , top:  self.height() /2 - 32
                })
                .hide()
                .appendTo(self);

            self.scheduler("load_week", self._today)
        })
   }
  , navigate: function(direction) {
        var self = this;
        self._html_navigation["back"].button({disabled: true});
        self._html_navigation["next"].button({disabled: true});
        if (self._state === "week") {
            newWeek = self._week._monday.clone().add(7 * direction).days();
            self._week.hide();
            self.scheduler("load_week", newWeek)
        } else if (self._state === "day") {
            newDay = self._day._date.clone().add(direction).days();
            self._day.hide();
            self.scheduler("load_day", newDay)
        }
   }
  , update_navigationText: function() {
        var self = this;
        self._html_navigation["back"].button({disabled: false});
        self._html_navigation["next"].button({disabled: false});
        if (self._state === "week") {
            var mon = self._week._monday
            var sun = mon.clone().add(6).days()
            
            self._html_navigation["text"].html("Week: " + mon.getDate() + " " + mon.getMonthName().substr(0,3) + " - " + sun.getDate() + " " + sun.getMonthName().substr(0,3));
            self._html_navigation["containerO"].fadeIn(self._animationSpeed)
        } else if (self._state === "day") {
            var date = self._day._date;
            self._html_navigation["text"].html("Day: " + date.getDate() + " " + date.getMonthName().substr(0,3));
        }
  }
  , load_week: function(date) {
        var self = this;
        self.scheduler("resetView");
        self.scheduler("update_headerText", "left", "Unassigned Jobs");
        self._html_headerViews["week"].button("option", {disabled: true});
        self.scheduler("show_load");
        if (self._day) {
            self._html_headerViews["day"].button("option", {disabled: false});
        }
        return this.each(function() {
            // Date range from Monday of the week entered, to Sunday of the week entered
            var dateStart = new Date(date).clearTime().mon();
            var dateFinish = dateStart.clone().add(7).days();
            var _continue = true;
            self._state = "week";
            
            $.each(self._weeks, function(date, week) {
                if (parseInt(date) === dateStart.getTime()) {
                    self._week = week
                    _continue = false;
                }
            });
            data_get.call(
                self
              , self.data_eventsUnassigned.url
              , function(data) {
                    self._html_sideBar.html("")
                    var sidebar = $("<div />")
                        .css({
                            width: self._html_sideBar.width() - 3
                          , height: self._html_sideBar.height() - 19
                          , border: "1px solid transparent"
                        })
                        .droppable({
                            accept: '.draggable-event',
                            hoverClass: 'ui-state-active',
                            drop: function(event, ui) {
                                // Get job dragged
                                var job = $(ui["helper"]).data("controller");
                                job.event("decompose", $(this));
                            }
                        })
                        .appendTo(self._html_sideBar);
                    $.each(self._eventsUnassigned, function(i, employee){
                        self._eventsUnassigned.remove(0);
                    })
                    $.each(data, function(i, event){
                        var e = $("<div />").event({
                            scheduler: self
                          , data: event
                        });
                        self._eventsUnassigned.push(e);
                        sidebar.append(e.event("write", "week"))
                    });
                }
            )
            if (((typeof self._week === "undefined") || (typeof date != "undefined")) & _continue) {
                // Load assigned events and week
                data_get.call(
                    self
                  , self.data_eventsAssigned.url + "?start="+(dateStart.getTime()/1000)+"&end="+(dateFinish.getTime()/1000)
                  , function(data) {
                    // Convert json data to Javascript objects
                    
                    $.each(data, function(i, event){
                        var e = $("<div />").event({
                            scheduler: self
                          , data: event
                        });
                        self._eventsAssigned.push(e);
                    });
                    // Load the unassigned jobs
                    data_get.call(
                        self
                      , self.data_eventsUnassigned.url
                      , function(data) {
                        // Convert json data to Javascript objects
                        
                        self._week = $("<table></table>").appendTo(self._html_main).week({
                            scheduler: self
                          , date: dateStart
                        });
                        self._weeks[dateStart.getTime()] = self._week
                        self.scheduler("hide_load");
                        self.scheduler("update_navigationText");
                    })
                })
            } else {
                self.scheduler("hide_load");
                self.scheduler("week_show");
                self.scheduler("update_navigationText");
            }
        })
   }
  , week_hide: function() {
        var self = this;
        $.each(self._weeks, function(i, week){
            week.hide();
        })
   }
  , week_show: function() {
        var self = this;
        if (typeof self._week != "undefined") {
            self._week.show();
            self.scheduler("update_navigationText");
        }
   }
  , load_day: function(date) {
        var self = this;

        if ((date) || (typeof self._day != "undefined")) {
            self.scheduler("resetView");
            self.scheduler("update_headerText", "left", "Employees");
            self._html_headerViews["week"].button("option", {disabled: false});
            self._html_headerViews["day"].button("option", {disabled: true});
            self.scheduler("show_load");
        }
        

        return this.each(function() {
            self._state = "day";
            if ((date) || (typeof self._day === "undefined")) {
                data_get.call(
                    self
                  , self.data_employees.url + "?start=0"
                  , function(data) {
                        $.each(self._employees, function(i, employee){
                            self._employees.remove(0);
                            
                        })

                        $.each(data, function(i, employee){
                            self._employees.push($("<div />").employee({
                                scheduler: self
                              , data: employee
                            }));

                        });
                        self._day = $("<div />").day({
                            scheduler: self
                          , date: date
                        })
                        self._html_sideBar.html("")
                        self._day._sidebar.appendTo(self._html_sideBar);
                        self._day.day("write")
                            .appendTo(self._html_main)
                        self._days[date.getTime()] = self._day;
                        
                        self.scheduler("hide_load");
                        self.scheduler("update_navigationText");
                    }
                );
            } else if (typeof self._day != "undefined") {
                self.scheduler("hide_load");
                self.scheduler("day_show");
                self.scheduler("update_navigationText");
            }
        })
   }
  , day_hide: function() {
        var self = this;
        if (typeof self._day != "undefined") {
            self._day.hide();
            self._day._sidebar.hide();
        }
   }
  , day_show: function() {
        var self = this;
        if (typeof self._day != "undefined") {
            self._day.show();
            self._html_sideBar.html("")
            self._day._sidebar.show();
            self._day.day("writeEmployees");
            self._html_sideBar.append(self._day._sidebar);
            self.scheduler("update_navigationText");
        }
   }
  , resetView: function() {
        var self = this;
        self.scheduler("week_hide");
        self.scheduler("day_hide");
   }
  , update_headerText: function(side, header) {
        var self = this;
        if ((typeof side === "string") && (typeof header === "string")){
            if (side === "left") {
                self._html_headerText_left.html(header);
            } else if (side === "right") {
                self._html_headerText_left.html(header);
            } else {
                $.error("Scheduler: side of type '" + side + "' is not a valid type");
            }
        }
   }
  , show_load: function() {
        var self = this;
        self._html_main.hide();
        self._html_sideBar.hide();
        self._img_loading.show();
   }
  , hide_load: function() {
        var self = this;
        self._html_main.show();
        self._html_sideBar.show();
        self._img_loading.hide();
   }
};

var Week = {
    __init__: function(options) {
        var self = this;
        return self.each(function() {
            // Validation
            if (typeof options.scheduler === "object") {
                self._scheduler = options.scheduler;
            } else {
                throw new Error("Week: scheduler is not of type Scheduler");
            }
            if (options.date instanceof Date) {
                self._monday = options.date.clearTime().mon();
            } else {
                throw new Error("Week: date is not of type Date");
            }
            
            // HTML
            self.html("")
                .width(self._scheduler._html_main.width() - 30)
                .height(self._scheduler._html_main.height() - 12)
                .css({
                    position: "relative"
                  , left: "10px"
                  , top: "10px"
                  , "border-collapse": "collapse"
                })
                .addClass("week")
                .hide()
            self._html_header  = $("<tr class='cal-week-header'></tr>")
                .appendTo(self);
            // self._sidebar = $("<div />")
            //     .css({
            //         width: self._scheduler._html_sideBar.width() - 3
            //       , height: self._scheduler._html_sideBar.height() - 19
            //       , border: "1px solid transparent"
            //     })
            //     .droppable({
            //         accept: '.draggable-event',
            //         hoverClass: 'ui-state-active',
            //         drop: function(event, ui) {
            //             // Get job dragged
            //             var job = $(ui["helper"]).data("controller");
            //             job.event("decompose", $(this));
            //         }
            //     })
            //     .appendTo(self._scheduler._html_sideBar);

            // Public variables
            // Init of dates in the week (7 days starting from monday of the week)
            self._dates = [];
            var date = new Date(self._monday);

            for (var i = 0; i < 7; i++) {
                self._dates.push(date.clone().add(i).days());
            };

            data_get.call(self, self._scheduler.yData.url, function(data){
                self.fadeIn(self.scheduler._animationSpeed);
                self._yData = data;
                self.week("write");
            });
        })
    }
  , write: function() {
        var self = this;
        self._html_header.append("<th></th>"); // Used for the y_data column
        
        // Construction of table headers
        for (var i = 0; i < 7; i++) {
            th = $("<th id='week-"+ parseInt(self._dates[i].getTime()) +"'>" 
                + self._dates[i].getDayName().substr(0,3) + " - " 
                + self._dates[i].getDate() 
                + " " + self._dates[i].getMonthName().substr(0,3)
                + "</th>"
            )
                .css({
                    border: "1px solid transparent"
                  , "border-bottom": "0"
                  , cursor: "pointer"
                  , width: self.width() / 8
                })
                .click(function() {
                    var date = new Date(parseInt($(this).attr("id").slice(5)));
                    self._scheduler.scheduler("load_day", date);
                }).disableSelection()
                .hover(function() {
                    $(this).toggleClass("ui-state-active")
                        .css("border", "1px solid")
                    }
                  , function() {
                        $(this).toggleClass("ui-state-active")
                        .css({
                            border: "1px solid transparent"
                          , "border-bottom": "1px solid #cccccc"
                        })
                    }
                )
                .appendTo(self._html_header)
        };

        // Construction of contents
        $.each(self._yData, function(i, dataObj) {
            var yID = dataObj[self._scheduler.yData.key];
            
            var tr = $("<tr></tr>");
            var yColumn = $("<td></td>").appendTo(tr);


            var img = $(
                "<div style='background-position:"+ (-32 * (i % 6)) +"px 0px'>"+ dataObj.title+"</div>"
            ).css({
                // "background-image": "url('images/trucks.png')"
               "background-repeat": "no-repeat"
              , "width": "68px"
              , "height": "32px"
            }).click(function() {
                // TODO
            }).data({
                object: dataObj
            }).appendTo(yColumn);

            for (var j = 0; j < 7; j++) {
                var td = $("<td id='week-cell-" + yID + "-" + parseInt(self._dates[j].getTime()) + "' class='week-cell ui-widget-content droppable-eventAssigned'></td>")
                    .css({
                        "text-align": "center"
                    })
                    .droppable({
                        accept: '.draggable-event',
                        hoverClass: 'ui-state-active',
                        drop: function(event, ui) {
                            // Get job dragged
                            var job = $(ui["helper"]).data("controller");
                            job.event("move", $(this))
                        }
                    })
                    .appendTo(tr)
            }

            tr.appendTo(self);
        });
        
        // Populate the week
        self.week("populate");

        // Construction of the unassigned events
        // $.each(self._scheduler._eventsUnassigned, function(i, event) {
        //     self.week("write_unassignedEvent", event);
        // })
    }
  , populate: function() {
        var self = this;
        $.each(self._scheduler._eventsAssigned, function(i, event){
            var dateEvent = new Date(convertDateString(event.start));
            $.each($("th:not(:first-child)", self.html_header), function(i, th){
                var id         = parseInt($(th).attr("id").slice(5));
                var dateStart  = new Date(id);
                var dateFinish = new Date(dateStart);
                dateFinish.setDate(dateFinish.getDate() + 1);

                if ((dateEvent >= dateStart) & (dateEvent < dateFinish)) {

                    $.each(self._yData, function(j, y_dataObj){
                        if (event.vehicleID == y_dataObj.vehicleID) {
                            event.event("write", "week").appendTo(
                                $("#week-cell-" + event.vehicleID + "-" + (dateStart.getTime()))
                            );
                        }
                    });
                }
            });
        });
    }
  , write_unassignedEvent: function(event) {
        var self = this;
        self._sidebar.append(
            event.event("write", "week")
        )
    }
};

var Day = {
    __init__: function(options) {
        var self = this;
        self._events = [];
        return self.each(function() {
            // Validation
            if (typeof options.scheduler === "object") {
                self._scheduler = options.scheduler;
            } else {
                throw new Error("Day: week is not of type Week");
            }
            if (options.date instanceof Date) {
                self._date = options.date.clearTime();
            } else {
                throw new Error("Day: date is not of type Date");
            }

            // HMTL
            self.html("")
                .width(self._scheduler._html_main.width() - 30)
                .height(self._scheduler._html_main.height() - 12)
                .css({
                    position: "relative"
                  , left: "10px"
                  , top: "10px"
                  , overflow: "auto"
                })
                .addClass("day")
                .hide();

            self._sidebar = $("<div />")
                .css({
                    width: "100%"
                  , height: self._scheduler._html_sideBar.height() -2
                })
                // .appendTo(self._scheduler._html_sideBar);
            self.times = $("<div /><br />")
                .css({
                    "padding-left": 80
                })
                .appendTo(self);
            self.yColumn = $("<div />")
            .css({
                "width":"80px"
              , "float":"left"
            }).appendTo(self);

            self.timeSlots = $("<div class='day-timeslots' />")
            .css({
                "width": 870
              , "overflow": "auto"
              , position: "relative"
            }).appendTo(self)

            self.show();
        });
    }
  , write: function() {
        var self = this;
        return self.each(function() {
            var slotNumber = 24 * 4
            var slotWidth  = parseInt((self.timeSlots.width() -1)/(slotNumber))
            for(var i=0; i<24; i++) {
                self.times.append(
                    $("<div />", {
                        html: function() {
                            num = (i % 12);
                            if (num === 0) {
                                num = 12;
                            }
                            returning = "<b style='font-size: 15px'>" + num + "</b>";
                            if (i < 12) {
                                return returning + "am"
                            } else {
                                return returning + "pm"
                            }
                        }
                    })
                        .css({
                            float:"left"
                          , width: (slotWidth + 0.25) * 4
                          , fontSize : 10
                          , paddingTop: 10
                        })
                )
            }
            self.times.append($("<br />"))
            $.each(self._scheduler._week._yData, function(i, dataObj){
                var yID = dataObj[self._scheduler.data_eventsAssigned.key];
                var img = $(
                    "<div style='background-position:"+ (-32 * (i % 6)) +"px 0px'>"+ dataObj.title +"</div>"
                ).css({
                    //"background-image": "url('images/trucks.png')"
                   "background-repeat": "no-repeat"
                  , "width": "80px"
                  , "height": "64px"
                }).data({
                    object: dataObj
                }).appendTo(self.yColumn);

                
                var row = $("<div id='"+i+"' class='day-row'></div>")
                .css({
                   "height": "64px"
                  , "white-space": "nowrap"
                  , "position": "relative"
                }).appendTo(self.timeSlots);

                for (var j = 1; j <= slotNumber; j++) {
                    timeSlot = $("<span id='day-cell-"+yID+"-"+j+"' class='day-cell'></span>")
                    .css({
                        "width": slotWidth - 1
                      , "height": row.height() -1 
                      , "display": "inline-block"
                      , "border-right": function() {
                            if (j % 4 == 0) {
                                return "1px solid #ccc"
                            } else {
                                return "1px solid #ddd"
                            }
                        }
                      , "border-bottom": "1px solid #bbb"
                      , "border-left": function() {
                            if (j == 1) {
                                return "1px solid #ccc"
                            }
                        }
                    , "border-top": function() {
                            if (i == 0) {
                                return "1px solid #ddd"
                            }
                        }

                    })
                    timeSlot.appendTo(row);
                }

                $.each(self._scheduler._eventsAssigned, function(i, event) {
                    var name;
                    $.each(self._scheduler._employees, function(j, emp) {
                        if (event.employeeID === emp.empID) {
                            name = emp.name;
                        }
                    })
                    if (typeof name === "undefined") {
                        name = "Not assigned"
                    }
                    dateEventStart = new Date(convertDateString(event.start));
                    dateEventEnd   = new Date(convertDateString(event.end));
                    update = function() {
                        var event = $(this).data("controller");
                        start = parseInt(parseInt($(this).css("left")) / slotWidth) * 15;
                        end = Math.round($(this).width() / slotWidth) * 15;

                        startTime = new Date(event.start).set({
                            hour: parseInt(start/60)
                          , minute: start % 60
                        });
                        
                        endTime = new Date(startTime).clone().add(end).minutes();
                        event.event("update", yID, [startTime, endTime])
                    }
                    if ((dateEventStart >= self._date) & (dateEventStart < self._date.clone().add(1).day()) & (yID === parseInt(event[self._scheduler.data_eventsAssigned.key]))) {
                        self._events.push(event);
                        var start = Math.round((dateEventStart.getHours() * 60) + (dateEventStart.getMinutes()));
                        var end   = Math.round((dateEventEnd.getHours()  * 60) + (dateEventEnd.getMinutes()));
                        var left  = Math.round(start/15) * slotWidth;
                        var width = Math.round(end/15) * slotWidth - left;
                        var eventView = $("<div class='ui-widget-content ui-state-active' />")
                        .append($("<div />")
                            .append(
                                $("<div><b>Job:</b>  &nbsp;"+event["clientName"] + "</div><br />")
                                .css({
                                   fontSize: 17
                                  , float: "left"
                                })
                            )
                            .append(
                                $("<div><b>Emp:</b> " + name +"</div>")
                                .css({
                                   fontSize: 17
                                  , float: "left"
                                })
                            )
                            .css({
                                float: "left"
                              , height: "100%"
                              , paddingTop: 5
                            })
                        )
                        .css({
                            "width": width - 1 + "px"
                          , "height": row.height() - 8
                          , "position":"absolute"
                          , "top":"0"
                          , "left":left + "px"
                          , "overflow":"hidden"
                        })
                        .resizable({
                            handles:"e, w"
                          , grid:[slotWidth,slotWidth]
                          , stop: update
                          , resize: function() {
                            var self = $(this);
                            var text = self.children(":nth-child(2)");
                            text.width(parseInt(($(this).width() - self.children(":nth-child(1)").width() - 20)/ slotWidth) * slotWidth);
                          }
                        })
                        .draggable({
                            axis:"x"
                          , grid:[slotWidth,slotWidth]
                          , stop: update
                          , containment: self.timeSlots
                        })
                        .click(function() {
                            document.location.href = event.url;
                        })
                        .droppable({
                            accept: '.draggable-employee'
                          , drop: function(e, ui) {
                                event = $(this).data("controller");
                                event.employeeID = ui["helper"].data("controller")["empID"]
                                event.event("update");
                                
                                $(this).children("div:nth-child(2)").children(":nth-child(3)").html(
                                    "<b>Emp: </b>" + ui["helper"].data("controller")["name"]
                                )
                                setTimeout(function(){
                                    $.each(self._scheduler._employees, function(i, employee) {
                                        employee.employee("refresh");
                                    })
                                }, 0)
                                
                            }
                        })
                        .data({"controller":event})
                        .prepend($("<img class='job-employeeStatus' />")
                            .css({
                                float:"left"
                              , "padding": "5px"
                            })
                        )
                        .appendTo(row);

                        if (parseInt(event["employeeID"]) <= 0 ) {
                            eventView.children(".job-employeeStatus").attr("src", "images/employee_empty.png")
                        } else {
                            eventView.children(".job-employeeStatus").attr("src", "images/employee_full.png")
                        }
                    }
                });
                row.appendTo(self.timeSlots);
            })

            self.day("writeEmployees");
        })
    }
  , writeEmployees: function() {
        var self = this;
        self._sidebar.html("")
        $.each(self._scheduler._employees, function(i, employee) {
            employee.employee("write", "day").appendTo(self._sidebar);
        });
    }
  , destroy: function() {
        var self = this;
        self.remove();
        self._scheduler._html_sideBar.html("");
        delete self;
    }
}
var Event = {
    __init__: function(options) {
        var self = this;
        return self.each(function() {
            //-------------------------------------- Validation
            // Validating parent scheduler
            if (typeof options.scheduler != "object") {
                throw new Error("Event: scheduler is not of type Object");
            } else {
                self._scheduler = options.scheduler;
            }

            // Validating dataObj
            if (typeof options.data === "object") {
                // Assign all elements of the data to the Event object
                $.each(options.data, function(key, value) {
                    self[key] = value

                });
            } else {
                throw new Error("Event: dataobj is not of type Object");
            }

            // HTML
            self.addClass("event draggable-event");
        })
    }
  , write: function(type, options) {
        var self = this;
        if (self.data("draggable")) {
            self.draggable("destroy")
        }
        switch(type) {
            case "week":
                self.html_week = $(
                    $("<p>" + self.clientName + "</p>")
                    .css({
                        padding: "4px"
                      , margin: "1px auto"
                      , height: "32px"
                    })
                    .disableSelection()
                )
                self.draggable({
                    revert: "invalid"
                  , opacity: 0.7
                  , helper: function() {
                        return self.clone()
                            .data("controller", self);
                  }
                  , containment: ".cal"
                  , scroll: false
                  , appendTo: $(".cal")
                })
                .click(function() {
                    document.location.href = self.url
                })
                .hover(function() {
                    $(this).addClass("ui-state-active")
                        .css({
                            border: "1px solid"
                        })
                   }, function() {
                        $(this).removeClass("ui-state-active")
                            .css({
                                border: "1px solid transparent"
                            })
                   }
                )
                .css({
                    cursor: "pointer"
                  , border: "1px solid transparent"
                })
                self.html(self.html_week)
                break;
        }
        return self
    }
  , move: function(newParent) {
        var self = this;
        self.appendTo(newParent);
        var parent = newParent.attr("id").split("-");
        var newTime = new Date(parseInt(parent[3])).clearTime();

        if ((typeof self.start === "string") & (self.start != "null")) {
            self.start = new Date(self.start);
        } else if ((typeof self.start === "undefined") || (self.start === "null")) {
            self.start = new Date(newTime).add(9).hours();
            self.end = self.start.clone().add(8).hours();
        }
        if (typeof self.end === "string") {
            self.end = new Date(self.end);
        }
        console.log("move: ", self.start)

        self.start.set({
            day: newTime.getDate()
          , month: newTime.getMonth()
          , yead: newTime.getFullYear()
        })
        self.end.set({
            day: newTime.getDate()
          , month: newTime.getMonth()
          , yead: newTime.getFullYear()
        })
        
        self.event("update", parent[2], [self.start, self.end])
  }
  , update: function(y_dataID, time) {
        var self = this;
        if (typeof y_dataID != "undefined") {
            self[self._scheduler.yData.key] = y_dataID;
        }
        if (typeof time != "undefined") {
            self.start = time[0];
            self.end = time[1];
            console.log(time)
        }
        if ((typeof self.start === "string") & (self.start != "null")) {
            self.start = new Date(self.start)
        }
        if ((typeof self.end === "string") & (self.end != "null")) {
            self.end = new Date(self.end)
        }

        // d = self.start.clone().add(self.start.getTimezoneOffset()).minutes();
        // TODO: 'Objectify' this url
        data_post("update-schedule.php", 
            function(response) {} 
            , {
                jobID: self.jobID
              , vehicleID: self[self._scheduler.yData.key]
              , start: function() {
                    if (self.start != "null") {
                        return self.start.getTime()/1000
                    } else {
                        return self.start
                    }
                }
              , end: function() {
                    if (self.end != "null") {
                        return self.end.getTime()/1000
                    } else {
                        return self.end
                    }
                }
            }
        );
        data_post("update-employee.php?jobID=" + self.jobID + "&empID=" + self.employeeID);
    }
  , decompose: function(newParent) {
        var self = this;
        self.appendTo(newParent);
        self.event("update", "null", ["null", "null"])
  }
};

var Employee = {
    __init__: function(options) {
        var self = this;
        self._type;
        return this.each(function() {
            //-------------------------------------- Validation
            // Validating parent scheduler
            if (typeof options.scheduler != "object") {
                throw new Error("Employee: scheduler is not of type Object");
            } else {
                self._scheduler = options.scheduler;
            }

            // Validating dataObj
            if (typeof options.data === "object") {
                // Assign all elements of the data to the Event object
                $.each(options.data, function(key, value) {
                    self[key] = value
                });
            } else {
                throw new Error("Employee: data is not of type Object");
            }

            // HTML
            self.addClass("event draggable-employee");
        })
    }
  , write: function(type) {
        var self = this;
        switch(type) {
            case "day":
                self._type = "day";
                self.html($("<div />")
                    .append(
                        $("<div />")
                        .css({
                            position: "relative"
                        })
                        .append(
                            $("<p class='draggable-employee'>" + self.name+ "</p>")
                            .disableSelection()                    
                            .draggable({
                                revert: "invalid"
                              , opacity: 0.7
                              , helper: function() {
                                    return $(this).clone()
                                        .data("controller", self);
                              }
                              , containment: ".cal"
                              , scroll: false
                              , appendTo: $(".cal")
                            })
                        )
                        .append( function() {
                            meter = $("<div></div>")
                                .css({
                                    position: "absolute"
                                  , top: 0
                                  , right:0
                                });

                            assigned = 0;
                            $.each(self._scheduler._day._events, function(i, event){
                                if (event.employeeID == self.empID) {
                                    assigned++
                                }
                            })
                            if (assigned === 4) {
                                self.addClass("ui-state-error");
                            } else {
                                self.removeClass("ui-state-error");
                            }
                            for(var i=0; i<4; i++) {
                                part = $("<div ></div>");
                                if (assigned > 0) {
                                    assigned--;
                                    part.css({
                                        background:"rgba(255,0,0,0.2)"
                                      , border:"1px solid rgba(128,0,0,0.2)"
                                    });
                                } else {
                                    part.css({
                                        background:"rgba(0,255,0,0.2)"
                                      , border:"1px solid rgba(0,128,0,0.2)"
                                    });
                                }
                                part.css({
                                        width:10
                                      , height:20
                                      , float:"left"
                                      , "border-left": function() {
                                            if ((i < 4) & (i != 0)){
                                                return 0;
                                            }
                                      }
                                    })
                                    .appendTo(meter);
                            }
                            return meter;
                        })
                    )
                    .css({
                        padding: "4px"
                      , margin: "1px auto"
                      , border: "1px solid transparent"
                    })
                    .hover(function() {
                        $(this).toggleClass("ui-state-active")
                            .css("border", "1px solid")
                        }
                      , function() {
                            $(this).toggleClass("ui-state-active")
                            .css({
                                border: "1px solid transparent"
                            })
                        }
                    )
                    
                );
                break;
        }
        return self
    }  
  , refresh: function() {
        var self = this;
        // TODO: Fix up unknown error
        self.employee("write", "day");
    }
};


(function( $ ){
    $.fn.scheduler = function(method) {
        return run.call(this, Scheduler, method, Array.prototype.slice.call(arguments, 1));    
    }
    $.fn.week = function(method) {
        return run.call(this, Week, method, Array.prototype.slice.call(arguments, 1));    
    }
    $.fn.day = function(method) {
        return run.call(this, Day, method, Array.prototype.slice.call(arguments, 1));    
    }
    $.fn.event = function(method) {
        return run.call(this, Event, method, Array.prototype.slice.call(arguments, 1));    
    }
    $.fn.employee = function(method) {
        return run.call(this, Employee, method, Array.prototype.slice.call(arguments, 1));    
    }
})( jQuery );



$(function() {
    c = $("#calendar").scheduler({
            eventsUnassigned: {url: "notscheduled.php", key:"vehicleID"}
          , eventsAssigned  : {url: "json-schedule.php", key:"vehicleID"}
          , employees       : {url: "employee-schedule.php", key: "empID"}
          , yData           : {url: "json-init.php", key: "vehicleID"}
    })
        .scheduler("load_week")
});     