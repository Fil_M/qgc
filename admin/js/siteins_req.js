function emailSiteReq(sitereq_id,conid,not_type,ins_no,not_date,coo,obj) {
   var emp_id = $('#empID').val();
   $.post("email.php", 'type=site_req' + '&sitereq_id='+sitereq_id + '&conid=' + conid + '&emp_id=' + emp_id + '&notify='+not_type +'&ins_no='+ins_no + '&not_date='+not_date + '&coo_id=' + coo,
         function(data) {
            if ( isNaN(data) ) {
               alert('something went wrong?\n' +data);
               return false;
            }
            else { 
					$(obj).html('Emailed');
               $(obj).addClass("emailed");
					$(obj).prop('disabled','disabled');
            }
       }
         ,"text");
}

