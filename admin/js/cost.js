function showTCEDiv() {
	var dv = $('#workdiv');
	dv.css("display","none");
	var sel = $('#tce_type').val();
	var txt = '';
	if (sel == 1) {
		txt += '<div class="div50 smll" ><input type="checkbox" name="check_1" id="check_1" class="chk" /><label>Construct lease pad and associated access road</label></div>';
      txt += '<div class="div49 smll" ><input type="checkbox" name="check_2" id="check_2" class="chk" /><label>Construct lease pad with sump and associated access road</label></div>';
      txt += '<div class="div50 smll" ><input type="checkbox" name="check_3" id="check_3" class="chk" /><label>Gravel pit</label></div>';
      txt += '<div class="div49 smll" ><input type="checkbox" name="check_4" id="check_4" class="chk" /><label>Equipment supply</label></div>';
      txt += '<div class="div50 smll" ><input type="checkbox" name="check_5" id="check_5" class="chk" /><label>Shear, clear and mulch</label></div>';
      txt += '<div class="div49 smll" ><input type="checkbox" name="check_6" id="check_6" class="chk" /><label>Fauna spotter</label></div>';
      txt += '<div style="clear:both;height:0.5rem;"> </div>';
		txt += '<div class="div50" >';
      txt += '<div class="div95 other" ><label>Other</label>';
      txt += '<textarea  name="othertext"  id="othertext" class="txt" ></textarea></div></div>';
		txt += '<div class="div50" >';
      txt += '<div class="div98 marg1" ><label >Scope of Works</label><textarea   name="scope"  id="scope" class="txt $DISABLED" $DISABLED  ></textarea></div></div>\n';

	}
	else if (sel == 2 ) {
		txt += '<div class="div50 smll" ><input type="checkbox" name="check_1" id="check_1" class="chk"  /><label>Fluid Haulage</label></div>';
      txt += '<div class="div49 smll" ><input type="checkbox" name="check_2" id="check_2" class="chk"  /><label>Rehabilitation of lease pad and associated access road</label></div>';
      txt += '<div class="div50 smll" ><input type="checkbox" name="check_3" id="check_3" class="chk"  /><label>Rehabilitation of lease pad (with sump) and associated access road</label></div>';
      txt += '<div class="div49 smll" ><input type="checkbox" name="check_4" id="check_4" class="chk"  /><label>Equipment supply</label></div>';
      txt += '<div style="clear:both;height:1.5rem;"> </div>';
		txt += '<div class="div50" >';
      txt += '<div class="div95 other" ><label>Other</label>';
      txt += '<textarea  name="othertext"  id="othertext" class="txt" ></textarea></div></div>';
		txt += '<div class="div50" >';
      txt += '<div class="div98 marg1" ><label >Scope of Works</label><textarea   name="scope"  id="scope" class="txt $DISABLED" $DISABLED  ></textarea></div></div>\n';

	}
	else if (sel == 3 ) {
		txt += '<div class="div50 smll" ><input type="checkbox" name="check_1" id="check_1" class="chk"  /><label>On-Plot Road Asset Management Process</label></div>';
      txt += '<div style="clear:both;height:0.5rem;"> </div>';
		txt += '<div class="div50" >';
      txt += '<div class="div95 other" ><label>Other</label>';
      txt += '<textarea  name="othertext"  id="othertext" class="txt" ></textarea></div></div>';
		txt += '<div class="div50" >';
      txt += '<div class="div98 marg1" ><label >Scope of Works</label><textarea   name="scope"  id="scope" class="txt $DISABLED" $DISABLED  ></textarea></div></div>\n';
	}
	txt += '<div style="clear:both;"> </div>\n';

	dv.html(txt);
	dv.fadeIn(2000);
}
function fieldDiv(obj,dvid,req_id,conid,typ) {
	 var line = '#linediv_'+ dvid;
	 var dv = '#field_div_'+ dvid;
	 var cs = $(obj).val();
	if (cs == 'closed' ) {
	 	$(obj).val('open');
     $.post("field_ests_div.php",'req_id=' + req_id + '&con_id=' + conid + '&lineno='+dvid +'&type='+typ,
     function(data) {
         $(dv).css("display","none");
         $(dv).html(data);
         $(dv).fadeIn(2000);
     }
    ,"html"); 
	  $(line).css("font-weight","bold");

	}
	else {
	 	$(obj).val('closed');
      $(dv).css("display","none");
		$(line).css("font-weight","normal");

	}
}

function updateField(obj,id,conid,typ) {
	var dv = $('#field_'+id);
	var h = obj.html();
	 $.post("upd_field.php",'field_id='+id + '&conid='+conid + '&ans=yes' +'&requestid=-1' +'&type='+typ,
         function(data) {
            if (data != 'OK') {
					alert('something went wrong?' + data);
					return false;
				}
				dv.css("background-color","#cdefbd"); 
				obj.html('Approved');
				obj.removeClass('oran');


         }
         ,"text");
}
function approveAll(obj,but,id,conid,typ) {
	  var host = window.location;
	  $.post("upd_field.php",'field_id=-1' + '&conid='+conid + '&ans=yes' +'&requestid=' + id +'&type='+typ,
         function(data) {
            if (data != 'OK') {
					alert('something went wrong?' + data);
					return false;
				}
				$(but).removeClass('oran').addClass('grn');
				obj.removeClass('oran').addClass('grn');
				loc = host + '&redo=redo';
				window.location.href = loc;
         }
         ,"text")
}
