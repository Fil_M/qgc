function updateHour(nm,conid,par) {
	var arr = nm.split("_");
   var suf =arr[1];
	var res = $("[name="+nm + "]:checked").val(); 
	var hr = $('#hour_'+suf).val();
	var dv = $('#line_'+suf);
	var rej = $('#reject_'+suf);
	if (res != 'yes' ) {
		if (typeof rej.val() != "undefined") {
			var orig = rej.val();
			if (orig.length < 1) {
				rej.val('REJECTED');
			}
		}
		var app = 'approve_'+ par;
		 $('input[name=' + app + ']').val([res]);
	}
	var rtxt = rej.val();
	// update contrcator hour record
	 $.post("upd_hour.php",'type=hour' +'&hrid='+hr + '&conid='+conid + '&ans='+res + '&reason='+rtxt,
         function(data) {
            if (data != 'OK') {
					alert('something went wrong?' + data);
					return false;
				}
				if (res == 'yes' ) {
					dv.css("background-color","#cdefbd"); 
				}
				else {
					dv.css("background-color","#ef7069");
				}

         }
         ,"text");
}
function show_perc(id,dat,coo_id,con_id) {
	var tog = '#toggle_'+id;  // closed r open
	var pdiv = '#perc_div_'+id;  // Parent Div
	var showdiv = '#perc_'+id; // hidden  "show"  div
	var state = $(tog).val();
   if (state == "closed") {
      $.post("perc_div.php",'coo_id=' + coo_id + '&date=' + dat + '&con_id=' + con_id,
      function(data) {
			 $(showdiv).css("display","none");
          $(showdiv).html(data);
			 $(showdiv).fadeIn(1500);

      }
      ,"html");
  
      //$(dv).css("display","block");
      $(pdiv).css("font-weight","bold");
      $(tog).val('open');
   }
   else {
      $(showdiv).css("display","none");
      $(pdiv).css("font-weight","normal");
      $(tog).val('closed');
   }
}
function showdiv(coo_id,dv,bt,dat,con_id) {
	var line = dv.replace('coo','linediv');
   var ht = $(bt).html();
   if (ht == "Show") {
      $.post("hour_div.php",'coo_id=' + coo_id + '&date=' + dat + '&con_id=' + con_id +'&dv=' + dv,
      function(data) {
			 $(dv).css("display","none");
          $(dv).html(data);
			 $(dv).fadeIn(1500);

      }
      ,"html");
  
      $(line).css("font-weight","bold");
      $(bt).html('Hide');
   }
   else {
      $(dv).css("display","none");
      $(line).css("font-weight","normal");
      $(bt).html('Show');
   }
}
