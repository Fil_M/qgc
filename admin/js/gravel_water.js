function showupdiv(gw_id,dv,bt,con_id) {
	var line = dv.replace('upload','linediv');
   var ht = $(bt).html();
   if (ht == "Uploads") {
      $.post("grav_div.php",'gw_id=' + gw_id +  '&con_id=' + con_id,
      function(data) {
          $(dv).css("display","none");
          $(dv).html(data);
          $(dv).fadeIn(1500);
      }
      ,"html"); 

      $(line).css("color","#888888");
      $(bt).html('Hide');
   }
   else {
      $(dv).css("display","none");
      $(line).css("color",'#000000');
      $(bt).html('Uploads');
   }
}

