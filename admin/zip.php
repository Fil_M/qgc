<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}
	$zipID = isset($_REQUEST['zip_id']) ? $_REQUEST['zip_id'] : NULL;
	


	//$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

    $hDate = isset($_REQUEST['hdate']) && strlen($_REQUEST['hdate']) > 5   ? $_REQUEST['hdate'] : NULL;
	 $con_id = isset($_REQUEST['con_id']) && intval($_REQUEST['con_id']) > 0  ? $_REQUEST['con_id'] : NULL;

	 if (isset($_REQUEST['search']) || intval($zipID) > 0 ) {
      $search = new FieldSearch();
		$search->ID = $zipID;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->creDate = !empty($_REQUEST['sdate'])   ? $_REQUEST['sdate'] : date ('d-m-Y',time() - (86400 * 14));
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

   }
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }


	$pg = new ZipList($action,$hDate,$search);
?>
