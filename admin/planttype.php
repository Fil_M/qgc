<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	if (isset($_REQUEST['plant_type_id'])) {
		$plantTypeID = $_REQUEST['plant_type_id'];
	}
	else {
		$plantTypeID = 0;
	}

	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

	 if (isset($_REQUEST['search'])) {
      $search = new FieldSearch();
      $search->pTypeName =isset($_REQUEST['ptype'])  && strlen($_REQUEST['ptype']) > 0 ? $_REQUEST['ptype'] : NULL;
      $search->contractorID =isset($_REQUEST['contractor']) && intval($_REQUEST['contractor']) > 0 ? intval($_REQUEST['contractor']) : NULL;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : NULL;
      $search->creDate = isset($_REQUEST['eff_date']) && strlen($_REQUEST['eff_date']) > 5   ? $_REQUEST['eff_date'] : NULL;
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

   }
   else {
      $search = NULL;
   }

	

	$pg = new PlantType($action,$plantTypeID,$search,$redo);
?>
