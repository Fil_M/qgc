--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE admin_menu (
    admin_menu_id integer NOT NULL,
    level smallint,
    menu_text character varying(32),
    href character varying(64),
    parent_menu_id integer,
    menu_order smallint,
    min_profile_id smallint,
    company_id integer
);


ALTER TABLE public.admin_menu OWNER TO postgres;

--
-- Name: admin_menu_admin_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_menu_admin_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_menu_admin_menu_id_seq OWNER TO postgres;

--
-- Name: admin_menu_admin_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_menu_admin_menu_id_seq OWNED BY admin_menu.admin_menu_id;


--
-- Name: admin_menu_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_menu ALTER COLUMN admin_menu_id SET DEFAULT nextval('admin_menu_admin_menu_id_seq'::regclass);


--
-- Data for Name: admin_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin_menu (admin_menu_id, level, menu_text, href, parent_menu_id, menu_order, min_profile_id, company_id) FROM stdin;
37	0	Hours	\N	\N	6	0	1
46	0	Reports	\N	\N	7	0	1
50	1	Daily Percentages	work.php?action=new	46	1	3	1
52	1	Print Invoices	invoice.php?action=print	46	3	3	1
53	1	Reconcile Hours	reconcile.php?action=update	46	4	3	1
39	1	Find/Print Hours	hours.php?action=list 	37	2	2	1
56	1	Fix Broken Hours	hours1.php?action=list	37	3	3	1
2	0	Employees	\N	\N	1	0	0
7	0	System	\N	\N	8	0	0
10	1	Add Employee	employee.php?action=new&type=Employee	2	1	3	0
11	1	Find Employee	employee.php?action=list&type=Employee	2	2	2	0
20	1	System Setup	system.php	7	1	3	0
41	1	Add Area	area.php?action=new	40	1	3	2
42	1	Find Area	area.php?action=list	40	2	2	0
43	0	Crew	\N	\N	5	0	0
44	1	Add Crew	crew.php?action=new	43	1	3	2
45	1	Find Crew	crew.php?action=list	43	2	2	0
48	1	Add Well	well.php?action=new	47	1	3	2
49	1	Find Well	well.php?action=list	47	2	2	0
55	1	Find Plant Type	planttype.php?action=list	6	4	2	0
12	1	Add Plant	plant.php?action=new	6	1	3	1
13	1	Find Plant	plant.php?action=list	6	2	2	1
54	1	Add Plant Type	planttype.php?action=new	57	1	3	2
58	1	Find Plant Type	planttype.php?action=list	57	2	2	2
51	1	Print Daily PDF's	work.php?action=print	46	2	3	1
59	0	Contractors	\N	\N	2	0	2
40	0	Area	\N	\N	3	0	0
47	0	Well	\N	\N	4	\N	0
6	0	Plant	\N	\N	6	0	1
57	0	Plant Type	\N	\N	6	0	2
60	1	Add Contractor	contractor.php?action=new	59	1	3	2
61	1	Find Contractor	contractor.php?action=list	59	2	2	2
62	0	Forms	\N	\N	7	0	2
65	1	Add Call Off Order	calloff.php?action=new	62	3	3	2
66	1	Find Call Off Order	calloff.php?action=list	62	4	2	2
67	1	Find Field Cost Estimate	field.php?action=list	59	3	2	2
63	1	Add Request Cost Estimate	cost_estimate.php?action=new	62	1	3	2
64	1	Find Request Cost Estimate	cost_estimate.php?action=list	62	2	2	2
\.


--
-- Name: admin_menu_admin_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_menu_admin_menu_id_seq', 67, true);


--
-- Name: admin_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY admin_menu
    ADD CONSTRAINT admin_menu_pkey PRIMARY KEY (admin_menu_id);


--
-- PostgreSQL database dump complete
--

