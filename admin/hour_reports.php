<?php  // sub scope reports from hours 
 	require_once("include/boot.php");
	//ini_set('display_errors','1');
	//error_reporting(E_ALL);
	$empID = $_SESSION['employee_id'];
	$conID = $_REQUEST['con_id'];
	$dat = $_REQUEST['date'];
	$type = $_REQUEST['type'];
	$dbarr=unserialize($_SESSION['dbarr']);
	$dbLink = $dbarr[$conID][0];
	$shortName = $dbarr[$conID][2];
   $conName =   $dbarr[$conID][4];
	$sql = "SELECT  distinct(calloff_order_id),area_name,wells_from_ids(co.well_ids) as well_name  from {$shortName}_hour
	LEFT JOIN area a using (area_id) 
	LEFT JOIN calloff_order co using(calloff_order_id)
 	where hour_date = '$dat'
	order by calloff_order_id";


   if (!$data = $conn->getAll($sql)) {
		if ($conn->ErrorNo() != 0 ) {
			die($conn->ErrorMsg());
		}	
	}

	if ($type == "csv" ) {
		$file= "tmp/subscope_percentage_{$dat}_{$conID}_{$empID}.csv";
      $headings = array('','Date','Contractor','C.O.O.','Work Scope','Sub Scope','Percentage Complete','Percentage of TCE','Total to Date','Original Estimate');
      $fp = fopen($file,'w');
      $title =  array("Sub Scope Percentages as of $dat");
      fputcsv($fp,$title);
      $lineCount=1;
      fputcsv($fp,$headings);
		foreach($data as $key=>$val) {
			extract($val);
			$subTitle = array("Call Off Order: $calloff_order_id for Area: $area_name Well/s: $well_name"); 
      	fputcsv($fp,$subTitle);
			$sql = "SELECT * from percent_est($calloff_order_id,'$dat',true)";
   		$sql = pg_escape_literal($sql);
   		$stmt = "SELECT * from dblink('$dbLink',$sql,true) AS t1(crew_name text,sub_crew_name text,tce_est double precision,subtot_to_date double precision,percentage_complete double precision)" ;

      	if (! $subs = $conn->getAll($stmt)) {
         	if($conn->ErrorNo() != 0 ) {
            	echo $stmt;
            	die($conn->ErrorMsg());
         	}
      	}
			foreach($subs as $k=>$v) {
				extract($v);
				$tce_est = $tce_est > 0 ? $tce_est : 1;
				$perc= $percentage_complete . "%";
      		$tcePerc = round($subtot_to_date / $tce_est,2) * 100 . "%";
				$subtot_to_date = number_format($subtot_to_date);
      		$tce_est = number_format($tce_est);

				fputcsv($fp,array('',$dat,$conName,$calloff_order_id,$crew_name,$sub_crew_name,$perc,$tcePerc,$subtot_to_date,$tce_est));
			}
			fputcsv($fp,array());
		}
		fclose($fp);

		if (file_exists($file)) {
               header('Content-Description: File Transfer');
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment; filename='.basename($file));
               header('Content-Transfer-Encoding: binary');
               header('Expires: 0');
               header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
               header('Pragma: public');
               header('Content-Length: ' . filesize($file)); //Remove

            readfile($file);
            exit;
      }

	}
	
?>
