<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
require_once("include/boot.php");
 $query=$_REQUEST['query'];
 $type=$_REQUEST['type'];
 $ar = array();
 	switch ($type) {
 	case "operator" :
		$ar = getEmployee($query);
		break;
 	case "unit" :
		$dat = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');
		$ar = getUnit($query,$dat);
		break;
 	case "area" :
		$ar = getArea($query);
		break;
 	case "plant" :
		$ar = getPlant($query);
		break;
 	case "othertxt" :
		$ar = otherTxt($query);
		break;
 	case "sitecond" :
		$ar = siteCond($query);
		break;
	case "planttype" :
      $ar = getPlantTypes($query);
      break;
	case "coo" :
      $ar = getCoos($query);
      break;
	case "tag" :
      $ar = getTags($query);
      break;
	case "companyone" :
      $ar = getComOneContact($query);
      break;
	case "atwtype" :
      $ar = getAtwType($query);
      break;
	case "landowner" :
      $ar = getLandOwner($query);
      break;
   case "calloffs" :
     $ar = getCallOffs($query);
    break;
   case "tce" :
     $ar = getTCE($query);
    break;
   case "ins_no_req" :
     $ar = getSiteInsReq($query);
    break;
   case "atwno" :
     $ar = getATWno($query);
    break;

   }
 echo json_encode($ar);

function getATWno($query) {
   global $conn;
	//$conID = 17;
   $suggestions = $dat = array();
   $sql = "select  atw_id,atw_number from atw where atw_number like '$query%' order by atw_number";


   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   foreach($data as $key=>$val) {
      $suggestions[] = $val['atw_number']; 
      $dat[] = $val['atw_id'];
   }
   $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
   return $ar;
}
function getTCE($query) {
   global $conn;
   $conID = $_REQUEST['con_id'];
	//$conID = 17;
   $suggestions = $dat = array();
   $sql = "select request_estimate_id from request_estimate re 
   Where request_estimate_id::text like '$query%' 
   and re.removed is false
   and re.contractor_id = $conID 
   order by request_estimate_id";


   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   foreach($data as $key=>$val) {
      $suggestions[] = $val['request_estimate_id']; 
      $dat[] = array($val['request_estimate_id'],$conID);
   }
   $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
   return $ar;
}
function getSiteInsReq($query) {
   global $conn;
   $conID = $_REQUEST['con_id'];
	//$conID = 17;
   $suggestions = $dat = array();
   $sql = "select site_ins_required_id,instruction_no from site_ins_required sir
   Where instruction_no::text like '$query%' 
   and sir.contractor_id = $conID 
   order by sir.instruction_no";


   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   foreach($data as $key=>$val) {
      $suggestions[] = $val['instruction_no']; 
      $dat[] = array($val['instruction_no'],$val['site_ins_required_id'],$conID);
   }
   $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
   return $ar;
}
function getCallOffs($query) {
   global $conn;
   $conID = $_REQUEST['con_id'];
	//$conID = 17;
   $suggestions = $dat = array();
   $sql = "select calloff_order_id from calloff_order co
   Where calloff_order_id::text like '$query%' 
   and co.removed is false
   and co.contractor_id = $conID 
   order by co.calloff_order_id";


   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   foreach($data as $key=>$val) {
      $suggestions[] = $val['calloff_order_id']; 
      $dat[] = array($val['calloff_order_id'],$conID);
   }
   $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
   return $ar;
}

function getAtwType($query) {
   global $conn;
   $q= strtoupper(pg_escape_string($query));
   $sql = "SELECT * from atw_type where upper(type_name)  like '$q%' and atw_type_id < 6"; //  don't need other
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['type_name'];
      $dat[] = $val['atw_type_id'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function otherTxt($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT other_text , substring(other_text from 0 for 30) as txt from request_estimate where upper(other_text) like '$q%' and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['txt'];
      $dat[] = $val['other_text'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function siteCond($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT site_conditions , substring(site_conditions from 0 for 30) as txt from request_estimate where upper(site_conditions) like '$q%' and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['txt'];
      $dat[] = $val['site_conditions'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getComOneContact($query) {
	global $conn;
	//$query= preg_replace('/\'/',"''",$query);
   $q= strtoupper($query);
	$sql = "SELECT 'EXT' as seltype ,contact_id, coalesce(contact_firstname,'') || ' ' || coalesce(contact_lastname,'') as name , contact_firstname,coalesce(contact_lastname,'') as contact_lastname,contact_email 
      from contact where upper(contact_firstname)  like '$q%' and removed is false and length(contact_email) > 0 and length(contact_lastname) > 0
UNION 
select 'CON' as seltype,contractor_id as contact_id,name || ' - CONTRACTOR' as name ,name as contact_firstname,'CONTRACTOR' as contact_lastname,a.email as contact_email
	from contractor c
	LEFT JOIN address_to_relation adr using (contractor_id)
	LEFT JOIN address a using (address_id)
	where upper(c.name)  like '$q%' and removed is false and length(a.email) > 0
UNION
select 'EMP' as seltype,employee_id as contact_id, coalesce(firstname,'') || ' ' || coalesce(lastname,'')  as name,firstname as contact_firstname,coalesce(lastname,'')  as contact_lastname,a.email as contact_email
	from employee e
	LEFT JOIN address_to_relation adr using (employee_id)
	LEFT JOIN address a using (address_id)
	where upper(e.firstname)  like '$q%' and removed is false and length(a.email) > 0 and length(lastname) > 0
order by contact_firstname";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['name'];
      $dat[] = array("conid"=>$val['contact_id'],"firstname"=>$val['contact_firstname'],"lastname"=>$val['contact_lastname'],"email"=>$val['contact_email'],"seltype"=>$val['seltype']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getLandOwner($query) {
   global $conn;
   $query= preg_replace('/\'/',"''",$query);
   $q= strtoupper($query);
   $sql = "SELECT landowner_id, owner_name,address from landowner where upper(owner_name)  like '$q%'  and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['owner_name'];
      $dat[] = array("landowner_id"=>$val['landowner_id'],"address"=>$val['address']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getEmployee($query) {
	global $conn;
   $conID = $_REQUEST['con_id'];
   $dbarr=unserialize($_SESSION['dbarr']);
   $shortName = $dbarr[$conID][2];
	$query= preg_replace('/\'/',"''",$query);
   $q= strtoupper($query);
   $sql = "SELECT employee_id, coalesce(firstname,'') || ' ' || lastname as name from {$shortName}_employee where upper(lastname)  like '$q%' and employee_id > 2  and removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['name'];
      $dat[] = $val['employee_id'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getUnit($query,$dat) {
   global $conn;
   $q= strtoupper($query);
	$shortName = $_REQUEST['shortname'];
   $sql = "SELECT plant_id,plant_unit,p_type from {$shortName}_plant p LEFT join plant_type pt using(plant_type_id) where upper(plant_unit) like '$q%' and p.removed is false";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }  
	
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['plant_unit'];
      $dat[] =  array($val['plant_id'],$val['p_type']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}

function getTags($query) {  // autocomplete from Hour Filter
   global $conn;
	$conID = $_REQUEST['con_id'];
	$dbarr=unserialize($_SESSION['dbarr']);
   $shortName = $dbarr[$conID][2];

   $q= strtoupper($query);
   $sql = "SELECT plant_id,plant_unit from {$shortName}_plant where upper(plant_unit) like '$q%'";
   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['plant_unit'];
      $dat[] =  array($val['plant_id']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getPlant($query) {
   global $conn;
   $q= strtoupper($query);
   $sql = "SELECT distinct(plant_name) as plant_name,plant_id from plant where  upper(plant_name) like '$q%' and removed is false";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['plant_name'];
      $dat[] = $val['plant_id'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getPlantTypes($query) {
   global $conn;
	$conID = isset($_REQUEST['con_id']) ? intval($_REQUEST['con_id']) : 0;
	$andClause = $conID > 0 ? " and contractor_id = $conID " : "";
   $q= strtoupper($query);
   $today = date('Y-m-d');
   $sql = "SELECT plant_type_id ,p_type from plant_type where  removed is false $andClause and upper(p_type) like '$q%'  and price_type != 'OPEN' order by p_type";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['p_type'];
      $dat[] = $val['plant_type_id'];
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}
function getCoos($query) {
   global $conn;
	$andClause = "";
	if (!empty($_REQUEST['con_id'])) {
		$conID = intval($_REQUEST['con_id']);
		$andClause = " and co.contractor_id = $conID";
	}
	$sql = "select co.calloff_order_id,co.contractor_id,a.area_name, wells_from_ids(co.well_ids) as well_name,crew_id from calloff_order  co
	LEFT JOIN area a  on a.area_id = co.area_id
	where calloff_order_id::text like '$query%'  $andClause order by calloff_order_id";

   if (! $data = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
   $suggestions = array();
   $dat = array();
   foreach($data as $key=>$val) {
      $suggestions[] =  $val['calloff_order_id'];
		$dat[] = array($val['contractor_id'],$val['area_name'],$val['well_name'],$val['crew_id']);
   }
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar;
}

function getArea($query) {
   global $conn;
	$q= strtoupper($query);
	$sql = "SELECT area_id,area_name from area where upper(area_name) like '$q%' and removed is false order by area_name";

	if (! $data = $conn->getAll($sql)) {
		if ($conn->ErrorNo()  != 0 ) {
			die($conn->ErrorMsg());
		}
	}
	$suggestions = array();
	$dat = array();
	foreach($data as $key=>$val) {
		$suggestions[] =  $val['area_name'];
		$dat[] = array($val['area_id']);
	}
 $ar = array('query'=>$query,'suggestions'=>$suggestions,'data'=>$dat);
 return $ar; 
}  


?>
