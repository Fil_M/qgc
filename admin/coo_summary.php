<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	$conID = isset($_REQUEST['con_id']) && strlen($_REQUEST['con_id']) > 0 ? $_REQUEST['con_id'] : NULL;
	if (isset($_REQUEST['contractor']) && is_null($conID) ) {
		$conID = intval($_REQUEST['contractor']);
	}  

	
	$cooID = isset($_REQUEST['coo_id']) && strlen($_REQUEST['coo_id']) > 0 ? $_REQUEST['coo_id'] : NULL;

	$pg = new COOSummary($action,$cooID,$conID);
?>
