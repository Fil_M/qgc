<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}
	if (isset($_REQUEST['request_id'])) {
		$request_id = $_REQUEST['request_id'];
	}
	else if (isset($_REQUEST['requestID'])) {
		$request_id = $_REQUEST['requestID'];
	}
	else {
		$request_id =  0; // p[osted form variable
	}
	if (isset($_REQUEST['lineno'])) {
		$lineNo = $_REQUEST['lineno'];
	}
	else {
		$lineNo=NULL;
	}
	if (isset($_REQUEST['type'])) {
		$type = $_REQUEST['type'];
	}
	else {
		$type=NULL;
	}
	if (isset($_REQUEST['search'])) {
		$search = new FieldSearch();
      $search->fieldEstID =!empty($_REQUEST['field_id']) ? $_REQUEST['field_id'] : NULL;
      $search->requestID =!empty($_REQUEST['req_id']) ? $_REQUEST['req_id'] : NULL;
      $search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']) : NULL;
      $search->areaName = !empty($_REQUEST['area'])   ? $_REQUEST['area'] : NULL;
		$wellID =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->crewID =isset($_REQUEST['crew']) ? intval($_REQUEST['crew']) : NULL;
		$search->subCrewID =isset($_REQUEST['sub_scope']) ? intval($_REQUEST['sub_scope']) : NULL;
      $search->contractorID =!empty($_REQUEST['contractor']) ? intval($_REQUEST['contractor']) : NULL;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->creDate = !empty($_REQUEST['sdate'])  ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

	}
	else {
		$search = NULL;
	}
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

	$pg = new CostEstimate($action,$request_id,$lineNo,$type,$search,$redo);
?>
