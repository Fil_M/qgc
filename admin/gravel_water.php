<?php  //  QGC Hours
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	$employeeID = $_SESSION['employee_id'];
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}

	 if (isset($_REQUEST['printcsv'])) {
       $file="tmp/water_gravel_{$employeeID}.csv";
      if (file_exists($file)) {
               header('Content-Description: File Transfer');
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment; filename='.basename($file));
               header('Content-Transfer-Encoding: binary');
               header('Expires: 0');
               header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
               header('Pragma: public');
               header('Content-Length: ' . filesize($file)); //Remove

            readfile($file);
            exit;
      }

		else {
			die("no file");
		}

   }


	$con_id = isset($_REQUEST['con_id']) && intval($_REQUEST['con_id']) > 0  ? $_REQUEST['con_id'] : NULL;
	$gravel_id = isset($_REQUEST['gravel_id']) ? $_REQUEST['gravel_id'] : 0;
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;
	$order=isset($_REQUEST['order']) ? $_REQUEST['order'] : NULL;

	 if (isset($_REQUEST['search']) ) {
      $search = new Search();
      $search->cooID =isset($_REQUEST['coo_id']) ? intval($_REQUEST['coo_id']): NULL;
      $search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']): NULL;
      $search->areaName = isset($_REQUEST['area']) && strlen($_REQUEST['area']) > 0  ? $_REQUEST['area'] : NULL;
      $well =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;    //   different  select made from selects.php  $type
      $search->wellID =is_array($well) ? $well[0] : $well;    //   different  select made from selects.php  $type
      $search->crewID =isset($_REQUEST['crew']) ? $_REQUEST['crew'] : NULL; 
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalto";
      $search->hDate = isset($_REQUEST['sdate']) && strlen($_REQUEST['sdate']) > 5   ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;
      $search->crewID =isset($_REQUEST['crew']) ? $_REQUEST['crew'] : NULL;
      $search->ownerID =isset($_REQUEST['landowner']) && intval($_REQUEST['landowner']) > 0 ? $_REQUEST['landowner'] : NULL;
      //$search->status = isset($_REQUEST['status']) && intval($_REQUEST['status']) > 0  ? $_REQUEST['status'] : NULL;
   }
	else if (isset($_REQUEST['setcon'])) {
      $search = NULL;
		$con_id = $_REQUEST['contractor'];
	}
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }


	$pg = new GravelWater($action,$gravel_id,$con_id,$search,$redo,$order);
?>
