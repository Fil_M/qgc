<?php  // Index.php
 	require_once("include/boot.php");
 	Functions::verify();
	$employeeID = $_SESSION['employee_id'];
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}
	if (isset($_REQUEST['startdate'])) {
		$startDate=$_REQUEST['startdate'];
	}
	else {
		$startDate=AdminFunctions::beginLastMonth();
	}
	if (isset($_REQUEST['enddate'])) {
		$endDate=$_REQUEST['enddate'];
	}
	else {
		$endDate=AdminFunctions::endLastMonth();
	}
	if (isset($_REQUEST['printcsv'])) {
      $file= $_REQUEST['file'];
      if (file_exists($file)) {
               header('Content-Description: File Transfer');
               header('Content-Type: application/vnd.ms-excel');
               header('Content-Disposition: attachment; filename='.basename($file));
               header('Content-Transfer-Encoding: binary');
               header('Expires: 0');
               header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
               header('Pragma: public');
               header('Content-Length: ' . filesize($file)); //Remove

            readfile($file);
            exit;
      }

   }



	$pg = new MonthCSV($action,$startDate,$endDate);
?>
