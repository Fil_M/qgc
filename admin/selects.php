<?php  // ajaxes from hours     QGC  QGC  !!!
 	require_once("include/boot.php");
	//ini_set('display_errors','1');
	//error_reporting(E_ALL);
	$val = $_REQUEST['val'];
	$type = $_REQUEST['type'];
	$conID = isset($_REQUEST['contractor_id']) ? $_REQUEST['contractor_id'] : 0;


if ($type == "unit" ) {
		$sql = "SELECT plant_id,plant_unit from plant where removed is false and plant_name= '$val' order by plant_unit";
		if (! $rs=$conn->Execute($sql)) {
			if ($conn->ErrorNo() != 0 ) {
				die($conn->ErrorMsg());
			}
		}

		$html = "<select name=\"unit\" id=\"unit\"  style=\"width:100%;\"  >";
		while (! $rs->EOF) {
			$id = $rs->fields['plant_id'];
			$name = $rs->fields['plant_unit'];
			$html .= "<option value=\"$id\">$name</option>";
			$rs->MoveNext();
		}
		$html .= "</select>";
		echo $html;
}  
else if ($type == "well" ) {
	$lineno = isset($_REQUEST['lineno']) ? $_REQUEST['lineno'] : 1;
	$sql = "SELECT well_id,well_name from well where contractor_id in (0,$conID) and removed is false and area_id  = $val order by well_name";
		if (! $rs=$conn->Execute($sql)) {
			if ($conn->ErrorNo() != 0 ) {
				die($conn->ErrorMsg());
			}
		}
		$html = "<label>Well/s</label>\n";
		$html .= "<select name=\"well[]\" id=\"well_$lineno\" class=\"required\" style=\"width:100%;\"  onChange=\"getCOO(this);\" >";
		$html .= "<option></option>";
		while (! $rs->EOF) {
			$id = $rs->fields['well_id'];
			$name = $rs->fields['well_name'];
			$html .= "<option value=\"$id\">$name</option>";
			$rs->MoveNext();
		}
		$html .= "</select>";
		echo $html;
}   
else if ($type == "well_s" ) {
	$sql = "SELECT well_id,well_name from well where contractor_id in (0,$conID) and removed is false and area_id  = $val order by well_name";
		if (! $rs=$conn->Execute($sql)) {
			if ($conn->ErrorNo() != 0 ) {
				die($conn->ErrorMsg());
			}
		}

		$html = "<label>Well/s</label>\n";
		$html .= "<select name=\"well\" id=\"well\" class=\"required\" style=\"width:100%;\"  >";
		$html .= "<option></option>";
		while (! $rs->EOF) {
			$id = $rs->fields['well_id'];
			$name = $rs->fields['well_name'];
			$html .= "<option value=\"$id\">$name</option>";
			$rs->MoveNext();
		}
		$html .= "</select>";
		echo $html;
}   
else if ($type == "well_m" ) {
		$html = AdminFunctions::wellSelectMulti($val,NULL,"");
		echo $html;
}   
else if ($type == "crew" ) {
 $html = AdminFunctions::crewSelect(NULL,"");
 echo $html;
}
else if ($type == "coo" ) {
   $wellID=$_REQUEST['well'];
   $areaID=$_REQUEST['area'];
   $crewID=$_REQUEST['crew'];
   $sql = "SELECT calloff_order_id from calloff_order co
      where contractor_id = $conID 
      and area_id = $areaID      
      and crew_id = $crewID   
      and $wellID in (select (unnest(string_to_array(co.well_ids::text,'|')::int[]))) 
      and removed is false ";

   if (! $cooID = $conn->getOne($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
   }
 echo $cooID;
}
else if ($type == "gwl_loc" ) {
	$html = AdminFunctions::locSelect(NULL,$val,$conID,"","location_$val",false,true);
	echo $html;
}
else if ($type == "landowner" ) {
	$html = AdminFunctions::ownerSelect(NULL,$val,"","landowner",true);
	echo $html;
}
elseif ($type == "cooline" ) {
   $cooID=$val;

   $sql = "SELECT co.crew_id,crew_name,co.area_id,area_name,co.well_ids,wells_from_ids(co.well_ids) as wells,subscopes_from_ids(co.sub_crew_ids) as subscopes,sub_crew_ids,email_emp_ids from calloff_order co
   LEFT JOIN area a using (area_id)
   LEFT JOIN crew c using(crew_id)
   where calloff_order_id = $cooID";
   $data=$conn->getRow($sql);
   if ($conn->ErrorNo() != 0 ) {
       die($conn->ErrorMsg());
   }
   extract($data);
	$sups = explode("|",$email_emp_ids); // array
   $subscopes = AdminFunctions::splitStr($subscopes);
   $wells = AdminFunctions::splitStr($wells);

   $ar = array($crew_name,$subscopes,$area_name,$wells,$crew_id,$sub_crew_ids,$area_id,$well_ids,$sups);
   echo json_encode($ar);
}


?>
