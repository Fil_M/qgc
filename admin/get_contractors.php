<?php  // ajaxes from hours 
 	require_once("include/boot.php");
	ini_set('display_errors','1');
	error_reporting(E_ALL);
	$areaID = $_REQUEST['area'];
	$wellID = $_REQUEST['well'];
	$conID = $_REQUEST['con_id'];
	$andClause = $conID > 0 ? " and co.contractor_id = $conID" : "";
   $sql = "SELECT area_name,well_name from well 
          LEFT JOIN area a using(area_id) 
         where well_id = $wellID";
         if (! $data = $conn->getRow($sql)) {
            die($conn->ErrorMsg());
         }
         extract($data);

  	$sql = "with tab as (select con_name,name,c.contractor_id from calloff_order co
	LEFT JOIN contractor c using(contractor_id)
 	where area_id = $areaID and $wellID in (select (unnest(string_to_array(well_ids::text,'|')::int[]))) $andClause order by contractor_id,well_ids)
	select distinct(con_name),name,contractor_id from tab order by con_name";
   if (! $condata = $conn->getAll($sql)) {
      if ($conn->ErrorNo() != 0 ) {
         die($conn->ErrorMsg());
      }
      else {
			//echo "$sql <br />";
         echo "<h3>No Calloff Orders found for Area: $area_name with Well/Pad: $well_name</h3>";
         return;
      }
   }
	 $content = "<div style=\"clear:both;\" ></div>\n";
	 $content .= "<div style=\"float:left;width:668px;\" >";
	 $content .= "<div style=\"width:120px;float:right;height:2rem;\" ><label style=\"float:left;\" >Download ALL</label><input type=\"checkbox\" style=\"float:right;margin:0;\" id=\"clickAll\" onclick=\"selectAll($(this));\" /></div>\n";
	 $content .= "<div style=\"clear:both;\" ></div>\n";
	 $content .= "<div class=\"heading\" >";
    $content .= "<div class=\"cli hd wbdr\">Contractor</div>\n";
    $content .= "<div class=\"addr narr hd wbdr\" >&nbsp;</div>\n";
    $content .= "<div class=\"cli hd wbdr\" >Download</div>\n";
    $content .= "</div>\n"; // Close heading
	 foreach($condata as $ind=>$val) {
		extract($val);
		$content .= "<div style=\"display:table;table-layout:fixed;border-bottom:1px solid #000;float:left;\" >";
      $content .= "<div class=\"cli highh\">$con_name</div>\n";
      $content .= "<div class=\"addr narr highh\" >&nbsp;</div>\n";
      $content .= "<div class=\"cli highh bdr\" ><input type=\"checkbox\" class=\"conclick\" style=\"margin-left:90px;\" name=\"concheck[]\" value=\"$contractor_id\" /></div>\n";
		$content .= "</div>\n";

	 }
	 //$content .= "<br />$sql";

	$content .= "<input type=\"submit\" name=\"SUBMIT\" value=\"Submit\" class=\"button margt4\"  />\n";
	$content .= "</div>\n";  // close left div
	echo $content;

?>
