<?php  
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	if (isset($_REQUEST['well_id'])) {
		$well_id = $_REQUEST['well_id'];
	}
	else {
		$well_id = 0;
	}
	  $redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

   if (isset($_REQUEST['search']) ) {
      $search = new FieldSearch();
      $search->cooID =isset($_REQUEST['coo'])  && intval($_REQUEST['coo']) > 0 ? $_REQUEST['coo'] : NULL;
      $search->siteInsNum =isset($_REQUEST['site_ins']) && intval($_REQUEST['site_ins']) > 0 ? $_REQUEST['site_ins'] : NULL;
      $search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']) : NULL;
      $search->areaName = strlen($_REQUEST['area']) > 0  ? $_REQUEST['area'] : NULL;
		$well =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->wellID = ! is_null($well) && is_array($well)  ? $well[0] : $well;
   }
   else {
      $search = NULL;
   }

	

	$pg = new Well($action,$well_id,$search,$redo);
?>
