<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}
	if (isset($_REQUEST['grav_req_id'])) {
		$grav_req_id = $_REQUEST['grav_req_id'];
	}
	else if (isset($_REQUEST['gravReqID'])) {
		$grav_req_id = $_REQUEST['gravReqID'];
	}
	else {
		$grav_req_id =  0; // p[osted form variable
	}
	$cooID = isset($_REQUEST['coo_id']) ? $_REQUEST['coo_id'] : NULL;

	if (isset($_REQUEST['search'])) {
		$search = new FieldSearch();
      $search->ID =isset($_REQUEST['grav_req_id']) && intval($_REQUEST['grav_req_id']) > 0 ? $_REQUEST['grav_req_id'] : NULL;
      $search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']) : NULL;
      $search->areaName = isset($_REQUEST['area']) && strlen($_REQUEST['area']) > 0  ? $_REQUEST['area'] : NULL;
		$well =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->wellID = ! is_null($well) && is_array($well)  ? $well[0] : $well;
      $search->crewID =isset($_REQUEST['crew']) ? intval($_REQUEST['crew']) : NULL;
      $search->contractorID =isset($_REQUEST['contractor']) && intval($_REQUEST['contractor']) > 0 ? intval($_REQUEST['contractor']) : NULL;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->creDate = isset($_REQUEST['cre_date']) && strlen($_REQUEST['cre_date']) > 5   ? $_REQUEST['cre_date'] : date('d-m-Y');
      $search->endDate = isset($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

	}
	else {
		$search = NULL;
	}
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

	$pg = new GravelTCE($action,$grav_req_id,$search,$redo);
?>
