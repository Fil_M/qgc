<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	if (isset($_REQUEST['request_id'])) {
		$request_id = $_REQUEST['request_id'];
	}
	else {
		$request_id = NULL;
	}
	if (isset($_REQUEST['calloff_id'])) {
		$coo_id = $_REQUEST['calloff_id'];
	}
	else {
		$coo_id = 0;
	}
	//$coo_id = isset($_REQUEST['cooID']) ? $_REQUEST['cooID'] : 0;

	if (isset($_REQUEST['search'])) {
      $search = new FieldSearch();
      $search->cooID =!empty($_REQUEST['coo_id'])   ? $_REQUEST['coo_id'] : NULL;
      $search->requestID =!empty($_REQUEST['req_id'])  ? $_REQUEST['req_id'] : NULL;
      $search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']) : NULL;
      $search->areaName = !empty($_REQUEST['area'])  ? $_REQUEST['area'] : NULL;
      $wellID =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->wellID = ! is_null($wellID) && is_array($wellID) ? $wellID[0] : $wellID;
      $search->crewID =isset($_REQUEST['crew']) ? intval($_REQUEST['crew']) : NULL;
      $search->subCrewID =isset($_REQUEST['sub_scope']) ? intval($_REQUEST['sub_scope']) : NULL;
      $search->contractorID =!empty($_REQUEST['contractor']) ? intval($_REQUEST['contractor']) : NULL;
      $search->dateType = !empty($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->creDate = !empty($_REQUEST['sdate'])    ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate =!empty($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

   }
   else {
      $search = NULL;
   }
   $redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

	

	$pg = new CallOff($action,$coo_id,$request_id,$search,$redo);
?>
