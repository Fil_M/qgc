<?php  // Index.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}

	$con_id = isset($_REQUEST['con_id']) && intval($_REQUEST['con_id']) > 0  ? $_REQUEST['con_id'] : NULL;
	$comp_id = isset($_REQUEST['comp_id']) ? $_REQUEST['comp_id'] : 0;
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;
	$order=isset($_REQUEST['order']) ? $_REQUEST['order'] : NULL;


	 if (isset($_REQUEST['search']) ) {
      $search = new FieldSearch();
      $search->contractorID =isset($_REQUEST['con_id']) ? intval($_REQUEST['con_id']): NULL;
      $search->ID =isset($_REQUEST['comp_id']) ? $_REQUEST['comp_id'] : NULL; 
      $search->cooID =isset($_REQUEST['coo_id']) ? $_REQUEST['coo_id'] : NULL; 
		$search->areaID =isset($_REQUEST['areaID']) ? intval($_REQUEST['areaID']): NULL;
      $search->areaName = !empty($_REQUEST['area']) ? $_REQUEST['area'] : NULL;
		$wellID =isset($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->wellID = ! is_null($wellID) && is_array($wellID) ? $wellID[0] : $wellID;
      $search->crewID =isset($_REQUEST['crew']) ? intval($_REQUEST['crew']) : NULL;
      $search->subCrewID =isset($_REQUEST['sub_scope']) ? intval($_REQUEST['sub_scope']) : NULL;
      $search->dateType = isset($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalto";
      $search->creDate = !empty($_REQUEST['sdate'])   ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate = !empty($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;
		$search->status = !empty($_REQUEST['compl_status']) ? intval($_REQUEST['compl_status']) :NULL;

   }
	else if (isset($_REQUEST['setcon'])) {
      $search = NULL;
		$con_id = $_REQUEST['contractor'];
	}
   else {
      $search = NULL;
    //   unset($_SESSION['SQL']); 
   }


	$pg = new Complete($action,$comp_id,$con_id,$search,$redo,$order);
?>
