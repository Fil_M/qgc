<?php  // site_instruction.php
 	require_once("include/boot.php");

 	Functions::verify();
	if ($_SESSION['company_id'] != 2 ) {
      return false;
   }
	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	else {
		$action="list";
	}
	if (isset($_REQUEST['site_ins_id'])) {
		$site_ins_id = $_REQUEST['site_ins_id'];
	}
	else if (isset($_REQUEST['requestID'])) {
		$site_ins_id = $_REQUEST['requestID'];
	}
	else {
		$site_ins_id =  0; // p[osted form variable
	}
	$con_id = isset($_REQUEST['con_id']) && intval($_REQUEST['con_id']) > 0  ? $_REQUEST['con_id'] : NULL;
	if (isset($_REQUEST['lineno'])) {
      $lineNo = $_REQUEST['lineno'];
   }
   else {
      $lineNo=NULL;
   }
   if (isset($_REQUEST['type'])) {
      $type = $_REQUEST['type'];
   }
   else {
      $type=NULL;
   }
	if (isset($_REQUEST['search'])) {
		$search = new FieldSearch();
      $search->fieldEstID =!empty($_REQUEST['field_id']) ? $_REQUEST['field_id'] : NULL;
      $search->requestID =!empty($_REQUEST['req_id'])  ? $_REQUEST['req_id'] : NULL;
      $search->siteInsNum =!empty($_REQUEST['ins_no'])  > 0 ? intval($_REQUEST['ins_no']) : NULL;
      $search->cooID =!empty($_REQUEST['call_id'])  ? intval($_REQUEST['call_id']) : NULL;
      $search->areaID =!empty($_REQUEST['areaID']) ? intval($_REQUEST['areaID']) : NULL;
      $search->areaName = !empty($_REQUEST['area'])   ? $_REQUEST['area'] : NULL;
		$well =!empty($_REQUEST['well']) ? $_REQUEST['well'] : NULL;
      $search->wellID = ! is_null($well) && is_array($well)  ? $well[0] : $well;
      $search->crewID =!empty($_REQUEST['crew']) ? intval($_REQUEST['crew']) : NULL;
		$search->subCrewID =isset($_REQUEST['sub_scope']) ? intval($_REQUEST['sub_scope']) : NULL;
      $search->dateType = !empty($_REQUEST['datetype']) ? $_REQUEST['datetype'] : "equalgreater";
      $search->creDate = !empty($_REQUEST['sdate'])    ? $_REQUEST['sdate'] : date('d-m-Y');
      $search->endDate = !empty($_REQUEST['end_date'])  ? $_REQUEST['end_date'] : NULL;

	}
	else if (isset($_REQUEST['setcon'])) {  //
      $search = NULL;
      $con_id = $_REQUEST['contractor'];
   }
	else {
		$search = NULL;
	}
	$redo=isset($_REQUEST['redo']) ? $_REQUEST['redo'] : NULL;

	$pg = new SiteInstruction($action,$site_ins_id,$con_id,$lineNo,$type,$search,$redo);
?>
